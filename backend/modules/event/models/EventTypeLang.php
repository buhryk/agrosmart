<?php

namespace backend\modules\event\models;

use Yii;

class EventTypeLang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'event_type_lang';
    }

    public function rules()
    {
        return [
            [['type_id', 'lang', 'title'], 'required'],
            [['type_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 128],
            ['title', 'uniqueWithLang']
        ];
    }

    public function uniqueWithLang($attribute, $params)
    {
        $models = $this::find()
            ->where(['title' => $this->$attribute, 'lang' => Yii::$app->language])
            ->all();

        if ($models && (count($models) > 1 || $models[0]->primaryKey != $this->primaryKey)) {
            $this->addError($attribute, Yii::t('common', 'Value') . ' "'.$this->$attribute.'" ' .
                Yii::t('common', 'is already used for this field'));
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => Yii::t('event', 'Type'),
            'lang' => Yii::t('common', 'Lang'),
            'title' => Yii::t('event', 'Title'),
        ];
    }
}