<?php

namespace backend\modules\event\models;

use Yii;

class EventLang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'event_model_lang';
    }

    public function rules()
    {
        return [
            [['event_id', 'lang', 'title', 'short_description', 'text'], 'required'],
            [['event_id'], 'integer'],
            [['short_description', 'text', 'address'], 'string'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => Yii::t('event', 'Event'),
            'lang' => Yii::t('common', 'Lang'),
            'title' => Yii::t('event', 'Title'),
            'short_description' => Yii::t('event', 'Short description'),
            'text' => Yii::t('event', 'Text'),
        ];
    }
}