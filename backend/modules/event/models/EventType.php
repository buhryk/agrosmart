<?php

namespace backend\modules\event\models;

use Yii;
use backend\modules\transliterator\services\TransliteratorService;

class EventType extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'event_type';
    }

    public function rules()
    {
        return [
            [['alias'], 'string', 'max' => 128],
            [['alias'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
        ];
    }

    public function getLang()
    {
        return $this->hasOne(EventTypeLang::className(), ['type_id' => 'id'])
            ->where(['lang' => \Yii::$app->language]);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function beforeSave($insert)
    {
        $this->alias = TransliteratorService::transliterate($this->alias);

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function beforeDelete()
    {
        EventTypeLang::deleteAll(['type_id' => $this->primaryKey]);
        Event::deleteAll(['type_id' => $this->primaryKey]);

        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}