<?php
namespace backend\modules\event\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use backend\modules\transliterator\services\TransliteratorService;
use backend\modules\event\models\EventSubject;
use backend\modules\event\models\EventSubjectLang;

class SubjectController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => EventSubject::find()
        ]);

        return $this->render('index', [
            'data' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new EventSubject();
        $modelLang = new EventSubjectLang();
        $modelLang->lang = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->subject_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->subject_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findSubject($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findSubject($id);

        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new EventSubjectLang();
            $modelLang->subject_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                if ($modelLang->lang == TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT) {
                    $model->alias = TransliteratorService::transliterate($modelLang->title);
                } else {
                    $modelLangForAlias = EventSubjectLang::find()
                        ->where([
                            'subject_id' => $model->primaryKey,
                            'lang' => TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT
                        ])
                        ->one();

                    if ($modelLangForAlias) {
                        $model->alias = TransliteratorService::transliterate($modelLangForAlias->title);
                    }
                }
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $category = $this->findSubject($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    private function findSubject($id)
    {
        $model = EventSubject::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('event', 'Subject not found'));
        }

        return $model;
    }
}