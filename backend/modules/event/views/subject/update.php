<?php
$this->title = Yii::t('event', 'Subject updating') . ' "' . $model->title . '"';

$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Subjects list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Subject updating')];
?>

<div class="rubric-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>