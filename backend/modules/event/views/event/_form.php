<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;
use backend\modules\event\models\EventSubject;
use backend\modules\event\models\EventType;
use backend\modules\commondata\models\Country;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'subject_id')->dropDownList(
                    ArrayHelper::map(EventSubject::find()->all(), 'id', 'title'), ['prompt' => '']
            ); ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'type_id')->dropDownList(
                    ArrayHelper::map(EventType::find()->all(), 'id', 'title'), ['prompt' => '']
            ); ?>
        </div>
        <div class="col-md-4 col-xs-12">
            <?= $form->field($model, 'country_id')->dropDownList(
                    ArrayHelper::map(Country::find()->all(), 'id', 'title'), ['prompt' => '']
            ); ?>
        </div>
    </div>

    <?= $form->field($modelLang, 'title') ?>
    <?= $form->field($modelLang, 'short_description')->textarea(); ?>
    <?= $form->field($modelLang, 'text')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/site/image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <?= $form->field($model, 'alias') ?>
    <?= $form->field($model, 'active')->dropDownList(\backend\modules\event\models\Event::getAllActiveProperties()); ?>

    <?= $form->field($model, 'start_date', ['options'=>['style' => 'width: 300px; float:left;']])->widget(
        DatePicker::className(), [
        'inline' => true,
        'language' => 'ru',
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <?= $form->field($model, 'end_date', ['options'=>['style' => 'width: 300px; float:left;']])->widget(
        DatePicker::className(), [
        'language' => 'ru',
        'inline' => true,
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]); ?>

    <div style="clear: both"></div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>