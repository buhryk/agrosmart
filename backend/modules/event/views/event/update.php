<?php
$this->title = Yii::t('event', 'Event updating');
$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Events list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model,
        'modelLang' => $modelLang
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>