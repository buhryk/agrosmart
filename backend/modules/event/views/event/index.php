<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\event\models\EventSubject;
use backend\modules\event\models\EventType;
use backend\modules\commondata\models\Country;
use backend\modules\event\models\Event;
use dosamigos\datepicker\DatePicker;

$this->title = Yii::t('event', 'Events list');
?>
<div class="event-index">
    <h1>
        <?= Html::encode($this->title) ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-primary block right">
            <?= Yii::t('event', 'Add event'); ?>
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'title',
                'label' => Yii::t('event', 'Title'),
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['event/view', 'id' => $model->primaryKey]);
                },
            ],
            [
                'attribute' => 'subject_id',
                'filter' => ArrayHelper::map(EventSubject::find()->all(), 'id', 'title'),
                'format'=>'raw',
                'value' => function ($model) {
                    $subject = $model->subject;
                    return $subject ? $subject->title : '';
                }
            ],
            [
                'attribute' => 'type_id',
                'filter' => ArrayHelper::map(EventType::find()->all(), 'id', 'title'),
                'format'=>'raw',
                'value' => function ($model) {
                    $type = $model->type;
                    return $type ? $type->title : '';
                }
            ],
            [
                'attribute' => 'country_id',
                'filter' => ArrayHelper::map(Country::find()->all(), 'id', 'title'),
                'format'=>'raw',
                'value' => function ($model) {
                    $country = $model->country;
                    return $country ? $country->title : '';
                }
            ],
            'alias',
            [
                'attribute' => 'start_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'end_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'end_date',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'active',
                'filter' => Event::getAllActiveProperties(),
                'value' => 'activeDetail'
            ],
            [
                'contentOptions'=>['style'=>'width: 120px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>
</div>