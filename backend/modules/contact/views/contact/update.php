<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\contact\models\Contact */

$this->title = 'Контакт: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Контакты', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="contact-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
