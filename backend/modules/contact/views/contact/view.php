<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\contact\models\Contact */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Контакты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="contact-form">
        <div class="row">
            <?php $form = ActiveForm::begin(); ?>
                <div class="col-md-3">
                <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>
                </div>
                <div class="col-md-3">
                    <br>
                    <div class="form-group">
                        <?= Html::submitButton('Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
             <?php ActiveForm::end(); ?>
         </div>
    </div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at:datetime',
            'updated_at:datetime',
            'name',
            'surname',
            'phone',
            'email:email',
            'message:ntext',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->statusList[$model->status];
                },
            ],
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->statusList[$model->status];
                    },  
            ],
                
            
        ],
    ]) ?>

</div>
