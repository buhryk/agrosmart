<?php

namespace backend\modules\contact\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use yii\web\Controller;

/**
 * Default controller for the `contact` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
}
