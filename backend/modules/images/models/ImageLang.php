<?php

namespace backend\modules\images\models;

use Yii;

class ImageLang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'images_lang';
    }

    public function rules()
    {
        return [
            [['image_id', 'lang'], 'required'],
            [['image_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title', 'alt'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_id' => 'Image ID',
            'lang' => Yii::t('common', 'Lang'),
            'title' => Yii::t('common', 'Title'),
            'alt' => 'Alt',
        ];
    }
}