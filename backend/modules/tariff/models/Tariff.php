<?php

namespace backend\modules\tariff\models;

use Yii;

/**
 * This is the model class for table "tariff".
 *
 * @property integer $id
 * @property string $price
 * @property integer $count_deys
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $position
 * @property string $model_name
 */
class Tariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const TYPE_ALLOTTED = 0;
    const TYPE_TOP = 1;
    const TYPE_COMPLEX = 2;

    const MODEL_PRICE = 'price';
    const MODEL_COMPANY = 'company';
    const MODEL_PRODUCT = 'product';

    public static function tableName()
    {
        return 'tariff';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [['price', 'count_deys', 'model_name'], 'required'],
            [['price'], 'number'],
            [['count_deys', 'type', 'created_at', 'updated_at', 'position'], 'integer'],
            [['model_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => Yii::t('common', 'Price'),
            'count_deys' => Yii::t('common', 'Количество дней'),
            'type' => Yii::t('common', 'Тип'),
            'created_at' => Yii::t('common', 'Создано'),
            'updated_at' => Yii::t('common', 'Update'),
            'position' => Yii::t('common', 'Position'),
            'model_name' => Yii::t('common', 'Для'),
            'typeName' => 'Тип',
        ];
    }

    public function getTypeList() {
        return [
            self::TYPE_ALLOTTED => Yii::t('common', 'Выделить желтым') ,
            self::TYPE_TOP => Yii::t('common', 'Поднять в топ'),
            self::TYPE_COMPLEX => Yii::t('common', 'Выделить желтым и поднять в топ'),
        ];
    }

    public function getTypeName()
    {
        return isset($this->typeList[$this->type]) ? $this->typeList[$this->type] : '';
    }

    public function getModelName()
    {
        return isset($this->modelList[$this->model_name]) ? $this->modelList[$this->model_name] : '';
    }

    public function getModelList() {
        return [
            self::MODEL_COMPANY => Yii::t('common', 'Компании') ,
            self::MODEL_PRICE => Yii::t('common', 'Цены'),
            self::MODEL_PRODUCT => Yii::t('common', 'Продукция'),
        ];
    }
}
