<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\tariff\models\Tariff */

$this->title = Yii::t('common', 'Update Tariff');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="tariff-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
