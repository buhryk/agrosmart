<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\tariff\models\Tariff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tariff-form">
    <div class="col-md-7">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'count_deys')->textInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'type')->dropDownList($model->typeList) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'position')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'model_name')->dropDownList($model->modelList) ?>
            </div>
        </div>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>


</div>
