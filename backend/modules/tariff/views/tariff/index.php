<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tariff\models\TariffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тарифы на рекламу';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить тариф', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'price',
            'count_deys',
            'updated_at:date',
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->typeName;
                },
                'filter' => $searchModel->typeList
            ],
            [
                'attribute' => 'madel_name',
                'value' => function($model) {
                    return $model->modelName;
                },
                'filter' => $searchModel->modelList
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
