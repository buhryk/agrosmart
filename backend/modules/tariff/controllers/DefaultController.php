<?php

namespace backend\modules\tariff\controllers;

use yii\web\Controller;
use backend\modules\accesscontrol\AccessControlFilter;

/**
 * Default controller for the `tariff` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
}
