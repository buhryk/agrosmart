<?php

namespace backend\modules\seo\models;

use backend\modules\seo\models\SeoLang;
use Yii;

/**
 * This is the model class for table "seo".
 *
 * @property integer $id
 * @property string $table_name
 * @property integer $record_id
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 */
class Seo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_name', 'record_id'], 'required'],
            [['record_id'], 'integer'],
            [['table_name'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => Yii::t('common', 'Table name'),
            'record_id' => Yii::t('common', 'Record id'),
        ];
    }

    public function getLang()
    {
        return $this->hasOne(SeoLang::className(), ['seo_id' => 'id'])
            ->where(['lang' => Yii::$app->language]);
    }

    public function getTitle()
    {
        return isset($this->lang) ? $this->lang->meta_title : '';
    }

    public function getDescription()
    {
        return isset($this->lang) ? $this->lang->meta_description : '';
    }

    public function getKeywords()
    {
        return isset($this->lang) ? $this->lang->meta_keywords : '';
    }

    public static function registerMetaTags(SeoLang $seo)
    {
        if ($seo->meta_title) {
            \Yii::$app->view->title = $seo->meta_title;
        }
        if ($seo->meta_keywords) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $seo->meta_keywords,
            ]);
        }
        if ($seo->meta_description) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $seo->meta_description,
            ]);
        }
    }
}