<?php

namespace backend\modules\seo\models;

use Yii;

class SeoLang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'seo_lang';
    }

    public function rules()
    {
        return [
            [['seo_id', 'lang'], 'required'],
            [['seo_id'], 'integer'],
            [['meta_keywords', 'meta_description'], 'string'],
            [['lang'], 'string', 'max' => 5],
            [['meta_title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seo_id' => 'Seo',
            'lang' => Yii::t('common', 'Lang'),
            'meta_title' => Yii::t('common', 'Meta title'),
            'meta_keywords' => Yii::t('common', 'Meta keywords'),
            'meta_description' => Yii::t('common', 'Meta description'),
        ];
    }
}