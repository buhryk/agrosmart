<?php

namespace backend\modules\package\controllers;

use backend\modules\package\models\PackageLang;
use Yii;
use backend\modules\package\models\Package;
use backend\modules\package\models\PackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PackageController implements the CRUD actions for Package model.
 */
class PackageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Package models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Package model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Package model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Package();
        $modelLang = new PackageLang();
        
        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post()) && $model->save()) {
            $model->saveInstrument(Yii::$app->request->post('inastrument_id'));
            $modelLang->package_id = $model->primaryKey;
            $modelLang->save(false);
            
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    /**
     * Updates an existing Package model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new PackageLang();
            $modelLang->package_id = $model->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post()) && $model->save() && $modelLang->save()) {
            $model->saveInstrument(Yii::$app->request->post('instrument_id'));
            Yii::$app->session->setFlash('success', 'Успешно сохранено');
            return $this->redirect(['index']);
        } else {

            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    /**
     * Deletes an existing Package model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Package model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Package the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Package::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
