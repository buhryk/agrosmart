<?php

namespace backend\modules\package\models;

use backend\modules\commondata\models\SiteRole;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "package".
 *
 * @property integer $id
 * @property integer $site_role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $price
 * @property integer $count_user
 * @property integer $interim
 */
class Package extends \yii\db\ActiveRecord
{
    const SHOW_PAGE = 1;
    const HIDE_PAGE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_role_id', 'price'], 'required'],
            [['site_role_id', 'created_at', 'updated_at', 'count_user', 'interim', 'position', 'is_page'], 'integer'],
            [['price', 'old_price'], 'number'],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_role_id' => 'Роль',
            'created_at' => 'Создано',
            'updated_at' => 'Редактирова',
            'price' => 'Цена',
            'count_user' => 'Количество пользователей',
            'interim' => 'Дней',
            'old_price' => 'Старая цена',
            'is_page' => 'Показывать на странице'
        ];
    }

    public function getLang()
    {
        return $this->hasOne(PackageLang::className(), ['package_id' => 'id'])
            ->where(['lang' => \Yii::$app->language]);
    }

    public function getRole()
    {
        return $this->hasOne(SiteRole::className(), ['id' => 'site_role_id']);
    }

    public function getRoleName()
    {
        return $this->role ? $this->role->name : '';
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->name : '';
    }

    public function getDescription()
    {
        return $this->lang ? $this->lang->description : '';
    }

    public function getText()
    {
        return $this->lang ? $this->lang->text : '';
    }

    public function getInstruments()
    {
        return $this->hasMany(PackageInstrument::className(), ['package_id' => 'id']);

    }

    public function getCheckedInstrument()
    {
        return ArrayHelper::getColumn($this->instruments, 'instrument_id');
    }

    public function saveInstrument($items)
    {
        PackageInstrument::deleteAll(['package_id' => $this->id]);
        if(!is_array($items)) {return ;}
        foreach ($items as $item) {
            $model = new PackageInstrument();
            $model->instrument_id = $item;
            $model->package_id = $this->id;
            $model->save();
        }
    }




}
