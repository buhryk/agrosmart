<?php

namespace backend\modules\package\models;

use backend\modules\commondata\models\SiteRole;
use frontend\modules\cabinet\models\PackageUser;
use frontend\modules\notification\models\Notification;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "package".
 *
 * @property integer $id
 * @property integer $site_role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $price
 * @property integer $count_user
 * @property integer $interim
 */
class TariffFinish
{
    public $time;
    public $date;

    public function __construct()
    {
        $this->time = time();
        $date = new \DateTime(date('Y-m-d'));
        $date->modify('+3 day');
        $this->date = $date->format('Y-m-d');
    }

    public function sendNotification()
    {
        $packageList = PackageUser::find()
            ->andWhere(['<=', 'start', $this->time])
            ->andWhere(['LIKE', "from_unixtime(`created_at`, '%Y-%m-%d')", $this->date])
            ->orderBy('id DESC')
            ->all();

        if(!$packageList) return NULL;
        foreach ($packageList as $item) {

            if(PackageUser::TYPE_COMPANY == $item->type) {
                $user = $item->model->admin->user;
            } else {
                $user = $item->model;
            }
            $this->createdNotification($user);
            $this->sendEmail($user, $item);
        }
    }
    
    public function sendEmail($user, $data)
    {
        Yii::$app->mailer->compose(
            ['html' => 'finishTariff'],
            ['data' => $data]
        )
            ->setTo($user->email)
            ->setFrom([\Yii::$app->params['supportEmail'] => 'AGROSMART'])
            ->setSubject(Yii::t('sendEmail/subject','Ваш тарифный план заканчивается'))
            ->send();
    }
    
    public function createdNotification($user)
    {
        $model = new Notification();
        $model->type = Notification::TYPE_USER_MESSAGE;
        $model->target_id = $user->id;
        $model->message = Yii::t('sendNotification', 'Ваш тарифный план заканчивается через 3 дня.');
        $model->save();
    }


    
}