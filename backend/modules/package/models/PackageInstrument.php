<?php

namespace backend\modules\package\models;

use Yii;

/**
 * This is the model class for table "package_instrument".
 *
 * @property integer $package_id
 * @property integer $instrument_id
 */
class PackageInstrument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package_instrument';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id', 'instrument_id'], 'required'],
            [['package_id', 'instrument_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'package_id' => 'Package ID',
            'instrument_id' => 'Instrument ID',
        ];
    }
}
