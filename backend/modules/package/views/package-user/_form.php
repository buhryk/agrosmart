<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\package\models\Package;
use kartik\datetime\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\PackageUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="package-user-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'package_id')->dropDownList(ArrayHelper::map(Package::find()->all(),'id' ,'title' )) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'type')->dropDownList($model->typeList) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'model_id')->textInput()->label('Id пользователя или компании') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <label>Старт</label>
            <?php echo DateTimePicker::widget([
                'name' => 'start',
                'value' => $model->start ? date('Y-m-d H:i:s', $model->start) : date('Y-m-d H:i:s'),
                'pluginOptions' => [
                    'autoclose' => true,
                ]
            ]); ?>
        </div>
        <div class="col-md-4">
            <label>Финиш</label>
             <?php echo DateTimePicker::widget([
                    'name' => 'finish',
                    'value' => $model->start ? date('Y-m-d H:i:s', $model->finish) : '',
                    'pluginOptions' => [
                        'autoclose' => true,
                    ]
                ]); ?>
        </div>
    </div>

<hr>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
