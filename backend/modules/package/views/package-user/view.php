<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\cabinet\models\PackageUser;
use backend\modules\package\models\Package;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\PackageUser */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи пакетов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at:datetime',
            'updated_at:datetime',
            'detailType',
            [
                'attribute' => 'package_id',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->packageTitle, ['package/view', 'id' => $model->package_id]);
                },
            ],
            [
                'attribute' => 'model_id',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->type == PackageUser::TYPE_COMPANY) {
                        return Html::a($model->model->name, ['/consumer/company/view', 'id' => $model->id]);
                    } else {
                        return Html::a($model->model->fullName, ['/consumer/user/view', 'id' => $model->id]);
                    }
                }
            ],
            'start:datetime',
            'finish:datetime',
            [
                'attribute' => 'refill_id',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->refill_id, ['/refill/refill/view', 'id' => $model->refill_id]);
                },
            ],
        ],
    ]) ?>

</div>
