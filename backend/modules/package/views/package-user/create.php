<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\PackageUser */

$this->title = 'Create Package User';
$this->params['breadcrumbs'][] = ['label' => 'Package Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
