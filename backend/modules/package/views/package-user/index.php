<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\cabinet\models\PackageUser;
use backend\modules\package\models\Package;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\package\models\PackageUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи пакетов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at:datetime',
            [
                'attribute' => 'package_id',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->packageTitle, ['package/view', 'id' => $model->package_id]);
                },
                'filter' => \yii\helpers\ArrayHelper::map(Package::find()->all(),'id' , 'title')
            ],
            [
                'attribute' => 'model_id',
                'format' => 'raw',
                'value' => function ($model) {
                    if($model->type == PackageUser::TYPE_COMPANY) {
                        return Html::a($model->model->name, ['/consumer/company/view', 'id' => $model->model->id]);
                    } else {
                        return Html::a($model->model->fullName, ['/consumer/user/view', 'id' => $model->model->id]);
                    }
                }
            ],
            [
                'attribute' => 'type',
                'value' => function($model){
                    return $model->detailType;
                },
                'filter' => $searchModel->typeList,
            ],
            'start:datetime',
            'finish:datetime',
            // 'refill_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
