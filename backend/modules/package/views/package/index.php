<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\commondata\models\SiteRole;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\package\models\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пакеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить пакет', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'title',
                'value' => function($model) {
                    return $model->title;
                } ,
                'filter' => ArrayHelper::map(SiteRole::find()->andWhere(['authorized' => SiteRole::AUTHORIZED_YES])->all(), 'id', 'name'),
            ],
            'roleName',
            'updated_at:datetime',
            'price',
            'count_user',
            'interim',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
