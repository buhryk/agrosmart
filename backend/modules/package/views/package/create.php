<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\package\models\Package */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Пакеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>
