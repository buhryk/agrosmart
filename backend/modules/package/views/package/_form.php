<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\commondata\models\SiteRole;
use yii\helpers\ArrayHelper;
use backend\modules\instrument\models\Instrument;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\package\models\Package */
/* @var $form yii\widgets\ActiveForm */
$roleList = SiteRole::find()->where(['authorized' => SiteRole::AUTHORIZED_YES])->all();
$instrumentList = Instrument::find()->all();
?>

<div class="package-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-8">
    <?= $form->field($model, 'site_role_id')->dropDownList(ArrayHelper::map($roleList,'id' ,'name'))?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'old_price')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'is_page')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'count_user')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'interim')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'position')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <?=$form->field($modelLang, 'name')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($modelLang, 'description')->widget(ImperaviWidget::className(), [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 100,
                    'plugins' => [
                        'clips',
                        'fullscreen'
                    ],
                    'imageUpload' => Url::to(['/site/image-upload']),
                    'convertDivs' => false,
                    'replaceDivs' => false
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($modelLang, 'text')->widget(ImperaviWidget::className(), [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 100,
                    'plugins' => [
                        'clips',
                        'fullscreen'
                    ],
                    'imageUpload' => Url::to(['/site/image-upload']),
                    'convertDivs' => false,
                    'replaceDivs' => false
                ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="input02" class="col-sm-2 control-label">Доступные инструменты</label>
                <div class="col-sm-12 instrument-access">
                    <?= Html::checkboxList('instrument_id', $model->checkedInstrument, ArrayHelper::map($instrumentList,'id' ,'title'));
                    ?>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
