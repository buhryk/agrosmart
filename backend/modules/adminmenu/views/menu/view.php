<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('accesscontrol', 'Adminmenu list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Role title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Role description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Admin menu relative path'); ?></th>
            <td><?= $model->path; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Admin menu parent'); ?></th>
            <td>
                <?php $parent = $model->parent; ?>
                <?php if ($parent) { ?>
                    <a href="<?= Url::to(['view', 'id' => $parent->primaryKey]); ?>">
                        <?= $parent->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Admin menu icon'); ?></th>
            <td>
                <?php if ($model->icon) { ?>
                    <span class="<?= $model->icon; ?>"></span>
                <?php } ?>
            </td>
        </tr>
        <?php if (!$parent) { ?>
            <tr>
                <th><?= Yii::t('accesscontrol', 'Admin menu submenus'); ?></th>
                <td>
                    <?php $submenus = $model->submenus; ?>
                    <?php foreach ($submenus as $submenu) { ?>
                        <a href="<?= Url::to(['view', 'id' => $submenu->primaryKey]); ?>" class="block">
                            <?= $submenu->title; ?>
                        </a>
                    <?php } ?>
                    <p></p>
                    <a href="<?= Url::to(['create', 'parent' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('accesscontrol', 'Add submenu'); ?>"
                       class="btn btn-default btn-sm block left"
                    >
                        <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;<?= Yii::t('common', 'Add') ;?>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>