<?php
use yii\helpers\Html;

$this->title = Yii::t('accesscontrol', 'Creating admin menu item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('accesscontrol', 'Adminmenu list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>