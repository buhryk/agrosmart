<?php
use yii\helpers\Url;

$this->title = Yii::t('accesscontrol', 'Adminmenu list');
?>

<div class="rubric-default-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-primary block right">
            <?= Yii::t('accesscontrol', 'Add menu item'); ?>
        </a>
    </h1>

    <table class="table table-bordered" style="margin-top: 15px;">
        <thead>
        <tr>
            <th width="75">ID</th>
            <th><?= Yii::t('accesscontrol', 'Admin menu title'); ?></th>
            <th><?= Yii::t('accesscontrol', 'Admin menu description'); ?></th>
            <th><?= Yii::t('accesscontrol', 'Admin menu submenus'); ?></th>
            <th width="50"><?= Yii::t('accesscontrol', 'Admin menu icon'); ?></th>
            <th width="130"><?= Yii::t('common', 'Actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $model) { ?>
            <tr>
                <td><?= $model->primaryKey; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"><?= $model->title; ?></a>
                </td>
                <td>
                    <?= $model->description; ?>
                </td>
                <td>
                    <?php $submenus = $model->submenus; ?>
                    <?php foreach ($submenus as $submenu) { ?>
                        <a href="<?= Url::to(['view', 'id' => $submenu->primaryKey]); ?>" class="block">
                            <?= $submenu->title; ?>
                        </a>
                    <?php } ?>
                </td>
                <td style="text-align: center">
                    <?php if ($model->icon) { ?>
                        <span class="<?= $model->icon; ?>"></span>
                    <?php } ?>
                </td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'View'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                    <a href="<?= Url::to(['update', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Edit'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="<?= Url::to(['delete', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Delete'); ?>"
                       class="btn btn-default btn-sm"
                       onclick="return confirm('<?= Yii::t('common', 'Are you sure you want to delete this item?'); ?>')"
                    >
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>