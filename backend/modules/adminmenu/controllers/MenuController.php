<?php
namespace backend\modules\adminmenu\controllers;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use backend\modules\adminmenu\models\Menu;
use backend\modules\accesscontrol\AccessControlFilter;

class MenuController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Menu::find()->where(['parent_id' => null]),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'data' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new Menu();
        $model->parent_id = Yii::$app->request->get('parent');

        if ($model->parent_id) {
            $parent = $this->findModel($model->parent_id);

            if ($parent->parent_id) {
                throw new BadRequestHttpException('Can not create third level menu');
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}