<?php
/**
 * @var View $this
 * @var SourceMessage $model
 */

use yii\helpers\Html;
use yii\web\View;
use Zelenin\yii\modules\I18n\models\SourceMessage;
use Zelenin\yii\modules\I18n\Module;
use Zelenin\yii\SemanticUI\collections\Breadcrumb;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\widgets\ActiveForm;

$this->title = $model->message;

$this->params['breadcrumbs'][] = ['label' => 'Переводы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<section class="tile">
    <!-- tile header -->
    <div class="tile-header dvd dvd-btm">
        <h1 class="custom-font">
            <strong><?= Elements::header(Module::t('','Source message'), ['class' => 'top attached']) ?></strong>
            <?= Elements::segment(Html::encode($model->message), ['class' => 'bottom attached']) ?>
        </h1>
    </div>
    <!-- /tile header -->
    <!-- tile body -->
    <div class="tile-body">
        <?php $form = ActiveForm::begin([ 'options' => ['class'=>'form-horizontal']]); ?>
        <?php foreach ($model->messages as $language => $message) : ?>

            <div class="form-group">
                <label for="input02" class="col-sm-2 control-label"><?=$language ?></label>
                <div class="col-sm-10">
                            <?= $form->field($model->messages[$language], '[' . $language . ']translation')
                                ->textarea(['class'=>'form-control'])
                                ->label(false) ?>
                </div>
            </div>
            <hr class="line-dashed line-full">

        <?php endforeach; ?>
        <hr class="line-dashed line-full">
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-1">
                <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <?php ActiveForm::end()  ?>
    </div>
    <!-- /tile body -->
</section>