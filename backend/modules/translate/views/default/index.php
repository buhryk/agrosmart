<?php
/**
 * @var View $this
 * @var SourceMessageSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use Zelenin\yii\modules\I18n\models\search\SourceMessageSearch;
use Zelenin\yii\modules\I18n\models\SourceMessage;
use Zelenin\yii\modules\I18n\Module;

$this->title = Yii::t('admin','Translations');
$this->params['breadcrumbs'][] = $this->title;

?>


<section class="tile">

    <!-- tile header -->
    <div class="tile-header dvd dvd-btm">
        <h1 class="custom-font"><strong><?=$this->title ?></strong> </h1>
        
    </div>
    <!-- /tile header -->
    <!-- tile body -->
    <div class="tile-body">
        <div class="table-responsive">
            <div id="editable-usage_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="dataTables_length" id="editable-usage_length">

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id="editable-usage_filter" class="dataTables_filter">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        Pjax::begin(['id' => 'trnslate-list']);
                        echo GridView::widget([
                            'filterModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                [
                                    'attribute' => 'id',
                                    'value' => function ($model, $index, $dataColumn) {
                                        return $model->id;
                                    },
                                    'filter' => false
                                ],
                                [
                                    'attribute' => 'translate',
                                    'label' => 'Перевод',
                                    'value' => function ($model, $index, $widget) {
                                        /** @var SourceMessage $model */
                                        return '';
                                    },
                                    'options' => [
                                        'style' => 'width: 65px;',
                                    ],
                                ],
                                [
                                    'attribute' => 'message',
                                    'label' => 'Источник сообщения',
                                    'format' => 'raw',
                                    'value' => function ($model, $index, $widget) {
                                        return Html::a($model->message, ['update', 'id' => $model->id], ['data' => ['pjax' => 0], 'class'=>'modalButton']);
                                    }
                                ],
                                [
                                    'attribute' => 'category',
                                    'label' => 'Категория',
                                    'value' => function ($model, $index, $dataColumn) {
                                        return $model->category;
                                    },
                                    'filter' => ArrayHelper::map($searchModel::getCategories(), 'category', 'category')
                                ],
                                [
                                    'attribute' => 'status',
                                    'label' => 'Статус',
                                    'value' => function ($model, $index, $widget) {
                                        /** @var SourceMessage $model */
                                        return $model->isTranslated() ? 'Translated' : 'Not translated';
                                    },
                                    'filter' => $searchModel->getStatus()
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{update} {delete}',
                                ]
                            ]
                        ]);
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /tile body -->
</section>