<?php
/**
 * @var View $this
 * @var SourceMessage $model
 */

use yii\helpers\Html;
use yii\web\View;
use Zelenin\yii\modules\I18n\models\SourceMessage;
use Zelenin\yii\modules\I18n\Module;
use Zelenin\yii\SemanticUI\collections\Breadcrumb;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\widgets\ActiveForm;

//$this->title = Module::t('Update') . ': ' . $model->message;




$this->registerJs('
        $("#translate_btn").click(function() {
            $(".form-group").removeClass("has-error");      //remove error class
            $(".help-block").html("");                      //remove existing error messages
    
            var form_data = $("#form-translate").serialize();
            var action_url = $("#form-translate").attr("action");
            $("#kartik-modal").modal("hide");
            $.ajax({
                method: "POST",
                url: action_url,
                data: form_data
            })
            .done(function( data ) {
				$.pjax.reload({container:"#translate-list"}); 
                if(data.success == true)    {     
				  //data saved successfully
                } else {       //validation errors occurred
           
                }
            });
            return false;
        });
    ', \yii\web\View::POS_READY, 'my-ajax-form-submit');
?>



<div class="modal-dialog modal-md">
    <div class="modal-content">
  
        <section class="tile">
        	<div class="tile-header dvd dvd-btm">
                <ul class="controls pull-right">
                    <li class="remove"><a role="button" tabindex="0" data-dismiss="modal" aria-hidden="true" class="tile-close">
                            <i class="fa fa-times"></i>
                        </a>
                    </li>
                </ul>
               <h2 class="custom-font">
                    <strong><?= Elements::header(Yii::t('admin','Source message'), ['class' => 'top attached']) ?></strong>
                    <?= Elements::segment(Html::encode($model->message), ['class' => 'bottom attached']) ?>
                </h2>

            </div>
            
            <!-- tile body -->
            <div class="tile-body">
                 <?php $form = ActiveForm::begin([
                    'enableClientValidation'=>false,
                    'options'=>[
                        'class'=>'form-horizontal',
                        'id'=>'form-translate',
                        'data-pjax' => true
                    ]]); ?>

				<?php foreach ($model->messages as $language => $message) : ?>
        
                    <div class="form-group">
                        <label for="input02" class="col-sm-2 control-label"><?=$language ?></label>
                        <div class="col-sm-10">
                            <?= $form->field($model->messages[$language], '[' . $language . ']translation')->textInput(['class'=>'form-control'])->label(false) ?>
                        </div>
                    </div>
            
        
                <?php endforeach; ?>
                <div class="form-group">
                    <div class="col-sm-4 col-sm-offset-1">
                        <?= Html::submitButton(Yii::t('admin', 'Save'), ['class' => 'btn btn-success', 'id' => 'translate_btn']) ?>
                    </div>
                </div>
                <?php ActiveForm::end()  ?>

            </div>
            <!-- /tile body -->

        </section>
    </div>
</div>
