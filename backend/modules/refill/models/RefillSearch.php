<?php

namespace backend\modules\refill\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cabinet\models\Refill;

/**
 * RefillSearch represents the model behind the search form about `frontend\modules\cabinet\models\Refill`.
 */
class RefillSearch extends Refill
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'user_id'], 'integer'],
            [['created_at', 'num', 'order_id'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'user_id' => $this->user_id,
            'sum' => $this->sum,
        ]);

        $query->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'num', $this->num])
            ->andFilterWhere(['like', 'order_id', $this->order_id])
            ->orderBy('id DESC');

        return $dataProvider;
    }

    public function getTypeList() 
    {
        return [
            self::TYPE_COMPANY => 'Поднятие компании в топ',
            self::TYPE_ALLOTTED =>  'Выделение желтым',
            self::TYPE_PACKAGE =>  'Оплата тарифа',
        ];
    }
    
    public function getDetailType()
    {
        return isset($this->typeList[$this->type]) ? $this->typeList[$this->type] : '';
    }

   

 
}
