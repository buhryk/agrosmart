<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 02.03.2017
 * Time: 14:44
 */

namespace backend\modules\notification\models;


use frontend\models\Company;
use frontend\models\CompanyUser;
use frontend\modules\notification\models\Notification;
use yii\base\Model;

class AdminNotificationForm extends Model
{
    public $type;
    public $message;
    public $target_id;

    public function rules()
    {
        return [
            [['type', 'message', 'target_id'], 'required'],
            [['target_id'], 'safe'],
        ];
    }

    public function dataSave()
    {

        $this->NotifyData();

        if (Notification::TYPE_USER_MESSAGE == $this->type) {
            if ($this->target_id) {
                foreach ($this->target_id as $target_id) {
                    $model = new Notification();
                    $model->type = $this->type;
                    $model->target_id = $target_id;
                    $model->message = $this->message;
                    $model->save();
                }
            }
        } elseif (Notification::TYPE_COMPANY_MESSAGE == $this->type) {
            if ($this->target_id) {
                foreach ($this->target_id as $target_id) {
                    $company = Company::findOne($target_id);
                    $userList = $company->admins;

                    foreach ($userList as $item) {
                        $model = new Notification();
                        $model->type = $this->type;
                        $model->target_id = $item->user_id;
                        $model->message = $this->message;
                        $model->save();
                    }
                }

            }
        }
    }

    public function NotifyData(){
        if($this->type == Notification::TYPE_ADMIN_MASSAGE){
            return '';
        }
        elseif($this->type == Notification::TYPE_COMPANY_MESSAGE){
            return CompanyUser::find()->where(['company_id'=>$this->target_id])->all();
        }
        elseif($this->type == Notification::TYPE_USER_MESSAGE){
            return $this->target_id;
        }
    }
    
}