<?php

namespace backend\modules\notification\controllers;

use backend\modules\notification\models\AdminNotificationForm;
use Faker\Provider\ar_JO\Company;
use frontend\models\User;
use frontend\modules\notification\models\Notification;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Default controller for the `notification` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new AdminNotificationForm();
            if($model->load(\Yii::$app->request->post())){
                $model->dataSave();
            }

        $data = Notification::find()->all();

        return $this->render('index',[
            'model' => $model,
            'data' => $data
        ]);
    }



    public function actionTargetData(){

        $type = \Yii::$app->request->post()['type'];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if($type == Notification::TYPE_ADMIN_MASSAGE ){
            return false;
        }
        elseif($type == Notification::TYPE_COMPANY_MESSAGE ){

            $companies = \frontend\models\Company::find()->all();

            return ArrayHelper::map($companies,'id','name');
        }
        elseif($type == Notification::TYPE_USER_MESSAGE ){

            $users = User::find()->all();

            return ArrayHelper::map($users,'id','email');

        }

        return false;

    }
}
