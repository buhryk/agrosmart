<?php
use kartik\form\ActiveForm;
use yii\helpers\Url;
use mihaildev\elfinder\InputFile;

$this->title = 'Уведомления пользователей';
$this->params['breadcrumbs'][] = $this->title;
$temp_var = [];
?>

<div class="notification-default-index">
    <h1><?= $this->title ?></h1>
    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'image-form',
            'id' => 'new-image-form',
        ],
    ]); ?>
    <tr>
        <?= $form->field($model,'type',['template' => ' <div class="col-sm-3">{input}{error}{hint}</div>'])->dropDownList(\frontend\modules\notification\models\Notification::getAllTypes(),[
            'prompt' => 'Выберите тип...',
        ])?>
        <?= $form->field($model,'target_id',['template' => ' <div class="col-sm-3">{input}{error}{hint}</div>'])->dropDownList($temp_var,[
            'prompt' => 'Выберите обект...','multiple' =>'multiple'
        ])?>
        <?= $form->field($model,'message',['template' => ' <div class="col-sm-6">{input}{error}{hint}</div>'])->textarea();?>

            <div class="form-group" style="text-align: right;">
                <?= \yii\bootstrap\Html::submitButton(Yii::t('common', 'Отправить'), [
                    'class' => 'btn btn-success submit-image-form',
                    'data-form-id' => 'new-image-form'
                ]); ?>
            </div>
    </tr>
    <?php ActiveForm::end(); ?>

</div>

<?php

$this->registerJS('
    $("#adminnotificationform-type")[0].addEventListener("change",function(){
         $.ajax({
         url:"/notification/default/target-data",
         type: \'post\',
         data: {
             type: $("#adminnotificationform-type")[0].value , 
             _csrf : "'.Yii::$app->request->getCsrfToken().'"
             },
       success: function (data) {
          document.getElementById(\'adminnotificationform-target_id\').options.length = 0;
          var select = document.getElementById("adminnotificationform-target_id");
          for(var item in data){
            var option = document.createElement(\'option\');
            option.text = data[item];
            option.value = item
            select.add(option, 0);
          }
       }
    });
    })
');

?>
