<?php

namespace backend\modules\page\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\page\models\PageLang;
use backend\modules\page\models\PageSearch;
use backend\modules\seo\models\SeoLang;
use Yii;
use yii\base\Model;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use backend\modules\page\models\Page;
use backend\modules\page\models\Category;
use yii\web\NotFoundHttpException;
use backend\modules\seo\models\Seo;
use backend\modules\transliterator\services\TransliteratorService;

class PageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex($id = null)
    {
        $searchModel = new PageSearch();

        $params = Yii::$app->request->queryParams;

        if ($id && !isset(Yii::$app->request->queryParams['PageSearch'])) {
            $params['PageSearch']['category_id'] = (int)$id;
        }

        $dataProvider = $searchModel->search($params);
        $category = $searchModel->category_id ? $this->findCategory($searchModel->category_id) : null;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category
        ]);
    }

    public function actionCreate()
    {
        $model = new Page();
        $modelLang = new PageLang();
        $modelLang->lang = Yii::$app->language;

        $categoryId = Yii::$app->request->get('category');
        $category = $categoryId ? $this->findCategory($categoryId) : null;
        $model->category_id = $categoryId;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->page_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->page_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang,
            'category' => $category
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findPage($id)
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findPage($id);

        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new PageLang();
            $modelLang->page_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->primaryKey]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionSeo($id)
    {
        $model = $this->findPage($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
            $seo->save();
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->seo_id = $seo->primaryKey;
            $seoLang->lang = Yii::$app->language;
            $seoLang->save();
        }

        if ($seoLang->load(Yii::$app->request->post()) && $seoLang->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    private function findPage($id)
    {
        $model = Page::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Page not found.');
        }

        return $model;
    }

    private function findCategory($id)
    {
        $category = Category::findOne($id);

        if (!$category) {
            throw new NotFoundHttpException('Category not found.');
        }

        return $category;
    }

    public function actionDelete($id)
    {
        $category = $this->findPage($id);
        $category->delete();

        return $this->redirect(['index']);
    }
}