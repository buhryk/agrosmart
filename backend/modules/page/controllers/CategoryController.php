<?php

namespace backend\modules\page\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\seo\models\Seo;
use backend\modules\seo\models\SeoLang;
use Yii;
use backend\modules\page\models\CategoryLang;
use yii\base\Model;
use yii\behaviors\SluggableBehavior;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use backend\modules\page\models\Page;
use backend\modules\page\models\Category;
use yii\web\NotFoundHttpException;
use backend\modules\transliterator\services\TransliteratorService;


class CategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Category::find()
                ->where(['parent_id' => null])
        ]);

        return $this->render('index', [
            'data' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new Category();
        $modelLang = new CategoryLang();
        $modelLang->lang = Yii::$app->language;

        $parentId = Yii::$app->request->get('parent');
        if ($parentId) {
            $parent = $this->findCategory($parentId);
            $model->parent_id = $parent->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->category_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->category_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => Category::findOne($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findCategory($id);

        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new CategoryLang();
            $modelLang->category_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                if ($modelLang->lang == 'ru') {
                    $model->alias = TransliteratorService::transliterate($modelLang->title);
                } else {
                    $modelLangForAlias = CategoryLang::find()
                        ->where(['category_id' => $model->primaryKey, 'lang' => 'ru'])
                        ->one();

                    if ($modelLangForAlias) {
                        $model->alias = TransliteratorService::transliterate($modelLangForAlias->title);
                    }
                }
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $category = $this->findCategory($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    public function actionSeo($id)
    {
        $model = $this->findCategory($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
            $seo->save();
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->seo_id = $seo->primaryKey;
            $seoLang->lang = Yii::$app->language;
            $seoLang->save();
        }

        if ($seoLang->load(Yii::$app->request->post()) && $seoLang->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    private function findCategory($id)
    {
        $model = Category::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Category not found.');
        }

        return $model;
    }
}