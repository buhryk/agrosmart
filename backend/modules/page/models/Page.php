<?php

namespace backend\modules\page\models;

use common\helpers\SitemapHelper;
use Yii;
use backend\modules\page\models\PageLang;
use backend\modules\transliterator\services\TransliteratorService;
use backend\modules\seo\models\Seo;

class Page extends \yii\db\ActiveRecord
{

    const ACTIVE_YES = 1;
    const ACTIVE_NO = 0;

    public static function tableName()
    {
        return 'page_model';
    }

    public function rules()
    {
        return [
            [['category_id', 'active'], 'integer'],
            [['alias'], 'string', 'max' => 128],
            [['alias'], 'unique'],
            ['alias', 'match', 'pattern' => '/[a-zA-Z0-9-_.]+$/',
                'message' => Yii::t('common', 'Field must contain only latin letters, digits, spaces and hyphens')],
            ['alias', 'default', 'value' => null],
            ['active', 'in', 'range' => [self::ACTIVE_YES, self::ACTIVE_NO]],
            ['active', 'default', 'value' => self::ACTIVE_YES]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => Yii::t('page', 'Category'),
            'alias' => 'Alias',
            'active' => Yii::t('page', 'Active'),
        ];
    }

    public function getLang()
    {
        return $this->hasOne(PageLang::className(), ['page_id' => 'id'])->where(['lang' => \Yii::$app->language]);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getShort_description()
    {
        return $this->lang ? $this->lang->short_description : '';
    }

    public function getText()
    {
        return $this->lang ? $this->lang->text : '';
    }

    public function getSeo()
    {
        return $this->hasOne(Seo::className(), ['record_id' => 'id'])->where(['table_name' => $this::tableName()]);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function beforeSave($insert)
    {
        $this->alias = TransliteratorService::transliterate($this->alias);

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterDelete()
    {
        Seo::deleteAll(['table_name' => $this::tableName(), 'record_id' => $this->primaryKey]);
        PageLang::deleteAll(['page_id' => $this->primaryKey]);
        parent::afterDelete(); // TODO: Change the autogenerated stub
    }

    public static function getAllActiveProperties()
    {
        return [
            self::ACTIVE_YES => Yii::t('common', 'Yes'),
            self::ACTIVE_NO => Yii::t('common', 'No')
        ];
    }

    public function getActiveDetail()
    {
        return isset(static::getAllActiveProperties()[$this->active]) ? static::getAllActiveProperties()[$this->active] : 'Undefined';
    }
}