<?php

namespace backend\modules\page\models;

use Yii;

/**
 * This is the model class for table "page_category_lang".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $lang
 * @property string $title
 */
class CategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_category_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'lang', 'title'], 'required'],
            [['category_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => Yii::t('page', 'Category'),
            'lang' => Yii::t('common', 'Lang'),
            'title' => Yii::t('page', 'Title'),
        ];
    }
}