<?php

namespace backend\modules\page\models;

use Yii;

/**
 * This is the model class for table "page_model_lang".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $lang
 * @property string $title
 * @property string $short_description
 * @property string $text
 */
class PageLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_model_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'lang', 'title', 'text'], 'required'],
            [['page_id'], 'integer'],
            [['short_description', 'text'], 'string'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => Yii::t('page', 'Page'),
            'lang' => Yii::t('common', 'Lang'),
            'title' => Yii::t('page', 'Title'),
            'short_description' => Yii::t('page', 'Short description'),
            'text' => Yii::t('page', 'Text'),
        ];
    }
}