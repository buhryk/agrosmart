<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;

$category = $model->category;
if ($category) {
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('page', 'Category pages') . ' "' .$category->title .'"',
        'url' => ['index', 'id' => $category->primaryKey]
    ];
} else {
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('page', 'Pages without category'),
        'url' => ['index']
    ];
}

if ($action == 'update') {
    $this->title = Yii::t('page', 'Page updating') . ' "' . $model->title . '"';
} elseif ($action == 'seo') {
    $this->title = Yii::t('page', 'Page SEO updating') . ' "' . $model->title . '"';
}
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<ul class="nav nav-tabs">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
            <?= Yii::t('common', 'Update'); ?>
        </a>
    </li>
    <li <?= ($action === 'seo') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['seo', 'id' => $model->primaryKey]) ?>">
            SEO
        </a>
    </li>
</ul>
<br>
