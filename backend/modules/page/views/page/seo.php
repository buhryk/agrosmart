<?php
use backend\widgets\SeoWidget;
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= SeoWidget::widget([
        'model' => $seo,
        'parameters' => ['modelLang' => $seoLang]
    ]); ?>
</div>