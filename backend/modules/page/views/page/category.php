<?php
use yii\helpers\Url;
use backend\modules\page\models\Page;
use yii\bootstrap\Html;

if ($category) {
    $this->title = Yii::t('page', 'Category pages') . ' "' . $category->title . '"';
} else {
    $this->title = Yii::t('page', 'Pages without category');
}
?>

<div class="rubric-default-index">
    <h1>
        <?= Html::encode($this->title) ?>
        <a href="<?= $category ? Url::to(['create', 'category' => $category->primaryKey]) :  Url::to(['create']);?>"
           class="btn btn-primary block right">
            <?= Yii::t('page', 'Add page'); ?>
        </a>
    </h1>

    <table class="table table-bordered" style="margin-top: 15px;">
        <thead>
        <tr>
            <th width="60">ID</th>
            <th><?= Yii::t('page', 'Title'); ?></th>
            <th>Alias</th>
            <th width="75"><?= Yii::t('page', 'Active'); ?></th>
            <th width="90"><?= Yii::t('common', 'Actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $model) { ?>
            <tr>
                <td><?= $model->primaryKey; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>">
                        <?= $model->title; ?>
                    </a>
                </td>
                <td><?= $model->alias; ?></td>
                <td><?= $model->active == Page::ACTIVE_YES ? Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?></td>
                <td>
                    <a href="<?= Url::to(['delete', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Delete'); ?>"
                       class="btn btn-default btn-sm block right"
                       onclick="return confirm('<?= Yii::t('common', 'Are you sure you want to delete this item?'); ?>')"
                    >
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                    <a href="<?= Url::to(['update', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Edit'); ?>"
                       class="btn btn-default btn-sm right block"
                    >
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>