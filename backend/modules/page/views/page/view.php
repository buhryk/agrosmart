<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\page\models\Page;

$this->title = $model->title;
$category = $model->category;
if ($category) {
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('page', 'Category pages'). ' "' . $category->title . '"',
        'url' => ['index', 'id' => $category->primaryKey]
    ];
} else {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('page', 'Pages without category'), 'url' => ['index']];
}

$this->params['breadcrumbs'][] = $model->title;
?>

<div class="rubric-view">

    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td>4</td>
        </tr>
        <tr>
            <th><?= Yii::t('page', 'Title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('page', 'Category'); ?></th>
            <td>
                <?php if ($category) { ?>
                    <a href="<?= Url::to(['category/view', 'id' => $category->primaryKey]); ?>">
                        <?= $category->title; ?>
                    </a>
                <?php } else { ?>
                    &nbsp;
                <?php }?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('active'); ?></th>
            <td><?= $model->activeDetail; ?></td>
        </tr>
        </tbody>
    </table>
</div>