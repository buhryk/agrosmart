<?php
use yii\helpers\Html;

$this->title = Yii::t('page', 'Page creating');

if ($category) {
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('page', 'Category pages') . ' "' . $category->title . '"',
        'url' => ['page/index', 'id' => $category->primaryKey]
    ];
} else {
    $this->params['breadcrumbs'][] = [
        'label' => Yii::t('page', 'Pages without category'),
        'url' => ['page/index']
    ];
}

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang,
        'category' => $category
    ]) ?>
</div>