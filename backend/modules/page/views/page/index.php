<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\page\models\Category;
use backend\modules\page\models\Page;

$categories = ArrayHelper::map(Category::find()->joinWith('lang')->all(), 'id', 'title');
$categories[0] = Yii::t('page', 'Pages without category');

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\page\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (is_numeric($searchModel->category_id)) {
    $this->title = $category ?  Yii::t('page', 'Category pages') . ' "' . $category->title . '"' : Yii::t('page', 'Pages without category');
} else {
    $this->title = Yii::t('page', 'Pages');
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">
    <div>
        <h1>
            <?= Html::encode($this->title) ?>
            <a href="<?= $category ? Url::to(['create', 'category' => $category->primaryKey]) :  Url::to(['create']);?>"
               class="btn btn-primary block right">
                <?= Yii::t('page', 'Add page'); ?>
            </a>
        </h1>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'title',
                'label' => Yii::t('page', 'Title'),
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['view', 'id' => $model->primaryKey]);
                },
            ],
            [
                'attribute' => 'category_id',
                'filter' => $categories,
                'format'=>'raw',
                'value' => function ($model) {
                    $category = $model->category;
                    return $category ? Html::a($category->title, ['category/view', 'id' => $model->category->primaryKey]) : '';
                }
            ],
            'alias',
            [
                'attribute' => 'active',
                'filter' => Page::getAllActiveProperties(),
                'value' => 'activeDetail'
            ],
            [
                'contentOptions'=>['style'=>'width: 135px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>
</div>