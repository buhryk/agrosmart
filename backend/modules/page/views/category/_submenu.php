<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;
if ($action == 'update') {
    $this->title = Yii::t('page', 'Category updating') . ' "' . $model->title . '"';
} elseif ($action == 'seo') {
    $this->title = Yii::t('page', 'Category SEO updating') . ' "' . $model->title . '"';
}
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<ul class="nav nav-tabs">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
            <?= Yii::t('common', 'Editing'); ?>
        </a>
    </li>
    <li <?= ($action === 'seo') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['seo', 'id' => $model->primaryKey]) ?>">
            SEO
        </a>
    </li>
</ul>
<br>
