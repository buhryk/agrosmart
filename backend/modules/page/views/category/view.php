<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\page\models\Page;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('page', 'Categories list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubric-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('page', 'Add page'), ['page/create', 'category' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>

        <a href="<?= Url::to(['create', 'parent' => $model->primaryKey]); ?>" class="btn btn-primary" style="float: right">
            <?= Yii::t('page', 'Add subcategory'); ?>
        </a>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('page', 'Title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('page', 'Parent category'); ?></th>
            <td>
                <?php $parent = $model->parent; ?>
                <?php if ($parent) { ?>
                    <a href="<?= Url::to(['view', 'id' => $parent->primaryKey]) ?>"><?= $parent->title; ?></a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('page', 'Subcategories'); ?></th>
            <td>
                <?php $subcategories = $model->subcategories; ?>
                <?php foreach ($subcategories as $subcategory) { ?>
                    <a href="<?= Url::to(['view', 'id' => $subcategory->primaryKey]); ?>" class="block">
                        <?= $subcategory->title; ?>
                    </a>
                <?php } ?>

                <div style="margin-top: 10px">
                    <a href="<?= Url::to(['create', 'parent' => $model->primaryKey]); ?>"
                       class="btn-sm btn-success without-hover-text-decoration"
                    >
                        <?= Yii::t('news', 'Add subcategory'); ?>
                    </a>
                </div>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('page', 'Pages'); ?></th>
            <td>
                <a href="<?= Url::to(['page/index', 'id' => $model->primaryKey]); ?>">
                    <?= Yii::t('page', 'Pages'); ?>
                    (<?= Page::find()->where(['category_id' => $model->primaryKey])->count(); ?>)
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>