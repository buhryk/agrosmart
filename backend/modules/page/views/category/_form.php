<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\page\models\Category;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'title') ?>
    <?= $form->field($model, 'alias') ?>
    <?= $form->field($model, 'active')->dropDownList(array(
        Category::ACTIVE_YES => Yii::t('common', 'Yes'),
        Category::ACTIVE_NO => Yii::t('common', 'No')
    )); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>