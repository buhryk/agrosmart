<?php
$this->params['breadcrumbs'][] = ['label' => Yii::t('page', 'Categories list'), 'url' => ['index']];
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model,
        'modelLang' => $modelLang
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>