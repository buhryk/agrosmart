<?php
use backend\widgets\SeoWidget;

$this->params['breadcrumbs'][] = ['label' => Yii::t('page', 'Categories list'), 'url' => ['index']];
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= SeoWidget::widget([
        'model' => $seo,
        'parameters' => ['modelLang' => $seoLang]
    ]); ?>
</div>