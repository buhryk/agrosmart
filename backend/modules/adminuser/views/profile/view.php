<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->username;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('users', 'Change password'), ['change-password', 'id' => $model->id], [
            'class' => 'btn btn-primary'
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('users', 'Username'); ?></th>
            <td><?= $model->username; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('users', 'EMAIL'); ?></th>
            <td><?= $model->email; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('users', 'PHONE'); ?></th>
            <td><?= $model->phone; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('users', 'NAME'); ?></th>
            <td><?= $model->name; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('users', 'SURNAME'); ?></th>
            <td><?= $model->surname; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('users', 'Role'); ?></th>
            <td>
                <?php $role = $model->role;
                echo $role ? $role->title : ''; ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>