<?php
use yii\helpers\Html;

$this->title = Yii::t('users', 'Updating user');
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>