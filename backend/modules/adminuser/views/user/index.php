<?php
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\accesscontrol\models\Role;
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;

$this->title = Yii::t('users', 'Users list');
?>

<div class="rubric-default-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-primary block right">
            <?= Yii::t('users', 'Add user'); ?>
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            'email',
            'username',
            'name',
            'surname',
            'phone',
            [
                'attribute' => 'role_id',
                'filter' => ArrayHelper::map(Role::find()->all(), 'id', 'title'),
                'format'=>'raw',
                'value' => function ($model) {
                    $role = $model->role;
                    return $role ? $role->title : '';
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'contentOptions' => ['style'=>'width: 120px;'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'status',
                'value' => 'statusDetail',
                'filter' => \common\models\User::getAllStatuses()
            ],
            [
                'contentOptions'=>['style'=>'width: 120px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>
</div>