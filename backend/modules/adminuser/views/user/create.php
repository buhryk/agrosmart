<?php
use yii\helpers\Html;

$this->title = Yii::t('users', 'Creating user');
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'Users list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>
</div>