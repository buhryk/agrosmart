<?php
use backend\widgets\SeoWidget;
use yii\helpers\Html;

$this->title = 'SEO: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Инструменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'SEO';
?>
<h1><?= Html::encode($this->title) ?></h1>
<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= SeoWidget::widget([
        'model' => $seo,
        'parameters' => ['modelLang' => $seoLang]
    ]); ?>
</div>