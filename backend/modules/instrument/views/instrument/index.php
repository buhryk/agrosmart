<?php

use yii\helpers\Html;
use yii\grid\GridView;
use vova07\imperavi\Widget as ImperaviWidget;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Инструменты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrument-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить иеструмент', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->statusList[$model->status];
                },
                'filter' => $searchModel->statusList,
            ],
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->typeList[$model->type];
                },
                'filter' => $searchModel->typeList,
            ],
            [
                'attribute' => 'image',
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
