<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\instrument\models\Instrument */

$this->title = 'Создание инструмента';
$this->params['breadcrumbs'][] = ['label' => 'Инструменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrument-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>
