<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\ArrayHelper;
use backend\modules\instrument\models\InstrumentAccessibilityType;
use backend\modules\instrument\models\Instrument;


$typeList = ArrayHelper::map(InstrumentAccessibilityType::find()->all(),'id' , 'name');
$instrumentList = Instrument::find()->where('parent_id IS NULL')->andFilterWhere(['!=', 'id', $model->id])->all();

?>

<div class="instrument-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'position')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'type')->dropDownList($model->typeList) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map($instrumentList, 'id', 'title'),
                ['prompt' => ' -- Выберите --']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'image')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => 'elfinder',
                'filter'        => 'image',
                'path'          => 'instrument',
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false
            ]) ?>
        </div>
        <div class="col-md-6">
            <?php if ($model->image): ?>
                <img height="100px" src="<?=$model->image ?>">
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'image_cabinet')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => 'elfinder',
                'filter'        => 'image',
                'path'          => 'instrument',
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false
            ]) ?>
        </div>
        <div class="col-md-6">
            <?php if ($model->image_cabinet): ?>
                <img height="100px" src="<?=$model->image_cabinet ?>">
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
           <?=$form->field($modelLang, 'title')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($modelLang, 'description')->widget(ImperaviWidget::className(), [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 100,
                    'plugins' => [
                        'clips',
                        'fullscreen'
                    ],
                    'imageUpload' => Url::to(['/site/image-upload']),
                    'convertDivs' => false,
                    'replaceDivs' => false
                ]
            ]); ?>
        </div>
     </div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($modelLang, 'text')->widget(ImperaviWidget::className(), [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 100,
                    'plugins' => [
                        'clips',
                        'fullscreen'
                    ],
                    'imageUpload' => Url::to(['/site/image-upload']),
                    'convertDivs' => false,
                    'replaceDivs' => false
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="input02" class="col-sm-2 control-label">Доступно для</label>
                <div class="col-sm-12 instrument-access">
                    <?= Html::checkboxList('accessibility_id', $model->checkedAccessibility, $typeList);
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <hr>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="input02" class="col-sm-2 control-label">Видимо для</label>
                <div class="col-sm-12 instrument-access">
                    <?= Html::checkboxList('visibility_id', $model->checkedVisibility, $typeList); ?>
                </div>
            </div>
        </div>
    </div>    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
