<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\instrument\models\Instrument */

$this->title = 'Редактирование: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Инструменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="instrument-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="rubric-create">
        <?= $this->render('_submenu', [
            'model' => $model
        ]); ?>
    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>
