<?php

namespace backend\modules\instrument\models;

use Yii;

/**
 * This is the model class for table "instrument_accessibility".
 *
 * @property integer $instrument_id
 * @property integer $type_id
 */
class InstrumentAccessibility extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'instrument_accessibility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instrument_id', 'type_id'], 'required'],
            [['instrument_id', 'type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'instrument_id' => 'Instrument ID',
            'type_id' => 'Type ID',
        ];
    }
}
