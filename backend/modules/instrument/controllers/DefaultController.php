<?php

namespace backend\modules\instrument\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\instrument\models\Instrument;
use yii\web\Controller;

/**
 * Default controller for the `instrument` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }
    
    
    public function actionIndex()
    {
       
    }
}
