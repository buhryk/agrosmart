<?php

namespace backend\modules\instrument\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\instrument\models\InstrumentLang;
use backend\modules\seo\models\SeoLang;
use backend\modules\seo\Seo;
use Yii;
use backend\modules\instrument\models\Instrument;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\instrument\models\InstrumentSearch;

/**
 * InstrumentController implements the CRUD actions for Instrument model.
 */
class InstrumentController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    /**
     * Lists all Instrument models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InstrumentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Instrument model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Instrument model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Instrument();
        $modelLang = new InstrumentLang();


        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post()) && $model->save()) {

            $model->saveAccessibility(Yii::$app->request->post('accessibility_id'));
            $model->saveVisibility(Yii::$app->request->post('visibility_id'));

            $modelLang->instrument_id = $model->primaryKey;
            $modelLang->save(false);

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    /**
     * Updates an existing Instrument model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new InstrumentLang();
            $modelLang->instrument_id = $model->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post()) && $model->save() && $modelLang->save()) {

            $model->saveAccessibility(Yii::$app->request->post('accessibility_id'));
            $model->saveVisibility(Yii::$app->request->post('visibility_id'));

            Yii::$app->session->setFlash('success', 'Успешно сохранено');
            return $this->redirect(['index']);
        } else {

            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    public function actionSeo($id)
    {
        $model = $this->findModel($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new \backend\modules\seo\models\Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
            $seo->save();
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->seo_id = $seo->primaryKey;
            $seoLang->lang = Yii::$app->language;
            $seoLang->save();
        }

        if ($seoLang->load(Yii::$app->request->post()) && $seoLang->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    /**
     * Deletes an existing Instrument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Instrument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Instrument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Instrument::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
