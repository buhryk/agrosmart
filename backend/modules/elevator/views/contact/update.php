<?php

use yii\helpers\Html;

$elevator = $model->elevator;
$this->title = Yii::t('elevator', 'Updating elevator contact');
$this->params['breadcrumbs'][] = ['label' => Yii::t('elevator', 'Elevators list'), 'url' => ['elevator/index']];
if ($elevator) {
    $this->params['breadcrumbs'][] = ['label' => $elevator->title, 'url' => ['elevator/view', 'id' => $elevator->primaryKey]];
}
$this->params['breadcrumbs'][] = ['label' => $model->fio, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>