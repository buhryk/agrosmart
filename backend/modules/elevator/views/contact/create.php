<?php

use yii\helpers\Html;

$this->title = Yii::t('elevator', 'Adding elevator contact');
$this->params['breadcrumbs'][] = ['label' => Yii::t('elevator', 'Elevators list'), 'url' => ['elevator/index']];
$this->params['breadcrumbs'][] = ['label' => $elevator->title, 'url' => ['elevator/view', 'id' => $elevator->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>