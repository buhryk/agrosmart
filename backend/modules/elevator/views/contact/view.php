<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\elevator\models\Elevator;

$elevator = $model->elevator;
$this->title = $model->fio;
$this->params['breadcrumbs'][] = ['label' => Yii::t('elevator', 'Elevators list'), 'url' => '/elevator/elevator/index'];
if ($elevator) {
    $this->params['breadcrumbs'][] = ['label' => $elevator->title, 'url' => ['elevator/view', 'id' => $elevator->primaryKey]];
}

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('elevator_id'); ?></th>
            <td>
                <?php if ($elevator) { ?>
                    <a href="<?= Url::to(['elevator/view', 'id' => $elevator->primaryKey]); ?>">
                        <?= $elevator->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('fio'); ?></th>
            <td><?= $model->fio; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('phones'); ?></th>
            <td><?= $model->phones; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('email'); ?></th>
            <td><?= $model->email; ?></td>
        </tr>
        </tbody>
    </table>
</div>