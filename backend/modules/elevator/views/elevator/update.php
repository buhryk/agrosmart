<?php
$this->title = Yii::t('elevator', 'Elevator updating');
$this->params['breadcrumbs'][] = ['label' => Yii::t('elevator', 'Elevators list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang,
        'modelAccessibility' => $modelAccessibility
    ]) ?>
</div>