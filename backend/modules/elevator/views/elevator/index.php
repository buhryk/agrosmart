<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\elevator\models\Elevator;
use backend\modules\elevator\models\ElevatorAccessibility;
use backend\assets\ElevatorAsset;
use kartik\grid\GridView;

ElevatorAsset::register($this);

$this->title = Yii::t('elevator', 'Elevators list');
?>
<div class="elevator-index">

    <h1>
        <?= Html::encode($this->title) ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-primary block right">
            <?= Yii::t('elevator', 'Add elevator'); ?>
        </a>
    </h1>

    <div style="clear: both;"></div>

    <div id="elevators_map" style="width: 100%; height: 600px; margin-bottom: 10px;"></div>

    <?php
    $this->registerJs("
        var locations = ".$locations.";
        ",
        \yii\web\View::POS_HEAD,
        'my-button-handler'
    ); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'title',
                'label' => Yii::t('elevator', 'Title'),
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['elevator/view', 'id' => $model->primaryKey]);
                },
            ],
            'address',
            [
                'attribute' => 'city',
                'label' => 'Город',
                'value' => 'location',
                'contentOptions'=>['style'=>'width: 210px;']
            ],
            [
                'attribute' => 'property_type',
                'filter' => Elevator::getPropertyTypes(),
                'format'=>'raw',
                'value' => 'propertyTypeTitle'
            ],
            [
                'attribute' => 'accessibilities',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(ElevatorAccessibility::getTypes(), 'id','title'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                    'options' => ['multiple' => true],
                ],
                'value' => function ($model) {
                    return implode(', ', ArrayHelper::getColumn($model->accessibilities, 'title'));
                }
            ],
            'volume',
            'loading',
            'unloading',
            [
                'attribute' => 'active',
                'filter' => Elevator::getAllActiveProperties(),
                'value' => 'activeDetail'
            ],
            [
                'contentOptions'=>['style'=>'width: 135px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>

</div>