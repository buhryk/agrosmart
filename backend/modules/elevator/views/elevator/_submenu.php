<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;
?>

<h1><?= Html::encode($this->title) ?></h1>

<ul class="nav nav-tabs">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
            <?= Yii::t('common', 'Editing'); ?>
        </a>
    </li>
    <li <?= ($action === 'address') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['address', 'id' => $model->primaryKey]) ?>">
            <?= Yii::t('common', 'Address'); ?>
        </a>
    </li>
</ul>
<br>
