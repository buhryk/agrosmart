<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use backend\assets\GoogleMapsAsset;
use backend\modules\elevator\models\Elevator;
GoogleMapsAsset::register($this);

$this->title = Yii::t('elevator', 'Elevator address updating');
$this->params['breadcrumbs'][] = ['label' => Yii::t('elevator', 'Elevators list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;

//$this->registerJs("var location = ".$model->location.";", \yii\web\View::POS_READY);
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model,
    ]); ?>

    <div class="rubric-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($modelLang, 'address')->textInput([
            'class' => 'form-control map_canvas',
            'data-error-message-no-results' => Yii::t('common', 'No results found'),
            'data-error-message-by-reason' => Yii::t('common', 'Geocode was not successful for the following reason'),
            'data-location' => $model->address
        ]); ?>

        <?= $form->field($model, 'coordinates')->textInput([
            'class' => 'form-control',
            'id' => 'coordinates',
            'data-error-message-no-results' => Yii::t('common', 'No results found'),
            'data-error-message-by-reason' => Yii::t('common', 'Geocode was not successful for the following reason'),
        ]); ?>

        <?php if ($model->address) { ?>
            <div style="width: 100%; height: 400px;" id="map_canvas_container"></div>
            <p>&nbsp;</p>

            <?= $form->field($model, 'address_confirmed')->dropDownList([
                Elevator::ADDRESS_CONFIRMED_NO => Yii::t('common', 'No'),
                Elevator::ADDRESS_CONFIRMED_YES => Yii::t('common', 'Yes')
            ]); ?>
        <?php } ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>