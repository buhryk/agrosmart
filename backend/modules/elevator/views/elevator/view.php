<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\elevator\models\Elevator;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('elevator', 'Elevators list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="rubric-view">

    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('elevator', 'Title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th>Город</th>
            <td>
                <?php $city = $model->city;
                echo $city ? $city->title : '';
                ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('address'); ?></th>
            <td><?= $model->address; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('elevator', 'Phones'); ?></th>
            <td><?= $model->phones; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('email'); ?></th>
            <td><?= $model->email; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('site'); ?></th>
            <td><?= $model->site; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'contacts'); ?></th>
            <td>
                <?php $contacts = $model->contacts; ?>
                <?php if ($contacts) { ?>
                    <div style="margin-bottom: 10px;">
                        <?php foreach ($contacts as $contact) { ?>
                            <div>
                                <a href="<?= Url::to(['contact/view', 'id' => $contact->primaryKey]); ?>">
                                    <?= $contact->fio . (', ' . $contact->phones) . ($contact->email ? (', ' . $contact->email) : ''); ?>
                                </a>
                                <a href="<?= Url::to(['contact/delete', 'id' => $contact->primaryKey]); ?>">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true" style="color: red"></span>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

                <a href="<?= Url::to(['contact/create', 'elevator' => $model->primaryKey]); ?>"
                   class="btn-sm btn-success without-hover-text-decoration"
                >
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;<?= Yii::t('elevator', 'Add contact'); ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('coordinates'); ?></th>
            <td><?= $model->coordinates; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('elevator', 'Accessibility'); ?></th>
            <td>
                <?php $accessibilities = $model->accessibilities; ?>
                <?php if ($accessibilities) { ?>
                    <ul>
                        <?php foreach ($accessibilities as $accessibility) { ?>
                            <li><?= $accessibility->title; ?></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('elevator', 'Property type'); ?></th>
            <td><?= $model->propertyTypeTitle; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('volume'); ?></th>
            <td><?= $model->volume; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('loading'); ?></th>
            <td><?= $model->loading; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('unloading'); ?></th>
            <td><?= $model->unloading; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('laboratory'); ?></th>
            <td><?= $model->laboratoryTitle; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('additional_info'); ?></th>
            <td><?= $model->additionalInfo; ?></td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('active'); ?></th>
            <td><?= $model->activeDetail; ?></td>
        </tr>
        </tbody>
    </table>
</div>