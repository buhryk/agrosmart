<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\elevator\models\Elevator;
use yii\helpers\ArrayHelper;
use backend\modules\elevator\models\ElevatorAccessibility;
use kartik\select2\Select2;
use yii\web\JsExpression;
$searchCityUrl = \yii\helpers\Url::to(['/city-list/index']);
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'title') ?>

    <div class="row">
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'city_id')->widget(Select2::className(), [
                'initValueText' => !empty($model->city) ? $model->city->title : '', // set the initial display text
                'options' => ['placeholder' => 'Ведите город'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    'ajax' => [
                        'url' => $searchCityUrl,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],

            ]); ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($modelLang, 'address') ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'coordinates') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'phones') ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'email') ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'site') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'volume')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'loading')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'unloading')->textInput(['type' => 'number'])  ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-4">
            <?php echo $form->field($modelAccessibility, 'accessibility_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(ElevatorAccessibility::getTypes(), 'id', 'title'),
                'language' => 'ru',
                'pluginOptions' => ['allowClear' => true, 'multiple' => true]
            ]); ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'property_type')->dropDownList(Elevator::getPropertyTypes(), ['prompt' => '']); ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'laboratory')->dropDownList(Elevator::getLaboratoriesList(), ['prompt' => '']); ?>
        </div>
    </div>

    <?= $form->field($modelLang, 'additional_info')->textarea(['rows' => 4, 'style' => 'resize: none;']); ?>
    <?= $form->field($model, 'alias') ?>
    <?= $form->field($model, 'active')->dropDownList(Elevator::getAllActiveProperties()); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>