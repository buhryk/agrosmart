<?php

use yii\helpers\Html;

$this->title = Yii::t('elevator', 'Creating elevator');
$this->params['breadcrumbs'][] = ['label' => Yii::t('elevator', 'Elevators list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang,
        'modelAccessibility' => $modelAccessibility
    ]) ?>
</div>