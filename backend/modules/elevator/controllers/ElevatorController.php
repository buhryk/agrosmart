<?php
namespace backend\modules\elevator\controllers;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use backend\modules\transliterator\services\TransliteratorService;
use backend\modules\elevator\models\Elevator;
use backend\modules\elevator\models\ElevatorLang;
use backend\modules\elevator\models\ElevatorAccessibility;
use backend\modules\elevator\models\ElevatorSearch;
use backend\modules\accesscontrol\AccessControlFilter;

class ElevatorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ElevatorSearch();
        $query = $searchModel->getSearchQuery(Yii::$app->request->queryParams);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $queryElevatorsWithCoordinates = clone $query;

        $dataProviderElevatorsWithCoordinates = new ActiveDataProvider([
            'query' => $queryElevatorsWithCoordinates
                ->andFilterWhere(['=', 'address_confirmed', Elevator::ADDRESS_CONFIRMED_YES])
                ->andFilterWhere(['<>', 'coordinates', null])
                ->andFilterWhere(['<>', 'coordinates', ''])
        ]);

        $models = $dataProviderElevatorsWithCoordinates->models;

        $locations = $this->buildMarkersArray($models,
            isset($queryParams['ElevatorSearch']) ? $queryParams['ElevatorSearch'] : []);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'elevatorsWithMaps' => $dataProviderElevatorsWithCoordinates->models,
            'locations' => $locations
        ]);
    }

    public function actionCreate()
    {
        $model = new Elevator();
        $modelLang = new ElevatorLang();
        $modelLang->lang = Yii::$app->language;
        $modelAccessibility = new ElevatorAccessibility();

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->elevator_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->elevator_id = $model->primaryKey;
                $modelLang->save();

                if ($modelAccessibility->load(Yii::$app->request->post())) {
                    $modelAccessibility->elevator_id = 0;
                    if ($modelAccessibility->validate()) {
                        $accessibilitiesIds = $modelAccessibility->accessibility_id;

                        if ($accessibilitiesIds) {
                            foreach ($accessibilitiesIds as $accessibilityId) {
                                $accessibility = new ElevatorAccessibility();
                                $accessibility->elevator_id = $model->primaryKey;
                                $accessibility->accessibility_id = $accessibilityId;
                                $accessibility->save();
                            }
                        }
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang,
            'modelAccessibility' => $modelAccessibility
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findElevator($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findElevator($id);
        $modelLang = $model->lang;
        $modelAccessibility = new ElevatorAccessibility();
        $modelAccessibility->elevator_id = $model->primaryKey;

        $accessibilities = $model->accessibilities;
        if ($accessibilities) {
            $modelAccessibility->accessibility_id = ArrayHelper::getColumn($accessibilities, 'accessibility_id');
        }

        if (!$modelLang) {
            $modelLang = new ElevatorLang();
            $modelLang->elevator_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {

            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                if ($modelAccessibility->load(Yii::$app->request->post()) && $modelAccessibility->validate()) {
                    $accessibilitiesIds = $modelAccessibility->accessibility_id;

                    if ($accessibilitiesIds) {
                        foreach ($accessibilitiesIds as $accessibilityId) {
                            if (!in_array($accessibilityId, ArrayHelper::getColumn($accessibilities, 'accessibility_id'))) {
                                $accessibility = new ElevatorAccessibility();
                                $accessibility->elevator_id = $model->primaryKey;
                                $accessibility->accessibility_id = $accessibilityId;
                                $accessibility->save();
                            }

                            foreach ($accessibilities as $accessibility) {
                                if (!in_array($accessibility->accessibility_id, $accessibilitiesIds)) {
                                    $accessibility->delete();
                                }
                            }
                        }
                    } else {
                        ElevatorAccessibility::deleteAll(['elevator_id' => $model->primaryKey]);
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang,
            'modelAccessibility' => $modelAccessibility
        ]);
    }

    public function actionDelete($id)
    {
        $category = $this->findElevator($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    private function findElevator($id)
    {
        $model = Elevator::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Elevator not found');
        }

        return $model;
    }

    public function actionAddress($id)
    {
        $model = $this->findElevator($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new ElevatorLang();
            $modelLang->elevator_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $modelLang])) {

            $model->save();
            $modelLang->save();

            return $this->refresh();
        }

        return $this->render('address', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }
    private function buildMarkersArray($models, $params)
    {

        $loadingTranslates = Yii::t('elevator', 'Elevator loading');
        $unloadingTranslates = Yii::t('elevator', 'Elevator unloading');
        $detailTranslates = Yii::t('common', 'Detail');
        $locations = "[";

        foreach ($models as $key => $elevator) {
            $coordinates = explode(',',$elevator->coordinates);
            $content = "<div class=\"marker\">";
            $content .= "<div class=\"marker-title\">".str_replace("'","\\'",$elevator->title)."</div>";
            $content .= "<div>".str_replace("'","\\'",$elevator->address)."</div>";

            if ($elevator->loading) {
                $content .= "<div>$loadingTranslates: $elevator->loading т</div>";
            }
            if ($elevator->unloading) {
                $content .= "<div>$unloadingTranslates: $elevator->unloading т</div>";
            }

            $content .= "<div><a href=\"".Url::to(['view', 'id' => $elevator->primaryKey])."\">$detailTranslates</a></div>";
            $content .= "</div>";
            $locations .= "['".$content."',".$coordinates[0].",".$coordinates[1]."],";
        }

        $locations = trim($locations, ',') . "]";

        return $locations;
    }

}