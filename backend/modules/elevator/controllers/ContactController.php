<?php

namespace backend\modules\elevator\controllers;

use backend\modules\elevator\models\ElevatorContactLang;
use Yii;
use backend\modules\elevator\models\ElevatorContact;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\elevator\models\Elevator;
use backend\modules\accesscontrol\AccessControlFilter;

class ContactController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ElevatorContact::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($elevator)
    {
        $elevator = $this->findElevator($elevator);
        $model = new ElevatorContact();
        $model->elevator_id = $elevator->primaryKey;
        $modelLang = new ElevatorContactLang();
        $modelLang->lang = Yii::$app->language;
        $modelLang->contact_id = 0;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $modelLang])) {
            if ($model->save()) {
                $modelLang->contact_id = $model->primaryKey;
                $modelLang->save();



                return $this->redirect(['elevator/view', 'id' => $elevator->primaryKey]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang,
            'elevator' => $elevator
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new ElevatorContactLang();
            $modelLang->lang = Yii::$app->language;
            $modelLang->contact_id = $model->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $elevator = $model->elevator;

        $model->delete();

        return $this->redirect(['elevator/view', 'id' => $elevator->primaryKey]);
    }

    protected function findElevator($id)
    {
        if (($model = Elevator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModel($id)
    {
        if (($model = ElevatorContact::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}