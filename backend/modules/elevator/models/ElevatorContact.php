<?php

namespace backend\modules\elevator\models;

use Yii;

/**
 * This is the model class for table "elevator_contact".
 *
 * @property integer $id
 * @property integer $elevator_id
 * @property string $fio
 * @property string $phones
 * @property string $email
 */
class ElevatorContact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'elevator_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elevator_id'], 'required'],
            [['elevator_id'], 'integer'],
            [['phones'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 128],
            ['phones', 'validatePhones'],
        ];
    }

    public function validatePhones($attribute, $params)
    {
        if ($this->phones) {

            $phones = explode(',', $this->phones);
            foreach ($phones as $key => $phone) {
                $phones[$key] = trim($phone);
            }

            foreach ($phones as $key => $phone) {
                if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/", $phone)) {
                    $this->addError('phones', Yii::t('advertisement', 'Phone numbers must be written in the format', ['format' => '0XX-XXX-XX-XX']));
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elevator_id' => Yii::t('elevator', 'Elevator'),
            'fio' => Yii::t('elevator', 'Elevator contact position, fio'),
            'phones' => Yii::t('common', 'Phones'),
            'email' => 'Email',
        ];
    }

    public function getElevator()
    {
        return $this->hasOne(Elevator::className(), ['id' => 'elevator_id'])->joinWith('lang');
    }

    public function getLang()
    {
        return $this->hasOne(ElevatorContactLang::className(), ['contact_id' => 'id'])
            ->where(['lang' => \Yii::$app->language]);
    }

    public function getFio()
    {
        return $this->lang ? $this->lang->fio : '';
    }
}