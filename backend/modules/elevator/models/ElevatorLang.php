<?php

namespace backend\modules\elevator\models;

use Yii;

/**
 * This is the model class for table "elevator_model_lang".
 *
 * @property integer $id
 * @property integer $elevator_id
 * @property string $lang
 * @property string $title
 */
class ElevatorLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'elevator_model_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elevator_id', 'lang', 'title', 'address'], 'required'],
            [['elevator_id'], 'integer'],
            [['additional_info'], 'string'],
            [['lang'], 'string', 'max' => 5],
            [['title', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elevator_id' => Yii::t('elevator', 'Elevator'),
            'lang' => Yii::t('common', 'Lang'),
            'title' => Yii::t('elevator', 'Title'),
            'additional_info' => Yii::t('elevator', 'Additional info'),
            'address' => Yii::t('elevator', 'Address'),
        ];
    }
}