<?php

namespace backend\modules\elevator\models;

use Yii;

/**
 * This is the model class for table "elevator_model_lang".
 *
 * @property integer $id
 * @property integer $elevator_id
 * @property string $lang
 * @property string $title
 */
class ElevatorContactLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'elevator_contact_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'lang'], 'required'],
            [['contact_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['fio'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elevator_id' => Yii::t('elevator', 'Elevator'),
            'lang' => Yii::t('common', 'Lang'),
            'fio' => Yii::t('elevator', 'Elevator contact position, fio')
        ];
    }
}