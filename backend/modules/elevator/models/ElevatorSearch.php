<?php
namespace backend\modules\elevator\models;

use backend\modules\commondata\models\City;
use backend\modules\commondata\models\CityLang;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

class ElevatorSearch extends Elevator
{
    public $title;
    public $accessibilities;
    public $city;
    public $address;

    public function rules()
    {
        return [
            [['id', 'property_type', 'active', 'address_confirmed'], 'integer'],
            [['address', 'coordinates', 'alias', 'volume', 'loading', 'unloading', 'title','accessibilities', 'city'], 'safe'],
            [['volume', 'loading', 'unloading'], 'digitValidator'],
            [['accessibilities'], 'accessibilityValidator']
        ];
    }

    public function digitValidator($attribute, $params)
    {
        $value = str_replace(['>=','>','<','<=','=','-'],'',$this->$attribute);

        if (!is_numeric($value)) {
            $this->addError($attribute, 'Field must be numeric or numeric with [>,>=,<,<=,=,-]');
        }
    }

    public function accessibilityValidator($attribute, $params)
    {
        $validate = true;

        if (!is_numeric($this->$attribute) && !is_array($this->$attribute)) {
            $this->addError($attribute, 'This field must be numeric or array of digits');
        } else {
            if (is_numeric($this->$attribute)) {
                $this->$attribute = array($this->$attribute);
            }

            foreach ($this->$attribute as $value) {
                if ($validate && !is_numeric($value)) {
                    $this->addError($attribute, 'This field must be numeric or array of digits');
                    break;
                } elseif ($validate) {
                    $validateValues = $attribute == 'region_id' ? ArrayHelper::getColumn(Region::find()->all(), 'id') :
                        ArrayHelper::getColumn(ElevatorAccessibility::getTypes(), 'id');
                    if (!in_array($value, $validateValues)) {
                        $this->addError($attribute, 'Wrong value for this field.');
                        break;
                    }
                }
            }
        }
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->getSearchQuery($params),
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public function getSearchQuery($params)
    {
        $query = Elevator::find();

        $query->orderBy('id DESC');

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $query;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'property_type' => $this->property_type,
            'active' => $this->active,
            'address_confirmed' => $this->address_confirmed,
        ]);

        $query->andFilterWhere(['like', 'alias', $this->alias]);

        if ($this->address) {
            $query->leftJoin(ElevatorLang::tableName(), ElevatorLang::tableName().'.elevator_id='.Elevator::tableName().'.id')
                ->where(['like', ElevatorLang::tableName().'.address', $this->address]);
        }

        if ($this->volume) {
            $this->addFilterByDigit($query, 'volume', $this->volume);
        }
        if ($this->loading) {
            $this->addFilterByDigit($query, 'loading', $this->loading);
        }
        if ($this->unloading) {
            $this->addFilterByDigit($query, 'unloading', $this->unloading);
        }

        if ($this->title) {
            $langs = ElevatorLang::find()
                ->where(['like', 'title', '%'.$this->title.'%', false])
                ->groupBy('elevator_id')
                ->all();

            $query->andFilterWhere(['in', 'id', $langs ? ArrayHelper::getColumn($langs, 'elevator_id') : [0]]);
        }

        if ($this->city) {
            $query->innerJoin(City::tableName(), Elevator::tableName().'.city_id='.City::tableName().'.id')
                ->innerJoin(CityLang::tableName(), City::tableName().'.id='.CityLang::tableName().'.city_id')
                ->andWhere(['like', CityLang::tableName().'.title', '%'.$this->city.'%', false]);
        }

        if ($this->accessibilities) {
            $elevatorsAccessibilities = ElevatorAccessibility::find()
                ->where(['in', 'accessibility_id', $this->accessibilities])
                ->groupBy('elevator_id')
                ->all();

            $query->andFilterWhere(['in', 'id', $elevatorsAccessibilities ? ArrayHelper::getColumn($elevatorsAccessibilities, 'elevator_id') : [0]]);
        }

        return $query;
    }

    public function addFilterByDigit($query, $field, $param) {
        if (strrpos($param, '>=') !== false) {
            $query->andFilterWhere(['>=', $field, str_replace('>=', '',$param)]);
        } elseif (strrpos($param, '>') !== false) {
            $query->andFilterWhere(['>', $field, str_replace('>', '',$param)]);
        } elseif (strrpos($param, '<=') !== false) {
            $query->andFilterWhere(['<=', $field, str_replace('<=', '',$param)]);
        } elseif (strrpos($param, '<') !== false) {
            $query->andFilterWhere(['<', $field, str_replace('<', '',$param)]);
        } elseif (strrpos($param, '-')) {
            $pieces = explode('-',$param);
            if (count($pieces) > 1) {
                $query->andFilterWhere(['between', $field, $pieces[0], $pieces[1]]);
            }
        } elseif (is_numeric($param)) {
            $query->andFilterWhere(['=', $field, $param]);
        }

        return $query;
    }
}