<?php

namespace backend\modules\news\models;

use Yii;

class NewsTagsNews extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'news_news_tags';
    }

    public function rules()
    {
        return [
            [['news_id', 'tag_id'], 'required'],
            [['news_id', 'tag_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'news_id' => Yii::t('news', 'One news'),
            'tag_id' => Yii::t('news', 'Tag'),
        ];
    }
}