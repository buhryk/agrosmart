<?php

namespace backend\modules\news\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii2mod\comments\models\CommentModel;

/**
 * CommentSearch represents the model behind the search form about `yii2mod\comments\models\CommentModel`.
 */
class CommentSearch extends CommentModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'entityId', 'level', 'createdBy', 'status', 'createdAt'], 'integer'],
            [['entity', 'content', 'relatedTo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CommentModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 15]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'entityId' => $this->entityId,
            'level' => $this->level,
            'createdBy' => $this->createdBy,
            'status' => $this->status,
            'entity' => '051d76ff'
        ]);

        if ($this->relatedTo) {
            $query->andFilterWhere(['relatedTo' => 'backend\modules\news\models\News:'.$this->relatedTo]);
        }

        $query->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
