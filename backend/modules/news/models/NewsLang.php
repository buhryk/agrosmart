<?php

namespace backend\modules\news\models;

use Yii;

class NewsLang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'news_model_lang';
    }

    public function rules()
    {
        return [
            [['news_id', 'lang', 'title', 'short_description', 'content'], 'required'],
            [['news_id'], 'integer'],
            [['short_description', 'content'], 'string'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => Yii::t('news', 'One news'),
            'lang' => Yii::t('common', 'Lang'),
            'title' => Yii::t('news', 'Title'),
            'short_description' => Yii::t('news', 'Short description'),
            'content' => Yii::t('news', 'Content'),
        ];
    }
}