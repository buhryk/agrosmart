<?php

namespace backend\modules\news\models;

use frontend\models\Company;
use Yii;

/**
 * This is the model class for table "news_user_accessibility".
 *
 * @property integer $id
 * @property integer $news_id
 * @property integer $role_id
 */
class NewsUserAccessibility extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_user_accessibility';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'type_id'], 'required'],
            [['news_id', 'type_id'], 'integer'],
            [['news_id'], 'exist', 'skipOnError' => true,
                'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true,
                'targetClass' => NewsUserAccessibilityType::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News ID',
            'type_id' => 'Type ID',
        ];
    }

    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    public function getType()
    {
        return $this->hasOne(NewsUserAccessibilityType::className(), ['id' => 'type_id']);
    }
}