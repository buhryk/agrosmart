<?php

namespace backend\modules\news\models;

use Yii;

class NewsCategoryLang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'news_category_lang';
    }

    public function rules()
    {
        return [
            [['category_id', 'lang', 'title'], 'required'],
            [['category_id'], 'integer'],
            [['short_description'], 'string'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => Yii::t('news', 'Category'),
            'lang' => Yii::t('common', 'Lang'),
            'title' => Yii::t('news', 'Title'),
            'short_description' => Yii::t('news', 'Short description'),
        ];
    }
}