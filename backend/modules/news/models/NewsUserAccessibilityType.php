<?php

namespace backend\modules\news\models;

use Yii;

/**
 * This is the model class for table "news_user_accessibility_type".
 *
 * @property integer $id
 * @property string $description
 */
class NewsUserAccessibilityType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_user_accessibility_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
        ];
    }
}