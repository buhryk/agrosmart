<?php

namespace backend\modules\news\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsLang;
use backend\modules\news\models\NewsTagLang;
use backend\modules\news\models\NewsTag;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class NewsSearch extends News
{
    const ONE_DAY_IN_SECONDS = 86400;
    public $title;
    public $tags;

    public function rules()
    {
        return [
            [['id', 'category_id', 'active', 'is_top', 'show_on_main_page'], 'integer'],
            [['created_at', 'published_at'], 'validateDateTime'],
            [['alias', 'title', 'tags'], 'safe'],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if ($this->$attribute != date('Y-m-d', strtotime($this->$attribute)) ) {
            $this->addError($attribute, Yii::t('common', 'Field must be in format') . ' "2000-00-00"');
        }
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = News::find()->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'active' => $this->active,
            'is_top' => $this->is_top,
            'show_on_main_page' => $this->show_on_main_page
        ]);

        $query->andFilterWhere(['like', 'alias', $this->alias]);

        if ($this->created_at) {
            $query->andFilterWhere([
                'between',
                'created_at',
                strtotime($this->created_at),
                strtotime($this->created_at) + self::ONE_DAY_IN_SECONDS]);
        }

        if ($this->published_at) {
            $query->andFilterWhere([
                'between',
                'published_at',
                strtotime($this->published_at),
                strtotime($this->published_at) + self::ONE_DAY_IN_SECONDS]);
        }

        if ($this->title) {
            $langs = NewsLang::find()
                ->where(['like', 'title', '%'.$this->title.'%', false])
                ->groupBy('news_id')
                ->all();

            $query->andFilterWhere(['in', 'id', $langs ? ArrayHelper::getColumn($langs, 'news_id') : [0]]);
        }

        if ($this->tags) {
            $tags = explode(',', $this->tags);

            $tagsLangs = NewsTagLang::find();

            foreach ($tags as $tagTitle) {
                $tagsLangs = $tagsLangs->orWhere(['like', 'title', '%'.trim($tagTitle).'%', false]);
            }

            $tagsLangs = $tagsLangs
                ->groupBy('tag_id')
                ->joinWith('tag')
                ->all();

            if (!$tagsLangs) {
                $query->andFilterWhere(['in', 'id', [0]]);
            } else {
                $newsModels = array();

                foreach ($tagsLangs as $tagLang) {
                    $newsModels = array_merge($newsModels, $tagLang->tag->news);
                }

                $query->andFilterWhere(['in', 'id', $newsModels ? ArrayHelper::getColumn($newsModels, 'id') : [0]]);
            }
        }

        return $dataProvider;
    }

    public function beforeValidate()
    {
        return \yii\db\ActiveRecord::beforeValidate();
    }

    public function afterValidate()
    {
        return \yii\db\ActiveRecord::afterValidate();
    }
}