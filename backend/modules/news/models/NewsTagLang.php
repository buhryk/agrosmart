<?php

namespace backend\modules\news\models;

use Yii;

class NewsTagLang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'news_tag_lang';
    }

    public function rules()
    {
        return [
            [['tag_id', 'lang', 'title'], 'required'],
            [['tag_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tag_id' => Yii::t('news', 'Tag'),
            'lang' => Yii::t('common', 'Lang'),
            'title' => Yii::t('news', 'Title'),
        ];
    }

    public function getTag()
    {
        return $this->hasOne(NewsTag::className(), ['id' => 'tag_id']);
    }
}