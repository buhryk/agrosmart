<?php
use backend\widgets\SeoWidget;

$this->title = Yii::t('news', 'Category SEO updating');
$this->params['breadcrumbs'][] = ['label' => Yii::t('news', 'Categories list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= SeoWidget::widget([
        'model' => $seo,
        'parameters' => ['modelLang' => $seoLang]
    ]); ?>
</div>