<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsCategory;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('news', 'Categories list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="rubric-view">

    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('news', 'Add news'), ['news/create', 'category' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>

        <a href="<?= Url::to(['create', 'parent' => $model->primaryKey]); ?>" class="btn btn-primary" style="float: right">
            <?= Yii::t('news', 'Add subcategory'); ?>
        </a>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Image'); ?></th>
            <td>
                <?php if ($model->image) { ?>
                    <?= Html::img($model->image->path, ['width' => 175]); ?>
                <?php } ?></td>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Parent category'); ?></th>
            <td>
                <?php $parent = $model->parent; ?>
                <?php if ($parent) { ?>
                    <a href="<?= Url::to(['view', 'id' => $parent->primaryKey]) ?>"><?= $parent->title; ?></a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Subcategories'); ?></th>
            <td>
                <?php $subcategories = $model->subcategories; ?>
                <?php foreach ($subcategories as $subcategory) { ?>
                    <a href="<?= Url::to(['view', 'id' => $subcategory->primaryKey]); ?>" class="block">
                        <?= $subcategory->title; ?>
                    </a>
                <?php } ?>

                <div style="margin-top: 10px">
                    <a href="<?= Url::to(['create', 'parent' => $model->primaryKey]); ?>"
                       class="btn-sm btn-success without-hover-text-decoration"
                    >
                        <?= Yii::t('news', 'Add subcategory'); ?>
                    </a>
                </div>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'News'); ?></th>
            <td>
                <a href="<?= Url::to(['news/index', 'id' => $model->primaryKey]); ?>">
                    <?= Yii::t('news', 'News'); ?>
                    (<?php echo News::find()->where(['category_id' => $model->primaryKey])->count(); ?>)
                </a>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('active'); ?></th>
            <td>
                <?= $model->active == NewsCategory::ACTIVE_YES ? Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('is_interesting'); ?></th>
            <td>
                <?= $model->is_interesting == NewsCategory::IS_INTERESTING_YES ?
                    Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Sort'); ?></th>
            <td><?= $model->sort; ?></td>
        </tr>
        </tbody>
    </table>
</div>