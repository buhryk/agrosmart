<?php
use yii\helpers\Url;
use backend\modules\news\models\NewsCategory;
use yii\helpers\Html;

$this->title = Yii::t('news', 'Categories list');
?>

<div class="rubric-default-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-primary block right">
            <?= Yii::t('news', 'Add category'); ?>
        </a>
    </h1>

    <table class="table table-bordered" style="margin-top: 15px;">
        <thead>
        <tr>
            <th width="75">ID</th>
            <th><?= Yii::t('news', 'Title'); ?></th>
            <th><?= Yii::t('news', 'Subcategories'); ?></th>
            <th>Alias</th>
            <th><?= Yii::t('news', 'Image'); ?></th>
            <th width="75"><?= Yii::t('news', 'Active'); ?></th>
            <th width="75"><?= Yii::t('news', 'Is interesting category'); ?></th>
            <th width="75"><?= Yii::t('news', 'Sort'); ?></th>
            <th width="130"><?= Yii::t('common', 'Actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $model) { ?>
            <tr class="<?= $model->active == NewsCategory::ACTIVE_NO ? 'danger' : ''; ?>">
                <td><?= $model->primaryKey; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"><?= $model->title; ?></a>
                </td>
                <td>
                    <?php $subcategories = $model->subcategories; ?>
                    <?php if ($subcategories) { ?>
                        <?php foreach ($subcategories as $subcategory) { ?>
                            <a href="<?= Url::to(['view', 'id' => $subcategory->primaryKey]); ?>" class="block">
                                <?= $subcategory->title; ?>
                            </a>
                        <?php } ?>
                    <?php } else { ?>
                        &nbsp;
                    <?php } ?>
                </td>
                <td><?= $model->alias; ?></td>
                <td style="text-align: center;">
                    <?php if ($model->image) { ?>
                        <?php echo Html::img($model->image->path, ['width' => 175]); ?>
                    <?php } ?>
                </td>
                <td>
                    <?= $model->active == NewsCategory::ACTIVE_YES ?
                        Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?>
                </td>
                <td>
                    <?= $model->is_interesting == NewsCategory::IS_INTERESTING_YES ?
                        Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?>
                </td>
                <td><?= $model->sort; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'View'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                    <a href="<?= Url::to(['update', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Edit'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="<?= Url::to(['delete', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Delete'); ?>"
                       class="btn btn-default btn-sm"
                       onclick="return confirm('<?= Yii::t('common', 'Are you sure you want to delete this item?'); ?>')"
                    >
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>