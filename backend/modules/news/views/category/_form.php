<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\news\models\NewsCategory;
use mihaildev\elfinder\InputFile;
use yii\helpers\Url;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'title') ?>
    <?= $form->field($modelLang, 'short_description')->textarea() ?>

    <?= $form->field($model, 'alias') ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number', 'value' => NewsCategory::DEFAULT_SORT]); ?>
    <?= $form->field($model, 'active')->dropDownList(array(
        NewsCategory::ACTIVE_YES => Yii::t('common', 'Yes'),
        NewsCategory::ACTIVE_NO => Yii::t('common', 'No')
    )); ?>
    <?= $form->field($model, 'is_interesting')->dropDownList(array(
        NewsCategory::IS_INTERESTING_NO => Yii::t('common', 'No'),
        NewsCategory::IS_INTERESTING_YES => Yii::t('common', 'Yes')
    )); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>