<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\news\models\NewsCategory;
use backend\modules\news\models\News;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Url;

$this->title = Yii::t('news', 'News list');
?>
<div class="news-index">
    <h1>
        <?= Html::encode($this->title) ?>
        <a href="<?= $searchModel->category_id ? Url::to(['create', 'category' => $searchModel->category_id]) : Url::to(['create']); ?>" class="btn btn-primary block right">
            <?= Yii::t('news', 'Add news'); ?>
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'title',
                'label' => Yii::t('news', 'Title'),
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['news/view', 'id' => $model->primaryKey]);
                },
            ],
            [
                'attribute' => 'category_id',
                'filter' => ArrayHelper::map(NewsCategory::find()->all(), 'id', 'title'),
                'format'=>'raw',
                'value' => function ($model) {
                    $category = $model->category;
                    return $category ? Html::a($category->title, ['category/view', 'id' => $model->category->primaryKey]) : '';
                }
            ],
            'alias',
            [
                'attribute' => 'active',
                'filter' => array(
                    News::ACTIVE_YES => Yii::t('common', 'Yes'),
                    News::ACTIVE_NO => Yii::t('common', 'No')
                ),
                'value' => function ($model) {
                    return $model->active == News::ACTIVE_YES ? Yii::t('common', 'Yes') : Yii::t('common', 'No');
                }
            ],
            [
                'attribute' => 'is_top',
                'filter' => array(
                    News::IS_TOP_YES => Yii::t('common', 'Yes'),
                    News::IS_TOP_NO => Yii::t('common', 'No')
                ),
                'value' => function ($model) {
                    return $model->is_top == News::IS_TOP_YES ? Yii::t('common', 'Yes') : Yii::t('common', 'No');
                }
            ],
            [
                'attribute' => 'show_on_main_page',
                'value' => 'showOnMainPageDetail',
                'filter' => News::getAllShowOnMainPageProperties()
            ],
            [
                'attribute' => 'tags',
                'label' => Yii::t('news', 'Tags'),
                'contentOptions'=>['style'=>'width: 85px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    $html = '';
                    foreach ($model->tags as $tag) {
                        $html .= Html::a($tag->title, ['tag/view', 'id' => $tag->primaryKey], ['class' => 'block']);
                    }
                    return $html;
                }
            ],
            [
                'attribute' => 'published_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->published_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'published_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'contentOptions'=>['style'=>'width: 130px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>
</div>