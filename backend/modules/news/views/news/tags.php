<?php
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('news', 'News tags managing');
$this->params['breadcrumbs'][] = ['label' => Yii::t('news', 'News list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model,
        'modelLang' => $modelLang
    ]); ?>
</div>

<?php if ($unselectedTags) { ?>
    <h2><?= Yii::t('news', 'Bind tag'); ?>:</h2>
    <div class="rubric-form">
        <?php $form = ActiveForm::begin(); ?>

        <?php echo $form->field($tag, 'tag_id', ['options' => ['style' => 'display: inline-block; float: left;']])
            ->dropDownList(ArrayHelper::map($unselectedTags, 'id', 'title'), [
                'prompt' => '',
                'style' => 'width: 300px; display: inline-block;'
            ])
            ->label(false)
        ?>

        <?= Html::submitButton(Yii::t('common', 'Add'), ['class' => 'btn btn-primary block left', 'style' => 'margin-left: 5px;']) ?>

        <?php ActiveForm::end(); ?>
    </div>
    <div style="clear: both"></div>
<?php } ?>

<h2><?= Yii::t('news', 'Selected tags'); ?>:</h2>

<table class="table table-bordered" style="margin-top: 15px;">
    <thead>
    <tr>
        <th width="75">#</th>
        <th><?= Yii::t('news', 'Title'); ?></th>
        <th>Alias</th>
        <th width="100">&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($model->tags as $key => $tag) { ?>
        <tr>
            <td><?= $key ?></td>
            <td>
                <?= $model->title; ?>
            </td>
            <td><?= $tag->alias; ?></td>
            <td>
                <a href="<?= Url::to(['unbind-tag', 'tag' => $tag->primaryKey, 'news' => $model->primaryKey]); ?>"
                    class="btn btn-danger"
                >
                    <?= Yii::t('common', 'Unbind'); ?>
                </a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>