<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\news\models\NewsCategory;
use yii\helpers\Url;
use backend\modules\news\models\News;
use vova07\imperavi\Widget as ImperaviWidget;
use kartik\widgets\DateTimePicker;
use yii\helpers\ArrayHelper;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')
        ->dropDownList(ArrayHelper::map(NewsCategory::find()->joinWith('lang')->all(), 'id', 'title'));
    ?>
    <?= $form->field($modelLang, 'title') ?>
    <?= $form->field($modelLang, 'short_description')->textarea() ?>

    <?= $form->field($modelLang, 'content')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['/site/image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <?= $form->field($model, 'alias') ?>

    <?php echo $form->field($model, 'published_at')->widget(DateTimePicker::classname(), [
        'options' => [],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd hh:ii'
        ]
    ]); ?>

    <div class="row">
        <div class="col-md-4 col-sm-12">
            <?= $form->field($model, 'active')->dropDownList(News::getAllActiveProperties()); ?>
        </div>
        <div class="col-md-4 col-sm-12">
            <?= $form->field($model, 'is_top')->dropDownList(News::getAllIsTopProperties()); ?>
        </div>
        <div class="col-md-4 col-sm-12">
            <?= $form->field($model, 'show_on_main_page')->dropDownList(News::getAllShowOnMainPageProperties()); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>