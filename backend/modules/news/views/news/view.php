<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsUserAccessibility;
use backend\modules\news\models\NewsUserAccessibilityType;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = $model->title;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('news', 'News category') . ' "'.$model->category->title.'"',
    'url' => ['index', 'id' => $model->category->primaryKey]
];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="rubric-view">

    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Category'); ?></th>
            <td>
                <?php $category = $model->category; ?>
                <?php if ($category) { ?>
                    <a href="<?= Url::to(['category/view', 'id' => $category->primaryKey]); ?>">
                        <?= $category->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Short description'); ?></th>
            <td><?= $model->short_description; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Image'); ?></th>
            <td>
                <?php if ($model->image) { ?>
                    <?= Html::img($model->image->path, ['width' => 175]); ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Selected tags'); ?></th>
            <td>
                <?php foreach ($model->tags as $tag) { ?>
                    <a href="<?= Url::to(['tag/view', 'id' => $tag->primaryKey]); ?>" class="block">
                        <?= $tag->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Published at'); ?></th>
            <td><?php echo date('Y-m-d H:i', $model->published_at); ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Created at'); ?></th>
            <td><?php echo date('Y-m-d H:i', $model->created_at); ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('news', 'Updated at'); ?></th>
            <td><?php echo date('Y-m-d H:i', $model->updated_at); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('active'); ?></th>
            <td><?= $model->activeDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('is_top'); ?></th>
            <td><?= $model->isTopDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('show_on_main_page'); ?></th>
            <td><?= $model->showOnMainPageDetail; ?></td>
        </tr>
        <tr>
            <th>Видимая для</th>
            <td>
                <?php $newsAccessibilities = $model->userAccessibilities; ?>
                <?php $allNewsAccessibilities = NewsUserAccessibilityType::find()->all(); ?>

                <?php if ($newsAccessibilities) { ?>
                    <div style="margin-bottom: 10px;">
                        <?php foreach ($newsAccessibilities as $one) { ?>
                            <div>
                                <?= $one->type->description; ?>
                                <a href="<?= Url::to(['delete-accessibility-property', 'id' => $one->primaryKey]); ?>">
                                    <span class="glyphicon glyphicon-trash" aria-hidden="true" style="color: #a94442;"></span>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

                <?php if (count($allNewsAccessibilities) > count($newsAccessibilities)) { ?>
                    <?php $newAccessibilitiesProperties = NewsUserAccessibilityType::find(); ?>
                    <?php if ($newsAccessibilities) {
                        $newAccessibilitiesProperties = $newAccessibilitiesProperties
                            ->where(['not in', 'id', ArrayHelper::getColumn($newsAccessibilities, 'type_id')]);
                    }
                    $newAccessibilitiesProperties = $newAccessibilitiesProperties->all(); ?>
                    <div class="news-user-accessibility-form">
                        <?php $form = ActiveForm::begin(['validateOnSubmit' => false]); ?>
                        <div class="row">
                            <div class="col-xs-8 col-md-6">
                                <?= $form->field($accessibility, 'type_id')
                                    ->dropDownList(ArrayHelper::map($newAccessibilitiesProperties, 'id', 'description'))
                                    ->label(false)
                                ?>
                            </div>
                            <div class="col-xs-4 col-md-6">
                                <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                <?php } ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>