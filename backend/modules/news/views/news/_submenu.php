<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;
?>

<h1><?= Html::encode($this->title) ?></h1>

<ul class="nav nav-tabs">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
            <?= Yii::t('common', 'Editing'); ?>
        </a>
    </li>
    <li <?= ($action === 'seo') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['seo', 'id' => $model->primaryKey]) ?>">
            SEO
        </a>
    </li>
    <li <?= ($action === 'tags') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['tags', 'id' => $model->primaryKey]) ?>">
            <?= Yii::t('news', 'Tags'); ?>
        </a>
    </li>
    <li <?= ($action === 'images') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['images', 'id' => $model->primaryKey]) ?>">
            <?= Yii::t('common', 'Images'); ?>
        </a>
    </li>
</ul>
<br>
