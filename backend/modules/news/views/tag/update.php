<?php
$this->title = Yii::t('news', 'Updating tag') . ' "' . $model->title . '"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('news', 'Tags list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => $model->primaryKey]];
?>

<div class="rubric-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>