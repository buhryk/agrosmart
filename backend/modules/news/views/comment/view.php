<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model yii2mod\comments\models\CommentModel */

$this->title = 'Комментарий #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Комментарии новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="comment-model-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('entity'); ?></th>
            <td><?= $model->entity; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('entityId'); ?></th>
            <td><?= $model->entityId; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('content'); ?></th>
            <td><?= $model->content; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('parentId'); ?></th>
            <td>
                <?php $parent = $model->parent;
                echo $parent ? Html::a('Комментарий #' . $parent->id, ['/news/comment/view', 'id' => $parent->id]) : '';
                ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('createdBy'); ?></th>
            <td><?= $model->createdBy; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('relatedTo'); ?></th>
            <td><?= $model->relatedTo; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('url'); ?></th>
            <td><?= $model->url; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('status'); ?></th>
            <td><?= $model->status; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('createdAt'); ?></th>
            <td><?= $model->createdAt; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('updatedAt'); ?></th>
            <td><?= $model->updatedAt; ?></td>
        </tr>
        </tbody>
    </table>
</div>
