<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use backend\modules\news\models\News;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\news\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Комментарии новостей';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="comment-model-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'createdBy',
                'value' => function ($model) {
                    return $model->getAuthorName();
                },
                'filter' => $searchModel::getAuthors(),
                'filterInputOptions' => ['prompt' => Yii::t('yii2mod.comments', 'Select Author'), 'class' => 'form-control'],
            ],
            'content:ntext',
            [
                'attribute' => 'parentId',
                'format' => 'raw',
                'value' => function ($model) {
                    $parent = $model->parent;
                    return $parent ? Html::a('Комментарий #' . $parent->id, ['/news/comment/view', 'id' => $parent->id]) : '';
                },
                'filter' => false

            ],
            'level',
            [
                'attribute' => 'relatedTo',
                'contentOptions' => ['style' => 'width: 140px;'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(News::find()->joinWith('lang')->all(), 'id', 'title'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>false, 'placeholder' => ''],
                ],
                'format'=>'raw',
                'value' => function ($model) {
                    $news = News::findOne($model->entityId);
                    return $news ? Html::a($news->title, ['/news/news/view', 'id' => $model->entityId]) : '';
                },
            ],
            [
                'attribute' => 'url',
                'format' => 'raw',
                'value' => function ($model) {
                    $frontendUrl = isset(Yii::$app->params['frontendUrl']) ? Yii::$app->params['frontendUrl'] : null;
                    return $frontendUrl ? Html::a($frontendUrl . $model->url, $frontendUrl . $model->url) : '';
                },
                'filter' => false
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return \yii2mod\moderation\enums\Status::getLabel($model->status);
                },
                'filter' => \yii2mod\moderation\enums\Status::listData(),
                'filterInputOptions' => ['prompt' => Yii::t('yii2mod.comments', 'Select Status'), 'class' => 'form-control'],
            ],
            [
                'attribute' => 'createdAt',
                'value' => function ($model) {
                    return date('Y-m-d H:i:s', $model->createdAt);
                },
                'filter' => false,
            ],
            [
                'contentOptions'=>['style'=>'width: 100px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        );
                }
            ],
        ],
    ]); ?>
</div>
