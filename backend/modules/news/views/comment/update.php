<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model yii2mod\comments\models\CommentModel */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Комментарии новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Комментарий #' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="comment-model-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
