<?php
namespace backend\modules\news\controllers;

use yii\base\Model;
use yii\web\Controller;
use backend\modules\news\models\NewsCategory;
use backend\modules\news\models\NewsCategoryLang;
use yii\data\ActiveDataProvider;
use Yii;
use yii\web\NotFoundHttpException;
use backend\modules\transliterator\services\TransliteratorService;
use backend\modules\seo\models\Seo;
use backend\modules\seo\models\SeoLang;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;
use backend\modules\accesscontrol\AccessControlFilter;

class CategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NewsCategory::find()
                ->where(['parent_id' => null])
                ->orderBy('sort DESC')
        ]);

        return $this->render('index', [
            'data' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new NewsCategory();
        $modelLang = new NewsCategoryLang();
        $modelLang->lang = Yii::$app->language;

        $parentId = Yii::$app->request->get('parent');
        if ($parentId) {
            $parent = $this->findCategory($parentId);

            $model->parent_id = $parent->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->category_id = 0;

            if (Model::validateMultiple([$model, $modelLang])) {
                $model->save();
                $modelLang->category_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findCategory($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findCategory($id);

        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new NewsCategoryLang();
            $modelLang->category_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                if ($modelLang->lang == TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT) {
                    $model->alias = TransliteratorService::transliterate($modelLang->title);
                } else {
                    $modelLangForAlias = NewsCategoryLang::find()
                        ->where([
                            'category_id' => $model->primaryKey,
                            'lang' => TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT
                        ])
                        ->one();

                    if ($modelLangForAlias) {
                        $model->alias = TransliteratorService::transliterate($modelLangForAlias->title);
                    }
                }
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $category = $this->findCategory($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    private function findCategory($id)
    {
        $model = NewsCategory::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Category not found.');
        }

        return $model;
    }

    public function actionSeo($id)
    {
        $model = $this->findCategory($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
            $seo->save();
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->seo_id = $seo->primaryKey;
            $seoLang->lang = Yii::$app->language;
            $seoLang->save();
        }

        if ($seoLang->load(Yii::$app->request->post()) && $seoLang->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    public function actionImages($id)
    {
        $model = $this->findCategory($id);

        $image = new Image();
        $imageLang = new ImageLang();
        $imageLang->lang = Yii::$app->language;

        return $this->render('images', [
            'model' => $model,
            'image' => $image,
            'imageLang' => $imageLang
        ]);
    }
}