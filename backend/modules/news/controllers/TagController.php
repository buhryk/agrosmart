<?php
namespace backend\modules\news\controllers;

use Yii;
use yii\base\Model;
use yii\web\Controller;
use backend\modules\news\models\NewsTag;
use backend\modules\news\models\NewsTagLang;
use backend\modules\transliterator\services\TransliteratorService;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use backend\modules\accesscontrol\AccessControlFilter;

class TagController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NewsTag::find()
        ]);

        return $this->render('index', [
            'data' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new NewsTag();
        $modelLang = new NewsTagLang();
        $modelLang->lang = Yii::$app->language;
        $modelLang->tag_id = 0;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->tag_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findTag($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findTag($id);

        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new NewsTagLang();
            $modelLang->tag_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            if ($model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $category = $this->findTag($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    private function findTag($id)
    {
        $model = NewsTag::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Tag not found.');
        }

        return $model;
    }
}