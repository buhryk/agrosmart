<?php
namespace backend\modules\news\controllers;

use backend\modules\news\models\NewsUserAccessibility;
use backend\modules\seo\models\SeoLang;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsLang;
use backend\modules\news\models\NewsTag;
use backend\modules\news\models\NewsTagsNews;
use backend\modules\news\models\NewsCategory;
use backend\modules\news\models\NewsSearch;
use backend\modules\transliterator\services\TransliteratorService;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use backend\modules\seo\models\Seo;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;
use yii\filters\AccessControl;
use backend\modules\accesscontrol\AccessControlFilter;

class NewsController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex($id = null)
    {
        $searchModel = new NewsSearch();
        $params = Yii::$app->request->queryParams;

        if ($id && !isset(Yii::$app->request->queryParams['NewsSearch'])) {
            $params['NewsSearch']['category_id'] = (int)$id;
        }

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new News();
        $modelLang = new NewsLang();
        $modelLang->lang = Yii::$app->language;

        $category = Yii::$app->request->get('category');
        if ($category) {
            $category = $this->findCategory($category);
            $model->category_id = $category->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->news_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->news_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $model->published_at = date('Y-m-d H:i');

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findNews($id);
        $model->published_at = date('Y-m-d H:i', $model->published_at);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new NewsLang();
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->news_id = $model->primaryKey;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findNews($id);
        $accessibility = new NewsUserAccessibility();
        $accessibility->news_id = $model->primaryKey;

        if ($accessibility->load(Yii::$app->request->post()) && $accessibility->save()) {
            return $this->refresh();
        }

        return $this->render('view', [
            'model' => $model,
            'accessibility' => $accessibility
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findNews($id);
        $category = $model->category;
        $model->delete();

        return $this->redirect(['index', 'id' => $category->primaryKey]);
    }

    public function actionSeo($id)
    {
        $model = $this->findNews($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
            $seo->save();
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->seo_id = $seo->primaryKey;
            $seoLang->lang = Yii::$app->language;
            $seoLang->save();
        }

        if ($seoLang->load(Yii::$app->request->post()) && $seoLang->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    private function findCategory($id)
    {
        $model = NewsCategory::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Category not found.');
        }

        return $model;
    }

    private function findNews($id)
    {
        $model = News::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('News not found.');
        }

        return $model;
    }

    public function actionTags($id)
    {
        $model = $this->findNews($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new NewsLang();
            $modelLang->news_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        $tag = new NewsTagsNews();
        $tag->news_id = $model->primaryKey;

        if ($tag->load(Yii::$app->request->post()) && $tag->save()) {
            return $this->redirect(['tags', 'id' => $model->primaryKey]);
        }

        return $this->render('tags', [
            'model' => $model,
            'modelLang' => $modelLang,
            'tag' => $tag,
            'unselectedTags' => $model->unselectedTags
        ]);
    }

    public function actionUnbindTag()
    {
        $tagId = Yii::$app->request->get('tag');
        $newsId = Yii::$app->request->get('news');

        if (!$tagId) {
            throw new BadRequestHttpException(Yii::t('common', 'Not all parameters provided') . ' (tag).');
        }
        if (!$newsId) {
            throw new BadRequestHttpException(Yii::t('common', 'Not all parameters provided') . ' (news).');
        }

        $news = $this->findNews($newsId);
        $tag = $this->findTag($tagId);

        NewsTagsNews::deleteAll(['tag_id' => $tag->primaryKey, 'news_id' => $news->primaryKey]);

        return $this->redirect(['tags', 'id' => $news->primaryKey]);
    }

    private function findTag($id)
    {
        $model = NewsTag::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Tag not found.');
        }

        return $model;
    }

    public function actionImages($id)
    {
        $model = $this->findNews($id);

        $image = new Image();
        $imageLang = new ImageLang();
        $imageLang->lang = Yii::$app->language;

        return $this->render('images', [
            'model' => $model,
            'image' => $image,
            'imageLang' => $imageLang
        ]);
    }

    public function actionDeleteAccessibilityProperty($id)
    {
        $model = NewsUserAccessibility::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Page not found.');
        }

        $newsId = $model->news_id;

        $model->delete();
        return $this->redirect(['view', 'id' => $newsId]);
    }
}