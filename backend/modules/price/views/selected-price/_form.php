<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Company;
use backend\modules\rubric\models\Rubric;
use yii\helpers\ArrayHelper;
use frontend\modules\cabinet\models\Sale;
use backend\modules\commondata\models\Region;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model backend\modules\price\models\SelectedPrice */
/* @var $form yii\widgets\ActiveForm */
$companyList = Company::find()
    ->where(['status' => Company::STATUS_ACTIVE_YES])
    ->andWhere(['type' => Company::TYPE_TRADER])
    ->all();

$rubricList = Rubric::find()
    ->andWhere(['parent_id' => Rubric::TRAIDER_RUBRIC_ID])
    ->andWhere(['active' => Rubric::ACTIVE_YES])
    ->all();

$regionList = Region::find()->all()

?>

<div class="selected-price-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8">
             <?= \kartik\select2\Select2::widget([
                'name' => 'selected_price_company',
                'value' => ArrayHelper::getColumn($model->companies, 'id'), // initial value
                'data' => ArrayHelper::map($companyList, 'id', 'name'),
                'options' => ['placeholder' => '', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'maximumInputLength' => 10
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'oblast_id')->dropDownList(ArrayHelper::map($regionList, 'id', 'title')) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'rubric_id')->dropDownList(ArrayHelper::map($rubricList, 'id', 'title')) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'position')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList([Sale::TYPE_PRODAZHA => 'Продажа', Sale::TYPE_ZAKUPKA => 'Закупка']) ?>
        </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
