<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\price\models\SelectedPrice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Выбранные цены', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="selected-price-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'rubricName',
            'position',
            'oblastName',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
