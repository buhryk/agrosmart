<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\price\models\SelectedPrice */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Выбранные цены', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="selected-price-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
