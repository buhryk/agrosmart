<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\price\models\SelectedPrice */

$this->title = 'Редактирование: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Выбранные цены', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="selected-price-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
