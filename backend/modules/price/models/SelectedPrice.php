<?php

namespace backend\modules\price\models;

use backend\modules\commondata\models\Region;
use Yii;
use frontend\models\Company;
use backend\modules\rubric\models\Rubric;

/**
 * This is the model class for table "selected_price".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $rubric_id
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 */
class SelectedPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'selected_price';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }


    public function rules()
    {
        return [
            [['oblast_id', 'rubric_id'], 'required'],
            [['rubric_id', 'position', 'created_at', 'updated_at', 'type', 'oblast_id'], 'integer'],
            [['rubric_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubric::className(), 'targetAttribute' => ['rubric_id' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'rubric_id' => 'Рубрика',
            'position' => 'Позиция',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
            'companyName' => 'Компания',
            'rubricName' => 'Категория',
            'oblast_id' => 'Область',
            'oblastName' => 'Область',
            'type' => 'Тип'
        ];
    }

    public function getRubric()
    {
        return $this->hasOne(Rubric::className(), ['id' => 'rubric_id']);
    }

    public function getOblast()
    {
        return $this->hasOne(Region::className(), ['id' => 'oblast_id']);
    }

    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])
            ->viaTable(SelectedPriceCompany::tableName(), ['selected_price_id' => 'id']);
    }
    
    public function getRubricName()
    {
        return $this->rubric ? $this->rubric->title : '';
    }

    public function getOblastName()
    {
        return $this->oblast ? $this->oblast->title : '';
    }

    public function companiesSave($data = [])
    {
        SelectedPriceCompany::deleteAll(['selected_price_id' => $this->id]);
        if (is_array($data)) {
            foreach ($data as $item) {
                $model = new SelectedPriceCompany();
                $model->company_id = $item;
                $model->selected_price_id= $this->id;
                $model->save();
            }
        }
    }

}
