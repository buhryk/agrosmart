<?php

namespace backend\modules\price\models;

use Yii;

/**
 * This is the model class for table "selected_price_company".
 *
 * @property integer $company_id
 * @property integer $selected_price_id
 */
class SelectedPriceCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'selected_price_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'selected_price_id'], 'required'],
            [['company_id', 'selected_price_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'selected_price_id' => 'Selected Price ID',
        ];
    }
}
