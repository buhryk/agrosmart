<?php

namespace backend\modules\commondata\models;

use backend\modules\package\models\Package;
use backend\modules\page\Page;
use backend\modules\tariff\models\Tariff;
use Yii;

/**
 * This is the model class for table "site_role".
 *
 * @property integer $id
 * @property string $name
 * @property integer $authorized
 * @property integer $type
 * @property integer $page_id
 */
class SiteRole extends \yii\db\ActiveRecord
{
    const AUTHORIZED_YES = 1;
    const AUTHORIZED_NOT = 2;

    const TYPE_COMPANY = 1;
    const TYPE_USER = 2;

    public static function tableName()
    {
        return 'site_role';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['authorized', 'type', 'page_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'authorized' => 'Авторизированный',
            'type' => 'Тип',
            'page_id' => 'Страница',
        ];
    }

    public function getAuthorizedList()
    {
        return [
            self::AUTHORIZED_NOT => 'Нет',
            self::AUTHORIZED_YES => 'Да',
            0 => 'Не задано',
        ];
    }

    public function getTypeList()
    {
        return [
            self::TYPE_COMPANY => 'Компания',
            self::TYPE_USER => 'Пользователь',
            0 => 'Не задано',
        ];
    }

    public function getPage()
    {
        return $this->hasOne(\backend\modules\page\models\Page::className(), ['id' => 'page_id']);
    }

    public function getPackageList()
    {
        return $this->hasMany(Package::className(), ['site_role_id' => 'id'])
            ->andWhere(['is_page' => Package::SHOW_PAGE]);
    }


    public function getPageTitle()
    {
        return $this->page ? $this->page->title : '';
    }

}
