<?php

namespace backend\modules\commondata\models;

use Yii;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property integer $oblast_id
 */
class RegionArea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oblast_id'], 'required'],
            [['oblast_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oblast_id' => 'Область',
            'title' => 'Название',
        ];
    }

    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'oblast_id'])->joinWith('lang');
    }

    public function getLang()
    {
        return $this->hasOne(RegionAreaLang::className(), ['area_id' => 'id'])
            ->where(['lang' => \Yii::$app->language]);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }
}