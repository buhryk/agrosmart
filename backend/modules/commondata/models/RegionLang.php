<?php

namespace backend\modules\commondata\models;

use Yii;

class RegionLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oblast_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oblast_id', 'lang', 'title'], 'required'],
            [['oblast_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 55],
            ['title', 'uniqueWithLang']
        ];
    }

    public function uniqueWithLang($attribute, $params)
    {
        $models = $this::find()
            ->where(['title' => $this->$attribute, 'lang' => Yii::$app->language])
            ->all();

        if ($models && (count($models) > 1 || $models[0]->primaryKey != $this->primaryKey)) {
            $this->addError($attribute, Yii::t('common', 'Value') . ' "'.$this->$attribute.'" ' .
                Yii::t('common', 'is already used for this field'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oblast_id' => 'Oblast ID',
            'lang' => 'Lang',
            'title' => 'Title',
        ];
    }
}