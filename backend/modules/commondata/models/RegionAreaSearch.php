<?php

namespace backend\modules\commondata\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\commondata\models\RegionArea;

/**
 * RegionAreaSearch represents the model behind the search form about `backend\modules\commondata\models\RegionArea`.
 */
class RegionAreaSearch extends RegionArea
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'oblast_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RegionArea::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'oblast_id' => $this->oblast_id,
        ]);

        return $dataProvider;
    }
}
