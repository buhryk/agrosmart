<?php

namespace backend\modules\commondata\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\commondata\models\Region;
use yii\helpers\ArrayHelper;

/**
 * RegionSearch represents the model behind the search form about `backend\modules\commondata\models\Region`.
 */
class RegionSearch extends Region
{
    public $title;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id'], 'integer'],
            [['alias', 'title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Region::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
        ]);

        $query->andFilterWhere(['like', 'alias', $this->alias]);

        if ($this->title) {
            $langs = RegionLang::find()
                ->where(['like', 'title', '%'.$this->title.'%', false])
                ->groupBy('oblast_id')
                ->all();

            $query->andFilterWhere(['in', 'id', $langs ? ArrayHelper::getColumn($langs, 'oblast_id') : [0]]);
        }

        return $dataProvider;
    }
}