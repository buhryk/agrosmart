<?php

namespace backend\modules\commondata\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property integer $oblast_id
 * @property integer $area_id
 * @property integer $type
 */
class City extends \yii\db\ActiveRecord
{
    const TYPE_CITY = 1;
    const TYPE_VILLAGE_CITY = 2;
    const TYPE_VILLAGE = 3;
    const TYPE_VILLAGE_MIN = 4;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oblast_id', 'area_id', 'type'], 'required'],
            [['oblast_id', 'area_id', 'type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oblast_id' => 'Область',
            'area_id' => 'Район',
            'type' => 'Тип',
            'title'=> 'Название'
        ];
    }

    public function getLang()
    {
        return $this->hasOne(CityLang::className(), ['city_id' => 'id'])
            ->where(['lang' => \Yii::$app->language]);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'oblast_id'])->joinWith('lang');
    }

    public function getArea()
    {
        return $this->hasOne(RegionArea::className(), ['id' => 'area_id'])->joinWith('lang');
    }

    public function getDetailInfo()
    {
        $area = ($area = $this->area) !== null ? $area : null;
        $region = $this->region;

        return $this->title . ', ' . ($area ? ($area->title . ', ') : '') . $region->title;
    }

    public function getTypeList()
    {
        return [
            self::TYPE_CITY => 'Горад',
            self::TYPE_VILLAGE_CITY => 'Поселок городского типа',
            self::TYPE_VILLAGE=> 'Поселок',
            self::TYPE_VILLAGE_MIN => 'Село',
        ];
    }

    public function getTypeDeteil()
    {
        return isset($this->typeList[$this->type]) ? $this->typeList[$this->type] : '';
    }
}