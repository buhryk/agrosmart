<?php

namespace backend\modules\commondata\models;

use Yii;

/**
 * This is the model class for table "city_lang".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $lang
 * @property string $title
 */
class CityLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city_lang';
    }

    public function behaviors()
    {
        return [
            'translatesDuplicator' => [
                'class' => \backend\behaviors\DuplicatorEntityTranslatesBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'lang', 'title'], 'required'],
            [['city_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'lang' => 'Lang',
            'title' => 'Title',
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::tableName(), ['id' => 'city_id']);
    }
}
