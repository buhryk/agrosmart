<?php

namespace backend\modules\commondata\models;

use Yii;

class Currency extends \yii\db\ActiveRecord
{
    const IS_MAIN_NO = 0;
    const IS_MAIN_YES = 1;
    const ACTIVE_NO = 0;
    const ACTIVE_YES = 1;
    const DEFAULT_CURRENCY_ID = 2;

    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code', 'sign', 'weight'], 'required'],
            [['status', 'is_main'], 'integer'],
            ['weight', 'number'],
            [['title'], 'string', 'max' => 128],
            [['code', 'sign'], 'string', 'max' => 5],
            [['code', 'title'], 'unique'],
            ['weight', 'default', 'value' => 1],
            ['status', 'default', 'value' => self::ACTIVE_YES],
            ['is_main', 'default', 'value' => self::IS_MAIN_NO],
            ['is_main', 'checkIsMain']
        ];
    }

    public function checkIsMain($attribute, $params)
    {
        if ($this->$attribute == self::IS_MAIN_YES) {
            $mainCurrencies = $this::find()->where(['is_main' => self::IS_MAIN_YES])->all();

            if (count($mainCurrencies) == 1) {
                if ($this->isNewRecord || !$this->isNewRecord && $mainCurrencies[0]->primaryKey != $this->primaryKey) {
                    $this->addError($attribute, Yii::t('common', 'Main currency can be only one'));
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('common', 'Title'),
            'code' => Yii::t('common', 'Code'),
            'sign' => Yii::t('common', 'Sign'),
            'weight' => Yii::t('common', 'Currency weight'),
            'status' => Yii::t('common', 'Is show'),
            'is_main' => Yii::t('common', 'Is main currency')
        ];
    }

    public static function getMainCurrency()
    {
        $model = self::find()->where(['is_main' => self::IS_MAIN_YES])->one();

        if (!$model) {
            $model = self::findOne(self::DEFAULT_CURRENCY_ID);

            if (!$model) {
                $model = self::find()->one();
            }
        }

        return $model;
    }

    public static function getAllStatuses()
    {
        return [
            self::ACTIVE_YES => Yii::t('common', 'Yes'),
            self::ACTIVE_NO => Yii::t('common', 'No')
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getAllStatuses()[$this->status]) ? self::getAllStatuses()[$this->status] : 'Undefined';
    }

    public static function getAllIsMainProperties()
    {
        return [
            self::IS_MAIN_NO => Yii::t('common', 'No'),
            self::IS_MAIN_YES => Yii::t('common', 'Yes')
        ];
    }

    public function getIsMainDetail()
    {
        return isset(self::getAllIsMainProperties()[$this->is_main]) ? self::getAllIsMainProperties()[$this->is_main] : 'Undefined';
    }
}