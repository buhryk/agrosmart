<?php

namespace backend\modules\commondata\models;

use Yii;

class RegionAreaLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area_lang';
    }

    public function behaviors()
    {
        return [
            'translatesDuplicator' => [
                'class' => \backend\behaviors\DuplicatorEntityTranslatesBehavior::className(),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area_id', 'lang', 'title'], 'required'],
            [['area_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_id' => 'Area ID',
            'lang' => 'Lang',
            'title' => 'Title',
        ];
    }
}