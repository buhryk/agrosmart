<?php
namespace backend\modules\commondata\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use backend\modules\transliterator\services\TransliteratorService;
use backend\modules\commondata\models\Country;
use backend\modules\commondata\models\CountryLang;

class CountryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Country::find()
        ]);

        return $this->render('index', [
            'data' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new Country();
        $modelLang = new CountryLang();
        $modelLang->lang = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->country_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->country_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findCountry($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findCountry($id);

        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new CountryLang();
            $modelLang->country_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                if ($modelLang->lang == TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT) {
                    $model->alias = TransliteratorService::transliterate($modelLang->title);
                } else {
                    $modelLangForAlias = CountryLang::find()
                        ->where([
                            'country_id' => $model->primaryKey,
                            'lang' => TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT
                        ])
                        ->one();

                    if ($modelLangForAlias) {
                        $model->alias = TransliteratorService::transliterate($modelLangForAlias->title);
                    }
                }
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $category = $this->findCountry($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    private function findCountry($id)
    {
        $model = Country::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('event', 'County not found'));
        }

        return $model;
    }
}