<?php

namespace backend\modules\commondata\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\commondata\models\RegionAreaLang;
use Yii;
use backend\modules\commondata\models\RegionArea;
use backend\modules\commondata\models\RegionAreaSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RegionAreaController implements the CRUD actions for RegionArea model.
 */
class RegionAreaController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    /**
     * Lists all RegionArea models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RegionAreaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RegionArea model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RegionArea model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RegionArea();
        $modelLang = new RegionAreaLang();
        $modelLang->lang = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {

            $modelLang->area_id= 1;

            if (Model::validateMultiple([$model, $modelLang])) {
                $model->save();
                $modelLang->area_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }


        $model->load(Yii::$app->request->get());

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing City model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new RegionAreaLang();
            $modelLang->area_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$model, $modelLang])) {
                if ($model->save() && $modelLang->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        $model->load(Yii::$app->request->get());

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Deletes an existing RegionArea model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RegionArea model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RegionArea the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RegionArea::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
