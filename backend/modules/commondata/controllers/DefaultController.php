<?php
namespace backend\modules\commondata\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}