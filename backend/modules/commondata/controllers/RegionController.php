<?php
namespace backend\modules\commondata\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\commondata\models\RegionSearch;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use backend\modules\transliterator\services\TransliteratorService;
use backend\modules\commondata\models\Region;
use backend\modules\commondata\models\RegionLang;

class RegionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new RegionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /*public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Region::find()->joinWith('lang')->orderBy(['title' => SORT_ASC]),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'data' => $dataProvider
        ]);
    }*/

    public function actionCreate()
    {
        $model = new Region();
        $modelLang = new RegionLang();
        $modelLang->lang = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->oblast_id = $model->primaryKey;

            if (Model::validateMultiple([$model, $modelLang])) {
                $model->save();
                $modelLang->oblast_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findRegion($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findRegion($id);

        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new RegionLang();
            $modelLang->oblast_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                if ($modelLang->lang == TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT) {
                    $model->alias = TransliteratorService::transliterate($modelLang->title);
                } else {
                    $modelLangForAlias = RegionLang::find()
                        ->where([
                            'oblast_id' => $model->primaryKey,
                            'lang' => TransliteratorService::DEFAUL_LANG_FOR_TRANSLIT
                        ])
                        ->one();

                    if ($modelLangForAlias) {
                        $model->alias = TransliteratorService::transliterate($modelLangForAlias->title);
                    }
                }
            }

            if (Model::validateMultiple([$model, $modelLang])) {
                if ($model->save() && $modelLang->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $category = $this->findRegion($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    private function findRegion($id)
    {
        $model = Region::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('common', 'Region not found'));
        }

        return $model;
    }
}