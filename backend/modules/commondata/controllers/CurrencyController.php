<?php
namespace backend\modules\commondata\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use backend\modules\commondata\models\Currency;

class CurrencyController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Currency::find()
        ]);

        return $this->render('index', [
            'data' => $dataProvider,
            'mainCurrency' => Currency::getMainCurrency()
        ]);
    }

    public function actionCreate()
    {
        $model = new Currency();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findCurrency($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findCurrency($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $category = $this->findCurrency($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    private function findCurrency($id)
    {
        $model = Currency::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('event', 'Currency not found'));
        }

        return $model;
    }
}