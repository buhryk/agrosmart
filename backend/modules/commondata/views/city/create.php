<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\commondata\models\City */

$this->title = 'CДобавление';
$this->params['breadcrumbs'][] = ['label' => 'Горада', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>
