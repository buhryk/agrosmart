<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\commondata\models\Region;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use backend\modules\commondata\models\RegionArea;

/* @var $this yii\web\View */
/* @var $model backend\modules\commondata\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ]]); ?>


    <?= $form->field($model, 'oblast_id')
        ->dropDownList(ArrayHelper::map(Region::find()->all(), 'id', 'title')) ?>
    <?php Pjax::begin() ?>
    <?php if ($model->oblast_id): ?>
    <?= $form->field($model, 'area_id')
            ->dropDownList(ArrayHelper::map(RegionArea::find()->andWhere(['oblast_id' => $model->oblast_id])->all(), 'id', 'title'))?>
    <?php endif; ?>
    <?php Pjax::end() ?>
    <?= $form->field($model, 'type')->dropDownList($model->typeList) ?>

    <?= $form->field($modelLang, 'title')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs("
    $('body').on('change', 'select#city-oblast_id', function() {
        var url = $(this).parents('form').attr('action');
        $.pjax.reload({container:'#p0', url: url, data: $(this).parents('form').serialize()});
    })
", \yii\web\View::POS_READY)

?>