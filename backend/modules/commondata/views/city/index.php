<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\commondata\models\RegionArea;
use backend\modules\commondata\models\Region;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\commondata\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Горада';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            [
                'attribute' => 'area_id',
                'label' => 'Район',
                'contentOptions' => ['style' => 'width: 140px;'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(RegionArea::find()->all(), 'id', 'title'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>false, 'placeholder' => ''],
                ],
                'format'=>'raw',
                'value' => function ($model) {
                    return $model->area ? $model->area->title : '';
                },
            ],
            [
                'attribute' => 'oblast_id',
                'label' => 'Область',
                'contentOptions' => ['style' => 'width: 140px;'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Region::find()->all(), 'id', 'title'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>false, 'placeholder' => ''],
                ],
                'format'=>'raw',
                'value' => function ($model) {
                    return $model->region->title;
                },
            ],
            [
                'attribute' => 'type',
                'filter' => $searchModel->typeList,
                'value' => function($model) {
                    return $model->typeDeteil;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
