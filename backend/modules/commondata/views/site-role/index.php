<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\commondata\models\SiteRoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Роли';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-role-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'authorized',
            [
                'attribute' => 'authorized',
                'value' => function($model) {
                    return $model->authorizedList[$model->authorized];
                },
                'filter' => $searchModel->authorizedList,
            ],
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->typeList[$model->type];
                },
                'filter' => $searchModel->typeList,
            ],
            'pageTitle',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
