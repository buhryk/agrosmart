<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\commondata\models\SiteRole;
use backend\modules\page\models\Category;
use backend\modules\page\models\Page;

$pageList = Page::find()->where(['category_id' => 1])->all()
?>

<div class="site-role-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'authorized')->dropDownList([0 => 'Не задано']+$model->authorizedList) ?>

    <?= $form->field($model, 'type')->dropDownList([0 => 'Не задано']+$model->typeList) ?>

    <?php
    if ($model->type == SiteRole::TYPE_COMPANY) {
        echo $form->field($model, 'page_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map($pageList, 'id', 'title'),
            'options' => ['placeholder' => '..'],
            'id' => 'rubric-select',
            'pluginOptions' => [
                
            ],
        ]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
