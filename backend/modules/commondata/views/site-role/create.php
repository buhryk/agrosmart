<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\commondata\models\SiteRole */

$this->title = 'Создание роли';
$this->params['breadcrumbs'][] = ['label' => 'Роли', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
