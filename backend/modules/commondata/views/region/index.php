<?php
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\commondata\models\Country;
use yii\helpers\Html;

$this->title = Yii::t('common', 'Regions list');
?>

<div class="rubric-default-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-primary block right">
            <?= Yii::t('common', 'Add region'); ?>
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'title',
                'label' => Yii::t('news', 'Title'),
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['view', 'id' => $model->primaryKey]);
                },
            ],
            'alias',
            [
                'attribute' => 'country_id',
                'filter' => ArrayHelper::map(Country::find()->joinWith('lang')->all(), 'id', 'title'),
                'format'=>'raw',
                'value' => function ($model) {
                    $country = $model->country;
                    return $country ? $country->title : '';
                },
            ],
            [
                'contentOptions'=>['style'=>'width: 120px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>
</div>