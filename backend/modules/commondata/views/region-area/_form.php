<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\commondata\models\Region;

/* @var $this yii\web\View */
/* @var $model backend\modules\commondata\models\RegionArea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="region-area-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'oblast_id')
        ->dropDownList(ArrayHelper::map(Region::find()->all(), 'id', 'title')) ?>
    <?=$form->field($modelLang, 'title') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
