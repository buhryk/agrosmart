<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\commondata\models\RegionArea */

$this->title = 'Добавление';
$this->params['breadcrumbs'][] = ['label' => 'Районы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-area-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>
