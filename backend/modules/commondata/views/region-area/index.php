<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\modules\commondata\models\Region;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\commondata\models\RegionAreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Районы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-area-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'title',
            [
                'attribute' => 'oblast_id',
                'label' => 'Область',
                'contentOptions' => ['style' => 'width: 140px;'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Region::find()->all(), 'id', 'title'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>false, 'placeholder' => ''],
                ],
                'format'=>'raw',
                'value' => function ($model) {
                    return $model->region->title;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
