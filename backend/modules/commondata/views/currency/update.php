<?php
$this->title = Yii::t('common', 'Currency updating');

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Currencies list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>
</div>