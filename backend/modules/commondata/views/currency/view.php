<?php
use yii\helpers\Html;
use backend\modules\commondata\models\Currency;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Currencies list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubric-view">

    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('code'); ?></th>
            <td><?= $model->code; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('sign'); ?></th>
            <td><?= $model->sign; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('weight'); ?></th>
            <td><?= $model->weight; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('is_main'); ?></th>
            <td><?= $model->isMainDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('status'); ?></th>
            <td><?= $model->statusDetail; ?></td>
        </tr>
        </tbody>
    </table>
</div>