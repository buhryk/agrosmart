<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\commondata\models\Currency;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'code') ?>
    <?= $form->field($model, 'sign') ?>
    <?= $form->field($model, 'weight')->textInput() ?>
    <?= $form->field($model, 'is_main')->dropDownList(Currency::getAllIsMainProperties())?>
    <?= $form->field($model, 'status')->dropDownList(Currency::getAllStatuses()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>