<?php
$this->title = Yii::t('event', 'Country updating') . ' "' . $model->title . '"';

$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Countries list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('event', 'Country updating')];
?>

<div class="rubric-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>