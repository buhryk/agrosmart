<?php

namespace backend\modules\accesscontrol\models;

use Yii;

class ModuleControllerAction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module_controller_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller_id', 'alias', 'title', 'description'], 'required'],
            [['controller_id'], 'integer'],
            [['description'], 'string'],
            [['alias', 'title'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'controller_id' => 'Controller ID',
            'alias' => 'Alias',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    public function getController()
    {
        return $this->hasOne(ModuleController::className(), ['id' => 'controller_id']);
    }
}