<?php
namespace backend\modules\accesscontrol\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use backend\modules\accesscontrol\models\ModuleControllerAction;
use backend\modules\accesscontrol\models\ModuleController;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class ActionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionCreate()
    {
        $model = new ModuleControllerAction();

        $controllerId = Yii::$app->request->get('controller');
        if (!$controllerId) {
            throw new BadRequestHttpException(Yii::t('common', 'Not all parameters provided') . ' (controller)');
        }

        $controller = ModuleController::findOne($controllerId);
        if (!$controller) {
            throw new NotFoundHttpException(Yii::t('accesscontrol', 'Controller not found'));
        }

        $model->controller_id = $controller->primaryKey;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['controller/view', 'id' => $controller->primaryKey]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'controller' => $controller
            ]);
        }
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = ModuleControllerAction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}