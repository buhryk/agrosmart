<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('accesscontrol', 'Modules list'), 'url' => ['module/index']];
$module = $model->module;
$this->params['breadcrumbs'][] = ['label' => $module->title, 'url' => ['module/view', 'id' => $module->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Module'); ?></th>
            <td>
                <?php $module = $model->module; ?>
                <?php if ($module) { ?>
                    <a href="<?= Url::to(['module/view', 'id' => $module->primaryKey]); ?>">
                        <?= $module->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Controller title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Controller description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Actions list'); ?></th>
            <td>
                <?php $actions = $model->actions; ?>
                <?php if ($actions) { ?>
                    <ul>
                        <?php foreach ($actions as $action) { ?>
                            <li>
                                <a href="<?= Url::to(['action/view', 'id' => $action->primaryKey]); ?>">
                                    <?= $action->title . ' ('.$action->alias.')'; ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <a href="<?= Url::to(['action/create', 'controller' => $model->primaryKey]); ?>"
                   class="btn-sm btn-success without-hover-text-decoration"
                >
                    <?= Yii::t('accesscontrol', 'Add action'); ?>
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>