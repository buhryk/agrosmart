<?php
use yii\helpers\Html;

$this->title = Yii::t('accesscontrol', 'Updating action');
$this->params['breadcrumbs'][] = ['label' => Yii::t('accesscontrol', 'Modules list'), 'url' => ['module/index']];
$controller = $model->controller;
$module = $controller->module;
$this->params['breadcrumbs'][] = ['label' => $module->title, 'url' => ['module/view', 'id' => $module->primaryKey]];
$this->params['breadcrumbs'][] = ['label' => $controller->title, 'url' => ['controller/view', 'id' => $controller->primaryKey]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['action/view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>