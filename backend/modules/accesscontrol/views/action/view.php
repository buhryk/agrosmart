<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('accesscontrol', 'Modules list'), 'url' => ['module/index']];
$controller = $model->controller;
$module = $controller->module;
$this->params['breadcrumbs'][] = ['label' => $module->title, 'url' => ['module/view', 'id' => $module->primaryKey]];
$this->params['breadcrumbs'][] = ['label' => $controller->title, 'url' => ['controller/view', 'id' => $controller->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Controller'); ?></th>
            <td>
                <?php $controller = $model->controller; ?>
                <?php if ($controller) { ?>
                    <a href="<?= Url::to(['controller/view', 'id' => $controller->primaryKey]); ?>">
                        <?= $controller->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Action title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Controller description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        </tbody>
    </table>
</div>