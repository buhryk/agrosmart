<?php
use yii\helpers\Html;

$this->title = Yii::t('accesscontrol', 'Updating role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('accesscontrol', 'Roles list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="role-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>