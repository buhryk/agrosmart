<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('accesscontrol', 'Roles list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Role title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Role description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Rules'); ?></th>
            <td>
                <a href="<?= Url::to(['rules', 'id' => $model->primaryKey]); ?>">
                    <?= Yii::t('common', 'View'); ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Admin menu items'); ?></th>
            <td>
                <a href="<?= Url::to(['admin-menu', 'id' => $model->primaryKey]); ?>">
                    <?= Yii::t('common', 'View'); ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('users', 'Users'); ?></th>
            <td>
                <div style="margin-bottom: 10px;">
                    <?php foreach ($model->users as $user) { ?>
                        <a href="<?= Url::to(['/adminuser/user/view', 'id' => $user->primaryKey]); ?>" class="block">
                            <?= $user->username . ' ('.$user->email.')'; ?>
                        </a>
                    <?php } ?>
                </div>

                <a href="<?= Url::to(['/adminuser/user/create', 'role' => $model->primaryKey]); ?>"
                   class="btn-sm btn-success without-hover-text-decoration"
                >
                    <?= Yii::t('users', 'Add user'); ?>
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>