<?php
use yii\helpers\Html;
use backend\assets\AdminMenuManagingAsset;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
AdminMenuManagingAsset::register($this);

$this->title = Yii::t('accesscontrol', 'Updating role rules');
$this->params['breadcrumbs'][] = ['label' => Yii::t('accesscontrol', 'Roles list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $role->title, 'url' => ['view', 'id' => $role->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="admin-menu-managing">
    <form onsubmit="return false;" data-url="<?= Url::to(['admin-menu', 'id' => $role->primaryKey]); ?>" id="admin-menu-form">
        <?php foreach ($items as $item) { ?>
            <div class="one-menu-chapter">
                <div class="checkbox">
                    <label>
                        <input
                            type="checkbox"
                            name="item[<?= $item->primaryKey; ?>]"
                            data-menu-id="<?= $item->primaryKey; ?>"
                            <?= in_array($item->primaryKey, $currentRoleMenusIds) ? 'checked="checked"' : ''; ?>
                        >
                        <?= $item->title; ?>
                    </label>
                </div>
                <?php $submenus = $item->submenus; ?>
                <?php if ($submenus) { ?>
                    <div style="padding-left: 30px;">
                        <?php foreach ($submenus as $submenu) { ?>
                            <div class="checkbox">
                                <label>
                                    <input
                                        type="checkbox"
                                        name="item[<?= $submenu->primaryKey; ?>]"
                                        data-menu-id="<?= $submenu->primaryKey; ?>"
                                        <?= in_array($submenu->primaryKey, $currentRoleMenusIds) ? 'checked="checked"' : ''; ?>
                                    >
                                    <?= $submenu->title; ?>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>

        <p>&nbsp;</p>

        <div class="form-group">
            <button type="button" class="btn btn-success check-all">
                <span class="glyphicon glyphicon-ok">&nbsp;<?= Yii::t('common', 'Check all'); ?></span>
            </button>
            <button type="button" class="btn btn-danger uncheck-all">
                <span class="glyphicon glyphicon-ok">&nbsp;<?= Yii::t('common', 'Uncheck all'); ?></span>
            </button>

            <?= Html::submitButton(Yii::t('common', 'Save'), [
                'class' => 'btn btn-primary',
                'id' => 'submit-admin-menu-form'
            ]) ?>
        </div>
    </form>
</div>