<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('accesscontrol', 'Modules list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Module title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Controller namespace'); ?></th>
            <td><?= $model->controller_namespace; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Module description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('accesscontrol', 'Controllers list'); ?></th>
            <td>
                <?php $controllers = $model->controllers; ?>
                <?php if ($controllers) { ?>
                    <ul>
                        <?php foreach ($controllers as $controller) { ?>
                            <li>
                                <a href="<?= Url::to(['controller/view', 'id' => $controller->primaryKey]); ?>">
                                    <?= $controller->title . ' ('.$controller->alias.')'; ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <a href="<?= Url::to(['controller/create', 'module' => $model->primaryKey]); ?>"
                    class="btn-sm btn-success without-hover-text-decoration"
                >
                    <?= Yii::t('accesscontrol', 'Add controller'); ?>
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>