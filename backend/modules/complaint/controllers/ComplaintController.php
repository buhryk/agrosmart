<?php
namespace backend\modules\complaint\controllers;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\modules\complaint\models\Complaint;
use backend\modules\complaint\models\ComplaintSearch;
use backend\modules\accesscontrol\AccessControlFilter;

class ComplaintController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ComplaintSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->status == Complaint::STATUS_NEW) {
            $model->status = Complaint::STATUS_VIEWED;
            $model->update();
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);

        $status = Yii::$app->request->get('status');
        if ($status === null) {
            throw new BadRequestHttpException('Not all parameters provided');
        }

        if (!in_array($status, array_keys(Complaint::getAllStatuses()))) {
            throw new BadRequestHttpException('Wrong status value');
        }

        $model->status = $status;
        $model->update();

        return $this->redirect(['view', 'id' => $id]);
    }

    private function findModel($id)
    {
        $model = Complaint::find()
            ->where([Complaint::tableName().'.id' => $id])
            ->joinWith('author')
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('Page not found');
        }

        return $model;
    }
}