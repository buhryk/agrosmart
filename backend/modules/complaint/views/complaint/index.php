<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use frontend\modules\complaint\models\Complaint;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;

$this->title = 'Жалобы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertisement-complaint-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model){
            switch ($model->status) {
                case Complaint::STATUS_NEW:
                    return ['class' => 'danger'];
                case Complaint::STATUS_PROCESSED:
                    return ['class' => 'success'];
                case Complaint::STATUS_VIEWED:
                    return ['class' => 'info'];
                default:
                    return ['class' => 'active'];
            }
        },
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'author_name',
                'label' => Yii::t('common', 'Complaint author'),
                'contentOptions' => ['style'=>'width: 85px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    $author = $model->author;
                    return $author ? Html::a($author->fullName, ['/consumer/user/view', 'id' => $author->id]) : '';
                }
            ],
            [
                'attribute' => 'table_name',
                'label' => Yii::t('common', 'Complaint subject'),
                'contentOptions' => ['style'=>'width: 120px;'],
                'format'=>'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [
                    Complaint::ALIAS_ADVERTISEMENT => 'Объявления',
                    Complaint::ALIAS_COMPANY => 'Компании',
                    Complaint::ALIAS_USER => 'Пользователи'
                ],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>false, 'prompt' => ''],
                ],
                'value' => function ($model) {
                    switch ($model->table_name) {
                        case Complaint::ALIAS_ADVERTISEMENT:
                            return Yii::t('common', 'Advertisement');
                        case Complaint::ALIAS_USER:
                            return Yii::t('common', 'User');
                        case Complaint::ALIAS_COMPANY:
                            return Yii::t('common', 'Company');
                        default:
                            return 'undefined';
                    }
                }
            ],
            [
                'label' => 'Жалоба на',
                'contentOptions' => ['style'=>'width: 85px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    $url = '';
                    $title = '';
                    $entity = null;
                    switch ($model->table_name) {
                        case Complaint::ALIAS_ADVERTISEMENT:
                            $entity = \frontend\modules\product\models\Advertisement::findOne($model->record_id);
                            $title = $entity ? $entity->title : '';
                            $url = Url::to(['/advertisement/advertisement/view', 'id' => $model->record_id]);
                            break;
                        case Complaint::ALIAS_USER:
                            $entity = \frontend\models\User::findOne($model->record_id);
                            $title = $entity ? $entity->fullName : '';
                            $url = Url::to(['/consumer/user/view', 'id' => $model->record_id]);
                            break;
                        case Complaint::ALIAS_COMPANY:
                            $entity = \frontend\models\Company::findOne($model->record_id);
                            $title = $entity ? $entity->name : '';
                            $url = Url::to(['/consumer/company/view', 'id' => $model->record_id]);
                            break;
                        default:
                            break;
                    }
                    return $entity ? Html::a($title, $url) : '';
                }
            ],
            [
                'attribute' => 'types',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Complaint::getAllTypes(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>true],
                ],
                'format'=>'raw',
                'value' => function ($model) {
                    $html = '<ul>';
                    foreach ($model->formattedTypes as $type) {
                        $html .= '<li>' . $type['title'] . '</li>';
                    }
                    return '</ul>' . $html;
                },
                'contentOptions'=>['style'=>'width: 210px;']
            ],
            'text:ntext',
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'status',
                'filter' => Complaint::getAllStatuses(),
                'contentOptions' => ['style'=>'width: 100px;'],
                'value' => 'detailStatus'
            ],
            [
                'contentOptions'=>['style'=>'width: 40px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        );
                }
            ],
        ],
    ]); ?>
</div>