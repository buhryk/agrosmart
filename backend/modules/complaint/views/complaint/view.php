<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\complaint\models\Complaint;

$entity = null;
$title = '';
switch ($model->table_name) {
    case Complaint::ALIAS_ADVERTISEMENT:
        $entity = \frontend\modules\product\models\Advertisement::findOne($model->record_id);
        $title = $entity ? $entity->title : '';
        $url = Url::to(['/advertisement/advertisement/view', 'id' => $model->record_id]);
        break;
    case Complaint::ALIAS_USER:
        $entity = \frontend\models\User::findOne($model->record_id);
        $title = $entity ? $entity->fullName : '';
        $url = Url::to(['/consumer/user/view', 'id' => $model->record_id]);
        break;
    case Complaint::ALIAS_COMPANY:
        $entity = \frontend\models\Company::findOne($model->record_id);
        $title = $entity ? $entity->name : '';
        $url = Url::to(['/consumer/company/view', 'id' => $model->record_id]);
        break;
    default:
        break;
}

$this->title = Yii::t('common', 'Complaint to the', ['item' => $title]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Complaints list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Complaint subject'); ?></th>
            <td>
                <?php if ($entity) { ?>
                    <a href="<?= $url; ?>"><?= $title; ?></a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Complaint author'); ?></th>
            <td>
                <?php $author = $model->author;
                if ($author) {
                    echo Html::a($author->fullName, ['/consumer/user/view', 'id' => $model->author->primaryKey]);
                } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Reason'); ?></th>
            <td>
                <ul>
                    <?php foreach ($model->formattedTypes as $type) { ?>
                        <li><?= $type['title']; ?></li>
                    <?php } ?>
                </ul>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Text'); ?></th>
            <td><?= $model->text; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Date created at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->created_at); ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Status'); ?></th>
            <td><?= $model->detailStatus; ?></td>
        </tr>
        </tbody>
    </table>

    <?php if ($model->status !== Complaint::STATUS_VIEWED) { ?>
        <a href="<?= Url::to(['change-status', 'id' => $model->primaryKey, 'status' => Complaint::STATUS_VIEWED]); ?>"
           class="btn btn-primary"
        >
            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
            Отметить как "<?= Yii::t('common', 'COMPLAINT_STATUS_VIEWED'); ?>"
        </a>
    <?php } ?>

    <?php if ($model->status !== Complaint::STATUS_PROCESSED) { ?>
        <a href="<?= Url::to(['change-status', 'id' => $model->primaryKey, 'status' => Complaint::STATUS_PROCESSED]); ?>"
           class="btn btn-primary"
        >
            <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
            Отметить как "<?= Yii::t('common', 'COMPLAINT_STATUS_PROCESSED'); ?>"
        </a>
    <?php } ?>
</div>