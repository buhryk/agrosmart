<?php

namespace backend\modules\complaint\models;

use backend\modules\news\models\NewsSearch;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\complaint\models\Complaint;

/**
 * ComplaintSearch represents the model behind the search form about `frontend\modules\complaint\models\Complaint`.
 */
class ComplaintSearch extends Complaint
{
    public $author_name;

    public function rules()
    {
        return [
            [['from_user_id', 'record_id', 'status'], 'integer'],
            [['created_at'], 'validateDateTime'],
            [['table_name', 'types', 'text', 'author_name', 'created_at'], 'safe'],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if ($this->$attribute != date('Y-m-d', strtotime($this->$attribute)) ) {
            $this->addError($attribute, Yii::t('common', 'Field must be in format') . ' "2000-00-00"');
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Complaint::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 15]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'from_user_id' => $this->from_user_id,
            'record_id' => $this->record_id,
            'status' => $this->status,
        ]);

        if ($this->author_name) {
            $query->joinWith('author')
                ->andFilterWhere(['and', [
                    'or',
                    ['like', 'name', $this->author_name],
                    ['like', 'surname', $this->author_name]]
                ]);
        }

        if ($this->created_at) {
            $query->andFilterWhere([
                'between',
                'created_at',
                strtotime($this->created_at),
                strtotime($this->created_at) + NewsSearch::ONE_DAY_IN_SECONDS]);
        }

        $query->andFilterWhere(['like', 'table_name', $this->table_name])
            ->andFilterWhere(['like', 'text', $this->text]);


        if ($this->types) {
            $typesConditions = ['or'];
            foreach ($this->types as $one) {
                $typesConditions[] = ['like', 'types', $one];
            }

            $query->andFilterWhere(['and',
                $typesConditions
            ]);
        }

        return $dataProvider;
    }
}