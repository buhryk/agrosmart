<?php
namespace backend\modules\advertisement\controllers;

use Yii;
use frontend\modules\product\models\Advertisement;
use backend\modules\advertisement\models\AdvertisementSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\accesscontrol\AccessControlFilter;
use yii\web\BadRequestHttpException;

class AdvertisementController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new AdvertisementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->admin_status == Advertisement::ADMIN_STATUS_NEW) {
            $model->admin_status = Advertisement::ADMIN_STATUS_VIEWED;
            $model->update();
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);

        $status = Yii::$app->request->get('status');
        if ($status === null) {
            throw new BadRequestHttpException('Not all parameters provided');
        }

        if (!in_array($status, array_keys(Advertisement::getAllStatuses()))) {
            throw new BadRequestHttpException('Wrong status value');
        }

        $model->status = $status;
        $model->update(false);

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Advertisement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}