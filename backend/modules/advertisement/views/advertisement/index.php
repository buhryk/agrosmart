<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use frontend\modules\product\models\Advertisement;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use backend\modules\rubric\models\Rubric;

$this->title = Yii::t('advertisement', 'Products and services');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="advertisement-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model) {
            if ($model->admin_status === Advertisement::ADMIN_STATUS_NEW) {
                return ['class' => 'info'];
            }
        },
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'title',
                'label' => Yii::t('common', 'Title'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['view', 'id' => $model->primaryKey]);
                }
            ],
            [
                'attribute' => 'rubric_id',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Rubric::find()->joinWith('lang')->all(), 'id', 'title'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>true],
                ],
                'contentOptions' => ['style'=>'width: 140px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    $rubric = $model->rubric;
                    return $rubric ? $rubric->title : '';
                }
            ],
            [
                'attribute' => 'type',
                'contentOptions' => ['style'=>'width: 110px;'],
                'filter' => Advertisement::getTypes(),
                'value' => 'detailType'
            ],
            [
                'attribute' => 'company_id',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(\frontend\models\Company::find()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>true],
                ],
                'contentOptions' => ['style'=>'width: 120px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    $company = $model->company;
                    return $company ? Html::a( Html::encode($company->name), ['/consumer/company/view', 'id' => $company->primaryKey]) : '';
                }
            ],
            [
                'attribute' => 'author',
                'label' => Yii::t('advertisement', 'Creator'),
                'contentOptions' => ['style'=>'width: 85px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    $user = $model->author;
                    return $user ? Html::a($user->fullName, ['/consumer/user/view', 'id' => $user->primaryKey]) : '';
                }
            ],
            [
                'attribute' => 'phones',
                'contentOptions' => ['style'=>'width: 100px;'],
            ],
            [
                'attribute' => 'views_count',
                'label' => Yii::t('common', 'Views'),
                'contentOptions' => ['style'=>'width: 50px;']
            ],
            [
                'attribute' => 'created_at',
                'contentOptions' => ['style'=>'width: 155px;'],
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'status',
                'filter' => Advertisement::getAllStatuses(),
                'contentOptions' => ['style'=>'width: 100px;'],
                'value' => 'detailStatus'
            ],
            [
                'attribute' => 'admin_status',
                'filter' => Advertisement::getAllAdminStatuses(),
                'contentOptions' => ['style'=>'width: 100px;'],
                'value' => 'adminStatusDetail'
            ],
            [
                'contentOptions'=>['style'=>'width: 95px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        ).' '.
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Редактировать']
                        );
                }
            ],

        ],
    ]); ?>
</div>