<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\modules\rubric\models\Rubric;
use yii\helpers\ArrayHelper;
use backend\modules\commondata\models\Currency;
use backend\modules\rubric\models\RubricMeasurement;
use kartik\date\DatePicker;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model backend\modules\tariff\models\Tariff */
$searchCityUrl = \yii\helpers\Url::to(['/city-list/index']);
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары и услуги'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$rubricsList = Rubric::find()->andWhere(['can_add_advertisement' => 1])->all();
$currencyList = Currency::find()->andWhere(['status' => Currency::ACTIVE_YES])->all();
$measurementList = RubricMeasurement::find()->all();
?>
<div class="tariff-update">
    <h1><?=$this->title ?></h1>
    <div class="--update-advertisement">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'text')->textarea(); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'rubric_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($rubricsList, 'id', 'title'),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => ['allowClear' => true]
                ])->label('Рубрика'); ?>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'price') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map($currencyList, 'id', 'title')) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'volume') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'measurement_id')->dropDownList(ArrayHelper::map($measurementList, 'id', 'title')) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'status')->dropDownList($model->allStatuses) ?>
                    </div>
                    <div class="col-md-6">
                        <label>Активно до </label>
                        <?php echo DatePicker::widget([
                            'attribute' => 'active_to',
                            'name' => 'Advertisement[active_to]',
                            'type' => DatePicker::TYPE_INPUT,
                            'value' => $model->active_to ? $model->active_to : '',
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'city_id')->widget(Select2::className(), [
                            'initValueText' => !empty($model->city) ? $model->city->title : '', // set the initial display text
                            'options' => ['placeholder' => 'Ведите город'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 2,
                                'ajax' => [
                                    'url' => $searchCityUrl,
                                    'dataType' => 'json',
                                    'data' => new \yii\web\JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                            ],

                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'address')->textInput() ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'phones')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'type')->dropDownList($model->types) ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'email') ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'admin_status')->dropDownList($model->allAdminStatuses) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Save'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
