<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\product\models\Advertisement;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('advertisement', 'Products and services'), 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubric-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <a class="btn btn-primary" href="<?=Url::to(['update', 'id' => $model->id]) ?>">
        Редактировать
    </a>
    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?php echo Yii::t('advertisement', 'advertisement author'); ?></th>
            <td>
                <?php $author = $model->author; ?>
                <?php if ($author) { ?>
                    <a href="<?= Url::to(['/consumer/user/view', 'id' => $author->id]); ?>">
                        <?= $author->fullName; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Company'); ?></th>
            <td>
                <?php $company = $model->company; ?>
                <?php if ($company) { ?>
                    <a href="<?= Url::to(['/company/company/view', 'id' => $company->primaryKey]); ?>">
                        <?= $company->name; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('advertisement', 'Show from name'); ?></th>
            <td>
                <?php if ($model->from_user_id) { ?>
                    <?php $user = \frontend\models\User::findOne($model->from_user_id); ?>
                    <?= $user ? Html::a($user->fullName, ['/consumer/user/view', 'id' => $user->id]) : ''; ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'From subsidiary'); ?></th>
            <td>
                <?php if ($model->from_subsidiary_id) { ?>
                    <?php $subsidiary = \frontend\modules\cabinet\models\Subsidiary::findOne($model->from_subsidiary_id); ?>
                    <?= $subsidiary ? ($subsidiary->getType() . ' - ' . $subsidiary->location) : ''; ?>
                <?php } ?>
            </td>
        </tr>


        <tr>
            <th><?= Yii::t('advertisement', 'Phones'); ?></th>
            <td><?= $model->phones; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Rubric'); ?></th>
            <td>
                <?php $rubric = $model->rubric; ?>
                <?php if ($rubric) { ?>
                    <a href="<?= Url::to(['/rubric/rubric/view', 'id' => $rubric->primaryKey]); ?>">
                        <?= $rubric->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('advertisement', 'Price'); ?></th>
            <td><?= $model->detailPrice; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Address'); ?></th>
            <td>
                <?= $model->location; ?>
            </td>
        </tr>
        <?php if ($model->volume) { ?>
            <tr>
                <th><?= Yii::t('advertisement', 'Volume'); ?></th>
                <td><?= $model->detailVolume; ?></td>
            </tr>
        <?php } ?>
        <tr>
            <th><?= Yii::t('advertisement', 'Advertisement text'); ?></th>
            <td><?= $model->text; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Images'); ?></th>
            <td>
                <div class="admin-advertisement-gallery-container">
                    <?php $images = $model->images; ?>
                    <?php $items = [];
                    foreach ($images as $image) {
                        $items[] = ['src' => '/'.$image['path']];
                    } ?>
                    <?= dosamigos\gallery\Gallery::widget(['items' => $items]);?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('advertisement', 'Active to'); ?></th>
            <td>
                <?php if ($model->active_to) { ?>
                    <?= date('Y-m-d', $model->active_to); ?>
                <?php } else { ?>
                    <?= Yii::t('advertisement', 'until date to'); ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Date created at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->created_at); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('status'); ?></th>
            <td><?= $model->detailStatus; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('admin_status'); ?></th>
            <td><?= $model->adminStatusDetail; ?></td>
        </tr>
        </tbody>
    </table>

    <div>
        <?php if ($model->status !== Advertisement::STATUS_DEACTIVATED_BY_ADMIN) { ?>
            <a href="<?= Url::to([
                'change-status',
                'id' => $model->primaryKey,
                'status' => Advertisement::STATUS_DEACTIVATED_BY_ADMIN
            ]); ?>" class="btn btn-primary">
                <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                <?= Yii::t('common', 'Check as'); ?> "<?= Yii::t('advertisement', 'STATUS_DEACTIVATED_BY_ADMIN'); ?>"
            </a>
        <?php } ?>
        <?php if ($model->status !== Advertisement::STATUS_ACTIVE_YES) { ?>
            <a href="<?= Url::to([
                'change-status',
                'id' => $model->primaryKey,
                'status' => Advertisement::STATUS_ACTIVE_YES
            ]); ?>" class="btn btn-primary">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                <?= Yii::t('common', 'Check as'); ?> "<?= Yii::t('advertisement', 'STATUS_ACTIVE_YES'); ?>"
            </a>
        <?php } ?>
        <?php if ($model->status !== Advertisement::STATUS_ACTIVE_NO) { ?>
            <a href="<?= Url::to([
                'change-status',
                'id' => $model->primaryKey,
                'status' => Advertisement::STATUS_ACTIVE_NO
            ]); ?>" class="btn btn-primary">
                <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                <?= Yii::t('common', 'Check as'); ?> "<?= Yii::t('advertisement', 'STATUS_ACTIVE_NO'); ?>"
            </a>
        <?php } ?>
    </div>
</div>