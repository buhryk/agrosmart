<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\mainservices\models\Block;

/* @var $this yii\web\View */
/* @var $model backend\modules\mainservices\models\Block */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="block-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'title')->textInput(); ?>
    <?= $form->field($model, 'type')->textInput()->dropDownList(Block::getTypesList()) ?>
    <?= $form->field($modelLang, 'url')->textInput(); ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'active')->textInput()->dropDownList(Block::getStatusesList()) ?>
    <?= $form->field($modelLang, 'icon_file')->fileInput(); ?>

    <?php if ($model->image) { ?>
        <?php echo Html::img($model->image, ['style' => 'max-width: 150px;']); ?>
        <p>&nbsp;</p>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>