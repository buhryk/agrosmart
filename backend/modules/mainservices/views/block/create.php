<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\mainservices\models\Block */

$this->title = Yii::t('mainservices', 'Block creating');
$this->params['breadcrumbs'][] = ['label' => Yii::t('mainservices', 'Blocks list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>