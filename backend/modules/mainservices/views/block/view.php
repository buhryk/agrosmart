<?php

use yii\helpers\Html;
use backend\modules\mainservices\models\Block;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('mainservices', 'Blocks list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title
?>
<div class="rubric-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('mainservices', 'Block title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('type'); ?></th>
            <td><?= $model->typeTitle; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('mainservices', 'Block url'); ?></th>
            <td><?= $model->url; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Image'); ?></th>
            <td><?= Html::img($model->image, ['width' => 200]); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('sort'); ?></th>
            <td><?= $model->sort; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('active'); ?></th>
            <td><?= $model->statusTitle; ?></td>
        </tr>
        </tbody>
    </table>
</div>