<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\mainservices\models\Block */

$this->title = Yii::t('mainservices', 'Block editing');
$this->params['breadcrumbs'][] = ['label' => Yii::t('mainservices', 'Blocks list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>