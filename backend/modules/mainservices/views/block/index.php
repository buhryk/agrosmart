<?php
use yii\helpers\Url;

$this->title = Yii::t('mainservices', 'Blocks list');
?>

<div class="rubric-default-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['create']) ?>"
           class="btn btn-primary block right">
            <?= Yii::t('mainservices', 'Add block'); ?>
        </a>
    </h1>

    <table class="table table-bordered" style="margin-top: 15px;">
        <thead>
        <tr>
            <th width="60">ID</th>
            <th><?= Yii::t('mainservices', 'Block title'); ?></th>
            <th><?= Yii::t('mainservices', 'Block type'); ?></th>
            <th><?= Yii::t('common', 'Image'); ?></th>
            <th><?= Yii::t('mainservices', 'Block url'); ?></th>
            <th><?= Yii::t('mainservices', 'Block sort'); ?></th>
            <th><?= Yii::t('mainservices', 'Block active'); ?></th>
            <th width="130">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($dataProvider->models as $model) { ?>
            <tr>
                <td><?= $model->primaryKey; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"><?= $model->title; ?></a>
                </td>
                <td><?= $model->typeTitle; ?></td>
                <td>
                    <?php $image = $model->image;
                    if ($image) {
                        echo \yii\helpers\Html::img($image, ['width' => 150]);
                    } ?>
                </td>
                <td><?= $model->url; ?></td>
                <td><?= $model->sort; ?></td>
                <td><?= $model->statusTitle; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'View'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                    <a href="<?= Url::to(['update', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Edit'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="<?= Url::to(['delete', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Delete'); ?>"
                       class="btn btn-default btn-sm"
                       onclick="return confirm('<?= Yii::t('common', 'Are you sure you want to delete this item?'); ?>')"
                    >
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
