<?php

namespace backend\modules\mainservices\controllers;

use backend\modules\mainservices\models\Block;
use backend\modules\mainservices\models\BlockLang;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use backend\modules\accesscontrol\AccessControlFilter;

class BlockController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Block::find()->orderBy('sort ASC'),
            'pagination' => false
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Block();
        $modelLang = new BlockLang();
        $modelLang->record_id = 0;
        $modelLang->lang = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            $modelLang->icon_file = UploadedFile::getInstance($modelLang, 'icon_file');

            if ($modelLang->icon_file) {
                $img_dir = Yii::getAlias('@frontend') . '/web/uploads/mainservices';
                if (!\file_exists($img_dir)) {
                    if (!\mkdir($img_dir, 0777)) {
                        throw new NotAcceptableHttpException('The requested content does not exist.');
                    }
                }

                $fileName = time().rand(0,100).'.'.$modelLang->icon_file->extension;
                $filePath = $img_dir.'/'.$fileName;
                if ($modelLang->icon_file->saveAs($filePath)) {
                    $modelLang->image = 'uploads/mainservices/'.$fileName;
                }

                $modelLang->icon_file = null;
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->record_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new BlockLang();
            $modelLang->lang = Yii::$app->language;
            $modelLang->record_id = $model->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            $modelLang->icon_file = UploadedFile::getInstance($modelLang, 'icon_file');

            if ($modelLang->icon_file) {
                $img_dir = Yii::getAlias('@frontend') . '/web/uploads/mainservices';
                if (!\file_exists($img_dir)) {
                    if (!\mkdir($img_dir, 0777)) {
                        throw new NotAcceptableHttpException('The requested content does not exist.');
                    }
                }

                $fileName = time().rand(0,100).'.'.$modelLang->icon_file->extension;
                $filePath = $img_dir.'/'.$fileName;
                if ($modelLang->icon_file->saveAs($filePath)) {
                    $modelLang->image = 'uploads/mainservices/'.$fileName;
                }

                $modelLang->icon_file = null;
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Block::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}