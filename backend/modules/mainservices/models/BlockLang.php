<?php

namespace backend\modules\mainservices\models;

use Yii;

class BlockLang extends \yii\db\ActiveRecord
{
    public $icon_file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_services_widget_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'title', 'url'], 'required'],
            [['record_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title', 'url'], 'string', 'max' => 128],
            [['image'], 'string', 'max' => 255],
            ['icon_file', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024*1024*2],
            ['record_id', 'exist', 'skipOnError' => true, 'targetClass' => Block::className(),
                'targetAttribute' => ['record_id' => 'id'], 'on' => 'update'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'title' => Yii::t('mainservices', 'Block title'),
            'url' => Yii::t('mainservices', 'Block url'),
            'image' => Yii::t('common', 'Image'),
            'icon_file' => Yii::t('common', 'Image')
        ];
    }

    public function getBlock()
    {
        return $this->hasOne(Block::className(), ['id' => 'record_id']);
    }
}