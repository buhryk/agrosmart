<?php

namespace backend\modules\mainservices\models;

use Yii;

class Block extends \yii\db\ActiveRecord
{
    const ACTIVE_YES = 1;
    const ACTIVE_NO = 0;
    const TYPE_CHAPTER = 1;
    const TYPE_BANNER = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_services_widget';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort', 'active', 'type'], 'integer'],
            ['sort', 'default', 'value' => 1],
            ['active', 'default', 'value' => self::ACTIVE_YES],
            ['type', 'default', 'value' => self::TYPE_BANNER],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sort' => Yii::t('mainservices', 'Block sort'),
            'active' => Yii::t('mainservices', 'Block active'),
            'type' => Yii::t('mainservices', 'Block type'),
        ];
    }

    public function getLang()
    {
        return $this->hasOne(BlockLang::className(), ['record_id' => 'id'])
            ->where(['lang' => Yii::$app->language]);
    }

    public function getUrl()
    {
        return $this->lang ? $this->lang->url : '';
    }

    public function getImage()
    {
        return $this->lang ? $this->lang->image : '';
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public static function getTypesList()
    {
        return [
            self::TYPE_CHAPTER => Yii::t('mainservices', 'TYPE_CHAPTER'),
            self::TYPE_BANNER => Yii::t('mainservices', 'TYPE_BANNER'),
        ];
    }

    public static function getStatusesList()
    {
        return [
            self::ACTIVE_YES => Yii::t('common', 'Yes'),
            self::ACTIVE_NO => Yii::t('common', 'No')
        ];
    }

    public function getTypeTitle()
    {
        foreach (self::getTypesList() as $typeKey => $type){
            if ($this->type == $typeKey) {
                return $type;
            }
        }

        return 'undefined';
    }

    public function getStatusTitle()
    {
        foreach (self::getStatusesList() as $typeKey => $type){
            if ($this->type == $typeKey) {
                return $type;
            }
        }

        return 'undefined';
    }
}