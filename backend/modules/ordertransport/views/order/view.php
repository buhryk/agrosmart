<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\ordertransport\models\Order;

$this->title = Yii::t('instruments', 'Transport order') . ' #' . $model->primaryKey;
$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Orders list'), 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubric-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Complaint author'); ?></th>
            <td>
                <a href="<?= Url::to(['/consumer/user/view', 'id' => $model->author->primaryKey]); ?>">
                    <?= $model->author->fullName; ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('from_location'); ?></th>
            <td><?= $model->from_location; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('to_location'); ?></th>
            <td><?= $model->to_location; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('transport_type'); ?></th>
            <td><?= $model->detailTransportType; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('message'); ?></th>
            <td><?= $model->message; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('created_at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->created_at); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('updated_at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->updated_at); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('status'); ?></th>
            <td><?= $model->detailStatus; ?></td>
        </tr>
        </tbody>
    </table>

    <?php if ($model->status !== Order::STATUS_VIEWED) { ?>
        <a href="<?= Url::to(['change-status', 'id' => $model->primaryKey, 'status' => Order::STATUS_VIEWED]); ?>"
           class="btn btn-primary"
        >
            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
            Отметить как "<?= Yii::t('common', 'COMPLAINT_STATUS_VIEWED'); ?>"
        </a>
    <?php } ?>

    <?php if ($model->status !== Order::STATUS_PROCESSED) { ?>
        <a href="<?= Url::to(['change-status', 'id' => $model->primaryKey, 'status' => Order::STATUS_PROCESSED]); ?>"
           class="btn btn-primary"
        >
            <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
            Отметить как "<?= Yii::t('common', 'COMPLAINT_STATUS_PROCESSED'); ?>"
        </a>
    <?php } ?>
</div>