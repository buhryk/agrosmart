<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use dosamigos\datepicker\DatePicker;
use frontend\modules\ordertransport\models\Order;

$this->title = Yii::t('instruments', 'Orders list');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="advertisement-complaint-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model){
            switch ($model->status) {
                case Order::STATUS_NEW:
                    return ['class' => 'danger'];
                case Order::STATUS_PROCESSED:
                    return ['class' => 'success'];
                case Order::STATUS_VIEWED:
                    return ['class' => 'info'];
                default:
                    return ['class' => 'active'];
            }
        },
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            [
                'attribute' => 'author_name',
                'label' => Yii::t('instruments', 'Order author'),
                'contentOptions' => ['style'=>'width: 85px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    $author = $model->author;
                    return $author ? Html::a($author->fullName, ['/consumer/user/view', 'id' => $author->id]) : '';
                }
            ],
            'from_location',
            'to_location',
            [
                'attribute' => 'transport_type',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Order::getTransportTypesList(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>false, 'prompt' => '...'],
                ],
                'value' => function ($model) {
                    return $model->detailTransportType;
                },
                'contentOptions'=>['style'=>'width: 210px;']
            ],
            'message:ntext',
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'status',
                'filter' => Order::getAllStatuses(),
                'contentOptions' => ['style'=>'width: 100px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return $model->detailStatus;
                }
            ],
            [
                'contentOptions'=>['style'=>'width: 90px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>
</div>