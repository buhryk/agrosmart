<?php

namespace backend\modules\ordertransport\models;

use backend\modules\news\models\NewsSearch;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\ordertransport\models\Order;

/**
 * OrderSearch represents the model behind the search form about `frontend\modules\ordertransport\models\Order`.
 */
class OrderSearch extends Order
{
    public $author_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'transport_type', 'status'], 'integer'],
            [['from_location', 'to_location', 'message', 'author_name', 'created_at'], 'safe'],
            ['created_at', 'validateDateTime'],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if ($this->$attribute != date('Y-m-d', strtotime($this->$attribute)) ) {
            $this->addError($attribute, Yii::t('common', 'Field must be in format') . ' "2000-00-00"');
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 20]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'transport_type' => $this->transport_type,
            'status' => $this->status,
        ]);

        if ($this->author_name) {
            $query->joinWith('author')
                ->andFilterWhere(['and', [
                    'or',
                    ['like', 'name', $this->author_name],
                    ['like', 'surname', $this->author_name]]
                ]);
        }

        if ($this->created_at) {
            $query->andFilterWhere([
                'between',
                'created_at',
                strtotime($this->created_at),
                strtotime($this->created_at) + NewsSearch::ONE_DAY_IN_SECONDS]
            );
        }

        $query->orderBy('id DESC');

        $query->andFilterWhere(['like', 'from_location', $this->from_location])
            ->andFilterWhere(['like', 'to_location', $this->to_location])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}