<?php

namespace backend\modules\ordertransport\controllers;

use Yii;
use frontend\modules\ordertransport\models\Order;
use backend\modules\ordertransport\models\OrderSearch;
use backend\modules\accesscontrol\AccessControlFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->status == Order::STATUS_NEW) {
            $model->status = Order::STATUS_VIEWED;
            $model->update();
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);

        $status = Yii::$app->request->get('status');
        if ($status === null) {
            throw new BadRequestHttpException('Not all parameters provided');
        }

        if (!in_array($status, array_keys(Order::getAllStatuses()))) {
            throw new BadRequestHttpException('Wrong status value');
        }

        $model->status = $status;
        $model->update();

        return $this->redirect(['view', 'id' => $id]);
    }
}