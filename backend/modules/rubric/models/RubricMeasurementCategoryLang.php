<?php

namespace backend\modules\rubric\models;

use Yii;

/**
 * This is the model class for table "rubric_measurement_category_lang".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $lang
 */
class RubricMeasurementCategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubric_measurement_category_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'title', 'lang'], 'required'],
            [['category_id'], 'integer'],
            [['title'], 'string', 'max' => 128],
            [['lang'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => Yii::t('rubric', 'Category'),
            'title' => Yii::t('rubric', 'Title'),
            'lang' => Yii::t('common', 'Lang'),
        ];
    }
}