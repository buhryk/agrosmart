<?php

namespace backend\modules\rubric\models;

use Yii;

/**
 * This is the model class for table "rubric_measurement_lang".
 *
 * @property integer $id
 * @property integer $measurement_id
 * @property string $short_title
 * @property string $title
 * @property string $lang
 */
class RubricMeasurementLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubric_measurement_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['measurement_id', 'short_title', 'title', 'lang'], 'required'],
            [['measurement_id'], 'integer'],
            [['short_title', 'title'], 'string', 'max' => 128],
            [['lang'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'measurement_id' => Yii::t('rubric', 'Measurement'),
            'short_title' => Yii::t('rubric', 'Short title'),
            'title' => Yii::t('rubric', 'Title'),
            'lang' => Yii::t('common', 'Lang'),
        ];
    }
}