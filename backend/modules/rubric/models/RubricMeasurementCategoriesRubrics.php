<?php

namespace backend\modules\rubric\models;

use Yii;

/**
 * This is the model class for table "rubric_measurement_categories_rubrics".
 *
 * @property integer $rubric_id
 * @property integer $category_id
 */
class RubricMeasurementCategoriesRubrics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubric_measurement_categories_rubrics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rubric_id', 'category_id'], 'required'],
            [['rubric_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rubric_id' => Yii::t('rubric', 'Rubric'),
            'category_id' => Yii::t('rubric', 'Category'),
        ];
    }
    


}