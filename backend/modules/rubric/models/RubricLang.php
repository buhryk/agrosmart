<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 02.12.16
 * Time: 15:07
 */

namespace backend\modules\rubric\models;

use Yii;

/**
 * This is the model class for table "rubric_model_lang".
 *
 * @property integer $id
 * @property integer $rubric_id
 * @property string $title
 * @property string $lang
 */
class RubricLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rubric_model_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rubric_id', 'title', 'lang'], 'required'],
            [['rubric_id'], 'integer'],
            [['title'], 'string', 'max' => 128],
            [['lang'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rubric_id' => Yii::t('rubric', 'Rubric'),
            'title' => Yii::t('rubric', 'Title'),
            'lang' => Yii::t('common', 'Lang'),
        ];
    }
}