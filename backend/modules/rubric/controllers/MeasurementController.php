<?php

namespace backend\modules\rubric\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\rubric\models\RubricMeasurement;
use backend\modules\rubric\models\RubricMeasurementLang;
use backend\modules\rubric\models\RubricMeasurementCategory;

class MeasurementController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionCreate()
    {
        $categoryId = Yii::$app->request->get('category');
        if (!$categoryId) {
            throw new BadRequestHttpException(Yii::t('common', 'Not all parameters provided') . ' (category).');
        }

        $category = RubricMeasurementCategory::findOne($categoryId);
        if (!$category) {
            throw new NotFoundHttpException('Category not found.');
        }

        $model = new RubricMeasurement();
        $model->category_id = $category->primaryKey;
        $modelLang = new RubricMeasurementLang();
        $modelLang->lang = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            $modelLang->measurement_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->measurement_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang,
            'category' => $category
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findMeasurement($id)
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findMeasurement($id);

        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new RubricMeasurementLang();
            $modelLang->measurement_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findMeasurement($id);
        $categoryId = $model->category_id;
        $model->delete();

        return $this->redirect(['measurement-category/view', 'id' => $categoryId]);
    }

    public function findCategory($id)
    {
        $model = RubricMeasurementCategory::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('rubric', 'Measurement not found'));
        }

        return $model;
    }

    public function findMeasurement($id)
    {
        $model = RubricMeasurement::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Measurement not found');
        }

        return $model;
    }
}
