<?php

namespace backend\modules\rubric\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\rubric\models\RubricMeasurement;
use backend\modules\rubric\models\RubricMeasurementLang;
use backend\modules\rubric\models\RubricMeasurementCategory;
use backend\modules\rubric\models\RubricMeasurementCategoryLang;

class MeasurementCategoryController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RubricMeasurementCategory::find()
        ]);

        return $this->render('index', [
            'data' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new RubricMeasurementCategory();
        $modelLang = new RubricMeasurementCategoryLang();
        $modelLang->lang = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            $modelLang->category_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->category_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findCategory($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findCategory($id);

        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new RubricMeasurementCategoryLang();
            $modelLang->category_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findCategory($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    public function findCategory($id)
    {
        $model = RubricMeasurementCategory::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('rubric', 'Measurement not found'));
        }

        return $model;
    }
}
