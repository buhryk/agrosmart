<?php
namespace backend\modules\rubric\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\accesscontrol\models\Module;
use backend\modules\accesscontrol\models\ModuleController;
use backend\modules\accesscontrol\models\ModuleControllerAction;
use backend\modules\accesscontrol\models\Role;
use backend\modules\rubric\models\RubricMeasurement;
use backend\modules\rubric\models\RubricMeasurementCategoryLang;
use backend\modules\rubric\models\RubricMeasurementLang;
use backend\modules\seo\models\Seo;
use backend\modules\seo\models\SeoLang;
use Yii;
use backend\modules\rubric\models\RubricMeasurementCategoriesRubrics;
use backend\modules\rubric\models\RubricMeasurementCategory;
use backend\modules\rubric\models\Rubric;
use backend\modules\rubric\models\RubricLang;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use backend\modules\transliterator\services\TransliteratorService;
use yii\web\NotAcceptableHttpException;
use yii\web\UploadedFile;

class RubricController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $parentId = Yii::$app->request->get('parent');

        if ($parentId !== null) {
            $parentId = (int) $parentId;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Rubric::find()
                ->where(['parent_id' => $parentId]),
        ]);

        $parent = $parentId ? Rubric::findOne($parentId) : null;

        return $this->render('index', [
            'data' => $dataProvider,
            'parent' => $parent
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findRubric($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Rubric();
        $model->parent_id = Yii::$app->request->get('parent');
        $parent = $model->parent_id ? $this->findRubric($model->parent_id) : null;

        $modelLang = new RubricLang();
        $modelLang->lang = Yii::$app->language;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $model->icon_file = UploadedFile::getInstance($model, 'icon_file');

            if ($model->icon_file) {
                $img_dir = Yii::getAlias('@frontend') . '/web/uploads/rubric';
                if (!\file_exists($img_dir)) {
                    if (!\mkdir($img_dir, 0777)) {
                        throw new NotAcceptableHttpException('The requested content does not exist.');
                    }
                }

                $fileName = time().rand(0,100).'.'.$model->icon_file->extension;
                $filePath = $img_dir.'/'.$fileName;
                if ($model->icon_file->saveAs($filePath)) {
                    $model->icon = 'uploads/rubric/'.$fileName;
                }

                $model->icon_file = null;
            }

            $modelLang->rubric_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->rubric_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang,
            'parent' => $parent
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findRubric($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new RubricLang();
            $modelLang->rubric_id = $model->primaryKey;
            $modelLang->lang = Yii::$app->language;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                if ($modelLang->lang == 'ru') {
                    $model->alias = TransliteratorService::transliterate($modelLang->title);
                } else {
                    $modelLangForAlias = RubricLang::find()
                        ->where(['rubric_id' => $model->primaryKey, 'lang' => 'ru'])
                        ->one();

                    if (!$modelLangForAlias) {
                        $modelLangForAlias = RubricLang::find()
                            ->where(['rubric_id' => $model->primaryKey])
                            ->one();
                    }

                    if ($modelLangForAlias) {
                        $model->alias = TransliteratorService::transliterate($modelLangForAlias->title);
                    }
                }
            }

            $model->icon_file = UploadedFile::getInstance($model, 'icon_file');

            if ($model->icon_file) {
                $img_dir = Yii::getAlias('@frontend') . '/web/uploads/rubric';
                if (!\file_exists($img_dir)) {
                    if (!\mkdir($img_dir, 0777)) {
                        throw new NotAcceptableHttpException('The requested content does not exist.');
                    }
                }

                $fileName = time().rand(0,100).'.'.$model->icon_file->extension;
                $filePath = $img_dir.'/'.$fileName;
                if ($model->icon_file->saveAs($filePath)) {
                    $model->icon = 'uploads/rubric/'.$fileName;
                }

                $model->icon_file = null;
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionSeo($id)
    {
        $model = $this->findRubric($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
            $seo->save();
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->seo_id = $seo->primaryKey;
            $seoLang->lang = Yii::$app->language;
            $seoLang->save();
        }

        if ($seoLang->load(Yii::$app->request->post()) && $seoLang->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findRubric($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionAddMeasureCategory()
    {
        $rubricId = Yii::$app->request->get('rubric');

        if (!$rubricId) {
            throw new BadRequestHttpException(Yii::t('common', 'Not all parameters provided') .' (rubric).');
        }

        $rubricId = (int)$rubricId;

        $rubric = $this->findRubric($rubricId);

        $choosenMeasurementCategories = $rubric->measurementCategories;
        $notChoosenMeasurementCategories = RubricMeasurementCategory::find()
            ->where(['not in', 'id', ArrayHelper::getColumn($choosenMeasurementCategories, 'id')])
            ->all();

        $notChoosen = array();
        foreach ($notChoosenMeasurementCategories as $notChoosenMeasurementCategory) {
            $notChoosen[] = array(
                'id' => $notChoosenMeasurementCategory->primaryKey,
                'title' => $notChoosenMeasurementCategory->title
            );
        }

        $model = new RubricMeasurementCategoriesRubrics();
        $model->rubric_id = $rubric->primaryKey;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $rubric->primaryKey]);
        }

        return $this->render('add_rubric_measurement_category', [
            'rubric' => $rubric,
            'choosen' => $choosenMeasurementCategories,
            'notChoosen' => $notChoosen,
            'model' => $model
        ]);
    }

    public function actionRemoveMeasurementCategory()
    {
        $categoryId = (int)Yii::$app->request->get('category');
        $rubricId = (int)Yii::$app->request->get('rubric');

        if (!$categoryId || !$rubricId) {
            throw new BadRequestHttpException(Yii::t('common', 'Not all parameters provided') .' (category, rubric).');
        }

        $rubric = $this->findRubric($rubricId);
        $category = $this->findCategory($categoryId);

        RubricMeasurementCategoriesRubrics::deleteAll([
            'category_id' => $category->primaryKey,
            'rubric_id' => $rubricId
        ]);

        return $this->render('view', [
            'model' => $rubric
        ]);
    }

    public function findRubric($id)
    {
        $rubric = Rubric::findOne($id);

        if (!$rubric) {
            throw new NotFoundHttpException('Rubric not found');
        }

        return $rubric;
    }

    public function findCategory($id)
    {
        $category = RubricMeasurementCategory::findOne($id);

        if (!$category) {
            throw new NotFoundHttpException('Category not found.');
        }

        return $category;
    }

    public function actionRemoveIcon($id)
    {
        $model = $this->findRubric($id);
        $model->icon = null;

        if ($model->update(false)) {
            Yii::$app->session->setFlash('success', 'Removing icon successfull');
        } else {
            Yii::$app->session->setFlash('error', 'Something went wrong. Please, try this after reloading page.');
        }

        return $this->redirect(['update', 'id' => $id]);
    }
}
