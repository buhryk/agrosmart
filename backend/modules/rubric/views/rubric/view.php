<?php

use yii\helpers\Html;
use backend\modules\rubric\models\Rubric;
use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rubric', 'Rubric list'), 'url' => ['index']];
$parents = [];
$parent = $model->parent;
while ($parent !== null) {
    array_unshift($parents, ['label' => $parent->title, 'url' => ['view', 'id' => $parent->primaryKey]]);
    $parent = $parent->parent;
}

foreach ($parents as $parent) {
    $this->params['breadcrumbs'][] = $parent;
}

$this->params['breadcrumbs'][] = $this->title
?>
<div class="rubric-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Parent rubric'); ?></th>
            <td>
                <?php $parent = $model->parent; ?>
                <?php if ($parent) { ?>
                    <a href="<?= Url::to(['view', 'id' => $parent->primaryKey]) ?>">
                        <?= $parent->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Subrubrics'); ?></th>
            <td>
                <?php $subrubrics = $model->subrubrics; ?>
                <?php if ($subrubrics) { ?>
                    <ol>
                        <?php foreach ($subrubrics as $subrubric) { ?>
                            <li>
                                <a href="<?= Url::to(['view', 'id' => $subrubric->primaryKey]); ?>">
                                    <?= $subrubric->title; ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ol>
                <?php } ?>

                <a href="<?= Url::to(['create', 'parent' => $model->primaryKey]); ?>"
                   class="btn-sm btn-success without-hover-text-decoration"
                >
                    <?= Yii::t('rubric', 'Add subrubric'); ?>
                </a>
            </td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Sort'); ?></th>
            <td><?= $model->sort; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Can add advertisement'); ?></th>
            <td>
                <?= $model->can_add_advertisement == Rubric::CAN_ADD_ADVERTISEMENT_YES ?
                    Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Can add prices'); ?></th>
            <td>
                <?= $model->can_add_prices == Rubric::CAN_ADD_PRICES_YES ?
                    Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?>
            </td>
        </tr>
        <tr>
            <th>Выводить на карте</th>
            <td>
                <?= $model->show_map == Rubric::SHOW_MAP_YES ?
                    Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Icon'); ?></th>
            <td>
                <?php if ($model->icon) { ?>
                    <?php echo Html::img($model->icon, ['style' => 'max-width: 150px;']); ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Active'); ?></th>
            <td><?= $model->active == Rubric::ACTIVE_YES ? Yii::t('common', 'Yes') : Yii::t('common', 'NO'); ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Units of measurement'); ?></th>
            <td>
                <div class="col-md-10">
                    <?php $measurementCategories = $model->measurementCategories; ?>

                    <?php if ($measurementCategories) { ?>
                        <table id="w0" class="table table-striped table-bordered detail-view">
                            <tbody>
                            <?php foreach ($measurementCategories as $measurementCategory) { ?>
                                <tr>
                                    <th width="200px">
                                        <a href="<?= Url::to([
                                                'measurement/view-category',
                                            'id' => $measurementCategory->primaryKey
                                        ]); ?>">
                                            <?= $measurementCategory->title; ?>
                                        </a>
                                    </th>
                                    <td>
                                        <?php $measurements = $measurementCategory->measurements; ?>
                                        <?php foreach ($measurements as $key => $measurement) { ?>
                                            <?= $measurement->title . ' (' . $measurement->short_title .')' .
                                            ($key != (count($measurements) -1) ? ', ' : ''); ?>
                                        <?php } ?>
                                        <a href="<?= Url::to([
                                            'remove-measurement-category',
                                            'rubric' => $model->primaryKey,
                                            'category' => $measurementCategory->primaryKey
                                        ]); ?>"
                                           title="Remove rubric measure category"
                                           class="btn btn-default btn-sm block right" style="margin-right: 5px;"
                                           onclick="return confirm('<?= Yii::t('rubric', 'Are you sure you want to remove measurement from category?'); ?>')"
                                        >
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
                <div class="col-md-2">
                    <a href="<?= Url::to(['add-measure-category', 'rubric' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('rubric', 'Add measure category to rubric'); ?>"
                       class="btn btn-default btn-sm block right"
                    >
                        <span class="glyphicon glyphicon-plus-sign"></span>
                    </a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>