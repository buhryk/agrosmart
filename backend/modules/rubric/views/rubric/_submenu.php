<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;

?>
<ul class="nav nav-tabs">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
            <?= Yii::t('common', 'Update'); ?>
        </a>
    </li>
    <li <?= ($action === 'seo') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['seo', 'id' => $model->primaryKey]) ?>">
            SEO
        </a>
    </li>
</ul>
<br>
