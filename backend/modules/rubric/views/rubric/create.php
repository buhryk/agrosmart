<?php

use yii\helpers\Html;

$this->title = Yii::t('rubric', 'Creating rubric');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rubric', 'Rubric list'), 'url' => ['index']];
if ($parent) {
    $parents = [];
    while ($parent !== null) {
        array_unshift($parents, ['label' => $parent->title, 'url' => ['view', 'id' => $parent->primaryKey]]);
        $parent = $parent->parent;
    }

    foreach ($parents as $parent) {
        $this->params['breadcrumbs'][] = $parent;
    }
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubric-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>