<?php

use yii\helpers\Html;

$this->title = Yii::t('rubric', 'Updating rubric');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rubric', 'Rubric list'), 'url' => ['index']];
$parents = [];
$parent = $model->parent;
while ($parent !== null) {
    array_unshift($parents, ['label' => $parent->title, 'url' => ['view', 'id' => $parent->primaryKey]]);
    $parent = $parent->parent;
}

foreach ($parents as $parent) {
    $this->params['breadcrumbs'][] = $parent;
}
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubric-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_submenu', [
        'model' => $model,
        'modelLang' => $modelLang
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>