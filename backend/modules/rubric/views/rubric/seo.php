<?php
use backend\widgets\SeoWidget;

$this->title = 'Seo рубрики';
$this->params['breadcrumbs'][] = ['label' => Yii::t('rubric', 'Rubric list'), 'url' => ['index']];
$parents = [];
$parent = $model->parent;
while ($parent !== null) {
    array_unshift($parents, ['label' => $parent->title, 'url' => ['view', 'id' => $parent->primaryKey]]);
    $parent = $parent->parent;
}

foreach ($parents as $parent) {
    $this->params['breadcrumbs'][] = $parent;
}
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= SeoWidget::widget([
        'model' => $seo,
        'parameters' => ['modelLang' => $seoLang]
    ]); ?>
</div>