<?php
use yii\helpers\Url;
use backend\modules\rubric\models\Rubric;

$this->title = Yii::t('rubric', 'Rubric list');
?>

<div class="rubric-default-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= !$parent ? Url::to(['create']) : Url::to(['create', 'parent' => $parent->primaryKey]); ?>"
           class="btn btn-primary block right">
            <?= Yii::t('rubric', 'Add rubric'); ?>
        </a>
    </h1>

    <?php if ($parent) { ?>
        <?= Yii::t('rubric', 'Parent category'); ?>:
        <a href="<?= Url::to(['view', 'id' => $parent->primaryKey]); ?>">
            <?= $parent->title; ?>
        </a>
    <?php } ?>

    <table class="table table-bordered" style="margin-top: 15px;">
        <thead>
        <tr>
            <th width="60">ID</th>
            <th><?= Yii::t('rubric', 'Title'); ?></th>
            <th><?= Yii::t('rubric', 'Subrubrics'); ?></th>
            <th width="250">Alias</th>
            <th width="60"><?= Yii::t('rubric', 'Sort'); ?></th>
            <th width="60"><?= Yii::t('rubric', 'Can add advertisement'); ?></th>
            <th width="60"><?= Yii::t('rubric', 'Can add prices'); ?></th>
            <th width="60"><?= Yii::t('rubric', 'Active'); ?></th>
            <th width="130"><?= Yii::t('common', 'Actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $model) { ?>
            <tr>
                <td><?= $model->primaryKey; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>">
                        <?= $model->title; ?>
                    </a>
                </td>
                <td>
                    <?php $subrubrics = $model->subrubrics;
                    if ($subrubrics) { ?>
                        <a href="<?= Url::to(['index', 'parent' => $model->primaryKey]); ?>">
                            <?= Yii::t('rubric', 'Subrubrics'); ?> (<?= count($subrubrics); ?>)
                        </a>
                    <?php } ?>
                    <ul>
                        <?php foreach ($subrubrics as $subrubric) { ?>
                            <li>
                                <a href="<?= Url::to(['view', 'id' => $subrubric->primaryKey]); ?>" class="block">
                                    <?= $subrubric->title; ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </td>
                <td><?= $model->alias; ?></td>
                <td><?= $model->sort; ?></td>
                <td>
                    <?= $model->can_add_advertisement == Rubric::CAN_ADD_ADVERTISEMENT_YES ?
                        Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?>
                </td>
                <td>
                    <?= $model->can_add_prices == Rubric::CAN_ADD_PRICES_YES ?
                        Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?>
                </td>
                <td><?= $model->active == Rubric::ACTIVE_YES ? Yii::t('common', 'Yes') : Yii::t('common', 'No'); ?></td>
                <td>

                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'View'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                    <a href="<?= Url::to(['update', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Edit'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="<?= Url::to(['delete', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Delete'); ?>"
                       class="btn btn-default btn-sm"
                       onclick="return confirm('<?= Yii::t('common', 'Are you sure you want to delete this item?'); ?>')"
                    >
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
