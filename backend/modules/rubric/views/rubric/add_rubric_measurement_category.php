<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = Yii::t('rubric', 'Add rubric measurement category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rubric', 'Rubric list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $rubric->title, 'url' => ['view', 'id' => $rubric->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
    <?php if ($choosen) { ?>
        <span><?= Yii::t('rubric', 'Choosen measurement categories'); ?>: </span>
        <?php foreach ($choosen as $key => $one) { ?>
            <a href="<?= Url::to(['measurement/view-category', 'id' => $one->primaryKey]); ?>">
                <?= $one->title . ($key != (count($choosen) - 1) ? ', ' : ''); ?>
            </a>
        <?php } ?>
    <?php } ?>
</p>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')
        ->dropDownList(ArrayHelper::map($notChoosen, 'id', 'title'), array('prompt'=>'...'))
        ->label('Measurement category');
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>


