<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\rubric\models\Rubric;
use yii\helpers\Url;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($modelLang, 'title') ?>
    <?= $form->field($model, 'alias'); ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']); ?>

    <?= $form->field($model, 'can_add_advertisement')->dropDownList(array(
        Rubric::CAN_ADD_ADVERTISEMENT_NO => Yii::t('common', 'No'),
        Rubric::CAN_ADD_ADVERTISEMENT_YES => Yii::t('common', 'Yes'),
    ));?>

    <?= $form->field($model, 'can_add_prices')->dropDownList(array(
        Rubric::CAN_ADD_PRICES_NO => Yii::t('common', 'No'),
        Rubric::CAN_ADD_PRICES_YES => Yii::t('common', 'Yes'),
    ));?>

    <?= $form->field($model, 'active')->dropDownList(array(
        Rubric::ACTIVE_YES => Yii::t('common', 'Yes'),
        Rubric::ACTIVE_NO => Yii::t('common', 'No'),
    ));?>

    <?= $form->field($model, 'show_map')->dropDownList(array(
        Rubric::ACTIVE_YES => Yii::t('common', 'Yes'),
        Rubric::ACTIVE_NO => Yii::t('common', 'No'),
    ));?>

    <?= $form->field($model, 'icon_file')->fileInput(); ?>

    <?php if ($model->icon) { ?>
        <?php echo Html::img($model->icon, ['style' => 'max-width: 150px;']); ?>
        <a href="<?= Url::to(['remove-icon', 'id' => $model->primaryKey]); ?>" style="color: red;">
            Remove icon
        </a>
        <p>&nbsp;</p>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>