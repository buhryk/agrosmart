<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::t('rubric', 'Measurement categories list');
?>

<div class="rubric-default-index">
    <h1>
        <?= Html::encode($this->title) ?>
        <a href="<?= Url::to(['create']) ?>" class="btn btn-primary block right">
            <?= Yii::t('rubric', 'Add category'); ?>
        </a>
    </h1>

    <table class="table table-bordered" style="margin-top: 15px;">
        <thead>
        <tr>
            <th width="60">ID</th>
            <th><?= Yii::t('rubric', 'Title'); ?></th>
            <th><?= Yii::t('rubric', 'Measurements'); ?></th>
            <th width="130"><?= Yii::t('common', 'Actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $model) { ?>
            <tr>
                <td><?= $model->primaryKey; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>">
                        <?= $model->title; ?>
                    </a>
                </td>
                <td>
                    <?php $measurements = $model->measurements; ?>
                    <?php foreach ($measurements as $key => $measurement) { ?>
                        <div>
                            <a href="<?= Url::to(['measurement/view', 'id' => $measurement->primaryKey]); ?>">
                                <?= $measurement->title . ' (' . $measurement->short_title . ')'; ?>
                            </a>
                        </div>
                    <?php } ?>
                </td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'View'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                    <a href="<?= Url::to(['update', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Edit'); ?>"
                       class="btn btn-default btn-sm"
                    >
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="<?= Url::to(['delete', 'id' => $model->primaryKey]); ?>"
                       title="<?= Yii::t('common', 'Delete'); ?>"
                       class="btn btn-default btn-sm"
                       onclick="return confirm('<?= Yii::t('common', 'Are you sure you want to delete this item?'); ?>')"
                    >
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
