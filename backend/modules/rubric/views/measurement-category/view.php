<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rubric', 'Measurement categories list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($this->title); ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->id; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th>Alias</th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Measurements'); ?></th>
            <td>

                    <?php $measurements = $model->measurements; ?>
                    <?php foreach ($measurements as $measurement) { ?>
                        <div>
                            <a href="<?= Url::to(['measurement/view', 'id' => $measurement->primaryKey]); ?>">
                                <?= $measurement->title . ' (' . $measurement->short_title . ')' ?>
                            </a>
                        </div>
                    <?php } ?>
                <div style="margin-top: 10px;">
                    <a href="<?= Url::to(['measurement/create', 'category' => $model->primaryKey]); ?>"
                       class="btn-sm btn-success without-hover-text-decoration"
                    >
                        <?= Yii::t('rubric', 'Add measurement'); ?>
                    </a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>