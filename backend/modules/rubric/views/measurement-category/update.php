<?php

use yii\helpers\Html;

$this->title = Yii::t('rubric', 'Updating measurement category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rubric', 'Measurement categories list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rubric-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>