<?php
use yii\helpers\Html;

$this->title = Yii::t('rubric', 'Updating measurement') . ' "' . $model->title . '"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('rubric', 'Measurement categories list'), 'url' => ['measurement-category/index']];
$category = $model->category;
if ($category) {
    $this->params['breadcrumbs'][] = ['label' => $category->title, 'url' => ['measurement-category/view', 'id' => $category->primaryKey]];
}
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>