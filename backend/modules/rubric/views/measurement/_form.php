<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'short_title') ?>
    <?= $form->field($modelLang, 'title') ?>
    <?= $form->field($model, 'weight') ?>
    <?= $form->field($model, 'default')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Save'), [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>