<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rubric', 'Measurement categories list'), 'url' => ['measurement-category/index']];
$category = $model->category;
if ($category) {
    $this->params['breadcrumbs'][] = ['label' => $category->title, 'url' => ['measurement-category/view', 'id' => $category->primaryKey]];
}
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="rubric-view">

    <h1><?= Html::encode($model->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->id; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Category'); ?></th>
            <td>
                <?php if ($category) { ?>
                    <a href="<?= Url::to(['measurement-category/view', 'id' => $category->primaryKey]); ?>">
                        <?= $category->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Short title'); ?></th>
            <td><?= $model->short_title; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Full title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('rubric', 'Weight'); ?></th>
            <td><?= $model->weight; ?></td>
        </tr>
        </tbody>
    </table>
</div>