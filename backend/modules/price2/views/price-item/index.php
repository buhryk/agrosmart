<?php

use backend\modules\price2\models\Price;
use kartik\select2\Select2;
use moonland\phpexcel\Excel;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\price2\models\PriceItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Элементы прайса');

if( $id = Yii::$app->request->get('price') )
{
    $price = \backend\modules\price2\models\Price::findOne(['id' => $id]);
    $this->title .= ' "' . $price->name . '"';
    $this->params['breadcrumbs'][] = ['label' => 'Прайсы', 'url' => ['/price2/price']];
    $this->params['breadcrumbs'][] = ['label' => $price->name, 'url' => ['/price2/price/view?id='.$id]];
}

$this->params['breadcrumbs'][] = $this->title;
$price = Price::find()->all();
?>
<div class="price-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать элемент прайса'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'price_id',
            [
                'attribute' => 'price_id',
                'value' => function ($model) {
                    return $model->price1->name;
                },
                'filter' => \backend\modules\price2\models\PriceItem::getPriceIndex(),
            ],
            'name',
            'price',
//            'column1',
            //'column2',
            //'column3',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
