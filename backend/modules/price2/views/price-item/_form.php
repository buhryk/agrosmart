<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\price2\models\PriceItem */
/* @var $form yii\widgets\ActiveForm */
$price = \backend\modules\price2\models\Price::find()->all();
?>

<div class="price-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'price_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($price, 'id', 'name'),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'column1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'column2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'column3')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
