<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\price2\models\PriceItem */

$this->title = Yii::t('app', 'Создание элемента прайса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Элемент прайса'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
