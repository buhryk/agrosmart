<?php

use backend\modules\price2\models\Price;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\price2\models\PriceColumn */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-column-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'price_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Price::find()->all(), 'id', 'name'),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'name1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name3')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
