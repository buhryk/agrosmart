<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\price2\models\PriceItemColumn */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Столбики прайса');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-column-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать столбик для прайса'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'price_id',
            'name1',
            'name2',
            'name3',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
