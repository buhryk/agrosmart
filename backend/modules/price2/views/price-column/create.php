<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\price2\models\PriceColumn */

$this->title = Yii::t('app', 'Добавление столбиков для прайса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Столбики прайса'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-column-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
