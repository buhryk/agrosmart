<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\price2\models\Price */

$this->title = Yii::t('app', 'Создание прайса');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Прайсы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
