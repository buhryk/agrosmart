<?php

use backend\modules\price2\models\Price;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\price2\models\Price */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Прайсы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
               'attribute' => 'udater_id',
               'value' => function ($model) {
                   return $model->author->username;
               }
           ],
            [
                'attribute' => 'company_id',
                'value' => function ($model) {
                    return $model->company->name;
                }
            ],
            'name',
            'description',
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return ($model->type == 0) ? 'Продажа' : 'Закупка';
                }
            ],
            'position',
            'created_at:date',
            'updated_at:date',
            [
                'attribute' => 'status',
                'filter' => $model::getStatusList(),
                'value' => $model->StatusDetail,
            ],
            [
                'attribute' => 'admin_status',
                'value' => $model->adminStatusDetail,
            ],
        ],
    ]) ?>


</div>
