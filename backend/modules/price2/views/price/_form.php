<?php

use backend\modules\price2\models\Company;
use backend\modules\price2\models\User;
use frontend\modules\cabinet\models\Sale;
use kartik\select2\Select2;
use mihaildev\elfinder\InputFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\price2\models\Price */
/* @var $form yii\widgets\ActiveForm */

$company = Company::find()->all();

?>

<div class="price-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'company_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($company, 'id', 'name'),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <?= $form->field($model, 'type')->dropDownList([Sale::TYPE_PRODAZHA => 'Продажа', Sale::TYPE_ZAKUPKA => 'Закупка'])  ?>

    <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
