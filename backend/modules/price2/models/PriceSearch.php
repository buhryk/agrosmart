<?php

namespace backend\modules\price2\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\price2\models\Price;

/**
 * PriceSearch represents the model behind the search form of `backend\modules\price2\models\Price`.
 */
class PriceSearch extends Price
{
    /**
     * {@inheritdoc}
     */
    const ONE_DAY_IN_SECONDS = 86400;
    public $company;

    public function rules()
    {
        return [
            [['id', 'author_id', 'udater_id', 'type', 'position', 'status', 'admin_status', ], 'integer'],
            [['created_at', 'updated_at'], 'validateDateTime'],
            [['name', 'description', 'company'], 'safe'],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if ($this->$attribute != date('Y-m-d', strtotime($this->$attribute)) ) {
            $this->addError($attribute, Yii::t('common', 'Field must be in format') . ' "2000-00-00"');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Price::find()->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('company');


        // grid filtering conditions
        $query->andFilterWhere([
            'price.id' => $this->id,
            'price.author_id' => $this->author_id,
//            'company_id' => $this->company_id,
            'price.udater_id' => $this->udater_id,
            'price.type' => $this->type,
            'price.position' => $this->position,
            'price.status' => $this->status,
            'price.admin_status' => $this->admin_status,
//            'price.created_at' => $this->created_at,
//            'price.updated_at' => $this->updated_at,
        ]);

        if ($this->created_at) {
            $query->andFilterWhere([
                'between',
                'price.created_at',
                strtotime($this->created_at),
                strtotime($this->created_at) + self::ONE_DAY_IN_SECONDS]);
        }

        if ($this->updated_at) {
            $query->andFilterWhere([
                'between',
                'price.updated_at',
                strtotime($this->updated_at),
                strtotime($this->updated_at) + self::ONE_DAY_IN_SECONDS]);
        }

        $query->andFilterWhere(['like', 'price.name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'company.name', $this->company]);

        return $dataProvider;
    }
}
