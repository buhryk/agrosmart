<?php

namespace backend\modules\price2\models;

use Yii;

/**
 * This is the model class for table "price_item".
 *
 * @property int $id
 * @property int $price_id
 * @property string $name
 * @property double $price
 * @property string $column1
 * @property string $column2
 * @property string $column3
 *
 * @property Price $price0
 */
class PriceItem extends \yii\db\ActiveRecord
{
    const SEARCH_EXACT_MATCH = 1;
    const SEARCH_OCCURRENCE_MATCH = 2;
    const SEARCH_NOT_OCCURRENCE_MATCH = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_id'], 'integer'],
            [['name', 'price', 'price_id'], 'required'],
//            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['column1', 'column2', 'column3'], 'safe'],
            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => Price::className(), 'targetAttribute' => ['price_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_id' => 'Прайс',
            'name' => 'Наименование',
            'price' => 'Цена',
            'column1' => 'Столбик1',
            'column2' => 'Столбик2',
            'column3' => 'Столбик3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice1()
    {
        return $this->hasOne(Price::className(), ['id' => 'price_id']);
    }

    public static function getPriceIndex(){
        $price = Price::find()->all();
        $arr = [];
        foreach ($price as $item){
            $arr[$item->id] = $item->name;
        }
        return $arr;
    }
}
