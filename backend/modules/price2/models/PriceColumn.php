<?php

namespace backend\modules\price2\models;

use Yii;

/**
 * This is the model class for table "price_column".
 *
 * @property int $id
 * @property int $price_id
 * @property string $name1
 * @property string $name2
 * @property string $name3
 *
 * @property Price $price
 */
class PriceColumn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_column';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_id'], 'required'],
            [['price_id'], 'integer'],
            [['name1', 'name2', 'name3'], 'string', 'max' => 255],
            [['price_id'], 'exist', 'skipOnError' => true, 'targetClass' => Price::className(), 'targetAttribute' => ['price_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_id' => 'Прайс',
            'name1' => 'Столбик1',
            'name2' => 'Столбик2',
            'name3' => 'Столбик3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasOne(Price::className(), ['id' => 'price_id']);
    }
}
