<?php

namespace backend\modules\price2\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name
 * @property string $logotype
 * @property int $type
 * @property int $status
 * @property double $ratio
 * @property int $last_activity_date
 * @property int $is_confirmed
 * @property string $short_description
 * @property string $full_description
 * @property int $created_at
 *
 * @property Price[] $prices
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type', 'status', 'last_activity_date', 'is_confirmed', 'created_at'], 'integer'],
            [['ratio'], 'number'],
            [['short_description', 'full_description'], 'string'],
            [['name', 'logotype'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'logotype' => 'Logotype',
            'type' => 'Type',
            'status' => 'Status',
            'ratio' => 'Ratio',
            'last_activity_date' => 'Last Activity Date',
            'is_confirmed' => 'Is Confirmed',
            'short_description' => 'Short Description',
            'full_description' => 'Full Description',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(Price::className(), ['company_id' => 'id']);
    }
}
