<?php

namespace backend\modules\price2\models;

use backend\modules\consumer\models\ConsumerSearch;
use common\behaviors\PositionBehavior;
use frontend\models\CompanyUser;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use frontend\models\User;

/**
 * This is the model class for table "price".
 *
 * @property int $id
 * @property int $author_id
 * @property int $company_id
 * @property int $udater_id
 * @property string $name
 * @property string $description
 * @property int $type
 * @property int $position
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $author
 * @property Company $company
 * @property PriceColumn[] $priceColumns
 * @property PriceItem[] $priceItems
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    const ADMIN_STATUS_NEW = 0;
    const ADMIN_STATUS_VIEWED = 1;

    const TYPE_SALE = 0;
    const TYPE_PURCHASE = 1;

    public static function tableName()
    {
        return 'price';
    }

    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            PositionBehavior::className(),
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'udater_id',
            ],
        ];
    }

    public function rules()
    {
        return [
            [['author_id', 'company_id', 'udater_id', 'type', 'position', 'status', 'admin_status', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => ConsumerSearch::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            ['admin_status', 'in', 'range' => [self::ADMIN_STATUS_NEW, self::ADMIN_STATUS_VIEWED]],
            ['admin_status', 'default', 'value' => self::ADMIN_STATUS_NEW],
            ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Автор',
            'company_id' => 'Компания',
            'udater_id' => 'Обновил',
            'name' => 'Название',
            'description' => 'Описание',
            'type' => 'Тип',
            'position' => 'Позиция',
            'status' => 'Активный',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'admin_status' => 'Статус Администратора'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
//        return $this->hasOne(ConsumerSearch::className(), ['id' => 'author_id']);
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceColumns()
    {
        return $this->hasOne(PriceColumn::className(), ['price_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceItems()
    {
        return $this->hasMany(PriceItem::className(), ['price_id' => 'id']);
    }

    static public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Да',
            self::STATUS_NOTACTIVE => 'Нет'
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }

    static public function getTypeList()
    {
        return [
            self::TYPE_SALE => 'Продажа',
            self::TYPE_PURCHASE => 'Закупка'
        ];
    }

    public function getTypeDetail()
    {
        return isset(self::getTypeList()[$this->type]) ? self::getTypeList()[$this->type] : '';
    }

    public static function getAllAdminStatuses()
    {
        return [
            self::ADMIN_STATUS_NEW => 'Новое',
            self::ADMIN_STATUS_VIEWED => 'Просмотренное'
        ];
    }

    public function getAdminStatusDetail()
    {
        return isset(self::getAllAdminStatuses()[$this->admin_status]) ? self::getAllAdminStatuses()[$this->admin_status] : 'Undefined';
    }

    public function getType()
    {
        return [
            self::TYPE_SALE => 'Продажа',
            self::TYPE_PURCHASE => 'Закупка'
        ];
    }

    public function beforeDelete()
    {
        PriceItem::deleteAll(['price_id' => $this->id]);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

}
