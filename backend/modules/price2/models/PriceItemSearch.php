<?php

namespace backend\modules\price2\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\price2\models\PriceItem;

/**
 * PriceItemSearch represents the model behind the search form of `backend\modules\price2\models\PriceItem`.
 */
class PriceItemSearch extends PriceItem
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'price_id'], 'integer'],
            [['name', 'column1', 'column2', 'column3'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if( $get = Yii::$app->request->get('price') ){
            $query = PriceItem::find()->where(['price_id' => $get])->orderBy('id DESC');
        }else{
            $query = PriceItem::find()->orderBy('id DESC');
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price_id' => $this->price_id,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'column1', $this->column1])
            ->andFilterWhere(['like', 'column2', $this->column2])
            ->andFilterWhere(['like', 'column3', $this->column3]);

        return $dataProvider;
    }
}
