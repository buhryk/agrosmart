<?php

namespace backend\modules\consumer\controllers;

use frontend\modules\cabinet\models\ConsumerResponsible;
use Yii;
use frontend\models\Company;
use backend\modules\consumer\models\CompanySearch;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\accesscontrol\AccessControlFilter;

class CompanyController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/uploads/company/common', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/company/common' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionChangeIsConfirmed($id)
    {
        $model = $this->findModel($id);
        $newStatus = Yii::$app->request->get('is_confirmed');

        if ($newStatus === null) {
            throw new BadRequestHttpException('Not all parameters provided');
        }

        if (!in_array($newStatus, [Company::CONFIRMED_YES, Company::CONFIRMED_NO])) {
            throw new BadRequestHttpException('Wrong value for "is_confirmed" parameter');
        }

        $model->is_confirmed = $newStatus;
        if ($model->update()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('common', 'Company status has changed'));
        } else {
            Yii::$app->getSession()->setFlash('error', 'Something went wrong. Please, try after reload page');
        }

        return $this->redirect(['view', 'id' => $model->primaryKey]);
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);
        $newStatus = Yii::$app->request->get('status');

        if ($newStatus === null) {
            throw new BadRequestHttpException('Not all parameters provided');
        }

        if (!in_array($newStatus, [Company::STATUS_ACTIVE_YES, Company::STATUS_ACTIVE_NO])) {
            throw new BadRequestHttpException('Wrong value for "status" parameter');
        }

        $model->status = $newStatus;
        if ($model->update()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('common', 'Company status has changed'));
        } else {
            Yii::$app->getSession()->setFlash('error', 'Something went wrong. Please, try after reload page');
        }

        return $this->redirect(['view', 'id' => $model->primaryKey]);
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionSetManager($id)
    {
        $model = ConsumerResponsible::find()
            ->andWhere(['consumer_id' => $id])
            ->andWhere(['type' => ConsumerResponsible::TYPE_COMPANY])
            ->one();
        if(!$model) {
            $model = new ConsumerResponsible();
            $model->type =  ConsumerResponsible::TYPE_COMPANY;
            $model->consumer_id = $id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Менеджер назначен');
            return $this->redirect(['company/view', 'id' => $id]);
        }

        return $this->renderAjax('set-manager', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        $category = $this->findModel($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}