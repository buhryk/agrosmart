<?php

namespace backend\modules\consumer\controllers;

use frontend\modules\cabinet\models\ConsumerResponsible;
use Yii;
use frontend\models\User;
use backend\modules\consumer\models\ConsumerSearch;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\consumer\models\ChangeStatusForm;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ConsumerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    private function viewUser($model)
    {
        if($model->is_new == User::USER_NEW){
            $model->is_new = User::USER_OLD;
            $model->save();
        }
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $changeStatusForm = new ChangeStatusForm();
        $this->viewUser($model);

        return $this->render('view', [
            'model' => $model,
            'changeStatusForm' => $changeStatusForm
        ]);
    }

    public function actionChangeStatus($id)
    {
        $user = $this->findModel($id);
        $model = new ChangeStatusForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = $user->id;
            if ($model->changeStatus()) {
                Yii::$app->session->setFlash('success', 'Статус пользователя успешно изменен');
            } else {
                Yii::$app->session->setFlash('success', 'Произошла ошибка. Попробуйте еще раз после перезагрузки страницы');
            }
        }

        return $this->redirect(['view', 'id' => $user->id]);
    }

    public function actionChangeCanAddAdvertisements($id)
    {
        $model = $this->findModel($id);
        $newProperty = Yii::$app->request->get('property');

        if ($newProperty === null) {
            throw new BadRequestHttpException('Not all parameters provided');
        }

        if (!in_array($newProperty, [User::CAN_ADD_ADVERTISEMENTS_YES, User::CAN_ADD_ADVERTISEMENTS_NO])) {
            throw new BadRequestHttpException('Wrong value for "property" parameter');
        }

        $model->can_add_advertisements = $newProperty;
        if ($model->update()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('users', 'User rules has changed'));
        } else {
            Yii::$app->getSession()->setFlash('error', 'Something went wrong. Please, try after reload page');
        }

        return $this->redirect(['view', 'id' => $model->primaryKey]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $this->viewUser($model);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $category = $this->findModel($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    public function actionSetManager($id)
    {
        $model = ConsumerResponsible::find()
            ->andWhere(['consumer_id' => $id])
            ->andWhere(['type' => ConsumerResponsible::TYPE_USER])
            ->one();
        if(!$model) {
            $model = new ConsumerResponsible();
            $model->type =  ConsumerResponsible::TYPE_USER;
            $model->consumer_id = $id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Менеджер назначен');
            return $this->redirect(['user/view', 'id' => $id]);
        }

        return $this->renderAjax('set-manager', ['model' => $model]);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}