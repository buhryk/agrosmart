<?php

namespace backend\modules\consumer\models;

use frontend\modules\cabinet\models\ConsumerResponsible;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Company;
use backend\modules\news\models\NewsSearch;

/**
 * CompanySearch represents the model behind the search form about `frontend\models\Company`.
 */
class CompanySearch extends Company
{
    public $oblast_id;
    public $user_id;

    public function rules()
    {
        return [
            [['id', 'type', 'status', 'is_confirmed', 'oblast_id', 'user_id'], 'integer'],
            [['last_activity_date'], 'validateDateTime'],
            [['ratio'], 'digitValidator'],
            [['name', 'title', 'ratio'], 'safe'],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if ($this->$attribute != date('Y-m-d', strtotime($this->$attribute)) ) {
            $this->addError($attribute, Yii::t('common', 'Field must be in format') . ' "2000-00-00"');
        }
    }

    public function digitValidator($attribute, $params)
    {
        $value = str_replace(['>=','>','<','<=','=','-'],'',$this->$attribute);

        if (!is_numeric($value)) {
            $this->addError($attribute, 'Field must be numeric or numeric with [>,>=,<,<=,=,-]');
        }
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->oblast_id ) {
            $query->innerJoin('subsidiary', 'company_id = `company`.`id`')
            ->innerJoin('city', '`city`.`id` = `subsidiary`.`city_id`')
            ->andWhere(['oblast_id' => $this->oblast_id]);
        }

        if ($this->user_id) {
            $query->innerJoin('consumer_responsible', 'consumer_responsible.`consumer_id` = company.`id`')
                ->andWhere(['consumer_responsible.type' => ConsumerResponsible::TYPE_COMPANY])
                ->andWhere(['consumer_responsible.user_id' => $this->user_id]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'status' => $this->status,
            'is_confirmed' => $this->is_confirmed,
        ]);

        $query->orderBy('id DESC');

        $query->andFilterWhere(['like', 'name', $this->name]);

        if ($this->last_activity_date) {
            $query->andFilterWhere([
                'between',
                'last_activity_date',
                strtotime($this->last_activity_date),
                strtotime($this->last_activity_date) + NewsSearch::ONE_DAY_IN_SECONDS]);
        }

        if ($this->ratio) {
            $this->addFilterByDigit($query, 'ratio', $this->ratio);
        }

        return $dataProvider;
    }

    public function addFilterByDigit($query, $field, $param) {
        if (strrpos($param, '>=') !== false) {
            $query->andFilterWhere(['>=', $field, str_replace('>=', '',$param)]);
        } elseif (strrpos($param, '>') !== false) {
            $query->andFilterWhere(['>', $field, str_replace('>', '',$param)]);
        } elseif (strrpos($param, '<=') !== false) {
            $query->andFilterWhere(['<=', $field, str_replace('<=', '',$param)]);
        } elseif (strrpos($param, '<') !== false) {
            $query->andFilterWhere(['<', $field, str_replace('<', '',$param)]);
        } elseif (strrpos($param, '-')) {
            $pieces = explode('-',$param);
            if (count($pieces) > 1) {
                $query->andFilterWhere(['between', $field, $pieces[0], $pieces[1]]);
            }
        } elseif (is_numeric($param)) {
            $query->andFilterWhere(['=', $field, $param]);
        }

        return $query;
    }

}