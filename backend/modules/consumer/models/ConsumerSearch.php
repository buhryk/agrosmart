<?php

namespace backend\modules\consumer\models;

use backend\modules\commondata\models\Region;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsSearch;
use frontend\models\Company;
use frontend\models\ConsumerOblast;
use frontend\modules\cabinet\models\ConsumerResponsible;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\User;
use yii\helpers\ArrayHelper;
use common\models\User as Admin;

/**
 * ConsumerSearch represents the model behind the search form about `frontend\models\User`.
 */
class ConsumerSearch extends User
{
    public $company_id;
    public $user_id;

    public function rules()
    {
        return [
            [['id', 'status', 'updated_at', 'oblast_id', 'user_id'], 'integer'],
            [['created_at', 'last_activity_date'], 'validateDateTime'],
            [['username', 'email', 'name', 'surname', 'phone', 'company_id'], 'safe'],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if ($this->$attribute != date('Y-m-d', strtotime($this->$attribute)) ) {
            $this->addError($attribute, Yii::t('common', 'Field must be in format') . ' "2000-00-00"');
        }
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        if ($this->last_activity_date) {
            $this->addFilterByDate($query, 'last_activity_date', $this->last_activity_date);
        }

        if ($this->created_at) {
            $this->addFilterByDate($query, 'created_at', $this->created_at);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        /*
         *  filter oblast
         */

        if ($this->oblast_id) {
            $query->andWhere(['oblast_id' => $this->oblast_id]);;
        }

        /*
         *  filter responsible
        */

        if ($this->user_id) {
            $query->innerJoin('consumer_responsible', 'consumer_responsible.`consumer_id` = consumer.`id`')
                ->andWhere(['consumer_responsible.type' => ConsumerResponsible::TYPE_USER])
                ->andWhere(['consumer_responsible.user_id' => $this->user_id]);
        }

        $query->orderBy('id DESC');

        if ($this->company_id) {
            $company = Company::findOne($this->company_id);
            if ($company && ($users = $company->users)) {
                $query->andFilterWhere(['in', 'id', ArrayHelper::getColumn($users, 'id')]);
            } else {
                $query->andFilterWhere(['in', 'id', [0]]);
            }
        }

        return $dataProvider;
    }

    public function getOblast()
    {
        return $this->hasOne(Region::className(), ['id' => 'oblast_id']);
    }

    public function addFilterByDate($query, $field, $param) {
        $query->andFilterWhere([
            'between',
            $field,
            strtotime($this->$field),
            strtotime($this->$field) + NewsSearch::ONE_DAY_IN_SECONDS]);

        return $query;
    }

}