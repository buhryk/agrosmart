<?php

namespace backend\modules\consumer;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\consumer\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
