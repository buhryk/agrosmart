<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use frontend\models\Company;
use dosamigos\datepicker\DatePicker;
use backend\modules\commondata\models\Region;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\consumer\models\ConsumerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$oblastList = Region::find()->all();
$this->title = Yii::t('common', 'Consumers list');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row">
        <div class="col-md-1 pull-right">
            <a href="#" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Экспорт
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <?= ExportMenu::widget([
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_EXCEL => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'template' => '{menu}',
                    'asDropdown'=>false,
                    'dataProvider' =>$dataProvider,
                    'columns' => [
                         'id',
                        [
                            'attribute' => 'email',
                            'label' => 'Email',
                            'format'=>'raw',
                            'value' => function ($model) {
                                return Html::a($model->email, ['view', 'id' => $model->primaryKey]);
                            }
                        ],
                        [
                            'attribute'=>'name',
                            'contentOptions'=>['style'=>'width: 120px;']
                        ],
                        [
                            'attribute'=>'surname',
                        ],
                        [
                            'attribute' => 'phone',
                            'label' => Yii::t('users', 'PHONE'),
                        ],
                        [
                            'attribute' => 'company_id',
                            'format'=>'raw',
                            'label' => Yii::t('users', 'Компания'),
                            'value' => function ($model) {
                                $company = $model->company;
                                return $company ? Html::a($company->name, ['company/view', 'id' => $company->primaryKey]) : '';
                            },
                        ],
                        [
                            'attribute' => 'oblast_id',
                            'label' => 'Область',
                            'value' => function($model){
                                return isset($model->oblast) ? $model->oblast->title : '';
                            },
                        ],
                        [
                            'attribute' => 'user_id',
                            'label' => 'Менеджер',
                            'format'=>'raw',
                            'value' => function($model){
                                return isset($model->responsible) ? $model->responsible->user->username : '';
                            },
                        ],
                    ],
                    'fontAwesome' => true,
                    'batchSize' => 10,
                ]);?>
            </ul>
        </div>

    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 50px;']
            ],
            [
                'attribute' => 'email',
                'label' => 'Email',
                'contentOptions' => ['style'=>'width: 85px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a($model->email, ['view', 'id' => $model->primaryKey]);
                }
            ],
            [
                'attribute' => 'username',
                'label' => Yii::t('users', 'User login'),
                'contentOptions' => ['style'=>'width: 85px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a($model->username, ['view', 'id' => $model->primaryKey]);
                }
            ],
            [
                'attribute'=>'name',
                'contentOptions'=>['style'=>'width: 120px;']
            ],
            [
                'attribute'=>'surname',
                'contentOptions'=>['style'=>'width: 140px;']
            ],
            [
                'attribute' => 'phone',
                'label' => Yii::t('users', 'PHONE'),
                'contentOptions' => ['style'=>'width: 130px;'],
            ],
            [
                'attribute' => 'company_id',
                'label' => 'Компания',
                'contentOptions' => ['style' => 'width: 140px;'],
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Company::find()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>false, 'placeholder' => ''],
                ],
                'format'=>'raw',
                'value' => function ($model) {
                    $company = $model->company;
                    return $company ? Html::a($company->name, ['company/view', 'id' => $company->primaryKey]) : '';
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'label' => Yii::t('users', 'Registration date'),
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'contentOptions' => ['style'=>'width: 120px;'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'last_activity_date',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->last_activity_date);
                },
                'contentOptions' => ['style'=>'width: 120px;'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'last_activity_date',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('users', 'STATUS'),
                'contentOptions' => ['style'=>'width: 85px;'],
                'filter' => \frontend\models\User::getAllStatuses(),
                'format'=>'raw',
                'value' => function ($model) {
                    return $model->detailStatus;
                }
            ],
            [
                'attribute' => 'is_new',
                'label' => 'Новый',
                'contentOptions' => ['style'=>'width: 85px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return $model->is_new == \frontend\models\User::USER_NEW ? 'Новый' : '';
                }
            ],
            [
                'attribute' => 'oblast_id',
                'label' => 'Область',
                'value' => function($model){
                    return isset($model->oblast) ? $model->oblast->title : '';
                },
                'filter' => \yii\helpers\ArrayHelper::map($oblastList, 'id', 'title'),
            ],
            [
                'attribute' => 'user_id',
                'label' => 'Менеджер',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>false, 'placeholder' => ''],
                ],
                'format'=>'raw',
                'value' => function($model){
                    return isset($model->responsible) ? $model->responsible->user->username : '';
                },
            ],
            [
                'contentOptions'=>['style'=>'width: 90px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        );
                }
            ],
        ],
    ]); ?>
</div>