<?php

use yii\helpers\Html;
use frontend\models\User;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Consumers list'), 'url' => ['index']];;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Удалить'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удалить этаго пользователя?")']) ?>
        <?php if (! $model->company ): ?>
            <?= Html::a(Yii::t('common', 'Назначить менеджера'), ['set-manager', 'id' => $model->id], ['class' => 'btn btn-primary modalButton']) ?>
        <?php endif; ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <?php if (isset($model->responsible)) : ?>
            <tr>
                <th width="200px">Менеджер</th>
                <td><?=Html::a($model->responsible->user->username, ['/adminuser/user/view', 'id' => $model->responsible->user_id]) ?></td>
            </tr>
        <?php endif; ?>
        <tr>
            <th><?= $model->getAttributeLabel('name'); ?></th>
            <td><?= $model->name; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('surname'); ?></th>
            <td><?= $model->surname; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('email'); ?></th>
            <td><?= $model->email; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('username'); ?></th>
            <td><?= $model->username; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('users', 'PHONE'); ?></th>
            <td><?= $model->phone; ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Company'); ?></th>
            <td>
                <?php $company = $model->company;
                echo $company ? Html::a($company->name, ['company/view', 'id' => $company->primaryKey]) : '';
                ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('can_add_advertisements'); ?></th>
            <td>
                <?= $model->canAddAdvertisementsDetail; ?>

                <?php if ($model->can_add_advertisements == User::CAN_ADD_ADVERTISEMENTS_YES) { ?>
                    <a href="<?= Url::to([
                        'change-can-add-advertisements',
                        'id' => $model->primaryKey,
                        'property' => User::CAN_ADD_ADVERTISEMENTS_NO]); ?>" class="btn-sm btn-danger"
                    >
                        <span class="glyphicon glyphicon-remove-sign"></span>
                        <?= Yii::t('common', 'Disallow'); ?>
                    </a>
                <?php } else { ?>
                    <a href="<?= Url::to([
                        'change-can-add-advertisements',
                        'id' => $model->primaryKey,
                        'property' => User::CAN_ADD_ADVERTISEMENTS_YES]); ?>" class="btn-sm btn-success"
                    >
                        <span class="glyphicon glyphicon-ok-sign"></span>
                        <?= Yii::t('common', 'Allow'); ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('users', 'Registration date'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->created_at); ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Status'); ?></th>
            <td>
                <b><i><?= $model->detailStatus; ?></i></b>

                <div style="margin-top: 15px;">
                    <?php $otherStatuses = User::getAllStatuses();
                    $classname = \yii\helpers\StringHelper::basename(get_class($changeStatusForm));
                    if (isset($otherStatuses[$model->status])) {
                        unset($otherStatuses[$model->status]);
                    } ?>

                    <?php $form = ActiveForm::begin([
                        'action' => ['change-status', 'id' => $model->id],
                        'id' => 'change-user-status-form'
                    ]); ?>
                    Изменить на <?= Html::activeDropDownList($changeStatusForm, 'status', $otherStatuses, ['prompt' => '...', 'id' => 'change-user-status-select']); ?>

                    <?php $this->registerJs("
                        $('#change-user-status-select').on('change', function(){
                            $('#change-user-status-form').submit();
                        });
                    ",
                        \yii\web\View::POS_READY,
                        'my-button-handler'
                    ); ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Date last updated at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->updated_at); ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Last activity date'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->last_activity_date); ?></td>
        </tr>
        </tbody>
    </table>
</div>