<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

$searchCityUrl = \yii\helpers\Url::to(['/city-list/index']);
$coordinates = json_decode($model->coordinates, true);

?>

<div class="subsidiary-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city_id')->widget(Select2::className(), [
        'initValueText' => !empty($model->city) ? $model->city->title : '', // set the initial display text
        'options' => ['placeholder' => 'Ведите город'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'ajax' => [
                'url' => $searchCityUrl,
                'dataType' => 'json',
                'data' => new \yii\web\JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],

    ]); ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList($model->listType) ?>
    <div class="row">
        <div class="col-md-6">
            <label>Lat</label>
            <?=Html::textInput('lat', $coordinates['lat'], ['class' => 'form-control']) ?>
        </div>
        <div class="col-md-6">
            <label>Lng</label>
            <?=Html::textInput('lng', $coordinates['lng'], ['class' => 'form-control']) ?>
        </div>

    </div>
    <hr>
    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
