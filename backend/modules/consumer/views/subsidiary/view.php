<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\Subsidiary */

$this->title = $model->getType().' : '.$model->location ;
$this->params['breadcrumbs'][] = ['label' => 'Компании', 'url' => ['company/view', 'id' => $model->company_id]];
$this->params['breadcrumbs'][] = ['label' => $model->company->name, 'url' => ['company/view', 'id' => $model->company_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subsidiary-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'city_id',
                 'value' => function($model) {
                    return $model->city->title;
                 }
            ],
            'address',
            'phone',
            'email:email',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'company_id',
                'value' => function($model) {
                    return $model->company->name;
                }
            ],
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->getType();
                }
            ],
        ],
    ]) ?>

</div>
