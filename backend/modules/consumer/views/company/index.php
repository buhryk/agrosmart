<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use dosamigos\datepicker\DatePicker;
use frontend\models\Company;
use backend\modules\commondata\models\Region;
use yii\helpers\ArrayHelper;

$oblastList = Region::find()->all();

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\consumer\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Companies list');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="company-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 50px;']
            ],
            [
                'attribute' => 'name',
                'contentOptions' => ['style'=>'width: 250px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->name), ['view', 'id' => $model->primaryKey]);
                }
            ],
            [
                'label' => 'Logotype',
                'format'=>'raw',
                'filter' => false,
                'value' => function ($model) {
                    return $model->logotype ?  Html::img($model->logotype, ['alt'=>'yii','width'=>'100']) : '';
                }
            ],
            [
                'attribute' => 'type',
                'contentOptions' => ['style'=>'width: 150px;'],
                'filter' => \frontend\models\Company::getTypesList(),
                'format'=>'raw',
                'value' => 'typeTitle'
            ],
            [
                'attribute' => 'ratio',
                'contentOptions' => ['style'=>'width: 50px;'],
                'filter' => \frontend\models\Company::getTypesList(),
            ],
            [
                'attribute' => 'last_activity_date',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->last_activity_date);
                },
                'contentOptions' => ['style'=>'width: 240px;'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'last_activity_date',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'is_confirmed',
                'contentOptions' => ['style'=>'width: 85px;'],
                'filter' => Company::getAllIsConfirmedProperties(),
                'value' => 'isConfirmedDetail'
            ],
            [
                'attribute' => 'status',
                'contentOptions' => ['style'=>'width: 85px;'],
                'filter' => \frontend\models\Company::getAllStatuses(),
                'format'=>'raw',
                'value' => 'detailStatus'
            ],
            [
                'attribute' => 'oblast_id',
                'label' => 'Область',
                'value' => function($model){
                    return isset($model->mainSubsidiary) ? $model->mainSubsidiary->city->region->title : '';
                },
                'filter' => \yii\helpers\ArrayHelper::map($oblastList, 'id', 'title'),
            ],
            [
                'attribute' => 'user_id',
                'label' => 'Менеджер',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                    'options' => ['multiple'=>false, 'placeholder' => ''],
                ],
                'format'=>'raw',
                'value' => function($model){
                    return isset($model->responsible) ? $model->responsible->user->username : '';
                },
            ],
            [
                'contentOptions'=>['style'=>'width: 90px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        );
                }
            ],
        ],
    ]); ?>
</div>