<?php

use yii\helpers\Html;
use frontend\models\Company;
use kartik\rating\StarRating;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Companies list'), 'url' => ['index']];;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-view">
    <h1><?= Html::encode($this->title) ?>
        <span style="display: inline-block; line-height: 1em; font-size: 12px;">
            <?= StarRating::widget([
                'name' => 'rating_company',

                'value' => $model->ratio,
                'pluginOptions' => [
                    'step' => 0.1,
                    'readonly' => true,
                    'showClear' => false,
                    'showCaption' => false,
                ]
            ]); ?>
        </span>
        <span style="display: inline-block">(<?= $model->ratio; ?>)</span>
    </h1>
    <p>
        <?= Html::a(Yii::t('common', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Удалить'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger', 'onclick' => 'return confirm("Вы уверены, что хотите удалить эту компанию?")']) ?>
        <?= Html::a(Yii::t('common', 'Назначить менеджера'), ['set-manager', 'id' => $model->id], ['class' => 'btn btn-primary modalButton']) ?>

    </p>


    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <?php if (isset($model->responsible)) : ?>
            <tr>
                <th width="200px">Менеджер</th>
                <td><?=Html::a($model->responsible->user->username, ['/adminuser/user/view', 'id' => $model->responsible->user_id]) ?></td>
            </tr>
        <?php endif; ?>
        <tr>
            <th width="200px"><?= $model->getAttributeLabel('name'); ?></th>
            <td><?=Html::encode($model->name); ?></td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Company users'); ?></th>
            <td>
                <?php $users = $model->users; ?>
                <?php if ($users) { ?>
                    <ul>
                        <?php foreach ($users as $user) { ?>
                            <li>
                                <a href="<?= Url::to(['user/view', 'id' => $user->primaryKey]); ?>">
                                    <?= $user->fullName; ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('common', 'Филиалы'); ?></th>
            <td>
                <?php $subsidiaries = $model->subsidiaries; ?>
                <?php if ($subsidiaries) { ?>
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Адрес</td>
                                <td>Телефон</td>
                                <td>Email</td>
                            </tr>
                        </thead>
                        <?php foreach ($subsidiaries as $subsidiary) { ?>
                            <tr>
                                <td>
                                        <?= $subsidiary->location; ?>
                                </td>
                                <td>
                                    <?=$subsidiary->phone ?>
                                </td>
                                <td>
                                    <?=$subsidiary->email ?>
                                </td>
                                <td>
                                    <a class="btn btn-default btn-sm"
                                       href="<?= Url::to(['subsidiary/view', 'id' => $subsidiary->primaryKey]); ?>"
                                       title="Просмотреть">
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>
                                    <a class="btn btn-default btn-sm"
                                       href="<?= Url::to(['subsidiary/update', 'id' => $subsidiary->primaryKey]); ?>"
                                       title="Редактировать">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                </td>
                            </tr>


                        <?php } ?>

                    </table>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('type'); ?></th>
            <td><?= $model->typeTitle; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('logotype'); ?></th>
            <td>
                <?php if ($model->logotype) { ?>
                    <?php echo Html::img($model->logotype, ['width' => '150']); ?>
                <?php } else { ?>
                    <?= Yii::t('common', 'No'); ?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('short_description'); ?></th>
            <td><?=Html::encode($model->short_description); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('full_description'); ?></th>
            <td><?= HtmlPurifier::process($model->full_description); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('ratio'); ?></th>
            <td><?= $model->ratio; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('is_confirmed'); ?></th>
            <td>
                <?= $model->isConfirmedDetail; ?>
                &nbsp;&nbsp;
                <?php $newStatus = $model->is_confirmed == Company::CONFIRMED_YES ? Company::CONFIRMED_NO : Company::CONFIRMED_YES;?>
                <?php $message = $newStatus == Company::CONFIRMED_YES ? Yii::t('common', 'Company confirmed') : Yii::t('common', 'Company not confirmed'); ?>
                <a href="<?= Url::to(['change-is-confirmed', 'id' => $model->primaryKey, 'is_confirmed' => $newStatus]); ?>"
                   class="btn-sm btn-success">
                    <?= Yii::t('common', 'Change to'); ?>&nbsp;"<?= $message; ?>"
                </a>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('status'); ?></th>
            <td>
                <?= $model->detailStatus; ?>
                &nbsp;&nbsp;
                <?php $newStatus = $model->status == Company::STATUS_ACTIVE_YES ? Company::STATUS_ACTIVE_NO : Company::STATUS_ACTIVE_YES; ?>
                <?php $message = $newStatus == Company::STATUS_ACTIVE_YES ? Yii::t('common', 'Active company') : Yii::t('common', 'Not active company'); ?>
                <a href="<?= Url::to(['change-status', 'id' => $model->primaryKey, 'status' => $newStatus]); ?>"
                   class="btn-sm btn-success">
                    <?= Yii::t('common', 'Change to'); ?>&nbsp;"<?= $message; ?>"
                </a>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('last_activity_date'); ?></th>
            <td><?= date('Y-m-d H:i', $model->last_activity_date); ?></td>
        </tr>
        </tbody>
    </table>
</div>