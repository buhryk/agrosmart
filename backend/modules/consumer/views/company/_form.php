<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Company;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model frontend\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php if ($model->logotype) {
        echo Html::img('/'.$model->logotype, ['width' => 200]);
    } ?>
    <?= $form->field($model, 'logotype')->widget(InputFile::className(), [
        'language'      => 'ru',
        'controller'    => 'elfinder',
        'filter'        => 'image',
        'path'          => 'company/common',
        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options'       => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple'      => false
    ]); ?>
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <?= $form->field($model, 'type')->dropDownList(Company::getTypesList()) ?>
        </div>
        <div class="col-md-4 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList(Company::getAllStatuses()) ?>
        </div>
        <div class="col-md-4 col-sm-12">
            <?= $form->field($model, 'is_confirmed')->dropDownList(Company::getAllIsConfirmedProperties()) ?>
        </div>
    </div>

    <?= $form->field($model, 'short_description')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>
    <?= $form->field($model, 'full_description')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>