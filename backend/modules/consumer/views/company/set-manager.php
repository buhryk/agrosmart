<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

$adminList = \common\models\User::find()->all();

?>
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Назначить менеджера</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <?php $form = ActiveForm::begin([
                    'id' => 'my-form-id',
                ]
            ); ?>
            <?= $form->field($model, 'user_id')->widget(Select2::className(), [
                'data' => ArrayHelper::map($adminList, 'id', 'username'),
                'options' => ['placeholder' => '..'],
                'id' => 'rubric-select',
                'pluginOptions' => [
                ],
            ])->label('Менеджер'); ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

