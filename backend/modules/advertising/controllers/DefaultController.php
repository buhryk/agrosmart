<?php

namespace backend\modules\advertising\controllers;


use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\advertising\models\AdvertisingPosition;
use yii\web\Controller;
use Yii;


class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }
    
    public function actionIndex($key = 'header')
    {
        $models = AdvertisingPosition::find()->where(['group_key' => $key])->orderBy('position')->all();
        return $this->render('index', ['models' => $models, 'key' => $key]);
    }

    public function actionCreate($key)
    {
        $model = new AdvertisingPosition();
        $model->group_key = $key;

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect('update', ['id' => $model->id]);
        }

        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdated($id)
    {
        $model = AdvertisingPosition::findOne($id);

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect('update', ['id' => $model->id]);
        }

        return $this->render('create', ['model' => $model]);
    }
}
