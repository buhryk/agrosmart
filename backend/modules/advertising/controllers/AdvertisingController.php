<?php
/**
 * Created by PhpStorm.
 * User: Diz
 * Date: 02.03.2017
 * Time: 16:27
 */

namespace backend\modules\advertising\controllers;


use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\advertising\models\Advertising;
use backend\modules\advertising\models\AdvertisingGroup;
use backend\modules\advertising\models\AdvertisingPosition;
use backend\modules\advertising\models\AdvertisingSearch;
use Symfony\Component\DomCrawler\Form;
use yii\web\Controller;
use yii;

class AdvertisingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }
    
    
    public function actionIndex($key = NULL)
    {
        $searchModel = new AdvertisingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $key);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'key' => $key
        ]);
    }

    public function actionCreate($key = NULL)
    {
        if (empty($key)) {
            $group = AdvertisingGroup::find()->one();
            $key = $group->key;
        }
        $modelPosition =  AdvertisingPosition::findAll(['group_key'=>$key]);
        $model = new Advertising();
        if($model->load(\Yii::$app->request->post()) && $model->save()) {
            $model->positionSave($model->id);
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'modelPosition' => $modelPosition,
            'model' => $model,
            'key' => $key,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Advertising::findOne($id);

        $modelPosition =  AdvertisingPosition::findAll(['group_key'=>$model->position[0]->group_key]);

        if($model->load(\Yii::$app->request->post()) && $model->save()) {
            $model->positionSave($model->id);
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'modelPosition' => $modelPosition,
            'model' => $model,
            'key' => $model->position[0]->group_key,
        ]);
    }

    public function actionDelete($id)
    {
        $model =   Advertising::findOne($id);
        $key = $model->position[0]->group_key;
        $model->delete();

        return $this->redirect(['/advertising/advertising/index', 'key' => $key]);
    }



}