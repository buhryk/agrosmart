<?php

namespace backend\modules\advertising\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use Yii;
use backend\modules\advertising\models\AdvertisingPosition;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdvertisingPositionController implements the CRUD actions for AdvertisingPosition model.
 */
class AdvertisingPositionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    /**
     * Lists all AdvertisingPosition models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => AdvertisingPosition::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AdvertisingPosition model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate($key)
    {
        $model = new AdvertisingPosition();
        $model->group_key = $key;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/advertising/default/index', 'key' => $model->group_key]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/advertising/default/index', 'key' => $model->group_key]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model =   $this->findModel($id);
        $key = $model->group_key;
        $model->delete();

        return $this->redirect(['/advertising/default/index', 'key' => $key]);
    }

    protected function findModel($id)
    {
        if (($model = AdvertisingPosition::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
