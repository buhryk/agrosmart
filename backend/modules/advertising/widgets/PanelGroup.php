<?php

namespace backend\modules\advertising\widgets;

use yii\base\Widget;
use backend\modules\advertising\models\AdvertisingGroup;

class PanelGroup extends Widget {

    public $key;

    public function init() {

    }

    public function run() {
        $models = AdvertisingGroup::find()->all();
        return $this->render('panel-group', [
            'models' => $models,
            'key' => $this->key
        ]);
    }

}