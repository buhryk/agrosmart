<?php

namespace backend\modules\advertising\widgets;

use backend\modules\advertising\models\Advertising;
use backend\modules\advertising\models\AdvertisingGroup;
use yii\base\Widget;

class AdvertisingWidget extends Widget {

    public $key;
    public $colum;
    public $widthDefault;
    public $height;
    public $params;
    public $typePosition;
    
    public function init()
    {

        if($this->typePosition == 'horizontally') {
            $this->params = 'width';
        } else {
            $this->params = 'height';
        }

        $sql = "SELECT COUNT(*) FROM advertising_position WHERE group_key = :key";
        $this->colum = \Yii::$app->db->createCommand($sql, [':key' => $this->key])->queryScalar();
        $this->widthDefault = 1 / ($this->colum != 0 ? $this->colum : 100) * 100;
        parent::init(); 
    }
    
    private function getData()
    {
        $result =[];



        $sql = "SELECT advertising.* , count(*) AS blocks, `position`, width, height
                FROM advertising
                INNER JOIN advertising_model_position AS model_position ON advertising.id = model_position.model_id
                INNER JOIN advertising_position as `position` ON `position`.id = model_position.position_id
                WHERE group_key =:key AND start < CURDATE() AND finish > CURDATE()
                GROUP BY advertising.id
                ORDER BY position";

        $listAdvertising = \Yii::$app->db->createCommand($sql, [':key' => $this->key])->queryAll();
        $blocksCount = 0;
        if ($listAdvertising) {
            foreach ($listAdvertising as $item){
                $blocksCount = $blocksCount + $item['blocks'];

                if($blocksCount < $item['position']) {
                    $result = array_merge($result, $this->itemsEmpty($item['position'] - $blocksCount));
                    $blocksCount = $blocksCount + $item['position'] - $blocksCount;
                }

                $result[] = [
                    'image' => $item['image'],
                    'link' => $item['link'],
                    'width' => $item['blocks'] / $this->colum * 100
                ];
            }
        } else {
            $item['position'] = $this->colum ;
        }

        if ($blocksCount < $this->colum && $blocksCount != $item['position']){
            $result = array_merge($result, $this->itemsEmpty($this->colum - $blocksCount));
        }

        return $result;
    }
    
    private function itemsEmpty($count)
    {
        $result = [];
        for($i=0; $i < $count; $i++){
            $result[] = [
                'image' => '',
                'link' => '#',
                'width' => $this->widthDefault
            ];
        }
        return $result;
    }

    public function run()
    {
        $section = \Yii::$app->db->createCommand('SELECT * FROM advertising_group WHERE `key` = :key AND active = :active', [':key' => $this->key, ':active' => AdvertisingGroup::ACTIVE])->queryOne();
        if (!$section) return false;

        return $this->render('advertising', [
            'data' => $this->getData(),
            'key' => $this->key,
            'colum' => $this->colum,
            'params' => $this->params
        ]);
    }
        

}