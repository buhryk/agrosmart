<?php

?>
<div id="widget-block-<?=$key ?>" class="widget-block-<?=$key ?> mobile-owl-<?=$key ?>">
    <?php foreach($data as $item) { ?>
        <div class="item" style="<?=$params ?>: <?=$item['width'] ?>%">
            <a href="<?=$item['link'] ?>">
                <?php if($item['image']){ ?>
                    <img src="<?=$item['image'] ?>">
                <?php } else { ?>
                    <?= Yii::$app->thumbnail->placeholder(['width' =>'170', 'height' =>'170', 'text' => Yii::t('common', 'Advertising')]);?>
                <?php } ?>
            </a>
        </div>
   <?php }  ?>
</div>
