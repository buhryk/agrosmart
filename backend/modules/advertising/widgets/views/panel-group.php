<?php
/**
 * Created by PhpStorm.
 * User: Diz
 * Date: 02.03.2017
 * Time: 15:38
 */
$controller = Yii::$app->controller->id;
?>
<ul class="nav nav-tabs">
    <?php foreach ($models as $model): ?>
        <li class="<?=$model->key == $key ? 'active': '' ?>"><a  href="<?=\yii\helpers\Url::to(['/advertising/'.$controller.'/index', 'key' => $model->key]) ?>"><?=$model->name ?></a></li>
    <?php endforeach ?>
</ul>
