<?php

namespace backend\modules\advertising\models;

use Yii;

/**
 * This is the model class for table "advertising_group".
 *
 * @property string $key
 * @property string $name
 */
class AdvertisingGroup extends \yii\db\ActiveRecord
{
    const ACTIVE = 1;
    const NO_ACTIVE =0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertising_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['key'], 'string', 'max' => 45],
            [['name'], 'string', 'max' => 255],
            [['active'], 'integer'],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => 'Ключ',
            'name' => 'Название',
            'active' => 'Активный',
        ];
    }
    
    public function getStatusList()
    {
        return [
            self::ACTIVE => 'Да',
            self::NO_ACTIVE => 'Нет',
        ];
    }
}
