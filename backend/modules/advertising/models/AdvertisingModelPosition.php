<?php

namespace backend\modules\advertising\models;

use Yii;

/**
 * This is the model class for table "advertising_model_position".
 *
 * @property integer $model_id
 * @property integer $position_id
 */
class AdvertisingModelPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertising_model_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id', 'position_id'], 'required'],
            [['model_id', 'position_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'position_id' => 'Position ID',
        ];
    }
}
