<?php

namespace backend\modules\advertising\models;



/**
 * This is the model class for table "advertising".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updatedd_at
 * @property string $start
 * @property string $finish
 * @property string $image
 * @property string $name
 */
class Advertising extends \yii\db\ActiveRecord
{
    public $positions = [];

    public static function tableName()
    {
        return 'advertising';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start', 'finish', 'image', 'name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['start', 'finish', 'positions'], 'safe'],
            [['image'], 'string', 'max' => 355],
            [['name', 'link'], 'string', 'max' => 255],
            [['start'], 'validateStart'],
            [['finish'], 'validateFinish']
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function validateStart($attribute, $params)
    {
        foreach ($this->positions as $item){
            $model = Advertising::find()
                ->innerJoin('advertising_model_position', 'advertising.`id` = advertising_model_position.`model_id`')
                ->where(['<=','start', $this->start])
                ->andWhere(['>=','finish', $this->start])
                ->andWhere(['position_id' => $item])
                ->andFilterWhere(['!=', 'id', $this->filterDate()])
                ->one();
            if($model){
                $this->addError($attribute, 'Дата занята');
                break;
            }
        }
    }

    private function filterDate()
    {
        if($this->isNewRecord) return NULL;
        return $this->id;
    }

    public function validateFinish($attribute, $params)
    {
        if($this->start > $this->finish){
            $this->addError($attribute, 'Дата окончания не может быть меньше даты старта');
        }

        foreach ($this->positions as $item){
            $model = Advertising::find()
                ->innerJoin('advertising_model_position', 'advertising.`id` = advertising_model_position.`model_id`')
                ->andWhere(['<=','start', $this->finish])
                ->andWhere(['>=','finish', $this->finish])
                ->andWhere(['position_id' => $item])
                ->andFilterWhere(['!=', 'id', $this->filterDate()])
                ->one();
            if($model){
                $this->addError($attribute, 'Дата занята');
                break;
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'start' => 'Дата старта',
            'finish' => 'Дата окончания',
            'image' => 'Банер',
            'name' => 'Название'

        ];
    }

    public function getPosition()
    {
        return $this->hasMany(AdvertisingPosition::className(), ['id' => 'position_id'])
            ->viaTable('advertising_model_position', ['model_id' => 'id']);
    }

    public function saveData()
    {
        if (!$this->validate()) {
            return null;
        }

        if(! $this->save()) return null;
        $this->positionSave($this->id);

        return true;
    }

    public function positionSave($id)
    {
        AdvertisingModelPosition::deleteAll(['model_id' =>$id]);

        foreach ($this->positions as $item) {
            $model = new AdvertisingModelPosition();
            $model->position_id = $item;
            $model->model_id = $id;
            $model->save();
        }
    }


}
