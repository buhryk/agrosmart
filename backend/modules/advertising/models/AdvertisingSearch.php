<?php

namespace backend\modules\advertising\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\advertising\models\Advertising;

/**
 * AdvertisingSearch represents the model behind the search form about `backend\modules\advertising\models\Advertising`.
 */
class AdvertisingSearch extends Advertising
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['start', 'finish', 'image', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $key)
    {
        $query = Advertising::find()
            ->innerJoin('advertising_model_position as model_position', 'advertising.id = model_position.model_id')
            ->innerJoin('advertising_position as position', 'position.id = model_position.position_id')
            ->andFilterWhere(['group_key'=> $key])
            ->groupBy('advertising.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
      
        if($this->start) {
            $query->andWhere(['start'=> \DateTime::createFromFormat('d.m.Y', $this->start)->format('Y-m-d')]);
        }

        if($this->finish) {
            $query->andWhere(['finish'=> \DateTime::createFromFormat('d.m.Y', $this->finish)->format('Y-m-d')]);
        }

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
