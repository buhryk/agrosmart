<?php
namespace backend\modules\advertising\models\advertising;

use Yii;
use yii\base\Model;


class AdvertisingModel extends Model
{
    public $start;
    public $finish;
    public $image;
    public $name;
    public $positions = [];


    public function rules()
    {
        return [
            [['image', 'name'], 'string', 'max' => 255],
            [['start', 'finish', 'positions'], 'safe'],
            [['start', 'finish', 'name', 'image'], 'required'],
            [['start'], 'validateStart'],
            [['finish'], 'validateFinish']

        ];
    }

    public function validateStart($attribute, $params)
    {
       foreach ($this->positions as $item){
           $model = Advertising::find()
               ->innerJoin('advertising_model_position', 'advertising.`id` = advertising_model_position.`model_id`')
               ->where(['<=','start', $this->start])
               ->andWhere(['>=','finish', $this->start])
               ->andWhere(['position_id' => $item])
               ->one();
           if($model){
               $this->addError($attribute, 'Дата занята');
               break;
           }
       }
    }

    public function validateFinish($attribute, $params)
    {
        if($this->start > $this->finish){
            $this->addError($attribute, 'Дата окончания не может быть меньше даты старта');
        }

        foreach ($this->positions as $item){
            $model = Advertising::find()
                ->innerJoin('advertising_model_position', 'advertising.`id` = advertising_model_position.`model_id`')
                ->andWhere(['<=','start', $this->finish])
                ->andWhere(['>=','finish', $this->finish])
                ->andWhere(['position_id' => $item])
                ->one();
            if($model){
                $this->addError($attribute, 'Дата занята');
                break;
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'start' => 'Дата старта',
            'finish' => 'Дата окончания',
            'image' => 'Банер',
            'name' => 'Название'

        ];
    }

    public function save() {
        if (!$this->validate()) {
            return null;
        }

        $model = new Advertising();
        $model->image = $this->image;
        $model->name = $this->name;
        $model->finish = $this->finish;
        $model->start = $this->start;

        if(! $model->save()) return null;

        $this->positionSave($model->id);

        return true;
    }
    public function positionSave($id) {
        foreach ($this->positions as $item) {
            $model = new AdvertisingModelPosition();
            $model->position_id = $item;
            $model->model_id = $id;
            $model->save();
        }
    }

}
