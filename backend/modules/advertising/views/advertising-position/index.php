<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Advertising Positions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertising-position-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Advertising Position', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'group_key',
            'position',
            'name',
            'price',
            // 'created_at',
            // 'updated_at',
            // 'width',
            // 'height',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
