<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\advertising\models\AdvertisingPosition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advertising-position-form">

    <?php $form = ActiveForm::begin() ?>
    <div class="col-md-7">
        <?=$form->field($model, 'name') ?>
        <?=$form->field($model, 'price') ?>
        <?=$form->field($model, 'position') ?>
        <?=$form->field($model, 'width') ?>
        <?=$form->field($model, 'height') ?>
        <div class="form-group">
            <button class="btn btn-success" type="submit">
                Сохранить
            </button>
        </div>
    </div>
    <div class="row">

    </div>
    <?php ActiveForm::end() ?>

</div>
