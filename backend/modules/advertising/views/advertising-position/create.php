<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\advertising\models\AdvertisingPosition */

$this->title = 'Создание рекламного блока';
$this->params['breadcrumbs'][] = ['label' => 'Реклама', 'url' => ['/advertising']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertising-position-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
