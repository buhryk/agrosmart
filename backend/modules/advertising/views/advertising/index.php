<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\advertising\models\AdvertisingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Банера';
$this->params['breadcrumbs'][] = ['label' => 'Реклама', 'url'=> '/advertising'];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="advertising-index">
    
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="col-md-12">
        <?=\backend\modules\advertising\widgets\PanelGroup::widget(['key' => $key]) ?>
    </div>

    <p>
        <?= Html::a('Добавить банер', ['create', 'key' => $key], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'start',
                'format' =>  ['date', ' Y.MM.dd'],
                'options' => ['width' => '200'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'start',
                    'pluginOptions' => [
                        'autoclose'=>true,
                    ]
                ])
            ],
            [
                'attribute' => 'finish',
                'format' =>  ['date', ' Y.MM.dd'],
                'options' => ['width' => '200'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'finish',
                    'pluginOptions' => [
                        'autoclose'=>true,
                    ]
                ])
            ],
            [
                'attribute' => 'image',
                'format' => ['image', [
                    'width'=>'100',
                    'height'=>'100'
                ]]
            ],
            'name',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>
</div>
