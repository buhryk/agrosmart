<?php
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

if(!$model->isNewRecord && empty($model->positions)) {
    $model->positions = \yii\helpers\ArrayHelper::getColumn($model->position, 'id');
}
?>
<?php $form =ActiveForm::begin(); ?>
    <div class="col-md-12">
        <div class="col-md-12">
            <?php foreach ($modelPosition as $item ): ?>
                <div class="item">
                    <label data-date="<?=$item->date ?>" class="advetising-item <?=in_array($item->id, $model->positions) ? 'active' : '' ?>">
                        <?=$item->name  ?>
                        <br>
                        <input class="checkbox" type="checkbox" name="Advertising[positions][]" value="<?=$item->id ?>"  <?=in_array($item->id, $model->positions) ? 'checked' : '' ?>>
                        <p style="font-size: 37px; font-weight: 100">
                            <?=$item->width ?>X<?=$item->height ?>
                        </p>
                    </label>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?=$form->field($model, 'start')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]) ?>
            </div>
            <div class="col-md-3">
                <?=$form->field($model, 'finish')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'image')->widget(\mihaildev\elfinder\InputFile::className(), [
                    'language'      => 'ru',
                    'controller'    => 'elfinder',
                    'filter'        => 'image',
                    'path'          => 'news',
                    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                    'options'       => ['class' => 'form-control'],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple'      => false
                ]); ?>
            </div>
            <div class="col-md-2">
                <?php if($model->image): ?>
                    <img style="height: 150px" src="<?=$model->image ?>">
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'name'); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'link'); ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <button class="btn btn-success">Сохранить</button>
            </div>
        </div>
    </div>

<?php ActiveForm::end() ?>