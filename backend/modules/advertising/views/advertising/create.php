<?php
/**
 * Created by PhpStorm.
 * User: Diz
 * Date: 02.03.2017
 * Time: 17:47
 */
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\assets\AdvertisingAsset;
use kartik\widgets\DatePicker;

AdvertisingAsset::register($this);
$this->title = 'Добавление баннера';


?>
<div class="index">
    <div class="col-md-12">
        <h1><?=$this->title ?></h1>
    </div>
    <div class="col-md-12">
        <?=\backend\modules\advertising\widgets\PanelGroup::widget(['key' => 'header']) ?>
    </div>
    <?=$this->render('_form', [
            'model' => $model,
            'modelPosition' => $modelPosition
    ]) ?>
</div>
