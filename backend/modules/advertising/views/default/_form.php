<?php
/**
 * Created by PhpStorm.
 * User: Diz
 * Date: 03.03.2017
 * Time: 14:37
 */
use yii\widgets\ActiveForm;

?>
<div class="row">
    <?php $form = ActiveForm::begin() ?>
    <div class="col-md-7">
        <?=$form->field($model, 'name') ?>
        <?=$form->field($model, 'price') ?>
        <?=$form->field($model, 'position') ?>
        <?=$form->field($model, 'width') ?>
        <?=$form->field($model, 'height') ?>
        <div class="form-group">
            <button class="btn btn-success" type="submit">
                Сохранить
            </button>
        </div>
    </div>
    <div class="row">

    </div>
    <?php ActiveForm::end() ?>
</div>
