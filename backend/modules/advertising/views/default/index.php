<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('common', 'Реклама');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertising-default-index">
    <div class="row">
        <div class="col-md-6">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-md-6">
            <a class="btn btn-success pull-right" href="<?=Url::to(['/advertising/advertising-group']) ?>">Секции</a>
        </div>
    </div>

    <div class="col-md-12">
       <?=\backend\modules\advertising\widgets\PanelGroup::widget(['key' => $key]) ?>
    </div>
    <div class="col-md-12">
        <div>
            <a class="btn" href="<?=\yii\helpers\Url::to(['/advertising/advertising/index', 'key' => $key]) ?>">
                Список банеров
            </a>
            <a class="btn " href="<?=\yii\helpers\Url::to(['/advertising/advertising/create', 'key' => $key]) ?>">
                Добавить банер
            </a>
        </div>
        <table class="table">
            <thead>

            </thead>

            <tbody>
                <?php foreach ($models as $model): ?>
                    <tr>
                        <td> <?=$model->name ?></td>
                        <td> <?=$model->price ?> грн</td>
                        <td>
                            <a href="<?=\yii\helpers\Url::to(['advertising/index', 'id' => $model->id]) ?>" title="Редактировать" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <a href="<?=\yii\helpers\Url::to(['advertising-position/update', 'id' => $model->id]) ?>" title="Редактировать" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="<?=\yii\helpers\Url::to(['advertising-position/delete', 'id' => $model->id]) ?>" data-method="post" title="Удалить" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div>
            <a class="btn btn-success" href="<?=\yii\helpers\Url::to(['/advertising/advertising-position/create', 'key' => $key]) ?>">
                Добавить блок
            </a>
        </div>
    </div>
</div>


