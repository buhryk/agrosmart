<?php
/**
 * Created by PhpStorm.
 * User: Diz
 * Date: 03.03.2017
 * Time: 14:36
 */
$this->title = 'Добавление блока';
?>
<div id="advertising-position-create">
    <div class="col-md-12">
        <h1> <?=$this->title ?></h1>
    </div>
    <div class="col-md-12">
        <?=$this->render('_form', ['model' => $model]) ?>
    </div>
</div>
