<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\setting\models\SettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['create']) ?>"
           class="btn btn-primary block right">
            <?= Yii::t('common', 'Add setting'); ?>
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            'alias',
            'value:ntext',
            'description:ntext',
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return date('Y-m-d H:i:s', $model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($model) {
                    return date('Y-m-d H:i:s', $model->updated_at);
                }
            ],
            [
                'contentOptions'=>['style'=>'width: 120px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'View')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => Yii::t('common', 'Edit')]
                        ) . ' ' .
                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-default btn-sm', 'title' => 'Удалить',
                                'onclick' => 'return confirm("'.Yii::t('common', 'Are you sure you want to delete this item?').'")']
                        );
                }
            ],
        ],
    ]); ?>
</div>