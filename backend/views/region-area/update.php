<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\commondata\models\RegionArea */

$this->title = 'Update Region Area: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Region Areas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="region-area-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
