<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\commondata\models\RegionArea */

$this->title = 'Create Region Area';
$this->params['breadcrumbs'][] = ['label' => 'Region Areas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-area-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
