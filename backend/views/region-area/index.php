<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\commondata\models\RegionAreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Region Areas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-area-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Region Area', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'oblast_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
