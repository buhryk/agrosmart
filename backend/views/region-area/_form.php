<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\commondata\models\RegionArea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="region-area-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'oblast_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
