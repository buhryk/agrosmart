<?php

/* @var $this \yii\web\View */
/* @var $content string */


use yii\helpers\Html;


if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language; ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo Html::csrfMetaTags(); ?>
    <title><?php echo Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body id="minovate" class="appWrapper">
<?php $this->beginBody(); ?>

<div id="wrap" class="animsition">
    <div class="page page-core page-404">
        <div class="text-center">
            <h3 class="text-light text-white"><?php echo Yii::$app->name; ?></h3>
        </div>
        <div class="container w-420 p-15 bg-white mt-40 text-center">
            <?php echo $content; ?>
            <div class="bg-slategray lt wrap-reset mt-40 text-center">
                <a href="/" class="btn btn-greensea btn-sm b-0">
                    <i class="fa fa-home"></i> Домой
                </a>

            </div>
        </div>
    </div>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
