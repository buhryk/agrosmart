<?php
use backend\assets\ChartAsset;

ChartAsset::register($this);

$this->title = 'Главная';
?>
<div class="row tile_count">
    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="glyphicon glyphicon-shopping-cart"></i> Объявлений</span>
        <div class="count">
            <?=isset($product['count']) ? $product['count'] : 0 ?>
        </div>
        <span class="count_bottom">
            <i class="green">
                <?=isset($product['last_week']) ? $product['last_week'] : 0 ?>
            </i>
            За неделю
        </span><br>
        <span class="count_bottom">
            <i class="green">
                <?=isset($product['last_day']) ? $product['last_day'] : 0 ?>
            </i>
            За день
        </span>
    </div>

    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-user"></i> Пользователя</span>
        <div class="count">
            <?=isset($user['count']) ? $user['count'] : 0 ?>
        </div>
        <span class="count_bottom">
            <i class="green">
                <?=isset($user['last_week']) ? $user['last_week'] : 0 ?>
            </i>
          За неделю
        </span><br>
        <span class="count_bottom">
            <i class="green">
                <?=isset($user['last_day']) ? $user['last_day'] : 0 ?>
            </i>
            За день
        </span>
    </div>

    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-users"></i> Компаний</span>
        <div class="count">
            <?=isset($company['count']) ? $company['count'] : 0 ?>
        </div>
        <span class="count_bottom">
            <i class="green">
                <?=isset($company['last_week']) ? $company['last_week'] : 0 ?>
            </i>
            За неделю
        </span><br>
        <span class="count_bottom">
            <i class="green">
                <?=isset($company['last_day']) ? $company['last_day'] : 0 ?>
            </i>
            За день
        </span>
    </div>

    <div class="card-container-custom col-lg-3 col-sm-6 col-sm-12">
        <section class="tile">
            <a  href="<?=\yii\helpers\Url::to(['index', 'clear-cache'=>1]) ?>" class="btn btn-default btn-ef btn-ef-3 btn-ef-3a mb-10" data-method="post" >
                Очистить кэш <i class="glyphicon glyphicon-folder-close"></i>
            </a>
        </section>
    </div>

    <div class="col-md-12">
        <h4 class="custom-font"><strong>Статистика посещаемости</h4>
        <div id="line-example" style="height: 300px;"></div>
    </div>
</div>
<?php


$script = <<< JS
    $(window).load(function(){
        Morris.Line({
                element: 'line-example',
                data:
                   $analytic
                ,
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['Посетителей', 'Просмотров'],
                lineColors:['#16a085','#FF0066']
            });
     });

JS;
$this->registerJs($script, yii\web\View::POS_READY);