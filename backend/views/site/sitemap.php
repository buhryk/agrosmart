<?php
use yii\helpers\Html;
?>

<style>
    .loaderSiteMap {
        border: 7px solid #f3f3f3;
        border-radius: 50%;
        border-top: 7px solid #3498db;
        width: 40px;
        display: none;
        height: 40px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

<h2>Обновление Sitemap</h2>
<?php if (isset(\Yii::$app->params['frontendUrl'])) {
    echo 'Обновленный сайтмап будет доступен по ссылке ' .  Html::a(\Yii::$app->params['frontendUrl'] . '/sitemap.xml', \Yii::$app->params['frontendUrl'] . '/sitemap.xml');
} ?>

<br><br>
<div class="col-md-2">
    <button class="btn btn-primary" id="siteMapGeneratorButton">Сгенерировать Sitemap</button>
</div>
<div class="col-md-3">
    <div class="loaderSiteMap"></div>
</div>
<div class="clearfix"></div>
<br><br>
<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" id="siteMapProgressBar">
        <span class="sr-only">60% Complete</span>
    </div>
</div>

<br>
<br>

<?php

$script = <<< JS
    
    var counter = 1;
    var progress = 0;
    $('#siteMapGeneratorButton').click(function(e) {
        $('#siteMapGeneratorButton').attr('disabled','disabled');
        $('.loaderSiteMap').fadeIn('fast');
        $('#siteMapProgressBar').css('width', '0%');
        siteMapsGenerators();
    });
    
    function siteMapsGenerators(){
        $.ajax({
               type: "POST",
               url: "sitemap",
               data: {'counter' : counter},
               success: function(msg){
                   console.log(msg);
                   var serverCounter = parseInt(msg);
                 //alert( serverCounter );
                 progress += 11;
                 $('#siteMapProgressBar').css('width', progress + '%');
                 if( serverCounter  ==  9 ){
                     $('.loaderSiteMap').fadeOut('slow');
                     counter = 0;
                     alert('Sitemap успешно создан');
                 }
                 counter++;
                 if( serverCounter != 9 ){
                     siteMapsGenerators();
                 }
               }
             });
    }
    
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);


?>
