<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>



<section class="tile">

    <!-- tile header -->
    <div class="tile-header dvd dvd-btm">
        <h1 class="custom-font"><strong><?=$this->title; ?></strong> </h1>
        <ul class="controls">

        </ul>
    </div>
    <!-- /tile header -->

    <!-- tile body -->
    <div class="tile-body">

        <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'active',
                        'format'=>'raw',
                        'options' => ['width' => '110'],
                        'value'=>function($data){
                            return $data->listActive[$data->active];
                        },
                        'filter'=>$searchModel->listActive,
                    ],
                    'created:datetime',
                    'name',
                    'last_name',
                    'phone',
                    'email:email',
                    [
                        'attribute' => 'active',
                        'format'=>'raw',
                        'value' => function($model){
                            return $model->listActive[$model->active];
                        },
                        'filter' => $searchModel->listActive,
                    ],
                    [
                        'attribute' => 'type',
                        'format'=>'raw',
                        'value' => function($model){
                            return $model->listType[$model->type];
                        },
                        'filter' => $searchModel->listType,
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template'=>'{view}{delete}',
                    ],
                ],
            ]); ?>
        </div>

    </div>
    <!-- /tile body -->

</section>
