<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model common\models\Contact */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Контакты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="tile">

    <!-- tile header -->
    <div class="tile-header dvd dvd-btm">
        <h1 class="custom-font"><strong> КОНТАКТ : </strong><?=$this->title ?></h1>

    </div>
    <!-- /tile header -->

    <!-- tile body -->
    <div class="tile-body">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'last_name',
            'phone',
            'email:email',
            'text:html',

            'created:datetime',
            [
                'label' => 'Статус',
                'format'=>'raw',
                'value' => $model->listActive[$model->active]
            ],
            [
                'label' => 'Тип',
                'format'=>'raw',
                'value' => $model->listType[$model->type]
            ],
            
        ],
    ]) ?>

    </div>
</section>