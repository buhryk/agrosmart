<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подписчики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-index">

    <?
    $gridColumns = [
        'id',
        'email:email',
        'created:datetime',
		 ['attribute' => '',
                        'format'=>'raw',
                        'options' => ['height' => '100'],
                        'value'=>function($data){

                            $view="
                                <a href=\"".\yii\helpers\Url::to(['delete','id'=>$data->id])."\" title=\"Удалить\" aria-label=\"Удалить\" data-confirm=\"Вы уверены, что хотите удалить этот элемент?\" data-method=\"post\" data-pjax=\"0\">
                                    <span class=\"glyphicon glyphicon-trash\"></span>
                                </a>";

                            return $view;
                        },
                    ],
    ];

    $fullExportMenu = ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'pjaxContainerId' => 'kv-pjax-container',
        'dropdownOptions' => [
            'label' => 'Full',
            'class' => 'btn btn-default',
            'itemsBefore' => [
                '<li class="dropdown-header">Експорт</li>',
            ],
        ],
    ]);

    ?>
    <?
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i> Подписчики</h3>',
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [

            $fullExportMenu,
           
        ]
    ]);

    ?>
</div>
