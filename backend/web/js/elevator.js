$(document).ready(function () {
    if (typeof(locations) !== 'undefined' && locations) {
        var bounds = new google.maps.LatLngBounds();

        var map = new google.maps.Map(document.getElementById('elevators_map'), {
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var markers = [];

        var infowindow = new google.maps.InfoWindow();

        var marker, i;
        var directionsDisplay = new google.maps.DirectionsRenderer();
        var directionsService = new google.maps.DirectionsService();

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            markers[i] = marker;

            bounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);

                    var from_location = $('#from_location').val();
                    if (from_location && i != 0) {
                        var geocoder = new google.maps.Geocoder();

                        geocoder.geocode({
                            'address': from_location
                        },function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var request = {
                                    origin: new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng()), //точка старта
                                    destination: new google.maps.LatLng(locations[i][1],locations[i][2]), //точка финиша
                                    travelMode: google.maps.DirectionsTravelMode.DRIVING //режим прокладки маршрута
                                };

                                directionsService.route(request, function(response, status) {
                                    if (status == google.maps.DirectionsStatus.OK) {
                                        directionsDisplay.setDirections(response);
                                    }
                                });

                                directionsDisplay.setMap(map);
                            }
                        });
                    }
                }
            })(marker, i));
        }


        map.fitBounds(bounds);
    }
});
