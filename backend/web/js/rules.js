$(document).ready(function () {
    $('.check-all-module').on('change', function () {
        var checkBoxes = $('[data-module-id="'+$(this).data('module-id')+'"]');
        if ($(this).is(':checked')) {
            checkBoxes.prop('checked', true);
        } else {
            checkBoxes.prop('checked',false);
        }
    });

    $('.check-all-controller').on('change', function () {
        var checkBoxes = $('[data-controller-id="'+$(this).data('controller-id')+'"]');
        console.log(checkBoxes);
        if ($(this).is(':checked')) {
            checkBoxes.prop('checked', true);
        } else {
            checkBoxes.prop('checked',false);
        }
    });

    $('#submit-rules-form').on('click', function () {
        var form = $('#rules-form');

        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serializeArray(),

            success: function(data) {
                if (data.status) {
                    location.reload();
                }
            }
        });
    });

    $('.change-position').on('click', function () {
        $.ajax({
            method: 'post',
            url: $(this).data('url'),
            dataType: 'json',

            success: function(data) {
                console.log(data);

                if (data.status) {
                    location.reload();
                } else {
                    alert('Something wrong. Please, reload page.')
                }
            }
        });
    });
});
