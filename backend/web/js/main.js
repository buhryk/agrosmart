/**
 * Created by Borys on 02.03.2017.
 */
$('.advetising-item .checkbox').on('click', function () {
    var element = $(this).parents('.advetising-item');
    if(element.hasClass('active')){
        element.removeClass('active');
    } else {
        element.addClass('active');
        setDateStart(element);
    }
})
function setDateStart(element) {
    var currentDate = element.find('input#advertisingmodel-start').val(),
        date = element.attr('data-date'),
        dateObject = new Date(date),
        currentDateObject = new Date(currentDate);

    if(dateObject.valueOf() < currentDateObject){
        $('input#advertisingmodel-start').val(date);
    }else if(date!=undefined){
        $('input#advertisingmodel-start').val(date);
    }

}