$(document).ready(function(){
    var geocoder;
    var input = $('.map_canvas');
    var address = input.data('location');
    var coordinatesInput = $('#coordinates');
    var coordinates = coordinatesInput.val();

    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var myOptions = {
        zoom: 16,
        center: latlng,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        navigationControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map_canvas_container"), myOptions);

    if (coordinates) {
        var coordinatesArray = coordinates.split(',');
        var location = {lat: Number(coordinatesArray[0]), lng: Number(coordinatesArray[1])};
        map.setCenter(location);

        var infowindow = new google.maps.InfoWindow({
            content: '<b>' + address + '</b>',
            size: new google.maps.Size(150, 50)
        });

        var marker = new google.maps.Marker({
            position: location,
            map: map,
            title: address
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });
    } else if (address) {
        function initialize() {
            if (geocoder) {
                geocoder.geocode({
                    'address': address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                            if (coordinatesInput.length) {
                                coordinatesInput.val(results[0].geometry.location.lat()+','+results[0].geometry.location.lng());
                            }

                            map.setCenter(results[0].geometry.location);

                            var infowindow = new google.maps.InfoWindow({
                                content: '<b>' + address + '</b>',
                                size: new google.maps.Size(150, 50)
                            });

                            var marker = new google.maps.Marker({
                                position: results[0].geometry.location,
                                map: map,
                                title: address
                            });

                            google.maps.event.addListener(marker, 'click', function() {
                                infowindow.open(map, marker);
                            });

                        } else {
                            alert($('.map_canvas').data('error-message-no-results'));
                        }
                    } else {
                        alert($('.map_canvas').data('error-message-by-reason') + ': ' + status);
                    }
                });
            }
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
});