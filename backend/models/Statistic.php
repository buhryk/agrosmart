<?php

namespace backend\models;

use backend\modules\advertising\models\Advertising;
use frontend\modules\product\models\Advertisement;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AuthRule;
use frontend\models\User;
use frontend\models\Company;

 class Statistic
 {
     public $lastDay =  86400;
     public $lastWeek = 604800;

     public function __construct()
     {
         $this->lastDay = time() - $this->lastDay;
         $this->lastWeek = time() - $this->lastWeek;
     }

     public function getData()
     {
         $catch = Yii::$app->cache;

         $data = $catch->get('site-statistic');
         if(!$data){
             $data['user'] = $this->getUser();
             $data['company'] = $this->getCompany();
             $data['product'] = $this->getProduct();
             $data['analytic'] = $this->getAnalytic();

             $catch->set('site-statistic', $data);
         }

         return $data;
     }

     private function getUser()
     {
         $mas['last_day'] = User::find()
             ->where(['status' => User::STATUS_ACTIVE])
             ->andWhere(['>=', 'created_at' , $this->lastDay])
             ->count();

         $mas['last_week'] = User::find()
             ->where(['status' => User::STATUS_ACTIVE])
             ->andWhere(['>=', 'created_at' , $this->lastWeek])
             ->count();

         $mas['count'] = User::find()
             ->where(['status' => User::STATUS_ACTIVE])
             ->count();

         return $mas;
     }

     private function getProduct()
     {
         $mas['last_day'] = Advertisement::find()
             ->where(['status' => Advertisement::STATUS_ACTIVE_YES])
             ->andWhere(['>=', 'created_at' , $this->lastDay])
             ->count();

         $mas['last_week'] = Advertisement::find()
             ->where(['status' => Advertisement::STATUS_ACTIVE_YES])
             ->andWhere(['>=', 'created_at' , $this->lastWeek])
             ->count();

         $mas['count'] = Advertisement::find()
             ->where(['status' => Advertisement::STATUS_ACTIVE_YES])
             ->count();

        return $mas;
     }

     private function getCompany()
     {
         $mas['last_day'] = Company::find()
             ->where(['status' => Company::STATUS_ACTIVE_YES])
             ->andWhere(['>=', 'created_at' , $this->lastDay])
             ->count();

         $mas['last_week'] = Company::find()
             ->where(['status' => Company::STATUS_ACTIVE_YES])
             ->andWhere(['>=', 'created_at' , $this->lastWeek])
             ->count();

         $mas['count'] = Company::find()
             ->where(['status' => Company::STATUS_ACTIVE_YES])
             ->count();

         return $mas;
     }

     private function getAnalytic()
     {
         $obj = new Metrika();
         $time=time();
         $month = $time - 30 * 24 * 3600;

         $metrika  = $obj->get_traffic(
             date("Ymd",  $month),
             date("Ymd", $time ),
             'traffic/summary'
         );

         return $this->convert($metrika['data']);
     }
     
     private function convert($dataList)
     {
         $data = [];
         if(!is_array($dataList)) return '';
         foreach ($dataList as $item) {
             $data[] = [
                 'y' => $item['dimension']['name'],
                 'a' => $item['metrics'][0],
                 'b' =>  $item['metrics'][1]
             ];
         }
         
         return json_encode($data);
     }
 }