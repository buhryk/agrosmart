<?php
/**
 * Created by PhpStorm.
 * User: Boris
 * Date: 02.12.2015
 * Time: 15:34
 */
namespace backend\models;


class Metrika {

    private $token 		= "AQAAAAAOdy-_AAQvlZPsb6Agq0TCs88iepKCmMc";
    private $url_api 	= "https://api-metrika.yandex.ru/stat/v1/data/drilldown";
    private $counter_id = "43631399";

    static function getDate()
    {
		
        $time=time();
        $date['today_at']=$time;
        $day['today'] = $time ;

        $date['last']=$time-24 * 3600;
        $day['today'] = $time-24 * 3600 ;

        $date['week'] = $time - 7 * 24 * 3600;
        $date['pre_week'] = $time - 14 * 24 * 3600;

        $date['month'] = $time - 30 * 24 * 3600;
        $date['pre_month'] = $time - 60 * 24 * 3600;
        $date['year'] = $time - 365 * 24 * 3600;
        return $date;
    }


    public function get_traffic($date1, $date2,$type){

        return $this->get_data(
            $this->url_api.
            "?id=".
            $this->counter_id.
      		"&metrics=ym:s:visits,ym:s:pageviews".
			"&dimensions=ym:s:date".
			"&sort=ym:s:date".
            "&date1=".$date1.
            "&date2=".$date2.
            "&oauth_token=".
            $this->token
        );
    }

    private function get_data($url){

        if($url){

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $data = json_decode(
                curl_exec($ch), true
            );

            curl_close($ch);

            if(is_array($data)){

                return $data;
            }
        }

        return false;
    }
}



