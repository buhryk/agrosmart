<?php
namespace backend\assets;

use yii\web\AssetBundle;


class ChartAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendor/morris/morris.css'
    ];


    public $js = [
        'vendor/raphael/raphael-min.js',
        'vendor/morris/morris.js'
    ];
    public $depends = [
        'yii\web\YiiAsset'
    ];
}
