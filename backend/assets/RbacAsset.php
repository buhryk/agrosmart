<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Widget asset bundle
 */
class RbacAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@backend/assets/rbac';

    public $js = [
        'js/users.js'
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
