<?php

namespace backend\widgets;

use yii\base\Widget;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;

class ImagesWidget extends Widget {

    public $model;
    public $parameters;
    public $table_name;
    public $image;
    public $imageLang;

    public function init() {
        parent::init();
        $this->table_name = isset($this->parameters['table_name']) ? $this->parameters['table_name'] : null;

        $this->image = new Image();
        $this->image->table_name = $this->table_name;
        $this->image->record_id = $this->model->primaryKey;
        $this->image->sort = count($this->model->images)+1;
        $this->imageLang = new ImageLang();
        $this->imageLang->lang = \Yii::$app->language;
    }

    public function run() {
        return $this->render('images', [
            'model' => $this->model,
            'image' => $this->image,
            'imageLang' => $this->imageLang
        ]);
    }

}