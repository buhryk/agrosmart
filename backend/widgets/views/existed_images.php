<?php
use yii\bootstrap\Html;
use backend\modules\images\models\Image;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use mihaildev\elfinder\InputFile;
?>

<h2><?= Yii::t('common', 'Pined image'); ?></h2>
<table class="table table-bordered">
    <thead>
    <tr>
        <th width="50">#</th>
        <th><?= Yii::t('common', 'Image'); ?></th>
        <th><?= Yii::t('common', 'Main image'); ?></th>
        <th><?= Yii::t('common', 'Is show'); ?></th>
        <th><?= Yii::t('common', 'Title'); ?></th>
        <th>Alt</th>
        <th>&nbsp;</th>
        <th width="140"><?= Yii::t('common', 'Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($model->images as $key => $image) { ?>
        <?php
        $imageLang = $image->lang;
        if (!$imageLang) {
            $imageLang = $image->getNewImageLang();
        } ?>
        <tr class="<?= $image->is_main == Image::IS_MAIN_YES ? 'success' : ''; ?>">
            <?php $form = ActiveForm::begin([
                'options' => [
                    'class' => 'image-form',
                    'id' => 'image-form-'.$key,
                    'onsubmit' => 'return false',
                    'data-url' => Url::to(['/images/image/update', 'id' => $image->primaryKey])
                ],
            ]); ?>

            <?= $form->field($image, 'table_name')->hiddenInput()->label(false); ?>
            <?= $form->field($image, 'record_id')->hiddenInput()->label(false); ?>
            <?= $form->field($imageLang, 'lang')->hiddenInput()->label(false); ?>

            <td><?= $key; ?></td>

            <td style="text-align: center;">
                <?= $form->field($image, 'path')->widget(InputFile::className(), [
                    'language'      => 'ru',
                    'controller'    => 'elfinder',
                    'filter'        => 'image',
                    'path'          => 'news',
                    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                    'options'       => ['class' => 'form-control', 'id' => 'image-'.$key],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple'      => false,
                ])->label(false); ?>
                <?= Html::img($image->path, ['width' => 150, 'style' => 'margin-top: -5px']); ?>
            </td>
            <td>
                <?= $form->field($image, 'is_main')->dropDownList(array(
                    Image::IS_MAIN_NO => Yii::t('common', 'No'),
                    Image::IS_MAIN_YES => Yii::t('common', 'Yes')
                ))->label(false); ?>
            </td>
            <td>
                <?= $form->field($image, 'active')->dropDownList(array(
                    Image::ACTIVE_YES => Yii::t('common', 'Yes'),
                    Image::ACTIVE_NO => Yii::t('common', 'No')
                ))->label(false); ?>
            </td>
            <td>
                <?= $form->field($imageLang, 'title')->textarea(['style' => 'resize: none'])->label(false); ?>
            </td>
            <td>
                <?= $form->field($imageLang, 'alt')->textarea(['style' => 'resize: none'])->label(false); ?>
            </td>
            <td>
                <div class="form-group" style="text-align: center;">
                    <?= Html::submitButton(Yii::t('common', 'Update'), [
                        'class' => 'btn btn-primary submit-image-form',
                        'data-form-id' => 'image-form-'.$key
                    ]); ?>
                </div>
                <div class="errors-block-image-form-<?= $key; ?>" style="color: red;"></div>
            </td>
            <td>
                <a title="<?php echo Yii::t('common', 'Up image'); ?>"
                   class="btn btn-default btn block left change-position"
                   data-url="<?= Url::to(['/images/image/up', 'id' => $image->primaryKey]); ?>"
                   onclick="return false"
                >
                    <span class="glyphicon glyphicon-arrow-up"></span>
                </a>
                <a title="<?php echo Yii::t('common', 'Down image'); ?>"
                   class="btn btn-default btn block left change-position"
                   data-url="<?= Url::to(['/images/image/down', 'id' => $image->primaryKey]); ?>"
                >
                    <span class="glyphicon glyphicon-arrow-down"></span>
                </a>
                <a title="<?php echo Yii::t('common', 'Delete'); ?>"
                   class="btn btn-default btn block left remove-image"
                   data-url="<?= Url::to(['/images/image/delete', 'id' => $image->primaryKey]); ?>"
                >
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
            </td>
            <?php ActiveForm::end(); ?>
        </tr>

    <?php } ?>
    </tbody>
</table>


