<?php
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'meta_title') ?>
    <?= $form->field($modelLang, 'meta_description')->textarea() ?>
    <?= $form->field($modelLang, 'meta_keywords')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>