<?php
use backend\assets\ImagesModuleAsset;
ImagesModuleAsset::register($this);
?>

<div class="table-responsive">
    <h2><?php echo Yii::t('common', 'Add new image'); ?></h2>

    <?= $this->render('images_form_create', [
        'image' => $image,
        'imageLang' => $imageLang,
        'model' => $model,
        'options' => [
            'action' => 'create'
        ]
    ]); ?>

    <?= $this->render('existed_images', [
        'model' => $model
    ]); ?>
</div>
