<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$module = Yii::$app->controller->module->id;
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$pathInfo = '/'.Yii::$app->request->pathInfo;
?>

<nav class="navbar navbar-default sidebar" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php foreach ($items as $item) { ?>
                    <?php if ($item['submenus']) { ?>
                        <?php $class = ''; ?>
                        <?php if (in_array($pathInfo, array_merge(ArrayHelper::getColumn($item['submenus'], 'path'), [$item['path']]))) {
                            $class = 'open';
                        } ?>
                        <li class="dropdown <?= $class; ?>">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <?= $item['title']; ?>
                                <?= $item['notification'] ? ('<span class="admin-menu-notification">'.$item['notification'].'</span>') : ''; ?>
                                <span class="caret"></span>
                                <span class="pull-right hidden-xs showopacity <?= $item['icon']; ?>"></span>
                            </a>
                            <?php if ($item['submenus']) { ?>
                                <ul class="dropdown-menu forAnimate" role="menu">
                                    <?php foreach ($item['submenus'] as $submenu) { ?>
                                        <?php $class = ($submenu['path'] == $pathInfo) ? 'active' : ''; ?>
                                        <li class="<?= $class; ?>">
                                            <a href="<?= Url::to([$submenu['path']]) ; ?>"><?= $submenu['title']; ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } else { ?>
                        <?php $class = $pathInfo == $item['path'] ? 'active' : ''; ?>
                        <li class="single <?= $class; ?>">
                            <a href="<?= $item['path']; ?>">
                                <?= $item['title']; ?>
                                <?= $item['notification'] ? ('<span class="admin-menu-notification">'.$item['notification'].'</span>') : ''; ?>
                                <span class="pull-right hidden-xs showopacity <?= $item['icon']; ?>"></span>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>


