<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 02.12.16
 * Time: 12:06
 */

namespace backend\widgets;

use backend\modules\accesscontrol\models\Role;
use backend\modules\contact\models\Contact;
use backend\modules\price2\models\Price;
use frontend\models\User;
use frontend\modules\complaint\models\Complaint;
use frontend\modules\ordertransport\models\Order;
use frontend\modules\product\models\Advertisement;
use yii\base\Widget;
use backend\modules\adminmenu\models\Menu;
use yii\helpers\ArrayHelper;

class MainAdminMenuWidget extends Widget {

    public $items = [];

    public function init() {
        parent::init();

        if (!\Yii::$app->user->isGuest) {
            $menus = Menu::find()->where(['parent_id' => null]);

            if (\Yii::$app->user->identity->role_id != Role::SUPERADMIN_ROLE_ID) {
                $menus = $menus->andWhere(['in', 'id', ArrayHelper::getColumn(
                    \Yii::$app->user->identity->role->adminMenus, 'menu_id')]);
            }

            $menus = $menus->all();

            foreach ($menus as $menu) {
                $notification = 0;
                if (in_array($menu->path, ['/ordertransport/order/index', '/complaint/complaint/index', '/advertisement/advertisement/index', '/contact/contact/index', '/consumer/user/index', '/price2/price/index'])) {
                    $notification = $this->getNotification($menu->path);
                }

                $oneItem = [
                    'title' => $menu->title,
                    'path' => $menu->path,
                    'icon' => $menu->icon,
                    'notification' => $notification,
                    'submenus' => []
                ];

                $submenus = $menu->submenus;
                if ($submenus) {
                    $submenusArray = [];
                    foreach ($submenus as $submenu) {
                        $notification = 0;
                        $submenusArray[] = [
                            'title' => $submenu->title,
                            'path' => $submenu->path,
                            'icon' => $submenu->icon,
                            'notification' => $notification,
                        ];
                    }

                    $oneItem['submenus'] = $submenusArray;
                }

                $this->items[] = $oneItem;
            }
        }
    }

    public function run() {
        return $this->render('menu', [
            'items' => $this->items
        ]);
    }

    private function getNotification($path)
    {
        switch ($path) {
            case '/ordertransport/order/index':
                return Order::find()->where(['status' => Order::STATUS_NEW])->count('id');
            case '/complaint/complaint/index':
                return Complaint::find()->where(['status' => Complaint::STATUS_NEW])->count('id');
            case '/advertisement/advertisement/index':
                return Advertisement::find()->where(['admin_status' => Advertisement::ADMIN_STATUS_NEW])->count('id');
            case '/contact/contact/index':
                return Contact::find()->where(['status' => Contact::STATUS_NEW])->count('id');
            case '/consumer/user/index':
                return User::find()->where(['is_new' => User::USER_NEW])->count('id');
            case '/price2/price/index':
                return Price::find()->where(['admin_status' => Price::ADMIN_STATUS_NEW])->count('id');
            default:
                return 0;
        }
    }

}