<?php

namespace backend\widgets;

use yii\bootstrap\Widget;
use yii\helpers\Json;
use yii\web\View;


class Alert extends Widget
{
    /**
     * @var array типы сообщений
     */
    public $alertTypes = [
        'success',
        'info',
        'warning',
        'error',
    ];

    /**
     * @var array опции
     */
    public $alertOptions = [
        'closeButton' => false,
        'debug' => false,
        'newestOnTop' => false,
        'progressBar' => true,
        'positionClass' => 'toast-top-right',
        'preventDuplicates' => false,
        'onclick' => null,
        'showDuration' => '300',
        'hideDuration' => '1000',
        'timeOut' => '5000',
        'extendedTimeOut' => '1000',
        'showEasing' => 'swing',
        'hideEasing' => 'linear',
        'showMethod' => 'fadeIn',
        'hideMethod' => 'fadeOut'
    ];

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();

        $session = \Yii::$app->session;
        $flashes = $session->getAllFlashes();

        if (count($flashes) == 0)
            return false;

        $this->view->registerJs("toastr.options = " . Json::encode($this->alertOptions), View::POS_LOAD);

        foreach ($flashes as $type => $data) {
            $types = array_flip($this->alertTypes);

            if (isset($types[$type])) {
                $data = (array)$data;

                foreach ($data as $i => $message) {
                    $this->view->registerJs("
                        toastr['{$type}']('{$message}');
                    ", View::POS_LOAD);
                }

                $session->removeFlash($type);
            }
        }
    }
}
