<?php
namespace backend\controllers;

use backend\models\Statistic;
use common\helpers\SitemapHelper;
use common\helpers\SitemapHelper2;
use common\models\User;
use frontend\modules\product\models\Advertisement;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'sitemap', 'test'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'image-upload', 'add-image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => '/uploads/images/', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/images' // Or absolute path to directory where files are stored.
            ],
            'get-image' => [
                'class' => 'vova07\imperavi\actions\GetImagesAction',
                'url' => '/uploads/images/',
                'path' => '@frontend/web/uploads/images',
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']],
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->request->get('clear-cache')) {
            \Yii::$app->cache->flush();
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Cache cleared'));
            return $this->redirect(['index']);
        }

        $statistic = new Statistic();
        $data = $statistic->getData();

      
        return $this->render('index', $data);

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout='error';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionTest()
    {
        Advertisement::find();
    }

    public function actionSitemap()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $counter = Yii::$app->request->post('counter');
            $counter = isset($counter) ? $counter : null;
            $sitemapHelper = new SitemapHelper2($counter);
            $sitemapHelper->createSitemap($counter);
            exit;
        }
        //$sitemapHelper = new SitemapHelper();
        //$sitemapHelper->createSitemap();

        return $this->render('sitemap');
    }
}
