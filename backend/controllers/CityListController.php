<?php

namespace backend\controllers;

use Yii;
use yii\db\Query;
use backend\modules\commondata\models\City;

class CityListController extends \yii\web\Controller
{
    public function actionIndex($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select(["city.id AS id, IF(area_lang.title IS NULL, CONCAT(city_lang.title, ', ', oblast_lang.title), CONCAT(city_lang.title, ', ', oblast_lang.title, ', ', area_lang.title))  AS text"])
                ->from('city')
                ->join('LEFT JOIN', 'city_lang', 'city.id = city_lang.city_id')
                ->join('LEFT JOIN', 'oblast_lang', 'city.oblast_id = oblast_lang.oblast_id')
                ->join('LEFT JOIN', 'area_lang', 'city.area_id=area_lang.area_id')
                ->where('city_lang.title LIKE :query')
                ->andWhere('city_lang.lang = :lang')
                //->andWhere('area_lang.lang = :lang')
                ->andWhere('oblast_lang.lang = :lang')
                ->orderBy(['city.type' => SORT_ASC])
                ->limit(100)
                ->addParams([':lang' => \Yii::$app->language, ':query'=>$q.'%']);
            $command = $query->createCommand();

            $data = $command->queryAll();


            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $city= City::findOne($id);
            $out['results'] = ['id' => $id, 'text' => $city->title];
        }
        return $out;
    }

    public function actionGetHtmlList()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();

            if (isset($data['number'])) {
                $number = (int)$data['number'];

                return $this->renderAjax('select-address-widget', ['number' => $number]);
            }
        }
    }
}
