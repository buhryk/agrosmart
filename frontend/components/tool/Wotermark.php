<?php

namespace frontend\components\tool;

use Yii;
use yii\base\Model;
use tpmanc\imagick\Imagick;
/**
 * ContactForm is the model behind the contact form.
 */
class Wotermark
{

    public static function set($filename) {

        $dir= $_SERVER['DOCUMENT_ROOT'].'/frontend/web';

        $xSize = '20%';
        $ySize = '20%';
        $xPosition = 'right';
        $yPosition = 'top';
        $wotermark=$dir.'/images/wotermark/wotermark.png';
        $file=$dir.$filename;
        Imagick::open($file)->watermark($wotermark,$xPosition,$yPosition,$xSize,$ySize)->saveTo($file);
    }

}
