<?php

namespace frontend\components\tool;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Image
{
    public static function resize($filename, $width, $height)
    {
        $dir = Yii::getAlias('@frontend/web');

        if (!is_file($dir . $filename)) {
            return '';
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $old_image = $filename;
        $new_image = '/cache' . mb_substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file($dir . $new_image) || (filectime($dir. $old_image) > filectime($dir . $new_image))) {
            $path = '';
            $directories = explode('/', dirname(str_replace('../', '', $new_image)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir($dir . $path)) {
                    @mkdir($dir. $path, 0777);
                }
            }

            $image_info = getimagesize($dir . $old_image);

            if ($image_info['0']  != $width || $image_info['1']  != $height) {
                $image = new \frontend\components\library\Image($dir . $old_image);
                $image->resize($width, $height);
                $image->save($dir . $new_image);
            } else {
                copy($dir . $old_image, $dir . $new_image);
            }
        }

        return $new_image;
    }

    /*public static function crop($filename, $width, $height)
    {
        $dir = $_SERVER['DOCUMENT_ROOT'];

        if (!is_file($dir . $filename)) {
            return '';
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $old_image = $filename;
        $new_image = '/cache' . mb_substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file($dir . $new_image) || (filectime($dir. $old_image) > filectime($dir . $new_image))) {
            $path = '';
            $directories = explode('/', dirname(str_replace('../', '', $new_image)));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir($dir . $path)) {
                    @mkdir($dir. $path, 0777);
                }
            }

            $image_info = getimagesize($dir . $old_image);

            if ($image_info['0']  != $width || $image_info['1']  != $height) {
                $image = new \frontend\components\library\Image($dir . $old_image);
                $image->crop();
                $image->save($dir . $new_image);
            } else {
                copy($dir . $old_image, $dir . $new_image);
            }
        }

        echo '<pre>';
        die(var_dump($new_image));

        return $new_image;
    }*/
}