<?php

namespace frontend\widgets;


use yii\bootstrap\Widget;
use backend\modules\mainservices\models\Block;

class MainServicesWidget extends Widget
{
    public $blocks;

    public function init()
    {
        $this->blocks = Block::find()
            ->where(['active' => Block::ACTIVE_YES])
            ->joinWith('lang')
            ->orderBy('sort ASC')
            ->groupBy(Block::tableName().'.id')
            ->all();
    }

    public function run() {
        return $this->render('main_services_widget', [
            'models' => $this->blocks
        ]);
    }
}