<?php
use yii\helpers\Url;
use frontend\modules\elected\models\Elected;

$user = $model->user;
?>

<?php if ($user) { ?>
    <div class="one-block">
        <a href="<?= Url::to(['/user/user/view', 'id' => $user->id]); ?>">
            <?= $user->fullName; ?>
        </a>
        <a role="button"
           class="square-close drop-from-elected"
           data-type="<?= Elected::ALIAS_USER; ?>"
           data-record_id="<?= $user->primaryKey; ?>"
        >
            &#10006;
        </a>
    </div>
<?php } ?>