<div class="one-block">
    <div><?= $model->address; ?></div>
    <a role="button"
       class="square-close drop-from-saved-address"
       data-address="<?= $model->address; ?>"
    >
        &#10006;
    </a>
</div>
