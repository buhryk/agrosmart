<?php
use backend\modules\mainservices\models\Block;
use yii\helpers\Html;
?>

<div class="container">
    <?php foreach ($models as $model): ?>
        <?php $mainImage = $model->image; ?>
        <div class="col-sm-3">
            <?php if ($model->type == Block::TYPE_CHAPTER) { ?>
                <a href="<?= $model->url; ?>">
                    <figure class="effect-sarah">
                        <?php if ($mainImage) { ?>
                            <?php echo Yii::$app->thumbnail->img($mainImage,
                                ['thumbnail' => ['width' => 291, 'height' => 233]]
                            ); ?>
                        <?php }
                        else { ?>
                            <?php echo Yii::$app->thumbnail->placeholder(['width' => 291, 'height' => 233, 'text' => '291x233']); ?>
                        <?php } ?>
                        <figcaption>
                            <div class="table-cell">
                                <p><?= $model->title; ?></p>
                            </div>
                        </figcaption>
                    </figure>
                </a>
            <?php } else { ?>
                <a href="<?= $model->url; ?>">
                    <?php if ($mainImage) { ?>
                        <?php echo Html::img($mainImage, ['width' => 291, 'height' => 233]); ?>
                    <?php } else { ?>
                        <?php echo Yii::$app->thumbnail->placeholder(['width' => 291, 'height' => 233, 'text' => '291x233']); ?>
                    <?php } ?>
                </a>
            <?php } ?>
        </div>
    <?php endforeach; ?>
</div>