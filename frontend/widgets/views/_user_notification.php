<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 03.03.2017
 * Time: 10:26
 */
?>
<div class="subscription <?=$models ? '' : 'none' ?>" style="visibility: visible;">

        <p class="subscription__message"><?=$models ? $models[0]->message : ''?></p>

        <a href="<?= \yii\helpers\Url::to('/notification/user-notify/index');?>">
        <div class="subscription-info" title="<?=Yii::t('common', 'Информация') ?>">
                            <span class="subscription__info">
                                <img src="<?= Yii::getAlias('@web')?>/images/i.png" alt="">
                            </span>
            <span class="subscription__info-number"><?= count($models); ?></span>
        </div>
        </a>
</div>
