<div class="widgets">
    <div class="saves saves-elected">
        <img src="/images/part-1.png" alt="">
        <div class="info-window info-window-elected">
            <ul class="nav nav-pills">
                <?php foreach ($elected as $key => $data) { ?>
                    <li class="<?= $key == 'advertisement' ? 'active' : ''; ?>">
                        <a href="#elected_<?= $key; ?>" data-toggle="tab"><?= $data['title']; ?></a>
                    </li>
                <?php } ?>
            </ul>
            <div class="tab-content clearfix">
                <?php foreach ($elected as $key => $data) { ?>
                    <div class="tab-pane <?= $key == 'advertisement' ? 'active' : ''; ?>" id="elected_<?= $key; ?>">
                        <?php if (!$data['data']) { ?>
                            <p><?= Yii::t('common', 'There is empty now'); ?></p>
                        <?php } ?>
                        <?php foreach ($data['data'] as $one) { ?>
                            <?= $this->render('_users_data_'.$key, ['model' => $one]); ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="saves saves-saves">
        <img src="/images/saved_addresses.png" alt="">
        <div class="info-window info-window-saves">
            <ul class="nav nav-pills">
                <?php foreach ($saved as $key => $data) { ?>
                    <li class="<?= $key == 'address' ? 'active' : ''; ?>">
                        <a href="#saved_<?= $key; ?>" data-toggle="tab"><?= $data['title']; ?></a>
                    </li>
                <?php } ?>
            </ul>
            <div class="tab-content clearfix">
                <?php foreach ($saved as $key => $data) { ?>
                    <div class="tab-pane <?= $key == 'address' ? 'active' : ''; ?>" id="saved_<?= $key; ?>">
                        <?php if (!$data['data']) { ?>
                            <p><?= Yii::t('common', 'There is empty now'); ?></p>
                        <?php } ?>
                        <?php foreach ($data['data'] as $one) { ?>
                            <?= $this->render('_users_saved_'.$key, ['model' => $one]); ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>