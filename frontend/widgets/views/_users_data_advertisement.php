<?php 
use yii\helpers\Url;
use frontend\modules\elected\models\Elected;

$advertisement = $model->advertisement; 
?>

<?php if ($advertisement) { ?>
    <div class="one-block">
        <a href="<?= Url::to(['/product/product/view', 'slug' => $advertisement->alias]); ?>">
            <?= $advertisement->title; ?>
        </a>
        <a role="button"
           class="square-close drop-from-elected"
           data-type="<?= Elected::ALIAS_ADVERTISEMENT; ?>"
           data-record_id="<?= $advertisement->primaryKey; ?>"
        >
            &#10006;
        </a>
    </div>
<?php } ?>