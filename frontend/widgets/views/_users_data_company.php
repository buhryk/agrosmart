<?php
use yii\helpers\Url;
use frontend\modules\elected\models\Elected;

$company = $model->company;
?>

<?php if ($company) { ?>
    <div class="one-block">
        <a href="<?= Url::to(['/company/company/view', 'id' => $company->primaryKey]); ?>">
            <?= $company->name; ?>
        </a>
        <a role="button"
           class="square-close drop-from-elected"
           data-type="<?= Elected::ALIAS_COMPANY; ?>"
           data-record_id="<?= $company->primaryKey; ?>"
        >
            &#10006;
        </a>
    </div>
<?php } ?>