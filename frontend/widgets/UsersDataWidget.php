<?php

namespace frontend\widgets;

use frontend\modules\elected\models\Elected;
use frontend\modules\saveddata\models\SavedAddress;
use yii\bootstrap\Widget;
use yii\helpers\ArrayHelper;
use Yii;

class UsersDataWidget extends Widget
{
    public $user;
    public $elected = [
        'advertisement' => [
            'title' => 'Товары/Услуги',
            'data' => []
        ],
        'company' => [
            'title' => 'Компании',
            'data' => []
        ],
        'consumer' => [
            'title' => 'Пользователи',
            'data' => []
        ]
    ];

    public $saved = [
        'address' => [
            'title' => 'ADRESA',
            'data' => []
        ]
    ];

    public function init()
    {
        $this->user = \Yii::$app->user->identity;

        if ($this->user) {
            $electedData = Elected::find()
                ->where(['user_id' => $this->user->id])
                ->orderBy('table_name DESC')
                ->addOrderBy('created_at DESC')
                ->all();

            foreach ($electedData as $one) {
                $this->elected[$one->table_name]['data'][] = $one;
            }

            $this->saved['address']['data'] = $this->user->savedAddresses;
        }
    }

    public function run() {
        if ($this->user) {
            $data = [
                'user' => $this->user,
                'elected' => $this->elected,
                'saved' => $this->saved
            ];
            $data = $this->setChaptersTitles($data);

            return $this->render('_users_data', $data);
        }
    }

    private function setChaptersTitles($data)
    {
        foreach ($data['elected'] as $key => $title) {
            switch ($key) {
                case 'advertisement':
                    $data['elected'][$key]['title'] = Yii::t('common', 'Products/Services');
                    break;
                case 'company':
                    $data['elected'][$key]['title'] = Yii::t('common', 'Companies');
                    break;
                case 'consumer':
                    $data['elected'][$key]['title'] = Yii::t('users', 'USERS');
                    break;
            }
        }

        foreach ($data['saved'] as $key => $title) {
            switch ($key) {
                case 'address':
                    $data['saved'][$key]['title'] = Yii::t('common', 'Addresses');
                    break;
            }
        }

        return $data;
    }
}
