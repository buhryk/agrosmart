<?php

namespace frontend\widgets;


use frontend\modules\notification\models\Notification;
use yii\bootstrap\Widget;
use backend\modules\mainservices\models\Block;

class NotificationWidget extends Widget
{

    public function init()
    {

    }

    public function run() {

        return $this->render('_user_notification', [
            'models' => Notification::findNewUserNotification()
        ]);
    }
}