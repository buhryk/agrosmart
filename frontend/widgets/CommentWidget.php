<?php

namespace frontend\widgets;


use frontend\modules\news\models\CommentModel;
use frontend\modules\news\models\NewsComment;
use yii\bootstrap\Widget;
use yii2mod\comments\widgets\Comment;
use Yii;

class CommentWidget extends Comment
{


    public function run()
    {
        $commentClass = CommentModel::class;

        $commentModel = Yii::createObject([
            'class' => $commentClass,
            'entity' => $this->entity,
            'entityId' => $this->entityId,
        ]);

        $commentDataProvider = $this->getCommentDataProvider($commentClass);

        return $this->render($this->commentView, [
            'commentDataProvider' => $commentDataProvider,
            'commentModel' => $commentModel,
            'maxLevel' => $this->maxLevel,
            'encryptedEntity' => $this->encryptedEntity,
            'pjaxContainerId' => $this->pjaxContainerId,
            'formId' => $this->formId,
            'listViewConfig' => $this->listViewConfig,
            'commentWrapperId' => $this->commentWrapperId,
        ]);
    }
}