<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="/images/favicon.png" rel="shortcut icon" type="image/png" />
    <meta name="google-site-verification" content="aX3CTQ6NvgxqyxA1W3LUg5QTo_3bX9Hf6jZEvJf8DUs" />
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php echo $this->render('header'); ?>

<?= $content ?>

<?php echo $this->render('footer'); ?>

<div id="alert-info">
    <?=Alert::widget() ?>
</div>

<?php $this->endBody() ?>
<?=\common\widgets\MainModal::widget() ?>
<?php if(!Yii::$app->user->isGuest): ?>
    <?=$this->registerJs('tryUpdateLastActivityDate("last_activity_date_'.Yii::$app->user->id.'");', \yii\web\View::POS_READY) ?>
<?php endif; ?>
</body>
</html>
<?php $this->endPage() ?>
