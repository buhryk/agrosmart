<?php
use yii\helpers\Url;
use frontend\modules\cabinet\models\DialogMessage;
use backend\modules\advertising\widgets\AdvertisingWidget;
use backend\modules\page\models\Page;
use backend\modules\news\models\NewsCategory;
use frontend\modules\instruments\models\InstrumentGet;

$showGreenMenu = (Yii::$app->controller->id = 'site' &&
    Yii::$app->controller->module->controllerNamespace == 'frontend\controllers' &&
    Yii::$app->controller->action->id == 'index') ? false : true;
$allPages = Page::find()->where(['or', ['alias' => 'o-proekte'], ['alias' => 'o-proekte'], ['alias' => 'kontakty']])->joinWith('lang')->all();
$allPagesAliases = \yii\helpers\ArrayHelper::getColumn($allPages, 'alias');
?>

<header>
    <?php if (Yii::$app->user->isGuest) { ?>
            <div class="top-panel">
                <div class="container">
                    <div class="row">
                        <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'header', 'typePosition' => 'horizontally']) ?>
                    </div>
                </div>
            </div>
            <div class="container main-menu">
                <div class="row">
                    <div class="logo-container">
                        <a href="<?= Yii::$app->homeUrl; ?>" class="logo">
                            <img src="<?= Yii::getAlias('@web')?>/images/logo.svg" alt="" />
                            <div id="logo-text-<?= Yii::$app->language == 'ru-RU' ? 'ru' : 'ua'; ?>"><?= Yii::t('common', 'Площадка выгодных сделок'); ?></div>
                        </a>
                    </div>
                    <div class="col-sm-9">
                        <div class="singup pull-right">
                            <a href="<?= Url::to(['/site/login']); ?>"><?= Yii::t('common', 'Login'); ?></a>
                            <a href="<?= Url::to(['/site/signup-company']); ?>"><?= Yii::t('cabinet', 'REGISTRATION'); ?></a>
                            <?= \common\widgets\WLangFrontend::widget(); ?>
                        </div>
                        <ul class="clear pull-right">
                            <li>
                                <a href="<?= Yii::$app->homeUrl; ?>">
                                    <?= Yii::t('common', 'Main'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::to(['/page/page/view', 'slug' => 'o-proekte']); ?>">
                                    <?= Yii::t('common','О проекте'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::to(['/tariff']); ?>">
                                    <?= Yii::t('common','Тарифы'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::to(['/site/contact']); ?>">
                                    <?= Yii::t('common','Контакты'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
    <?php } ?>

    <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="top-panel">
            <div class="container">
                <div class="row">
                    <?= AdvertisingWidget::widget(['key' => 'header', 'typePosition' => 'horizontally']) ?>
                </div>
            </div>
        </div>

        <nav class="cp-header">
            <div class="container">
                <div class="logo-container" style="padding-top: 16px;">
                    <a href="<?= Yii::$app->homeUrl; ?>" class="logo">
                        <img src="<?= Yii::getAlias('@web')?>/images/logo.svg" alt="" />
                        <div id="logo-text-<?= Yii::$app->language == 'ru-RU' ? 'ru' : 'ua'; ?>"><?= Yii::t('common', 'Площадка выгодных сделок'); ?></div>
                    </a>
                </div>
                <?php echo \frontend\widgets\NotificationWidget::widget();?>
                <div class="navigation">
                    <a class="navigation-mail" href="<?= Url::to(['/cabinet/dialog/index']); ?>" title="<?=Yii::t('common', 'Сообщения') ?>">
                        <img src="<?= Yii::getAlias('@web')?>/images/letter.png" alt="letter">
                        <span class="navigation-mail__number">
                            <?= DialogMessage::find()->where([
                                'to_user_id' => Yii::$app->user->identity->id,
                                'status' => \frontend\modules\cabinet\models\DialogMessage::STATUS_NOT_VIEWED
                            ])->count('id'); ?>
                        </span>
                    </a>
                    <div class="navigation-link-wrapper">
                        <a class="navigation__link" href="<?= Url::to(['/cabinet/']); ?>">
                            <?= Yii::t('cabinet', 'Личный <br> кабинет'); ?>
                        </a>
                        <a class="navigation__link" href="<?= Url::to(['/site/contact']); ?>">
                            <?= Yii::t('common', 'Контакты'); ?>
                        </a>
                    </div>
                    <a class="navigation__link" href="<?= Url::to(['/site/logout']); ?>">
                        <?= Yii::t('cabinet', 'LOGOUT'); ?>
                    </a>

                    <div class="lenguages"><?= \common\widgets\WLangFrontend::widget(); ?></div>
                </div>
            </div>
        </nav>
    <?php } ?>

    <?php if ($showGreenMenu) { ?>
            <div class="extra-menu">
      		<div class="container">
      			<nav class="navbar navbar-default">
      				<!-- Brand and toggle get grouped for better mobile display -->
      				<div class="navbar-header">
      					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
      				</div>
      				<!-- Collect the nav links, forms, and other content for toggling -->
      				<div class="collapse navbar-collapse green-header-menu-container" id="bs-example-navbar-collapse-1">
      					<ul class="nav navbar-nav">
      						<li>
                                <a href="<?= Url::to(['/product/product/index']); ?>">
                                    <?= Yii::t('advertisement', 'Products and services'); ?>
                                </a>
                            </li>
      						<li>
                                <a href="<?= Url::to(['/instruments']); ?>">
                                    <?= Yii::t('instruments', 'Instruments'); ?>
                                </a>
                            </li>
      						<li>
                                <a href="<?= Url::to(['/instruments/analytics']); ?>">
                                    <?= Yii::t('instruments', 'Analytics'); ?>
                                </a>
                            </li>
      						<li>
                                <a href="<?= Url::to(['/company/company/companies']); ?>">
                                    <?= Yii::t('common', 'Companies'); ?>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="<?=Url::to(['/company/price/search-index'])?>"  aria-haspopup="true" aria-expanded="false">
                                    <?= Yii::t('common', 'Prices'); ?>&nbsp;<span class="caret"></span>
                                </a>
                            </li>
      						<li>
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <?= Yii::t('news', 'News'); ?>&nbsp;<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <?php $newsCategories = NewsCategory::find()->where(['active' => NewsCategory::ACTIVE_YES])->joinWith('lang')->all(); ?>
                                    <?php foreach ($newsCategories as $category) { ?>
                                        <li>
                                            <a href="<?= Url::to(['/news/news/category', 'slug' => $category->alias]) ?>">
                                                <?= $category->title; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <li>
                                        <a href="<?=Url::to(['/event/event/index']) ?>">
                                            <?= Yii::t('common', 'Events'); ?>
                                        </a>
                                    </li>
                                </ul>
                            </li>
      					</ul>
      				</div>
      			</nav>
      		</div>
      	</div>

    <?php } ?>
</header>
