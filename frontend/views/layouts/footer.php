<?php
use yii\helpers\Url;
use backend\modules\rubric\models\Rubric;
use backend\modules\page\models\Page;
use yii\helpers\ArrayHelper;
use backend\modules\setting\models\Setting;

?>
<footer>
    <?= \frontend\widgets\UsersDataWidget::widget([]); ?>
    <?php
    if ($this->beginCache('footer_block', [
        'duration' => 3600,
        'variations' => [\Yii::$app->language, Yii::$app->user->isGuest],
    ])) {
    ?>
    <?php $allPages = Page::find()->joinWith('lang')->all(); ?>
    <?php $allPagesAliases = ArrayHelper::getColumn($allPages, 'alias'); ?>
    <?php $socialButtons = Setting::find()->where(['LIKE', 'alias', 'social_button'])->all(); ?>
    <?php $socialButtonsAliases = ArrayHelper::getColumn($socialButtons, 'alias'); ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <img src="<?= Yii::getAlias('@web')?>/images/logo-white.png" alt="" />
                <div class="social">
                    <?= ($index = array_search('social_button_vk', $socialButtonsAliases)) !== false ?
                        ('<a href="'.$socialButtons[$index]->value.'"><i class="flaticon-vk-social-logotype"></i></a>') : '';
                    ?>
                    <?= ($index = array_search('social_button_fb', $socialButtonsAliases)) !== false ?
                        ('<a href="'.$socialButtons[$index]->value.'"><i class="flaticon-facebook-logo-button"></i></a>') : '';
                    ?>
                    <?= ($index = array_search('social_button_gp', $socialButtonsAliases)) !== false ?
                        ('<a href="'.$socialButtons[$index]->value.'"><i class="flaticon-google-plus-logo-button"></i></a>') : '';
                    ?>
                    <?= ($index = array_search('social_button_tw', $socialButtonsAliases)) !== false ?
                        ('<a href="'.$socialButtons[$index]->value.'"><i class="flaticon-twitter-logo-button"></i></a>') : '';
                    ?>
                    <?= ($index = array_search('social_button_instagram', $socialButtonsAliases)) !== false ?
                        ('<a href="'.$socialButtons[$index]->value.'"><i class="flaticon-instagram-logo"></i></a>') : '';
                    ?>
                </div>
            </div>
            <div class="col-sm-9 clear">
                <div class="item">
                    <a href="<?= Url::to(['/page/page/view', 'slug' => 'fermeru']); ?>">
                        <?= ($index = array_search('fermeru', $allPagesAliases)) !== false ? $allPages[$index]->title : 'Фермеру'; ?>
                    </a>
                    <ul>
                        <li>
                            <a href="<?=Url::to(['/company/price/search-index']) ?>">
                                <?= Yii::t('price', 'Prices'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?=Url::to(['/instruments/fuel/prices']) ?>">
                                <?= Yii::t('instruments', 'Цены на ГСМ'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?=Url::to(['/instruments/map-price-company/index']) ?>">
                                <?= Yii::t('instruments', 'Companies prices map'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="item">
                    <a href="<?= Url::to(['/page/page/view', 'slug' => 'treyderu']); ?>">
                        <?= ($index = array_search('treyderu', $allPagesAliases)) !== false ? $allPages[$index]->title : 'Трейдеру'; ?>
                    </a>
                    <ul>
                        <li>
                            <a href="<?=Url::to(['/company/price/search-index']) ?>">
                                <?= Yii::t('price', 'Prices'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/company/company/rating']); ?>">
                                <?= Yii::t('common', 'Rating companies'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/company/company/companies']); ?>">
                                <?= Yii::t('common', 'Companies'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/instruments/elevators/map']); ?>">
                                <?= Yii::t('instruments', 'Elevators map'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/instruments/logistics-calculators/index']); ?>">
                                <?= Yii::t('instruments', 'Logistic calculator'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <?php $rubrics = Rubric::find()->where(['parent_id' => null, 'active' => Rubric::ACTIVE_YES])->joinWith('lang')->all(); ?>

                <?php if ($rubrics) { ?>
                    <div class="item">
                        <a href="<?= Url::to(['/product/product/index']); ?>">
                            <?= Yii::t('advertisement', 'Products and services'); ?>
                        </a>
                        <ul>
                            <?php foreach ($rubrics as $rubric) { ?>
                                <li>
                                    <a href="<?= Url::to(['/product/product/index', 'rubric' => $rubric->alias]); ?>">
                                        <?= $rubric->title; ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
                <div class="item">
                    <a href="<?= Url::to(['/page/page/view', 'slug' => 'o-proekte']); ?>">
                        <?= Yii::t('common','О проекте'); ?>
                    </a>
                    <ul>
                        <li>
                            <a href="<?= Url::to(['/page/page/view', 'slug' => 'reklama']); ?>">
                                <?=Yii::t('common','Реклама'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/page/page/view', 'slug' => 'pravila-ispolzovaniya']); ?>">
                                <?=Yii::t('common','Правила использования'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/tariff']); ?>">
                                <?=Yii::t('common','Тарифы'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/site/contact']); ?>">
                                <?= Yii::t('common','Контакты'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <p style="text-align: left"><?= Yii::t('common', 'Footer text', ['year' => date('Y')]); ?></p>
                <a class="link-dk" href="http://designkiev.com/web-design/">
                    <img class="dk-logo" src="/images/dk.png">
                </a>
            </div>
        </div>
    </div>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter43631399 = new Ya.Metrika({
                        id:43631399, clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/43631399" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-94107453-1', 'auto');
        ga('send', 'pageview');
    </script>
    <?php
        $this->endCache();
        } ?>
</footer>
<div class="" style="display: none">
    <script type='text/javascript'>
        (function(){ var widget_id = 'cjopECMukN';
            var d=document;var w=window;
            function l(){
            var s = document.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = '//code.jivosite.com/script/widget/'+widget_id;
                var ss = document.getElementsByTagName('script')[0];
                ss.parentNode.insertBefore(s, ss);
            }
            if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
    </script>

</div>
