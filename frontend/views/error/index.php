<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;



$this->title = $name;
?>


<div  class="container wrop">
    <div class="col-md-6 col-md-offset-3"
        <div class="logo" style="
        text-align:center;
        margin-top:100px;">
            <h1 ><?= Html::encode($this->title) ?></h1>
            <span class="error-code" style=" font-size:120px;"><?=Yii::$app->errorHandler->exception->statusCode ?></span>
            <div class="alert alert-danger">
                <?= nl2br(Html::encode($message)) ?>
            </div>
            <div class="sub">
                <p>
                    <a class="btn-green" href="<?=\yii\helpers\Url::to(['/']) ?>">
                        <span class="glyphicon glyphicon-home"></span>
                        Назад
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>