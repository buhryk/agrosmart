<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if (!$this->title) {
    $this->title = $model->title. ' | '.Yii::$app->params['titlePrefix'];
    $this->registerMetaTag(['name'=>'description', 'content'=> $model->title.' | '.Yii::$app->params['descriptionPrefix']]);
}

$this->params['breadcrumbs'][] = $model->title;
?>

<div class="bread">
    <h1><?= $model->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dstu">
    <div class="row">
        <div class="col-sm-7 mt45">
            <?= $model->text; ?>
        </div>
        <div class="col-sm-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($modelForm, 'name')->textInput(['autofocus' => true, 'placeholder' => Yii::t('common', 'Name')])->label(false) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($modelForm, 'surname')->textInput(['placeholder' => Yii::t('common', 'Surname')])->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($modelForm, 'phone')->textInput(['placeholder' => Yii::t('common', 'Phone')])->label(false) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($modelForm, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($modelForm, 'message')->textarea(['rows'=>6, 'placeholder' => Yii::t('common', 'Message')])->label(false) ?>
                </div>
            </div>


            <div class="form-group">
                <?= Html::submitButton(Yii::t('common', 'Submit'), ['class' => 'btn-green', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

