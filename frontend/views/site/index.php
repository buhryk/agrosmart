<?php
use yii\helpers\Url;
use yii\helpers\StringHelper;

?>

<?php if (isset($mainPageTop) && $mainPageTop) echo $mainPageTop->text; ?>

<?php if (isset($news) && $news) { ?>
    <div class="news-list">
        <div class="container">
            <div class="row">
                <?php foreach ($news as $one) { ?>
                    <div class="col-sm-6">
                        <div class="item-news">
                            <h6><?= $one->title; ?></h6>
                            <div class="date"><?= date('d.m.Y', $one->published_at); ?></div>
                            <div class="media">
                                <a class="pull-left" href="<?= Url::to(['/news/news/view', 'slug' => $one->alias]); ?>">
                                    <?php $mainImage = $one->image; ?>
                                    <?php if ($mainImage) { ?>
                                        <?php echo Yii::$app->thumbnail->img($mainImage->path,
                                            ['thumbnail' => ['width' => 237, 'height' => 237]],
                                            ['class' => 'media-object', 'alt' => $mainImage->alt  ? $mainImage->alt : $one->title]
                                        ); ?>
                                    <?php } else { ?>
                                        <?php echo Yii::$app->thumbnail->placeholder(['width' => 237, 'height' => 237, 'text' => '237x237'],
                                            ['class' => 'media-object', 'alt' => $one->title]
                                        ); ?>
                                    <?php } ?>
                                </a>
                                <div class="media-body">
                                    <p><?=StringHelper::truncate($one->short_description, 400); ?></p>
                                    <a href="<?= Url::to(['/news/news/view', 'slug' => $one->alias]); ?>" class="btn-green"><?= Yii::t('common', 'further'); ?> ...</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php $this->registerJs("
        function setEqualHeight(columns) {
            var tallestcolumn = 0;
            columns.each(
                function() {
                    currentHeight = $(this).height();
                    if (currentHeight > tallestcolumn) {
                        tallestcolumn = currentHeight;
                    }
                }
            );
            columns.height(tallestcolumn);
        }

        setEqualHeight($(\".item-news h6\"));
    ",
                    \yii\web\View::POS_READY,
                    'my-button-handler'
                ); ?>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($instrumentyPages) { ?>
    <div class="container instruments">
        <div class="owl-instruments text-center">
            <?php foreach ($instrumentyPages as $one) {
                echo '<figure>';
                echo $one->text;
                echo '</figure>';
            } ?>
        </div>
        <div class="text-center btn-all">
            <a href="<?= Url::to(['/instruments']); ?>" class="btn-green">
                <?= Yii::t('instruments', 'All instruments'); ?>
            </a>
        </div>
    </div>
<?php } ?>

<div class="bg-grey home-category">
    <?= \frontend\widgets\MainServicesWidget::widget([]); ?>
</div>

<?php if (isset($mainPageBottom) && $mainPageBottom) echo $mainPageBottom->text; ?>
<?=$this->registerJsFile('js/ofi.min.js',  ['depends' => [yii\web\JqueryAsset::className()]]); ?>


