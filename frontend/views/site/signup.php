<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Company;
use yii\widgets\Breadcrumbs;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use backend\modules\commondata\models\City;
use backend\modules\commondata\models\Region;
use yii\helpers\ArrayHelper;


$action = Yii::$app->controller->action->id;

$pageH1 = $action == 'signup-company' ? Yii::t('common', 'Регистрация компании') :
    \Yii::t('users', 'SIGNUP');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->params['breadcrumbs'][] = $pageH1;
$city = !empty($company_model->city_id) ? City::findOne($company_model->city_id) : '';

$classname = \yii\helpers\StringHelper::basename(get_class($model));

$getCityUrl = \yii\helpers\Url::to(['/city-list/index']);
$this->registerJsFile('js/mask.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile('js/register.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$oblastList = Region::find()->all();
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<ul class="ap-choose">
    <li class="ap-choose__item registration <?= $action == 'signup-company' ? 'active' : ''; ?>">
        <?=Html::a(\Yii::t('users', 'COMPANY'),['signup-company']) ?>
    </li>
    <li class="ap-choose__item registration <?= $action == 'signup' ? 'active' : ''; ?>">
        <?=Html::a(\Yii::t('users', 'PRIVATE_PERSON'),['signup']) ?>
    </li>
</ul>
<div class="ap-wrapper">
    <?php $form = ActiveForm::begin([
        'id' => 'form-signup',
        'options' => [
            'class' => 'registration-form clearfix',
            'autocomplete' => 'off'
        ]
    ]); ?>

    <?php if ($action == 'signup-company') { ?>
        <?= $form->field($company_model, 'name')->textInput(['placeholder' => Yii::t('users', 'COMPANY_NAME'),])->label(false); ?>

        <?= $form->field($company_model, 'type')->widget(Select2::classname(), [
            'data' => Company::getTypesList(),
            'options' => ['placeholder' => Yii::t('cabinet', 'Choose company type')],
            'pluginOptions' => ['allowClear' => true]
        ])->label(false); ?>

        <?= $form->field($company_model, 'phones')->textInput(['placeholder' => Yii::t('users', 'Main office phone')])->label(false); ?>
        <?= $form->field($company_model, 'email')->textInput(['placeholder' => Yii::t('users', 'Main office email')])->label(false); ?>
        <?= $form->field($company_model, 'city_id')->widget(Select2::className(), [
            'initValueText' => !empty($company_model->city_id) ? $city->title : '',
            'options' => [
                'placeholder' => Yii::t('advertisement', 'Enter city name')
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 2,
                'ajax' => [
                    'url' => $getCityUrl,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ])->label(false); ?>

        <?= $form->field($company_model, 'address')
            ->textInput(['placeholder' => Yii::t('users', 'Адрес главного офиса (адрес должен быть корректным)')])
            ->label(false); ?>

        <p><?=Yii::t('user', 'Личные данные пользователя (администратора Вашей компании)') ?></p>
    <?php } ?>

    <?= $form->field($model, 'name')->textInput(['placeholder' => Yii::t('users', 'NAME')])->label(false) ?>
    <?= $form->field($model, 'surname')->textInput(['placeholder' => Yii::t('users', 'SURNAME')])->label(false) ?>
    <?= $form->field($model, 'email')->textInput(['type' => 'email', 'placeholder' => 'Email', 'id' => 'signupform-email'])->label(false) ?>
    <?php if ($action != 'signup-company') { ?>
        <?= $form->field($model, 'oblast_id')->widget(Select2::className(), [
            'data' => ArrayHelper::map($oblastList, 'id', 'title'),
            'options' => ['placeholder' => Yii::t('users', 'Выберите область')],
            'id' => 'rubric-select',
            'pluginOptions' => [
            ],
        ])->label(false); ?>
    <?php } ?>
    <input type="checkbox" id="same-user" name="<?= $classname . '[email_username]'; ?>" value="1"
        <?= $model->email_username == 1 ? 'checked="checked"' : ''; ?>>
    <label for="same-user" class="same-user"><?= Yii::t('users', 'username like my email'); ?></label>

    <?= $form->field($model, 'username')->textInput(['placeholder' => Yii::t('users', 'User login'), 'id' => 'signupform-username'])->label(false) ?>

    <?= $form->field($model, 'phone')->textInput([
        'placeholder' => 'Телефон',
        'id' => 'signupform-phone',
        'onfocus' => 'this.removeAttribute("readonly")',
        'readonly' => ''
    ])->label(false) ?>

    <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>

    <input type="checkbox" id="agreement" name="<?= $classname . '[admit]'; ?>" value="1" <?= $model->admit == 1 ? 'checked="checked"' : ''; ?>>
    <label for="agreement" class="agreement">
        <?= Yii::t('users', 'Agree with rules for website', ['url' => '/page/pravila-ispolzovaniya']); ?>
    </label>

    <?php if (isset($model->getErrors()['admit'])) { ?>
        <p class="help-block help-block-error" style="color: #a94442"><?= $model->getErrors()['admit'][0]; ?></p>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('users', 'ENROLL'), ['class' => 'btn-green btn-center mtb50']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>