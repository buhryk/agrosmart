<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 13.02.2017
 * Time: 14:57
 */

use yii\widgets\Breadcrumbs;

$this->title = Yii::t('common', 'Confirm email');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-sm-offset-3 " style="padding:100px">
        <p style="font-size: 20px;">
            <?= Yii::t('common','On you email send letter please confirm your email') ?>
        </p>
    </div>
</div>


