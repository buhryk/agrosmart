<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('common', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<h2 class="cp-title"><?= $this->title; ?></h2>

<div class="ap-wrapper">
    <?php $form = ActiveForm::begin([
        'id' => 'request-password-reset-form',
        'options' => [
            'class' => 'registration-form clearfix'
        ]
    ]); ?>

    <?= $form->field($model, 'email')->textInput(['type' => 'email']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('users', 'SEND'), ['class' => 'btn-green btn-center mtb50']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

