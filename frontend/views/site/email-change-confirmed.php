<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 13.02.2017
 * Time: 14:57
 */

use yii\widgets\Breadcrumbs;

$this->title = Yii::t('common', 'Congratulations, your email is changed successfully');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 m-profile" style="padding-top: 15px;">
            <div class="row">
                <div class="bread">
                    <h1><?= $this->title; ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>




