<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$pageH1 = Yii::t('common', 'Login');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);

$this->params['breadcrumbs'][] = $pageH1;
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<h2 class="cp-title"><?= Yii::t('common', 'Enter to cabinet'); ?></h2>

<div class="ap-wrapper">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => [
            'class' => 'registration-form clearfix'
        ]
    ]); ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <div class="enter-help clearfix">
        <input type="hidden" name="LoginForm[rememberMe]" value="0">
        <input  value="1" id="remember" name="LoginForm[rememberMe]" type="checkbox">
        <label for="remember">
            <span><?=Yii::t('common', 'Запомнить меня') ?></span>
        </label>
        <a href="<?= Url::to(['site/request-password-reset']); ?>" class="enter-help__right"><?= Yii::t('common', 'Forgot your password?'); ?></a>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('cabinet', 'LOGIN'), ['class' => 'btn-green btn-center mtb50', 'name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
