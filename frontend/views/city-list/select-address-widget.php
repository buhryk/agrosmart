<?php
use yii\web\JsExpression;
use kartik\widgets\Select2;
$getCityUrl = \yii\helpers\Url::to(['/city-list/index']);
?>

<div class="one-address-container" data-address-container-id="<?= $number; ?>">
    <div class="help-block form-error-block custom-error-block" id="error-address-<?= $number; ?>"></div>
    <div class="col-sm-6 region">
        <?php echo Select2::widget([
            'name' => 'address['.$number.'][city]',
            'options' => [
                'placeholder' => Yii::t('advertisement', 'Enter city name'),
                'data-city-id' => $number,
                'id' => 'select-city-widget-'.$number
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 2,

                'ajax' => [
                    'url' => $getCityUrl,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ]); ?>
    </div>
    <div class="col-sm-6 region city">
        <input type="text" name="address[<?= $number; ?>][address]" class="input-new-advertisement-address"
               placeholder="<?= Yii::t('advertisement', 'Enter address'); ?>"
               data-address-id="<?= $number; ?>"
        >
    </div>
    <div style="clear: both"></div>
</div>