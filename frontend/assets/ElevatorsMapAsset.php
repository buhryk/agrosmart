<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class ElevatorsMapAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/sidebar.css'
    ];
    public $cssOptions = [
        'position' => View::POS_END,
    ];
    public $js = [
        'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js',
        '//maps.googleapis.com/maps/api/js?key=AIzaSyBwqlzPxZ-5ttym0zswhaaKsGDEciU0PMM&libraries=places,geometry&sensor=false',
        //'http://maps.googleapis.com/maps/api/js?key=AIzaSyDPFMj9CPyAg6JHrd49lU9ou3VRx8fFDJc&libraries[]=places&&libraries[]=geometry&sensor=false',
        //'http://maps.google.com/maps/api/js?key=AIzaSyDPFMj9CPyAg6JHrd49lU9ou3VRx8fFDJc&libraries=places&sensor=false&libraries=geometry',
        'js/elevators-map.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
