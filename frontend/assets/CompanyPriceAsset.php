<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class CompanyPriceAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/sidebar-2.css',
        'css/sidebar.css'
    ];
    public $js = [
        'js/company-price.js',
        'js/sidebar.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
