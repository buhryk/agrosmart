<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class CabinetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome-css.min.css',
        'css/sidebar.css',
        'css/style-kolia-r.css',
    ];
    public $cssOptions = [
        'position' => View::POS_END,
    ];
    public $js = [
        'js/sidebar.js',
        'js/cabinet.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
