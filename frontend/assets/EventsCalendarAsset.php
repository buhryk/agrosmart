<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class EventsCalendarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/calendar/fullcalendar.min.css',
    ];
    public $cssOptions = [
        'position' => View::POS_END,
    ];
    public $js = [
        'js/calendar/lib/moment.min.js',
        'js/calendar/fullcalendar.min.js',
        'js/calendar/locale/ru.js',
        'js/calendar/locale/uk.js',
        'js/calendar/events.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
