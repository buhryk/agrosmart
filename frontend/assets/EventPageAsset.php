<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class EventPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBwqlzPxZ-5ttym0zswhaaKsGDEciU0PMM',
        'js/event-page.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
