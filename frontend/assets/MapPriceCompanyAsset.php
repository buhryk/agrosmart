<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class MapPriceCompanyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js',
        'js/map-price-company.js',
        'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyBwqlzPxZ-5ttym0zswhaaKsGDEciU0PMM&callback=initMap&libraries=places,geometry&sensor=false',


    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
