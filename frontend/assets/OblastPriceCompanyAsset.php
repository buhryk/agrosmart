<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class OblastPriceCompanyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//www.amcharts.com/lib/3/plugins/export/export.css'
    ];

    public $js = [
        '//www.amcharts.com/lib/3/ammap.js',
        '//www.amcharts.com/lib/3/serial.js',
        '//www.amcharts.com/lib/3/plugins/export/export.min.js',
        '//www.amcharts.com/lib/3/themes/light.js',
        'js/oblast-map-price.js'

    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
