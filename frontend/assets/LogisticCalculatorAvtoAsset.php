<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class LogisticCalculatorAvtoAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $cssOptions = [
        'position' => View::POS_END,
    ];
    public $js = [
        '//maps.googleapis.com/maps/api/js?key=AIzaSyBwqlzPxZ-5ttym0zswhaaKsGDEciU0PMM&libraries=places&sensor=false',
        'js/logistic-calculator-avto.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
