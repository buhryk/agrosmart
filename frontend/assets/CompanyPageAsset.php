<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class CompanyPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/sidebar.css',
        'css/sidebar-2.css',
        'css/new.css'
    ];
    public $cssOptions = [
        'position' => View::POS_END,
    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBwqlzPxZ-5ttym0zswhaaKsGDEciU0PMM',
        'js/company-page.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
