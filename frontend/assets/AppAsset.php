<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/owl.carousel.css',
        'css/flaticon.css',
        'css/style-old.css',
        'css/style-slavik.css',
        'css/style-kolia-z.css',
        'css/footer-2.css',
        'css/header-2.css',
        'css/responsive.css',
        'css/jquery.stickynotif.css',
        'css/fixes.css',
        'jivosite/jivosite.css',

    ];
    public $js = [
        'js/jquery.cookie.min.js',
        'js/owl.carousel.min.js',
        'js/jquery.chained.min.js',
        'js/jquery.stickynotif.js',
        'js/imagepreview.js',
        'js/percircle.js',
        'js/main.js',
        'js/scripts.js',
        'jivosite/jivosite.js',
        'js/mask.min.js',
        'js/uk.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
