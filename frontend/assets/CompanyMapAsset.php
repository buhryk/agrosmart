<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class CompanyMapAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

    ];
    public $js = [
        'js/c2b4132807.js',
        'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js',
        '//maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBwqlzPxZ-5ttym0zswhaaKsGDEciU0PMM&libraries=places,geometry',
        'https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js',
        'js/company-map.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
