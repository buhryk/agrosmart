<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class FuelAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://www.amcharts.com/lib/3/plugins/export/export.css'
    ];
    public $js = [
        'https://www.amcharts.com/lib/3/amcharts.js',
        'https://www.amcharts.com/lib/3/serial.js',
        'https://www.amcharts.com/lib/3/plugins/export/export.min.js',
        'https://www.amcharts.com/lib/3/themes/none.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
