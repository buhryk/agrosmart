<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class PriceAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new.css',
    ];
    public $cssOptions = [
        'position' => View::POS_END,
    ];
    public $js = [
        'js/price-new.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}