<?php
return [
    'adminEmail' => '',
    'supportEmail' => 'notification@agro-smart.com.ua',
    'user.passwordResetTokenExpire' => 3600,
    'descriptionPrefix' => Yii::t('common2', 'на сайте agro-smart.com.ua'),
    'titlePrefix' => Yii::t('common2', 'agro-smart.com.ua'),
];