<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name'=>'AGROSMART',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'cabinet' => [
            'class' => 'frontend\modules\cabinet\Module',
        ],
        'product' => [
            'class' => 'frontend\modules\product\Module',
        ],
        'elected' => [
            'class' => 'frontend\modules\elected\Module',
        ],
        'complaint' => [
            'class' => 'frontend\modules\complaint\Module',
        ],
        'company' => [
            'class' => 'frontend\modules\company\Module',
        ],
        'instruments' => [
            'class' => 'frontend\modules\instruments\Module',
        ],
        'page' => [
            'class' => 'frontend\modules\page\Module',
        ],
        'price' => [
            'class' => 'frontend\modules\price\Module',
        ],
        'saveddata' => [
            'class' => 'frontend\modules\saveddata\Module',
        ],
        'news' => [
            'class' => 'frontend\modules\news\Module',
        ],
        'comment' => [
            'class' => 'yii2mod\comments\Module',
        ],
        'event' => [
            'class' => 'frontend\modules\event\Module',
        ],
        'user' => [
            'class' => 'frontend\modules\user\Module',
        ],
        'ordertransport' => [
            'class' => 'frontend\modules\ordertransport\Module',
        ],
        'notification' => [
            'class' => 'frontend\modules\notification\Module',
        ],
        'tariff' => [
            'class' => 'frontend\modules\tariff\Modules',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ],
    'homeUrl' => '/',
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl'=>'',
            'class' => 'common\components\LangRequest',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
           // 'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'site/set-last-activity-date' => 'set-last-activity-date/index',
                'page/<slug:[\w-]+>' => 'page/page/view',
                'page/category/<slug:[\w-]+>' => 'page/category/view',
                'news/<slug:[\w-]+>' => 'news/news/view',
                'news/category/<slug:[\w-]+>' => 'news/news/category',
                'event/<slug:[\w-]+>' => 'event/event/view',

                '/product/rubric/<rubric>'=>'product/product/index',
                '/product' => 'product/product/index',
                '/product/<slug>'=>'product/product/view',
                'company' => '/company/company/companies',
//                'company/search-rezult/<type>/<q>' => '/company/price/search-rezult',
//                'company/search-rezult/<q>' => '/company/price/search-rezult',
//                'company/search' => '/company/price/search-index',
//                'company/<id>/<type>' => '/company/company/view',
                'company/<id>' => '/company/company/view',
//                'price-item/<id>/<item>/' => '/company/price/view',
//                'price/<id>' => '/company/price/view',
                '/company/company/products/<id>' => '/company/company/products',
                '/user/user/view/<id>' => '/user/user/view',
                '/user/user/products/<id>' => '/user/user/products',
                '/company/company/feedbacks/<id>' => '/company/company/feedbacks',
                'tariff' => 'tariff/tariff/index',
                'tariff/<id>' => 'tariff/tariff/view',
                '/instruments/elevators/view/<alias>' => '/instruments/elevators/view',
                'company/price/trader-zakupka/<company_id>' => 'company/price/trader-zakupka-info',
                'company/price/trader-prodazha/<company_id>' => '/company/price/trader-prodazha-info',
                'company/price/zakupka/<company_id>' => '/company/price/zakupka-info',
                'company/price/prodazha/<company_id>' => '/company/price/prodazha-info',

            ],
        ],
        'errorHandler' => [
            'errorAction' => 'error',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        '//code.jquery.com/jquery-2.2.4.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' :         'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],

        'userRole' => [
            'class' => 'frontend\components\GetUserRole',
        ],
        'thumbnail' => [
            'class' => 'common\components\Thumbnail',
            'basePath' => '@webroot',
            'prefixPath' => '/',
            'cachePath' => 'uploads/thumbnails'
        ],


    ],
    'params' => $params,
];