<?php
/**
 * Created by PhpStorm.
 * User: Boris
 * Date: 11.03.2016
 * Time: 16:43
 */
namespace frontend\controllers;

use Yii;
use yii\web\Controller;


class ErrorController extends Controller
{
    /**
     * @var string название слоя шаблона
     */
    public $layout = 'error';

    /**
     * Внешние действия для контроллера
     * @return array
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

}
