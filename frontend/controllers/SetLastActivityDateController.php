<?php

namespace frontend\controllers;


use Yii;
use yii\web\Controller;


/**
 * Site controller
 */
class SetLastActivityDateController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax && Yii::$app->request->method == 'POST') {
            $currentUser = Yii::$app->user->identity;
            $company = $currentUser->company;

            $currentUser->last_activity_date = time();
            $currentUser->update(false);
            setcookie('last_activity_date_'.Yii::$app->user->id, true, time() + 300, "/");

            if ($company && ((time() - $company->last_activity_date) > 300)) {
                $company->last_activity_date = time();
                $company->update(false);
            }
        }
    }
}