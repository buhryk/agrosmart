<?php
namespace frontend\controllers;

use backend\modules\contact\models\Contact;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsLang;
use backend\modules\page\models\Category;
use backend\modules\page\models\Page;
use backend\modules\seo\models\Seo;
use common\helpers\SitemapHelper;
use frontend\models\CompanyUser;
use frontend\models\CompanyUserPasswordForm;
use frontend\models\ConsumerOblast;
use frontend\models\RegisterCompanyForm;
use frontend\models\SignupUserForm;
use frontend\modules\cabinet\models\EmailChangeConfirm;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\EmailConfirmForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\base\DynamicModel;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [

            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $mainPageTop = Page::find()
            ->where(['alias' => 'main-page-top', 'active' => Page::ACTIVE_YES])
            ->joinWith('lang')
            ->one();
        $news = News::find()
            ->where(['active' => News::ACTIVE_YES])
            ->joinWith('lang')
            ->joinWith('userAccessibilities')
            ->andWhere(['in', 'type_id', News::getVisabilityTypesIdsForCurrentUser()])
            ->andWhere(['show_on_main_page' => News::SHOW_ON_MAIN_PAGE_YES])
            ->andWhere(['<=', 'published_at', time()])
            ->orderBy('published_at DESC')
            ->groupBy(NewsLang::tableName().'.news_id')
            ->limit(2)
            ->all();
        $mainPageBottom = Page::find()
            ->where(['alias' => 'main-page-bottom', 'active' => Page::ACTIVE_YES])
            ->joinWith('lang')
            ->one();

        if ($mainPageTop) {
            $seo = ($seoModel = $mainPageTop->seo) != null ? $seoModel->lang : null;
            if ($seo) Seo::registerMetaTags($seo);
        }

        $instrumentyCategory = Category::find()->where(['alias' => 'instrumenty-glavnaya-stranitsa'])->one();
        $instrumentyPages = $instrumentyCategory ?
            Page::find()->where(['category_id' => $instrumentyCategory->primaryKey])->joinWith('lang')->all() : [];

        return $this->render('index', [
            'mainPageTop' => $mainPageTop,
            'news' => $news,
            'mainPageBottom' => $mainPageBottom,
            'instrumentyPages' => $instrumentyPages
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/cabinet/default/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/cabinet/default/index']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLoginAdmin()
    {

    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = Page::findOne(8);

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $seo = ($seoModel = $model->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);
        
        
        $modelForm = new Contact();
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
                Yii::$app->session->setFlash('success', Yii::t('common','Спасибо, в ближайшее время мы с вами свяжемся'));
            return $this->refresh();
        } else {
            $user = Yii::$app->user->identity;
            if($user) {
                $modelForm->email = $user->email;
                $modelForm->phone = $user->phone;
                $modelForm->name = $user->name;
                $modelForm->surname = $user->surname;
            }


            return $this->render('contact', [
                'model' => $model,
                'modelForm' => $modelForm
            ]);
        }
    }


    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/cabinet/default/index']);
        }

        $model = new SignupUserForm();

        if ($model->load(Yii::$app->request->post())&& $model->validate()) {
            if ($user = $model->signup()) {
                Yii::$app->getUser()->login($user);
                return $this->redirect('send-email');
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionSignupCompany()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/cabinet/default/index']);
        }

        $model = new SignupForm();
        $companyForm = new RegisterCompanyForm();

        if ($model->load(Yii::$app->request->post()) && $companyForm->load(Yii::$app->request->post()) &&
            $companyForm->validate() && $model->validate() && ($user = $model->signup()) && ($company = $companyForm->signup())) {

            $companyUser = new CompanyUser();
            $companyUser->user_id = $user->primaryKey;
            $companyUser->company_id = $company->primaryKey;
            $companyUser->role_id = CompanyUser::ROLE_ADMINISTRATOR;
            $companyUser->save();
            
            Yii::$app->getUser()->login($user);

            return $this->redirect('send-email');
        }

        return $this->render('signup', [
            'model' => $model,
            'company_model' => $companyForm
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('common','Check your email for further instructions.'));

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', Yii::t('common','Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('common','New password was saved.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    public function actionEmailConfirm($token)
    {
        try {
            $model = new EmailConfirmForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->confirmEmail()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('common','Спасибо! Ваш Email успешно подтверждён.'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('common','Ошибка подтверждения Email.'));
        }

        return $this->redirect(['/cabinet']);
    }

    public function actionSendEmail(){
        return $this->render('send-email');
    }


    public function actionCompanyUserFinishAuth($token){
        try {
            $model = new CompanyUserPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('common','New password was saved.'));

            return $this->goHome();
        }

        return $this->render('company-user-pass', [
            'model' => $model,
        ]);
    }

    public function actionEmailChangeConfirm($token){
        try {
            $model = new EmailChangeConfirm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->confirmEmail()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('common','Спасибо! Ваш Email успешно подтверждён.'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('common','Ошибка подтверждения Email.'));
        }
        return $this->render('email-change-confirmed');
    }

    public function actionSetLastActivityDate()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax && Yii::$app->request->method == 'POST') {
            $currentUser = Yii::$app->user->identity;
            $company = $currentUser->company;

            if ((time() - $currentUser->last_activity_date) > 300) {
                $currentUser->last_activity_date = time();
                $currentUser->update(false);
            }

            if ($company && ((time() - $company->last_activity_date) > 300)) {
                $company->last_activity_date = time();
                $company->update(false);
            }
        }
    }

    public function actionError()
    {
        $this->layout = 'error';

        $this->render('error');
    }
}