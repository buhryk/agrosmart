var bounds = new google.maps.LatLngBounds();

var map = new google.maps.Map(document.getElementById('events_map'), {
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var infowindow = new google.maps.InfoWindow();

var marker, i;

for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
    });

    bounds.extend(marker.position);

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            infowindow.setContent(locations[i].detailAddress);
            infowindow.open(map, marker);
        }
    })(marker, i));
}

map.fitBounds(bounds);