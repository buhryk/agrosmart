$( document ).ready(function() {
    $('body').on('click', '#price-search label', function () {
        $('form#price-search').submit();
    });
    $('body').on('change', '#price-search #w0-kvdate input.krajee-datepicker', function () {
        $('form#price-search').submit();
    });
    $('body').on('click', '.icon-block-price span', function () {
        $('.icon-block-price span').removeClass('active');
        $(this).addClass('active');
        var value = $(this).attr('data-value');
        $('#companypricesearch-listtype').val(value);
        $('form#price-search').submit();
    });

    $('body').on('change', '#selectedPrice .company-list select', function () {
        var data = $('form#selectedPrice').serialize(),
            url = $('form#selectedPrice').attr('action');

        $.ajax({
            method: 'get',
            url: url,
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data.status == 'success') {
                    $('.selected-price-block').html(data.data);
                }
            }
        });
    });

});
