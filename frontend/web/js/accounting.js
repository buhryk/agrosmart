$(window).on("load", function () {
function calc(a1, a2) {
    if (a1 !== '' && a2 !== '') {
        return (a1 * a2).toFixed(2)
    }
}
function v (item) {
    return +$("."+item+"").val();
}
function t (item) {
    return +$("."+item+"").text();
}

function isNumber(input) {
    var reg = /^\d+$/;
    return reg.test(input);
}

function multiplication(a, b) {
    if (isNumber(a) && isNumber(b)) {
        return (a * b).toFixed(2)
    }
}
// *****************        a         ********************
$('.row-a input').on("keyup", function () {
    $(".a5").text(multiplication(v("a1"), v("a2")));
    if (isNumber(v("a1")) && isNumber(v("a2"))) {
        $(".a3").text((t("a5") / 6).toFixed(2));
        $(".a4").text((t("a5") / 1.2).toFixed(2));
    }
    sum();
});

// *****************        b         ********************
$('.row-b input').on("keyup", function () {
    $(".b5").text(multiplication(v("b1"), v("b2")));
    sum();
});

// *****************        c         ********************
$('.row-c input').on("keyup", function () {
    $(".c5").text(multiplication(v("c1"), v("c2")));
    sum();
});

// *****************        d         ********************
$('.row-d input').on("keyup", function () {
    $(".d5").text(multiplication(v("d1"), v("d2")));
    sum();
});

// *****************        e         ********************
$('.row-e input').on("keyup", function () {
    $(".e5").text(multiplication(v("e1"), v("e2")));
    sum();
});

// *****************        f         ********************
$('.row-f input').on("keyup", function () {
    $(".f5").text(multiplication(v("f1"), v("f2")));
    sum();
});

// *****************        g         ********************
$('.row-g input').on("keyup", function () {
    var _result = v("g1") * v("g2") / 30 * v("g0");
    $(".g5").text(_result);
    sum();
});

// *****************        h         ********************
$('.row-h input').on("keyup", function () {
    $(".h5").text(multiplication(v("h1"), v("h2")));
    sum();
});

// *****************        i         ********************
$('.row-i input').on("keyup", function () {
    $(".i5").text(multiplication(v("i1"), v("i2")));
    sum();
});

// *****************        j         ********************
$('.row-j input').on("keyup", function () {
    $(".j5").text(multiplication(v("j0"), v("j2")));
    sum();
});

// *****************        k         ********************
$('.row-k input').on("keyup", function () {
    $(".k5").text(multiplication(v("k1"), v("k2")));
    sum();
});

// *****************        l         ********************
$('.row-l input').on("keyup", function () {
    $(".l5").text(multiplication(v("l1"), v("l2")));
    sum();
});

// *****************        m         ********************
$('.row-m input').on("keyup", function () {
    $(".m5").text(multiplication(v("m1"), v("m2")));
    sum();
});

// *****************        n         ********************
$('.row-n input').on("keyup", function () {
    var _result = (v("n0") / 100) * v("a2");
    var _result2 = v("n1") * _result;
    $(".n2").text(_result);
    $(".n3").text(_result2);
    sum();
});
$('.a2').on("keyup", function () {
    var _result = (v("n0") / 100) * v("a2");
    var _result2 = v("n1") * _result;
    $(".n2").text(_result);
    $(".n3").text(_result2);
});

// *****************        o         ********************
$('.row-o input').on("keyup", function () {
    var _result = (v("o0") / 100) * v("a2");
    var _result2 = v("o1") * _result;
    $(".o2").text(_result);
    $(".o3").text(_result2);
    sum();
});
$('.a2').on("keyup", function () {
    var _result = (v("o0") / 100) * v("a2");
    var _result2 = v("o1") * _result;
    $(".o2").text(_result);
    $(".o3").text(_result2);
});

// *****************        p         ********************
$('.row-p input').on("keyup", function () {
    var _result = (v("c2") + v("d2") + v("e2") + v("l2")) * v("p0");
    var _result2 = v("p1") * _result;
    $(".p2").text(_result);
    $(".p3").text(_result2);
    sum();
});

// *****************        q         ********************
$('.row-q input').on("keyup", function () {
    var _result =  ((t("t1") / v("b1") * (v("q0") / 100)) / 365) * v("days-count");
    var _result2 = _result * v("q1");
    $(".q3").text(_result2.toFixed(2));
    $(".q2").text(_result.toFixed(2));
    sum();
});

$('.days-count').on("keyup", function () {
    sum();
});
function sum () {
    // *****************        r         ********************
    $(".r5").text(sumCounter($(".total-sum")));
    $(".r3").text((t("r5") / 6).toFixed(2));
    $(".r4").text((t("r5") / 1.2).toFixed(2));
    $(".r2").text((t("r5") / v("b1")).toFixed(2));
    $(".r1").text(v("b1"));

    // *****************        s         ********************
    $(".s1").text(v("a1"));
    $(".s2").text(v("a2"));
    $(".s3").text(t("a3"));
    $(".s4").text(t("a4"));
    $(".s5").text(t("a5"));

    // *****************        t         ********************
    $(".t1").text(sumCounter($(".n-needs")))

    // *****************        u         ********************
    $(".u1").text(t("a5"));
    $(".u2").text(v("a2"));

    // *****************        v         ********************
    $(".v1").text(t("b5") + t("c5"));
    $(".v2").text(v("b2") + v("c2") + t("n2"));

    // *****************        w         ********************
    $(".w1").text(parseInt(t("r5")) - parseInt(t("v1")));
    $(".w2").text((t("w1") / v("b1")).toFixed(2));

    // *****************        x         ********************
    $(".x1").text((t("u1") - t("v1") - t("w1")).toFixed(2));
    $(".x2").text((t("u2") - t("v2") - t("w2")).toFixed(2));

    // *****************        y         ********************
    $(".y1").text((t("x1") / t("u1") * 100).toFixed(2));

    // *****************        z         ********************
    $(".z1").text((t("x1") / (t("w1") + t("v1")) * 100).toFixed(2));

    function sumCounter (selector) {
        var sumArr = [];
        var _toNum;
        selector.each(function (i) {
            _toNum = parseInt($(this).text())
            sumArr.push(_toNum);
        });
        var sum = sumArr.reduce(add, 0);
        function add(a, b) {
            return a + b;
        }

        return sum;
    }

}
    sum();
});