$( document ).ready(function() {
    new google.maps.places.Autocomplete(document.getElementById('companymapsearch-point_location'));

    var bounds = new google.maps.LatLngBounds();

    var map = new google.maps.Map(document.getElementById('subsidiaries-map'), {
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var oms = new OverlappingMarkerSpiderfier(map, {
        markersWontMove: true,
        markersWontHide: true,
        basicFormatEvents: true
    });

    var marker, i, markers = [];

    if(typeof radius !=="undefined" && radius != 0 && typeof point_location !=="undefined") {
        var latitude = parseFloat(point_location.lat),
            longitude = parseFloat(point_location.lng),
            centerPoint = new google.maps.LatLng(latitude, longitude);
        var cityCircle = new google.maps.Circle({
            strokeColor: '#c6c6c6',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#c6c6c6',
            fillOpacity: 0.35,
            map: map,
            center: centerPoint,
            radius: radius * 1000
        });
    }
    var item = 0;
    var countCompany = 0;
    for (i = 0; i < locations.length; i++) {
        if(radius != 0){
            var latlng = new google.maps.LatLng(locations[i][1], locations[i][2]);
            if (distHaversine(latlng, centerPoint) <= radius) {
                countCompany ++;
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map
                });

                bounds.extend(marker.position);

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                oms.addMarker(marker);
                markers[item] = marker;
                item++;
            }
        } else {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            bounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }

            })(marker, i));
            oms.addMarker(marker);
            markers[i] = marker;
        }

    }

    if (radius != 0) {
        $('.filter-result .count-company').text(countCompany);
    }

    map.fitBounds(bounds);
});

rad = function(x) {return x*Math.PI/180;}

distHaversine = function(p1, p2) {
    var R = 6371; // earth's mean radius in km
    var dLat  = rad(p2.lat() - p1.lat());
    var dLong = rad(p2.lng() - p1.lng());

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) * Math.sin(dLong/2) * Math.sin(dLong/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    console.log(d.toFixed(3));
    return d.toFixed(3);
}