$( document ).ready(function() {
    new google.maps.places.Autocomplete(document.getElementById('from_location'));

    if (typeof(locations) !== 'undefined' && locations) {
        var bounds = new google.maps.LatLngBounds();

        var map = new google.maps.Map(document.getElementById('elevators_map'), {
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var markers = [];
        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i].coordinates.lat, locations[i].coordinates.lng),
                map: map,
                icon: locations[i].icon,
                title: locations[i].title
            });

            markers[i] = marker;
            bounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    var from_location = $('#from_location').val();
                    var content = locations[i].content;

                    if (from_location && i != 0) {
                        var distance = (google.maps.geometry.spherical.computeDistanceBetween(
                            new google.maps.LatLng(locations[0].coordinates.lat, locations[0].coordinates.lng),
                            new google.maps.LatLng(locations[i].coordinates.lat, locations[i].coordinates.lng)
                        ) / 1000).toFixed(2);

                        if (distance) {
                            content += '<div style="margin-top: 10px;">' + locations[i].distanceTranslates + ': ' + distance + ' км</div>';
                        }
                    }

                    if (i != 0) {
                        content += locations[i].linkBlock;
                    }
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }

        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
        map.fitBounds(bounds);
    }
});