/**
 * Created by User on 21.02.2017.
 */
$( document ).ready(function() {
    new google.maps.places.Autocomplete(document.getElementById('from_location'));
    new google.maps.places.Autocomplete(document.getElementById('to_location'));

    var map = new google.maps.Map(document.getElementById('map'), {
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var bounds = new google.maps.LatLngBounds();

    if (typeof(locations) !== 'undefined' && locations) {
        var infowindow = new google.maps.InfoWindow();

        var marker, i;
        var directionsDisplay = new google.maps.DirectionsRenderer();
        var directionsService = new google.maps.DirectionsService();

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            bounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }

        var request = {
            origin: new google.maps.LatLng(locations[0][1],locations[0][2]), //точка старта
            destination: new google.maps.LatLng(locations[1][1],locations[1][2]), //точка финиша
            travelMode: google.maps.DirectionsTravelMode.DRIVING //режим прокладки маршрута
        };

        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            }
        });

        directionsDisplay.setMap(map);

        map.fitBounds(bounds);
    } else {
        var latlng = new google.maps.LatLng(49.299, 31.986);
        var myOptions = {
            zoom: 6,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            },
            navigationControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map"), myOptions);
    }

    $('body').on('click', '#save-ordertransport', function (event) {
        var form = $('#form-ordertransport');

        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serializeArray(),
            success: function(data) {
                if (data.status) {
                    if (!data.validation && data.errors) {
                        var errorsHtml = '<ul style="list-style-type: initial; padding-left: 15px;">';
                        $.each(data.errors, function(index, value) {
                            errorsHtml += '<li>'+value+'</li>';
                        });
                        errorsHtml += '</ul>';
                        $('#ordertransport-errors').html(errorsHtml);
                    } else {
                        $('#ordertransport-errors').html('');
                    }

                    if (data.validation) {
                        location.reload();
                    }
                }
            }
        });

        return false;
    });
});