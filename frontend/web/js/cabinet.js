$( document ).ready(function() {
    $('body').on('click', '#save-company-user', function () {
        var form = $('#add-company-user');
        var url = form.data('url');

        $.ajax({
            method: 'post',
            url: url,
            dataType: 'json',
            data: form.serializeArray(),
            success: function(data) {
                if (data.status) {
                    if (!data.validation && data.errors) {
                        var errorsHtml = '<ul style="list-style-type: initial;padding-left: 15px;">';
                        $.each(data.errors, function(index, value) {
                            errorsHtml += '<li>'+value+'</li>';
                        });
                        errorsHtml += '</ul>';
                        $('#save-company-user-errors').html(errorsHtml);
                    } else {
                        $('#save-company-user-errors').html('');
                    }

                    if (data.validation) {
                        location.reload();
                    }
                }
            }
        });
    });

    $('body').on('click', '#delete-sale, #copy-sale', function () {
        var url = $(this).attr('data-ajax--url'),
            checked =getCheckboxChecked();
        if(checked.length > 0){
            
            var data = {
                sales : checked
            };
            $.ajax({
                method: 'post',
                url: url,
                dataType: 'json',
                data: data,
                success: function(data) {
                    if (!data.error) {
                        var messageTemplate = getMessage(data.message);
                        $.pjax.reload({container:"#sale"});
                        $.sticky(messageTemplate , { stickyClass: 'info' });
                    }
                }
            });
        }else{
            var messageTemplate = getMessage($(this).attr('data-data'));
            $.sticky(messageTemplate , { stickyClass: 'error' });
        }
    });



    function getCheckboxChecked() {
        var i= 0,
            checked = [];
        $('.sale input.item-sale:checkbox:checked').each(function(){
            checked[i]=$(this).val();
            i++;
        });
        
        return checked;
    }

    function getMessage(message) {
        var messageTemplate = '';
        if (message) {
            messageTemplate += '<p>'+message+'</p>';
        }
        return messageTemplate;
    }




        
});
