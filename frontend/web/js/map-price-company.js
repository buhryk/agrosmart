$( document ).ready(function() {
    new google.maps.places.Autocomplete(document.getElementById('mappricesearch-point_location'));
});


var globalLocation = {
    loc:[],

    item:0,
    circle:'',
    radius:0,
    centerPoint : '',
    countPoint:0,
    point_location : '',
    i:0,
    markers:[],
    map:{},
    oms: '',
    init: function () {
        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: {lat: 49.0, lng: 30.53},
            mapTypeId: google.maps.MapTypeId.TERRAIN
        });
        this.oms = new OverlappingMarkerSpiderfier(this.map, {
            markersWontMove: true,
            markersWontHide: true,
            basicFormatEvents: true
        });
        if(typeof radius !=="undefined") {
            this.radius = radius;
            this.setGeoCoder(point_coordinat);
        }
        new google.maps.places.Autocomplete(document.getElementById('mappricesearch-point_location'));
    },

    setGeoCoder : function (point_coordinat) {
        console.log(point_coordinat);
        var latitude = parseFloat(point_coordinat.lat);
        var longitude = parseFloat(point_coordinat.lng);

        globalLocation.centerPoint = new google.maps.LatLng(latitude, longitude);
        var cityCircle = new google.maps.Circle({
            strokeColor: '#c6c6c6',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#c6c6c6',
            fillOpacity: 0.35,
            map: globalLocation.map,
            center: globalLocation.centerPoint,
            radius: globalLocation.radius * 1000
        });
    },

    geoCoderCustomer:function () {
        var infowindow = new google.maps.InfoWindow();
        var item = 0;
        for (var i in locations) {
            var lat = parseFloat(locations[i].coordinat[0]);
            var lng = parseFloat(locations[i].coordinat[1]);
            var latlng = new google.maps.LatLng(lat, lng);


            if(this.radius != 0){
                if (distHaversine(latlng, globalLocation.centerPoint) <= globalLocation.radius) {

                    var marker = new google.maps.Marker({
                        position: {lat: lat, lng: lng},
                        title: locations[i]['address'],
                        clickable: true,
                    });
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            var from_location = globalLocation.centerPoint;
                            var content = locations[i].marker;
                            var distance = (google.maps.geometry.spherical.computeDistanceBetween(
                                new google.maps.LatLng(point_coordinat.lat, point_coordinat.lng),
                                new google.maps.LatLng(locations[i].coordinat[0], locations[i].coordinat[1])
                            ) / 1000).toFixed(2);

                            if (distance) {
                                content += '<div style="margin-top: 10px;">' + locations[i].distanceTranslates + ': ' + distance + ' км</div>';
                            }

                            infowindow.setContent(content);
                            infowindow.open(map, marker);

                        }
                    })(marker, i));
                    this.markers[item] = marker;
                    this.oms.addMarker(marker);
                    item++;
                }
            } else {
                var marker = new google.maps.Marker({
                    position: {lat: lat, lng: lng},
                    title: locations[i]['address'],
                    clickable: true,
                });

                this.markerSet(marker, locations[i].marker);
                this.markers[i] = marker;
                this.oms.addMarker(marker);
            }

        }


    },

    markerSet : function (marker, label) {
        var infowindow = new google.maps.InfoWindow({
            content: label,
            size: new google.maps.Size(70, 30),
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });
    }
}

function initMap() {
    globalLocation.init();
    globalLocation.geoCoderCustomer();
}


rad = function(x) {return x*Math.PI/180;}

//эта функция используются для определения расстояния между точками на
//поверхности Земли, заданных с помощью географических координат
//результат возвращается в км
distHaversine = function(p1, p2) {
    var R = 6371; // earth's mean radius in km
    var dLat  = rad(p2.lat() - p1.lat());
    var dLong = rad(p2.lng() - p1.lng());

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) * Math.sin(dLong/2) * Math.sin(dLong/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    console.log(d.toFixed(3));
    return d.toFixed(3);
}
