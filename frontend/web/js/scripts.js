$(document).ready(function() {
    $('body').on('click', '.change-elected', function() {
        var button = $(this);
        var type = $(this).data('type');
        var record_id = $(this).data('record_id');
        var url = '';
        if (button.hasClass('add-to-elected')) {
            url = '/elected/elected/create';
        } else if (button.hasClass('remove-from-elected')) {
            url = '/elected/elected/delete';
        }
        if (type && record_id && url) {
            $.ajax({
                method: 'post',
                url: url,
                dataType: 'json',
                data: {
                    type: type,
                    record_id: record_id
                },
                success: function(data) {
                    var messageTemplate = getMessageForNotifier(data);
                    $.sticky(messageTemplate, {
                        stickyClass: data.status
                    });
                    if (data.status == 'success') {
                        if (button.data('page') == 'cabinet') {
                            location.reload();
                        } else if (button.hasClass('add-to-elected')) {
                            button.removeClass('add-to-elected');
                            button.addClass('remove-from-elected');
                            button.html(button.data('message-remove'));
                        } else {
                            button.removeClass('remove-from-elected');
                            button.addClass('add-to-elected');
                            button.html(button.data('message-add'));
                        }
                    }
                }
            });
        }
    });
    $('body').on('click', '.drop-from-elected', function() {
        var button = $(this);
        var type = button.data('type');
        var record_id = button.data('record_id');
        var url = '/elected/elected/delete';
        $.ajax({
            method: 'post',
            url: url,
            dataType: 'json',
            data: {
                type: type,
                record_id: record_id
            }
        });
    });
    $('.add-address-to-saved').on('click', function() {
        var url = '/saveddata/saved-address/create';
        var block = $(this);
        var address = block.data('address');
        $.ajax({
            method: 'post',
            url: url,
            dataType: 'json',
            data: {
                address: address
            },
            success: function(data) {
                var messageTemplate = getMessageForNotifier(data);
                $.sticky(messageTemplate, {
                    stickyClass: data.status
                });
                if (data.status == 'success') {
                    $.each($('.add-address-to-saved'), function() {
                        if ($(this).data('address') == address) {
                            $(this).remove();
                        }
                    });
                }
            }
        });
    });
    $('.drop-from-saved-address').on('click', function() {
        var url = '/saveddata/saved-address/delete';
        var address = $(this).data('address');
        $.ajax({
            method: 'post',
            url: url,
            dataType: 'json',
            data: {
                address: address
            }
        });
    });

    function getMessageForNotifier(data) {
        var messageTemplate = '';
        if (data.title) {
            messageTemplate += '<h2>' + data.title + '</h2>';
        }
        if (data.message) {
            messageTemplate += '<p>' + data.message + '</p>';
        }
        return messageTemplate;
    }
    $("#form-advertisement-complaint").submit(function(event) {
        return false;
    });
    $('body').on('click', '#save-complaint', function(event) {
        var form = $('#form-advertisement-complaint');
        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serializeArray(),
            success: function(data) {
                if (data.status) {
                    if (!data.validation && data.errors) {
                        var errorsHtml = '<ul style="list-style-type: initial;padding-left: 15px;">';
                        $.each(data.errors, function(index, value) {
                            errorsHtml += '<li>' + value + '</li>';
                        });
                        errorsHtml += '</ul>';
                        $('#advertisement-complaint-errors').html(errorsHtml);
                    } else {
                        $('#advertisement-complaint-errors').html('');
                    }
                    if (data.validation) {
                        location.reload();
                    }
                }
            }
        });
        return false;
    });
    $('body').on('click', '#send-message-modal', function(event) {
        var form = $('#form-send-message-modal');
        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serializeArray(),
            success: function(data) {
                if (data.status) {
                    if (!data.validation && data.errors) {
                        var errorsHtml = '<ul style="list-style-type: initial;padding-left: 15px;">';
                        $.each(data.errors, function(index, value) {
                            errorsHtml += '<li>' + value + '</li>';
                        });
                        errorsHtml += '</ul>';
                        $('#form-send-message-modal-errors').html(errorsHtml);
                    } else {
                        $('#form-send-message-modal-errors').html('');
                    }
                    if (data.validation) {
                        location.reload();
                    }
                }
            }
        });
        return false;
    });
    $("#company-feedback-form").submit(function(event) {
        return false;
    });
    $('body').on('click', '#send-user-feedback', function(event) {
        var form = $('#company-feedback-form');
        console.log('ssssss');
        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serializeArray(),
            success: function(data) {
                if (data.status) {
                    if (!data.validation && data.errors) {
                        var errorsHtml = '<ul style="list-style-type: initial;padding-left: 15px;">';
                        $.each(data.errors, function(index, value) {
                            errorsHtml += '<li>' + value + '</li>';
                        });
                        errorsHtml += '</ul>';
                        $('#company-feedback-errors').html(errorsHtml);
                    } else {
                        $('#company-feedback-errors').html('');
                    }
                    if (data.status == 'success') {
                        location.reload();
                    }
                }
            }
        });
        return false;
    });
    $('body').on('click', '#send-user-feedback', function(event) {
        var form = $('#company-feedback-form');
        console.log('ssssss');
        $.ajax({
            method: 'post',
            url: form.data('url'),
            dataType: 'json',
            data: form.serializeArray(),
            success: function(data) {
                if (data.status) {
                    if (!data.validation && data.errors) {
                        var errorsHtml = '<ul style="list-style-type: initial;padding-left: 15px;">';
                        $.each(data.errors, function(index, value) {
                            errorsHtml += '<li>' + value + '</li>';
                        });
                        errorsHtml += '</ul>';
                        $('#company-feedback-errors').html(errorsHtml);
                    } else {
                        $('#company-feedback-errors').html('');
                    }
                    if (data.status == 'success') {
                        location.reload();
                    }
                }
            }
        });
        return false;
    });
    $('body').on('click', '.saves-elected  img', function(e) {
        e.stopPropagation();
        $('.info-window-saves').removeClass('open');
        $('.info-window-elected').toggleClass('open');
    });
    $('body').on('click', '.saves-saves img', function(e) {
        e.stopPropagation();
        $('.info-window-elected').removeClass('open');
        $('.info-window-saves').toggleClass('open');
    });
    $('.remove-product-image').on('click', function(e) {
        e.stopPropagation();
        var messageConfirm = $(this).data('message-confirm');
        var imageId = $(this).data('image-id');
        if (confirm(messageConfirm)) {
            $.ajax({
                method: 'post',
                url: '/cabinet/product/remove-image/?id=' + imageId,
                dataType: 'json'
            });
        }
    });
    $('.change-dialog-status').on('click', function() {
        var buttom = $(this);
        $.ajax({
            method: 'get',
            url: buttom.data('url'),
            dataType: 'json',
            data: {
                status: buttom.data('status')
            },
            success: function(data) {
                if (data.status) {
                    location.reload();
                }
            }
        });
    });
});

function tryUpdateLastActivityDate(name) {
    var lastUnixTime = $.cookie(name);

    if (!lastUnixTime) {
        updateLastActivityDate();
    }
}

function updateLastActivityDate() {
    $.ajax({
        method: 'post',
        url: '/set-last-activity-date/index',
        dataType: 'json'
    });
}