
$( document ).ready(function() {
    var register = {
        status: 0,
        init : function () {
            $('#same-user').on('click', function () {
                if ($(this).prop( "checked")) {
                    $('#signupform-username').attr('disabled', true);
                    register.status = 1;
                } else {
                    register.status = 0;
                    $('#signupform-username').attr('disabled', false);
                }
                register.copyData();
            });

            $('#signupform-email').on('keyup', function () {
                register.copyData();
            });
        },
        copyData : function () {
            if (this.status) {
                var email = $('#signupform-email').val();
                if (email.length > 1) {
                    $('#signupform-username').val(email);
                }

            } else {
                $('#signupform-username').val('')
            }
        }
    }

    register.init();

    $('#signupform-phone, #registercompanyform-phones').mask("099-999-99-99");
});