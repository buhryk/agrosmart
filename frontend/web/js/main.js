$(document).ready(function() {
    if (window.objectFitImages) {
        $(function () {
            objectFitImages();
        });
    }

    var Alert = {
        init: function() {
            var text = $('div#alert-info div').text();
            if (text.length > 2) {
                $.notify(text, {
                    animationType: "drop"
                });
            }
        }
    };
    Alert.init();
    $('body').on('click', '.square-close', function() {
        $(this).parents('.one-block').remove();
    });
    $('.sr-btn').appendTo(".extra-menu .navbar-header");
    $('body').on('click', '.sr-btn', function() {
        $("header, footer, [data-content='page_content']").addClass("blur");
        $('.mobile_theme').css({
            left: '0'
        });
    });
    $('body').on('click', '.sr_close', function() {
        $("header, footer, [data-content='page_content']").removeClass("blur");
        $('.mobile_theme').css({
            left: '-100%'
        });
    });
    $('.show-button').click(function() {
        $('.block-aj-show').removeClass('hiddens');
    })
    $('.open-table').on('click', function() {
        $('.description-seo .container').toggleClass('seo-hidden-text');
    });
    $('.ui-loader').remove();
    $('.owl-instruments').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        navText: ['<i class="flaticon-back"></i>', '<i class="flaticon-next"></i>', ],
        responsive: {
            0: {
                nav: true,
                items: 1
            },
            600: {
                nav: true,
                items: 3
            },
            1024: {
                nav: false,
                items: 4
            },
            1170: {
                items: 4
            }
        }
    });
    $('.owl-category').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        navText: ['<i class="flaticon-back"></i>', '<i class="flaticon-next"></i>', ],
        responsive: {
            0: {
                nav: false,
                items: 1
            },
            600: {
                nav: false,
                items: 3
            },
            1024: {
                nav: false,
                items: 4
            },
            1170: {
                items: 6
            }
        }
    });
    $('.owl-product').owlCarousel({
        loop: true,
        margin: 30,
        nav: true,
        dots: false,
        navText: ['<i class="flaticon-back"></i>', '<i class="flaticon-next"></i>', ],
        responsive: {
            0: {
                nav: false,
                items: 1
            },
            600: {
                nav: false,
                items: 2
            },
            1024: {
                nav: false,
                items: 3
            },
            1170: {
                items: 3
            }
        }
    });
    $(function() {
        $("[id$='circle']").percircle();
        $("#custom").percircle({
            text: "",
            percent: $("#custom").attr('data-value')
        });
        $("#custom2").percircle({
            text: "",
            percent: $("#custom2").attr('data-value')
        });
        $("#custom3").percircle({
            text: "",
            percent: $("#custom3").attr('data-value')
        });
    });
    $(".important-info__hide").click(function() {
        $(".important-info-content").slideToggle();
        $(this).toggleClass("rotate");
    });
    $(".important-info__close").click(function() {
        $(".important-info").fadeOut("slow");
    });
    $("#subheading").chainedTo("#heading");
    $('body').imagepreview({
        input: '[id="image1"]',
        preview: '.item1'
    });
    $('body').imagepreview({
        input: '[id="image2"]',
        preview: '.item2'
    });
    $('body').imagepreview({
        input: '[id="image3"]',
        preview: '.item3'
    });
    $('body').imagepreview({
        input: '[id="image4"]',
        preview: '.item4'
    });
    $('body').imagepreview({
        input: '[id="image5"]',
        preview: '.item5'
    });
    $('body').imagepreview({
        input: '[id="image6"]',
        preview: '.item6'
    });
    $(".ap-form-photo input").on("change", function() {
        var inputValue = $(this).val();
        var fileExtFinder = inputValue.indexOf(".");
        var fileExt = inputValue.slice(fileExtFinder + 1);
        if (fileExt === "jpg" || fileExt === "png" || fileExt === "jpeg") {
            $(this).parent().next().css("display", "block")
        } else {
            $(this).val('');
            var imgBlock = $(this).next().next().next();
            console.log(imgBlock);
            setTimeout(function() {
                imgBlock.empty();
            }, 1)
            alert("Поддерживаются только форматы jpg, jpeg, png");
        }
    });
    $(".ap-form-photo__close").click(function() {
        $(this).prev().val('');
        $(this).next().next().empty();
        if ($(this).parent().hasClass('mb-hidden')) {
            $(this).parent().css("display", "none")
        }
    });
    $("body").on("click", ".languages__select", function() {
        console.log("dsd");
        $(".navigation__languages").slideToggle();
    });
    $('#contact-phone, #user-phone, #subsidiary-phone, #advertisementform-phones').mask("099-999-99-99");
});
$(window).on('load resize', function(e) {
    if ($(this).width() <= 768) {
        $('.apto').appendTo('.apend');
        $('.mobile-owl-content').owlCarousel({
            loop: true,
            margin: 0,
            nav: false,
            dots: false,
            navText: ['<i class="flaticon-back"></i>', '<i class="flaticon-next"></i>', ],
            autoplay:true,
            autoplayTimeout: 2000,
            responsive: {
                0: {
                    nav: false,
                    items: 1
                },
                1024: {
                    nav: false,
                    items: 4
                },
                1170: {
                    items: 4
                }
            }
        });
    } else {
        $('.mobile-owl-content').trigger('destroy.owl.carousel');
    }
});
$("#message-all").click(function() {
    $('.message-container input:checkbox').not(this).prop('checked', this.checked);
});
$("#add-file").on("change", function() {
    if ($(this).val().length !== 0) {
        $('.load-com-logo p.text_grey').css("display", "block");
    }
    $(".input-file-name").text($(this).val().slice(12));
});
$("#choose-logo-file").on("change", function() {
    if ($(this).val().length !== 0) {
        $('.field-choose-logo-file').css("display", "block");
    }
    $(".company-image-name").text($(this).val().slice(12));
});
$(".checked-all-rubric").click(function() {
    var status = $(this).attr('data-value');
    if (status == 'true') {
        $(".an_chekbox").prop("checked", true);
        $(this).attr('data-value', 'false');
    } else {
        $(".an_chekbox").removeAttr("checked");
    }
});
$(".checked-all-rubric").click(function() {
    var status = $(this).attr('data-value');
    if (status == 'true') {
        $(".an_chekbox").prop("checked", true);
        $(this).attr('data-value', 'false');
    } else {
        $(".an_chekbox").removeAttr("checked");
    }
});
$(".item-rubric input.an_chekbox_rubric").on('change', function() {
    var element = $(this).parents('.item-rubric');
    if ($(this).prop("checked")) {
        element.find(".an_chekbox").prop("checked", true)
    } else {
        element.find(".an_chekbox").prop("checked", false)
    }
});