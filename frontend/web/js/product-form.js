$( document ).ready(function() {
    $('body').on('change', '.select-rubric-dropdownlist', function() {
        var url = $('#rubric_id').data('url');
        var rubricId = $(this).val();
        var url_get_rubric_measurements = $('#rubric_id').data('url-get-rubric-measurements');
        var currentLevel = $(this).data('level');

        var lists = $('.select-rubric-dropdownlist');
        $.each(lists, function() {
            if ($(this).data('level') > currentLevel) {
                $(this).remove();
            }
        });

        if (rubricId && rubricId > 0) {
            $.ajax({
                method: 'post',
                url: url,
                dataType: 'json',
                data: {rubric_id: rubricId},
                success: function(data) {
                    if (data.status) {
                        if (data.subrubrics.length != 0) {
                            var htmlList = '<select class="ap-select-heading select-rubric-dropdownlist" data-level="'+(currentLevel + 1)+'">';
                            htmlList += '<option>...</option>';

                            $.each(data.subrubrics, function(index, value) {
                                $.each(value, function(id, title) {
                                    htmlList += '<option value="' + id + '">' + title + '</option>';
                                });
                            });
                            htmlList += '</select>';

                            $('#rubrics-container').append(htmlList);
                        } else {
                            $('#rubric_id').val(rubricId);
                            getRubricMeasurements(rubricId, url_get_rubric_measurements, 'choose-measurement');
                        }
                    } else {
                        $('#rubric_id').val('');
                        $('#choose-measurement').html('');
                    }
                }
            });
        } else {
            $('#rubric_id').val('');
            $('#choose-measurement').html('');
        }
    });

    var status = true;

    $('#submit-advertisement-form').on('click', function (e) {
        e.preventDefault();
        if (status == true) {
            status = false;
            var form = $('#advertisement-form');
            var url = form.data('url');
            var formElement = document.getElementById("advertisement-form");
            var data = new FormData(formElement);
            $('.custom-error-block').hide();

            $.ajax({
                method: 'post',
                url: url,
                dataType: 'json',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function(data) {
                    status = true;
                    if (data.status) {
                        if (data.validation) {
                            $(location).attr("href", data.success_url);
                        }

                        if (data.errors) {
                            $.each(data.errors, function(index, value) {
                                var errorBlock = $('#error-'+index);
                                errorBlock.html(value).show();
                            });

                            var scrollToError = false;

                            var visibleErrorBlocks = $('.custom-error-block');
                            $.each(visibleErrorBlocks, function() {
                                if ($(this).css('display') == 'block' && !scrollToError) {
                                    scrollTo($(this).attr('id'));
                                    scrollToError = true;
                                }
                            });
                        }
                    }
                }
            });
        }

    });

    function getRubricMeasurements(rubric_id, url, list_id) {
        $.ajax({
            method: 'post',
            url: url,
            dataType: 'json',
            data: {rubric_id: rubric_id},
            success: function(data) {
                if (data.status) {
                    if (data.measurements) {
                        var htmlList = '';
                        $.each(data.measurements, function(index, value) {
                            htmlList += '<optgroup label="'+value.title+'">';
                            var options = '';
                            $.each(value.measurements, function(index, value) {
                                options += '<option value="'+index+'">'+value+'</option>';
                            });
                            htmlList += options;
                            htmlList += '</optgroup>';
                        });

                        $('#'+list_id).html(htmlList);
                    }
                }
            }
        });
    }

    function scrollTo(id){
        id = id.replace("link", "");
        $('html,body').animate({
            scrollTop: ($("#"+id).offset().top - 60)
        }, 'slow');
    }

    $('.select-author').on('change', function () {
        var value = $(this).val();

        if (value == 'other') {
            $('#company-users-dropdown').removeClass('hidden');
        } else {
            $('#company-users-dropdown').addClass('hidden');
        }

        if (value == 'subsidiary') {
            $('#company-subsidiaries-dropdown').removeClass('hidden');
        } else {
            $('#company-subsidiaries-dropdown').addClass('hidden');
        }

    });

    $('#company-users-dropdown').on('change', function () {
        var value = $(this).val();
        if (value) {
            $('#author_id').val(value);
        } else {
            $('#author_id').val($('#author_id').data('default-user'));
        }
    });
    
    $('#price').on('click', function () {
        if($(this).prop("checked")){
            $('#advertisementform-price').prop( "disabled", true);
        } else   {
            $('#advertisementform-price').prop( "disabled", false);
        }

    })
    $('#until').on('click', function () {
        if($(this).prop("checked")){
            $('#advertisementform-active_to').prop( "disabled", true);
        } else   {
            $('#advertisementform-active_to').prop( "disabled", false);
        }

    })

    
});