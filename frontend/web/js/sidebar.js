//$.getScript("another_script.js");
// ====== CONSTANTS ====== //
const breakepoint = 768;
const $sidebar = $('#sidebar');
const $blocker = $('#blocker');
const $blured = $("header, footer, [data-content='page_content']");

// ====== FUNCTIONS ====== //
function sidebar_mobile() {
	ww = $(window).width();
	if (ww <= breakepoint){
		$sidebar.addClass("mobile_theme");
		$sidebar.css("left", "-100%");
		// if ($sidebar.css("left") == "0px"){//фикс баги
		// 	$blured.addClass("blur");
		// }
	}
	else {
		$sidebar.removeClass("mobile_theme");
		$blured.removeClass("blur");
		$sidebar.css("left", "0");
	}
}
//sidebar init
$(document).ready(sidebar_mobile());
$(window).resize(function() {
	sidebar_mobile();
});
$(document).on("swipeleft swiperight", function(event) {
	if (ww <= breakepoint){
		if (event.type === "swipeleft") {//open sidebar
				$sidebar.css("left", "-300px")
				$blocker.css("display","none");
				$blured.removeClass("blur");
	  } else if (event.type === "swiperight") {//close sidebar
				$blocker.css("display","block");
				$sidebar.css("left", "0px");
				$blured.addClass("blur");
	  }
	}
});
