for (var i = 0 ; i < locations.length; i++){
    locations[i].isGeocodingSuccess = false;
}

var geocoder = new google.maps.Geocoder();
var counter = 0;


buildMap(locations);

function buildLocation(location) {
    geocoder.geocode({
        'address': location.detailAddress
    },function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            location.isGeocodingSuccess = true;
            location.coordinates = {
                lat: results[0].geometry.location.lat(),
                lng: results[0].geometry.location.lng()
            };
        }
    });
}


function buildMap(locations) {
            var bounds = new google.maps.LatLngBounds();

            var map = new google.maps.Map(document.getElementById('subsidiaries-map'), {
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i].coordinates.lat, locations[i].coordinates.lng),
                    map: map
                });

                bounds.extend(marker.position);

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i].detailAddress);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }

            map.fitBounds(bounds);

}