// calculate which map to be used

var titles = [];

titles.push( {
    "text": ""
} );


var map = AmCharts.makeChart( "map", {
    "type": "map",
    "language": "ru",
    "theme": "light",
    "colorSteps": 10,
    "dataProvider": {
        "mapURL": "/images/map/ukraineLow.svg",
        "getAreasFromMap": true,
        "zoomLevel": 0.9,
        "areas": []
    },
    "areasSettings": {
        "autoZoom": false,
        "balloonText": "[[title]]: <strong>[[value]]</strong>"
    },
    "valueLegend": {
        "right": 10,
        "minValue": "Min",
        "maxValue": "Max"
    },
    "zoomControl": {
        "minZoomLevel": 0.9
    },
    "titles": titles,
    "listeners": [ {
        "event": "init",
        "method": updateHeatmap
    } ]
} );


function updateHeatmap( event ) {

    var map = event.chart;
    if ( map.dataGenerated )
        return;
    if ( map.dataProvider.areas.length === 0 ) {
        setTimeout( updateHeatmap, 100 );
        return;
    }

    console.log(map.dataProvider.areas);

    for ( var i = 0; i < map.dataProvider.areas.length; i++ ) {
        console.log( i );
        console.log(map.dataProvider.areas[ i ]);
        if(mapData[i] == undefined){
            map.dataProvider.areas[ i ].value = 0;
        } else {
            console.log(map.dataProvider.areas[ i ]);
            map.dataProvider.areas[ i ].value = mapData[i].value;
            map.dataProvider.areas[ i ].title = mapData[i].title;
        }
    }

    if ( map.dataProvider.areas[12]) {
        map.dataProvider.areas[11].value = map.dataProvider.areas[12].value;
    }

    map.dataGenerated = true;
    map.validateNow();
}

//
// $("body").on( "click", "#map path" ,function(){
//     var oblast = $(this).attr("aria-label"),
//     url = $('#get-city-url').attr('data-ajax--url'),
//         value = $('#get-city-url').val();
//     var  data = {
//         oblast:oblast,
//         value: value,
//     };
//
//     $.ajax({
//         method: 'get',
//         url: url,
//         dataType: 'json',
//         data: data,
//         success: function(data) {
//             if (data.status) {
//                 window.location.href= data.result;
//             }
//         }
//     });
// })