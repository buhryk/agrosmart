<?php
namespace frontend\models;

use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class CompanyUserPasswordForm extends Model
{


    public $password;

    /**
     * @var \common\models\User
     */
    private $_user;
    public $new_password;
    public $repeat_new_password;

    public function rules()
    {
        return [
            [[ 'new_password', 'repeat_new_password'],'required'],
            ['repeat_new_password', 'compare', 'compareAttribute' => 'new_password'],
        ];
    }


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByEmailConfirmToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */


    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;

        $user->setPassword($this->new_password);
        $user->generateAuthKey();
        $user->removeEmailConfirmToken();
        $user->status = $user::STATUS_ACTIVE;


        return $user->save(false);
    }

}
