<?php
namespace frontend\models;

use yii\base\Model;
use frontend\models\Company;
use frontend\models\CompanyUser;

/**
 * Signup form
 */
class CompanySignupForm extends Model
{
    public $name;
    public $type;
    public $role_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            [['name', 'type'], 'required'],
            ['role_id', 'integer'],
            ['role_id', 'in', 'range' => array_keys(CompanyUser::getRolesList())],
            ['name', 'unique', 'targetClass' => '\frontend\models\Company', 'message' => 'This name has already been taken.'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            [['type'], 'match', 'pattern' => "/[^\.]$/"],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('users', 'COMPANY_NAME'),
            'type' => \Yii::t('users', 'COMPANY_TYPE'),
            'role_id' => \Yii::t('users', 'Role'),
        ];
    }

    /**
     * Save Company up.
     *
     * @return Company|null the saved model or null if saving fails
     */
    public function saveCompany()
    {
        if (!$this->validate()) {
            return null;
        }

        $company = new Company();
        $company->name = $this->name;
        $company->type = $this->type;

        if ($company->save()) {
            $company_user = new CompanyUser();
            $company_user->user_id = \Yii::$app->user->id;
            $company_user->company_id = $company->id;
            $company_user->role_id = CompanyUser::ROLE_ADMINISTRATOR;

            if ($company_user->save()) {
                return $company;
            }
        }

        return null;
    }
}
