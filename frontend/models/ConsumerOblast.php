<?php

namespace frontend\models;

use backend\modules\commondata\models\Region;
use Yii;

/**
 * This is the model class for table "consumer_oblast".
 *
 * @property integer $user_id
 * @property integer $oblast_id
 */
class ConsumerOblast extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consumer_oblast';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'oblast_id'], 'integer'],
            [['user_id', 'oblast_id'], 'required'],
            [['user_id'], 'unique'],
            [['oblast_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['oblast_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'oblast_id' => 'Область',
        ];
    }
}
