<?php

namespace frontend\models;

use backend\modules\price2\models\Price;
use backend\modules\rubric\models\Rubric;
use common\helpers\ReplaceLinkHelper;
use common\helpers\SitemapHelper;
use frontend\modules\cabinet\models\ConsumerResponsible;
use frontend\modules\product\models\Advertisement;
use frontend\modules\cabinet\models\Sale;
use frontend\modules\cabinet\models\Subsidiary;
use frontend\modules\company\models\CompanyMark;
use frontend\modules\company\models\Feedback;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use frontend\modules\cabinet\models\Top;
use backend\modules\tariff\models\Tariff;
use yii\helpers\Html;
use yii2mod\comments\models\CommentModel;
use yii2mod\moderation\enums\Status;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 *
 * @property CompanyUser[] $companyUsers
 */
class Company extends ActiveRecord
{
    const TYPE_TRADER = 1;
    const TYPE_FARMER = 2;
    const TYPE_LOGIST = 3;
    const TYPE_DISTRIBUTOR = 4;
   // const TYPE_DEALER = 5;
    const TYPE_RECYCLER = 6;
    const TYPE_SERVICE = 7;

    const STATUS_ACTIVE_YES = 1;
    const STATUS_ACTIVE_NO = 0;
    const CONFIRMED_YES = 1;
    const CONFIRMED_NO = 0;

    public $logo_file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'status', 'last_activity_date', 'is_confirmed'], 'integer'],
            ['ratio', 'number'],
            ['type', 'in', 'range' => [self::TYPE_TRADER, self::TYPE_FARMER, self::TYPE_LOGIST, self::TYPE_DISTRIBUTOR, self::TYPE_RECYCLER, self::TYPE_SERVICE]],
            [['name'], 'string', 'max' => 255],
            [['logotype', 'short_description', 'full_description'], 'safe'],
            ['logo_file', 'file', 'extensions' => ['png', 'jpg', 'gif', 'jpeg'], 'maxSize' => 1024*1024*2],
            ['status', 'default', 'value' => self::STATUS_ACTIVE_YES],
            [['last_activity_date', 'ratio'], 'default', 'value' => 0],
            ['created_at', 'integer'],
            ['created_at', 'default', 'value' => function () {
                return time();
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('users', 'ID'),
            'name' => Yii::t('users', 'COMPANY_NAME'),
            'type' => Yii::t('users', 'COMPANY_TYPE'),
            'logotype' => Yii::t('common', 'LOGOTYPE'),
            'logo_file' => Yii::t('common', 'LOGOTYPE'),
            'status' => Yii::t('common', 'Status'),
            'ratio' => Yii::t('common', 'Ratio'),
            'last_activity_date' => Yii::t('common', 'Last activity date'),
            'is_confirmed' => Yii::t('common', 'Is company confirmed'),
            'short_description' => Yii::t('common', 'Short about company'),
            'full_description' => Yii::t('common', 'Company full description'),
        ];
    }

    public function getName()
    {
        return Html::encode($this->name);
    }

    public function getFullDescription()
    {
        return ReplaceLinkHelper::replace($this->full_description);
    }

    public function getShortDescription()
    {
        return ReplaceLinkHelper::replace($this->short_description);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUsers()
    {
        return $this->hasMany(CompanyUser::className(), ['company_id' => 'id'])->joinWith('user');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubric()
    {
        return $this->hasMany(CompanyRubric::className(), ['company_id' => 'id']);
    }

    function getPrices()
    {
        return $this->hasMany(Price::className(), ['company_id' => 'id']);
    }

    public function getRubrics()
    {
        return $this->hasMany(Rubric::className(), ['id' => 'rubric_id'])
            ->viaTable(CompanyRubric::tableName(), ['company_id' => 'id'])
            ->joinWith('lang');
    }
    
    public function getCompanyZakupka(){
        return $this->hasMany(Sale::className(), ['company_id' => 'id'])
            ->where(['type' => Sale::TYPE_ZAKUPKA])
            ->where(['between', 'date', date('Y-m-d', time()-(60*60*24*30)), date('Y-m-d', time()+(60*60*24*30))])
            ->orderBy(['updated_at' => SORT_DESC])
            ->limit(5);
    }

    public function getSale(){
        return $this->hasMany(Sale::className(), ['company_id' => 'id']);
    }

    public static function getTypesList()
    {
        return [
            self::TYPE_TRADER => Yii::t('cabinet', 'COMPANY_TYPE_TRADER'),
            self::TYPE_FARMER => Yii::t('cabinet', 'COMPANY_TYPE_FARMER'),
            self::TYPE_LOGIST => Yii::t('cabinet', 'COMPANY_TYPE_LOGIST'),
            self::TYPE_DISTRIBUTOR => Yii::t('cabinet', 'Дилер/Дистрибьютор'),
            self::TYPE_RECYCLER => Yii::t('cabinet', 'Производитель/Переработчик'),
            self::TYPE_SERVICE => Yii::t('cabinet', 'Услуги для АПК'),
        ];
    }

    public function getTypeTitle()
    {
        $typesList = self::getTypesList();

        foreach ($typesList as $key => $value) {
            if ($this->type == $key) {
                return $value;
            }
        }

        return 'undefined';
    }

    public function rubricSave($data = []){
        CompanyRubric::deleteAll(['company_id'=>$this->id]);

        if (is_array($data)) {
            foreach ($data as $item) {
                $model = new CompanyRubric();
                $model->rubric_id = $item;
                $model->company_id = $this->id;
                $model->save();
            }
        }
    }

    public function updateLastActivityDate()
    {
        $this->last_activity_date = time();
        $this->update(false);
    }

    public static function getAllStatuses()
    {
        return [
            self::STATUS_ACTIVE_YES => Yii::t('common', 'Active company'),
            self::STATUS_ACTIVE_NO => Yii::t('common', 'Not active company'),
        ];
    }

    public function getDetailStatus()
    {
        foreach (self::getAllStatuses() as $key => $title) {
            if ($this->status == $key) {
                return $title;
            }
        }

        return 'Undefined';
    }

    public static function getAllIsConfirmedProperties()
    {
        return [
            self::CONFIRMED_NO => Yii::t('common', 'Company not confirmed'),
            self::CONFIRMED_YES => Yii::t('common', 'Company confirmed')
        ];
    }

    public function getIsConfirmedDetail()
    {
        return isset(self::getAllIsConfirmedProperties()[$this->is_confirmed]) ?
            self::getAllIsConfirmedProperties()[$this->is_confirmed] : 'Undefined';
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->via('companyUsers');
    }

    public function getAdmin()
    {
        return $this->hasOne(CompanyUser::className(), ['company_id' => 'id'])
            ->joinWith('user')
            ->andWhere(['role_id' => CompanyUser::ROLE_ADMINISTRATOR]);
    }

    public function getAdmins()
    {
        return $this->hasMany(CompanyUser::className(), ['company_id' => 'id'])
            ->joinWith('user')
            ->andWhere(['role_id' => CompanyUser::ROLE_ADMINISTRATOR]);
    }

    public function getAdvertisementsCount()
    {
        return Advertisement::find()
            ->where(['company_id' => $this->primaryKey])
            ->andWhere([Advertisement::tableName().'.status' => Advertisement::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
            ->count();
    }

    public function getSaleZakupkaCount()
    {
        return Sale::find()
            ->andWhere(['company_id' => $this->primaryKey])
            ->andWhere(['type' => Sale::TYPE_ZAKUPKA])
            ->andWhere(['date' => $this->lastDateSale(Sale::TYPE_ZAKUPKA)])
            ->andWhere(['between', 'date', date('Y-m-d', time()-(60*60*24*30)), date('Y-m-d', time()+(60*60*24*30))])
            ->count();
    }

    public function getSaleProdCount()
    {
        return Sale::find()
            ->andWhere(['company_id' => $this->primaryKey])
            ->andWhere(['type' => Sale::TYPE_PRODAZHA])
            ->andWhere(['date' => $this->lastDateSale(Sale::TYPE_PRODAZHA)])
            ->andWhere(['between', 'date', date('Y-m-d', time()-(60*60*24*30)), date('Y-m-d', time()+(60*60*24*30))])
            ->count();
    }

    private function lastDateSale($type)
    {
        $model = Sale::find()
            ->andWhere(['company_id' => $this->id])
            ->andWhere(['type' => $type])
            ->andWhere(['<=','date', date('y-m-d')])
            ->orderBy(['DATE(`date`)' => SORT_DESC])
            ->one();

        return $model ? $model->date: '';
    }

    public function getAdvertisements($count = null)
    {
        $query = Advertisement::find()
            ->where(['company_id' => $this->primaryKey])
            ->andWhere([Advertisement::tableName().'.status' => Advertisement::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
            ->orderBy(['created_at' => SORT_DESC]);

        if ($count) {
            $query = $query->limit($count);
        }

        return $query->all();
    }

    public function getSubsidiaries()
    {
        return $this->hasMany(Subsidiary::className(), ['company_id' => 'id']);
    }

    public function getMainSubsidiary()
    {
        return $this->hasOne(Subsidiary::className(), ['company_id' => 'id'])
            ->andWhere(['type' => Subsidiary::TYPE_MAIN_OFFICE]);
    }

    public function getResponsible()
    {
            return $this->hasOne(ConsumerResponsible::className(), ['consumer_id' => 'id'])
                ->andWhere(['type' => ConsumerResponsible::TYPE_COMPANY]);

    }

    public function canCreateFeedback()
    {
        if (!Yii::$app->user->isGuest) {
            $userCompany = Yii::$app->user->identity->company;
            if ($userCompany && $userCompany->id == $this->primaryKey) {
                return false;
            }
            $lastFeedback = Feedback::find()
                ->andFilterWhere([
                    'from_user_id' => Yii::$app->user->identity->id,
                    'company_id' => $this->primaryKey])
                ->andWhere('created_at >= :created_at', [':created_at' => (time()-5*60*60)])
                ->one();

            if (!$lastFeedback) {
                return true;
            }
        }

        return false;
    }

    public function canCreateMark()
    {
        if (!Yii::$app->user->isGuest) {
            $currentUserCompany = Yii::$app->user->identity->company;
            $mark = CompanyMark::find()
                ->where([
                    'from_user_id' => Yii::$app->user->identity->id,
                    'company_id' => $this->primaryKey
                ])
                ->one();

            if (!$mark && (!$currentUserCompany || ($currentUserCompany->primaryKey != $this->primaryKey))) {
                return true;
            }
        }

        return false;
    }

    public function getFeedbacks($limit = false)
    {
        return Feedback::find()
            ->where(['company_id' => $this->primaryKey])
            ->limit($limit)
            ->orderBy('created_at DESC')
            ->joinWith('user')
            ->all();
    }

    public function getFeedbackStatistic()
    {
        $models = Feedback::find()
            ->where(['company_id' => $this->primaryKey])
            ->all();

        $data = [
            'price_relevance' => [
                'title' => Yii::t('common', 'Price relevance'),
                'countPoints' => 0,
                'points' => 0
            ],
            'description_relevance' => [
                'title' => Yii::t('common', 'Description relevance'),
                'countPoints' => 0,
                'points' => 0
            ],
            'conditions_relevance' => [
                'title' => Yii::t('common', 'Conditions relevance'),
                'countPoints' => 0,
                'points' => 0
            ],
        ];

        foreach ($models as $model) {
            foreach ($data as $key => $row) {
                if ($model->$key) {
                    $data[$key]['countPoints'] = $data[$key]['countPoints'] + 1;
                }
                if ($model->$key == Feedback::ANSWER_POSITIVE) {
                    $data[$key]['points'] = $data[$key]['points'] + 1;
                }
            }
        }

        foreach ($data as $key => $row) {
            if ($row['countPoints']) {
                $data[$key]['percentage'] = (int)($row['points'] / $row['countPoints'] * 100);
            }
        }

        return $data;
    }

    public function getMarks()
    {
        return $this->hasMany(CompanyMark::className(), ['company_id' => 'id']);
    }

    public function updateRatio()
    {
        $averageMark = CompanyMark::find()
            ->where(['company_id' => $this->primaryKey])
            ->average('mark');

        $this->ratio = round($averageMark, 1);
        $this->update();
    }

    public function getAdvertisementsRubrics()
    {
        return Rubric::find()
            ->where([
                'in',
                Rubric::tableName().'.id',
                ArrayHelper::getColumn(
                    Advertisement::find()
                        ->where([
                            'company_id' => $this->primaryKey,
                            'status' => Advertisement::STATUS_ACTIVE_YES
                        ])
                        ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
                        ->groupBy('rubric_id')
                        ->all(),
                    'rubric_id'
                )
            ])
            ->andWhere(['active' => Rubric::ACTIVE_YES])
            ->joinWith('lang')
            ->all();
    }

    public function getContactInfo()
    {
        $info = [];
        $mainSubsidiary = Subsidiary::find()
            ->where(['company_id' => $this->primaryKey, 'type' => Subsidiary::TYPE_MAIN_OFFICE])
            ->one();

        if ($mainSubsidiary) {
            if ($mainSubsidiary->email) {
                $info['email'] = $mainSubsidiary->email;
            }
            if ($mainSubsidiary->address && $mainSubsidiary->city_id) {
                $info['location'] = $mainSubsidiary->location;
            }
            if ($mainSubsidiary->phone) {
                $info['phone'] = $mainSubsidiary->phone;
            }
        }

        return $info;
    }

    public function getAllotted()
    {
        return $this->hasOne(Top::className(), ['model_id' => 'id'])
            ->andWhere(['model_name' => Tariff::MODEL_COMPANY])
            ->andWhere(['in','type' , [Tariff::TYPE_ALLOTTED, Tariff::TYPE_COMPLEX]])
            ->andWhere(['<=', 'start', time()])
            ->andWhere(['>=', 'finish', time()]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->status == self::STATUS_ACTIVE_NO) {
            $users = $this->users;
            User::updateAll(['status' => User::STATUS_BANNED], ['id' => ArrayHelper::getColumn($users, 'id')]);
            CommentModel::updateAll(['status' => Status::PENDING], ['createdBy' => ArrayHelper::getColumn($users, 'id')]);

        } elseif ($this->status == self::STATUS_ACTIVE_YES) {
            User::updateAll(['status' => User::STATUS_ACTIVE], ['id' => ArrayHelper::getColumn($this->users, 'id')]);
        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function beforeDelete()
    {
        $users = CompanyUser::find()->where(['company_id' => $this->id])->all();
        $prices = Price::find()->where(['company_id' => $this->id])->all();

        foreach ($users as $item) {
            $item->delete();
        }

        foreach ($prices as $item) {
            $item->delete();
        }

        Sale::deleteAll(['company_id' => $this->id]);
        Subsidiary::deleteAll(['company_id' => $this->id]);

        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}