<?php

namespace frontend\models;

use backend\modules\rubric\models\Rubric;
use Yii;

/**
 * This is the model class for table "company_rubric".
 *
 * @property integer $company_id
 * @property integer $rubric_id
 */
class CompanyRubric extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_rubric';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'rubric_id'], 'required'],
            [['company_id', 'rubric_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'rubric_id' => 'Rubric ID',
        ];
    }

    public function getRubric()
    {
        return $this->hasOne(Rubric::className(), ['id' => 'rubric_id'])->joinWith('lang');
    }
}
