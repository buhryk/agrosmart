<?php
namespace frontend\models;

use backend\modules\commondata\models\City;
use backend\modules\commondata\models\Region;
use frontend\modules\cabinet\models\Subsidiary;
use yii\base\Model;
use Yii;
use common\helpers\GoogleMapsApiHelper;

/**
 * Signup form
 */
class RegisterCompanyForm extends Model
{
    public $name;
    public $type;
    public $phones;
    public $email;
    public $city_id;
    public $address;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'phones', 'email', 'address', 'city_id'], 'required'],
            [['name', 'phones', 'email', 'address'], 'trim'],
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['email', 'email'],
            ['address', 'string'],
            ['email', 'unique', 'targetClass' => 'frontend\modules\cabinet\models\Subsidiary', 'message' => \Yii::t('users', 'This email address has already been taken')],
           // [['address'], 'validateLocation'],
            [['type', 'city_id'], 'integer'],
            ['phones', 'validatePhones'],
            ['type', 'in', 'range' => [Company::TYPE_TRADER, Company::TYPE_FARMER, Company::TYPE_LOGIST,
                Company::TYPE_DISTRIBUTOR, Company::TYPE_RECYCLER, Company::TYPE_SERVICE]],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }


    public function validateLocation($attribute, $params)
    {
        $city = City::findOne($this->city_id);
        $region = Region::findOne($city->oblast_id);
        $location = $region->title.', '. $city->title.', '.$this->address;;
        $googleMapsApiHelper = new GoogleMapsApiHelper();

        $coordinates = $googleMapsApiHelper->getLocationByAddress($location);
        if (!$coordinates) {
            $this->addError($attribute, Yii::t('instruments', 'Wrong location', ['field' => $this->getAttributeLabel($attribute)]));

        }
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Название'),
            'type' => Yii::t('common', 'Тип'),
            'phones' => Yii::t('common', 'Телефон главного офиса'),
            'email' => Yii::t('common', 'Email главного офиса'),
            'address' => Yii::t('common', 'Адрес'),
            'city_id' => Yii::t('common', 'Населенный пункт'),

        ];
    }

    public function validatePhones($attribute, $params)
    {
        if ($this->phones) {

            $phones = explode(',', $this->phones);
            foreach ($phones as $key => $phone) {
                $phones[$key] = trim($phone);
            }

            foreach ($phones as $key => $phone) {
                if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/", $phone)) {
                    $this->addError('phones', Yii::t('advertisement', 'Phone numbers must be written in the format', ['format' => '0XX-XXX-XX-XX']));
                }
            }
        }
    }

    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $company = new Company();
        $company->name = $this->name;
        $company->type = $this->type;

        $subsidiary = new Subsidiary();
        $subsidiary->company_id = 0;
        $subsidiary->type = Subsidiary::TYPE_MAIN_OFFICE;
        $subsidiary->city_id = $this->city_id;
        $subsidiary->email = $this->email;
        $subsidiary->address = $this->address;
        $subsidiary->phone = $this->phones;
        $subsidiary->validate();

        if ($company->validate() && (count($subsidiary->getErrors()) == 0 || (count($subsidiary->getErrors()) == 1
                    && isset($subsidiary->getErrors()['company_id']))) && $company->save()) {

            $subsidiary->company_id = $company->primaryKey;
            $subsidiary->save();
            return $company;
        }

        return null;
    }
}
