<?php
namespace frontend\models;

use backend\modules\contact\models\Contact;
use backend\modules\price2\models\Price;
use backend\modules\rubric\models\Rubric;
use common\helpers\SitemapHelper;
use frontend\modules\cabinet\models\Accounting;
use frontend\modules\cabinet\models\ConsumerResponsible;
use frontend\modules\cabinet\models\Dialog;
use frontend\modules\cabinet\models\DialogDetail;
use frontend\modules\cabinet\models\DialogMessage;
use frontend\modules\cabinet\models\PackageUser;
use frontend\modules\cabinet\models\PricesDispath;
use frontend\modules\cabinet\models\Refill;
use frontend\modules\company\models\CompanyMark;
use frontend\modules\complaint\models\Complaint;
use frontend\modules\product\models\Advertisement;
use frontend\modules\cabinet\models\Subsidiary;
use frontend\modules\elected\models\Elected;
use frontend\modules\saveddata\models\SavedAddress;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use yii2mod\comments\models\CommentModel;
use yii2mod\moderation\enums\Status;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $surname
 * @property string $phone
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_NOT_CONFIRMED = 1;
    const STATUS_BANNED = 2;
    const STATUS_ACTIVE = 10;
    const SCENARIO_CREATE_COMPANY_USER = 'createCompanyUser';
    const CAN_ADD_ADVERTISEMENTS_YES = 1;
    const CAN_ADD_ADVERTISEMENTS_NO = 0;

    const USER_NEW = 1;
    const USER_OLD = 0;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consumer';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'phone'], 'required'],
            [['username', 'email', 'phone'], 'unique'],
            [['status', 'created_at', 'updated_at', 'last_activity_date', 'can_add_advertisements', 'is_new', 'oblast_id'], 'integer'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_NOT_CONFIRMED, self::STATUS_BANNED]],
            ['phone', 'string', 'max' => 20],
            [['name', 'surname'], 'string'],
            [['name', 'surname'], 'required'],
            [['name', 'surname', 'phone', 'email'], 'required', 'on' => self::SCENARIO_CREATE_COMPANY_USER],
            [['username', 'email'], 'unique', 'on' => self::SCENARIO_CREATE_COMPANY_USER],
            ['status', 'default', 'value' => self::STATUS_NOT_CONFIRMED],
            ['last_activity_date', 'default', 'value' => 0],
            ['can_add_advertisements', 'in', 'range' => [self::CAN_ADD_ADVERTISEMENTS_YES, self::CAN_ADD_ADVERTISEMENTS_NO]],
            ['can_add_advertisements', 'default', 'value' => self::CAN_ADD_ADVERTISEMENTS_YES],
            ['phone', 'validatePhone'],
            ['oblast_id', 'integer'],
            ['email', 'email'],


        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('users', 'ID'),
            'username' => Yii::t('users', 'USERNAME'),
            'name' => Yii::t('users', 'NAME'),
            'surname' => Yii::t('users', 'SURNAME'),
            'phone' => Yii::t('users', 'PHONE'),
            'last_activity_date' => Yii::t('common', 'Last activity date'),
            'can_add_advertisements' => Yii::t('users', 'Can add advertisements')
        ];
    }

    public function validatePhone($attribute, $params)
    {
        if ($this->phone) {
            if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/", $this->phone)) {
                $this->addError('phone', Yii::t('advertisement', 'Phone numbers must be written in the format', ['format' => '0XX-XXX-XX-XX']));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function getUserDropdown() {

        $query = ArrayHelper::map(static::find()->select(['username', 'id'])->all(), 'id', 'username');

        return $query;
    }



    public function getCompanyUser()
    {
        return $this->hasOne(CompanyUser::className(), ['user_id' => 'id']);
    }

    public function beforeValidate()
    {
        if ($this->scenario == self::SCENARIO_CREATE_COMPANY_USER && $this->isNewRecord) {
            if ($this->email) {
                $this->username = $this->email;
            }
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function getFullname()
    {
        $fullname = $this->surname ? $this->surname : '';

        if ($this->name) {
            $fullname .= ($fullname ? ' ' : '') . $this->name;
        }

        return $fullname;
    }

    public function getElectedAdvertisements()
    {
        return $this->hasMany(Advertisement::className(), ['id' => 'record_id'])
            ->viaTable(Elected::tableName(), ['user_id' => 'id'],
                function ($query){
                    $query->onCondition(['table_name' => Elected::ALIAS_ADVERTISEMENT]);
                });
    }

    public function getElectedCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'record_id'])
            ->viaTable(Elected::tableName(), ['user_id' => 'id'],
                function ($query){
                    $query->onCondition(['table_name' => Elected::ALIAS_COMPANY]);
                });
    }

    public function getElectedUsers()
    {
        return $this->hasMany(self::className(), ['id' => 'record_id'])
            ->viaTable(Elected::tableName(), ['user_id' => 'id'],
                function ($query){
                    $query->onCondition(['table_name' => Elected::ALIAS_USER]);
                });
    }

    public function updateLastActivityDate()
    {
        $this->last_activity_date = time();
        $this->update(false);
    }

    public static function getAllStatuses()
    {
        return [
            self::STATUS_DELETED => Yii::t('users', 'USER_STATUS_DELETED'),
            self::STATUS_NOT_CONFIRMED => Yii::t('users', 'USER_STATUS_NOT_CONFIRMED'),
            self::STATUS_ACTIVE => Yii::t('users', 'USER_STATUS_ACTIVE'),
            self::STATUS_BANNED => Yii::t('users', 'USER_STATUS_BANNED')
        ];
    }

    public function getDetailStatus()
    {
        foreach (self::getAllStatuses() as $key => $title) {
            if ($this->status == $key) {
                return $title;
            }
        }

        return 'Undefined';
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id'])
            ->via('companyUser');
    }

    public function getResponsible()
    {
        if($this->company) {
            return $this->hasOne(ConsumerResponsible::className(), ['consumer_id' => 'company_id'])
                ->via('companyUser')
                ->andWhere(['type' => ConsumerResponsible::TYPE_COMPANY]);
        } else {
            return $this->hasOne(ConsumerResponsible::className(), ['consumer_id' => 'id'])
                ->andWhere(['type' => ConsumerResponsible::TYPE_USER]);
        }
    }

    public function getSavedAddresses()
    {
        return $this->hasMany(SavedAddress::className(), ['user_id' => 'id']);
    }

    public function getAdvertisements($count = null)
    {
        $query = Advertisement::find()
            ->where(['author_id' => $this->id])
            ->andWhere([Advertisement::tableName().'.status' => Advertisement::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()]);

        if ($count) {
            $query = $query->limit($count);
        }

        return $query->all();
    }

    public function getAdvertisementsRubrics()
    {
        return Rubric::find()
            ->where([
                'in',
                Rubric::tableName().'.id',
                ArrayHelper::getColumn(
                    Advertisement::find()
                        ->where([
                            'author_id' => $this->primaryKey,
                            'status' => Advertisement::STATUS_ACTIVE_YES
                        ])
                        ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
                        ->groupBy('rubric_id')
                        ->all(),
                    'rubric_id'
                )
            ])
            ->andWhere(['active' => Rubric::ACTIVE_YES])
            ->joinWith('lang')
            ->all();
    }


    /**
     * @param string $email_confirm_token
     * @return static|null
     */
    public static function findByEmailConfirmToken($email_confirm_token)
    {
        return static::findOne(['email_confirm_token' => $email_confirm_token, 'status' => self::STATUS_NOT_CONFIRMED]);
    }

    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }

    public function generateEmailChangeToken($email)
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
        $this->new_email = $email;
    }
    public static function findByEmailChangeToken($token)
    {
        if (empty($token)) {
            return false;
        }

        return static::findOne(['email_confirm_token' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    public function SubsidiaryList(){

        return Subsidiary::SubsidiaryListByCompanyId(User::find()->where(['s'])->company());

    }

    public function companyUserSendEmail(){

        $this->generateEmailConfirmToken();
        $this->status = self::STATUS_NOT_CONFIRMED;

        if ($this->save()) {
            Yii::$app->mailer->compose('@frontend/modules/user/mails/emailCompanyUserConfirm', ['user' => $this])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($this->email)
                ->setSubject('Email confirmation for ' . Yii::$app->name)
                ->send();
            return true;
        }

        return false;
    }

    public static function validateNewEmail($email){

        return static::findOne(['email'=>$email]) ? true : false;

    }

    public static function getAllCanAddAdvertisementsProperties()
    {
        return [
            self::CAN_ADD_ADVERTISEMENTS_YES => Yii::t('common', 'Yes'),
            self::CAN_ADD_ADVERTISEMENTS_NO => Yii::t('common', 'No')
        ];
    }

    public function getCanAddAdvertisementsDetail()
    {
        return isset(self::getAllCanAddAdvertisementsProperties()[$this->can_add_advertisements]) ?
            self::getAllCanAddAdvertisementsProperties()[$this->can_add_advertisements] : 'Undefined';
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->status != self::STATUS_ACTIVE) {
            CommentModel::updateAll(['status' => Status::PENDING], ['createdBy' => $this->id]);
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function getRole()
    {
        
    }

    public function beforeDelete()
    {
        Accounting::deleteAll(['user_id' => $this->id]);
        Advertisement::deleteAll(['author_id' => $this->id]);
        CompanyMark::deleteAll(['from_user_id' => $this->id]);
        CompanyUser::deleteAll(['user_id' => $this->id]);
        Complaint::deleteAll(['from_user_id' => $this->id]);
        Contact::deleteAll(['user_id' => $this->id]);
        Dialog::deleteAll(['from_user_id' => $this->id]);
        Dialog::deleteAll(['to_user_id' => $this->id]);
        DialogDetail::deleteAll(['user_id' => $this->id]);
        DialogMessage::deleteAll(['from_user_id' => $this->id]);
        DialogMessage::deleteAll(['to_user_id' => $this->id]);
        Elected::deleteAll(['user_id' => $this->id]);
        PricesDispath::deleteAll(['user_id' => $this->id]);
        Refill::deleteAll(['user_id' => $this->id]);
        SavedAddress::deleteAll(['user_id' => $this->id]);
        ConsumerOblast::deleteAll(['user_id' => $this->id]);

        $prices = Price::find()->where(['author_id' => $this->id])->all();
        foreach ($prices as $item) {
            $item->delete();
        }

        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}