<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 20.02.2017
 * Time: 10:44
 */

namespace frontend\modules\instruments\models;

use yii\base\Model;
use frontend\components\library\SimpleHTMLDom as SHD;
use Yii;

class FuelPricesModel extends Model
{
    public  $regionAliace ;
    public  $content;
    public function getRegions(){
        $content = SHD::file_get_html('http://finance.i.ua/fuel/');

        $regionIds = [];
        foreach ($content->find('#popup-regions_menu li') as $one) {

            $regionIds[$one->children[0]->attr['href']] = Yii::t('common', $one->plaintext);
        }

        return $regionIds;
    }

    public function getTablePrices($regionAliace = '/fuel/'){

        if ($this->getCache('table_price_'.$regionAliace)) {
            $tablePrices = $this->getCache('table_price_'.$regionAliace);
            $this->regionAliace = $regionAliace;
        } else {
            $content = SHD::file_get_html('http://finance.i.ua'.$regionAliace);

            $this->content = $content;
            $this->regionAliace = $regionAliace;
            $tablePrices = [];

            foreach ($content->find('.data_container tbody tr') as $one) {
                $tablePrices[] = [
                    'title' => $one->children[0]->plaintext,
                    'a-92' => $one->children[1]->plaintext,
                    'a-95' => $one->children[2]->plaintext,
                    'disel' => $one->children[3]->plaintext,
                ];
            }

            $this->setCache('table_price_'.$regionAliace,$tablePrices);
        }

        return  $tablePrices ;
    }

    public function graphData(){

        if ($this->getCache('get_chart_data'.$this->regionAliace)) {
            return $this->getCache('get_chart_data'.$this->regionAliace);
        } else {
            $content = $this->content;
            $tablePricesByFuel = [];

            foreach ($content->find('table tr') as $one) {
                if(!in_array($one->children[0]->plaintext,['Заправка ','Средняя','Максимальная','Минимальная'])){
                    $tablePricesByFuel['a_92'][] = [
                        'title' => Yii::t('common', $one->children[0]->plaintext),
                        'value' => (float) $one->children[1]->plaintext,
                        'color' => '#7fc556'
                    ];

                    $tablePricesByFuel['a_95'][] = [
                        'title' => Yii::t('common', $one->children[0]->plaintext),
                        'value' => (float) $one->children[2]->plaintext,
                        'color' => '#7fc556'
                    ];

                    $tablePricesByFuel['disel'][] = [
                        'title' => Yii::t('common', $one->children[0]->plaintext),
                        'value' => (float) $one->children[3]->plaintext,
                        'color' => '#7fc556'
                    ];

                }
            };

            $this->setCache('get_chart_data'.$this->regionAliace,$tablePricesByFuel);

            return $tablePricesByFuel;
        }
    }

    public function setCache($key, $data)
    {
        return Yii::$app->cache->set($key.'_'.Yii::$app->language,$data,60*60*12);
    }

    public function getCache($key)
    {
        return  Yii::$app->cache->get($key.'_'.Yii::$app->language);
    }
}