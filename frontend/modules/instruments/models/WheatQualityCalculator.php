<?php

namespace frontend\modules\instruments\models;

use yii\base\Model;
use Yii;

class WheatQualityCalculator extends Model
{
    /**
     * @inheritdoc
     */
    public $natura;
    public $stekl;
    public $vlaga;
    public $zernov;
    public $sor;
    public $belok;
    public $kley;
    public $idk;
    public $paden;

    public function rules()
    {
        return [
            [['natura', 'stekl', 'vlaga', 'zernov', 'sor', 'belok', 'kley', 'idk', 'paden'],  'default', 'value' => 0],
            [['natura', 'stekl', 'vlaga', 'zernov', 'sor', 'belok', 'kley', 'idk', 'paden'], 'number', 'min' => 0]
        ];
    }

    public function attributeLabels()
    {
        return [
            'natura' => Yii::t('instruments', 'Натура'),
            'stekl' => Yii::t('instruments', 'Стекловидность'),
            'vlaga' => Yii::t('instruments', 'Влага'),
            'zernov' => Yii::t('instruments', 'Зерновая'),
            'sor' => Yii::t('instruments', 'Сор'),
            'belok' => Yii::t('instruments', 'Белок'),
            'kley' => Yii::t('instruments', 'Клейковина'),
            'idk' => Yii::t('instruments', 'ИДК'),
            'paden' => Yii::t('instruments', 'Число падения'),
        ];
    }

    public function getResult()
    {
        if (!$this->validate()) {
            return $this;
        }

        $natura = $this->calculateNaturaParam($this->natura);
        $stekl = $this->calculateSteklParam($this->stekl);
        $vlaga = $this->calculateVlagaParam($this->vlaga);
        $zernov = $this->calculateZernovParam($this->zernov);
        $sor = $this->calculateSorParam($this->sor);
        $belok = $this->calculateBelokParam($this->belok);
        $kley = $this->calculateKleyParam($this->kley);
        $idk = $this->calculateIdkParam($this->idk);
        $paden = $this->calculatePadenParam($this->paden);

        $result = max([$natura, $stekl, $vlaga, $zernov, $sor, $belok, $kley, $idk, $paden]);
       
        switch ($result) {
            case 1:
                return Yii::t('instruments', 'пшеница 1-го кл.');
            case 2:
                return Yii::t('instruments', 'пшеница 2-го кл.');
            case 3:
                return Yii::t('instruments', 'пшеница 3-го кл.');
            case 4:
                return Yii::t('instruments', 'пшеница 4-го кл.');
            case 5:
                return Yii::t('instruments', 'пшеница 5-го кл.');
            case 6:
                return Yii::t('instruments', 'фуражная пшеница');
            default:
                return Yii::t('instruments', 'внеклассная пшеница');
        }
    }

    private function calculateNaturaParam($value)
    {
        if ($value >= 760) {
            return 1;
        } elseif ($value >= 740) {
            return 2;
        } elseif ($value >= 730) {
            return 3;
        } elseif ($value >= 710) {
            return 4;
        } elseif ($value >= 690) {
            return 5;
        }

        return 6;
    }

    private function calculateSteklParam($value)
    {
        if ($value == 0) {
            return 0;
        } elseif ($value >= 50) {
            return 1;
        } elseif ($value >= 40) {
            return 2;
        }

        return 3;
    }

    private function calculateVlagaParam($value)
    {
        return ($value < 14) ? 1 : 7;
    }

    private function calculateZernovParam($value)
    {
        if ($value <= 5) {
            return 1;
        } elseif ($value <= 8) {
            return 2;
        } elseif ($value <= 8) {
            return 3;
        } elseif ($value <= 10) {
            return 4;
        } elseif ($value <= 12) {
            return 5;
        } elseif ($value <= 15) {
            return 6;
        }

        return 7;
    }

    private function calculateSorParam($value)
    {
        if ($value <= 1) {
            return 1;
        } elseif ($value <= 2) {
            return 2;
        } elseif ($value <= 5) {
            return 6;
        }

        return 0;
    }

    private function calculateBelokParam($value)
    {
        if ($value >= 14) {
            return 1;
        } elseif ($value >= 12.5) {
            return 2;
        } elseif ($value >= 11) {
            return 3;
        }

        return 6;
    }

    private function calculateKleyParam($value)
    {
        if ($value >= 28) {
            return 1;
        } elseif ($value >= 23) {
            return 2;
        } elseif ($value >= 18) {
            return 3;
        }

        return 4;
    }

    private function calculateIdkParam($value)
    {
        return (($value < 45) || ($value > 100)) ? 4 : 1;
    }

    private function calculatePadenParam($value)
    {
        if ($value >= 220) {
            return 1;
        } elseif ($value >= 180) {
            return 2;
        } elseif ($value >= 150) {
            return 3;
        } elseif ($value >= 130) {
            return 5;
        }

        return 6;
    }
}