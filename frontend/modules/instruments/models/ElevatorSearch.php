<?php

namespace frontend\modules\instruments\models;

use backend\modules\commondata\models\City;
use backend\modules\commondata\models\Region;
use backend\modules\elevator\models\ElevatorAccessibility;
use yii\base\Model;
use backend\modules\elevator\models\Elevator;
use backend\modules\elevator\models\ElevatorLang;
use yii\db\Expression;

/**
 * ElevatorSearch represents the model behind the search form about `backend\modules\elevator\models\Elevator`.
 */
class ElevatorSearch extends Elevator
{
    /**
     * @inheritdoc
     */

    public $name;
    public $region;
    public $volume_from;
    public $volume_to;
    public $loading_from;
    public $loading_to;
    public $unloading_from;
    public $unloading_to;
    public $from_location;
    public $type;
    public $laboratory;
    public $accessibility;

    public function rules()
    {
        return [
            [['id', 'property_type', 'volume', 'loading', 'unloading', 'active', 'address_confirmed', 'city_id',
                'volume_from', 'volume_to', 'loading_from', 'loading_to', 'unloading_from', 'unloading_to'], 'integer'],
            [['address', 'name', 'region', 'from_location', 'type', 'laboratory', 'accessibility'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Elevator::find();
        $this->load($params);

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
        ]);



        if ($this->name) {
            $query->joinWith('lang')
                ->andFilterWhere(['like', ElevatorLang::tableName().'.title', '%'.$this->name.'%', false]);
        }

        if ($this->region) {
            $query->innerJoin(City::tableName(), City::tableName().'.id='.Elevator::tableName().'.city_id')
                ->innerJoin(Region::tableName(), Region::tableName().'.id='.City::tableName().'.oblast_id')
                ->andWhere(['in', Region::tableName().'.id', $this->region]);
        }

        $query->andFilterWhere(['active' => Elevator::ACTIVE_YES])->joinWith('lang');

        if ($this->volume_from) {
            $query->andFilterWhere(['>=', 'volume', $this->volume_from]);
        }
        if ($this->volume_to) {
            $query->andFilterWhere(['<=', 'volume', $this->volume_to]);
        }
        if ($this->loading_from) {
            $query->andFilterWhere(['>=', 'loading', $this->loading_from]);
        }
        if ($this->loading_to) {
            $query->andFilterWhere(['<=', 'loading', $this->loading_to]);
        }
        if ($this->unloading_from) {
            $query->andFilterWhere(['>=', 'unloading', $this->unloading_from]);
        }
        if ($this->unloading_to) {
            $query->andFilterWhere(['<=', 'unloading', $this->unloading_to]);
        }
        if ($this->type) {
            $query->andFilterWhere(['IN', 'property_type', $this->type]);
        }
        if ($this->laboratory) {
            $query->andFilterWhere(['IN', 'laboratory', $this->laboratory]);
        }
        if ($this->accessibility) {
            $query->leftJoin(ElevatorAccessibility::tableName(), Elevator::tableName().'.id='.ElevatorAccessibility::tableName().'.elevator_id');
            $query->andFilterWhere(['IN', ElevatorAccessibility::tableName().'.accessibility_id', $this->accessibility]);
        }

        $query->andFilterWhere(['IS NOT', 'coordinates', (new Expression('Null'))])
            ->andFilterWhere(['address_confirmed' => Elevator::ADDRESS_CONFIRMED_YES]);

        return $query;
    }
}