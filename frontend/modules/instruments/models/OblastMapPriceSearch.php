<?php

namespace frontend\modules\instruments\models;

use backend\modules\commondata\models\Region;
use backend\modules\commondata\models\RegionLang;
use backend\modules\rubric\models\Rubric;
use backend\modules\setting\models\Setting;
use common\helpers\MyHelpers;
use Yii;
use yii\base\Model;
use frontend\modules\cabinet\models\Sale;
use common\helpers\GoogleMapsApiHelper;
use backend\modules\commondata\models\Currency;


class OblastMapPriceSearch extends Sale
{

    public $rubrics;
    public $avgPrice;

    public $convert = [
        1 => 2,
        2 => 3,
        3 => 4,
        4 => 5,
        5 => 6,
        6 => 7,
        7 => 8,
        8 => 9,
        9 => 10,
        10 => 12,
        11 => 14,
        12 => 15,
        13 => 16,
        14 => 17,
        15 => 18,
        16 => 19,
        17 => 20,
        18 => 21,
        19 => 23,
        20 => 25,
        21 => 26,
        22 => 24,
        23 => 13,
        24 => 22,
        25 => 1,

    ];

    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['regions', 'rubrics', 'from_date', 'to_date'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $type = NULL)
    {
        $this->load($params);
        $main_currency = Currency::getMainCurrency()->weight;

            $date = new \DateTime('-1 month');
            $from_date = $date->format('Y-m-d');
            $to_date = date('Y-m-d');


        if($this->rubrics) {
            $setting = Setting::find()->where(['alias' => 'company-list'])->one();
            $company = MyHelpers::multiexplode($setting->value);

            $models = self::find()
                ->select(Sale::tableName().'.*,   MIN(price) AS minPrice, IF(`currency_id`=1, `price`, `price` * '.$main_currency.') as price, MAX(price) AS maxPrice, AVG(price) AS avgPrice')
                ->innerJoin('rubric_model', 'sale.rubric_id = rubric_model.id')
                ->andWhere(['show_map' => Rubric::SHOW_MAP_YES])
                ->andFilterWhere(['IN', 'rubric_id', $this->rubrics])
                ->andWhere('date >= :from_date', [':from_date' => $from_date])
                ->andWhere('date <= :to_date', [':to_date' => $to_date])
                ->andWhere(['in', 'company_id', $company])
                ->andWhere(['type' => $type])
                ->groupBy('oblast_id')
                ->all();
        } else {
            $models = [];
        }




        return $this->formate($models);
    }

    private function formate($models )
    {
        $items = [];

        for($i = 0; $i < count($this->convert); $i++) {
            $oblast = Region::find()->where(['id' => $i])->one();

            $items[$this->convert[$i]] = [
                'title' => $oblast->lang->title,
            ];
        }

        if($models) {

            foreach ($models as $item) {
                $items[$this->convert[$item->oblast_id]] = [
                    'title' => $item->oblast->title,
                    'value' => round($item->avgPrice, 2)
                ];
            }
        }

        return json_encode($items);
    }

}