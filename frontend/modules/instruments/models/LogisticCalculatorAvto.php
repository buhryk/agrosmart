<?php

namespace frontend\modules\instruments\models;

use common\helpers\GoogleMapsApiHelper;
use yii\base\Model;
use Yii;

class LogisticCalculatorAvto extends Model
{
    /**
     * @inheritdoc
     */
    public $from_location;
    public $to_location;

    public function rules()
    {
        return [
            [['from_location', 'to_location'], 'required'],
            [['from_location', 'to_location'], 'string', 'min' => 1, 'max' => 255],
            [['from_location', 'to_location'], 'validateLocation']
        ];
    }

    public function validateLocation($attribute, $params)
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();

        if ($this->$attribute) {
            $coordinates = $googleMapsApiHelper->getLocationByAddress($this->$attribute);
            if (!$coordinates) {
                $this->addError($attribute, Yii::t('instruments', 'Wrong location', ['field' => $this->getAttributeLabel($attribute)]));
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'from_location' => Yii::t('instruments', 'Отправление из'),
            'to_location' => Yii::t('instruments', 'Прибытие в'),
        ];
    }

    public function getLocations(GoogleMapsApiHelper $googleMapsApiHelper)
    {
        if (!$this->validate()) {
            return $this;
        }

        return [
            $googleMapsApiHelper->getLocationByAddress($this->from_location),
            $googleMapsApiHelper->getLocationByAddress($this->to_location)
        ];
    }
}