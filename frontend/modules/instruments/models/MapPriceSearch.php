<?php

namespace frontend\modules\instruments\models;

use backend\modules\rubric\models\Rubric;
use frontend\components\tool\Image;
use Yii;
use yii\base\Model;
use frontend\modules\cabinet\models\Sale;
use common\helpers\GoogleMapsApiHelper;
use yii\helpers\Url;

class MapPriceSearch extends Sale
{
    public $regions;
    public $rubrics;
    public $maxPrice;
    public $minPrice;
    public $avgPrice;
    public $from_date;
    public $to_date;
    public $point_location;
    public $result = [];
    public $radius;



    public function rules()
    {
        return [
            [['id', 'company_id','city_id', 'radius'], 'integer'],
            [['regions', 'rubrics', 'from_date', 'to_date', 'point_location'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $type = NULL)
    {
        $this->type = $type;
        $this->load($params);

        $date = new \DateTime('-1 month');
        $this->from_date = $date->format('Y-m-d');
        $this->to_date = date('Y-m-d');
   
        $priceList = self::find()
            ->select(Sale::tableName().'.*, CONCAT(`rubric_id`, `city_id`, `address`) as items')
            ->innerJoin('rubric_model', 'sale.rubric_id = rubric_model.id')
            ->andWhere(['show_map' => Rubric::SHOW_MAP_YES])
            ->andWhere(['type' => $type])
            ->andFilterWhere(['IN', 'oblast_id', $this->regions])
            ->andFilterWhere(['IN', 'rubric_id', $this->rubrics])
            ->andFilterWhere(['city_id' => $this->city_id])
            ->andWhere('date >= :from_date', [':from_date' => $this->from_date])
            ->andWhere('date <= :to_date', [':to_date' => $this->to_date])
            ->orderBy('updated_at DESC')
            ->groupBy('items')
            ->all();

        $this->items($priceList);
        $mapData = [];
        foreach ($this->result as $item){
            $mapData[] =$item;
        }
        return $mapData;
        
    }

    public function getCoordinat()
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();
        return json_encode($googleMapsApiHelper->getLocationByAddress($this->point_location));
    }

    public function items($list)
    {
        foreach ($list AS $item) {
            $this->constructionMap($item);
        }
    }

    public function constructionMap($item){
        if($item->coordinates) {
            if (empty($this->result[$item->location])) {
                $this->result[$item->location] = [
                    'address' => $item->location,
                    'coordinat' => explode(',', $item->coordinates),
                    'marker' => $this->getMarker($item),
                    'distanceTranslates' => Yii::t('common', 'Расстояние'),
                ];
            }
            $this->result[$item->location]['marker'] .= "<p>" . $item->rubric->title . " : " . ceil($item->price) . " грн</p>";
        }
    }

    private function getMarker($item)
    {
        $logo = $item->company->logotype ? Image::resize('/'.$item->company->logotype, 70, 50) : '/images/user_274x185.jpg';

        $type = $item->type == Sale::TYPE_PRODAZHA ? 'prodazha' : 'zakupka';

        $marker = '<img style="    display: inline-block; vertical-align: middle; width:70px; " src ="'.$logo.'"> ';
        $marker.= '<h4 style="display: inline;"><a href="'.Url::to(['/company/price/'.$type.'-info', 'company_id' => $item->company_id]).'">' . $item->company->name . '</a></h4>';
        $marker.= '<p>Адреса : ' . $item->location . ' </p>';
        return $marker;
    }


}