<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 20.02.2017
 * Time: 10:44
 */

namespace frontend\modules\instruments\models;

use yii\base\Model;
use frontend\components\library\SimpleHTMLDom as SHD;
use Yii;
use backend\modules\instrument\models\Instrument;

class InstrumentGet
{
    const STATUS_ACCESS = '1';
    const STATUS_NO_ACCCES = '0';
    
    public $instrument_id;

    public function __construct($instrument_id)
    {
        $this->instrument_id = $instrument_id;
    }

    public function searchOne($company_type = NULL)
    {
        $query = Instrument::find()->andWhere(['id' => $this->instrument_id]);

        
        if($package = Yii::$app->userRole->package) {
            $query->leftJoin('package_instrument', 'instrument.id = package_instrument.instrument_id');
            $query->andWhere(['package_id' => $package->id]);
        } else {
            $query->innerJoin('instrument_accessibility', 'instrument.id = instrument_accessibility.instrument_id');
            $query->andWhere(['in', 'instrument_accessibility.type_id', Yii::$app->userRole->role]);
        }

        $model = $query->one();

        if (!$model) {
            $model = Instrument::findOne($this->instrument_id);
            $status = self::STATUS_NO_ACCCES;
        } else {
            $status = self::STATUS_ACCESS;
        }

        return ['model' =>$model, 'status' =>$status];
    }

    public static function instrumentShow($instrument_id)
    {
        $query = Instrument::find()->andWhere(['id' => $instrument_id]);


        if($package = Yii::$app->userRole->package) {
            $query->leftJoin('package_instrument', 'instrument.id = package_instrument.instrument_id');
            $query->andWhere(['package_id' => $package->id]);
        } else {
            $query->innerJoin('instrument_visibility', 'instrument.id = instrument_visibility.instrument_id');
            $query->andWhere(['in', 'instrument_visibility.type_id', Yii::$app->userRole->role]);
        }

        $model = $query->one();
        if ($model) {
            return true;
        }

    }
}