<?php

namespace frontend\modules\instruments\models;

use Yii;
use yii\base\Model;
use frontend\modules\cabinet\models\Sale;
use backend\modules\rubric\models\Rubric;
use backend\modules\commondata\models\Currency;

class PriceSearch extends Sale
{
    public $from_date;
    public $to_date;
    public $regions;
    public $rubrics;
    public $maxPrice;
    public $minPrice;
    public $avgPrice;
    public $result = [];

    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            [['regions', 'rubrics', 'from_date', 'to_date'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $type = NULL)
    {
        $this->type = $type;
        $this->load($params);

        if (!$this->from_date) {
            $date = new \DateTime('-1 month');
            $this->from_date = $date->format('d/m/Y');
        }

        if (!$this->to_date) {
            $this->to_date = date('d/m/Y');
        }
        $rubricList = Rubric::find()
            ->andWhere(['parent_id' => Rubric::TRAIDER_RUBRIC_ID])
            ->andFilterWhere(['IN', 'id', $this->rubrics])
            ->all();

        $priceListChart = [];
        $priceListTable = [];
        $priceListChartRubric = [];
        $main_currency = Currency::getMainCurrency()->weight;

        foreach($rubricList as $item) {
            $query = self::find()
                ->select(Sale::tableName().'.*,   MIN(price) AS minPrice,  price, MAX(price) AS maxPrice, AVG(price) AS avgPrice')
                ->andWhere(['type' => $type])
                ->andFilterWhere(['IN', 'oblast_id', $this->regions])
                ->andWhere(['rubric_id' => $item->id])
                ->andWhere(['currency_id' => 1])
                ->andWhere('date >= :from_date', [':from_date' => \DateTime::createFromFormat('d/m/Y', $this->from_date)->format('Y-m-d')])
                ->andWhere('date <= :to_date', [':to_date' => \DateTime::createFromFormat('d/m/Y', $this->to_date)->format('Y-m-d')]);
            
            $chare = clone  $query;

            $this->items($chare->groupBy('date')->all());
            $tableItem = $query->groupBy('rubric_id')->one();
            $priceListTable[$item->id] = ['price' => $this->itemsFormation($tableItem), 'title' => $item->title];
            if($tableItem){
                $priceListChartRubric[] =  $this->chartSetting($tableItem);
            }
        }

        foreach ($this->result as $item){
            $priceListChart[] = $item;
        }

        return ['priceListTable' => $priceListTable, 'priceListChart' => $priceListChart, 'priceListChartRubric' => $priceListChartRubric];
    }

    public function chartSetting($item)
    {
        if(isset($item)){
            return [
                'balloonText' => '[[title]]   [[category]] <br> Цена :[[value]] ',
                'bullet' => 'round',
                'title' =>  $item->rubric->title,
                'valueField' =>  'rubric_'.$item->rubric_id,
                'fillAlphas'=> 0,

            ];
        }
    }

    public function items($items)
    {
        if($items){
            foreach ($items as $item) {
                $this->constructionChart($item);
            }
        }
    }

    public function itemsFormation($item)
    {
        if(isset($item)){
            $result = [
                'date' => $item->date,
                'price' => $item->price,
                'minPrice' => $item->minPrice,
                'maxPrice' => $item->maxPrice,
                'avgPrice' => round($item->avgPrice,2),
                'changePrice' => $item->maxPrice - $item->minPrice,
                'changePercent' => round($item->maxPrice/$item->minPrice * 100-100, 2),
            ];
            return $result;
        }
    }

    private function constructionChart($item){
       if(empty($this->result[$item->date])){
           $this->result[$item->date] =['date' => $item->date];
       }
        $this->result[$item->date] = array_merge($this->result[$item->date], [
            'rubric_'.$item->rubric_id => round($item->avgPrice)
        ]);
    }
}
