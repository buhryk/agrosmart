<?php
use yii\helpers\Url;
use yii\bootstrap\Modal;

?>

<div class="container instruments">
    <div class="owl-instruments text-center">
        <?php foreach ($models as $model): ?>
            <div class="item">
                <a href="<?= Url::to([$model->url]); ?>">
                    <img src="<?=$model->image ?>">
                    <h4><?=$model->title ?></h4>
                    <?=$model->description ?>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php if($modal): ?>
  <?php  Modal::begin([
        'header' => '<h2>'.Yii::t('page', 'Здравствуйте').'</h2>',
        'toggleButton' => [
            'tag' => 'button',
            'class' => 'btn btn-lg btn-block btn-info modale-guest-user',
            'label' => '',
            'style' => 'display:none'
        ]
    ]);
    ?>
        <?=$role->page->short_description?>
    <?php Modal::end();?>

    <?php
    $script = "
       setTimeout(function(){
          $('.modale-guest-user').trigger('click');
       },2500);
    ";
    $this->registerJs($script, \yii\web\View::POS_READY);

    ?>
<?php endif; ?>