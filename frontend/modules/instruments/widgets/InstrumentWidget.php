<?php

namespace frontend\modules\instruments\widgets;


use backend\modules\commondata\models\SiteRole;
use yii\bootstrap\Widget;
use Yii;
use backend\modules\instrument\models\Instrument;

class InstrumentWidget extends Widget
{
    public $model_id;
    public function init()
    {
        
    }

    public function run()
    {
        $role = SiteRole::find()
            ->where(['page_id' => $this->model_id])
            ->andWhere(['authorized' => SiteRole::AUTHORIZED_NOT])
            ->one();
        $currentRole = Yii::$app->userRole->role;
        $modal = 0;

        if(!in_array($role->id, $currentRole) && Yii::$app->user->isGuest) {
            Yii::$app->userRole->setUserRole($role->id);
            $modal = 1;
        }
        
        $models = Instrument::find()
            ->innerJoin('instrument_visibility', 'instrument.id = instrument_visibility.instrument_id')
            ->where(['in', 'instrument_visibility.type_id', [$role->id , 7, 9]])
            ->andWhere(['type' => Instrument::TYPE_INSTRUMENT])
            ->andWhere('parent_id IS NULL')
            ->orderBy('position')
            ->all();

        
        return $this->render('instrument-widget', ['models' => $models, 'role' => $role, 'modal' => $modal]);
    }
}