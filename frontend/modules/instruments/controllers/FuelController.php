<?php

namespace frontend\modules\instruments\controllers;

use frontend\modules\instruments\models\FuelPricesModel;
use frontend\modules\instruments\models\InstrumentGet;
use yii\web\Controller;
use Yii;

class FuelController extends Controller
{
    public function actionPrices($regionAliace = '/fuel/12/')
    {
        $instrumentGet = new InstrumentGet(11);
        $instrument = $instrumentGet->searchOne(\Yii::$app->request->get('company_type'));

        if ($instrument['status'] == InstrumentGet::STATUS_NO_ACCCES) {
            return $this->redirect(['/instruments/instrument/no-access', 'id' => $instrumentGet->instrument_id ]);
        }

        $model = new FuelPricesModel();

        $regions = $model->getRegions();
        $content = $model->getTablePrices($regionAliace);
        $chartData = $model->graphData();


        return $this->render('index',[
            'model' => $model,
            'regions' => $regions,
            'fuelTable' => $content,
            'chartData' => $chartData
        ]);
    }
}