<?php

namespace frontend\modules\instruments\controllers;

use backend\modules\instrument\models\Instrument;
use yii\web\Controller;

class AnalyticsController extends Controller
{
    public function actionIndex()
    {
        $models = Instrument::find()
            ->innerJoin('instrument_visibility', 'instrument.id = instrument_visibility.instrument_id')
            ->where(['in', 'instrument_visibility.type_id', Instrument::getCurrentUserType()])
            ->andWhere(['type' => Instrument::TYPE_ANALYTIC])
            ->orderBy('position')
            ->all();

        return $this->render('index', ['models' => $models]);
        
       
    }
}