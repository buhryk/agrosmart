<?php

namespace frontend\modules\instruments\controllers;

use backend\modules\page\models\Page;
use backend\modules\page\models\Category as PageCategory;
use yii\web\Controller;
use backend\modules\instrument\models\Instrument;

class DefaultController extends Controller
{

    public function actionIndex()
    {
        $models = Instrument::find()
            ->innerJoin('instrument_visibility', 'instrument.id = instrument_visibility.instrument_id')
            ->where(['in', 'instrument_visibility.type_id', \Yii::$app->userRole->role])
            ->andWhere(['type' => Instrument::TYPE_INSTRUMENT])
            ->andWhere('parent_id IS NULL')
            ->orderBy('position')
            ->all();

        return $this->render('index', ['models' => $models]);
    }
}