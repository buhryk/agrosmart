<?php

namespace frontend\modules\instruments\controllers;

use frontend\modules\instruments\models\InstrumentGet;
use frontend\modules\instruments\models\WheatQualityCalculator;
use yii\web\Controller;
use Yii;
use backend\modules\instrument\models\Instrument;

class QualityCalculationController extends Controller
{
    public function actionIndex()
    {
        $instrumentGet = new InstrumentGet(9);
        $instrument = $instrumentGet->searchOne(\Yii::$app->request->get('company_type'));

        if ($instrument['status'] == InstrumentGet::STATUS_NO_ACCCES) {
            return $this->redirect(['/instruments/instrument/no-access', 'id' => $instrumentGet->instrument_id ]);
        }

        $models = Instrument::find()
            ->innerJoin('instrument_visibility', 'instrument.id = instrument_visibility.instrument_id')
            ->where(['in', 'instrument_visibility.type_id', Instrument::getCurrentUserType()])
            ->andWhere(['type' => Instrument::TYPE_INSTRUMENT])
            ->andWhere(['parent_id' => 9 ])
            ->orderBy('position')
            ->all();


        return $this->render('index', ['models' => $models]);
    }

    public function actionPshenitsa()
    {
        $instrumentGet = new InstrumentGet(13);
        $instrument = $instrumentGet->searchOne(\Yii::$app->request->get('company_type'));

        if ($instrument['status'] == InstrumentGet::STATUS_NO_ACCCES) {
            return $this->redirect(['/instruments/instrument/no-access', 'id' => $instrumentGet->instrument_id ]);
        }

        $model = new WheatQualityCalculator();
        $result = null;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $result = $model->getResult();
        }

        return $this->render('pshenitsa', [
            'model' => $model,
            'result' => $result
        ]);
    }
}