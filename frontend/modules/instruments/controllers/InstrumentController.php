<?php

namespace frontend\modules\instruments\controllers;


use yii\web\Controller;
use backend\modules\instrument\models\Instrument;
use yii\web\NotFoundHttpException;

class InstrumentController extends Controller
{
    public function actionIndex()
    {

    }
    public function actionNoAccess($id)
    {
        if(!$model = Instrument::findOne($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('no-access', ['model' => $model]);
    }

}