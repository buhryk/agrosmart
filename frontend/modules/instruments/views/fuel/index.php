<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 17.02.2017
 * Time: 12:13
 */
use \kartik\form\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\Breadcrumbs;


$pageH1 = Yii::t('instruments', 'FUEL PRICE');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);


$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Analytics'), 'url' => ['analytics/index']];
$this->params['breadcrumbs'][] = $pageH1;

\frontend\assets\FuelAsset::register($this);

?>



<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dstu">
    <div class="row">
        <div class="col-sm-9 mt45">
            <?php
            $form = ActiveForm::begin([
                    'id' => 'fuel_region'
            ]);
            $params = [
                'prompt' => Yii::t('instrument', 'Выберите регион...'),
            ];
            ?>
            <div class="form-group">
                <?= $form->field($model, 'regionAliace',
                    [ 'template' =>
                        '<div class="row">
                            <div class="col-sm-5">
                            {label}
                           </div>
                           <div class="col-sm-4">
                            {input}{error}
                           </div>
                     </div>']
                )->dropDownList($regions,$params,['onchange'=>'test();'])->label(Yii::t('instruments',"PRICE ON FUEL"));?>
            </div>
            <?php
            ActiveForm::end();
            ?>
<!--"Цены на топливо на АЗС Украины в"-->

            <table class="table">

                <?php foreach ($fuelTable as $k=>$item ): ?>

                    <?php if($k == 0) : echo "<thead class=\"thead-inverse\">"?>
                        <tr>
                            <th><?= $item['title'] ?></th>
                            <th><?= $item['a-92'] ?></th>
                            <th><?= $item['a-95'] ?></th>
                            <th><?= Yii::t('common', $item['disel']) ?></th>
                        </tr>
                        </thead>
                    <?php endif;?>
                <?php endforeach; ?>
                <?php
                array_shift( $fuelTable);

                $newTable =  array_reverse($fuelTable);
                ?>
                <?php foreach ($newTable as $k=>$item ): ?>
                    <?php if ($k == 1) echo "<tbody>"?>

                    <?php if(in_array($item['title'],['Средняя','Максимальная','Минимальная'])) : ?>
                        <tr style="background-color: #7fc556">
                            <th><?= Yii::t('common', $item['title']) ?></th>
                            <th><?= $item['a-92'] ?></th>
                            <th><?= $item['a-95'] ?></th>
                            <th><?= Yii::t('common', $item['disel']) ?></th>
                        </tr>


                    <?php  elseif ($k >= 1) : ?>
                        <tr >
                            <td><?= Yii::t('common', $item['title']) ?></td>
                            <td><?= $item['a-92'] ?></td>
                            <td><?= $item['a-95'] ?></td>
                            <td><?= $item['disel'] ?></td>

                        </tr>
                    <?php endif; ?>

                    <?php if ($k == 1)  echo "<tbody>"?>

                <?php endforeach; ?>
            </table>
            <h3 style="text-align: center"> A - 92</h3>
            <div id="chartdiv_92"></div>
            <h3 style="text-align: center">A - 95</h3>
            <div id="chartdiv_95"></div>
            <h3 style="text-align: center">Дизель</h3>
            <div id="chartdiv_diesel"></div>


        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</div>
    <style>
        #chartdiv_92,#chartdiv_95,#chartdiv_diesel {
            width: 100%;
            height: 500px;
        }

        .amcharts-export-menu-top-right {
            top: 10px;
            right: 0;
        }
    </style>
<?php
$this->registerJs('
        $("#fuelpricesmodel-regionaliace").on("change", function (){
            var region = $("#fuelpricesmodel-regionaliace")[0].value;
            if(region == "") region = "/fuel/12/";
            var action = $("#fuel_region")[0].action;
            $("#fuel_region")[0].action = "/ru/instruments/fuel/prices?regionAliace="+region
            console.log($("#fuel_region")[0].action);
            $("#fuel_region").submit();
            
        });
    ');
$this->registerJs('
    var chart_one = AmCharts.makeChart("chartdiv_92", {
          "type": "serial",
          "theme": "none",
          "marginRight": 70,
          "dataProvider": '.json_encode($chartData["a_92"]).',
          "valueAxes": [{
            "axisAlpha": 0,
            "position": "left",
            "title": "'.Yii::t('instrument', 'Цена в грн.').'"
          }],
          "startDuration": 1,
          "graphs": [{
            "balloonText": "<b>[[category]]: [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "value"
          }],   
          "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
          },
          "categoryField": "title",
          "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 45
          },
      }); 
      var chart_two = AmCharts.makeChart("chartdiv_95", {
          "type": "serial",
          "theme": "none",
          "marginRight": 70,
          "dataProvider": '.json_encode($chartData["a_95"]).',
          "valueAxes": [{
            "axisAlpha": 0,
            "position": "left",
            "title": "'.Yii::t('instrument', 'Цена в грн.').'"
          }],
          "startDuration": 1,
          "graphs": [{
            "balloonText": "<b>[[category]]: [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "value"
          }],   
          "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
          },
          "categoryField": "title",
          "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 45
          },
      }); 
      var chart_disel = AmCharts.makeChart("chartdiv_diesel", {
          "type": "serial",
          "theme": "none",
          "marginRight": 70,
          "dataProvider": '.json_encode($chartData["disel"]).',
          "valueAxes": [{
            "axisAlpha": 0,
            "position": "left",
            "title": "'.Yii::t('instrument', 'Цена в грн.').'"
          }],
          "startDuration": 1,
          "graphs": [{
            "balloonText": "<b>[[category]]: [[value]]</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "value"
          }],   
          "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
          },
          "categoryField": "title",
          "categoryAxis": {
            "gridPosition": "start",
            "labelRotation": 45
          },
    }); 
');
?>