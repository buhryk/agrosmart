<?php
use backend\modules\page\models\Category;
use backend\modules\page\models\Page;
use yii\helpers\Url;


$pageH1 = Yii::t('instruments', 'Инструменты');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['/instruments'])]);
$this->params['bredcrumbs'] = $pageH1;
?>
<div class="container">
    <div class="row">
        <div class="col-sm-9 logistic-calc" data-content="page_content">
            <?php if(is_array($models)): ?>
                <?php foreach ($models as $model) :?>
                    <div class="row">
                        <a href="<?=Url::to([$model->url]) ?>" class="logistic-calc-item clearfix">
                            <div class="col-sm-2 col-xs-4">
                                <img  src="<?=$model->image ?>" alt="map">
                            </div>
                            <div class="col-sm-4 col-xs-8">
                                <p class="logistic-calc-item__title"><?=$model->title ?>
                                </p>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <?=$model->description ?>
                            </div>
                        </a>
                    </div>
                <?php endforeach;  ?>
            <?php endif; ?>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>

            <?= \frontend\modules\news\widgets\LastNewsWidget::widget(['options' => ['count' => 5]]); ?>
        </div>
    </div>
</div>