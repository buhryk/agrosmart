<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$pageH1 = Yii::t('instruments', 'Calculation of quality of grain');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Instruments'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $pageH1;
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9 logistic-calc" data-content="page_content">
            <?php if(is_array($models)): ?>
                <?php foreach ($models as $model) :?>
                    <div class="row">
                        <a href="<?=Url::to([$model->url]) ?>" class="logistic-calc-item clearfix">
                            <div class="col-sm-2 col-xs-4">
                                <img style="height: 150px" src="<?=$model->image ?>" alt="map">
                            </div>
                            <div class="col-sm-4 col-xs-8">
                                <p class="logistic-calc-item__title"><?=$model->title ?>
                                </p>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <?=$model->description ?>
                            </div>
                        </a>
                    </div>
                <?php endforeach;  ?>
            <?php endif; ?>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</div>