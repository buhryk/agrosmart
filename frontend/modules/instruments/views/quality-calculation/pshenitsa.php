<?php
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\Html;


$pageH1 = Yii::t('instruments', 'Calculation of quality wheat');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Instruments'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Calculation of quality of grain'), 'url' => ['quality-calculation/index']];
$this->params['breadcrumbs'][] = $pageH1;

/* @var $this yii\web\View */
/* @var $model frontend\modules\instruments\models\WheatQualityCalculator */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9 wheat-quality" data-content="page_content">
            <div class="col-xs-12 important-info">
                <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                <div class="important-info-content">
                    <p class="important-info__title"><?= Yii::t('instruments', 'Используются данные из ДСТУ на зерновые культуры'); ?></p>
                </div>
            </div>
            <?php $form = ActiveForm::begin(['options' => ['class' => 'wheat-quality-form']]); ?>
            <div class="col-xs-12 about-container">
                <div class="page-form">
                    <div class="row wheat-quality-form-item">
                        <div class="col-sm-6">
                            <p class="wheat-quality-form__title"><?= $model->getAttributeLabel('natura'); ?>, г/л</p>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'natura')->label(false)->textInput([
                                'class' => 'wheat-quality-form__input placeholder',
                                'type' => 'number',
                                'step' => '0.1',
                            ]); ?>
                        </div>
                    </div>
                    <div class="row wheat-quality-form-item">
                        <div class="col-sm-6">
                            <p class="wheat-quality-form__title"><?= $model->getAttributeLabel('stekl'); ?>, %</p>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'stekl')->label(false)->textInput([
                                'class' => 'wheat-quality-form__input placeholder',
                                'type' => 'number',
                                'step' => '0.1',
                            ]); ?>
                        </div>
                    </div>
                    <div class="row wheat-quality-form-item">
                        <div class="col-sm-6">
                            <p class="wheat-quality-form__title"><?= $model->getAttributeLabel('vlaga'); ?>, %</p>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'vlaga')->label(false)->textInput([
                                'class' => 'wheat-quality-form__input placeholder',
                                'type' => 'number',
                                'step' => '0.1',
                            ]); ?>
                        </div>
                    </div>
                    <div class="row wheat-quality-form-item">
                        <div class="col-sm-6">
                            <p class="wheat-quality-form__title"><?= $model->getAttributeLabel('zernov'); ?>, %</p>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'zernov')->label(false)->textInput([
                                'class' => 'wheat-quality-form__input placeholder',
                                'type' => 'number',
                                'step' => '0.1',
                            ]); ?>
                        </div>
                    </div>
                    <div class="row wheat-quality-form-item">
                        <div class="col-sm-6">
                            <p class="wheat-quality-form__title"><?= $model->getAttributeLabel('sor'); ?>, %</p>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'sor')->label(false)->textInput([
                                'class' => 'wheat-quality-form__input placeholder',
                                'type' => 'number',
                                'step' => '0.1',
                            ]); ?>
                        </div>
                    </div>
                    <div class="row wheat-quality-form-item">
                        <div class="col-sm-6">
                            <p class="wheat-quality-form__title"><?= $model->getAttributeLabel('belok'); ?>, %</p>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'belok')->label(false)->textInput([
                                'class' => 'wheat-quality-form__input placeholder',
                                'type' => 'number',
                                'step' => '0.1',
                            ]); ?>
                        </div>
                    </div>
                    <div class="row wheat-quality-form-item">
                        <div class="col-sm-6">
                            <p class="wheat-quality-form__title"><?= $model->getAttributeLabel('kley'); ?>, %</p>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'kley')->label(false)->textInput([
                                'class' => 'wheat-quality-form__input placeholder',
                                'type' => 'number'
                            ]); ?>
                        </div>
                    </div>
                    <div class="row wheat-quality-form-item">
                        <div class="col-sm-6">
                            <p class="wheat-quality-form__title"><?= $model->getAttributeLabel('idk'); ?></p>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'idk')->label(false)->textInput([
                                'class' => 'wheat-quality-form__input placeholder',
                                'type' => 'number',
                                'step' => '0.1',
                            ]); ?>
                        </div>
                    </div>
                    <div class="row wheat-quality-form-item">
                        <div class="col-sm-6">
                            <p class="wheat-quality-form__title"><?= $model->getAttributeLabel('paden'); ?></p>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'paden')->label(false)->textInput([
                                'class' => 'wheat-quality-form__input placeholder',
                                'type' => 'number',
                                'step' => '0.1',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= Html::button(Yii::t('common', 'Reset'), ['class' => 'btn-green btn-center btn-reset', 'onclick' => "this.form.reset();"]) ?>
            <?= Html::submitButton(Yii::t('common', 'Calculate'), ['class' => 'btn-green btn-center']) ?>
            <?php ActiveForm::end(); ?>
            <?php if (isset($result) && $result) { ?>
                <div class="col-xs-12 about-container">
                    <p class="wheat-quality__class"><?= $result; ?></p>
                </div>
            <?php } ?>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</div>
<?php

$this->registerJs("$(\"button.btn-reset\").on(\"click\", function () {
  $('.wheat-quality-form-item input').val('');
});");