<?php
use yii\bootstrap\ActiveForm;
use backend\modules\rubric\models\Rubric;
use backend\modules\commondata\models\Region;
use frontend\modules\company\models\CompanyPriceSearch;


?>


    <div class="clearfix"></div>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <h2 class="sidebar-grey__title"><?=Yii::t('common', 'Продукция') ?></h2>
        <div class="panel panel-default">
            <?php $listRubric = Rubric::find()
                ->andWhere(['show_map' => Rubric::SHOW_MAP_YES])
                ->andWhere('parent_id IS NULL')
                ->all(); ?>
            <?php foreach ($listRubric as $item): ?>
                    <div class="panel-heading" role="tab" id="headingOne-<?=$item['id'] ?>">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne-<?=$item['id'] ?>" aria-expanded="true" aria-controls="collapseOne-<?=$item['id'] ?>">
                                <?=$item['title'] ?>
                            </a>
                        </h4>
                    </div>
                    <?php
                    $checked = '';
                    $in = '';
                    $str = '';
                    if(isset($item->subrubricsPriceMap)):
                        foreach ($item->subrubricsPriceMap as $child) {
                            if(!empty($child->subrubricsPriceMap)){
                                $input = '';
                                foreach ($child->subrubricsPriceMap as $child2) {
                                    if (is_array($model->rubrics) && in_array($child2['id'], $model->rubrics)) {
                                        $checked = 'checked';
                                        $in = ' in';
                                    } else {
                                        $checked = '';
                                    }
                                    $input .= '<input type="checkbox"   name="' . $classname . '[rubrics][]"  id="test-id-'.$child2['id'] .'" value="' . $child2['id'] . '" ' . $checked . '>';
                                    $input .= '<label for="test-id-'.$child2['id'].'">' . $child2['title'] . '</label>';
                                }

                                $str.='
                                    <div class="accordion-group sub-accordion">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseInnerOne-'.$child['id'].'">
                                                '.$child['title'].'
                                            </a>
                                        </div>
                                        <div id="collapseInnerOne-'.$child['id'].'" class="accordion-body collapse  '.$in.'">
                                            <div class="accordion-inner">
                                                   '.$input.'
                                            </div>
                                        </div>
                                    </div>
                                ';
                            }
                            if(empty($child->subrubricsPriceMap)){
                                if (is_array($model->rubrics) && in_array($child['id'], $model->rubrics)) {
                                    $checked = 'checked';
                                    $in = 'in';
                                } else {
                                    $checked = '';
                                }
                                $str .= '<input type="checkbox"   name="' . $classname . '[rubrics][]"  id="test-id-'.$child['id'] .'" value="' . $child['id'] . '" ' . $checked . '>';
                                $str .= '<label for="test-id-'.$child['id'].'">' . $child['title'] . '</label>';
                            }
                        }
                        ?>
                        <div id="collapseOne-<?=$item['id'] ?>"
                             class="panel-collapse collapse <?=$in ?>"
                             role="tabpanel"
                             aria-labelledby="headingOne-<?=$item['id'] ?>">
                            <div class="panel-body">
                                <?=$str ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
        </div>
    </div>
    <h2 class="sidebar-grey__title reg"><?=Yii::t('common', 'Regions') ?></h2>
    <div class="sidebar-grey-region">
        <?php $region = Region::find()->all() ?>
        <input type="hidden" name="<?=$classname ?>[regions]">
        <?php foreach ($region as $item): ?>
            <div class="sidebar-grey-input col-sm-12">
                <input type="checkbox" id="oblast-id-<?=$item->id ?>"
                       name="<?=$classname ?>[regions][]" value="<?=$item->id ?>"
                    <?=is_array($model->regions) && in_array($item->id, $model->regions) ? 'checked' : '' ?>>
                <label for="oblast-id-<?=$item->id ?>"><?=$item->title ?></label>
            </div>
        <?php endforeach; ?>
    </div>
