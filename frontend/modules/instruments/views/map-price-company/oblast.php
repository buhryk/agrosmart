<?php
use frontend\assets\CompanyPriceAsset;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use frontend\assets\MapPriceCompanyAsset;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use frontend\assets\OblastPriceCompanyAsset;


$classname = \yii\helpers\StringHelper::basename(get_class($modelSearch));
$getCityUrl = \yii\helpers\Url::to(['/city-list/index']);


$pageH1 = Yii::t('instruments', 'Карта цен по областям');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Instruments'), 'url' => ['/instruments/default/index']];
$this->params['breadcrumbs'][] = $pageH1;

OblastPriceCompanyAsset::register($this);
CompanyPriceAsset::register($this);
?>
    <div class="bread">
        <h1><?= $pageH1; ?></h1>
        <div class="bread-bg">
            <div class="container">
                <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php $form = ActiveForm::begin([
                'action' => [Yii::$app->controller->action->id],
                'method' => 'get',
                'id' => 'price-search',
                'options'=>[
                    'data-pjax'=>'#x1g'
                ],
            ]); ?>
            <div class="col-sm-3 sidebar-grey" id="sidebar">
                <div id="blocker"></div>
                <div>
                    <div class="op-button">
                        <button type="button" class="sr-btn">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <img class="sr_close" src="/images/important-close.png" alt="important-close">
                    </div>
                    <div class="sbbg">
                        <?php $action = Yii::$app->controller->action->id;    ?>
                        <div class="sell-block <?=$action == 'oblast' ? 'active' : '' ?>">
                            <a href="<?=Url::to(['oblast']) ?>" class="like-tabs"><?=Yii::t('common', 'ZAKUPKA') ?></a>
                        </div>
                        <div class="sell-block <?=$action == 'oblast-prodazha' ? 'active' : '' ?>">
                            <a href="<?=Url::to(['oblast-prodazha']) ?>" class="like-tabs"><?=Yii::t('common', 'PRODAZHA') ?></a>
                        </div>
                        <?=$this->render('_oblast_search', ['model' => $modelSearch, 'classname' => $classname]); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 logistic-calc" data-content="page_content">
                <div class="col-xs-12 important-info">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__title">
                            <?=Yii::t('informing', 'Map price oblast') ?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row filter-show-custom">

                            <div class="col-xs-12" style="">
                                <?php echo Html::submitButton(Yii::t('advertisement', 'Search'), ['class' => 'btn-green']) ?>
                                <a href="<?= Url::to(['index']); ?>" class="btn-green" style="">
                                    <?= Yii::t('advertisement', 'Reset filter'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="map-price-company">
                            <div id="map"></div>
                        </div>
                    </div>

                </div>
            </div>
            <?php ActiveForm::end();  ?>
        </div>
    </div>
<?php $request = Yii::$app->request->get() ? Yii::$app->request->get() : [] ?>
<input type="hidden"  id="get-city-url" value='<?= json_encode(Yii::$app->request->get()) ?>' data-ajax--url="<?=Url::to(['/instruments/map-price-company/oblast-search', 'action' => $action]) ?>">
<?php
$this->registerJs('
  var mapData = '.$mapData.';  
   
',\yii\web\View::POS_HEAD);

?>