<?php
use yii\bootstrap\ActiveForm;
use backend\modules\commondata\models\Region;
use backend\modules\rubric\models\Rubric;
use frontend\assets\CompanyPriceAsset;

CompanyPriceAsset::register($this);

$classname = \yii\helpers\StringHelper::basename(get_class($model));
?>


    <div class="clearfix"></div>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <h2 class="sidebar-grey__title"><?=Yii::t('common', 'продукция') ?></h2>
        <div class="panel panel-default">
            <?php $listRubric  = Rubric::find()
                ->andWhere(['parent_id' => Rubric::TRAIDER_RUBRIC_ID])
                ->all(); ?>
            <?php foreach ($listRubric as $item):  ?>
                    <div class="sidebar-light-grey-input  panel-heading">
                        <input type="checkbox" id="rubrics-id-<?=$item['id'] ?>"
                               name="<?=$classname ?>[rubrics][]" value="<?=$item['id'] ?>"
                            <?=is_array($model->rubrics) && in_array($item['id'], $model->rubrics) ? 'checked' : '' ?>>
                        <label for="rubrics-id-<?=$item['id'] ?>"><?=$item['title'] ?></label>
                    </div>
            <?php endforeach; ?>
        </div>
    </div>
    <h2 class="sidebar-grey__title reg"><?=Yii::t('common', 'Regions') ?></h2>
    <div class="sidebar-grey-region">
        <?php $region = Region::find()->all() ?>
        <input type="hidden" name="<?=$classname ?>[regions]">
        <?php foreach ($region as $item): ?>
            <div class="sidebar-grey-input col-sm-12">
                <input type="checkbox" id="oblast-id-<?=$item->id ?>"
                       name="<?=$classname ?>[regions][]" value="<?=$item->id ?>"
                    <?=is_array($model->regions) && in_array($item->id, $model->regions) ? 'checked' : '' ?>>
                <label for="oblast-id-<?=$item->id ?>"><?=$item->title ?></label>
            </div>
        <?php endforeach; ?>
    </div>
