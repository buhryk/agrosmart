<?php
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
use frontend\assets\AnalyticsPriceAsset;

AnalyticsPriceAsset::register($this);

$pageH1 = Yii::t('common', 'Аналитика цен трейдеров');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => \yii\helpers\Url::to('/instruments/analytics-price')]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Analytics'), 'url' => ['analytics']];
$this->params['breadcrumbs'][] = $pageH1;
\yii\widgets\Pjax::begin();
?>
<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => [Yii::$app->controller->action->id],
            'method' => 'get',
            'id' => 'price-search',
            'options'=>[
                'data-pjax'=>'#x1g'
            ],
        ]); ?>
            <div class="col-sm-3 sidebar-grey" id="sidebar">
                <div id="blocker"></div>
                <div>
                    <div class="op-button">
                        <button type="button" class="sr-btn">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <img class="sr_close" src="/images/important-close.png" alt="important-close">
                    </div>
                    <div class="sbbg">
                        <?php $action = Yii::$app->controller->action->id;    ?>
                        <div class="sell-block <?=$action == 'index' ? 'active' : '' ?>">
                            <a href="<?=Url::to(['index']) ?>" class="like-tabs"><?=Yii::t('common', 'ZAKUPKA') ?></a>
                        </div>
                        <div class="sell-block <?=$action == 'prodazha' ? 'active' : '' ?>">
                            <a href="<?=Url::to(['prodazha']) ?>" class="like-tabs"><?=Yii::t('common', 'PRODAZHA') ?></a>
                        </div>
                        <?=$this->render('_search', ['model' => $modelSearch]); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 logistic-calc" data-content="page_content">
                <div class="col-xs-12 important-info">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__title">
                            <?=Yii::t('instruments', 'Для построения графика и таблицы используются', ['link' => Url::to('/company/price/trader-zakupka')]) ?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-sm-6 mt25">
                                <label class="col-md-4"><?=Yii::t('instruments', 'Период от') ?>:</label>
                                <?= DatePicker::widget([
                                    'model' => $modelSearch,
                                    'name' => 'PriceSearch[from_date]',
                                    'value' => $modelSearch->from_date,
                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                    'options' => [
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd/mm/yyyy'
                                    ]
                                ]); ?>
                            </div>
                            <div class="col-sm-6 mt25">
                                <label class="col-md-4"><?=Yii::t('instruments', 'Период до') ?>:</label>
                                <?= DatePicker::widget([
                                    'model' => $modelSearch,
                                    'name' => 'PriceSearch[to_date]',
                                    'value' => $modelSearch->to_date,
                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                    'options' => [
                                    ],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd/mm/yyyy'
                                    ]
                                ]); ?>
                            </div>

                            <div class="col-xs-12 mt20 db"></div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div id="chartdiv"></div>
                        <span class="ap-form-date__title mtb25 db"><?=Yii::t('instruments', 'Таблица цен') ?></span>
                        <div class="table-container">
                            <table class="branches-table little col-xs-12 mb25">
                                <tbody>
                                <tr class="grey-row">
                                    <th class="text-center" rowspan="2"><?=Yii::t('instruments', 'Продукт') ?></th>
                                    <th class="text-center brbn" colspan="2"><?=Yii::t('instruments', 'Продажа') ?></th>
                                    <th class="text-center" rowspan="2"><?=Yii::t('instruments', 'Изменение') ?></th>
                                </tr>
                                <tr class="grey-row text-center">
                                    <th class="text-center brtn"><?=Yii::t('instruments', 'Мин. цена') ?><br />(<?=Yii::t('instruments', 'за сутки') ?>)</th>
                                    <th class="text-center brtn"><?=Yii::t('instruments', 'Макс. цена') ?><br />(<?=Yii::t('instruments', 'за сутки') ?>)</th>

                                </tr>
                                <?php foreach ($priceListTable as $item):  ?>
                                    <?php if(isset($item['price'])): ?>
                                        <tr class="white-row text-center">
                                            <td><?=$item['title'] ?></td>
                                            <td><?=$item['price']['minPrice'] ?></td>
                                            <td><?=$item['price']['maxPrice'] ?></td>
                                            <td><?=$item['price']['changePrice'] ?> (<?=$item['price']['changePercent'] ?>%)</td>
                                        </tr>
                                    <?php endif;?>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$this->registerJs('
AmCharts.translations[ "export" ][ "en" ][ "menu.label.save.data" ] = "'.Yii::t('instrument',  'Скачать данные').'";
AmCharts.translations[ "export" ][ "en" ][ "menu.label.save.image" ] = "'.Yii::t('instrument',  'Скачать').'";
AmCharts.translations[ "export" ][ "en" ][ "menu.label.draw" ] = "'.Yii::t('instrument',  'Анотировать').'";
AmCharts.translations[ "export" ][ "en" ][ "menu.label.print" ] = "'.Yii::t('instrument',  'Печать').'";
AmCharts.translations[ "export" ][ "en" ][ "menu.label.change" ] = "'.Yii::t('instrument',  'Изменить ...').'";
AmCharts.translations[ "export" ][ "en" ][ "menu.label.cancel" ] = "'.Yii::t('instrument',  'Отмена').'";

    var chart = AmCharts.makeChart("chartdiv", {
       "type": "serial",
        
        "legend": {
            "useGraphSettings": true
        },
        "dataProvider": '.json_encode($priceListChart).',
        "valueAxes": [{
        "axisAlpha": 0,
        "dashLength": 5,
        "gridCount": 10,
        "position": "left",
        "title": "Place taken"
    }],
    "startDuration": 0.5,
    "graphs": '.json_encode($priceListChartRubric).',
    "chartCursor": {
        "cursorAlpha": 0,
        "zoomable": true
    },
    "categoryField": "date",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 45
    },
  
    "export": {
    	"enabled": true,
        "position": "bottom-right"
     }
    })'
);

\yii\widgets\Pjax::end();

?>