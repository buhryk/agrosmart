<?php
use yii\widgets\Breadcrumbs;
use frontend\assets\LogisticCalculatorAvtoAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\ordertransport\models\Order;
LogisticCalculatorAvtoAsset::register($this);

$pageH1 = Yii::t('instruments', 'Logistic calculator avto');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Instruments'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Logistics calculators'), 'url' => ['logistics-calculators/index']];
$this->params['breadcrumbs'][] = $pageH1;
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container logistic-calc-auto">
    <div class="row">
        <div class="col-sm-9 logistic-calc logistic-calc-auto" data-content="page_content">
            <div class="col-xs-12 important-info">
                <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                <div class="important-info-content">
                    <p class="important-info__title"><?=Yii::t('common/instrument', 'Используются открытые данные перевозчиков <span>(дописать названия)</span> Украины') ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php $form = ActiveForm::begin(['options' => ['class' => 'logistic-calc-form']]); ?>
                        <div class="row">
                            <div class="col-xs-4 col-md-3">
                                <span style="line-height: 42px;"><?= $model->getAttributeLabel('from_location'); ?>:</span>
                            </div>
                            <div class="col-xs-8 col-md-7">
                                <?= $form->field($model, 'from_location')->textInput(['id' => 'from_location'])->label(false); ?>
                            </div>
                        </div>
                        <div class="row mt20">
                            <div class="col-xs-4 col-md-3">
                                <span style="line-height: 42px;"><?= $model->getAttributeLabel('to_location'); ?>:</span>
                            </div>
                            <div class="col-xs-8 col-md-7">
                                <?= $form->field($model, 'to_location')->textInput(['id' => 'to_location'])->label(false); ?>
                            </div>
                        </div>
                        <div class="logistic-calc-buttons">
                            <?= Html::submitButton(Yii::t('instruments', 'Calculate cost'), ['class' => 'btn-green']) ?>

                            <?php /*= Html::a(Yii::t('instruments', 'Rush order transport'), ['/ordertransport/order/create'],
                                    ['class' => 'btn-white fr modalButton']
                                );
                                */
                            ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

            <?php if (isset($locations) && $locations) { ?>
                <div class="col-xs-12" id="map"></div>
                <?php $this->registerJs("var locations = ".$locations.";",
                    \yii\web\View::POS_HEAD,
                    'my-button-handler'
                ); ?>

                <?php $distanceInfo = explode(' ', $distance); ?>
                <div class="col-xs-12 about-container logistic-calc-data">
                    <div class="logistic-calc-info">
                        <div class="logistic-calc-info-dist">
                            <span><?= Yii::t('elevator', 'Distance'); ?>: </span>
                            <?php if (isset($distanceInfo[0])) { ?>
                                <span class="number"><?= $distanceInfo[0]; ?></span>
                            <?php } ?>
                            <?php if (isset($distanceInfo[1])) { ?>
                                <span> <?= $distanceInfo[1]; ?></span>
                            <?php } ?>
                        </div>
                        <div class="logistic-calc-info-price">
                            <span><?=Yii::t('common/instrument', 'Стоимость перевозки') ?>: </span>
                            <span class="number"><?= $cost; ?></span>
                            <span> грн</span>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-xs-12" id="map"></div>
            <?php } ?>
        </div>

        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</div>