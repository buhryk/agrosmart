<?php
/**
 * Created by PhpStorm.
 * User: Borys
 * Date: 03.04.2017
 * Time: 19:33
 */
$this->title = $model->title;
?>
<div class="bread">
    <h1><?= $this->title ?></h1>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <strong>
                    <?=Yii::t('common', 'У вас нету доступа к этому инструменту') ?>
                </strong>
            </div>
        </div>
        <div class="col-md-12">
            <?=$model->text ?>
        </div>
    </div>
</div>
