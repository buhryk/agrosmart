<?php
use yii\widgets\ActiveForm;
use backend\modules\commondata\models\City;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\modules\elevator\models\Elevator;
use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\assets\ElevatorsMapAsset;
use yii\widgets\Breadcrumbs;
ElevatorsMapAsset::register($this);


$pageH1 = Yii::t('instruments', 'Elevators map');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => '/instruments/elevators/map']);
$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Instruments'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $pageH1;

$getCityUrl = \yii\helpers\Url::to(['/city-list/index']);
$classname = \yii\helpers\StringHelper::basename(get_class($searchModel));
?>

<div class="bread">
    <h1><?= $pageH1 ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container elevators-map-container">
    <div class="row">
        <?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::to(['map'])]); ?>
        <?= $this->render('_left_filters', ['form' => $form, 'searchModel' => $searchModel]); ?>

        <div class="col-sm-9 logistic-calc" data-content="page_content">
            <div class="col-xs-12 important-info">
                <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                <div class="important-info-content">
                    <p class="important-info__title"><?=Yii::t('instrument', 'Используются собранные в открытом доступе данные об элеваторах') ?></p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-xs-12" style="padding-top: 20px;">
                    <?php echo $form->field($searchModel, 'name')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Elevator::find()->joinWith('lang')->all(), 'title', 'title'),
                        'options' => [
                            'placeholder' => Yii::t('elevator', 'Title')
                        ],
                        'pluginOptions' => ['allowClear' => true]
                    ])->label(false); ?>

                    <?php $city = $searchModel->city_id ? City::find()
                        ->where([City::tableName().'.id' => $searchModel->city_id])
                        ->joinWith('region')
                        ->one() : null ?>

                    <?= $form->field($searchModel, 'city_id')->widget(Select2::className(), [
                        'initValueText' => $city ? $city->detailInfo : '',
                        'options' => [
                            'placeholder' => Yii::t('advertisement', 'Enter city name')
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 2,
                            'ajax' => [
                                'url' => $getCityUrl,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ])->label(false); ?>
                </div>
                <div class="col-xs-12" style="padding-bottom: 15px;">
                    <div class="row">
                        <div class="col-xs-12 col-md-2" style="padding-top: 5px;"><?= Yii::t('elevator', 'Distance from'); ?>:</div>
                        <div class="col-xs-12 col-md-10">
                            <input type="text"
                                   id="from_location"
                                   name="<?= $classname.'[from_location]'; ?>"
                                   value="<?= $searchModel->from_location; ?>"
                                   placeholder="<?= Yii::t('advertisement', 'Enter city name'); ?>"
                                   style="width: 100%"
                            >
                        </div>
                    </div>
                </div>

                <div class="col-xs-12" style="text-align: center; padding-bottom: 15px;">
                    <?php echo Html::submitButton(Yii::t('advertisement', 'Search'), ['class' => 'btn-green']) ?>
                    <a href="<?= Url::to(['map']); ?>" class="btn-green" style="">
                        <?= Yii::t('advertisement', 'Reset filter'); ?>
                    </a>
                </div>

                <div class="col-xs-12">
                    <div id="elevators_map" style="width: 100%; height: 600px; margin-bottom: 10px;"></div>
                </div>

                <?php $this->registerJs("var locations = ".$locations.";",
                    \yii\web\View::POS_HEAD,
                    'my-button-handler'
                ); ?>

                <div class="col-xs-12 rating-company ptb">
                    <?php foreach ($models as $model) { ?>
                        <?= $this->render('_elevator', ['model' => $model]); ?>
                    <?php } ?>
                </div>

                <div style="text-align: center">
                    <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages, 'registerLinkTags' => true]); ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
