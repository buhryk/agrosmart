<?php
use yii\widgets\Breadcrumbs;
use frontend\assets\ElevatorAsset;
use backend\modules\elevator\models\ElevatorAccessibility;
use yii\helpers\Url;
ElevatorAsset::register($this);

$pageH1 = $model->title;
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['view', 'alias' =>$model->alias])]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Instruments'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('instruments', 'Elevators map'), 'url' => ['map']];
$this->params['breadcrumbs'][] = $pageH1;
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<div class="container elevator-page" style="padding-top: 46px; padding-bottom: 46px;">
    <div class="row">
        <div class="col-xs-12 col-md-9">
            <?php if ($locations != 'false') { ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="elevators_map" style="height: 400px; width: 100%"></div>
                    </div>
                    <p>&nbsp;</p>
                </div>
            <?php } ?>
            <?php $this->registerJs("var locations = ".$locations.";",
                \yii\web\View::POS_HEAD,
                'my-button-handler'
            ); ?>
            <div class="clearfix"></div>

            <?php $accessibilities = $model->accessibilities; ?>

            <?php if ($accessibilities || $model->volume || $model->loading || $model->unloading) { ?>
                <div class="col-xs-12 sales-product elevator-block">
                    <div class="col-sm-6" style="line-height: 100px;text-align: center;">
                        <?php foreach ($accessibilities as $one) { ?>
                            <?php if ($one->accessibility_id == ElevatorAccessibility::ACCESS_BY_CAR) { ?>
                                <img src="/images/auto.png" title="<?= Yii::t('elevator', 'Access by car'); ?>" alt="">
                            <?php } elseif ($one->accessibility_id == ElevatorAccessibility::ACCESS_BY_TRAIN) { ?>
                                <img src="/images/zd.png" title="<?= Yii::t('elevator', 'Access by train'); ?>" alt="">
                            <?php } elseif ($one->accessibility_id == ElevatorAccessibility::ACCESS_BY_BOAT) { ?>
                                <img src="/images/water.png" title="<?= Yii::t('elevator', 'Access by boat'); ?>" alt="">
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6" style="height: 100px;text-align: center; display: table">
                        <div style="display: table-cell; vertical-align: middle">
                            <span class="dsb">
                                <?php $volume = $model->volume ? ($model->volume . ' т') : Yii::t('common', 'No data'); ?>
                                <?= Yii::t('elevator', 'Warehouses'); ?>: <?= $volume; ?>
                            </span>
                            <span class="dsb">
                                <?php $loading = $model->loading ? ($model->loading . ' '.Yii::t('elevator', 't\day')) : Yii::t('common', 'No data'); ?>
                                <?= Yii::t('elevator', 'Elevator loading'); ?>: <?= $loading; ?>
                            </span>
                            <span class="dsb">
                                <?php $unloading = $model->unloading ? ($model->unloading . ' '.Yii::t('elevator', 't\day')) : Yii::t('common', 'No data'); ?>
                                <?= Yii::t('elevator', 'Elevator unloading'); ?>: <?= $unloading; ?>
                            </span>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-xs-12">
                    <div><?= $model->location; ?></div>
                    <?php if ($model->phones) { ?>
                        <div>Тел.: <?= $model->phones; ?></div>
                    <?php } ?>
                    <?php if ($model->email) { ?>
                        <div>Email: <?= $model->email; ?></div>
                    <?php } ?>
                    <?php if ($model->site) { ?>
                        <div>Сайт: <a href="<?= $model->site; ?>"><?= $model->site; ?></a></div>
                    <?php } ?>
                    <?php if ($model->property_type) { ?>
                        <div><?= $model->getAttributeLabel('property_type') . ': ' . $model->propertyTypeTitle; ?></div>
                    <?php } ?>
                    <?php if (is_numeric($model->laboratory)) { ?>
                        <div><?= $model->getAttributeLabel('laboratory') . ': ' . mb_strtolower($model->laboratoryTitle ? $model->laboratoryTitle : Yii::t('instrument/elevators', 'Нет данных')); ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <?php $contacts = $model->contacts; ?>
            <?php if ($contacts) { ?>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-xs-12">
                        <?php foreach ($contacts as $contact) { ?>
                            <div class="one-elevator-contact">
                                <div class="one-elevator-contact-title"><?= $contact->fio; ?></div>
                                <div class="one-elevator-contact-info">
                                    <div><?= 'тел.: ' . $contact->phones; ?></div>
                                    <?= $contact->email ? ('<div>email: '.$contact->email.'</div>') : ''; ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            <?php } ?>
            <?php if ($model->additionalInfo) { ?>
                <div class="row">
                    <div class="col-xs-12">
                        <?= $model->additionalInfo; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            <?php } ?>
        </div>
        <div class="col-sm-3 sidebar-banner" style="margin-top: 0">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>

</div>