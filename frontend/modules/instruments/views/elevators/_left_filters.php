<?php
use backend\modules\commondata\models\Region;
use backend\modules\elevator\models\Elevator;
use backend\modules\elevator\models\ElevatorAccessibility;

$classname = \yii\helpers\StringHelper::basename(get_class($searchModel));
?>

<div class="col-sm-3 sidebar-grey left-filters" id="sidebar">
    <div class="sbbg" style="padding-top: 30px; padding-bottom: 30px;">
        <h4 class="sidebar-grey__title"><?= Yii::t('elevator', 'Warehouse volume'); ?>, т</h4>
        <div class="sidebar-grey-region">
            <div class="sidebar-grey-input col-sm-6">
                <?php echo $form->field($searchModel, 'volume_from')
                    ->textInput(['placeholder' => Yii::t('advertisement', 'From'), 'type' => 'number']
                    )->label(false);
                ?>
            </div>
            <div class="sidebar-grey-input col-sm-6">
                <?php echo $form->field($searchModel, 'volume_to')
                    ->textInput(['placeholder' => Yii::t('advertisement', 'To'), 'type' => 'number']
                    )->label(false);
                ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <h4 class="sidebar-grey__title"><?= Yii::t('elevator', 'Elevator loading'); ?>, т</h4>
        <div class="sidebar-grey-region">
            <div class="sidebar-grey-input col-sm-6">
                <?php echo $form->field($searchModel, 'loading_from')
                    ->textInput(['placeholder' => Yii::t('advertisement', 'From'), 'type' => 'number']
                    )->label(false);
                ?>
            </div>
            <div class="sidebar-grey-input col-sm-6">
                <?php echo $form->field($searchModel, 'loading_to')
                    ->textInput(['placeholder' => Yii::t('advertisement', 'To'), 'type' => 'number']
                    )->label(false);
                ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <h4 class="sidebar-grey__title"><?= Yii::t('elevator', 'Elevator unloading'); ?>, т</h4>
        <div class="sidebar-grey-region">
            <div class="sidebar-grey-input col-sm-6">
                <?php echo $form->field($searchModel, 'unloading_from')
                    ->textInput(['placeholder' => Yii::t('advertisement', 'From'), 'type' => 'number']
                    )->label(false);
                ?>
            </div>
            <div class="sidebar-grey-input col-sm-6">
                <?php echo $form->field($searchModel, 'unloading_to')
                    ->textInput(['placeholder' => Yii::t('advertisement', 'To'), 'type' => 'number']
                    )->label(false);
                ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <h4 class="sidebar-grey__title" style="margin-top: 10px;"><?= Yii::t('elevator', 'Property type'); ?></h4>
        <div class="sidebar-grey-region">
            <?php $types = \backend\modules\elevator\models\Elevator::getPropertyTypes(); ?>
            <?php foreach ($types as $key => $type) { ?>
                <div class="sidebar-grey-input col-sm-12">
                    <input
                            type="checkbox"
                            id="type-<?= $key; ?>"
                            name="<?= $classname; ?>[type][]"
                            value="<?= $key; ?>"
                        <?= ($searchModel->type && in_array($key, $searchModel->type)) ? 'checked="checked"' : ''; ?>
                    >
                    <label for="type-<?= $key; ?>"><?= $type; ?></label>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>

        <h4 class="sidebar-grey__title" style="margin-top: 20px;"><?= Yii::t('elevator', 'Laboratory'); ?></h4>
        <div class="sidebar-grey-region">
            <?php $laboratories = Elevator::getLaboratoriesList(); ?>
            <?php foreach ($laboratories as $key => $laboratory) { ?>
                <div class="sidebar-grey-input col-sm-12">
                    <input
                            type="checkbox"
                            id="laboratory-<?= $key; ?>"
                            name="<?= $classname; ?>[laboratory][]"
                            value="<?= $key; ?>"
                        <?= ($searchModel->laboratory && in_array($key, $searchModel->laboratory)) ? 'checked="checked"' : ''; ?>
                    >
                    <label for="laboratory-<?= $key; ?>"><?= $laboratory; ?></label>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>

        <h4 class="sidebar-grey__title" style="margin-top: 20px;"><?= Yii::t('elevator', 'Accessibility'); ?></h4>
        <div class="sidebar-grey-region">
            <?php $accessibilities = ElevatorAccessibility::getTypes(); ?>
            <?php foreach ($accessibilities as $accessibility) { ?>
                <div class="sidebar-grey-input col-sm-12">
                    <input
                            type="checkbox"
                            id="accessibility-<?= $accessibility['id']; ?>"
                            name="<?= $classname; ?>[accessibility][]"
                            value="<?= $accessibility['id']; ?>"
                        <?= ($searchModel->accessibility && in_array($accessibility['id'], $searchModel->accessibility)) ? 'checked="checked"' : ''; ?>
                    >
                    <label for="accessibility-<?= $accessibility['id']; ?>"><?= $accessibility['title']; ?></label>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>

        <h4 class="sidebar-grey__title reg" style="margin-top: 20px;"><?= Yii::t('common', 'Regions'); ?></h4>
        <div class="sidebar-grey-region">
            <?php $regions = Region::find()->joinWith('lang')->all(); ?>
            <?php foreach ($regions as $region) { ?>
                <div class="sidebar-grey-input col-sm-12">
                    <input
                            type="checkbox"
                            id="region-<?= $region->primaryKey; ?>"
                            name="<?= $classname; ?>[region][]"
                            value="<?= $region->primaryKey; ?>"
                        <?= ($searchModel->region && in_array($region->primaryKey, $searchModel->region)) ? 'checked="checked"' : ''; ?>
                    >
                    <label for="region-<?= $region->primaryKey; ?>"><?= $region->title; ?></label>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>