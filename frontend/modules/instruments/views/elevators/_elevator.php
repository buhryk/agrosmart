<?php
use backend\modules\elevator\models\ElevatorAccessibility;
use yii\helpers\Url;
use yii\helpers\StringHelper;
?>

<div class="sales-product elevator-block col-sm-12">
    <div class="col-sm-3 accessibilities-images-block">
        <?php $accessibilities = $model->accessibilities; ?>
        <?php foreach ($accessibilities as $one) { ?>
            <?php if ($one->accessibility_id == ElevatorAccessibility::ACCESS_BY_CAR) { ?>
                <img src="/images/auto.png" title="<?= Yii::t('elevator', 'Access by car'); ?>" alt="">
            <?php } elseif ($one->accessibility_id == ElevatorAccessibility::ACCESS_BY_TRAIN) { ?>
                <img src="/images/zd.png" title="<?= Yii::t('elevator', 'Access by train'); ?>" alt="">
            <?php } elseif ($one->accessibility_id == ElevatorAccessibility::ACCESS_BY_BOAT) { ?>
                <img src="/images/water.png" title="<?= Yii::t('elevator', 'Access by boat'); ?>" alt="">
            <?php } ?>
        <?php } ?>
    </div>
    <div class="col-sm-6 info-block" style="margin-top: 20px;">
        <a href="<?= Url::to(['view', 'alias' => $model->alias]); ?>">
            <?=StringHelper::truncate($model->title, 45, '...' )?>
        </a>
        <div class="location">
            <?php $location = $model->location; ?>
            <div title="<?= $location; ?>"><?= $location; ?></div>
            <?php if ($model->phones) { ?>
                <div><?= $model->phones; ?></div>
            <?php } ?>
            <?php if ($model->email) { ?>
                <div><?= $model->email; ?></div>
            <?php } ?>
        </div>
        <div style="padding-top: 15px">
            <a href="<?= Url::to(['view', 'alias' => $model->alias]); ?>" class="standart-link">
                <?= Yii::t('common', 'Detail'); ?>
            </a>
        </div>
    </div>
    <div class="col-sm-3 volumes-info-block" style="text-align: center">
        <span class="dsb">
            <?php $volume = $model->volume ? ($model->volume . ' т') : Yii::t('common', 'No data'); ?>
            <?= Yii::t('elevator', 'Warehouses'); ?>: <?= $volume; ?>
        </span>
        <span class="dsb">
            <?php $loading = $model->loading ? ($model->loading . ' '.Yii::t('elevator', 't\day')) : Yii::t('common', 'No data'); ?>
            <?= Yii::t('elevator', 'Elevator loading'); ?>: <?= $loading; ?>
        </span>
        <span class="dsb">
            <?php $unloading = $model->unloading ? ($model->unloading . ' '.Yii::t('elevator', 't\day')) : Yii::t('common', 'No data'); ?>
            <?= Yii::t('elevator', 'Elevator unloading'); ?>: <?= $unloading; ?>
        </span>
    </div>
</div>