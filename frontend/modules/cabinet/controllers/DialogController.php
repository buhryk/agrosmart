<?php

namespace frontend\modules\cabinet\controllers;

use frontend\modules\cabinet\models\Dialog;
use frontend\modules\cabinet\models\DialogDetail;
use frontend\modules\cabinet\models\DialogMessage;
use frontend\models\User;
use frontend\modules\cabinet\models\DialogMessagesSearch;
use frontend\modules\cabinet\models\DialogSearch;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DialogController extends Controller
{
    public $user;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        $this->user = Yii::$app->user->identity;
        parent::init(); // TODO: Change the autogenerated stub
    }

    public function actionIndex()
    {
        $searchModel = new DialogSearch(['currentUser' => $this->user]);
        $query = $searchModel->search(Yii::$app->request->queryParams);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => $searchModel->page_size
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('list', [
            'searchModel' => $searchModel,
            'models' => $models,
            'pages' => $pages,
            'currentUser' => $this->user
        ]);
    }

    public function actionCreateMessageModal($receiver)
    {
        $model = new DialogMessage();

        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $receiver = ($receiver && is_numeric($receiver)) ? $this->findReceiver($receiver) : null;
            if (!$receiver) {
                throw new BadRequestHttpException('Receiver not found');
            }

            $dialog = $this->getDialog($this->user->id, $receiver->id);
            $model->dialog_id = $dialog->primaryKey;
            $model->from_user_id = $this->user->id;
            $model->to_user_id = $receiver->id;

            Yii::$app->response->format = Response::FORMAT_JSON;

            $validate = $model->validate();
            $errors = $model->getErrors();

            $response = [
                'status' => true,
                'validation' => $validate,
            ];

            if (!$validate) {
                $response['errors'] = $errors;
                $response['message'] = Yii::t('common', 'Something went wrong. Please, try again after reloading page');
                Yii::$app->session->setFlash('error', Yii::t('common', 'Something went wrong. Please, try again after reloading page'));
            } else {
                $response['message'] = Yii::t('cabinet', 'Dialog message sent successfully');
                Yii::$app->session->setFlash('success', Yii::t('cabinet', 'Dialog message sent successfully'));
                $model->save();
            }

            return $response;
        } else {
            return $this->renderAjax('create_message_modal', [
                'model' => $model,
                'receiver' => $receiver
            ]);
        }
    }

    protected function findReceiver($id)
    {
        if (($model = User::find()->where(['id' => $id, 'status' => User::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function canCreateDialog($fromUserId, $toUserId)
    {
        $model = Dialog::find()
            ->where(['from_user_id' => $fromUserId, 'to_user_id' => $toUserId])
            ->one();

        return $model ? false : true;
    }

    public function getDialog($fromUserId, $toUserId)
    {
        $model = Dialog::find()
            ->where(['and', ['or',
                ['from_user_id' => $fromUserId, 'to_user_id' => $toUserId],
                ['from_user_id' => $toUserId, 'to_user_id' => $fromUserId]]])
            ->one();

        if (!$model) {
            $model = new Dialog();
            $model->from_user_id = $fromUserId;
            $model->to_user_id = $toUserId;

            if (!$model->save()) {
                throw new BadRequestHttpException('Can not create a dialog by reasons: '. implode(', ', $model->getErrors()));
            }
        }

        return $model;
    }

    public function actionChangeStatus($id)
    {
        $dialog = $this->findDialog($id);

        if (Yii::$app->request->method == 'GET' && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $status = Yii::$app->request->get('status');
            if (!in_array($status, [DialogDetail::STATUS_ACTIVE, DialogDetail::STATUS_ARCHIVE])) {
                throw new BadRequestHttpException('Wrong value for status parameter');
            }

            $dialogDetail = $dialog->detail;

            if ($dialogDetail) {
                $dialogDetail->status = $status;
                if ($dialogDetail->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('cabinet', 'Dialog status changed successfully'));
                    return ['status' => true];
                }
            }

            Yii::$app->session->setFlash('error', 'Error');

            return [
                'status' => true
            ];
        }
    }

    public function actionDialog($id)
    {
        $model = $this->findDialog($id);
        $currentUser = $this->user;
        $interlocutor = ($model->from_user_id == $currentUser->id) ? $model->receiver : $model->author;
        $newMessage = new DialogMessage();
        $newMessage->dialog_id = $model->primaryKey;
        $newMessage->to_user_id = $interlocutor->id;
        $newMessage->from_user_id = $currentUser->id;

        $searchModel = new DialogMessagesSearch(['dialog' => $model]);
        $query = $searchModel->search(Yii::$app->request->queryParams);
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        if ($newMessage->load(Yii::$app->request->post()) && $newMessage->save()) {
            return $this->refresh();
        }

        if ($models) {
            DialogMessage::updateAll(['status' => DialogMessage::STATUS_VIEWED], [
                'id' => ArrayHelper::getColumn($models, 'id'),
                'to_user_id' => $currentUser->id
            ]);
        }

        return $this->render('dialog', [
            'model' => $model,
            'interlocutor' => $interlocutor,
            'currentUser' => $currentUser,
            'newMessage' => $newMessage,
            'messages' => $models,
            'pages' => $pages,
            'searchModel' => $searchModel
        ]);
    }

    protected function findDialog($id)
    {
        $dialog = Dialog::find()
            ->where(['id' => $id])
            ->andWhere(['and',
                ['or',
                    ['from_user_id' => $this->user->id],
                    ['to_user_id' => $this->user->id]
                ]
            ])->one();

        if (!$dialog) {
            throw new NotFoundHttpException('The requested page not found');
        }

        return $dialog;
    }
}