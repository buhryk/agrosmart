<?php

namespace frontend\modules\cabinet\controllers;

use frontend\modules\cabinet\models\PullTop;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;

class PullTopController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex($model_name, $model_id)
    {
        $model = new PullTop();
        
        $model->load(Yii::$app->request->get());
        $model->initPage(Yii::$app->request->queryParams);
        
        return $this->render('index', ['model' => $model]);
    }


}