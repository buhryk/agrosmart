<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 09.02.2018
 * Time: 12:53
 */

namespace frontend\modules\cabinet\controllers;


use backend\modules\price2\models\Price;
use backend\modules\setting\models\Setting;
use common\components\ExelPrice;
use frontend\models\User;
use frontend\modules\cabinet\models\PriceForm;
use Yii;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;

class PriceController extends CompanyController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    public function actionIndex($price_id = null)
    {
        if ($price_id) {
            $priceModel = $this->findModel($price_id);
            $model = new PriceForm(['priceModel' => $priceModel]);
        } else {
            $model = new PriceForm();
        }


        $type = Yii::$app->request->get('type');

        if(!isset($type)){
            $type = 1;
        }

        $company = Yii::$app->user->identity->company;

        $query = Price::find()
            ->where(['price.type' => $type, 'price.company_id' => $company->primaryKey, 'price.status' => Price::STATUS_ACTIVE])
            ->joinWith('author')
            ->andWhere(['consumer.status' => User::STATUS_ACTIVE])
            ->orderBy('price.updated_at DESC');

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $prices = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->exel = UploadedFile::getInstance($model, 'exel');

            if ( $model->exel && $model->upload($type)) {
                return $this->redirect(['/cabinet/price/index', 'type' => $type, 'file' => $model->priceModel->id]);
            }
        }

        $setting = Setting::find()->where(['alias' => 'exel_template'])->one();
        $template = $setting ? $setting->value : null;

        return $this->render('index',[
            'model' => $model,
            'error' => ExelPrice::$arr,
            'prices' => $prices,
            'pagination' => $pagination,
            'type' => $type,
            'template' => $template
        ]);
    }

    public function actionDownload($id)
    {
        $exel = new ExelPrice();
        $exel->export($id);
    }

    public function actionDelete($id, $type)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'type' => $type]);
    }

    protected function findModel($id)
    {
        $company = Yii::$app->user->identity->company;
        if (($model = Price::find()
                ->where(['company_id' => $company->primaryKey, 'id' => $id])
                ->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}