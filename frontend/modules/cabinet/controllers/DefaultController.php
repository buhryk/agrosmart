<?php

namespace frontend\modules\cabinet\controllers;

use backend\modules\page\models\Category;
use backend\modules\page\models\Page;
use frontend\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\modules\instrument\models\Instrument;
use yii\web\NotFoundHttpException;
use yii\web\NotAcceptableHttpException;


class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        $userCompany = $user->company;

        if ($userCompany) {
            $data = $this->getCompany($userCompany);
            $view = 'index';
        } else {
            $view = 'index-user';
            $data = $this->getUser($user);
        }

        
        if($userCompany) {
            $view = 'index-full';
            $data['page'] = Page::findOne(33);
            $data['instruments'] = Instrument::find()
                ->innerJoin('instrument_visibility', 'instrument.id = instrument_visibility.instrument_id')
                ->where(['in', 'instrument_visibility.type_id', Yii::$app->userRole->role])
                ->andWhere(['type' => Instrument::TYPE_INSTRUMENT])
                ->orderBy('position')
                ->all();
        }
        

        return $this->render($view, [
            'data' => $data,
  
        ]);
    }

    protected function getCompany($userCompany)
    {
        if (empty($userCompany->logo_file) && empty($userCompany->short_description) && empty($userCompany->full_description)) {
            $data['profile'] = Page::findOne(30);
        }

        if (!$userCompany->sale) {
            $data['price'] = Page::findOne(29);
        }

        if (!$userCompany->advertisements) {
            $data['product'] = Page::findOne(31);
        }

        return isset($data) ? $data : NULL;
    }

    protected function getUser($user)
    {
        if (empty($user->name) && empty($user->surname)) {
            $data['profile'] = Page::findOne(32);
        }

        if (!$user->advertisements) {
            $data['product'] = Page::findOne(31);
        }

        return isset($data) ? $data : NULL;
    }
}