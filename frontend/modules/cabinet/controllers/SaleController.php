<?php

namespace frontend\modules\cabinet\controllers;

use backend\modules\rubric\models\RubricMeasurement;
use backend\modules\rubric\models\RubricMeasurementCategoriesRubrics;
use frontend\modules\instruments\models\InstrumentGet;
use Yii;
use frontend\modules\cabinet\models\Sale;
use frontend\modules\cabinet\models\SaleSearch;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * SaleController implements the CRUD actions for Sale model.
 */
class SaleController extends  CompanyController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sale models.
     * @return mixed
     */
    public function actionIndex($currency = Sale::TABLE_UAH)
    {
        $searchModel = new SaleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Sale::TYPE_PRODAZHA, $currency);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currency' => $currency
        ]);
    }


    public function actionZakupka($currency = Sale::TABLE_UAH)
    {
        $searchModel = new SaleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Sale::TYPE_ZAKUPKA, $currency);

        return $this->render('zakupka', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currency' => $currency
        ]);
    }
    
    /**
     * Displays a single Sale model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = Sale::TYPE_PRODAZHA, $currency = NULL)
    {
        $model = new Sale();
        $model->company_id = Yii::$app->user->identity->companyUser->company_id;
        $model->type = $type;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('cabinet', 'ADDED_SUCCESSFULLY'));
            $view = $model->type == Sale::TYPE_ZAKUPKA ? 'zakupka' : 'index';
            return $this->redirect([$view, 'currency' => $model->currency_id]);
        } else {
            $model->date = date('Y-m-d');
            if(Yii::$app->request->isAjax) {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'currency' => $currency
                ]);
            }else{
                return $this->render('create', [
                    'model' => $model,
                    'currency' => $currency
                ]);
            }
        }
    }
    
    public function actionValidCreate($type = Sale::TYPE_PRODAZHA, $currency = NULL)
    {
        $model = new Sale();
        $model->company_id = Yii::$app->user->identity->companyUser->company_id;
        $model->type = $type;


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return true;
        }else{
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('cabinet', 'UPDATED_SUCCESSFULLY'));
            $view = $model->type == Sale::TYPE_ZAKUPKA ? 'zakupka' : 'index';
            return $this->redirect([$view]);
        } else {
            if(Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }else{
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }


    public function actionDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $mas = Yii::$app->request->post('sales');
        foreach ($mas as $item){
            if($model = Sale::find()->where(['id' => $item])->andWhere(['company_id' => Yii::$app->user->identity->company->id])->one()){
                $model->delete();
            }
        }



        return [
            'message' => Yii::t('cabinet', 'SALE_REMOVED_SUCCESSFULLY'),
            'error'=>''
        ];
    }

    public function actionCopy(){

        Yii::$app->response->format = Response::FORMAT_JSON;
        $mas = Yii::$app->request->post('sales');
        $error = '';
        $company_id = Yii::$app->user->identity->company->id;
        $count = 0;
        foreach ($mas as $key => $item){
            $model = Sale::find()->where(['id' => $item])
                ->andWhere(['company_id' => $company_id])
                ->one();

            if($model){
                $new_model = new Sale();
                $new_model->city_id = $model->city_id;
                $new_model->address = $model->address;
                $new_model->volume = $model->volume;
                $new_model->description_address = $model->description_address;
                $new_model->price = $model->price;
                $new_model->is_list = $model->is_list;
                $new_model->type = $model->type;
                $new_model->currency_id = $model->currency_id;
                $new_model->rubric_id = $model->rubric_id;
                $new_model->measurement_id =  $model->measurement_id;
                $new_model->date =date("Y-m-d");
                if($new_model->save()){
                    $count++;
                }
            }
        }

        return [
            'message' => Yii::t('cabinet', 'Cкопировано успешно {count} цены', ['count' => $count]),
            'error' => ''
        ];


    }

    public function actionMeasurementCurrent()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->post('rubric_id');
        $measurment = RubricMeasurementCategoriesRubrics::find()->where(['rubric_id' =>$id])->one();

        if($measurment){
            $measurement_default = RubricMeasurement::find()
                ->where(['category_id' => $measurment->category_id])
                ->orderBy('default DESC')
                ->one();

            return [
                'category_id' => $measurment->category_id,
                'measurement_default' => $measurement_default->id,
                'error'=>''
            ];
        }
    }

    /**
     * Finds the Sale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sale::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action)
    {
        $instrumentGet = new InstrumentGet(16);
        $instrument = $instrumentGet->searchOne();

        if ($instrument['status'] == InstrumentGet::STATUS_NO_ACCCES) {
            return $this->redirect(['/instruments/instrument/no-access', 'id' => $instrumentGet->instrument_id ]);
        }

        if (Yii::$app->user->isGuest) {
            throw new NotAcceptableHttpException('Access denied');
        }
        if (!Yii::$app->user->identity->company) {
            throw new NotAcceptableHttpException('The requested content does not exist.');
        }

        return parent::beforeAction($action);
    }
}
