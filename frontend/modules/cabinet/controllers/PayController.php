<?php

namespace frontend\modules\cabinet\controllers;

use frontend\modules\cabinet\models\Pay;
use frontend\modules\cabinet\models\PayPackage;
use Yii;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;

class PayController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex() 
    {
        $pay = new Pay();
        if(Yii::$app->request->post()){
            $result = $pay->pay(Yii::$app->request->post());
        } else {
            return $this->redirect(['/']);
        }

        return $this->render('index', ['result' => $result]);
    }
    
    public function actionResult()
    {
        $pay = new Pay();
        if(Yii::$app->request->post()){
            $result = $pay->pay(Yii::$app->request->post());
        }
    }

    public function actionPackageResult()
    {
        $pay = new PayPackage();
        if(Yii::$app->request->post()){
            $result = $pay->pay(Yii::$app->request->post());
            return $this->render('package-result', ['result' => $result]);
        } else {
            return $this->redirect(['/cabinet/tariff/index']);
        }
        
    }


}