<?php

namespace frontend\modules\cabinet\controllers;

use backend\modules\package\models\Package;
use frontend\modules\cabinet\models\PayPackage;
use Yii;
use frontend\modules\cabinet\models\Subsidiary;
use frontend\modules\cabinet\models\SubsidiarySearch;
use yii\web\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * SubsidiaryController implements the CRUD actions for Subsidiary model.
 */
class TariffController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex() 
    {
        $models = Package::find()
            ->where(['in', 'site_role_id', Yii::$app->userRole->role])
            ->orderBy('position')
            ->all();
        
        return $this->render('index', ['models' => $models]);
    }

    public function actionView($id)
    {
        if(!$model = Package::findOne($id)) {
            throw new NotAcceptableHttpException('The requested content does not exist.');
        }
        
        $pay = new PayPackage();

        return $this->render('view', ['model' => $model, 'button' => $pay->getButton($model)]);
    }

  
}