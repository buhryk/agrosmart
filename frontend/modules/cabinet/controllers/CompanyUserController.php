<?php

namespace frontend\modules\cabinet\controllers;

use frontend\models\User;
use Yii;
use frontend\models\CompanyUser;
use frontend\modules\cabinet\models\CompanyUserSearch;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * CompanyUserController implements the CRUD actions for CompanyUser model.
 */
class CompanyUserController extends CompanyController
{
    /**
     * Lists all CompanyUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanyUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompanyUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CompanyUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = new User();
        $user->scenario = User::SCENARIO_CREATE_COMPANY_USER;
        $model = new CompanyUser();
        $model->company_id = Yii::$app->user->identity->companyUser->company_id;

        if ($model->load(Yii::$app->request->post()) && $user->load(Yii::$app->request->post())) {
            $validate = $user->validate();
            $errors = $user->getErrors();

            $response = [
                'status' => true,
                'validation' => $validate,
            ];

            if (!$validate) {
                $response['errors'] = $errors;
            }

            if ($validate) {
               $user->companyUserSendEmail();
               $model->user_id = $user->id;
               $model->save();
            }
        

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'user' => $user
            ]);
        }
    }

    /**
     * Updates an existing CompanyUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = User::findOne($model->user_id);
        $user->scenario = User::SCENARIO_CREATE_COMPANY_USER;

        if ($user->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) {
            $response = [
                'status' => true,
            ];

            if ($user->email != $user->oldAttributes['email']) {
                $user->username = $user->email;
            }

            if ($user->save() && $model->save()) {
                $response['validation'] = true;
            } else {
                $response['validation'] = false;
                $response['errors'] = array_merge($user->getErrors(), $model->getErrors());
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
                'user' => $user
            ]);
        }
    }

    /**
     * Deletes an existing CompanyUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        User::deleteAll(['id' => $model->user_id]);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompanyUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanyUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanyUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            throw new NotAcceptableHttpException('Access denied');
        }
        if (!Yii::$app->user->identity->company) {
            throw new NotAcceptableHttpException('The requested content does not exist.');
        }

        return parent::beforeAction($action);
    }
}