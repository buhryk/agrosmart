<?php

namespace frontend\modules\cabinet;

use Yii;
/**
 * cabinet module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * replace main layout
     */
    //public $layout = 'sidebar';
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cabinet\controllers';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }



}
