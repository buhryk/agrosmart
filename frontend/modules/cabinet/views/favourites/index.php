<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\cabinet\models\ElectedSearch;

$this->title = Yii::t('cabinet', 'FAVORITES');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dialog">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 ">
            <div class="margin-bottom-10">
                <div class="important-info ">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__title">
                            <?=Yii::t('informing', 'My Account - Favorites') ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 dialog-container dialogs">
                <div class="row">
                    <?php $form = ActiveForm::begin([
                        'method' => 'get',
                        'action' => ['index'],
                        'options' => ['id' => 'favourites-form']
                    ]); ?>
                    <div class="col-xs-12 dialog-filter-wrapper">
                        <?= Yii::t('common', 'Choose type'); ?>:
                        <div style="display: inline-block">
                            <?= $form->field($searchModel, 'table_name')
                                ->dropDownList(ElectedSearch::getRecordTypesList(), [
                                    'prompt' => '...',
                                    'id' => 'favourites-records-type'
                                ])->label(false);
                            ?>
                        </div>
                    </div>

                    <div class="col-xs-12 message-container">
                        <table class="branches-table col-xs-12">
                            <tr class="grey-row">
                                <th>Тип</th>
                                <th>Дата добавления</th>
                                <th>Заголовок/название</th>
                                <th class="branches-table__actions">Действия</th>
                            </tr>
                            <?php foreach ($models as $model) { ?>
                                <?php $title = '';
                                $type = '';
                                $url = ''; ?>
                                <?php switch ($model->table_name) {
                                    case 'advertisement':
                                        $type = 'Товар';
                                        $title = ($entity = $model->advertisement) != null ? $entity->title : '';
                                        $url = $entity ? Url::to(['/product/product/view', 'slug' => $entity->alias]) : '#';
                                        break;
                                    case 'company':
                                        $type = 'Компания';
                                        $title = ($entity = $model->company) != null ? $entity->name : '';
                                        $url = $entity ? Url::to(['/company/company/view', 'id' => $entity->primaryKey]) : '#';
                                        break;
                                    case 'consumer':
                                        $type = 'Пользователь';
                                        $title = ($entity = $model->user) != null ? $entity->fullName : '';
                                        $url = $entity ? Url::to(['/user/user/view', 'id' => $entity->primaryKey]) : '#';
                                        break;
                                    default:
                                        $type = 'undefined';
                                        $title = $type;
                                        $url = '#';
                                } ?>
                                <tr class="white-row">
                                    <td><?= $type; ?></td>
                                    <td>
                                        <?= date('Y-m-d H:i', $model->created_at); ?>
                                    </td>
                                    <td>
                                        <?= $title; ?>
                                    </td>
                                    <td>
                                        <a href="<?= $url; ?>"
                                           title="<?= Yii::t('common', 'View'); ?>"
                                           style="text-decoration: none">
                                            <img src="/images/watch.png" alt="watch">
                                        </a>
                                        <a class="change-elected remove-from-elected"
                                           data-type="<?= $model->table_name; ?>"
                                           data-record_id="<?= $model->record_id; ?>"
                                           data-page="cabinet"
                                           style="cursor: pointer"
                                           title="<?= Yii::t('common', 'Remove from elected'); ?>"
                                        >
                                            <img src="/images/imp.png" alt="imp">
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="message-pagination" style="margin-top: 15px;">
                        <div class="col-xs-12 col-md-6">
                            <span class="message-pagination__text"><?= Yii::t('common', 'Show per page'); ?>:</span>
                            <?php echo Html::activeDropDownList($searchModel,
                                'page_size',
                                ElectedSearch::getPerPagePropertiesList(), [
                                    'class' => 'message-pagination__select all-select',
                                    'id' => 'per-page-favourites'
                                ]); ?>
                            <?php $this->registerJs("
                            $('#per-page-favourites').on('change', function(){
                                $('form#favourites-form').submit();
                            });
                            $('#favourites-records-type').on('change', function(){
                                $('form#favourites-form').submit();
                            });
                        ", \yii\web\View::POS_READY,
                                'my-button-handler'
                            ); ?>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages]); ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>
    </div>
</div>