<?php
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;
use frontend\assets\CabinetAsset;
use yii\grid\GridView;
CabinetAsset::register($this);

$this->title = Yii::t('cabinet', 'Company users');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <p>&nbsp;</p>
        <div class="row">
            <div class="col-sm-3" id="sidebar">
                <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                    <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
                </div>
            </div>
            <div class="col-sm-9" data-content="page_content">
                <!-- important-info -->
                <div class="col-xs-12 important-info">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__text"><?=Yii::t('cabinet', 'Если вы хотите, чтобы этим кабинетом пользовались другие менеджеры компании, добавьте их как пользователей в таблицу ниже.') ?></p>
                    </div>
                </div>
                <div class="col-xs-12 mt40 ap-wrapper pl35 pr35 mb40">
                    <div class="ap-container">
                        <div class="row" style="padding-top: 40px;">
                            <div class="branches-table-wrapper">
                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    //'filterModel' => $searchModel,
                                    'layout' => "{items}\n{summary}\n{pager}",
                                    'columns' => [
                                        [
                                            'attribute' => 'roleTitle',
                                            'value' => function($data){
                                                return $data->roleTitle;
                                            },
                                        ],
                                        [
                                            'attribute' => 'Email',
                                            'value' => function($data) {
                                                $user = $data->user;
                                                return $user ? $user->email : 'undefined';
                                            },
                                        ],
                                        [
                                            'label' => Yii::t('users', 'NAME'),
                                            'value' => function($data) {
                                                $user = $data->user;
                                                return $user ? $user->name : 'undefined';
                                            },
                                        ],
                                        [
                                            'label' => Yii::t('users', 'SURNAME'),
                                            'value' => function($data) {
                                                $user = $data->user;
                                                return $user ? $user->surname : 'undefined';
                                            },
                                        ],
                                        [
                                            'label' => Yii::t('users', 'PHONE'),
                                            'value' => function($data) {
                                                $user = $data->user;
                                                return $user ? $user->phone : 'undefined';
                                            },
                                        ],
                                        [

                                            'format'=>'raw',
                                            'contentOptions'=>['style'=>'width: 60px;', 'class' => 'actions-buttons'],
                                            'value' => function ($model) {
                                                return "
                                                <a href='".\yii\helpers\Url::to(['update', 'id' => $model->id])."' 
                                                    title='".Yii::t('common', 'Edit')."' 
                                                    aria-label='".Yii::t('common', 'Edit')."' 
                                                    data-pjax='0' 
                                                    class='modalButton'>
                                                        <span class='glyphicon glyphicon-pencil'></span>
                                                </a>
                                                <a href='".\yii\helpers\Url::to(['delete', 'id' => $model->id])."' 
                                                    title='".Yii::t('common', 'Delete')."' 
                                                    aria-label='".Yii::t('common', 'Delete')."' 
                                                    data-pjax='0'
                                                    data-confirm='".Yii::t('common', 'Are you sure you want to delete this item?')."'>
                                                        <span class='glyphicon glyphicon-remove'></span>
                                                </a>";
                                            },
                                        ],
                                    ],
                                    'tableOptions'=>['class'=>'branches-table col-xs-12'],
                                ]); ?>
                            </div>
                            <div class="col-xs-12 tac">
                                <?php echo Html::a('+ '.Yii::t('cabinet', 'Add user'), ['create'], ['class' => 'btn-green tdn mb40 modalButton']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

