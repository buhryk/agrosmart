<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use \yii\helpers\ArrayHelper;
$url = \yii\helpers\Url::to(['/city-list/index']);
?>
<div class="modal-body">
    <div class="subsidiary-form">
        <div id="save-company-user-errors" style="color: red;"></div>
        <?php $form = ActiveForm::begin([
            'id'=>'add-company-user',
            'enableAjaxValidation' => false,
            'options' => [
                //'class' => 'ap-company-form clearfix',
                'onsubmit' => 'return false',
                //'id' => 'advertisement-form',
                'data-url' => $user->isNewRecord ? Url::to(['create']) : Url::to(['update', 'id' => $model->primaryKey]),
            ]
        ]); ?>

        <?= $form->field($model, 'role_id')->dropDownList($model::getRolesList()) ?>
        <?= $form->field($model, 'subsidiary_id')->dropDownList(ArrayHelper::map( $model->subsidiaryListByCompanyId(), 'id', 'address'),['prompt' => Yii::t('users','CHOOSE SUBSIDIARY')]); ?>

        <?= $form->field($user, 'phone')->textInput(['maxlength' => true]) ?>
        <?= $form->field($user, 'email')->textInput(['maxlength' => true, 'type' => 'email'])->label('Email ('.Yii::t('users', 'USERNAME').')') ?>
       <div class="row">
           <div class="col-md-6">
               <?= $form->field($user, 'name')->textInput(['maxlength' => true]) ?>
           </div>
           <div class="col-md-6">
               <?= $form->field($user, 'surname')->textInput(['maxlength' => true]) ?>
           </div>
       </div>
        <div class="form-group">
            <?= Html::button($model->isNewRecord ? Yii::t('cabinet', 'Add user') : Yii::t('cabinet', 'UPDATE'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                'id' => 'save-company-user',
                'type' => 'button'
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>