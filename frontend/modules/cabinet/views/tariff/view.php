<?php
use yii\widgets\Breadcrumbs;

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Тарифы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$package = Yii::$app->userRole->package;
$packageUser = Yii::$app->userRole->packageUser;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container rate">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="col-sm-12">
                <?php if($package && $package->id == $model->id): ?>
                    <div class="row">
                        <div class="col-md-6">
                            <hr>
                         </div>

                        <div class="col-md-12">
                           <strong><?=Yii::t('cabinet', 'Вы используете данный тариф') ?></strong>
                        </div>
                        <div class="col-md-12">
                            Дата старта : <?=date('Y-m-d H:i', $packageUser->start) ?> <br>
                            Дата окончания : <?=date('Y-m-d H:i', $packageUser->finish) ?>
                        </div>

                        <div class="col-md-6">
                            <hr>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-sm-3">
                        <p><?=Yii::t('cabinet', 'Количество людей') ?>:</p>
                    </div>
                    <div class="col-sm-9">
                        <p><strong><?=$model->count_user ?></strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p><?=Yii::t('cabinet', 'Количество дней') ?>:</p>
                    </div>
                    <div class="col-sm-9">
                        <p><strong><?=$model->interim ?></strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p><?=Yii::t('cabinet', 'Описание') ?></p>
                    </div>
                    <div class="col-sm-9">
                        <?=$model->text ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p style="text-transform: uppercase;"><?=Yii::t('cabinet', 'Всего к оплате') ?>:</p>
                    </div>
                    <div class="col-sm-9">
                        <p><strong style="font-size: 25px;"><?=$model->price ?></strong> грн</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <?=$button ?>
                    </div>
                    <div class="col-sm-9">
                        <a href="<?=\yii\helpers\Url::to(['index']) ?>">
                            <?=Yii::t('cabinet', 'Вернуться к тарифам') ?>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
