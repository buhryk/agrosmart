<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use frontend\modules\cabinet\models\Subsidiary;
use yii\widgets\LinkPager;
use frontend\models\User;
use frontend\modules\product\models\Advertisement;

$this->title = Yii::t('common', 'Тарифы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$package = Yii::$app->userRole->package;
?>
<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container rate">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="margin-bottom-10">
                <div class="important-info ">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__title">
                            <?=Yii::t('informing', 'My Account - Tariffs') ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php if(is_array($models)): ?>
               <?php foreach ($models as $model): ?>
                   <div class="col-sm-4  <?=isset($package) && $package->id == $model->id ? 'block-rate-next' : 'block-rate' ?>">
                       <div class="bg-full">
                           <div class="status-package">
                               <?php if(isset($package) && $model->id ==$package->id): ?>
                                   <?=Yii::t('cabinet', 'ВЫ ИСПОЛЬЗУЕТЕ ') ?>
                               <?php endif; ?>
                           </div>
                           <h3 class="rate-block-title"><?=$model->title ?></h3>
                           <div  class="price" style="text-align: center">
                               <span class="old-price">


                               <?php if($model->old_price): ?>

                                    <strike>  <?=$model->old_price ?>грн </strike>
                               <?php endif; ?>
                               </span>
                               <?=$model->price ?> грн
                           </div>
                           <div class="description">
                               <?=$model->description ?>
                           </div>
                           <div class="more">
                                   <a href="<?=Url::to(['view', 'id' => $model->id]) ?>" class="btn-green" >
                                       <?=Yii::t('cabinet', 'Падробнее') ?>
                                   </a>
                           </div>
                       </div>
                   </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
