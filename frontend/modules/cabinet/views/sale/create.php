<?php

use yii\helpers\Html;
use frontend\assets\NewAdvertisementAsset;

NewAdvertisementAsset::register($this);

$this->title = Yii::t('cabinet', 'Сreature');

?>
<div class="modal-dialog modal-ms">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title custom-font" style="display: inline-block;"><?= Html::encode($this->title) ?></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="/images/close-modal.png" alt="close-modal">
            </button>
        </div>
        <?= $this->render('_form', [
            'model' => $model,
            'currency' => $currency
        ]) ?>
    </div>
</div>
