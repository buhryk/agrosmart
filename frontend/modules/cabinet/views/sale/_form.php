<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use backend\modules\rubric\models\Rubric;
use yii\helpers\ArrayHelper;
use backend\modules\commondata\models\Currency;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

$url = \yii\helpers\Url::to(['/city-list/index']);
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPFMj9CPyAg6JHrd49lU9ou3VRx8fFDJc"> </script>
<div class="modal-body">
    <div class="sale-form">

        <?php $form = ActiveForm::begin([
            'id' => 'my-form-id',
            'action' => $model->isNewRecord ? Url::to(['create', 'type'=> $model->type]) : Url::to(['update', 'id' => $model->id]),
            'validationUrl' => \yii\helpers\Url::to(['valid-create', 'type'=> $model->type]),
            'enableAjaxValidation' => true,
            'options' =>
                ['data-pjax' => true]
            ]
        ); ?>
        <div class="row">
            <div class="col-sm-6 ap-company-heading" id="rubrics-container">
                <p class=""><?= Yii::t('advertisement', 'Choose rubric'); ?>*:</p>
                <div class="help-block form-error-block custom-error-block" id="error-rubric_id"></div>
                <?=$form->field($model, 'rubric_id')->hiddenInput([
                    'id' => 'rubric_id',
                    'data-url-get-rubric-measurements' => Url::to(['/product/product/get-rubric-measurements']),
                    'data-url' => Url::to(['/product/product/get-rubric-subrubrics'])
                ])->label(false) ?>
                <?php if ($model->isNewRecord): ?>
                <select class="ap-select-heading select-rubric-dropdownlist" id="heading" data-level="1" required>
                    <option>...</option>
                    <?php $rubrics = Rubric::find()->where(['parent_id' => null])->joinWith('lang')->all(); ?>
                    <?php foreach ($rubrics as $rubric) { ?>
                        <option value="<?= $rubric->primaryKey; ?>"><?= $rubric->title; ?></option>
                    <?php } ?>
                </select>
                <div class="help-block"></div>
                <?php else:
                    $rubric = $model->rubric;
                    $parent = $rubric->parent;
                    $parent2 =  $parent->parent ? $parent->parent : $parent;
                    $level = $parent->parent ? 3: 2;
                    ?>
                    <select class="ap-select-heading select-rubric-dropdownlist" id="heading" data-level="1" required>
                            <option>...</option>
                            <?php $rubrics = Rubric::find()->where(['parent_id' => null])->joinWith('lang')->all(); ?>
                            <?php foreach ($rubrics as $item) { ?>
                                <option value="<?= $item->primaryKey; ?>" <?=$parent2->id == $item->id ? 'selected' : '' ?>><?= $item->title; ?></option>
                            <?php } ?>
                    </select>
                    <?php if ($level == 3): ?>
                        <select class="ap-select-heading select-rubric-dropdownlist" id="heading" data-level="2" required>
                            <option>...</option>
                            <?php $rubrics = $parent2->subrubrics; ?>
                            <?php foreach ($rubrics as $item) { ?>
                                <option value="<?= $item->primaryKey; ?>" <?=$parent->id == $item->id ? 'selected' : '' ?>><?= $item->title; ?></option>
                            <?php } ?>
                        </select>
                    <?php endif; ?>
                    <select class="ap-select-heading select-rubric-dropdownlist" id="heading" data-level="<?=$level ?>" required>
                        <option>...</option>
                        <?php $rubrics = $parent->subrubrics; ?>
                        <?php foreach ($rubrics as $item) { ?>
                            <option value="<?= $item->primaryKey; ?>" <?=$rubric->id == $item->id ? 'selected' : '' ?>><?= $item->title; ?></option>
                        <?php } ?>
                    </select>

                <?php endif; ?>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label></label>
                    <input type="hidden" name="Sale[is_list]" value="0">
                    <input type="checkbox"
                           class="form-control"
                           id="is_list"
                           name="Sale[is_list]"
                           value="1" <?=$model->is_list == 1 ? 'checked' : ''; ?>>

                    <label for="is_list"><?=Yii::t('cabinet','Вывести в список') ?></label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?php if(empty($currency) && ($model->isNewRecord)){ ?>
                    <?php echo $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map(
                        Currency::find()->where(['status' => Currency::ACTIVE_YES])->all(), 'id', 'code'))
                    ?>
                <?php }elseif (isset($currency)){ ?>
                    <?php echo $form->field($model, 'currency_id')->dropDownList(ArrayHelper::map(
                        Currency::find()->where(['id' => $currency])->all(), 'id', 'code'))
                    ?>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                    <?= $form->field($model, 'city_id')->widget(Select2::className(), [
                        'initValueText' => !empty($model->city) ? $model->city->title : '', // set the initial display text
                        'options' => ['placeholder' => 'Ведите город'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 2,
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],

                    ]); ?>
             </div>
             <div class="col-md-5">
                    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => '...', 'style' => 'width: 130px;'],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true
                        ]
                    ]); ?>
            </div>
         </div>
        <div class="row">
            <div class="col-md-12">
                     <?= $form->field($model, 'address')
                         ->textInput(['maxlength' => true, 'placeholder' => Yii::t('cabinet', 'Нужен корректный адрес для карт цен')]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'description_address')
                    ->textInput(['maxlength' => true, 'placeholder' => Yii::t('cabinet', 'Дополнительное описание месторасположения')])
                    ->label(false)?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'volume')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-6">
                <?php $measurements = \backend\modules\rubric\models\RubricMeasurement::find()
                    ->joinWith('lang')
                    ->all();
                $data = [];
                ?>
                    <div class="form-group field-choose-measurement required">
                        <label></label>
                        <select id="choose-measurement-sale" class="form-control" name="Sale[measurement_id]">
                            <option class="item">...</option>
                           <?php foreach ($measurements as $item):?>
                                <option class="item <?= isset($model->measurement->category_id) && $item->category_id == $model->measurement->category_id ? 'active' : '' ?>"
                                        data-data="<?=$item->category_id ?>" value="<?=$item->id ?>"
                                    <?=$model->measurement_id==$item->id ? 'selected' : '' ?>>
                                    <?=$item->title ?>
                                </option>
                          <?php endforeach; ?>
                        </select>
                        <div class="help-block"></div>
                    </div>
                    <div class="help-block form-error-block custom-error-block" id="error-measurement-id"></div>

            </div>
         </div>
     
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('cabinet', 'CREATE') : Yii::t('cabinet', 'UPDATE'), ['class' => 'btn-green btn-min']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
//$this->registerJsFile("/js/cabinet-sale.js");


$this->registerJs(" 
     $('body').on('change', '.select-rubric-dropdownlist',function () {
        var url = '/cabinet/sale/measurement-current',
            rubric_id = $(this).val();
        data= {
            rubric_id:rubric_id
        };
        $.ajax({
            method: 'post',
            data :data,
            url: url,
            dataType: 'json',
            success: function(data) {
                if (!data.error) {
                    $('#choose-measurement-sale option.item').removeClass('active');
                    $('#choose-measurement-sale option[data-data='+data.category_id+']').addClass('active');
                    $('#choose-measurement-sale option[value='+data.measurement_default+']').prop('selected', true);
                }
            }
        });
    });
");
