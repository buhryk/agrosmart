<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use backend\modules\commondata\models\Currency;
use frontend\modules\cabinet\models\AdvertisementForm;
use frontend\assets\NewAdvertisementAsset;
use yii\helpers\Url;
use backend\modules\tariff\models\Tariff;
use vova07\imperavi\Widget;

NewAdvertisementAsset::register($this);

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', Yii::$app->user->identity->company ?
    'Company products and services' : 'MY_GOODS_AND_SERVICES'), 'url' => ['/cabinet/product/index']];
$this->params['breadcrumbs'][] = $this->title;
$getCityUrl = \yii\helpers\Url::to(['/city-list/index']);
$classname = \yii\helpers\StringHelper::basename(get_class($model));
$action = Yii::$app->controller->action->id;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>text
        </div>
    </div>
</div>
<div class="container dialog cabinet-edit-product">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 dialog-container dialogs" style="padding: 30px 15px;">
            <a href="<?= Url::to(['images', 'id' => $advertisement->primaryKey]); ?>" class="btn btn-success" style="border-radius: 0">
                < <?= Yii::t('advertisement', 'Edit product images'); ?>
            </a>
            <a href="<?=Url::to(['pull-top/index', 'model_id' => Yii::$app->request->get('id'), 'model_name'=>Tariff::MODEL_PRODUCT ]) ?>" class="btn btn-success" style="border-radius: 0">
                <?=Yii::t('common', 'Pull top') ?>
            </a>

            <div class="advertisement-form" style="padding-top: 30px">
                <?php $form = ActiveForm::begin(); ?>

                <?php if ($advertisement->company_id) { ?>
                    <div class="row">
                        <div class="col-sm-6 ap-form-contacts" style="margin-top: 0">
                            <label class="control-label"><?= Yii::t('advertisement', 'Contact details'); ?></label>
                            <div class="help-block form-error-block custom-error-block" id="error-author_id"></div>
                            <?php $currentUser = Yii::$app->user->identity; ?>
                            <?php echo Html::activeTextInput($model, 'author_id', [
                                'type' => 'hidden',
                                'id' => 'author_id',
                                'value' => $currentUser->id
                            ]); ?>
                            <label for="authorized">
                                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[publish_from]'; ?>"
                                       value="<?= AdvertisementForm::PUBLISH_FROM_AUTHOR; ?>" data-role="none" <?= (!$model->from_user_id && !$model->from_subsidiary_id) ? 'checked="checked"': ''; ?>>
                                <?php $username = '';
                                if ($currentUser->name) {
                                    $username .= $currentUser->name;
                                }
                                if ($currentUser->surname) {
                                    $username .= ($username ? ' ' : '') . $currentUser->surname;
                                } ?>
                                <?= Yii::t('advertisement', 'advertisement author'); ?>&nbsp;(<?= $username; ?>)
                            </label>
                            <div class="clearfix"></div>

                            <?php if ($otherCompanyUsers) { ?>
                                <label for="another">
                                    <input type="radio" id="another" class="select-author" name="<?= $classname . '[publish_from]'; ?>"
                                           value="<?= AdvertisementForm::PUBLISH_FROM_OTHER_USER; ?>" <?= $model->from_user_id ? 'checked="checked"' : ''; ?>>
                                    <?= Yii::t('advertisement', 'other user'); ?>
                                </label>
                                <?php echo Html::dropDownList($classname.'[from_user_id]', $model->from_user_id, ArrayHelper::map($otherCompanyUsers, 'id', 'fullname'), [
                                    'class' => 'ap-select-heading slct-w50-ib' . (!$model->from_user_id ? ' hidden' : ''),
                                    'id' => 'company-users-dropdown',
                                    'data-role' => 'none',
                                    'prompt' => '...'
                                ]); ?>
                                <div class="clearfix"></div>
                            <?php } ?>

                            <?php if ($subsidiaries) { ?>
                                <label for="subsidiary">
                                    <input type="radio" id="subsidiary" class="select-author" name="<?= $classname . '[publish_from]'; ?>"
                                           value="<?= AdvertisementForm::PUBLISH_FROM_SUBSIDIARY; ?>" <?= $model->from_subsidiary_id ? 'checked="checked"' : ''; ?>>
                                    <?= Yii::t('advertisement', 'subsidiary'); ?>
                                </label>
                                <?php echo Html::dropDownList($classname.'[from_subsidiary_id]', $model->from_subsidiary_id, ArrayHelper::map($subsidiaries, 'id', 'info'), [
                                    'class' => 'ap-select-heading slct-w50-ib' . (!$model->from_subsidiary_id ? ' hidden' : ''),
                                    'id' => 'company-subsidiaries-dropdown',
                                    'data-role' => 'none',
                                    'prompt' => '...'
                                ]); ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'text')->widget(Widget::className(), [
                                        'settings' => [
                                                    'language' => \Yii::$app->language,
                                                    'minHeight' => 300,
                                                    'plugins' => [
                                                                'fullscreen',
                                                            ]
                                                    ]
                                        ])->label(false) ?>

                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <?= $form->field($model, 'city_id')->widget(Select2::className(), [
                            'initValueText' => $model->location,
                            'options' => ['placeholder' => Yii::t('advertisement', 'Enter city name')],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 2,
                                'ajax' => [
                                    'url' => $getCityUrl,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                            ],

                        ]); ?>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12 col-md-3">
                                <label class="control-label"><?= Yii::t('advertisement', 'Price'); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-3">
                                <?= $form->field($model, 'price')->textInput(['type' => 'number'])->label(false) ?>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <?php echo $form->field($model, 'currency_id')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Currency::find()->where(['status' => Currency::ACTIVE_YES])->all(), 'id', 'code'),
                                    'options' => [
                                        'placeholder' => Yii::t('common', 'Choose region')
                                    ],
                                    'pluginOptions' => ['allowClear' => true]
                                ])->label(false); ?>
                            </div>
                            <div class="col-sm-12 col-md-6" style="padding-top: 5px;">
                                <input type="checkbox" id="price" name="<?= $classname . '[contract_price]'; ?>" value="1" <?= !$model->price ? 'checked="checked"' : ''; ?>>
                                <label for="price"><?= Yii::t('advertisement', 'contract price'); ?></label>
                            </div>
                        </div>
                    </div>
                    <?php if ($measurements) { ?>
                        <div class="col-sm-12 col-md-6">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <?= $form->field($model, 'volume')->textInput(['type' => 'number']) ?>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <br>
                                    <?php echo $form->field($model, 'measurement_id')->widget(Select2::classname(), [
                                        'data' => $measurements,
                                        'pluginOptions' => ['allowClear' => true]
                                    ])->label(false); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'phones')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>
                    </div>
                </div>


                <div class="row" style="margin-bottom: 30px;">
                    <div class="col-sm-12 col-md-6">
                        <div class="row">
                            <div class="col-sm-12 col-md-7">
                                <label class="control-label"><?= Yii::t('advertisement', 'Active to'); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-7">
                                <div>
                                    <?php echo DatePicker::widget([
                                        'model' => $model,
                                        'attribute' => 'active_to',
                                        'options' => [
                                            'placeholder' => '...',
                                            'style' => 'width: 130px;'
                                        ],
                                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                        'pluginOptions' => [
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true
                                        ]
                                    ]); ?>
                                </div>
                                <div class="clearfix"></div>
                                <?php if (isset($model->getErrors()['active_to'])) { ?>
                                    <div class="help-block" style="color: #a94442"><?= implode(', ', $model->getErrors()['active_to']); ?></div>
                                <?php } ?>
                            </div>
                            <div class="col-sm-12 col-md-5" style="padding-top: 5px">
                                <input type="checkbox" id="without_active_to" name="<?= $classname . '[without_active_to]'; ?>" value="1" <?= !$model->active_to ? 'checked="checked"' : ''; ?>>
                                <label for="without_active_to"><?= Yii::t('advertisement', 'until date to'); ?></label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>