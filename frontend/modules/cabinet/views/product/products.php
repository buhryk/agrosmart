<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use frontend\modules\cabinet\models\Subsidiary;
use yii\widgets\LinkPager;
use frontend\models\User;
use frontend\modules\product\models\Advertisement;

$this->title = Yii::t('cabinet', Yii::$app->user->identity->company ? 'Company products and services' : 'MY_GOODS_AND_SERVICES');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dialog cabinet-products">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 ">
            <div class="margin-bottom-10">
                <div class="important-info ">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__title">
                            <?=Yii::t('informing', 'My Account - Products') ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="dialog-container dialogs col-md-12">
                <a href="<?= Url::to(['/product/product/create']); ?>" class="btn-green about-container__save">
                    <?= Yii::t('advertisement', 'Add product/service'); ?>
                </a>
                <div class="row">
                    <div class="col-xs-12 message-container">
                        <?php $params = ['model' => $searchModel, 'company' => $company]; ?>
                        <?php if ($company) {
                            $params['companyUsers'] = $companyUsers;
                            $params['subsidiaries'] = $subsidiaries;
                        } ?>
                        <?php echo $this->render('_products_search', $params); ?>
                    </div>
                    <div class="col-xs-12 message-container" style="margin-top: 20px">
                        <div class="table-bordered">
                            <table class="table products-table">
                                <thead>
                                <tr>
                                    <th width="60">ID</th>
                                    <th width="100"><?= Yii::t('common', 'Дата создания'); ?></th>
                                    <th width="60"><?= Yii::t('common', 'Type'); ?></th>

                                    <th <?= $company ? 'width="175"' : ''; ?>><?= Yii::t('common', 'Title'); ?></th>
                                    <th <?= $company ? 'width="150"' : ''; ?>><?= Yii::t('rubric', 'Rubric'); ?></th>
                                    <?php if ($company) { ?>
                                        <th width="100"><?= Yii::t('common', 'Author'); ?></th>
                                        <th width="100"><?= Yii::t('common', 'From name'); ?></th>
                                        <th width="100"><?= Yii::t('common', 'From subsidiary'); ?></th>
                                    <?php } ?>
                                    <th <?= $company ? 'width="50"' : ''; ?>><?= Yii::t('common', 'Status'); ?></th>
                                    <th width="55">&nbsp;</th>
                                </tr>
                                <?php foreach ($models as $model) { ?>
                                    <tr>
                                        <td><?= $model->id; ?></td>
                                        <td><?=date('Y-m-d H:i', $model->created_at) ?></td>
                                        <td><?= $model->detailType; ?></td>
                                        <td>
                                            <a href="<?= Url::to(['update', 'id' => $model->primaryKey]); ?>">
                                                <?= $model->title; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <?= ($rubric = $model->rubric) ? $rubric->title : ''; ?>
                                        </td>
                                        <?php if ($company) { ?>
                                            <td><?= $model->author->fullName; ?></td>
                                            <td>
                                                <?php $from = $model->from_user_id ? User::findOne($model->from_user_id) : null; ?>
                                                <?= $from ? $from->fullName : ''; ?>
                                            </td>
                                            <td>
                                                <?php $subsidiary = $model->from_subsidiary_id ? Subsidiary::findOne($model->from_subsidiary_id) : null; ?>
                                                <?= $subsidiary ? ($subsidiary->getType() . ' - ' . $subsidiary->detailAddress) : ''; ?>
                                            </td>
                                        <?php } ?>
                                        <td><?= ($model->status == Advertisement::STATUS_ACTIVE_YES || (!$model->active_to || ($model->active_to >= time()))) ?
                                                Yii::t('advertisement', 'STATUS_ACTIVE_YES') : Yii::t('advertisement', 'STATUS_ACTIVE_NO'); ?>
                                        </td>
                                        <td>
                                            <a href="<?= Url::to(['update', 'id' => $model->id]); ?>"
                                               title="<?= Yii::t('common', 'Edit'); ?>"
                                               aria-label=" <?= Yii::t('common', 'Edit'); ?>">
                                                <span class='glyphicon glyphicon-pencil'></span>
                                            </a>
                                            <a href="<?= Url::to(['/product/product/view', 'slug' => $model->alias]); ?>"
                                               title="<?= Yii::t('common', 'View'); ?>"
                                               aria-label=" <?= Yii::t('common', 'View'); ?>"
                                            >
                                                <span class='glyphicon glyphicon-eye-open'></span>
                                            </a>
                                            <a href="<?= Url::to(['delete', 'id' => $model->id]); ?>"
                                               title="<?= Yii::t('common', 'Delete'); ?>"
                                               aria-label=" <?= Yii::t('common', 'Delete'); ?>">
                                                <span class='glyphicon glyphicon-trash'></span>
                                            </a>
                                            <a href="<?= Url::to(['/cabinet/pull-top/index', 'model_id' => $model->id, 'model_name' => 'product']); ?>"
                                               title="<?= Yii::t('common', 'Выделяйся'); ?>"
                                               aria-label=" <?= Yii::t('common', 'Выделяйся'); ?>">
                                                <span class='glyphicon glyphicon-thumbs-up'></span>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </thead>
                            </table>
                        </div>

                        <div style="text-align: center">
                            <?php echo LinkPager::widget(['pagination' => $pages]); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>