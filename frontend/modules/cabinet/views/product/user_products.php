<?php
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('cabinet', Yii::$app->user->identity->company ? 'Company products and services' : 'MY_GOODS_AND_SERVICES');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dialog">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 dialog-container dialogs">
            <a href="#" class="btn-green about-container__save">+ добавить товар/услугу</a>
            <div class="row">
                <div class="col-xs-12 dialog-filter-wrapper">
                    <input type="checkbox" id="delete-chosen"><label for="delete-chosen">деактевировать</label>
                    <input type="checkbox" id="all"><label for="all">все</label>
                    <input type="checkbox" id="prod"><label for="prod">товар/услуга</label>
                    <input type="checkbox" id="company"><label for="company">компания</label>
                    <input type="checkbox" id="elev"><label for="elev">элеватор</label>
                    <input type="checkbox" id="deleted"><label for="deleted">удаленные</label>
                </div>
                <div class="col-xs-12 dialog-search">
                    <input class="dialog__search-input input-center" type="text" placeholder="Поиск">
                </div>
                <div class="col-xs-12 message-container">
                    <table class="branches-table col-xs-12">
                        <tr class="grey-row">
                            <td  class="col-xs-2">
                                <input type="checkbox" id="message-all">
                                <label for="message-all" class="message-label">Дата</label>
                            </td>
                            <td class="col-xs-3">
                                <select class="all-select prod-select slct-bn">
                                    <option>По категории</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </td>
                            <td>Заголовок</td>
                            <td class="my-prod-to-top">Сообщение</td>
                        </tr>
                        <tr class="white-row">
                            <td class="col-xs-2">
                                <input type="checkbox" id="message1">
                                <label for="message1" class="message-label">02.04/12:53</label>
                            </td>
                            <td class="col-xs-3">Продам 100 т. кукурудзи первого сорта</td>
                            <td>
                                Херсонская область, с. Городище, ул. Светлая, 1
                                <div class="clearfix"></div>
                                <div class="col-sm-4 prod-link1">
                                    <a href="#" class="serv-link">В избранное</a>
                                </div>
                                <div class="col-sm-4 prod-link2">
                                    <a href="#" class="serv-link">Запомнить адрес</a>
                                </div>
                                <div class="col-sm-4 prod-link3">
                                    <a href="#" class="serv-link">Жалоба</a>
                                </div>
                            </td>
                            <td>
                                <a href="#"><img src="images/letter.png" alt="letter"></a>
                                <a href="#">Поднять в топ</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-12 message-pagination">
                    <span class="message-pagination__text">Выводить по:</span>
                    <select class="message-pagination__select all-select">
                        <option>5</option>
                        <option>10</option>
                        <option>20</option>
                    </select>
                    <span class="message-pagination__text text-cen">Показано 1-10 из 24</span>
                    <ul class="pagination">
                        <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">2 <span class="sr-only">(current)</span></a></li>
                        <li class="disabled"><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>