<?php
use yii\widgets\Breadcrumbs;
use frontend\assets\NewAdvertisementAsset;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use frontend\assets\CabinetAdvertisementAsset;
CabinetAdvertisementAsset::register($this);

$this->title = $advertisement->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', Yii::$app->user->identity->company ?
    'Company products and services' : 'MY_GOODS_AND_SERVICES'), 'url' => ['/cabinet/product/index']];
$this->params['breadcrumbs'][] = $this->title;
$getCityUrl = \yii\helpers\Url::to(['/city-list/index']);
$action = Yii::$app->controller->action->id;
$messageRemoveImageConfirm = Yii::t('advertisement', 'Are you sure you want to delete this product image?')
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dialog cabinet-edit-product-images">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 dialog-container dialogs" style="padding: 30px 15px 15px 15px;">
            <div style="margin-bottom: 20px;">
                <a href="<?= Url::to(['update', 'id' => $advertisement->primaryKey]); ?>" class="btn btn-success" style="border-radius: 0">
                    < <?= Yii::t('advertisement', 'Edit product'); ?>
                </a>
            </div>

            <?php if (count($images) < 6) { ?>
                <div class="product-new-image-block" style="padding-bottom: 20px;">

                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'id' => 'new-product-image-form',
                            'enctype' => 'multipart/form-data'
                        ]
                    ]); ?>

                    <div>
                        <?= $form->field($newImage, 'image')->fileInput()
                            ->label(Yii::t('advertisement', 'Upload additional image'));
                        ?>
                        <div><?= Html::submitButton(Yii::t('common', 'Upload'), ['class' => 'btn btn-primary']) ?></div>
                    </div>

                    <div class="clearfix"></div>
                    <?php ActiveForm::end(); ?>
                </div>
            <?php } ?>

            <div class="self-advertisement-image-block">
                <div id="blueimp-gallery" class="blueimp-gallery">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>

                <table>
                    <tbody id="links" style="width: 100%;">
                    <?php foreach ($images as $key => $image) { ?>
                        <?php if (($key % 3) == 0 || $key == 0) { ?>
                            <tr style="width: 100%;">
                        <?php } ?>
                        <td width="33%" style="padding-bottom: 30px; max-width: 33%">
                            <a href="<?= '/'.$image->path; ?>" title="Banana" class="col-sm-6 col-md-4" style="padding-bottom: 15px; width: 100%;">
                                <img src="<?= '/'.$image->path; ?>" alt="Banana" style="width: 100%;">
                            </a>
                            <div class="clearfix"></div>

                            <div href="<?= Url::to(['remove-image', 'id' => $image->primaryKey]); ?>"
                                 class="remove-image remove-product-image"
                                 style="text-align: center; display: block"
                                 data-image-id="<?= $image->primaryKey; ?>"
                                 data-message-confirm="<?= $messageRemoveImageConfirm; ?>"
                            >
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;<?= Yii::t('advertisement', 'Remove image'); ?>
                            </div>
                        </td>
                        <?php if (($key % 3) == 2 || ($key == (count($images)-1))) { ?>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>