<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\rubric\models\Rubric;
use frontend\modules\product\models\Advertisement;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\modules\product\models\AdvertisementSearch */
/* @var $form yii\widgets\ActiveForm */

if (isset($company) && $company) {
    $advertisementsQuery = Advertisement::find()
        ->where(['company_id' => $company->primaryKey])
        ->andWhere(['status' => Advertisement::STATUS_ACTIVE_YES])
        ->groupBy('rubric_id')
        ->all();
} else {
    $advertisementsQuery = Advertisement::find()
        ->where(['author_id' => Yii::$app->user->identity->id])
        ->andWhere(['status' => Advertisement::STATUS_ACTIVE_YES])
        ->groupBy('rubric_id')
        ->all();
}
$rubrics = Rubric::find()
    ->where(['in', Rubric::tableName().'.id', ArrayHelper::getColumn($advertisementsQuery, 'rubric_id')])
    ->joinWith('lang')
    ->all();
$classname = \yii\helpers\StringHelper::basename(get_class($model));
?>

<div class="advertisement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-6 col-md-4">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-sm-6 col-md-4">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?php echo $form->field($model, 'rubric_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($rubrics, 'id', 'title'),
                'options' => [
                    'placeholder' => '...'
                ],
                'pluginOptions' => ['allowClear' => true]
            ])->label(Yii::t('rubric', 'Rubric')); ?>
        </div>
    </div>
    <?php if (isset($company) && $company) { ?>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <?php echo $form->field($model, 'author_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($companyUsers, 'id', 'fullname'),
                    'options' => [
                        'placeholder' => '...'
                    ],
                    'pluginOptions' => ['allowClear' => true]
                ])->label(Yii::t('common', 'Author')); ?>
            </div>
            <div class="col-sm-6 col-md-4">
                <?php echo $form->field($model, 'from_user_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($companyUsers, 'id', 'fullname'),
                    'options' => [
                        'placeholder' => '...'
                    ],
                    'pluginOptions' => ['allowClear' => true]
                ])->label(Yii::t('common', 'From name')); ?>
            </div>
            <div class="col-sm-6 col-md-4">
                <?php echo $form->field($model, 'from_subsidiary_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($subsidiaries, 'id', 'info'),
                    'options' => [
                        'placeholder' => '...'
                    ],
                    'pluginOptions' => ['allowClear' => true]
                ])->label(Yii::t('common', 'From subsidiary')); ?>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <?php echo $form->field($model, 'type')->widget(Select2::classname(), [
                'data' => Advertisement::getTypes(),
                'options' => [
                    'prompt' => '...',
                ],
                'pluginOptions' => ['allowClear' => true]
            ]); ?>
        </div>
        <div class="col-sm-6 col-md-4">
            <?php echo $form->field($model, 'status')->widget(Select2::classname(), [
                'data' => [
                    Advertisement::STATUS_ACTIVE_YES => Yii::t('advertisement', 'STATUS_ACTIVE_YES'),
                    Advertisement::STATUS_ACTIVE_NO => Yii::t('advertisement', 'STATUS_ACTIVE_NO')
                ],
                'options' => [
                    'prompt' => '...',
                ],
                'pluginOptions' => ['allowClear' => true]
            ]); ?>
        </div>
        <div class="col-sm-6 col-md-4">
            <label class="control-label"><?= Yii::t('common', 'Date created at'); ?></label>
            <?= DatePicker::widget([
                'name' => $classname.'[date_create_from]',
                'value' => $model->date_create_from,
                'type' => DatePicker::TYPE_RANGE,
                'separator' => '<i class="glyphicon glyphicon-calendar"></i>',
                'name2' => $classname.'[date_create_to]',
                'value2' => $model->date_create_to,
                'layout' => '{input1}{separator}{input2}<span class="input-group-addon kv-date-remove"><i class="glyphicon glyphicon-remove"></i></span>',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]);
            ?>
            <?php if (isset($model->getErrors()['date_create_from'])) { ?>
                <div class="help-block" style="color: #a94442;">
                    <?= implode(', ', $model->getErrors()['date_create_from']); ?>
                </div>
            <?php } elseif (isset($model->getErrors()['date_create_to'])) { ?>
                <div class="help-block" style="color: #a94442;">
                    <?= implode(', ', $model->getErrors()['date_create_to']); ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="form-group" style="margin-top: 15px; text-align: center">
        <?= Html::submitButton(Yii::t('common', 'Lets search'), ['class' => 'btn btn-primary', 'style' => 'background: #71bf44; border: none;']) ?>
        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default" style="text-decoration: none;">
            <?= Yii::t('advertisement', 'Reset filter'); ?>
        </a>
    </div>

    <?php ActiveForm::end(); ?>

</div>