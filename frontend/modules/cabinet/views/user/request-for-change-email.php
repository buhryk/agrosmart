

<?php
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('cabinet', 'Sent a request to change the email');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 m-profile" style="padding-top: 15px; margin-bottom: 30px; text-align: center">
            <h3><?= Yii::t('cabinet', 'To your old email has been sent a letter with a request to the email change. click the link to confirm your email change'); ?></h3>
        </div>
    </div>
</div>