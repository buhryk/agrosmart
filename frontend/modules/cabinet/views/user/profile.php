<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = \Yii::t('cabinet', 'MY_PROFILE');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 m-profile" style="padding-top: 15px;">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h2><?= Yii::t('cabinet', 'CONTACT_DATA'); ?></h2>
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn-green']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
                <?php /* ?>
                <div class="col-md-6">
                    <a href="<?=Url::to(['delete']) ?>"
                       class="btn btn-danger pull-right"
                       onclick="return confirm('<?=Yii::t('cabinet', 'Вы уверены што хотите удалить свой профиль') ?>')"
                    >
                        <?=Yii::t('cabinet', 'Удалить профиль') ?>
                    </a>
                </div>
                <?php */ ?>

            </div>

        </div>
    </div>
</div>