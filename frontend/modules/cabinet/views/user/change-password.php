<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('cabinet', 'Password changing');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 m-profile" style="padding-top: 15px;">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h2><?= $this->title; ?></h2>
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'old_password')->textInput(['type' => 'password']) ?>
                    <?= $form->field($model, 'new_password')->textInput(['type' => 'password']) ?>
                    <?= $form->field($model, 'repeat_new_password')->textInput(['type' => 'password']) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('cabinet', 'Обновить'), ['class' => 'btn-green']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>
    </div>
</div>