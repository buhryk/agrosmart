<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\modules\cabinet\models\Subsidiary;

use kartik\widgets\Select2;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\Subsidiary */
/* @var $form yii\widgets\ActiveForm */

$url = \yii\helpers\Url::to(['/city-list/index']);
?>
<div class="modal-body">
    <div class="subsidiary-form">
        <?php $form = ActiveForm::begin([
            'id'=>'add-subsidiary',
            'enableAjaxValidation' => true,
            'options' =>
                ['data-pjax' => true]
            ]
        ); ?>

        <?= $form->field($model, 'city_id')->widget(Select2::className(), [
            'initValueText' => !empty($model->city) ? $model->city->title : '', // set the initial display text
            'options' => ['placeholder' => 'Ведите город'],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 2,
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],

        ]); ?>

        <?= $form->field($model, 'type')->dropDownList($model->getListType()) ?>
        <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder'=> Yii::t('cabinet', 'Ведите адрес (адрес должен быть корректным)')]) ?>
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder'=> Yii::t('cabinet', 'Ведите телефон')]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder'=> Yii::t('cabinet', 'Ведите email')]) ?>

        <div class="form-group">
            <?= Html::button($model->isNewRecord ? Yii::t('cabinet', 'ADD_SUBSIDIARY') : Yii::t('cabinet', 'UPDATE'), [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                'type' => 'submit'
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>