<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\Subsidiary */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'Subsidiaries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subsidiary-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('cabinet', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('cabinet', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('cabinet', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'city_id',
            'company_user_id',
            'type',
            'address',
            'phone',
            'email:email',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
