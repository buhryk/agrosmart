<?php

use yii\helpers\Html;
?>
<div class="modal-dialog modal-ms">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title custom-font"><?= Html::encode($this->title) ?></h3>
        </div>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
