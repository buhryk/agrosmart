<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('cabinet', 'SUBSIDIARIES_AND_CONTACTS');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'COMPANY_PROFILE'), 'url' => ['default/company-profile']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?=$this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/"><?=Yii::t('common', 'Главная') ?></a></li>
                <li class="active"><?=$this->title ?></li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <p>&nbsp;</p>
        <div class="row">
            <div class="col-sm-3" id="sidebar">
                <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                    <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
                </div>
            </div>
            <div class="col-sm-9" data-content="page_content">
                <!-- important info -->
                <div class="col-xs-12 important-info">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__title"><?=Yii::t('cabinet','Вносите в таблицу все филиалы вашей компании с 
                            адресами и контактами. Потом вы сможете использовать 
                            разные контакты для связи в товарах и услугах') ?></p>
                    </div>
                </div>
                <div class="col-xs-12 branches">
                    <div class="row">
                        <div class="branches-table-wrapper">

                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                //'filterModel' => $searchModel,
                                'layout' => "{items}\n{summary}\n{pager}",
                                'columns' => [
                                    [
                                        'attribute' => 'city_id',
                                        //'filter' =>Region::getList(),
                                        'value'=>function($data){
                                            return $data->city->title;
                                        },
                                    ],
                                    [
                                        'attribute' => 'type',
                                        //'filter' => Html::activeDropDownList($searchModel, 'type', Subsidiary::getSubsidiaryTypeDropdown(), ['class' => 'form-control', 'multiple' => false]),
                                        'value' => function ($model) {
                                            return isset($model->listType[$model->type]) ? $model->listType[$model->type] : 'Undefined';
                                        },
                                    ],
                                    'address',
                                    'phone',
                                    'email:email',
                                    [

                                        'format'=>'raw',
                                        //'filter' => Html::activeDropDownList($searchModel, 'type', Subsidiary::getSubsidiaryTypeDropdown(), ['class' => 'form-control', 'multiple' => false]),
                                        'value' => function ($model) {
                                            return "<a href='".\yii\helpers\Url::to(['update', 'id'=>$model->id])."' title='Редактировать' aria-label='Редактировать' data-pjax='0' class='modalButton'><span class='glyphicon glyphicon-pencil'></span></a>";
                                        },
                                    ],
                                ],
                                'tableOptions'=>['class'=>'branches-table col-xs-12"'],

                            ]); ?>
                        </div>

                        <?= Html::a('+ '.Yii::t('cabinet', 'ADD_SUBSIDIARY'), ['create'], ['class' => 'btn-green about-container__save modalButton']) ?>
                       
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

