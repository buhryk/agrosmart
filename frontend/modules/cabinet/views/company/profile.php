<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use backend\modules\rubric\models\Rubric;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;

$this->title = \Yii::t('cabinet', 'COMPANY_PROFILE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
    "$('#choose-logo').on('click', function() { $('#choose-logo-file').click(); });
    
    ",
    \yii\web\View::POS_READY,
    'my-button-handler'
);
?>

<div class="bread">
    <h1><?= $this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9" data-content="page_content">
            <!-- important-info -->
            <div class="col-xs-12 important-info">
                <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                <?=\Yii::t('informing','Cabinet company profile') ?>
            </div>
            <!-- /important-info -->
            <div class="col-xs-12 mt40 ap-wrapper pl35 pr35 mb40">
                <div class="ap-container">
                    <div class="row">
                        <?php $form = ActiveForm::begin([
                            'id' => 'form-company-profile',
                            'options' => [
                                'enctype' => 'multipart/form-data'
                            ]
                        ]); ?>
                        <div class="col-xs-12">
                            <p class="ap-form-price__title mt40 mb25"><?= $company->getAttributeLabel('name'); ?></p>
                        </div>
                        <div class="col-sm-7 col-xs-12">
                            <?= $form->field($company, 'name')->textInput([
                                'autofocus' => false,
                                'value' => $company->name,
                                'placeholder' => \Yii::t('cabinet', 'ENTER_COMPANY_NAME')
                            ])->label(false) ?>
                        </div>
                        <div class="col-sm-5 col-xs-12 load-com-logo" style="text-align: center">
                            <?php if ($company->logotype) { ?>
                               <?php
                                    echo Html::a(Html::img('/images/important-close.png', ['alt' => 'important-close']),
                                        ['deletelogo'],
                                        [
                                            'data' => [
                                                'method' => 'post',
                                                'params' => [
                                                    'company_id' => $company->id,
                                                ],
                                            ],
                                            ]
                                );
                                ?>
                                <?php echo Html::img('/'.$company->logotype, ['width' => 120, 'style' => 'margin-bottom: 15px;']); ?>
                            <?php } ?>

                            <p class="text_grey" style="display: block!important;">
                                <label for="add-file" class="btn-green" id="choose-logo" style="cursor: pointer; padding: 7.5px 30px;">Загрузить логотип</label>
                            </p>
                            <div class="company-image-name"></div>

                            <?php echo $form->field($company, 'logo_file')->fileInput([
                                'id' => 'choose-logo-file',
                                'style' => 'display: none;'
                            ])->label(false) ?>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-xs-12">
                            <p class="ap-form-price__title mt35"><?= $company->getAttributeLabel('short_description'); ?>:</p>
                            <?= $form->field($company, 'short_description')->widget(Widget::className(), [
                                'settings' => [
                                    'language' => \Yii::$app->language,
                                    'minHeight' => 200,
                                    'imageUpload' => Url::to(['/cabinet/company/image-upload']),
                                    'plugins' => [
                                        'fullscreen',
                                    ]
                                ]
                            ])->label(false) ?>
                        </div>
                        <div class="col-xs-12">
                            <p class="ap-form-price__title mt35"><?= $company->getAttributeLabel('full_description'); ?>:</p>
                            <?= $form->field($company, 'full_description')->widget(Widget::className(), [
                                'settings' => [
                                    'language' => \Yii::$app->language,
                                    'minHeight' => 300,
                                    'imageUpload' => Url::to(['/cabinet/company/image-upload']),
                                    'plugins' => [
                                        'fullscreen',
                                    ]
                                ]
                            ])->label(false) ?>
                        </div>
                        <div class="col-xs-12">
                            <p class="ap-form-price__title m0 mt40"><?=Yii::t('cabinet', 'Сферы деятельности') ?>:</p>
                        </div>

                        <?php foreach (Rubric::getList() as $key=>$value):?>
                            <div class="col-xs-6 col-md-4 item-rubric">
                                <p class="bold fs16 black mt30">

                               <input type="checkbox" class="an_chekbox_rubric" id="<?=$key ?>">
                                    <label class="text-align" for="<?=$key ?>">   <?=$key ?></label>
                                </p>
                                <?=Html::checkboxList('rubric_id', ArrayHelper::getColumn($company->rubric, 'rubric_id') ,$value, [
                                    'item' => function($index, $label, $name, $checked, $value){
                                        $check = $checked ? ' checked="checked"' : '';
                                        return "<div><input type=\"checkbox\" class=\"an_chekbox\" name=\"$name\" value=\"$value\" id=\"$name$value\" $check><label class='text-align' for=\"$name$value\">$label</label></div>";
                                    }]) ?>
                            </div>
                        <?php endforeach; ?>

                        <div class="col-xs-12 tac mt40 mb40">
                            <?= Html::submitButton(\Yii::t('cabinet', 'UPDATE'), [
                                'class' => 'btn-green tdn',
                                'name' => 'update-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
