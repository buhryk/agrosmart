<?php
use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use backend\modules\tariff\models\Tariff;
use frontend\modules\cabinet\models\PullTop;

$this->title = Yii::t('common', 'Pull top');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
$action = Yii::$app->controller->action->id;

$map = function ($items){
    $topItems = [];
    foreach ($items as $item) {
        $topItems[$item->id] = $item->count_deys.' '.Yii::t('cabinet', 'дней');
    }
    return $topItems;
}

?>

<div class="bread">
    <h1><?=$this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<?php Pjax::begin() ?>
<?php if(Yii::$app->request->get('type') =='new'): ?>
    <div class="ap-success-container">
        <div class="ap-success-container__message" role="alert">
            <?=Yii::t('cabinet', 'Объявление опубликовано!') ?>
        </div>
    </div>

<?php endif; ?>
<div class="container-fluid pays pay-check">
    <div class="container">
        <div class="row">
            <?php $form = ActiveForm::begin(['method'=>'get',
                        'id' => 'form-pull-top',
                        'action' => [$action, 'model_name' => $model->model_name, 'model_id' => $model->model_id],
                        'options'=>[
                            'data-pjax'=>'#x1g'
                         ],]) ;
            ?>
            
            <div class="col-sm-12"><span class="step-way"><?=Yii::t('common', 'Select a service') ?>:</span></div>
            <div class="col-sm-4">
                <div class="radio">
                    <label>
                        <div class="form-group field-pulltop-type">
                            <div class="radio">
                                <input type="radio" id="pulltop-type" name="PullTop[type]" value="<?=PullTop::TYPE_TOP ?>"   <?=$model->type == PullTop::TYPE_TOP ? 'checked' : ''?>>
                            </div>
                        </div>
                        <br>
                        <?=Yii::t('common', 'Picking up the list')?>
                    </label>
                </div>
                <div class="date-price">
                   <?=$form->field($model, 'top')->dropDownList($map($model->allottedList))->label(false)?>

                    <?php if($model->top): ?>
                        <span class="price"><?=$model->topModel->price ?><span class="value-s">грн</span></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="radio">
                    <label>

                        <div class="form-group field-pulltop-type">
                            <div class="radio">
                                <input type="radio"
                                       id="pulltop-type"
                                       name="PullTop[type]"
                                       value="<?=PullTop::TYPE_ALLOTTED ?>"
                                    <?=$model->type == PullTop::TYPE_ALLOTTED ? 'checked' : ''?>>
                            </div>
                        </div>
                        <br>
                        <?=Yii::t('common', 'Highlight in yellow') ?>
                    </label>
                </div>
                <div class="date-price">
                    <?=$form->field($model, 'allotted')->dropDownList($map($model->allottedList))->label(false)?>

                    <?php if($model->allotted): ?>
                        <span class="price"><?=$model->allottedModel->price ?><span class="value-s">грн</span></span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="radio">
                    <label>
                        <div class="form-group field-pulltop-type">
                            <div class="radio">
                                <input type="radio" id="pulltop-type" name="PullTop[type]" value="<?=PullTop::TYPE_COMPLEX ?>"   <?=$model->type == PullTop::TYPE_COMPLEX ? 'checked' : ''?>>
                            </div>
                        </div>
                        <br>
                        <?=Yii::t('common', 'Выделите желтый и поднять в топ') ?>
                    </label>
                </div>
                <div class="date-price">
                    <?=$form->field($model, 'complex')->dropDownList($map($model->complexList))->label(false)?>

                    <?php if($model->complex): ?>
                        <span class="price"><?=$model->complexModel->price ?><span class="value-s">грн</span></span>
                    <?php endif; ?>
                </div>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
    </div>
 </div>

<?php if($model->currentModel): ?>
<div class="container-fluid pays pay-service">
    <div class="container">
        <div class="row">
            <span class="step-way"><?=Yii::t('common', 'Pay') ?></span>
            <div class="total-block">
            <span class="total-name"><?=Yii::t('common', 'Total payment') ?>:</span>
            <span class="total-price"><?=$model->currentModel->price ?><span class="value-t">грн</span></span>
            <a href="<?=\yii\helpers\Url::to([$action, 'model_name' => $model->model_name, 'model_id' => $model->model_id]) ?>">Отмена</a>
            <?=$model->button ?>
        </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php Pjax::end(); ?>

<?php
$this->registerJs("
    $('body').on('click','#form-pull-top input', function(){
        $('#form-pull-top').submit();
    });
    
    $('body').on('change','#form-pull-top select', function(){
        $('#form-pull-top').submit();
    });

");
?>
