<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\AccountingAsset;

AccountingAsset::register($this);
/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\Accounting */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="margin-bottom-10">
    <div class="important-info ">
        <img class="important-info__i" src="/images/important-i.png" alt="important-i">
        <img class="important-info__close" src="/images/important-close.png" alt="important-close">
        <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
        <div class="important-info-content">
            <p class="important-info__title">
                <?=Yii::t('informing', 'Personal account - Transaction calculator') ?>
            </p>
        </div>
    </div>
</div>

<div class="col-xs-12 calc-container">
    <div class="accounting-form">
        <?php $form = ActiveForm::begin(['id'=>'form-accounting']); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'responsible')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'product')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'buyers')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'basis_delivery')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'seller')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'basis_purchase')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'elevator')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'logist')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'numbercars_per_day')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'fpproval_date_line')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'count_day')->textInput(['maxlength' => true, 'class' => 'days-count']) ?>
            </div>
        </div>
        <div class="table-container">
            <table class="branches-table little col-xs-12 text-center">
                <thead class="thead-green">
                <tr class="grey-row">
                    <td rowspan="2" colspan="2"><?=Yii::t('cabinet/accounting', 'Валовый доход') ?> </td>
                    <td rowspan="2" style="width: 15%"><?=Yii::t('cabinet/accounting', 'Кол-во, т') ?></td>
                    <td rowspan="2"><?=Yii::t('cabinet/accounting', 'Ст-сть ед., грн/т') ?> </td>
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'в т.ч.') ?></td>
                    <td rowspan="2"><?=Yii::t('cabinet/accounting', 'Сумма, грн.') ?></td>
                </tr>
                <tr class="grey-row">
                    <td >НДС</td>
                    <td >без НДС</td>
                </tr>
                </thead>
                <tbody class="sum-items">
                <tr class="row-a">
                    <td colspan="2"> </td>
                    <td class="input">
                        <?= $form->field($model, 'count')->textInput(['class' => 'a1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'price')->textInput(['class' => 'a2'])->label(false) ?>
                    </td>
                    <td class="a3"></td>
                    <td class="a4"></td>
                    <td class="a5">0</td>
                </tr>
                </tbody>
                <thead class="thead-green">
                <tr class="grey-row">
                    <td rowspan="2" colspan="2"><?=Yii::t('cabinet/accounting', 'Валовые расходы ') ?></td>
                    <td rowspan="2"><?=Yii::t('cabinet/accounting', 'Кол-во, т') ?></td>
                    <td rowspan="2"><?=Yii::t('cabinet/accounting', 'Ст-сть ед., грн/т') ?> </td>
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'в т.ч.') ?></td>
                    <td rowspan="2"><?=Yii::t('cabinet/accounting', 'Сумма, грн.') ?></td>
                </tr>
                <tr class="grey-row">
                    <td >НДС</td>
                    <td >без НДС</td>
                </tr>
                </thead>
                <tbody class="sum-items">
                <tr class="row-b">
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'Стоимость (EXW)') ?> </td>
                    <td class="input">
                        <?= $form->field($model, 'exw_count')->textInput(['class' => 'b1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'exw_price')->textInput(['class' => 'b2'])->label(false) ?>
                    </td>
                    <td class="b3"></td>
                    <td class="b4"></td>
                    <td class="b5 total-sum n-needs">0</td>
                </tr>
                <tr class="row-c">
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'Бонус менеджера') ?>  </td>
                    <td class="input">
                        <?= $form->field($model, 'meneger_count')->textInput(['class' => 'c1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'meneger_price')->textInput(['class' => 'c2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="c5 total-sum n-needs">0</td>
                </tr>
                <tr class="row-d">
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'Командировочные') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'traveling_expenses_count')->textInput(['class' => 'd1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'traveling_expenses_price')->textInput(['class' => 'd2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="d5 total-sum n-needs">0</td>
                </tr>

                <tr class="row-e">
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'Бонус контрагента') ?> </td>
                    <td class="input">
                        <?= $form->field($model, 'counterparty_count')->textInput(['class' => 'e1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'counterparty_price')->textInput(['class' => 'e2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="e5 total-sum n-needs">0</td>
                </tr>

                <tr class="row-f">
                    <td colspan="2">Биржа брокер </td>
                    <td class="input">
                        <?= $form->field($model, 'broker_count')->textInput(['class' => 'f1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'broker_price')->textInput(['class' => 'f2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="f5 total-sum n-needs">0</td>
                </tr>

                <tr class="row-g">
                    <td colspan="1" class="row-g_1"><?=Yii::t('cabinet/accounting', 'Услуги элеватора по хранению, дней') ?></td>
                    <td class="input row-g_input">
                        <?= $form->field($model, 'elevator_days')->textInput(['class' => 'g0'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'elevator_count')->textInput(['class' => 'g1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'elevator_price')->textInput(['class' => 'g2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="g5 total-sum n-needs">0</td>
                </tr>

                <tr class="row-h">
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'Услуги элеватора, приемка') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'elevator_service_count')->textInput(['class' => 'h1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'elevator_service_price')->textInput(['class' => 'h2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="h5 total-sum n-needs">0</td>
                </tr>


                <tr class="row-i">
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'Услуги элеватора, отгрузка') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'elevator_shipment_days')->textInput(['class' => 'i1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'elevator_shipment_price')->textInput(['class' => 'i2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="i5 total-sum n-needs">0</td>
                </tr>

                <tr class="row-j">
                    <td colspan="1"><?=Yii::t('cabinet/accounting', 'Услуги элеватора, перепись, шт ') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'elevator_census_days')->textInput(['class' => 'j0'])->label(false) ?>
                    </td>
                    <td>  </td>
                    <td class="input">
                        <?= $form->field($model, 'elevator_census_price')->textInput(['class' => 'j2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="j5 total-sum n-needs">0</td>
                </tr>

                <tr class="row-k">
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'Логистические затраты (авто) Ф1') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'logistics_costs_f1_count')->textInput(['class' => 'k1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'logistics_costs_f1_price')->textInput(['class' => 'k2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="k5 total-sum n-needs">0</td>
                </tr>

                <tr class="row-l">
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'Логистические затраты (авто) Ф2') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'logistics_costs_f2_count')->textInput(['class' => 'l1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'logistics_costs_f2_price')->textInput(['class' => 'l2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="l5 total-sum n-needs">0</td>
                </tr>

                <tr class="row-m">
                    <td colspan="2"><?=Yii::t('cabinet/accounting', 'Логистические затраты (жд)') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'logistics_costs_count')->textInput(['class' => 'm1'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'logistics_costs_price')->textInput(['class' => 'm2'])->label(false) ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td class="m5 total-sum n-needs">0</td>
                </tr>

                <tr class="row-n">
                    <td colspan="1"><?=Yii::t('cabinet/accounting', 'Естественная убыль') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'natural_decline')->textInput(['class' => 'n0'])->label(false) ?>

                    </td>
                    <td class="input">
                        <?= $form->field($model, 'natural_count')->textInput(['class' => 'n1'])->label(false) ?>
                    </td>
                    <td></td>
                    <td class="n2">0</td>
                    <td></td>
                    <td class="n3">0</td>
                </tr>

                <tr class="row-o">
                    <td colspan="1"><?=Yii::t('cabinet/accounting', 'Налоговая нагрузка') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'tax_burden_percent')->textInput(['class' => 'o0'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'tax_burden_percent')->textInput(['class' => 'o1'])->label(false) ?>
                    </td>
                    <td></td>
                    <td class="o2"></td>
                    <td></td>
                    <td class="o3 total-sum">0</td>
                </tr>

                <tr class="row-p">
                    <td colspan="1"><?=Yii::t('cabinet/accounting', 'Upgrade ') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'upgrade_count')->textInput(['class' => 'p0'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'upgrade_price')->textInput(['class' => 'p1'])->label(false) ?>
                    </td>
                    <td></td>
                    <td class="p2"></td>
                    <td></td>
                    <td class="p3 total-sum">0</td>
                </tr>
                <tr class="row-q">
                    <td colspan="1"><?=Yii::t('cabinet/accounting', 'Стоимость денег %/год ') ?></td>
                    <td class="input">
                        <?= $form->field($model, 'cost_money_percent')->textInput(['class' => 'q0'])->label(false) ?>
                    </td>
                    <td class="input">
                        <?= $form->field($model, 'cost_money_count')->textInput(['class' => 'q1'])->label(false) ?>
                    </td>
                    <td></td>
                    <td class="q2">0</td>
                    <td></td>
                    <td class="q3 total-sum">0</td>
                </tr>
                </tbody>
                <thead class="thead-green">
                <tr class="grey-row">
                    <td rowspan="2" colspan="2">
                        <?=Yii::t('cabinet/accounting', 'Финансовые результаты') ?>
                    </td>
                    <td rowspan="2">
                        <?=Yii::t('cabinet/accounting', 'Кол-во, т') ?>
                    </td>
                    <td rowspan="2">
                        <?=Yii::t('cabinet/accounting', 'Ст-сть ед., грн/т') ?>
                    </td>
                    <td colspan="2">
                        <?=Yii::t('cabinet/accounting', 'в т.ч.') ?>
                    </td>
                    <td rowspan="2">
                        <?=Yii::t('cabinet/accounting', 'Сумма, грн.') ?>
                    </td>
                </tr>
                <tr class="grey-row">
                    <td >НДС</td>
                    <td >без НДС</td>
                </tr>
                </thead>
                <tbody>
                <tr class="row-r">
                    <td colspan="2" class="thead-green"><strong>
                            <?=Yii::t('cabinet/accounting', 'Валовые расходы') ?>
                        </strong></td>
                    <td class="r1"></td>
                    <td class="r2">0</td>
                    <td class="r3"></td>
                    <td class="r4"></td>
                    <td class="r5"></td>
                </tr>
                <tr class="row-s">
                    <td colspan="2" class="thead-green"><strong>
                        <?=Yii::t('cabinet/accounting', 'Валовые доходы') ?>
                            <strong>
                    </td>
                    <td class="s1"></td>
                    <td class="s2"></td>
                    <td class="s3"></td>
                    <td class="s4"></td>
                    <td class="s5"></td>
                </tr>
                <tr class="mt35"></tr>
                <tr>
                    <td colspan="2" class="thead-green">
                        <?=Yii::t('cabinet/accounting', 'Потребность в денежных средствах') ?>
                    </td>
                    <td class="t1 pink-color" colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2" class="thead-green">
                        <?=Yii::t('cabinet/accounting', 'Реализация') ?>
                    </td>
                    <td class="u1"></td>
                    <td class="u2"></td>
                </tr>
                <tr>
                    <td colspan="2" class="thead-green">
                        <?=Yii::t('cabinet/accounting', 'Себестоимость') ?>
                    </td>
                    <td class="v1"></td>
                    <td class="v2"></td>
                </tr>
                <tr>
                    <td colspan="2" class="thead-green">
                        <?=Yii::t('cabinet/accounting', 'Затраты') ?>
                    </td>
                    <td class="w1"></td>
                    <td class="w2"></td>
                </tr>
                <tr>
                    <td colspan="2" class="thead-green">
                        <?=Yii::t('cabinet/accounting', 'Маржинальный доход') ?>
                     </td>
                    <td class="x1"></td>
                    <td class="x2"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td class="y1"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td class="z1"></td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('cabinet', 'Сохранить'), ['class' => 'btn-green about-container__save']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>


<? /* ?>
<div class="col-xs-12 calc-container">
    <div class="accounting-form">

        <?php $form = ActiveForm::begin(['id'=>'form-accounting']); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'responsible')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'product')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'buyers')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'basis_delivery')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'seller')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'basis_purchase')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'elevator')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'logist')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'numbercars_per_day')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'fpproval_date_line')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'count_day')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <table class="branches-table little col-xs-12 text-center">
            <thead class="thead-green">
                <tr class="grey-row">
                    <td rowspan="2" colspan="2">Валовый доход </td>
                    <td rowspan="2">Кол-во, т</td>
                    <td rowspan="2"> Ст-сть ед., грн/т</td>
                    <td colspan="2">в т.ч.</td>
                    <td rowspan="2">Сумма, грн.</td>
                </tr>
                <tr class="grey-row">
                    <td >НДС</td>
                    <td >без НДС</td>
                </tr>
            </thead>
            <tbody>
                <tr >
                    <td colspan="2"> </td>
                    <td class="input"> <?= $form->field($model, 'count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td><?=round($model->nds, 2) ?></td>
                    <td><?=round($model->notNds, 2) ?></td>
                    <td><?=round($model->sum, 2) ?></td>
                </tr>
            </tbody>
            <thead class="thead-green">
                <tr class="grey-row">
                    <td rowspan="2" colspan="2">Валовые расходы </td>
                    <td rowspan="2">Кол-во, т</td>
                    <td rowspan="2"> Ст-сть ед., грн/т</td>
                    <td colspan="2">в т.ч.</td>
                    <td rowspan="2">Сумма, грн.</td>
                </tr>
                <tr class="grey-row">
                    <td >НДС</td>
                    <td >без НДС</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2">Стоимость (EXW) </td>
                    <td class="input"> <?= $form->field($model, 'exw_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'exw_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td><?=round($model->exwNds, 2) ?></td>
                    <td><?=round($model->exwNotNds, 2) ?></td>
                    <td><?=round($model->exwSum, 2) ?></td>
                </tr>
                <tr>
                    <td colspan="2">Бонус менеджера </td>
                    <td class="input"> <?= $form->field($model, 'meneger_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'meneger_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->menegerSum, 2) ?></td>
                </tr>
                <tr>
                    <td colspan="2">Командировочные</td>
                    <td class="input"> <?= $form->field($model, 'traveling_expenses_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'traveling_expenses_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->travelingExpensesSum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="2">Бонус контрагента </td>
                    <td class="input"> <?= $form->field($model, 'counterparty_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'counterparty_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->counterpartySum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="2">Биржа брокер </td>
                    <td class="input"> <?= $form->field($model, 'broker_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'broker_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->brokerSum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="1">Услуги элеватора по хранению, дней </td>
                    <td class="input"><?= $form->field($model, 'elevator_days')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td class="input"> <?= $form->field($model, 'elevator_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'elevator_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->elevatorSum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="2">Услуги элеватора, приемка</td>
                    <td class="input"> <?= $form->field($model, 'elevator_service_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'elevator_service_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->elevatorServiceSum, 2) ?></td>
                </tr>


                <tr>
                    <td colspan="2">Услуги элеватора, отгрузка</td>
                    <td class="input"> <?= $form->field($model, 'elevator_shipment_days')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'elevator_shipment_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->elevatorShipmentSum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="1">Услуги элеватора, перепись, шт </td>
                    <td class="input"><?= $form->field($model, 'elevator_census_days')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td>  </td>
                    <td class="input"> <?= $form->field($model, 'elevator_census_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->elevatorCensusSum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="2">Логистические затраты (авто) Ф1</td>
                    <td class="input"> <?= $form->field($model, 'logistics_costs_f1_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'logistics_costs_f1_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->logisticsF1Sum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="2">Логистические затраты (авто) Ф2</td>
                    <td class="input"> <?= $form->field($model, 'logistics_costs_f2_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'logistics_costs_f2_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->logisticsF2Sum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="2">Логистические затраты (жд)</td>
                    <td class="input"> <?= $form->field($model, 'logistics_costs_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'logistics_costs_price')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->logisticsSum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="1">Естественная убыль</td>
                    <td><?=round($model->naturalPercent,2) ?></td>
                    <td class="input"><?= $form->field($model, 'natural_decline')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td><?=round($model->naturalPrice, 2) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->naturalSum, 2) ?> </td>
                </tr>

                <tr>
                    <td colspan="1">Налоговая нагрузка</td>
                    <td class="input"> <?= $form->field($model, 'tax_burden_percent')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td class="input"> <?= $form->field($model, 'tax_burden_count')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td> <?=round($model->taxBurdenPrice, 2) ?></td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->taxBurderSum, 2) ?></td>
                </tr>

                <tr>
                    <td colspan="1">Upgrade </td>
                    <td class="input"><?= $form->field($model, 'upgrade_count')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td class="input"> <?= $form->field($model, 'upgrade_price')->textInput(['maxlength' => true])->label(false) ?> </td>
                    <td><?=round($model->upgrade, 2) ?> </td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->upgradeSum, 2) ?></td>

                </tr>

                <tr>
                    <td colspan="1">Стоимость денег %/год </td>
                    <td class="input"><?= $form->field($model, 'cost_money_percent')->textInput(['maxlength' => true])->label(false) ?></td>
                    <td class="input"> <?= $form->field($model, 'cost_money_count')->textInput(['maxlength' => true])->label(false) ?> </td>

                    <td><?=round($model->costMoney, 2) ?> </td>
                    <td></td>
                    <td></td>
                    <td><?=round($model->costMoneySum, 2) ?> </td>
                </tr>
            </tbody>
            <?php if(!$model->isNewRecord): ?>
                <thead>
                    <tr class="grey-row">
                        <td rowspan="2" colspan="2">Финансовые результаты</td>
                        <td rowspan="2">Кол-во, т</td>
                        <td rowspan="2"> Ст-сть ед., грн/т</td>
                        <td colspan="2">в т.ч.</td>
                        <td rowspan="2">Сумма, грн.</td>
                    </tr>
                    <tr class="grey-row">
                        <td >НДС</td>
                        <td >без НДС</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2"><strong>Валовые расходы</strong></td>
                        <td><?=$model->exw_count ?></td>
                        <td><?=$model->totalPrice ?></td>
                        <td><?=$model->exwNds ?></td>
                        <td><?=$model->exwNotNds ?></td>
                        <td><?=$model->totalSum ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Себестоимость</td>
                        <td> </td>
                        <td><?=$model->exw_price ?></td>
                        <td> </td>
                        <td> </td>
                        <td><?=$model->exwSum ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Затраты</td>
                        <td> </td>
                        <td><?=$model->exw_price ?></td>
                        <td> </td>
                        <td> </td>
                        <td><?=$model->costsSum ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Валовые доходы</strong></td>
                        <td><?=$model->count ?> </td>
                        <td><?=$model->price ?></td>
                        <td><?=round($model->nds) ?> </td>
                        <td> <?=round($model->notNds) ?></td>
                        <td><?=round($model->sum) ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Маржинальный доход</strong></td>
                        <td> </td>

                        <td><?=round($model->marginalIncome) ?> </td>
                        <td></td>
                        <td></td>
                        <td><?=round($model->marginalIncomeSum , 2) ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"> </td>
                        <td></td>
                        <td><?=round($model->marginalIncome/$model->price*100,2 ) ?></td>
                        <td></td>
                        <td></td>
                        <td><?=round($model->marginalIncomeSum/$model->sum*100,2 ) ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Потребность в денежных средствах</strong></td>
                        <td> </td>
                        <td><?=$model->needMoney ?></td>
                        <td> </td>
                        <td></td>
                        <td></td>

                    </tr>
                </tbody>
            <?php endif; ?>
        </table>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Сохранить', ['class' => 'btn-green about-container__save']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>


 <? */ ?>
