<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\Accounting */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Accountings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accounting-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'responsible',
            'product',
            'buyers',
            'basis_delivery:',
            'seller:',
            'basis_purchase:',
            'elevator',
            'logist',
            'numbercars_per_day',
            'fpproval_date_line',
            'currency',
            'count_day',
            'count',
            'price',
            'exw_count',
            'exw_price',
            'meneger_count',
            'meneger_price',
            'traveling_expenses_count',
            'traveling_expenses_price',
            'counterparty_count',
            'counterparty_price',
            'broker_count',
            'broker_price',
            'elevator_days',
            'elevator_price',
            'elevator_count',
            'elevator_service_count',
            'elevator_service_price',
            'elevator_shipment_days',
            'elevator_shipment_price',
            'elevator_census_days',
            'elevator_census_price',
            'logistics_costs_f1_count',
            'logistics_costs_f1_price',
            'logistics_costs_f2_count',
            'logistics_costs_f2_price',
            'logistics_costs_count',
            'logistics_costs_price',
            'natural decline',
            'tax_burden_percent',
            'tax_burden_count',
            'upgrade_count',
            'upgrade_price',
            'cost_money_percent',
            'cost_money_count',
        ],
    ]) ?>

</div>
