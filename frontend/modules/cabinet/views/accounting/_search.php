<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\AccountingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accounting-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'responsible') ?>

    <?= $form->field($model, 'product') ?>

    <?= $form->field($model, 'buyers') ?>

    <?= $form->field($model, 'basis_delivery:') ?>

    <?php // echo $form->field($model, 'seller:') ?>

    <?php // echo $form->field($model, 'basis_purchase:') ?>

    <?php // echo $form->field($model, 'elevator') ?>

    <?php // echo $form->field($model, 'logist') ?>

    <?php // echo $form->field($model, 'numbercars_per_day') ?>

    <?php // echo $form->field($model, 'fpproval_date_line') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'count_day') ?>

    <?php // echo $form->field($model, 'count') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'exw_count') ?>

    <?php // echo $form->field($model, 'exw_price') ?>

    <?php // echo $form->field($model, 'meneger_count') ?>

    <?php // echo $form->field($model, 'meneger_price') ?>

    <?php // echo $form->field($model, 'traveling_expenses_count') ?>

    <?php // echo $form->field($model, 'traveling_expenses_price') ?>

    <?php // echo $form->field($model, 'counterparty_count') ?>

    <?php // echo $form->field($model, 'counterparty_price') ?>

    <?php // echo $form->field($model, 'broker_count') ?>

    <?php // echo $form->field($model, 'broker_price') ?>

    <?php // echo $form->field($model, 'elevator_days') ?>

    <?php // echo $form->field($model, 'elevator_price') ?>

    <?php // echo $form->field($model, 'elevator_count') ?>

    <?php // echo $form->field($model, 'elevator_service_count') ?>

    <?php // echo $form->field($model, 'elevator_service_price') ?>

    <?php // echo $form->field($model, 'elevator_shipment_days') ?>

    <?php // echo $form->field($model, 'elevator_shipment_price') ?>

    <?php // echo $form->field($model, 'elevator_census_days') ?>

    <?php // echo $form->field($model, 'elevator_census_price') ?>

    <?php // echo $form->field($model, 'logistics_costs_f1_count') ?>

    <?php // echo $form->field($model, 'logistics_costs_f1_price') ?>

    <?php // echo $form->field($model, 'logistics_costs_f2_count') ?>

    <?php // echo $form->field($model, 'logistics_costs_f2_price') ?>

    <?php // echo $form->field($model, 'logistics_costs_count') ?>

    <?php // echo $form->field($model, 'logistics_costs_price') ?>

    <?php // echo $form->field($model, 'natural decline') ?>

    <?php // echo $form->field($model, 'tax_burden_percent') ?>

    <?php // echo $form->field($model, 'tax_burden_count') ?>

    <?php // echo $form->field($model, 'upgrade_count') ?>

    <?php // echo $form->field($model, 'upgrade_price') ?>

    <?php // echo $form->field($model, 'cost_money_percent') ?>

    <?php // echo $form->field($model, 'cost_money_count') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
