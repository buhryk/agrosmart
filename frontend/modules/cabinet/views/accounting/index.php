<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\cabinet\models\AccountingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('cabinet', 'Учет сделок');

$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="bread">
    <h1><?=$this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <p>&nbsp;</p>
        <div class="row">
            <div class="col-sm-3" id="sidebar">
                <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                    <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
                </div>
            </div>
            <div class="col-sm-9" data-content="page_content">
                <!-- important info -->
                <div class="col-xs-12 important-info">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__title">
                            <?=Yii::t('informing', 'My Account - List of transactions') ?>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 mt40 ap-wrapper pl35 pr35 mb40">
                    <div class="ap-container">
                        <div class="row">
                            <div class="col-xs-12 tac mt40">
                                <a href="<?=\yii\helpers\Url::to(['create']) ?>" class="btn-green tdn mb30">+ <?=Yii::t('cabinet', 'Добавить сделку') ?></a>
                            </div>
                            <div class="branches-table-wrapper col-xs-12 mb40">

                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'tableOptions' => [
                                        'class' => 'branches-table purchase-table col-xs-12'
                                    ],
                                    'layout' => "{items}\n{summary}\n{pager}",
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        'product',
                                        'buyers',
                                        'seller',
                                        'elevator',


                                        [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{update}{delete}'
                                        ],
                                    ],
                                ]); ?>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
