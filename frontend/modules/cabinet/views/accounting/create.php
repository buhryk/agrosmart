<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cabinet\models\Accounting */

$this->title = Yii::t('cabinet', 'Transaction passport');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'Accounting transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="bread">
    <h1><?= Yii::t('cabinet', 'CALCULATOR TRANSACTIONS')?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <p>&nbsp;</p>
        <div class="row">
            <div class="col-sm-3" id="sidebar">
                <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                    <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
                </div>
            </div>
            <div class="col-sm-9" data-content="page_content">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>

    </div>
</div>

