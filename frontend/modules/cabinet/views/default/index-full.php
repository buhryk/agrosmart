<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = Yii::t('cabinet', 'PRIVATE_CABINET');
$this->params['breadcrumbs'][] = $this->title;
$instruments = $data['instruments'];
?>
<div class="bread">
    <h1><?= $this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9" data-content="page_content">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12 important-info">
                        <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                        <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                        <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                        <div class="important-info-content">
                            <p class="important-info__title">
                                <?=Yii::t('informing', 'Cabinet full index') ?>
                            </p>
                        </div>
                    </div>
                </div>
                <hr>
            </div>



            <div class="col-xs-12 p0">
               <?=$data['page']->text ?>
            </div>
            <div class="col-xs-12 instruments-block p0">
                <div class="col-xs-12 instruments-block-title"><?=Yii::t('instruments', 'Инструменты') ?></div>
                <div class="block-aj-show">
                    <?php foreach ($instruments  as $key => $item): ?>
                        <?php $hiddens = $key >= 8 ? 'hiddens' : ''; ?>
                        <?=$key % 3 == 0 ? "</div><div class=\"block-aj-show  ".$hiddens." \">" : "" ?>
                        <a href="<?=Url::to([$item->url]) ?>" class="instruments-block-item col-xs-6 col-sm-4">
    							<span class="vac">
    								<img src="<?=$item->image_cabinet ? $item->image_cabinet : $item->image ?>">
    								<h4><?=$item->title ?></h4>
    							</span>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="text-center">
                <span class="show-button btn-green mt40 mb40 tdn"><?=Yii::t('cabinet', 'Больше инструментов') ?></span>
                <img class="subscription__arrow" src="/images/arrow-green.png" alt="arrow">
            </div>
        </div>
    </div>
</div>
