<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = Yii::t('cabinet', 'PRIVATE_CABINET');
$this->params['breadcrumbs'][] = $this->title;
$item = 0;
?>
<div class="bread">
    <h1><?= $this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <p>&nbsp;</p>
    <div class="row">
            <div class="col-sm-3" id="sidebar">
                <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                    <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
                </div>
            </div>
			<div class="col-sm-9" data-content="page_content">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-xs-12 important-info">
                            <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                            <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                            <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                            <div class="important-info-content">
                                <p class="important-info__title">
                                    <?=Yii::t('informing', 'Cabinet index') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="col-xs-12 steps p0">

                    <?php if(isset($data['profile'])): ?>
                        <div class="col-xs-12 steps mb40 p0">
                            <div class="steps-step">
                                <div class="col-sm-8">
                                    <div>
                                        <div class="steps-step__number"><?=++$item ?></div>
                                        <div class="steps-step__title"><?=$data['profile']->title ?></div>
                                    </div>
                                </div>
                                <div class="steps-step__desctiption col-sm-8">
                                    <?=$data['profile']->text ?>
                                </div>
                                <div class="col-sm-4">
                                    <a href="<?=Url::to(['/cabinet/company/profile']) ?>" class="btn-green"><?=Yii::t('cabinet', 'Заполнить профиль') ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($data['price'])): ?>
                        <div class="col-xs-12 steps mb40 p0">
                            <div class="steps-step">
                                <div class="col-sm-8">
                                    <div>
                                        <div class="steps-step__number"><?=++$item ?></div>
                                        <div class="steps-step__title"><?=$data['price']->title ?></div>
                                    </div>
                                </div>
                                <div class="steps-step__desctiption col-sm-8">
                                    <?=$data['price']->text ?>
                                </div>
                                <div class="col-sm-4">
                                    <a href="<?=Url::to(['/cabinet/sale/zakupka']) ?>" class="btn-green"><?=Yii::t('cabinet', 'Добавить цены') ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($data['product'])): ?>
                        <div class="col-xs-12 steps mb40 p0">
                            <div class="steps-step">
                                <div class="col-sm-8">
                                    <div>
                                        <div class="steps-step__number"><?=++$item ?></div>
                                        <div class="steps-step__title"><?=$data['product']->title ?></div>
                                    </div>
                                </div>
                                <div class="steps-step__desctiption col-sm-8">
                                    <?=$data['product']->text ?>
                                </div>
                                <div class="col-sm-4">
                                    <a href="<?=Url::to(['/product/product/create']) ?>" class="btn-green"><?=Yii::t('advertisement', 'Add product/service'); ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
			</div>
	</div>
</div>