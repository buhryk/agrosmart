<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\cabinet\models\DialogSearch;
use frontend\modules\cabinet\models\DialogDetail;
use frontend\modules\cabinet\models\DialogMessage;

$this->title = Yii::t('cabinet', 'DIALOGS');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dialog">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 ">
            <div class="margin-bottom-10">
                <div class="important-info ">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__title">
                            <?=Yii::t('informing', 'My Account - Dialogues') ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 dialog-container dialogs">
                <div class="row">
                    <?php $form = ActiveForm::begin([
                        'method' => 'get',
                        'action' => ['index'],
                        'options' => ['id' => 'favourites-form']
                    ]); ?>
                    <div class="col-xs-12 dialog-filter-wrapper">
                        <?= Yii::t('common', 'Choose type'); ?>:
                        <div style="display: inline-block">
                            <?= $form->field($searchModel, 'type')->dropDownList([
                                DialogDetail::STATUS_ACTIVE => Yii::t('cabinet', 'Active dialogs'),
                                DialogDetail::STATUS_ARCHIVE => Yii::t('cabinet', 'Archive dialogs')
                            ], ['id' => 'favourites-records-type'])
                                ->label(false); ?>
                        </div>
                    </div>

                    <div class="col-xs-12 message-container">
                        <table class="table">
                            <tr class="grey-row">
                                <th><?=Yii::t('cabinet', 'Пользователь') ?></th>
                                <th><?=Yii::t('cabinet', 'Сообщение') ?></th>
                                <th width="60">&nbsp;</th>
                            </tr>
                            <?php foreach ($models as $model) { ?>
                                <tr class="<?= $model->lastMessage->status == DialogMessage::STATUS_NOT_VIEWED ? 'warning' : 'active'; ?>">
                                    <td>
                                        <?php $interlocutor = ($model->from_user_id == $currentUser->id) ? $model->receiver : $model->author; ?>
                                        <?php if ($interlocutor) { ?>
                                            <a href="<?= Url::to(['/user/user/view', 'id' => $interlocutor->id]); ?>">
                                                <?= $interlocutor->fullName; ?>
                                            </a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <div><?= $model->lastMessage->text; ?></div>
                                        <div style="margin-top: 10px; font-style: italic; color: #989898; font-size: 13px;">
                                            <?= date('d.m.Y H:i:s', $model->lastMessage->created_at); ?>
                                        </div>
                                    </td>
                                    <td style="padding: 10px;">
                                        <a href="<?= Url::to(['dialog', 'id' => $model->primarykey]); ?>"
                                           title="<?= Yii::t('common', 'View'); ?>"
                                           aria-label=" <?= Yii::t('common', 'View'); ?>"
                                           style="text-decoration: none"
                                        >
                                            <span class='glyphicon glyphicon-eye-open'></span>
                                        </a>
                                        <?php $changeStatusUrl = Url::to(['change-status', 'id' => $model->primaryKey]); ?>
                                        <?php if ($model->detail->status == DialogDetail::STATUS_ACTIVE) { ?>
                                            <a title="<?= Yii::t('cabinet', 'Move dialog to archive'); ?>"
                                               style="cursor: pointer;"
                                               class="change-dialog-status"
                                               data-dialog_id="<?= $model->primaryKey; ?>"
                                               data-status="<?= DialogDetail::STATUS_ARCHIVE; ?>"
                                               data-url="<?= $changeStatusUrl; ?>"
                                            >
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        <?php } else { ?>
                                            <a title="<?= Yii::t('cabinet', 'Move dialog to active'); ?>"
                                               style="cursor: pointer;"
                                               class="change-dialog-status"
                                               data-dialog_id="<?= $model->primaryKey; ?>"
                                               data-status="<?= DialogDetail::STATUS_ACTIVE; ?>"
                                               data-url="<?= $changeStatusUrl; ?>"
                                            >
                                                <span class="glyphicon glyphicon-share-alt"></span>
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="message-pagination" style="margin-top: 15px;">
                        <div class="col-xs-12 col-md-6">
                            <span class="message-pagination__text"><?= Yii::t('common', 'Show per page'); ?>:</span>
                            <?php echo Html::activeDropDownList($searchModel,
                                'page_size',
                                DialogSearch::getPerPagePropertiesList(), [
                                    'class' => 'message-pagination__select all-select',
                                    'id' => 'per-page-favourites'
                                ]); ?>
                            <?php $this->registerJs("
                            $('#per-page-favourites').on('change', function(){
                                $('form#favourites-form').submit();
                            });
                            $('#favourites-records-type').on('change', function(){
                                $('form#favourites-form').submit();
                            });
                        ", \yii\web\View::POS_READY,
                                'my-button-handler'
                            ); ?>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages]); ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>
    </div>
</div>