<?php
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cabinet\models\DialogDetail;

$this->title = Yii::t('cabinet', 'Dialog') . ': ' . $interlocutor->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'DIALOGS'), 'url' => ['/cabinet/dialog/index']];
$this->params['breadcrumbs'][] = $this->title;

$interlocutorCompany = $interlocutor->company;
$interlocutorImg = ($interlocutorCompany && $interlocutorCompany->logotype) ?
    Html::img('/'.$interlocutorCompany->logotype, ['width' => 71]) : Html::img('/images/user_71x48.jpg');

$currentUserCompany = $currentUser->company;
$currentUserImg = ($currentUserCompany && $currentUserCompany->logotype) ?
    Html::img('/'.$currentUserCompany->logotype, ['width' => 71]) : Html::img('/images/user_71x48.jpg');
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dialog">
    <div class="row">
        <p>&nbsp;</p>
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="col-sm-9 dialog-container clearfix" style="padding-bottom: 15px;">

            <div class="col-xs-12 dialog-search">
                <?php $changeStatusUrl = Url::to(['change-status', 'id' => $model->primaryKey]); ?>
                <?php if ($model->detail->status == DialogDetail::STATUS_ACTIVE) { ?>
                    <a class="change-dialog-status dialog__archive"
                       style="cursor: pointer"
                       data-dialog_id="<?= $model->primaryKey; ?>"
                       data-status="<?= DialogDetail::STATUS_ARCHIVE; ?>"
                       data-url="<?= $changeStatusUrl; ?>"
                    >
                        <?= Yii::t('cabinet', 'Move dialog to archive'); ?>
                    </a>
                <?php } else { ?>
                    <a class="change-dialog-status dialog__archive"
                       style="cursor: pointer"
                       data-dialog_id="<?= $model->primaryKey; ?>"
                       data-status="<?= DialogDetail::STATUS_ACTIVE; ?>"
                       data-url="<?= $changeStatusUrl; ?>"
                    >
                        <?= Yii::t('cabinet', 'Move dialog to active'); ?>
                    </a>
                <?php } ?>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="dialog-message-form">
                        <?php $form = ActiveForm::begin([
                            'action' => ['dialog', 'id' => $model->primaryKey],
                            'options' => ['id' => 'dialog-form']
                        ]); ?>
                        <?= $form->field($newMessage, 'text')->textarea(); ?>
                        <?= Html::submitButton(Yii::t('common', 'Send'), ['class' => 'btn btn-primary',]) ?>
                        <div class="clearfix"></div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <?php foreach ($messages as $oneMessage) { ?>
                <div class="col-xs-10 col-sm-offset-2 dialog-message">
                    <div class="row">
                        <div class="col-sm-2">
                            <?= ($oneMessage->from_user_id == $currentUser->id) ? $currentUserImg : $interlocutorImg; ?>
                        </div>
                        <div class="col-sm-10">
                            <div>
                                <?php if ($oneMessage->from_user_id == $currentUser->id) { ?>
                                    <span class="dialog-message__name"><?= $currentUser->fullName; ?> (Вы)</span>

                                    <?php if ($currentUserCompany) { ?>
                                        <span> / </span>
                                        <span>
                                            <?= Html::a($currentUserCompany->name,
                                                ['/company/company/view', 'id' => $currentUserCompany->primaryKey],
                                                ['class' => 'dialog-message__org']
                                            ); ?>
                                        </span>
                                    <?php } ?>
                                <?php } else { ?>
                                    <a href="<?= Url::to(['/user/user/view', 'id' => $interlocutor->id]); ?>" style="text-decoration: none">
                                        <span class="dialog-message__name"><?= $interlocutor->fullName; ?></span>
                                    </a>

                                    <?php if ($interlocutorCompany) { ?>
                                        <span> / </span>
                                        <span>
                                            <?= Html::a($interlocutorCompany->name,
                                                ['/company/company/view', 'id' => $interlocutorCompany->primaryKey],
                                                ['class' => 'dialog-message__org']
                                            ); ?>
                                        </span>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <div style="font-style: italic; font-size: 12px;">
                                <?= date('d.m.Y H:i:s', $oneMessage->created_at); ?>
                            </div>
                            <p class="dialog-message__text"><?= $oneMessage->text; ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-xs-12" style="margin-top: 30px; text-align: center">
                <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages]); ?>
            </div>
        </div>
    </div>
</div>