<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<div class="modal-body">
    <div class="subsidiary-form">
        <div id="form-send-message-modal-errors" style="color: red;"></div>
        <?php $form = ActiveForm::begin([
            'id' => 'form-send-message-modal',
            'enableAjaxValidation' => false,
            'options' => [
                'onsubmit' => 'return false',
                'data-url' => Url::to(['/cabinet/dialog/create-message-modal', 'receiver' => $receiver])
            ]
        ]); ?>

        <?= $form->field($model, 'text', ['options' => ['style' => 'margin: 25px 0 20px 0;']])
            ->textarea(['style' => 'resize: none;']);
        ?>

        <div class="form-group">
            <?php echo Html::button(Yii::t('users', 'SEND'), [
                'class' => 'btn btn-primary',
                'id' => 'send-message-modal',
                'type' => 'button',
                'onclick' => 'return false;'
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>