<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\modules\cabinet\components\sidebar\CabinetSidebarWidget;
use frontend\assets\CabinetAsset;

AppAsset::register($this);
CabinetAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => \Yii::t('cabinet', 'PRIVATE_CABINET') . ' ' . \Yii::$app->user->identity->username,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        //['label' => 'Home', 'url' => ['/site/index']],
        //['label' => 'About', 'url' => ['/site/about']],
        //['label' => 'Contact', 'url' => ['/site/contact']],
    ];

    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => \Yii::t('cabinet', 'SIGNUP'), 'url' => ['/site/signup']];
        $menuItems[] = ['label' => \Yii::t('cabinet', 'LOGIN'), 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                \Yii::t('cabinet', 'LOGOUT') . ' (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    $menuItems[] = ['label' => \Yii::$app->language, 'items' => [
        ['label' => \Yii::t('i18n', 'EN'), 'url' => '/i18n/default/en'],
        ['label' => \Yii::t('i18n', 'RU'), 'url' => '/i18n/default/ru'],
        ['label' => \Yii::t('i18n', 'UK'), 'url' => '/i18n/default/uk']
        ]];

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

 <div class="cabinet-sidebar-container" style="padding: 51px 15px 20px 300px;">
    <div class="cabinet-sidebar" style="margin-left: -300px; position: absolute; width: 200px;">
        <?= CabinetSidebarWidget::widget(); ?>
    </div>
        <div class="container" style="padding-top: 15px;">
            <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Agrosmart <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered('RGroup') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
