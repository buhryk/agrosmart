<?php
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;
use frontend\assets\CabinetAsset;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use \frontend\modules\cabinet\models\PricesDispath;
use frontend\modules\cabinet\models\PricesDispatchForm;
use frontend\modules\cabinet\models\Sale;

CabinetAsset::register($this);

$this->title = Yii::t('cabinet', 'Notification price');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <p>&nbsp;</p>
        <div class="row">
            <div class="col-sm-3" id="sidebar">
                <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                    <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
                </div>
            </div>
            <div class="col-sm-9" data-content="page_content">
                <!-- important-info -->
                <div class="col-xs-12 important-info">
                    <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                    <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                    <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                    <div class="important-info-content">
                        <p class="important-info__text"><?=Yii::t('cabinet', 'Если вы хотите, чтобы вам приходили уведомления об обновлениях цен. Создайте фильтр цен ниже.') ?></p>
                    </div>
                </div>

                <div class="col-xs-12 mt40 ap-wrapper pl35 pr35 mb40">
                    <div class="ap-container">
                        <div class="row" style="padding-top: 40px;">
                            <div class="branches-table-wrapper">

                                <div class="col-sm-11 ap-form-contacts ap-form-new-price">
<!--                                    <span class="block-label">Уведомления об обновлениях цен</span>-->
                                    <div class="row">

                                        <?php $form = ActiveForm::begin([
                                            'enableAjaxValidation' => false,
                                        ]);
                                        $params_culture = [
                                            'prompt' => Yii::t('cabinet','Select culture')
                                        ];
                                        $params_compare = [
                                            'prompt' => Yii::t('cabinet','Select compare param')
                                        ];
                                       ?>

                                        <?= $form->field($model, 'rubric_id',['template' => "<div class='col-sm-6'>\n{hint}\n{input}\n{error}</div>"])
                                            ->dropDownList(PricesDispatchForm::getRubrics(),$params_culture  )
                                            ->label(false)    ?>

                                        <?= $form->field($model, 'filter',['template' => "<div class='col-sm-6'>\n{hint}\n{input}\n{error}</div>"])
                                            ->dropDownList(PricesDispath::statusFilters(),$params_compare)
                                            ->label(false) ?>

                                        <?= $form->field($model, 'type',['template' => "<div class='col-sm-6'>\n{hint}\n{input}\n{error}</div>"])
                                            ->dropDownList(Sale::OffersTypes(),$params_compare)
                                            ->label(false) ?>

                                        <?= $form->field($model, 'value',['template' => "<div class='col-sm-6 '>\n{hint}\n{input}\n{error}</div>"])
                                            ->textInput(['placeholder'=>Yii::t('cabinet', 'PRICE')])
                                            ->label(false); ?>

                                        <?= Html::submitButton( Yii::t('cabinet', 'Add filter'), [
                                            'class' => 'btn-green ui-link' ,
                                        ]) ?>


                                        <?php ActiveForm::end(); ?>

                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{summary}\n{pager}",
                        'columns' => [
                            [
                                'label' => Yii::t('cabinet','Rubruc title'),
                                'value'=>'rubric.title'
                            ],
                            [
                                'label' => Yii::t('common', 'Type'),
                                'value' =>function($data){
                                    return \frontend\modules\cabinet\models\Sale::OffersTypes()[$data->type] ;
                                }
                            ],
                            [
                                'label' => Yii::t('cabinet','Compare name'),
                                'value' =>function($data){
                                    return PricesDispath::statusFilters()[$data->filter] ? PricesDispath::statusFilters()[$data->filter] : 'undefine' ;
                                }
                            ],
                            [
                                'label' => Yii::t('cabinet','Value in UAH'),
                                'value' =>'value'
                            ],
                            [

                                'format'=>'raw',
                                'contentOptions'=>['style'=>'width: 60px;', 'class' => 'actions-buttons'],
                                'value' => function ($model) {
                                    return "
                                                <a href='".\yii\helpers\Url::to(['send-email', 'id' => $model->id])."' 
                                                    title='".Yii::t('common', 'Send Email')."' 
                                                    aria-label='".Yii::t('common', 'Send Email')."' 
                                                    data-pjax='0'>
                                                        <span class='glyphicon glyphicon-envelope'></span>
                                                </a>
                                                <a href='".\yii\helpers\Url::to(['delete', 'id' => $model->id])."' 
                                                    title='".Yii::t('common', 'Delete')."' 
                                                    aria-label='".Yii::t('common', 'Delete')."' 
                                                    data-pjax='0'
                                                    data-confirm='".Yii::t('common', 'Are you sure you want to delete this item?')."'>
                                                        <span class='glyphicon glyphicon-remove'></span>
                                                </a>";
                                },
                            ],
                        ],
                        'tableOptions'=>['class'=>'branches-table col-xs-12'],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

