<?php
use frontend\assets\PriceAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;

PriceAsset::register($this);

$type_name = ($type == 0) ? Yii::t('price', 'sale') : Yii::t('price', 'purchase');
$this->title = $type_name;
$this->params['breadcrumbs'][] = [
        'label' => Yii::t('price', 'Personal Cabinet'),
        'url' => '/cabinet'
    ];
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="bread">
    <h1><?= $this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<div class="container">
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget() ?>
            </div>
        </div>
        <div class="col-sm-9" data-content="page_content">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12 important-info">
                        <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                        <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                        <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                        <div class="important-info-content">
                            <p class="important-info__title">
                                <?= Yii::t('price', 'information window price') ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form action="" id="form1" class="<?= $model->priceModel ? 'open-upload-not-active' : 'open-upload-active' ?>">

                            <?php if (!$error && Yii::$app->request->get('file')): ?>
                                <div class="b-table">
                                    <div class="b-head b-head_margintb">
                                        <?= Yii::t('price', 'add-price list') ?>
                                    </div>
                                    <div class="b-thanks">
                                            <span class="b-thanks__info-icon">
                                                <img src="/images/okey.png" alt="ok">
                                            </span>
                                        <?= Yii::t('price',
                                            'Thank you! Your price has been successfully downloaded and published on the website') ?>
                                        <p class="b-thanks__info">
                                            <?= Yii::t('price', 'You can see it by going to') ?> <a href="<?= Url::to(['/company/price/view', 'id' => Yii::$app->request->get('file')])?>" target="_blank"><?= Yii::t('price', 'link') ?></a>
                                        </p>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="b-load-file">
                                <label class="b-load-file__load-btn btn-green"><span
                                            class="b-load-file__load-plus">+</span><?= Yii::t('price',
                                        'upload price list') ?></label>
                                <div class="form-group">
                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </form>

                        <?php $form = ActiveForm::begin([
                            'id' => 'form2',
                            'options' => ['class' => $model->priceModel ?  'upload-not-active' : 'upload-active'],
//                            'enableClientValidation' => false
                        ]); ?>

                        <div class="b-table">
                            <div class="b-head b-head_margintb">
                                <?php if ($model->priceModel): ?>
                                    <?= Yii::t('price', 'Reloading "{name}"', ['name' => $model->priceModel->name]) ?>
                                <?php else: ?>
                                    <?= Yii::t('price', 'add-price list') ?>
                                <?php endif; ?>
                            </div>

                            <div class="b-price__load">
                                <?= Yii::t('price', 'upload file') ?>

                                <ol class="b-list-num">
                                    <li><?= Yii::t('price', 'download template exel') ?> <a href="<?=$template?>"><?= Yii::t('price', 'download template') ?></a></li>
                                    <?= Yii::t('price', 'download exel') ?>
                                    <li><?= Yii::t('price', 'Upload your file') ?>

                                        <div class="b-list-num__text">
                                            <div class="b-list-num__file">
                                                <?php if ($error): ?>
                                                    <?php foreach ($error as $item): ?>
                                                        <div  class="alert alert-danger alert-dismissible">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                            <?= Yii::t('price', 'Warning') ?>! <?= $item ?>
                                                        </div>
                                                    <?php endforeach;?>
                                                <?php endif; ?>
                                                <div id="exist-alert" style="display: none" class="alert alert-danger alert-dismissible">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <?= Yii::t('price', 'select a file') ?>
                                                </div>
                                                <div id="format-alert" style="display: none" class="alert alert-danger alert-dismissible">
                                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                    <?= Yii::t('price', 'The wrong format was selected') ?>
                                                </div>

                                                <label for="add"
                                                       class="b-list-num__load b-btn btn btn-lg"><?= Yii::t('price','selected file') ?></label>
                                                <div class="b-list-num__load-info">
                                                    <?php echo $form->field($model, 'exel')
                                                        ->fileInput([
                                                            'id' => 'add',
                                                            'style' => 'display:none',
                                                            'onchange' => 'document.getElementById(\'file_exists\').innerHTML=this.value',
                                                            'required' => 'required'
                                                        ])
                                                        ->label(false) ?>
                                                    <p class="b-list-num__load-warning" id="file_exists"><?= Yii::t('price','No file selected') ?></p>
                                                </div>
                                            </div>
                                            <p><?= Yii::t('price','The file size must not exceed') ?></p>
                                        </div>
                                    </li>
                                    <li><?= Yii::t('price', 'publish price-list on site') ?></li>
                                </ol>

                            </div>
                        </div>
                        <div class="b-load-file-small">
                            <button id="valid" class="b-load-file__load-btn btn-green-small">
<!--                                <span-->
<!--                                        class="b-load-file__load-plus">+</span>-->
                                <?= Yii::t('price',
                                    'published price list') ?></button>
                            <button type="button" id="cencel" class="b-btn b-btn_dark btn btn-lg">

                                <?= Yii::t('price', 'cancel') ?>
                            </button>

                            <div class="form-group field-choose-logo-file">
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>

<!--                        <div class="b-price__cancel text-center">-->
<!--                            <button type="button" id="cencel" class="b-btn b-btn_dark btn btn-lg">-->
<!---->
<!--                                --><?//= Yii::t('price', 'cancel') ?>
<!--                            </button>-->
<!--                        </div>-->
                        <ul class="b-info-list">
                            <?= Yii::t('price','Required field') ?>
                        </ul>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>

            <!--price-->
            <div class="row">
                <div class="col-md-12">
                    <div class="b-table">
                        <div class="b-head">
                            <?= Yii::t('price', 'my prices') ?>
                        </div>
                        <div class="b-table__list">
                            <?php foreach ($prices as $price): ?>
                                <?= $this->render('_view', ['price' => $price, 'type' => $type]) ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--end price-->
            <div class="b-pagination">
                <?php
                    echo LinkPager::widget([
                        'pagination' => $pagination,
                        'maxButtonCount' => 6,
                    ]);
                ?>
            </div>
        </div>
    </div>
</div>