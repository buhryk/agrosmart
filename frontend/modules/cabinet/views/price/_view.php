<?php
use frontend\assets\PriceAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\StringHelper;

?>
<div class="b-panel">
    <div class="b-panel__info">
        <div class="b-panel__title"><?= Html::encode($price->name) ?></div>
        <div class="b-panel__date"><?= Yii::t('price', 'updated_at') ?>:
            <span><?= date('d.m.y', $price->updated_at) ?></span></div>
        <div class="b-panel__text">
            <p> <?= StringHelper::truncate(Html::encode($price->description),
                    180) ?> </p>
        </div>
    </div>

    <div class="b-panel__buttons">
        <a href="<?= Url::toRoute(['/company/price/view', 'id' => $price->id]) ?>"
           type="button" class="b-btn btn btn-default btn-lg">
            <span class="fa fa-eye" aria-hidden="true"></span><?= Yii::t('price',
                'view') ?>
        </a>
        <button id="development" type="button" class="b-btn btn btn-default btn-lg">
            <span class="fa fa-pencil" aria-hidden="true"></span><?= Yii::t('price',
                'Edit') ?>
        </button>
        <button id="development" type="button" class="b-btn btn btn-default btn-lg" >
            <span class="fa fa-calendar" aria-hidden="true"></span><?= Yii::t('price',
                'Update date') ?>
        </button>
        <a type="button" class="b-btn btn btn-default btn-lg" href="<?= Url::to(['price/index', 'type' => $type, 'price_id' => $price->id]) ?>">
            <span class="fa fa-refresh" aria-hidden="true"></span><?= Yii::t('price',
                'Reload again') ?>
        </a>
        <a href="<?= Url::toRoute(['/cabinet/price/download', 'id' => $price->id]) ?>"
           class="b-btn btn btn-default btn-lg">
            <span class="fa fa-download" aria-hidden="true"></span><?= Yii::t('price',
                'Download') ?>
        </a>
        <?php
    echo Html::a('<span class="fa fa-remove" aria-hidden="true"></span>'.Yii::t('price', 'Delete'),
    ['delete', 'id' => $price->id, 'type' => $type],
    [
    'class' => 'b-btn btn btn-default btn-lg',
    'title' => Yii::t('price', 'Delete'),
    'onclick' => 'return confirm("' . Yii::t('common',
    'Are you sure you want to delete this item?') . '")'
    ]
    );
    ?>

        <div style="display: none" id="development-alert" class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?= Yii::t('price', 'This section is under construction') ?>
        </div>
    </div>
</div>
