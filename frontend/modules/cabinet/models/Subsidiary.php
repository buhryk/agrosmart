<?php

namespace frontend\modules\cabinet\models;

use common\event\NotFoundCoordinate;
use frontend\models\Company;
use Yii;
use frontend\models\CompanyUser;
use yii\behaviors\TimestampBehavior;
use backend\modules\commondata\models\City;
use common\helpers\GoogleMapsApiHelper;
/**
 * This is the model class for table "subsidiary".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $company_id
 * @property string $type
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property CompanyUser $companyUser
 */
class Subsidiary extends \yii\db\ActiveRecord
{
    /**
     * Типы филиалов
     */
    const TYPE_MAIN_OFFICE = 0;
    const TYPE_SALE_POINT = 1;
    const TYPE_DEALER_OFFICE = 2;
    const TYPE_BRANCH = 3;
    const TYPE_SALE_DEPARTAMENT = 4;
    const TYPE_PURCHASING_DEPARTMENT = 5;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subsidiary';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'company_id', 'address', 'phone', 'email'], 'required'],
            [['city_id', 'company_id', 'created_at', 'updated_at'], 'integer'],
            [['type'], 'integer'],
            ['type', 'in', 'range' => array_keys($this->getListType())],
            [['address', 'phone', 'email', 'coordinates'], 'string', 'max' => 255],
            [['email'], 'unique'],
            ['email', 'email'],
            //[['address'], 'validateLocation'],
            [['type'], 'validateType'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            ['type', 'default', 'value' => self::TYPE_SALE_POINT]
        ];
    }

    public function validateType($attributeNames = null, $clearErrors = true)
    {
        if ($this->type == self::TYPE_MAIN_OFFICE) {
            $model = Subsidiary::find()
                ->where(['company_id' => $this->company_id])
                ->andFilterWhere(['!=','id', $this->id])
                ->andWhere(['type' => self::TYPE_MAIN_OFFICE])
                ->exists();
            if ($model) {
                $this->addError($attributeNames, Yii::t('cabinet', 'У компании может быть только один главный офис ', ['field' => $this->getAttributeLabel($attributeNames)]));
            }

        }

    }

    public function validateLocation($attribute, $params)
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();

        if ($this->location) {
            $coordinates = $googleMapsApiHelper->getLocationByAddress($this->location);
            if (!$coordinates) {
                $this->addError($attribute, Yii::t('instruments', 'Wrong location', ['field' => $this->getAttributeLabel($attribute)]));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cabinet', 'ID'),
            'city_id' => Yii::t('cabinet', 'CITY'),
            'company_id' => Yii::t('cabinet', 'COMPANY'),
            'type' => Yii::t('cabinet', 'TYPE'),
            'address' => Yii::t('cabinet', 'ADDRESS'),
            'phone' => Yii::t('cabinet', 'PHONE'),
            'email' => Yii::t('cabinet', 'EMAIL'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUser()
    {
        return $this->hasOne(CompanyUser::className(), ['id' => 'company_user_id']);
    }

    /**
     * @inheritdoc
     * @return SubsidiaryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubsidiaryQuery(get_called_class());
    }
    /**
     * Получение списка типа филиалов
     * @return array
     */
    public function getListType()
    {
        return [
            self::TYPE_MAIN_OFFICE => Yii::t('cabinet', 'MAIN-OFFICE'),
            self::TYPE_SALE_POINT => Yii::t('cabinet', 'SALE-POINT'),
            self::TYPE_DEALER_OFFICE => Yii::t('cabinet', 'DEALER-OFFICE'),
            self::TYPE_BRANCH => Yii::t('cabinet', 'Филиал'),
            self::TYPE_SALE_DEPARTAMENT => Yii::t('cabinet', 'Отдел продаж'),
            self::TYPE_PURCHASING_DEPARTMENT => Yii::t('cabinet', 'Отдел закупки'),

        ];
    }
    /**
     * Получение типа
     * @return string
     */
    public function getType()
    {
        $data = $this->getListType();
        return isset($data[$this->type]) ? $data[$this->type] : 'Не определено';
    }




    public function getDetailAddress()
    {
        $city = $this->city;
        $area = (($area = $city->area) !== null)  ? $area : null;
        $region = $city->region;

        return $city->title . ', ' . ($area ? ($area->title . ', ') : '') . $region->title . ', ' . $this->address;
    }

    public function getLocation()
    {
        $city = $this->city;
        $area = (($area = $city->area) !== null)  ? $area : null;
        $region = $city->region;

        return $city->title . ', ' . ($area ? ($area->title . ', ') : '') . $region->title . ', ' . $this->address;
    }

    public function getLocationMap()
    {
        $data = (new \yii\db\Query())
            ->select(['city_lang.title', 'oblast_lang.title as oblastTitle'])
            ->from('city')
            ->innerJoin('city_lang', 'city_id = city.id')
            ->innerJoin('oblast_lang', 'oblast_lang.oblast_id = city.oblast_id')
            ->where(['city.id' => $this->city_id])
            ->andWhere(['city_lang.lang' =>  Yii::$app->language, 'oblast_lang.lang' =>  Yii::$app->language])
            ->one();

        return addslashes($data['title']).', '.$data['oblastTitle'].', '.$this->address;
    }

    public function beforeSave($insert)
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();
        $coordinates = $googleMapsApiHelper->getLocationByAddress($this->location);
        if (!$coordinates) {
            $city = $this->city;
            $region = $city->region;
            $address = $city->title.', '. $region->title ;
            $coordinates = $googleMapsApiHelper->getLocationByAddress($address);
            if (!$coordinates) {
                $coordinates = $googleMapsApiHelper->getLocationByAddress($city->title);
            }
        }
        if ($coordinates) {
            $this->coordinates = json_encode($coordinates);
        }
        if (empty($coordinates)  && empty($this->coordinates)) {
            $notFoundCoordinate = new NotFoundCoordinate($this);
            $notFoundCoordinate->send();
        }


        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($changedAttributes['city_id'] != $this->city_id) {
            $googleMapsApiHelper = new GoogleMapsApiHelper();
            $coordinates = $googleMapsApiHelper->getLocationByAddress($this->city->title);
            if (!$coordinates) {
                $notFoundCoordinate = new NotFoundCoordinate($this);
                $notFoundCoordinate->send();
            }
        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
