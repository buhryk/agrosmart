<?php

namespace frontend\modules\cabinet\models;

use frontend\models\User;
use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $user;
    public $old_password;
    public $new_password;
    public $repeat_new_password;

    public function rules()
    {
        return [
            [['old_password', 'new_password', 'repeat_new_password'],'required'],
            ['old_password', 'findPasswords'],
            ['repeat_new_password', 'compare', 'compareAttribute' => 'new_password'],
        ];
    }

    public function __construct(array $config)
    {
        $this->user = $config['user'];
        parent::__construct($config);
    }

    public function attributeLabels(){
        return [
            'old_password' => Yii::t('cabinet', 'Old password'),
            'new_password' => Yii::t('cabinet', 'New password'),
            'repeat_new_password' => Yii::t('cabinet', 'Repeat new password'),
        ];
    }

    public function findPasswords($attribute, $params)
    {
        $user = User::find()->where(['username' => $this->user->username])->one();

        if (!Yii::$app->security->validatePassword($this->$attribute, $user->password_hash)) {
            $this->addError($attribute, Yii::t('cabinet', 'Old password is incorrect'));
        }
    }
}