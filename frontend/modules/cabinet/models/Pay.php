<?php

namespace frontend\modules\cabinet\models;

use Yii;
use yii\base\Model;
use frontend\components\library\LiqPay;
use yii\web\Controller;

class Pay extends PayBase
{
    public function pay($post)
    {
        $decode = $this->decode($post);
        if(!$decode) return NULL;
        if($result = $this->getStatusPay($decode)) return $result;
        $orderData = $this->getOrder($decode->order_id);

        $tariff = Tariff::find()->where(['id'=>$orderData['tariff_id']])->one();
        $refillModel = new Refill();
        $transaction =Yii::$app->db->beginTransaction();

        try {
            $refillModel->sum = $tariff->price;
            $refillModel->user_id = $orderData['user_id'];
            $refillModel->type = $tariff->type;
            $refillModel->order_id = $decode->order_id;
            $refillModel->save();


            $topModel = new Top();
            $topModel->model_name = $orderData['model_name'];
            $topModel->model_id = $orderData['model_id'];
            $topModel->type = $tariff->type;
            $topModel->start = $this->getDateStart($orderData['model_id'], $orderData['model_name'], $tariff->type);
            $topModel->finish = $topModel->start  + ($tariff->count_deys*3600*24);
            $topModel->refill_id = $refillModel->id;
            $topModel->save();
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch(\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->statusList[$decode->status];
    }

    public function getDateStart($model_id, $model_name, $type)
    {
        $model = Top::find()
            ->where(['model_id'=> $model_id])
            ->andWhere(['model_name' => $model_name])
            ->andWhere(['type' => $type])
            ->andWhere(['>', 'finish' , time()])
            ->one();

        return $model ? $model->finish : time();
    }

    public function getButton(PullTop $model){
        $liqpay = new LiqPay($this->public_key, $this->private_key);

        return $liqpay->cnb_form(array(
            'version'        => '3',
            'amount'         => $model->currentModel->price,
            'currency'       => 'UAH',
            'description'    => Yii::t('common', 'Pull top'),
            'order_id'       => $this->setOrder($model),
            'sandbox'        => '0',
            'customer'       => Yii::$app->user->id,
            'server_url'     => Yii::$app->urlManager->createAbsoluteUrl(['/cabinet/pay/result']),
            'result_url'     => Yii::$app->urlManager->createAbsoluteUrl(['/cabinet/pay/index']),
        ));
    }

    public function setOrder(PullTop $model)
    {
        $order = $this->initDataOrder(Yii::$app->user->id, $model->model_name ,$model->model_id ,$model->currentModel->id);
        return implode('_', $order);
    }

    public function getOrder($order)
    {
        $data = explode('_', $order);
        return $this->initDataOrder($data[0],$data[1] ,$data[2] ,$data[3]);
    }

    public function initDataOrder($user_id, $model_name, $model_id, $tariff_id)
    {
        return [
            'user_id' => $user_id,
            'model_name' => $model_name,
            'model_id' => $model_id,
            'tariff_id' => $tariff_id,
            'time' => time(),
        ];
    }

}