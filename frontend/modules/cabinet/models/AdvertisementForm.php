<?php

namespace frontend\modules\cabinet\models;

use backend\modules\commondata\models\City;
use backend\modules\rubric\models\Rubric;
use frontend\models\Company;
use frontend\models\User;
use frontend\modules\product\models\Advertisement;
use yii\behaviors\SluggableBehavior;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class AdvertisementForm extends Model
{
    public $rubric_id;
    public $title;
    public $text;
    public $active_to;
    public $without_active_to;
    public $currency_id;
    public $price;
    public $contract_price;
    public $volume;
    public $measurement_id;
    public $author_id;
    public $from_user_id;
    public $from_subsidiary_id;
    public $phones;
    public $city_id;
    public $address;
    public $publish_from;
    public $status;

    const PUBLISH_FROM_OTHER_USER = 'other';
    const PUBLISH_FROM_SUBSIDIARY = 'subsidiary';
    const PUBLISH_FROM_AUTHOR = 'author';

    public function rules()
    {
        return [
            [['title', 'currency_id', 'city_id'], 'required'],
            ['title', 'trim'],
            ['phones', 'required', 'message' => Yii::t('advertisement', 'You need to enter at least one phone number')],
            ['rubric_id', 'required', 'message' => Yii::t('advertisement', 'You need to choose rubric')],
            [['text'], 'string'],
            [['rubric_id', 'measurement_id', 'author_id', 'from_user_id', 'from_subsidiary_id',
                'active_to', 'currency_id', 'city_id', 'status'], 'integer'],
            [['without_active_to', 'contract_price'], 'integer', 'max' => 1],
            [['price', 'volume'], 'number'],
            ['title', 'string', 'max' => 70],
            ['publish_from', 'string'],
            ['publish_from', 'in', 'range' => [self::PUBLISH_FROM_OTHER_USER, self::PUBLISH_FROM_SUBSIDIARY, self::PUBLISH_FROM_AUTHOR]],
            [['phones', 'address'], 'string', 'max' => 255],
            ['active_to', 'validateActiveTo'],
            ['volume', 'validateVolume'],
            ['rubric_id', 'exist', 'skipOnError' => true, 'targetClass' => Rubric::className(),
                'targetAttribute' => ['rubric_id' => 'id'], 'message' => Yii::t('advertisement', 'Rubric not found')],
            [['contract_price', 'without_active_to'], 'default', 'value' => 0],
            ['price', 'validatePriceWithContractPrice'],
            ['from_user_id', 'validateFromUserId'],
            ['from_subsidiary_id', 'validateFromSubsidiaryId'],
            ['phones', 'validatePhones']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('advertisement', 'Title'),
            'text' => Yii::t('advertisement', 'Advertisement text'),
            'volume' => Yii::t('advertisement', 'Volume'),
            'phones' => Yii::t('advertisement', 'Phones'),
            'address' => Yii::t('advertisement', 'Address'),
            'city_id' => Yii::t('advertisement', 'Locality'),
            'status' =>  Yii::t('common', 'Status'),

        ];
    }

    public function __construct(Advertisement $advertisement)
    {
        $this->title = $advertisement->title;
        $this->text = $advertisement->text;
        $this->currency_id = $advertisement->currency_id;
        $this->price = $advertisement->price;
        $this->volume = $advertisement->volume;
        $this->rubric_id = $advertisement->rubric_id;
        $this->measurement_id = $advertisement->measurement_id;
        $this->phones = $advertisement->phones;
        $this->city_id = $advertisement->city_id;
        $this->address = $advertisement->address;
        $this->status = $advertisement->status;
        $this->from_user_id = $advertisement->from_user_id;
        $this->from_subsidiary_id = $advertisement->from_subsidiary_id;
        
        if ($advertisement->active_to) {
            $this->active_to = date('d/m/Y', $advertisement->active_to);
        }
        parent::__construct([]);
    }

    public function validateActiveTo($attribute, $params)
    {
        if ($this->active_to && $this->without_active_to) {
            $this->addError('active_to', Yii::t('advertisement', 'You must choose active to time or until'));
        }
    }

    public function validatePhones($attribute, $params)
    {
        if ($this->phones) {

            $phones = explode(',', $this->phones);
            foreach ($phones as $key => $phone) {
                $phones[$key] = trim($phone);
            }

            foreach ($phones as $key => $phone) {
                if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/", $phone)) {
                    $this->addError('phones', Yii::t('advertisement', 'Phone numbers must be written in the format', ['format' => '0XX-XXX-XX-XX']));
                }
            }
        }
    }

    public function validateVolume($attribute, $params)
    {
        if ($this->volume && $this->rubric_id && !$this->measurement_id) {
            $this->addError('measurement_id', Yii::t('advertisement', 'You must choose measurement'));
        }
    }

    public function validatePriceWithContractPrice($attribute, $params)
    {
        if (($this->price == '0' && !$this->contract_price) || ($this->price && $this->contract_price == 1)) {
            $this->addError('price', Yii::t('advertisement', 'You must set price or set price as contract'));
        }
    }

    public function validateFromUserId()
    {
        if ($this->from_user_id && !Yii::$app->user->isGuest) {

            $company = Yii::$app->user->identity->company;
            $companyUsers = $company ? $company->companyUsers : [];

            if (!in_array($this->from_user_id, ArrayHelper::getColumn($companyUsers, 'user_id'))) {
                $this->addError('author_id', Yii::t('advertisement', 'Wrong from_user_id'));
            }
        }
    }

    public function validateFromSubsidiaryId()
    {
        if ($this->from_subsidiary_id && !Yii::$app->user->isGuest) {

            $company = Yii::$app->user->identity->company;
            $subsidiaries = $company ? $company->subsidiaries : [];

            if (!in_array($this->from_subsidiary_id, ArrayHelper::getColumn($subsidiaries, 'id'))) {
                $this->addError('author_id', Yii::t('advertisement', 'Wrong from_subsidiary_id'));
            }
        }
    }

    public function getCity()
    {
        return City::find()->where([City::tableName().'.id' => $this->city_id])->joinWith('lang')->one();
    }

    public function getLocation()
    {
        $city = $this->city;
        $area = (($area = $city->area) !== null)  ? $area : null;
        $region = $city->region;

        return $city->title . ', ' . ($area ? ($area->title . ', ') : '') . $region->title;
    }


    public function beforeValidate()
    {
        if ($this->active_to) {
            $this->active_to = \DateTime::createFromFormat('d/m/Y',$this->active_to)->format('U');
        }
        if ($this->publish_from == self::PUBLISH_FROM_OTHER_USER && !$this->from_user_id) {
            $this->addError('author_id', Yii::t('advertisement', 'Wrong from_user_id'));
        }
        if ($this->publish_from == self::PUBLISH_FROM_SUBSIDIARY && !$this->from_subsidiary_id) {
            $this->addError('author_id', Yii::t('advertisement', 'Wrong from_subsidiary_id'));
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function afterValidate()
    {
        if ($this->active_to) {
            $this->active_to = date('d/m/Y', $this->active_to);
        }

        parent::afterValidate(); // TODO: Change the autogenerated stub
    }

    public function saveAdvertisement(Advertisement $advertisement)
    {
        if ($this->validate()) {
            $advertisement->title = $this->title;
            $advertisement->text = $this->text;
            $advertisement->active_to = $this->active_to ? \DateTime::createFromFormat('d/m/Y',$this->active_to)->format('U') : null;
            $advertisement->measurement_id = $this->measurement_id;
            $advertisement->volume = $this->volume ? $this->volume : null;
            $advertisement->price = $this->price ? $this->price : null;
            $advertisement->currency_id = $this->currency_id;
            $advertisement->city_id = $this->city_id;
            $advertisement->address = $this->address;
            $advertisement->phones = $this->phones;
            $advertisement->status = $this->status;
            $advertisement->alias = '';


            if ($advertisement->company_id) {
                if ($this->publish_from == AdvertisementForm::PUBLISH_FROM_AUTHOR) {
                    $advertisement->from_user_id = null;
                    $advertisement->from_subsidiary_id = null;
                } elseif ($this->publish_from == AdvertisementForm::PUBLISH_FROM_OTHER_USER) {
                    $advertisement->from_user_id = $this->from_user_id;
                    $advertisement->from_subsidiary_id = null;
                } elseif ($this->publish_from == AdvertisementForm::PUBLISH_FROM_SUBSIDIARY) {
                    $advertisement->from_user_id = null;
                    $advertisement->from_subsidiary_id = $this->from_subsidiary_id;
                }
            }

            return $advertisement->save() ? $advertisement : false;
        }

        return false;
    }

    public function getStatusList()
    {
        return [
            Advertisement::STATUS_ACTIVE_NO => Yii::t('advertisement', 'STATUS_ACTIVE_NO'),
            Advertisement::STATUS_ACTIVE_YES => Yii::t('advertisement', 'STATUS_ACTIVE_YES'),
        ];
    }
}