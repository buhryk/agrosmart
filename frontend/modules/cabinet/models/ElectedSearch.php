<?php

namespace frontend\modules\cabinet\models;

use Yii;
use yii\base\Model;
use frontend\modules\elected\models\Elected;

/**
 * ElectedSearch represents the model behind the search form about `frontend\modules\elected\models\Elected`.
 */
class ElectedSearch extends Elected
{
    const DEFAULT_PAGE_SIZE = 10;
    const PAGE_SIZE_10_PER_PAGE = 10;
    const PAGE_SIZE_25_PER_PAGE = 25;
    const PAGE_SIZE_50_PER_PAGE = 50;

    public $page_size;
    public $currentUser;

    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->currentUser = isset($config['currentUser']) ? $config['currentUser'] : Yii::$app->user->identity;
        $this->page_size = self::DEFAULT_PAGE_SIZE;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'created_at', 'page_size'], 'integer'],
            [['table_name'], 'safe'],
            ['page_size', 'default', 'value' => self::DEFAULT_PAGE_SIZE]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Elected::find();

        // add conditions that should always apply here


        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->currentUser->id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'table_name', $this->table_name]);

        return $query;
    }

    public static function getPerPagePropertiesList()
    {
        return [
            self::PAGE_SIZE_10_PER_PAGE => self::PAGE_SIZE_10_PER_PAGE,
            self::PAGE_SIZE_25_PER_PAGE => self::PAGE_SIZE_25_PER_PAGE,
            self::PAGE_SIZE_50_PER_PAGE => self::PAGE_SIZE_50_PER_PAGE
        ];
    }

    public static function getRecordTypesList()
    {
        return [
            Elected::ALIAS_ADVERTISEMENT => Yii::t('advertisement', 'Products and services'),
            Elected::ALIAS_COMPANY => Yii::t('common', 'Companies'),
            Elected::ALIAS_USER => Yii::t('users', 'Users')
        ];
    }
}