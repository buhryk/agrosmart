<?php

namespace frontend\modules\cabinet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cabinet\models\Sale;

/**
 * SaleSearch represents the model behind the search form about `frontend\modules\cabinet\models\Sale`.
 */
class SaleSearch extends Sale
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'rubric_id', 'created_at', 'updated_at', 'oblast_id'], 'integer'],
          //  [['volume', 'price'], 'number'],
            [['address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type = NULL, $currency = NULL)
    {

        $query = Sale::find()
            ->andWhere(['type' => $type])
            ->andWhere(['company_id' => Yii::$app->user->identity->companyUser->company_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($currency){
            $query->andWhere(['currency_id' => $currency]);
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'rubric_id' => $this->rubric_id,
            'volume' => $this->volume,
            'price' => $this->price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'oblast_id' => $this->oblast_id
        ]);
        
        $query->andWhere(['company_id' => Yii::$app->user->identity->companyUser->company_id]);

        if(isset($params['from_date']) && $params['from_date']){
            $query->andWhere('date >= :from_date', [':from_date' => \DateTime::createFromFormat('d/m/Y', $params['from_date'])
                ->format('Y-m-d')]);
        }else{
            $query->andWhere('date >= :from_date', [':from_date' => date('Y-m-d')]);
        }

        if(isset($params['to_date']) && $params['to_date']){
            $query->andWhere('date <= :to_date', [':to_date' => \DateTime::createFromFormat('d/m/Y', $params['to_date'])
                ->format('Y-m-d')]);
        }else{
            $query->andWhere('date <= :to_date', [':to_date' => date('y-m-d')]);
        }

        $query->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
