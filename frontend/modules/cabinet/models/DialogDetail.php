<?php

namespace frontend\modules\cabinet\models;

use frontend\models\User;
use Yii;

/**
 * This is the model class for table "dialog_detail".
 *
 * @property integer $id
 * @property integer $dialog_id
 * @property integer $user_id
 * @property integer $status
 * @property integer $updated_at
 */
class DialogDetail extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_ARCHIVE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dialog_detail';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dialog_id', 'user_id'], 'required'],
            [['dialog_id', 'user_id', 'status', 'updated_at'], 'integer'],
            ['user_id', 'exist', 'skipOnError' => true, 'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']],
            ['dialog_id', 'exist', 'skipOnError' => true, 'targetClass' => Dialog::className(),
                'targetAttribute' => ['dialog_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dialog_id' => 'Dialog ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDialog()
    {
        return $this->hasOne(Dialog::className(), ['id' => 'dialog_id']);
    }
}