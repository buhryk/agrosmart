<?php

namespace frontend\modules\cabinet\models;

use Yii;

/**
 * This is the model class for table "top".
 *
 * @property integer $id
 * @property string $model_name
 * @property integer $start
 * @property integer $finish
 * @property integer $refill_id
 * @property string $type
 */
class Top extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'top';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_name', 'start', 'finish', 'type'], 'required'],
            [['start', 'finish', 'refill_id', 'model_id', 'type'], 'integer'],
            [['model_name'], 'string', 'max' => 255],
        ];
    }




    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_name' => 'Model Name',
            'start' => 'Start',
            'finish' => 'Finish',
            'refill_id' => 'Refill ID',
            'type' => 'Type',
        ];
    }
}
