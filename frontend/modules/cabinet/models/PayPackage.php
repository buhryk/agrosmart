<?php

namespace frontend\modules\cabinet\models;

use backend\modules\commondata\models\SiteRole;
use backend\modules\package\models\Package;
use Yii;
use yii\base\Model;
use frontend\components\library\LiqPay;
use yii\web\Controller;
use frontend\modules\cabinet\models\PackageUser;

class PayPackage extends PayBase
{

    public function pay($post)
    {
        $decode = $this->decode($post);
        if(!$decode) return NULL;
        if($result = $this->getStatusPay($decode)) return $result;
        $orderData = $this->getOrder($decode->order_id);

        $package = Package::find()
            ->where(['id'=>$orderData['model_id']])
            ->one();

        $refillModel = new Refill();
        $transaction =Yii::$app->db->beginTransaction();

        try {
            $refillModel->sum = $decode->amount;
            $refillModel->user_id = $orderData['user_id'];
            $refillModel->type = Refill::TYPE_PACKAGE;
            $refillModel->order_id = $decode->order_id;
            $refillModel->save();
            
            $packageModel = new PackageUser();
            $packageModel->package_id = $orderData['model_id'];
            $packageModel->model_id = $orderData['user_id'];
            $packageModel->type = $package->role->type == SiteRole::TYPE_COMPANY ? PackageUser::TYPE_COMPANY : PackageUser::TYPE_USER;
            $packageModel->start = $this->getDateStart($orderData['model_id'], $packageModel->type, $orderData['user_id']);
            $packageModel->finish = $packageModel->start  + ($package->interim * 24*3600);
            $packageModel->refill_id = $refillModel->id;
            $packageModel->save();
            
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch(\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->statusList[$decode->status];
    }

    public function getDateStart($model_id, $type, $user_id)
    {
        $model = PackageUser::find()
            ->where(['package_id'=> $model_id])
            ->andWhere(['model_id' => $user_id])
            ->andWhere(['type' => $type])
            ->andWhere(['>', 'finish' , time()])
            ->one();

        return $model ? $model->finish : time();
    }

    public function getButton(Package $model)
    {
        $liqpay = new LiqPay($this->public_key, $this->private_key);

        return $liqpay->cnb_form(array(
            'version'        => '3',
            'amount'         => $model->price,
            'currency'       => 'UAH',
            'description'    => Yii::t('common', 'Оплата тарифа'),
            'order_id'       => $this->setOrder($model),
            'sandbox'        => '0',
            'customer'       => Yii::$app->user->id,
            'server_url'     => Yii::$app->urlManager->createAbsoluteUrl(['/cabinet/pay/package-result']),
            'result_url'     => Yii::$app->urlManager->createAbsoluteUrl(['/cabinet/pay/package-result']),
        ));
    }

    public function setOrder(Package $model)
    {
        $user =  Yii::$app->user->identity;
        $user_id = $user->company ? $user->company->id : $user->id;
        $order = $this->initDataOrder($user_id, $model->id);
        return implode('_', $order);
    }

    public function getOrder($order)
    {
        $data = explode('_', $order);
        return $this->initDataOrder($data[0],$data[1]);
    }

    public function initDataOrder($user_id, $model_id)
    {
        return [
            'user_id' => $user_id,
            'model_id' => $model_id,
            'time' => time(),
        ];
    }
}