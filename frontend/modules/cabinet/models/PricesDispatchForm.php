<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 28.02.2017
 * Time: 9:31
 */

namespace frontend\modules\cabinet\models;


use backend\modules\rubric\models\Rubric;
use backend\modules\rubric\models\RubricLang;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

class PricesDispatchForm extends Model
{
    public  $rubric_id;
    public  $filter;
    public  $value;
    public  $type;

    public function rules()
    {
        return [
            [['rubric_id', 'filter','value','type'], 'required'],
            [['value'], 'number'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'rubric_id' => Yii::t('common', 'Рубрика'),
            'filter' => Yii::t('common', 'Price'),
            'value' => Yii::t('common', 'Значение'),
            'type' => Yii::t('common', 'Тип'),

        ];
    }

    public static function getRubrics(){

        try{

            $rubrics = Rubric::find()
                ->where(['can_add_prices'=> Rubric::CAN_ADD_PRICES_YES])
                ->andWhere(['active'=>Rubric::ACTIVE_YES])
                ->all();

            return ArrayHelper::map($rubrics,'id','title');

        }
        catch (Exception $e){

            return $e;

        }

    }

    public function addFilter(){

        $model = new PricesDispath();

        $model->user_id = Yii::$app->user->identity->getId();
        $model->rubric_id = $this->rubric_id;
        $model->filter = $this->filter;
        $model->value = $this->value;
        $model->type = $this->type;
         if($model->save()){
            return true;
        }

        return $model->getErrors();

    }

}