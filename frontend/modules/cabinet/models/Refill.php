<?php

namespace frontend\modules\cabinet\models;

use Yii;


class Refill extends \yii\db\ActiveRecord
{
    const TYPE_ALLOTTED = 0;
    const TYPE_COMPANY = 1;
    const TYPE_PACKAGE = 2;

    public static function tableName()
    {
        return 'refill';
    }


    public function rules()
    {
        return [
            [['type', 'user_id'], 'required'],
            [['type', 'user_id', 'sum'], 'integer'],
            [['created_at', 'order_id'], 'string', 'max' => 255],
            [['num'], 'string', 'max' => 40],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата оплаты',
            'type' => 'Тип ',
            'num' => 'Num',
            'user_id' => 'Пользователь',
            'sum' => 'Сума'
        ];
    }
}
