<?php

namespace frontend\modules\cabinet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cabinet\models\Dialog;

/**
 * DialogSearch represents the model behind the search form about `frontend\modules\cabinet\models\Dialog`.
 */
class DialogSearch extends Dialog
{
    const DEFAULT_PAGE_SIZE = 10;
    const PAGE_SIZE_10_PER_PAGE = 10;
    const PAGE_SIZE_25_PER_PAGE = 25;
    const PAGE_SIZE_50_PER_PAGE = 50;

    public $page_size;
    public $currentUser;
    public $type;

    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->type = DialogDetail::STATUS_ACTIVE;
        $this->currentUser = isset($config['currentUser']) ? $config['currentUser'] : Yii::$app->user->identity;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id', 'created_at', 'page_size', 'type'], 'integer'],
            ['page_size', 'default', 'value' => self::DEFAULT_PAGE_SIZE],
            ['type', 'default', 'value' => DialogDetail::STATUS_ACTIVE]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Dialog::find();
        $this->load($params);

        $query->andFilterWhere(['and', ['or',
            ['from_user_id' => $this->currentUser->id],
            ['to_user_id' => $this->currentUser->id]
        ]]);

        if ($this->type && in_array($this->type, [DialogDetail::STATUS_ACTIVE, DialogDetail::STATUS_ARCHIVE])) {
            $query->joinWith('detail')
                ->andFilterWhere([DialogDetail::tableName().'.status' => $this->type]);
        } else {
            $query->joinWith('detail')
                ->andFilterWhere([DialogDetail::tableName().'.status' => DialogDetail::STATUS_ACTIVE]);
        }

        $query->orderBy('updated_at DESC');

        return $query;
    }

    public static function getPerPagePropertiesList()
    {
        return [
            self::PAGE_SIZE_10_PER_PAGE => self::PAGE_SIZE_10_PER_PAGE,
            self::PAGE_SIZE_25_PER_PAGE => self::PAGE_SIZE_25_PER_PAGE,
            self::PAGE_SIZE_50_PER_PAGE => self::PAGE_SIZE_50_PER_PAGE
        ];
    }
}