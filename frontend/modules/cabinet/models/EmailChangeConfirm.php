<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 13.02.2017
 * Time: 13:49
 */

namespace frontend\modules\cabinet\models;

use frontend\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

class EmailChangeConfirm extends Model
{
    /**
     * @var User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param  string $token
     * @param  array $config
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Отсутствует код подтверждения.');
        }
        $this->_user = User::findByEmailChangeToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Неверный токен.');
        }
        parent::__construct($config);
    }

    /**
     * Confirm email.
     *
     * @return boolean if email was confirmed.
     */
    public function confirmEmail()
    {
        $user = $this->_user;
        $user->email = $user->new_email;
        $user->new_email = null;

        $user->removeEmailConfirmToken();
        return $user->save();
    }


}