<?php
namespace frontend\modules\cabinet\models;

use yii\base\Model;
use common\models\User;
use yii\base\InvalidParamException;
/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $old_password;
    public $new_password;
    public $repeat_password;
    /**
     * @var \common\models\User
     */
    private $_user;
    /**
     * Creates a form model given a user model.
     */
    public function __construct($model)
    {
        $this->_user = $model;
        if (empty($this->_user->password_hash) || !is_string($this->_user->password_hash)) {
            throw new InvalidParamException('Old password record cannot be blank.');
        }
        if (!$this->_user) {
            throw new InvalidParamException('There is no such user.');
        }
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_password', 'new_password', 'repeat_password'], 'required'],
            ['new_password', 'string', 'min' => 6],
            ['repeat_password', 'compare', 'compareAttribute' => 'new_password'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'old_password' => \Yii::t('cabinet', 'OLD_PASSWORD'),
            'new_password' => \Yii::t('cabinet', 'NEW_PASSWORD'),
            'repeat_password' => \Yii::t('cabinet', 'REPEAT_PASSWORD'),
        ];
    }
    /**
     * Resets password.
     * @return bool if password was reset.
     */
     public function resetPassword()
    {
        $user = $this->_user;
        if ($user->validatePassword($this->old_password)) {

            $user->setPassword($this->new_password);
        } else {
            \Yii::$app->session->setFlash('error', 'Old password was incorrect.');
            return false;
        }
        return $user->save(false);
    }
}
