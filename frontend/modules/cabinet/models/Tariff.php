<?php

namespace frontend\modules\cabinet\models;

use Yii;

/**
 * This is the model class for table "tariff".
 *
 * @property integer $id
 * @property string $price
 * @property integer $count_deys
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $position
 * @property string $model_name
 */
class Tariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'model_name'], 'required'],
            [['price'], 'number'],
            [['count_deys', 'type', 'created_at', 'updated_at', 'position'], 'integer'],
            [['model_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'count_deys' => 'Count Deys',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'position' => 'Position',
            'model_name' => 'Model Name',
        ];
    }
}
