<?php

namespace frontend\modules\cabinet\models;


use Yii;
use yii\base\Model;
use backend\modules\tariff\models\Tariff;


class PullTop extends Model
{
    const TYPE_ALLOTTED = 'allotted';
    const TYPE_COMPLEX = 'complex';
    const TYPE_TOP = 'top';

    public $model_name;
    public $model_id;
    public $tariff_id;
    public $allotted;
    public $top;
    public $complex;
    public $type;
    public $currentModel;

    public function rules()
    {
        return [
            [['type'], 'string'],
            [['allotted', 'top', 'complex'], 'integer'],
        ];
    }

    public function initPage($params) 
    {
        $this->model_name = $params['model_name'];
        $this->model_id = $params['model_id'];
        $this->findModel();
    }
    
    protected function findModel()
    {
         if ($this->type == self::TYPE_ALLOTTED) {
            $this->currentModel = $this->allottedModel;
         }elseif($this->type == self::TYPE_TOP)  {
            $this->currentModel = $this->topModel;
        }elseif($this->type == self::TYPE_COMPLEX)  {
             $this->currentModel = $this->complexModel;
         }
        
    }

    public function getAllottedList()
    {
        return Tariff::find()
            ->where(['model_name' => $this->model_name])
            ->andWhere(['type' => Tariff::TYPE_ALLOTTED])
            ->orderBy('position')
            ->all();
    }

    public function getTopList()
    {
        return Tariff::find()
            ->where(['model_name' => $this->model_name])
            ->andWhere(['type' => Tariff::TYPE_TOP])
            ->orderBy('position')
            ->all();
    }

    public function getComplexList()
    {
        return Tariff::find()
            ->where(['model_name' => $this->model_name])
            ->andWhere(['type' => Tariff::TYPE_COMPLEX])
            ->orderBy('position')
            ->all();
    }
    
    public function getAllottedModel()
    {
        return Tariff::findOne($this->allotted);
    }

    public function getTopModel()
    {
        return Tariff::findOne($this->top);
    }

    public function getComplexModel()
    {
        return Tariff::findOne($this->complex);
    }
    
    public function getButton() 
    {
        $pay = new Pay();
        return $pay->getButton($this);
    }


}