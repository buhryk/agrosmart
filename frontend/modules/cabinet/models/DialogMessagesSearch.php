<?php

namespace frontend\modules\cabinet\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cabinet\models\DialogMessage;
use yii\web\NotFoundHttpException;

/**
 * DialogMessagesSearch represents the model behind the search form about `frontend\modules\cabinet\models\DialogMessage`.
 */
class DialogMessagesSearch extends DialogMessage
{
    public $dialog;

    public function rules()
    {
        return [
            [['dialog_id'], 'integer'],
            [['text'], 'safe'],
        ];
    }

    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->dialog = isset($config['dialog']) ? $config['dialog'] : null;

        if (!$this->dialog) {
            throw new NotFoundHttpException('Dialog not found');
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return mixed
     */
    public function search($params)
    {
        $query = DialogMessage::find();

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'dialog_id' => $this->dialog->primaryKey
        ]);

        $query->orderBy('created_at DESC');

        $query->andFilterWhere(['like', 'text', $this->text]);

        return $query;
    }
}