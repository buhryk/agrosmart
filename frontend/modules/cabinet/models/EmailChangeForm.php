<?php
namespace frontend\modules\cabinet\models;

use Yii;
use yii\base\Model;
use frontend\models\User;

/**
 * Password reset request form
 */
class EmailChangeForm extends Model
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
//            ['email', 'exist',
//                'targetClass' => '\frontend\models\User',
//                'filter' => ['status' => User::STATUS_ACTIVE],
//                'message' => 'There is no user with such email.'
//            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => Yii::$app->user->identity->email,
        ]);

        if (!$user) {

            return false;
        }

        if($user::validateNewEmail($this->email)){
           return false;
        }


        $user->generateEmailChangeToken($this->email);


        if (!$user->save()) {

            return false;
        }
            Yii::$app->mailer->compose('@frontend/modules/user/mails/emailChange', ['user' => $user])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($user->email)
                ->setSubject('Email confirmation for ' . Yii::$app->name)
                ->send();
        return true;
    }

}
