<?php

namespace frontend\modules\cabinet\models;

use Yii;

/**
 * This is the model class for table "accounting".
 *
 * @property integer $id
 * @property string $responsible
 * @property string $product
 * @property string $buyers
 * @property string $basis_delivery:
 * @property string $seller:
 * @property string $basis_purchase:
 * @property string $elevator
 * @property string $logist
 * @property string $numbercars_per_day
 * @property string $fpproval_date_line
 * @property string $currency
 * @property string $count_day
 * @property string $count
 * @property string $price
 * @property integer $exw_count
 * @property integer $exw_price
 * @property integer $meneger_count
 * @property integer $meneger_price
 * @property integer $traveling_expenses_count
 * @property integer $traveling_expenses_price
 * @property integer $counterparty_count
 * @property integer $counterparty_price
 * @property integer $broker_count
 * @property integer $broker_price
 * @property integer $elevator_days
 * @property integer $elevator_price
 * @property integer $elevator_count
 * @property integer $elevator_service_count
 * @property integer $elevator_service_price
 * @property integer $elevator_shipment_days
 * @property integer $elevator_shipment_price
 * @property integer $elevator_census_days
 * @property integer $elevator_census_price
 * @property integer $logistics_costs_f1_count
 * @property integer $logistics_costs_f1_price
 * @property integer $logistics_costs_f2_count
 * @property integer $logistics_costs_f2_price
 * @property integer $logistics_costs_count
 * @property integer $logistics_costs_price
 * @property integer $natural decline
 * @property integer $tax_burden_percent
 * @property integer $tax_burden_count
 * @property integer $upgrade_count
 * @property integer $upgrade_price
 * @property integer $cost_money_percent
 * @property integer $cost_money_count
 */
class Accounting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accounting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['responsible', 'product', 'buyers', 'seller', 'count', 'price'], 'required'],
            [['user_id'], 'integer'],
            [['count', 'price', 'natural_count'], 'number'],
            [['exw_count', 'exw_price', 'meneger_count', 'meneger_price', 'traveling_expenses_count',
                'traveling_expenses_price', 'counterparty_count', 'counterparty_price', 'broker_count',
                'broker_price', 'elevator_days', 'elevator_price', 'elevator_count', 'elevator_service_count',
                'elevator_service_price', 'elevator_shipment_days', 'elevator_shipment_price', 'elevator_census_days',
                'elevator_census_price', 'logistics_costs_f1_count', 'logistics_costs_f1_price', 'logistics_costs_f2_count',
                'logistics_costs_f2_price', 'logistics_costs_count', 'logistics_costs_price', 'natural_decline',
                'tax_burden_percent', 'tax_burden_count', 'upgrade_count', 'upgrade_price', 'cost_money_percent', 'cost_money_count'
            ], 'number'],
            [['responsible', 'product', 'buyers', 'basis_delivery', 'seller', 'basis_purchase', 'elevator', 'logist',
                'numbercars_per_day', 'fpproval_date_line', 'currency', 'count_day'
            ], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'responsible' => Yii::t('cabinet', 'Ответственный'),
            'product' => Yii::t('cabinet', 'Продукт'),
            'buyers' => Yii::t('cabinet', 'Покупатель'),
            'basis_delivery' => Yii::t('cabinet', 'Базис поставки'),
            'seller' => Yii::t('cabinet', 'Продавец'),
            'basis_purchase' => Yii::t('cabinet', 'Базис закупки'),
            'elevator' => Yii::t('cabinet', 'Элеватор'),
            'logist' => Yii::t('cabinet', 'Логист'),
            'numbercars_per_day' => Yii::t('cabinet', 'Кол-во вагонов в сутки '),
            'fpproval_date_line' => Yii::t('cabinet', 'Согласование очереди на дату'),
            'currency' => Yii::t('cabinet', 'Валюта'),
            'count_day' => Yii::t('cabinet', 'Кол-во дней сделка'),
            'count' => Yii::t('cabinet', 'Кол-во, т'),
            'price' => Yii::t('cabinet', 'Ст-сть ед., грн/т'),
        ];
    }

    public function getSum()
    {
        return $this->price * $this->count;
    }

    public function getNds()
    {
        return $this->sum / 6;
    }

    public function getNotNds()
    {
        return $this->sum / 1.2;
    }

    public function getExwSum()
    {
        return $this->exw_count * $this->exw_price;
    }

    public function getExwNds()
    {
        return $this->exwSum / 6;
    }

    public function getExwNotNds()
    {
        return $this->exwSum / 1.2;
    }

    public function getMenegerSum()
    {
        return $this->meneger_count * $this->meneger_price;
    }

    public function getTravelingExpensesSum()
    {
        return $this->traveling_expenses_count * $this->traveling_expenses_price;
    }

    public function getCounterpartySum()
    {
        return $this->counterparty_count * $this->counterparty_price;
    }

    public function getBrokerSum()
    {
        return $this->traveling_expenses_count * $this->traveling_expenses_price;
    }

    public function getElevatorSum()
    {
        return ($this->elevator_count * $this->elevator_service_price) / 30 * $this->elevator_days;
    }

    public function getElevatorServiceSum()
    {
        return $this->elevator_service_count * $this->elevator_service_price;
    }

    public function getElevatorShipmentSum()
    {
        return $this->elevator_shipment_days * $this->elevator_shipment_price;
    }

    public function getElevatorCensusSum()
    {
        return $this->elevator_census_days * $this->elevator_census_price;
    }

    public function getLogisticsSum()
    {
        return $this->logistics_costs_count * $this->logistics_costs_price;
    }

    public function getLogisticsF1Sum()
    {
        return $this->logistics_costs_f1_count * $this->logistics_costs_f1_price;
    }

    public function getLogisticsF2Sum()
    {
        return $this->logistics_costs_f2_count * $this->logistics_costs_f2_price;
    }

    public function getNaturalPrice()
    {
        return $this->naturalPercent * $this->price;
    }

    public function getNaturalPercent()
    {
        return $this->isNewRecord OR   $this->exw_count ? 0 : (1 - $this->count / $this->exw_count);
    }

    public function getNaturalSum()
    {
        return $this->natural_decline * $this->naturalPrice;
    }

    public function getTaxBurdenPrice()
    {
        return $this->price * $this->tax_burden_percent;
    }

    public function getTaxBurderSum()
    {
        return $this->taxBurdenPrice * $this->tax_burden_percent;
    }

    public function getUpgrade()
    {
        return ($this->meneger_price + $this->counterparty_price + $this->logistics_costs_f2_price + $this->traveling_expenses_price) * $this->upgrade_price;
    }

    public function getUpgradeSum()
    {
        return $this->upgrade_count*$this->upgrade;
    }

    /*
     * Стоимость денег %/год price
     */
    public function getCostMoney()
    {
        return $this->exw_count ? ($this->needMoney / $this->exw_count * ($this->cost_money_percent / 100) / 365 * $this->count_day) : 0;
    }

    public function getCostMoneySum()
    {
        return $this->cost_money_count * $this->costMoney;
    }

    public function getTotalSum()
    {
        return $this->exwSum
            + $this->menegerSum
            + $this->travelingExpensesSum
            + $this->counterpartySum
            + $this->brokerSum
            + $this->elevatorSum
            + $this->elevatorCensusSum
            + $this->elevatorServiceSum
            + $this->elevatorShipmentSum
            + $this->logisticsF1Sum
            + $this->logisticsF2Sum
            + $this->logisticsSum
            + $this->taxBurderSum
            + $this->upgradeSum;
    }

    public function getCostsSum()
    {
        return $this->menegerSum
            + $this->travelingExpensesSum
            + $this->counterpartySum
            + $this->brokerSum
            + $this->elevatorSum
            + $this->elevatorCensusSum
            + $this->elevatorServiceSum
            + $this->elevatorShipmentSum
            + $this->logisticsF1Sum
            + $this->logisticsF2Sum
            + $this->logisticsSum
            + $this->taxBurderSum
            + $this->upgradeSum
            + $this->costMoneySum;
    }

    /*
     * Потребность в денежных средствах
     */
    public function getNeedMoney()
    {
        return $this->exwSum 
            + $this->menegerSum
            + $this->travelingExpensesSum
            + $this->counterpartySum
            + $this->brokerSum
            + $this->elevatorSum
            + $this->elevatorCensusSum
            + $this->elevatorServiceSum
            + $this->elevatorShipmentSum
            + $this->logisticsF1Sum
            + $this->logisticsF2Sum
            + $this->logisticsSum
            + $this->taxBurderSum;
    }

    public function getTotalPrice()
    {
        return $this->totalSum / $this->exw_count;
    }

    public function getMarginalIncome()
    {
       return $this->totalPrice  - $this->exw_price;
    }

    public function getMarginalIncomeSum()
    {
        return $this->sum - $this->totalSum;
    }

    public function beforeSave($insert)
    {
        $this->user_id = Yii::$app->user->id;
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
}