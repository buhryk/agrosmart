<?php

namespace frontend\modules\cabinet\models;

use backend\modules\package\models\Package;
use frontend\models\Company;
use frontend\models\User;
use Yii;

/**
 * This is the model class for table "package_user".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $package_id
 * @property integer $type
 * @property integer $model_id
 * @property string $start
 * @property string $finish
 */
class PackageUser extends \yii\db\ActiveRecord
{
    
    const TYPE_COMPANY = 1;
    const TYPE_USER = 2;
    
    public static function tableName()
    {
        return 'package_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'package_id', 'type', 'model_id', 'start', 'finish', 'refill_id'], 'integer'],
            [['package_id', 'type', 'model_id', 'start', 'finish'], 'required'],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
            'package_id' => 'Пакеты',
            'type' => 'Тип',
            'model_id' => 'Пользователи',
            'start' => 'Старт',
            'finish' => 'Финиш',
            'detailType' => 'Тип'
        ];
    }

    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    public function getModel()
    {
        return self::TYPE_COMPANY == $this->type ? $this->hasOne(Company::className(), ['id' => 'model_id']) : $this->hasOne(User::className(), ['id' => 'model_id']);
    }
    
    public function getPackageTitle()
    {
        return isset($this->package) ? $this->package->title : '';
    }

    public function getTypeList()
    {
        return [
          self::TYPE_COMPANY => 'Компания',
          self::TYPE_USER => 'Пользователь',
        ];
    }

    public function getDetailType()
    {
        return isset($this->typeList[$this->type]) ? $this->typeList[$this->type] : '';
    }

    public function getUser()
    {
        if ($this->type == self::TYPE_COMPANY) {
            
        }
    }
}
