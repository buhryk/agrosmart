<?php

namespace frontend\modules\cabinet\models;

use frontend\modules\product\models\Advertisement;
use yii\base\Model;

class AdvertisementImageForm extends Model
{
    public $image;

    public function rules()
    {
        return [
            [['image'], 'required'],
            ['image', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024*1024*2]
        ];
    }

    public function saveImage(Advertisement $advertisement)
    {
        $images = [];
        $dir = \Yii::getAlias('@web') . 'uploads/products/';

        if ($this->image && !empty($this->image->name)) {
            $filename = $dir.time().rand(0,1000).'.'.$this->image->extension;
            $path = $this->image->saveAs($filename);

            if (!empty($this->image) && $path) {
                $images[] = [
                    'is_main' => false,
                    'path' => $filename
                ];
            }
        }

        if ($images) {
            $advertisement->saveImages($images);
        }

        return true;
    }
}