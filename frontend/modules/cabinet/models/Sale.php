<?php

namespace frontend\modules\cabinet\models;

use backend\modules\commondata\models\City;
use backend\modules\commondata\models\Currency;
use backend\modules\commondata\models\Region;
use backend\modules\rubric\models\RubricMeasurement;
use frontend\models\Company;
use Yii;
use frontend\models\CompanyUser;
use backend\modules\rubric\models\Rubric;
use common\helpers\GoogleMapsApiHelper;


/**
 * This is the model class for table "sale".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $rubric_id
 * @property string $volume
 * @property string $price
 * @property string $location
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CompanyUser $companyUser
 * @property Rubric $rubric
 */
class Sale extends \yii\db\ActiveRecord
{
    private $oldAttrs = array();

    const TYPE_ZAKUPKA = 1;
    const TYPE_PRODAZHA = 0;

    const TABLE_CURRENCY = 2;
    const TABLE_UAH = 1;

    const SALE_IS_LIST = 1;

    public static function OffersTypes()
    {
        return [
            self::TYPE_PRODAZHA => Yii::t('common', 'Продажа'),
            self::TYPE_ZAKUPKA => Yii::t('common', 'Закупка')
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sale';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rubric_id', 'address',  'volume', 'price', 'currency_id','measurement_id', 'date', 'city_id'], 'required'],
            [['company_id', 'rubric_id', 'created_at', 'updated_at', 'measurement_id', 'currency_id', 'user_created', 'user_updated', 'oblast_id', 'city_id', 'type', 'is_list'], 'integer'],
            [['volume', 'price'], 'number', 'min' => '0'],

            [['address', 'coordinates', 'description_address'], 'string', 'max' => 255],
            [['date'], 'safe'],
            ['rubric_id', 'validateRubric'],
            [['address'], 'validateLocation'],
            [['oblast_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['oblast_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['rubric_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rubric::className(), 'targetAttribute' => ['rubric_id' => 'id']],
        ];
    }

    public function validateRubric($attribute, $params)
    {
        $child = Rubric::find()->andWhere(['parent_id' => $this->rubric_id])->exists();
        if ($child) {
            $this->addError('rubric_id', Yii::t('cabinet', 'Это не конечная рубрика'));
        }
    }

    public function validateLocation($attribute, $params)
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();

        if ($this->location) {

            $coordinates = $googleMapsApiHelper->getLocationByAddress($this->location);

            if(!$coordinates) {
                $address = $this->city->title.', '.$this->address;
                $coordinates = $googleMapsApiHelper->getLocationByAddress($address);
            }
            if(!$coordinates) {
                $address = $this->city->title;
                $coordinates = $googleMapsApiHelper->getLocationByAddress($address);
            }

            if (!$coordinates) {

                $this->addError($attribute, Yii::t('instruments', 'Wrong location', ['field' => $this->getAttributeLabel($attribute)]));
            }
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('cabinet', 'ID'),
            'company_id' => Yii::t('cabinet', 'COMPANY'),
            'rubric_id' => Yii::t('cabinet', 'RUBRIC'),
            'volume' => Yii::t('cabinet', 'VOLUME'),
            'price' => Yii::t('cabinet', 'PRICE'),
            'address' => Yii::t('cabinet', 'LOCATION'),
            'created_at' => Yii::t('cabinet', 'CREATED'),
            'updated_at' => Yii::t('cabinet', 'UPDATED'),
            'currency_id' => Yii::t('cabinet', 'CURRENCY'),
            'city_id' => Yii::t('cabinet', 'CITY'),
            'measurement_id' => Yii::t('cabinet', 'MESUREMENT'),
            'date' => Yii::t('cabinet', 'DATE'),
            'description_address' => Yii::t('common', 'Дополнительное описание месторасположения')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubric()
    {
        return $this->hasOne(Rubric::className(), ['id' => 'rubric_id']);
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getOblast()
    {
        return $this->hasOne(Region::className(), ['id' => 'oblast_id']);
    }

    public function getGeneralPrice()
    {
        return $this->currency_id == Currency::getMainCurrency()->id ? $this->price : ($this->price / $this->currency->weight) * Currency::getMainCurrency()->weight;
    }

    public function getLocation()
    {
        $city = $this->city;
        $area = (($area = $city->area) !== null)  ? $area : null;
        $region = $city->region;

        return $region->title.', '. $city->title.', '.$this->address;
    }

    public function getMeasurement()
    {
        return $this->hasOne(RubricMeasurement::className(), ['id' => 'measurement_id']);
    }

    public function getMeasurementTitle()
    {
        return isset($this->measurement) ? $this->measurement->short_title : '';
    }

    public function getMeasurementWeight()
    {
        return isset($this->measurement) ? $this->measurement->weight : '';
    }
    
    public static function getCompanySaleList($company_id, $type)
    {
        return Sale::find()
            ->andWhere(['company_id' => $company_id])
            ->andWhere(['type' => $type])
            ->andWhere(['is_list' => self::SALE_IS_LIST])
            ->andWhere(['between', 'date', date('Y-m-d', time()-(60*60*24*30)), date('Y-m-d', time()+(60*60*24*30))])
            ->orderBy('date DESC, updated_at DESC')
            ->limit(2)
            ->all();
    }

    public function getGeneralVolume()
    {
        if (isset(RubricMeasurement::getDefault($this->measurement->category_id)->id)) {
            return $this->measurement_id == RubricMeasurement::getDefault($this->measurement->category_id)->id
                ? $this->volume : ($this->volume * $this->measurement->weight) / RubricMeasurement::getDefault($this->measurement->category_id)->weight;
        } else {
            return $this->volume;
        }

    }

    public function beforeSave($insert)
    {
        
        if ($this->isNewRecord) {
            $this->user_created = Yii::$app->user->id;
        } else {
            $this->user_updated = Yii::$app->user->id;
        }

        $city = City::find()->where(['id' => $this->city_id])->one();
        $this->oblast_id = $city->oblast_id;
        $this->company_id = Yii::$app->user->identity->companyUser->company_id;

        $googleMapsApiHelper = new GoogleMapsApiHelper();
        $coordinates = $googleMapsApiHelper->getLocationByAddress($this->location);

        if (!$coordinates) {
            $address = $this->city->title.', '.$this->address;
            $coordinates = $googleMapsApiHelper->getLocationByAddress($address);
        }

        if(!$coordinates) {
            $address = $this->city->title;
            $coordinates = $googleMapsApiHelper->getLocationByAddress($address);
        }

        if ($coordinates) {
            $this->coordinates = implode(',', $coordinates);
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert,$changedAttributes)
    {
        if (!$this->isNewRecord) {
            $newAttrs = $this->getAttributes();
            $oldAttrs = $this->getOldAttributes();
            if ($newAttrs['price'] !== $oldAttrs['price']){

                $model = new PricesDispath();

                $sendStatus = $model->sendSaleNotificationData($newAttrs['rubric_id'],$newAttrs['type']);

                if(!$sendStatus) return false;

            }
        } else {
            $newAttrs = $this->getAttributes();

            $model = new PricesDispath();

            $sendStatus = $model->sendSaleNotificationData($newAttrs['rubric_id'],$newAttrs['type']);

            if(!$sendStatus) return false;
        }

        return parent::afterSave($insert,$changedAttributes);
    }

    public function afterFind()
    {
        // Save old values
        $this->setOldAttributes($this->getAttributes());

        return parent::afterFind();
    }

    public function getOldAttributes()
    {
        return $this->oldAttrs;
    }

    public function setOldAttributes($attrs)
    {
        $this->oldAttrs = $attrs;
    }

}
