<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 01.02.2018
 * Time: 13:12
 */

namespace frontend\modules\cabinet\models;


use common\components\ExelPrice;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class PriceForm extends Model
{
    public $exel;
    public $priceModel;

    public function rules()
    {
        return [
//            [ 'exel', 'required', 'message' => Yii::t('price', 'select a file') ],
            [['exel'], 'file', 'extensions' => 'xls, xlsx', 'wrongExtension' => ''],
        ];
    }

    public function upload($type)
    {
        $fileName = Yii::getAlias('@web').$this->exel;
        $exel = new ExelPrice();

        if ($this->exel->saveAs($fileName)) {
            $this->priceModel = $exel->import($fileName, $type, $this->priceModel);
            unlink($fileName);

            if (ExelPrice::$arr){
                return false;
            }else{
                return true;
            }
        }
    }
}