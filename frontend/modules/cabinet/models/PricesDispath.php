<?php

namespace frontend\modules\cabinet\models;

use backend\modules\commondata\models\Currency;
use backend\modules\rubric\models\Rubric;
use frontend\models\User;
use frontend\modules\notification\models\Notification;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "prices_dispath".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $rubric_id
 * @property integer $filter
 * @property double $value
 */
class PricesDispath extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const EQUAL_VALUE = 0;
    const EQUAL_TEXT = 'Равно';

    const MORE_VALUE = 1;
    const MORE_TEXT = 'Больше';

    const LESS_VALUE = 2;
    const LESS_TEXT = 'Меньше';


    public static function tableName()
    {
        return 'prices_dispath';
    }

    public static function statusFilters()
    {
        return [
            self::EQUAL_VALUE => Yii::t('common', self::EQUAL_TEXT),
            self::MORE_VALUE => Yii::t('common', self::MORE_TEXT),
            self::LESS_VALUE => Yii::t('common', self::LESS_TEXT),
        ];
    }
    public static function statusFiltersValue()
    {
        return [
            self::EQUAL_VALUE => '=',
            self::MORE_VALUE => '>',
            self::LESS_VALUE => '<',
        ];
    }

        /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'rubric_id', 'filter','type'], 'required'],
            [['user_id', 'rubric_id', 'filter','type'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'rubric_id' => Yii::t('common', 'Рубрика'),
            'filter' => Yii::t('common', 'Price'),
            'value' => Yii::t('common', 'Значение'),
            'type' => Yii::t('common', 'Тип'),

        ];
    }

    public function getRubric()
    {
        return $this->hasOne(Rubric::className(), ['id' => 'rubric_id']);
    }

    public static function sendUSerNotification($id)
    {
        $model = static::find()->where(['id'=>$id])->one();
        $currency = Currency::getMainCurrency()->weight;

        $data_sale = Sale::find()
            ->select(Sale::tableName().'.*, IF(`currency_id`=1,`price`, `price`*'.$currency.') as price , CONCAT(sale.`city_id`, sale.`address`) as place')
            ->where([static::statusFiltersValue()[$model->filter], 'price', $model->value])
            ->andWhere(['type'=>$model->type])
            ->andWhere(['rubric_id' => $model->rubric_id])
            ->andWhere(['>', 'date' , date('Y-m-d', time() - 3600 * 24 * 30)])
            ->groupBy('place')
            ->orderBy('price DESC')
            ->all();

        static::sendEmail($data_sale);
    }

    public static function sendEmail($data)
    {
        $user = Yii::$app->user->identity;

        Yii::$app->mailer->compose(
            ['html' => '@frontend/modules/user/mails/prices-notification'],
            ['data' => $data, 'company' => $user->company]
        )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user->email)
            ->setSubject(Yii::t('common', 'Уведомление о ценах ') . Yii::$app->name)
            ->send();
        return true;
    }

    public function sendNotification($id,$data, $company_id)
    {
        $user = User::findIdentity($id);
        $company =  $user->company;
        if ($company_id != $company->id) {
            $this->sessionSetNotification($user->id);

            return Yii::$app->mailer->compose(    ['html' => '@frontend/modules/user/mails/prices-notification'], ['data' => $data, 'company' => $company])
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($user->email)
                ->setSubject(Yii::t('common', 'Уведомление о ценах ') . Yii::$app->name)
                ->send();
        }

    }

    public function sendSaleNotificationData($rubric_id,$type)
    {
        $priceNotificationItems = $this->getPricesNotificationItems($rubric_id,$type);

        if(!$priceNotificationItems) return false;
        $company_id = Yii::$app->user->identity->company->id;
        foreach ($priceNotificationItems as $item){

            $saleData = $this->getSaleNotificationData($item->filter, $type, $item->value, $item->rubric_id);

            if($saleData){
                $this->sendNotification($item->user_id,$saleData, $company_id);
            }
            else return false;

        }

        return true;

    }

    public function getPricesNotificationItems($rubric_id,$type)
    {
        return $this::find()
            ->andWhere(['rubric_id'=>$rubric_id])
            ->andWhere(['type'=>$type])
            ->andWhere(['<>','user_id', Yii::$app->user->identity->getId()])
            ->all();
    }

    public function getSaleNotificationData($filter,$type,$value, $rubric_id)
    {
        $currency = Currency::getMainCurrency()->weight;
        $lastDate = date('Y-m-d', time() - 3600 * 24 * 30);
        return Sale::find()
            ->select(Sale::tableName().'.*, IF(`currency_id`=1,`price`, `price`*'.$currency.') as price, , CONCAT(sale.`city_id`, sale.`address`) as place')
            ->andWhere(['>', 'date' , $lastDate])
            ->andWhere(['rubric_id' => $rubric_id])
            ->where([static::statusFiltersValue()[$filter], 'price', $value])
            ->andWhere(['type'=>$type])
            ->groupBy('place')
            ->all();
    }

    public function sessionSetNotification($user_id)
    {
        $model = new Notification();

        $model->target_id = $user_id;
        $model->type = Notification::TYPE_USER_MESSAGE;
        $model->status = Notification::STATUS_NEW;
        $model->message = Yii::t('cabinet','New prices list on your email');

        if ($model->save()) return true;
            else return $model->getErrors();

    }
}
