<?php

namespace frontend\modules\cabinet\models;

/**
 * This is the ActiveQuery class for [[Subsidiary]].
 *
 * @see Subsidiary
 */
class SubsidiaryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Subsidiary[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Subsidiary|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
