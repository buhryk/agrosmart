<?php

namespace frontend\modules\cabinet\models;

use frontend\models\User;
use frontend\modules\cabinet\models\Dialog;
use Yii;

/**
 * This is the model class for table "dialog_message".
 *
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $text
 * @property integer $status
 * @property integer $dialog_id
 */
class DialogMessage extends \yii\db\ActiveRecord
{
    const STATUS_NOT_VIEWED = 0;
    const STATUS_VIEWED = 1;

    public static function tableName()
    {
        return 'dialog_message';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dialog_id', 'from_user_id', 'to_user_id', 'text'], 'required'],
            [['dialog_id', 'from_user_id', 'to_user_id', 'status', 'created_at'], 'integer'],
            [['text'], 'string', 'min' => 1, 'max' => 2000],
            ['dialog_id', 'exist', 'skipOnError' => true, 'targetClass' => Dialog::className(),
                'targetAttribute' => ['dialog_id' => 'id']],
            ['from_user_id', 'exist', 'skipOnError' => true, 'targetClass' => User::className(),
                'targetAttribute' => ['from_user_id' => 'id']],
            ['to_user_id', 'exist', 'skipOnError' => true, 'targetClass' => User::className(),
                'targetAttribute' => ['to_user_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_NOT_VIEWED]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_user_id' => 'Author',
            'to_user_id' => 'Receiver',
            'text' => Yii::t('common', 'Message text'),
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    public function getReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    public function getDialog()
    {
        return $this->hasOne(Dialog::className(), ['id' => 'dialog_id']);
    }
}