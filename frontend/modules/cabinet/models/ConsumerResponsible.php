<?php

namespace frontend\modules\cabinet\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "consumer_responsible".
 *
 * @property integer $id
 * @property integer $consumer_id
 * @property integer $user_id
 * @property integer $type
 */
class ConsumerResponsible extends \yii\db\ActiveRecord
{
    const TYPE_USER = 1;
    const TYPE_COMPANY = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consumer_responsible';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['consumer_id', 'user_id'], 'required'],
            [['consumer_id', 'user_id', 'type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'consumer_id' => 'Consumer ID',
            'user_id' => 'User ID',
            'type' => 'Type',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
