<?php

namespace frontend\modules\cabinet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cabinet\models\Accounting;

/**
 * AccountingSearch represents the model behind the search form about `frontend\modules\cabinet\models\Accounting`.
 */
class AccountingSearch extends Accounting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'exw_count', 'exw_price', 'meneger_count', 'meneger_price',
                'traveling_expenses_count', 'traveling_expenses_price',
                'counterparty_count', 'counterparty_price', 'broker_count',
                'broker_price', 'elevator_days', 'elevator_price', 'elevator_count',
                'elevator_service_count', 'elevator_service_price', 'elevator_shipment_days',
                'elevator_shipment_price', 'elevator_census_days', 'elevator_census_price',
                'logistics_costs_f1_count', 'logistics_costs_f1_price', 'logistics_costs_f2_count',
                'logistics_costs_f2_price', 'logistics_costs_count', 'logistics_costs_price',
                'natural_decline', 'tax_burden_percent', 'tax_burden_count', 'upgrade_count',
                'upgrade_price', 'cost_money_percent', 'cost_money_count'], 'integer'],
            [['responsible', 'product', 'buyers', 'seller', 'elevator'], 'safe'],
            [['count', 'price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Accounting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'count' => $this->count,
            'price' => $this->price,

        ]);
        $query->andFilterWhere(['like', 'product', $this->product]);
        $query->andFilterWhere(['like', 'buyers', $this->buyers]);
        $query->andFilterWhere(['like', 'seller', $this->seller]);
        $query->andFilterWhere(['like', 'elevator', $this->elevator]);
        $query->andWhere(['user_id' => Yii::$app->user->id]);

        return $dataProvider;
    }
}
