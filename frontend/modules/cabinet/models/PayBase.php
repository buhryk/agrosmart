<?php

namespace frontend\modules\cabinet\models;

use Yii;
use yii\base\Model;
use frontend\components\library\LiqPay;
use yii\web\Controller;

abstract class PayBase
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILURE = 'failure';
    const STATUS_ERROR = 'error';
    const STATUS_SANDBOX = 'sandbox';
    const STATUS_PREAPARED = 'prepared';
    const STATUS_WAIT_ACCEPT = 'wait_accept';
    const STATUS_OPT_VERIFY = 'otp_verify';
    const STATUS_3DS_VERIFY = '3ds_verify';
    const STATUS_CVV_VERIFY = 'cvv_verify';
    const STATUS_PHONE_VERIFY = 'phone_verify';
    const STATUS_IVN_VERIFY = 'ivr_verify';
    const STATUS_PIN_VERIFY = 'pin_verify';
    const STATUS_CAPTCHA_VERIFY = 'captcha_verify';
    const STATUS_PASSWORD_VERIFY = 'password_verify';
    const STATUS_SENDERAPP = 'senderapp_verify';
    const STATUS_PROCESSING = 'processing';
    const STATUS_PREPARED = 'prepared';
    const STATUS_WAIT_SECURE = 'wait_secure';
    const STATUS_REVERS = 'reversed';


    public $public_key='i4612947775';
    public $private_key='DVASFwXQCeMt8TNpxy7xOVO1VjiqqYQ7vNeOPhpU';
    public $username;
    public $order;

    public $statusList=[
        self::STATUS_SUCCESS        => 'Успешный платеж',
        self::STATUS_ERROR          => 'Неуспешный платеж. Некорректно заполнены данные',
        self::STATUS_FAILURE        => 'Неуспешный платеж',
        self::STATUS_SANDBOX        => 'Тестовый платеж',

        self::STATUS_PREAPARED      => 'Подготовленный платеж',
        self::STATUS_WAIT_ACCEPT    => 'Деньги с клиента списаны, но магазин еще не прошел проверку. Если магазин не пройдет активацию в течение 90 дней, платежи будут автоматически отменены',
        self::STATUS_OPT_VERIFY     => 'Требуется OTP подтверждение клиента. OTP пароль отправлен на номер телефона Клиента. 
                                        Для завершения платежа, требуется выполнить otp_verify.',
        self::STATUS_3DS_VERIFY     => 'Требуется 3DS верификация. 
                                        Для завершения платежа, требуется выполнить 3ds_verify.',
        self::STATUS_PHONE_VERIFY   => 'Ожидается ввод телефона клиентома',
        self::STATUS_IVN_VERIFY     => 'Ожидается подтверждение звонком ivr',
        self::STATUS_PIN_VERIFY     => 'Ожидается подтверждение pin-code',
        self::STATUS_CAPTCHA_VERIFY => 'Ожидается подтверждение captcha',
        self::STATUS_PASSWORD_VERIFY=> 'Ожидается подтверждение пароля приложения Приват24',
        self::STATUS_SENDERAPP      => 'Ожидается подтверждение в приложении Sender',
        self::STATUS_PROCESSING     => 'Платеж обрабатывается',
        self::STATUS_PREAPARED      => 'Платеж создан, ожидается его завершение отправителем',
        self::STATUS_WAIT_SECURE    => 'Платеж на проверке',
        self::STATUS_REVERS    => 'Платеж возвращен',
    ];


    public function decode($post)
    {
        Yii::info($post, 'pay');
        $decode = base64_decode($post['data']);
        $decode = json_decode($decode);

        $sign = base64_encode( sha1(
            $this->private_key .
            $post['data'] .
            $this->private_key
            , 1 ));

        if ($sign != $post['signature']) {
            return NULL;
        } else {
            return $decode;
        }
    }

    public function getStatusPay($decode)
    {
        if ($decode->status != self::STATUS_SUCCESS) return $this->statusList[$decode->status];
        if (Refill::find()->where(['order_id' => $decode->order_id])->one()) return $this->statusList[$decode->status];
    }
    
    abstract public function getOrder($model);


}