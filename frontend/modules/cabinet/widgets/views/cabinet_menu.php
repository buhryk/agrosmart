<?php
use frontend\assets\CabinetAsset;
use yii\helpers\Url;
use yii\bootstrap\Nav;
CabinetAsset::register($this);
$current_url = Yii::$app->request->url;
?>
<div class="op-button">
    <button type="button" class="sr-btn">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <img class="sr_close" src="/images/important-close.png" alt="important-close">
</div>
<?php foreach ($items as $key => $item) { ?>
    <?php if (isset($item['items']) && $item['items']) { ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <p data-toggle="collapse" data-parent="#accordion" href="#collapseOne_<?=$key; ?>"><?= $item['label']; ?>
                    <i class="fa fa-angle-down panel__icon" aria-hidden="true"></i>
                </p>
            </div>
            <?php $class = ''; ?>
            <?php foreach ($item['items'] as $subitem) {
                if (stristr($current_url, $subitem['url'][0]) !== false) {
                    $class = 'in';
                    break;
                }
            } ?>
            <div id="collapseOne_<?=$key; ?>" class="panel-collapse collapse <?= $class; ?>">
                <ul class="panel-collapse__inner">
                    <?php foreach ($item['items'] as $subitem) { ?>
                        <?php $active = ''; ?>
                        <?php if (stristr($current_url, $subitem['url'][0]) !== false) {
                            $active = 'active';
                        } ?>
                        <li class="panel-collapse__link-block <?= $active; ?>">
                            <div class="panel__indicator"></div>
                            <a class="panel__link" href="<?=Url::to([$subitem['url'][0]]); ?>"><?= $subitem['label']; ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    <?php } else { ?>
        <?php $active = ''; ?>
        <?php if (stristr($current_url, $item['url'][0]) !== false) {
            $active = 'active';
        } ?>
        <div class="panel panel-default <?= $active; ?>">
            <div class="panel-heading transition">
                <p class="panel-title">
                    <a href="<?=Url::to([$item['url'][0]]); ?>"><?= $item['label']; ?></a>
                </p>
            </div>
        </div>
    <?php } ?>
<?php } ?>
<div>
    <?php
    $packageUser = Yii::$app->userRole->packageUser;
     if($packageUser) {
         echo Yii::t('cabinet', 'Ваш тарифный план действует до : {date} <a href="{url}">Подробнее </a>',
             ['date' => date('Y-m-d H:i' ,$packageUser->finish), 'url' => Url::to(['/cabinet/tariff/view', 'id' => $packageUser->package_id])]);
     }
    ?>
</div>