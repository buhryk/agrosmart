<?php

namespace frontend\modules\cabinet\widgets;

use backend\modules\price2\models\Price;
use backend\modules\setting\models\Setting;
use common\helpers\MyHelpers;
use yii\bootstrap\Widget;
use Yii;
use frontend\modules\instruments\models\InstrumentGet;
use yii\helpers\Url;

class CabinetMenu extends Widget
{
    public $company;

    public function init()
    {
        $this->company = !Yii::$app->user->isGuest ? Yii::$app->user->identity->company : null;
    }

    public function run()
    {
        //echo "<pre>";
        //var_dump( $this->company);
        /*$items = [
           ['label' => 'Главная личного кабинета', 'url' => ['#']],
           ['label' => Yii::t('cabinet', 'COMPANY_PROFILE'), 'url' => ['#'], 'items' => [
               ['label' => Yii::t('cabinet', 'ABOUT_HTML'), 'url' => ['/cabinet/company/profile']],
               ['label' => Yii::t('cabinet', 'SUBSIDIARIES_AND_CONTACTS'), 'url' => ['/cabinet/subsidiary/index']],
               ['label' => Yii::t('cabinet', 'USERS'), 'url' => ['/cabinet/company-user/index']],
           ]],
           ['label' => Yii::t('cabinet', $this->company ? 'Company products and services' : 'MY_GOODS_AND_SERVICES'), 'url' => ['/cabinet/advertisement/index']],
           ['label' => Yii::t('cabinet', 'DIALOGS'), 'url' => ['#']],
           ['label' => Yii::t('cabinet', 'CONTACT_DATABASE'), 'url' => ['#']],
           ['label' => Yii::t('cabinet', 'MY_PROFILE'), 'url' => ['/cabinet/user/profile']],
           ['label' => Yii::t('cabinet', 'FAVORITES'), 'url' => ['#']],
        ];*/
        $items[] = ['label' => Yii::t('cabinet', 'Главная личного кабинета'), 'url' => ['/cabinet/default/index']];
        if ($this->company) {
            $items[] = ['label' => Yii::t('cabinet', 'COMPANY_PROFILE'), 'url' => ['#'], 'items' => [
                ['label' => Yii::t('cabinet', 'ABOUT_HTML'), 'url' => ['/cabinet/company/profile']],
                ['label' => Yii::t('cabinet', 'SUBSIDIARIES_AND_CONTACTS'), 'url' => ['/cabinet/subsidiary/index']],
                ['label' => Yii::t('cabinet', 'USERS'), 'url' => ['/cabinet/company-user/index']],
            ]];
            if (InstrumentGet::instrumentShow(16)) {

                $setting = Setting::find()->where(['alias' => 'company-list'])->one();
                $company = MyHelpers::multiexplode($setting->value);

                if (in_array($this->company->id, $company)){
                    $items[] = ['label' => Yii::t('price', 'My price'), 'url' => ['#'], 'items' => [
                        ['label' => Yii::t('cabinet', 'PRODAZHA'), 'url' => ['/cabinet/sale/index'] ],
                        ['label' => Yii::t('cabinet', 'ZAKUPKA'), 'url' => ['/cabinet/sale/zakupka'] ],
                        ['label' => Yii::t('price', 'selection by price'), 'url' => [ '/cabinet/prices-dispatch/index'] ],
                    ]];
                }

                $items[] = ['label' => Yii::t('common', 'Prices'), 'url' => ['#'], 'items' => [
                    ['label' => Yii::t('price', 'sale'), 'url' => ['/cabinet/price/index?type='.Price::TYPE_SALE] ],
                    ['label' => Yii::t('price', 'purchase'), 'url' => [ '/cabinet/price/index?type='.Price::TYPE_PURCHASE] ],
                ]];
            }

        }

        $items[] = ['label' => Yii::t('cabinet', $this->company ? 'Company products and services' : 'MY_GOODS_AND_SERVICES'), 'url' => ['/cabinet/product/index']];
        $items[] = ['label' => Yii::t('cabinet', 'Личные данные'), 'url' => ['#'], 'items' => [

            ['label' => Yii::t('cabinet', 'Редактировать профиль'), 'url' => ['/cabinet/user/profile']],
            ['label' => Yii::t('cabinet', 'Измененить email'), 'url' => ['/cabinet/user/change-email']],
            ['label' => Yii::t('cabinet', 'Изменить пароль'), 'url' => ['/cabinet/user/change-password']],

        ]];
        $items[] = ['label' => Yii::t('cabinet', 'FAVORITES'), 'url' => ['/cabinet/favourites/index']];
        $items[] = ['label' => Yii::t('cabinet', 'DIALOGS'), 'url' => ['/cabinet/dialog/index']];
        $items[] = ['label' => Yii::t('cabinet', 'Accounting transactions'), 'url' => ['#'], 'items' => [
            ['label' =>  Yii::t('cabinet', 'Deal calculator'), 'url' => ['/cabinet/accounting/create']],
            ['label' => Yii::t('cabinet', 'Transaction list'), 'url' => ['/cabinet/accounting/index']],
            ]];
        $items[] = ['label' => Yii::t('cabinet', 'Уведомления'), 'url' => ['/notification/user-notify/index']];
        if(isset($this->company)) {
            $items[] = ['label' => Yii::t('cabinet', 'Тарифы'), 'url' => ['/cabinet/tariff/index']];
        }

        
        
        
       return $this->render('cabinet_menu', ['items'=>$items]);
    }
}
