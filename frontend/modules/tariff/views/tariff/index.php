<?php
use yii\widgets\Breadcrumbs;

if (!$this->title) {
    $this->title = $model->title. ' | '.Yii::$app->params['titlePrefix'];
    $this->registerMetaTag(['name'=>'description', 'content'=> $model->title.' | '.Yii::$app->params['descriptionPrefix']]);
}

$category = $model->category;

$this->params['breadcrumbs'][] = $model->title;
?>

<div class="bread">
    <h1><?= $model->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dstu">
    <div class="row">
        <div class="col-sm-9 mt45">
            <div class=" rate">
                <?php foreach ($models as $item):?>
                    <?php if(count($item->packageList)>=1): ?>

                        <h2><?=Yii::t('common', 'Тарифы для '.$item->name ) ?></h2>
                        <?php foreach ($item->packageList as $child): ?>
                            <?=$this->render('_item', ['model' => $child]); ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
       
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</div>