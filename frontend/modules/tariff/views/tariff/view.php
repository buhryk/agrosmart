<?php
use yii\widgets\Breadcrumbs;



if (!$this->title) {
    $this->title = $model->title. ' | '.Yii::$app->params['titlePrefix'];
    $this->registerMetaTag(['name'=>'description', 'content'=> $model->title.' | '.Yii::$app->params['descriptionPrefix']]);
}

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Тарифы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;

$package = Yii::$app->userRole->package;

?>

<div class="bread">
    <h1><?= $model->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container rate">
    <div class="row">
        <p>&nbsp;</p>

        <div class="col-sm-9">
            <div class="col-sm-12">
               
                <div class="row">
                    <div class="col-sm-3">
                        <p><?=Yii::t('cabinet', 'Количество людей') ?>:</p>
                    </div>
                    <div class="col-sm-9">
                        <p><strong><?=$model->count_user ?></strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p><?=Yii::t('cabinet', 'Количество дней') ?>:</p>
                    </div>
                    <div class="col-sm-9">
                        <p><strong><?=$model->interim ?></strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p><?=Yii::t('cabinet', 'Описание') ?></p>
                    </div>
                    <div class="col-sm-9">
                        <?=$model->text ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <p style="text-transform: uppercase;"><?=Yii::t('cabinet', 'Всего к оплате') ?>:</p>
                    </div>
                    <div class="col-sm-9">
                        <p><strong style="font-size: 25px;"><?=$model->price ?></strong> грн</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <a class="btn-green" href="<?=\yii\helpers\Url::to(['/cabinet/tariff/view', 'id' =>1]) ?>">
                            <?=Yii::t('common', 'Подключить') ?>
                        </a>
                    </div>
                    <div class="col-sm-9">
                        <a href="<?=\yii\helpers\Url::to(['index']) ?>">
                            <?=Yii::t('cabinet', 'Вернуться к тарифам') ?>
                        </a>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</div>
