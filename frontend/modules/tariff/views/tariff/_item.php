<?php
use yii\helpers\Url;

?>

<div class="col-md-4 col-sm-6 block-rate">
    <div class="bg-full">
        <h3 class="rate-block-title"><?=$model->title ?></h3>
        <div  class="price" style="text-align: center">
            <?php if($model->old_price): ?>
                <span class="old-price">
                   <strike>  <?=$model->old_price ?>грн </strike>
                </span>
                <br>
            <?php endif; ?>
            <?=$model->price ?> грн
        </div>
        <div class="description">
            <?=$model->description ?>
        </div>
        <div class="more">
            <a href="<?=Url::to(['view', 'id' => $model->id]) ?>" class="btn-green" >
                <?=Yii::t('cabinet', 'Падробнее') ?>
            </a>
        </div>
    </div>
</div>