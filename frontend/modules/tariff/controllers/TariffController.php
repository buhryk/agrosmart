<?php

namespace frontend\modules\tariff\controllers;

use backend\modules\commondata\models\SiteRole;
use backend\modules\package\models\Package;
use backend\modules\page\models\Page;
use backend\modules\seo\models\Seo;
use frontend\modules\cabinet\models\Tariff;
use yii\web\NotFoundHttpException;

class TariffController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\PageCache',
                'only' => ['index'],
                'variations' => [
                    \Yii::$app->language,
                ],
                'duration' => 3600,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = Page::findOne(1);

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $seo = ($seoModel = $model->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);
        
        $models = SiteRole::find()->where(['authorized' => SiteRole::AUTHORIZED_YES])->all();

        return $this->render('index' , ['models' => $models, 'model' => $model]);
    }
    
    public function actionView($id)
    {
        if(!$model = Package::findOne($id)){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('view', ['model' => $model]);
    }

}
