<?php

namespace frontend\modules\company\controllers;

use frontend\models\Company;
use frontend\modules\company\models\CompanyMark;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;
use frontend\modules\company\models\Feedback;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FeedbackController extends Controller
{
    public function actionCreate($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new AccessDeniedException('Access denied');
        }

        $company = Company::findOne($id);
        if (!$company) {
            throw new NotFoundHttpException('Company not found');
        }

        $model = new Feedback();
        $request = Yii::$app->request->post();


        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax && $model->load($request)) {
            $userId = Yii::$app->user->identity->id;

            Yii::$app->response->format = Response::FORMAT_JSON;

            if (!$company->canCreateFeedback()) {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'message' => Yii::t('common', 'You are left a feedback about this company')
                ];
            }

            $feedback = $this->createFeedback($userId, $company->primaryKey, $request);
            if ($model->rating) {
                $modelMarck = new CompanyMark();
                $modelMarck->mark =  $model->rating;
                $modelMarck->company_id = $company->primaryKey;
                $modelMarck->from_user_id = $userId;
                $modelMarck->save();
            }
            if ($feedback->save()) {
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'You feedback sent')
                ];
            } else {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'errors' => $feedback->getErrors()
                ];
            }
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
                'company' => $company
            ]);
        }
    }

    private function createFeedback($userId, $company_id, $request)
    {
        $model = new Feedback();
        $model->load($request);
        $model->from_user_id = $userId;
        $model->company_id = $company_id;

        return $model;
    }
}