<?php

namespace frontend\modules\company\controllers;

use frontend\models\CompanySignupForm;
use frontend\models\User;
use frontend\modules\company\models\CompanyListSearch;
use frontend\modules\company\models\CompanyMapSearch;
use frontend\modules\company\models\Feedback;
use frontend\modules\company\models\PriceSearch;
use frontend\modules\instruments\models\InstrumentGet;
use Yii;
use frontend\models\Company;
use frontend\modules\company\models\CompanySearch;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\NotAcceptableHttpException;
use yii\data\ActiveDataProvider;
use frontend\modules\company\models\CompanyAdvertisementSearch;
use backend\modules\rubric\models\Rubric;
use yii\helpers\Html;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\PageCache',
                'only' => ['map'],
                'variations' => [
                    Yii::$app->user->isGuest, Yii::$app->request->get(), Yii::$app->language
                ],
                'duration' => 3600,
            ],
        ];
    }

    public function actionRating()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allCompanies = Company::find()
            ->where(['status' => Company::STATUS_ACTIVE_YES])
            ->orderBy('ratio DESC')
            ->all();

        return $this->render('rating', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allCompanies' => $allCompanies,
        ]);
    }

    public function actionMap()
    {
        $instrumentGet = new InstrumentGet(14);
        $instrument = $instrumentGet->searchOne(\Yii::$app->request->get('company_type'));

        if ($instrument['status'] == InstrumentGet::STATUS_NO_ACCCES) {
            return $this->redirect(['/instruments/instrument/no-access', 'id' => $instrumentGet->instrument_id ]);
        }

        $searchModel = new CompanyMapSearch();
        $query = $searchModel->search(Yii::$app->request->queryParams);
        $countQuery = clone $query;
        $count = $countQuery->count();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
        $locations = $this->buildMarkersArray($query->all());

        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => 10
        ]);
        $models = $query->offset($pages->offset)->limit($pages->limit)->all();

        $allCompanies = Company::find()
            ->where(['status' => Company::STATUS_ACTIVE_YES])
            ->orderBy('ratio DESC')
            ->all();

        return $this->render('map', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allCompanies' => $allCompanies,
            'pages' => $pages,
            'models' => $models,
            'locations' => $locations,
            'count' => $count
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $type = (Yii::$app->request->get('type') != null) ? (Yii::$app->request->get('type')) : 0 ;


        $model_search = new PriceSearch();
        $query = $model_search->getSearch($type, null, true, $id);

        $prices = Yii::$app->db
            ->createCommand($query. ' LIMIT 5' )
            ->queryAll();

        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'rubrics' => $model->rubrics,
            'subsidiary' => $model->subsidiaries,
            'advertisements' => $model->getAdvertisements(3),
            'canCreateMark' => $model->canCreateMark(),
            'prices' => $prices,
            'type' => $type,
//            'limit' => $limit
        ]);
    }

    public function actionCreate()
    {
       if(!Yii::$app->user->isGuest && Yii::$app->user->identity->company){
           throw new NotAcceptableHttpException('The requested content does not exist.');
       }
        $model = new CompanySignupForm();

        if ($model->load(Yii::$app->request->post()) && $model->saveCompany()) {
            Yii::$app->session->setFlash('success', 'The company was established successfully');

            return $this->redirect(['/cabinet/company/profile']);
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionFeedbacks($id)
    {
        $model = $this->findModel($id);

        $dataProvider = new ActiveDataProvider([
            'query' => Feedback::find()->where(['company_id' => $id])->orderBy('created_at DESC'),
            'pagination' => [
                'pageSize' => 10
            ],
        ]);

        return $this->render('feedbacks', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'rubrics' => $model->rubrics,
            'canCreateMark' => $model->canCreateMark()
        ]);
    }

    public function actionProducts($id)
    {
        $company = $this->findModel($id);
        $searchModel = new CompanyAdvertisementSearch(['company' => $company]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $company->primaryKey);

        $choosenRubric = $searchModel->rubric_id ?
            Rubric::find()->where(['id' => $searchModel->rubric_id, 'active' => Rubric::ACTIVE_YES])->one() : null;

        return $this->render('products', [
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'choosenRubric' => $choosenRubric
        ]);
    }

    protected function findModel($id)
    {
        $model = Company::find()->where(['id' => $id, 'status' => Company::STATUS_ACTIVE_YES])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function buildMarkersArray($models)
    {
        $time = microtime(true);
        $companyPageTranslates = Yii::t('common', 'Company page');

        $locations = "[";

        foreach ($models as $key => $company) {
            $subsidiaries = $company->subsidiaries;
            foreach ($subsidiaries as $subsidiary) {
                $location = addslashes($subsidiary->locationMap);
                $coordinates = json_decode($subsidiary->coordinates, true);

                if ($subsidiary->coordinates) {
                    $content = "<div class=\"marker\">";
                    $content .= "<div class=\"marker-title\">".addslashes($company->getName())."</div>";
                    $content .= "<div>" . $subsidiary->getType() . "</div>";
                    $content .= "<div>$location</div>";
                    $content .= "<div><a href=\"" . Url::to(['view', 'id' => $company->primaryKey]) . "\">$companyPageTranslates</a></div>";
                    $content .= '</div>';
                    $locations .= "['" . $content . "'," . $coordinates['lat'] . "," . $coordinates['lng'] . "],";
                }
            }
        }

        $locations = trim($locations, ',') . "]";

        return $locations;
    }

    private function getLocationByAddress($address)
    {
        $cityclean = str_replace (" ", "+", $address);
        $details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $cityclean . "&sensor=false";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geoloc = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if ($geoloc['status'] == 'OK' && isset($geoloc['results'][0]['geometry']['location'])) {
            return $geoloc['results'][0]['geometry']['location'];
        }

        return null;
    }

    public function actionCompanies(){
        $searchModel = new CompanyListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $allCompanies = Company::find()
            ->where(['status' => Company::STATUS_ACTIVE_YES])
            ->orderBy('created_at ASC')
            ->all();

        return $this->render('companies', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allCompanies' => $allCompanies,
        ]);
    }
}