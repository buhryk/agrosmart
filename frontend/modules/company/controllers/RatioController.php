<?php

namespace frontend\modules\company\controllers;

use frontend\models\Company;
use frontend\modules\company\models\CompanyMark;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class RatioController extends Controller
{
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            throw new AccessDeniedException('Access denied');
        }

        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax) {
            $request = Yii::$app->request->post();
            $userId = Yii::$app->user->identity->id;

            if (empty($request['company_id']) || empty($request['mark'])) {
                throw new BadRequestHttpException('Not all parameters provided');
            }

            $companyId = (int)$request['company_id'];
            $mark = (float)$request['mark'];

            if ($mark > 5 || $mark < 0) {
                throw new BadRequestHttpException('Mark value must be from 0 to 5');
            }

            $company = Company::findOne($companyId);
            if (!$company) {
                throw new NotFoundHttpException('Company not found');
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            if (!$company->canCreateMark()) {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'message' => Yii::t('common', 'You are left a mark about this company')
                ];
            }

            $mark = $this->createMark($userId, $company->primaryKey, $mark);

            if ($mark->save()) {
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'You mark apply')
                ];
            } else {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'errors' => $mark->getErrors()
                ];
            }
        }
    }

    private function createMark($userId, $company_id, $mark)
    {
        $model = new CompanyMark();
        $model->from_user_id = $userId;
        $model->company_id = $company_id;
        $model->mark = $mark;

        return $model;
    }
}