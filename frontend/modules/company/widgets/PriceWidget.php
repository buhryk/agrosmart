<?php

namespace frontend\modules\company\widgets;

use frontend\modules\cabinet\models\Sale;
use frontend\modules\company\models\PriceSearch;
use yii\bootstrap\Widget;
use Yii;

class PriceWidget extends Widget
{
    public $company_id;
    public $limit = 5;

    public function run() {
        if ($this->company_id) {
            $items[Sale::TYPE_PRODAZHA] = PriceSearch::find()
                ->select('sale.*,  CONCAT(sale.`city_id`, sale.`rubric_id`, sale.`address`) as place')
                ->andWhere(['company_id' => $this->company_id])
                ->andWhere(['between', 'date', date('Y-m-d', time()-(60*60*24*30)), date('Y-m-d', time()+(60*60*24*30))])
                ->andWhere(['type' => Sale::TYPE_PRODAZHA])
                ->groupBy('place')
                ->indexBy('place')
                ->orderBy(['updated_at'=> SORT_DESC])
                ->limit($this->limit)
                ->all();

            $items[Sale::TYPE_ZAKUPKA] = PriceSearch::find()
                ->select('sale.*,  CONCAT(sale.`city_id`, sale.`rubric_id`, sale.`address`) as place')
                ->andWhere(['company_id' => $this->company_id])
                ->andWhere(['between', 'date', date('Y-m-d', time()-(60*60*24*30)), date('Y-m-d', time()+(60*60*24*30))])
                ->andWhere(['type' => Sale::TYPE_ZAKUPKA])
                ->groupBy('place')
                ->indexBy('place')
                ->orderBy(['updated_at'=> SORT_DESC])
                ->limit($this->limit)
                ->all();
            if($items[Sale::TYPE_ZAKUPKA] ||  $items[Sale::TYPE_PRODAZHA]) {
                return $this->render('price-widget', ['items' => $items, 'company_id' => $this->company_id]);
            }else{
                return NULL;
            }
        }
    }


}
