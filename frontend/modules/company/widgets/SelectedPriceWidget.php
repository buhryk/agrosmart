<?php

namespace frontend\modules\company\widgets;

use frontend\modules\cabinet\models\Sale;
use frontend\modules\company\models\PriceSearch;
use frontend\modules\company\models\SelectedPriceSearch;
use yii\bootstrap\Widget;
use Yii;

class SelectedPriceWidget extends Widget
{
    public $type;
    
    public function run()
    {
         $model = new SelectedPriceSearch();
         $dataList = $model->search(Yii::$app->request->get(), $this->type);
      
        if($dataList) {
            return $this->render('selected_price_widget', ['dataList' => $dataList, 'type' => $this->type]);
        }
    }

}
