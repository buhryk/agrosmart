<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>

<div class="table-container">
    <form id="selectedPrice" action="<?=\yii\helpers\Url::to(['selected-price', 'type' => $type]) ?>">
        <table class="branches-table little col-xs-12 mt20 mb25 text-center">
            <tbody>
            <tr class="grey-row">
                <th class="text-center"> <?=Yii::t('common', 'Область') ?></th>
                <th class="text-center"> <?=Yii::t('common', 'Products') ?></th>
                <th class="text-center"> <?=Yii::t('common', 'Price') ?></th>
                <th class="text-center"> <?=Yii::t('common', 'Изминение') ?> </th>
                <th class="text-center"> <?=Yii::t('common', 'Трейдер') ?> </th>
            </tr>

            <?php foreach ($dataList as $key => $item): ?>
                <tr class="white-row">
                    <td><?=$item['oblastName'] ?></td>
                    <td><?=$item['rubricName'] ?></td>
                    <td><?=$item['price'] ?> грн</td>
                    <td class="">
                        <?php if($item['priceDiff']): ?>
                            <?=$item['priceDiff'] > 0 ? '+'.$item['priceDiff'] : $item['priceDiff'] ?>
                            <?=$item['percentDiff'] ? '('.$item['percentDiff'].'%)' : '' ?>
                        <?php endif; ?>
                    </td>
                    <td class="selected">
                        <div class=" company-list">
                            <?=\yii\helpers\Html::dropDownList('selected_price['.$item['id'].']', $item['company_id'], ArrayHelper::map($item['companyList'],'id' , 'name'), ['class' => 'form-control'])?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </form>
</div>
