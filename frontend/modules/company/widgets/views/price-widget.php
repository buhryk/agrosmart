<?php
use frontend\modules\cabinet\models\Sale;
?>
<div class="col-sm-12 mt50" id="price-block">
    <span class="fs19 fw600 ttu din"><?=Yii::t('common', 'Prices') ?></span>

    <div>
        <ul class="nav nav-tabs ap-choose sale-ul-tab">
            <li class="active ap-choose__item active">
                <a data-toggle="tab" href="#zakupka"><?=Yii::t('common', 'ZAKUPKA') ?></a>
            </li>
            <li class="ap-choose__item">
                <a data-toggle="tab" href="#prodazha"><?=Yii::t('common', 'PRODAZHA') ?></a>
            </li>
        </ul>
        <div class=" branches sale-block-content row">
            <div class="tab-content">
                <div id="zakupka" class="tab-pane fade in active">
                    <table class="branches-table col-xs-12 mt20 text-center">
                        <tbody>
                        <tr class="grey-row">
                            <th class="text-center"><a href="#" class="click-filter"><?=Yii::t('common', 'Products') ?></a></th>
                            <th class="text-center"><a href="#" class="click-filter"></a></th>
                            <th class="text-center"><a href="#" class="click-filter"><?=Yii::t('common', 'Price UAH') ?></a></th>
                            <th class="text-center"><a href="#" class="click-filter"><?=Yii::t('common', 'Address') ?></a></th>
                        </tr>
                        <?php foreach ($items[Sale::TYPE_ZAKUPKA] as $item): ?>
                            <tr class="white-row">
                                <td><?=$item->rubric->title ?></td>
                                <td><?=round($item->volume, 2) ?> <span class="pull-right"><?=$item->measurement->short_title ?> </span></td>
                                <td><?=round($item->price, 2) ?></span></td>
                                <td><?=$item->location ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                    <a href="<?=\yii\helpers\Url::to(['/company/price/zakupka-info', 'company_id'=> $company_id]) ?>" role="button" class="open-table-price mt40">
                        <?=Yii::t('common', 'See full table') ?>
                    </a>
                </div>
                <div id="prodazha" class="tab-pane fade">
                    <table class="branches-table col-xs-12 mt20 text-center">
                        <tbody>
                        <tr class="grey-row">
                            <th class="text-center"><a href="#" class="click-filter"><?=Yii::t('common', 'Products') ?></a></th>
                            <th class="text-center"><a href="#" class="click-filter"></a></th>
                            <th class="text-center"><a href="#" class="click-filter"><?=Yii::t('common', 'Price UAH') ?></a></th>
                            <th class="text-center"><a href="#" class="click-filter"><?=Yii::t('common', 'Address') ?></a></th>
                        </tr>
                        <?php foreach ($items[Sale::TYPE_PRODAZHA] as $item): ?>
                            <tr class="white-row">
                                <td><?=$item->rubric->title ?></td>
                                <td><?=round($item->volume,2) ?> <span class="pull-right"><?=$item->measurement->short_title ?> </span></td>
                                <td><?=round($item->price, 2) ?></td>
                                <td><?=$item->location ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                    <a href="<?=\yii\helpers\Url::to(['/company/price/prodazha-info', 'company_id'=> $company_id]) ?>" role="button" class="open-table-price mt40">
                        <?=Yii::t('common', 'See full table') ?>
                    </a>
                </div>
            </div>
        </div>

        </div>
</div>


