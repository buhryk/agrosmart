<?php

public static function getSearch($type, $q, $prof, $company)
{
    if($prof){
        $query = 'SELECT * FROM price WHERE company_id =' . $company . ' AND `type`=' . $type . ' ORDER BY id DESC';
    }else{

        $setting = Setting::find()->where(['alias' => 'company-list'])->one();
        $company = MyHelpers::multiexplode($setting->value);

        $stemmer = new LinguaStemRu();

        $arr_words = explode(' ', $q);

        $where5 = '';
        $where2 = '';
        $where3 = '';
        $not_where3 = '';
        $not_where2 = '';

        $i = 0;
        foreach ($arr_words as $item){
            $i++;
            if (strlen ($item) > 2){

                if(count($arr_words) == 1) {
                    $where2 = " AND( price_item.name LIKE '% " . $stemmer->stem_word($item) . " %'  
                        OR price_item.name LIKE '" . $stemmer->stem_word($item) . " %' 
                        OR price_item.name LIKE '% " . $stemmer->stem_word($item) . "' 
                        )";
                    $not_where2 = " AND( price_item.name not LIKE '% " . $stemmer->stem_word($item) . " %' 
                        and price_item.name not LIKE '" . $stemmer->stem_word($item) . " %' 
                        and price_item.name not LIKE '% " . $stemmer->stem_word($item) . "' 
                        )";
                }

                $where5 .= " price_item.name LIKE '%" . $stemmer->stem_word($item) . "%'";
                $where5 .=  ( count( $arr_words ) == $i ) ? "" : " OR ";

                $where3 .= " price_item.name LIKE '%" . $item . "%'";
                $where3 .=  ( count( $arr_words ) == $i ) ? "" : " AND ";

                $not_where3 .= " price_item.name not LIKE '%" . $stemmer->stem_word($item) . "%'";
                $not_where3 .=  ( count( $arr_words ) == $i ) ? "" : " OR ";
            }

        }
        $where3 =  ( strlen( $q ) > 2 ) ?  'AND (' . $where3 . ')' : '';
        $not_where3 = ( strlen( $q ) > 2 ) ?  'AND (' . $not_where3 . ')' : '';
        $where5 = ( strlen( $q ) > 2 ) ?  'AND (' . $where5 . ')' : '';



        $select = '
              SELECT
              price_item.name AS p_i_name, 
              price.name AS p_name, 
              price_item.id AS p_i_id, 
              price.id AS p_id, 
              price_item.price,
              price.company_id,
              price.updated_at AS p_updated_at, 
            ';

        $from_and_join = '
              FROM price_item 
              LEFT JOIN price ON price.`id` = price_item.`price_id`
              LEFT JOIN consumer ON consumer.`id` = price.`author_id`
            ';

        $common = "
              AND price.status = " . Price::STATUS_ACTIVE . "
              AND price.company_id NOT IN ('".implode("','", $company)."')
              AND consumer.status = 10
              AND price.type = " . $type . "
            ";

        $block2 = (count($arr_words) == 1) ? $select . '             
                          2 AS weight 
                          ' . $from_and_join . '
                          WHERE price_item.`name` != \''. $q .'\'           
                          ' . $common . '  
                         ' . $where2 . '               
                          UNION  ' : '';

        $query = $select . '
              1 AS weight 
              ' . $from_and_join . '
              WHERE price_item.`name` = \''. $q .'\' 
              ' . $common . ' 
              ' . $not_where2 . '
              UNION  
             
             ' . $block2 . '
              
              ' . $select . '
              3 AS weight 
              ' . $from_and_join . '
              WHERE price_item.`name` != \''. $q .'\'
             ' . $where3 . '
             ' . $not_where2 . '    
              ' . $common . '             
              UNION                            
              
              ' . $select . '             
              4 AS weight 
              ' . $from_and_join . '
              WHERE price_item.`name` != \''. $q .'\'
              AND price_item.`name` 
              LIKE \'%'. $q .'%\' 
              ' . $common . '  
             ' . $not_where3 . '
             ' . $not_where2 . '
              UNION  
                         
              ' . $select . '              
              5 AS weight 
              ' .  $from_and_join  . '
              WHERE price_item.`name` != \''. $q .'\'
              AND price_item.`name` 
              NOT LIKE \'%'. $q .'%\'
              ' . $common . '
             ' . $not_where3 . '
               ' . $not_where2 . '
             ' . $where5 . '    
              ORDER BY weight ASC, p_i_id DESC
            ';

    }

    return $query;
}