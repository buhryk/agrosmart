<?php

namespace frontend\modules\company\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Company;
use frontend\models\CompanyRubric;
use yii\helpers\ArrayHelper;
use backend\modules\tariff\models\Tariff;
use frontend\modules\cabinet\models\Top;

/**
 * CompanySearch represents the model behind the search form about `frontend\models\Company`.
 */
class CompanySearch extends Company
{
    public $rubrics;
    public $sort;
    public $topCompany;


    public function rules()
    {
        return [
            [['id', 'type'], 'integer'],
            [['name', 'rubrics', 'sort'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $activeDataProviderParams = [])
    {
        $query = Company::find();
        $role = Yii::$app->userRole->role;

        if(in_array(1, $role) || in_array(10, $role)) {
            if(!Yii::$app->request->get('CompanySearch')) {
                $this->rubrics[] = 2;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => isset($activeDataProviderParams['pagination']) ?
                $activeDataProviderParams['pagination'] : ['pageSize' => 15]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            Company::tableName().'.id' => $this->id,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        if ($this->rubrics) {
            $companiesIds = CompanyRubric::find()
                ->where(['in', 'rubric_id', is_array($this->rubrics) ? $this->rubrics : array($this->rubrics)])
                ->groupBy('company_id')
                ->all();

            $query->andFilterWhere(['in',   Company::tableName().'.id', $companiesIds ?
                ArrayHelper::getColumn($companiesIds, 'company_id') : [0]]);
        }

        $query->andFilterWhere(['status' => Company::STATUS_ACTIVE_YES]);

        $sort = 'rating-desc';
        if (in_array($this->sort, array_keys(self::getSortingProperties()))) {
            $sort = $this->sort;
        }

        if ($sort == 'rating-desc') {
            $query->orderBy('ratio DESC, last_activity_date DESC');
        } else {
            $query->orderBy('ratio ASC, last_activity_date DESC');
        }


        return $dataProvider;
    }



    public static function getSortingProperties()
    {
        return [
            'rating-desc' => Yii::t('common', 'COMPANY_SORT_RATING_DESC'),
            'rating-asc' => Yii::t('common', 'COMPANY_SORT_RATING_ASC'),
        ];
    }
}