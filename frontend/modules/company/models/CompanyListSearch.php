<?php

namespace frontend\modules\company\models;

use backend\modules\setting\models\Setting;
use common\helpers\MyHelpers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Company;
use frontend\models\CompanyRubric;
use yii\helpers\ArrayHelper;
use frontend\modules\cabinet\models\Top;
use frontend\modules\cabinet\models\Subsidiary;
use backend\modules\tariff\models\Tariff;
/**
 * CompanySearch represents the model behind the search form about `frontend\models\Company`.
 */
class CompanyListSearch extends Company
{
    public $rubrics;
    public $sort;
    public $topCompany;

    public function rules()
    {
        return [
            [['id', 'type'], 'integer'],
            [['name', 'rubrics', 'sort'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $activeDataProviderParams = [])
    {
        $query = Company::find();

        $query->leftJoin(Subsidiary::tableName(), Subsidiary::tableName().'.company_id='.Company::tableName().'.id');

        $role = Yii::$app->userRole->role;

        if(in_array(1, $role) || in_array(10, $role)) {
            if(!Yii::$app->request->get('CompanyListSearch')) {
                $this->rubrics[] = 2;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => isset($activeDataProviderParams['pagination']) ?
                $activeDataProviderParams['pagination'] : ['pageSize' => 15]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            Company::tableName().'.id' => $this->id,
            Company::tableName().'.type' => $this->type,
        ]);


        $query->andFilterWhere(['like', 'name', $this->name]);

        if ($this->rubrics) {
            $companiesIds = CompanyRubric::find()
                ->where(['in', 'rubric_id', is_array($this->rubrics) ? $this->rubrics : array($this->rubrics)])
                ->groupBy('company_id')
                ->all();

            $query->andFilterWhere(['in', Company::tableName().'.id', $companiesIds ?
                ArrayHelper::getColumn($companiesIds, 'company_id') : [0]]);
        }

        $query->andFilterWhere(['status' => Company::STATUS_ACTIVE_YES]);

        $setting = Setting::find()->where(['alias' => 'company-list'])->one();
        $company = MyHelpers::multiexplode($setting->value);

        $query->andFilterWhere(['not in', 'company.id' , $company]);

        $query->groupBy('company.id');

        $query->orderBy('last_activity_date DESC');

        $newQuery = clone $query;

        $this->selectTopCompany($newQuery);

        return $dataProvider;
    }

    public function selectTopCompany($query)
    {
        $query->select(Company::tableName().'.*, round(1, 500) as `random`');
        $query->innerJoin(Top::tableName(), Company::tableName().'.`id` = `model_id`')
            ->andWhere(['model_name' => Tariff::MODEL_COMPANY])
            ->andWhere(['in', Top::tableName().'.type' , [Tariff::TYPE_TOP, Tariff::TYPE_COMPLEX]])
            ->andWhere(['<=', 'start', time()])
            ->andWhere(['>=', 'finish', time()])
            ->orderBy('random');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => 3,
            'pagination' => [
                'pageSize' =>3,
            ]
        ]);

        $this->topCompany =  $dataProvider;
    }


}