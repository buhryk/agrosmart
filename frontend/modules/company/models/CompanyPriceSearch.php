<?php

namespace frontend\modules\company\models;

use backend\modules\rubric\models\Rubric;
use frontend\modules\cabinet\models\Sale;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Company;
use frontend\models\CompanyRubric;
use yii\helpers\ArrayHelper;

/**
 * CompanySearch represents the model behind the search form about `frontend\models\Company`.
 */
class CompanyPriceSearch extends Company
{
    public $rubrics;
    public $sort;
    public $oblast;
    public $trader;
    public $listType;
    public $type;

    public function rules()
    {
        return [
            [['id', 'type'], 'integer'],
            [['name', 'rubrics', 'sort', 'oblast', 'listType'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $type, $trader = false)
    {
        $this ->type = $type;

        $this->load($params);

        $query = Company::find()
            ->leftJoin( '(SELECT company_id, type, rubric_id , MAX(DATE(`date`)) as date FROM sale GROUP BY company_id) as saleSort', 'company.id = saleSort.company_id')
            ->leftJoin( 'sale', 'company.id = sale.company_id')
            ->andWhere(['sale.type' => $type])
            ->orderBy(['DATE(saleSort.date)'=> SORT_DESC, 'updated_at' => SORT_DESC])
            ->groupBy('saleSort.company_id');

        if($trader){
            $this->trader = $trader;
            $rubricTrader = ArrayHelper::getColumn(Rubric::find()->andWhere(['parent_id' => Rubric::TRAIDER_RUBRIC_ID])->asArray()->all(), 'id');
            $query->andWhere(['in', 'sale.rubric_id', $rubricTrader]);
        }

        if(!empty($this->oblast)){
            $query->andWhere(['in', 'sale.oblast_id', $this->oblast]);
        }
        if(!empty($this->rubrics)){
            $query->andWhere(['in', 'sale.rubric_id', $this->rubrics]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => isset($activeDataProviderParams['pagination']) ?
                $activeDataProviderParams['pagination'] : ['pageSize' => $this->getListType() == 'list' ? 15 : 40]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['status' => Company::STATUS_ACTIVE_YES]);

        return $dataProvider;
    }

    public static function getSortingProperties()
    {
        return [
            'rating-desc' => Yii::t('common', 'COMPANY_SORT_RATING_DESC'),
            'rating-asc' => Yii::t('common', 'COMPANY_SORT_RATING_ASC'),
        ];
    }
    
    public function getRubricList(){
        $rybric = [];
        if(!$this->trader){
            $listRubric = Rubric::find()
                ->andWhere(['can_add_prices' => Rubric::CAN_ADD_PRICES_YES])
                ->andWhere('parent_id IS NULL')
                ->orderBy(['sort' => SORT_ASC, 'id' => SORT_ASC])
                ->all();
            foreach ($listRubric as $item){
                $listRubricChild = Rubric::find()
                    ->andWhere(['parent_id' =>$item->id])
                    ->andWhere(['can_add_prices' => Rubric::CAN_ADD_PRICES_YES])
                    ->orderBy(['sort' => SORT_ASC, 'id' => SORT_ASC])
                    ->all();
                $child = [];
                if($listRubricChild){
                    foreach ($listRubricChild as $itemChild){

                        $listRubricChild2 = Rubric::find()
                            ->andWhere(['parent_id' =>$itemChild->id])
                            ->andWhere(['can_add_prices' => Rubric::CAN_ADD_PRICES_YES])
                            ->orderBy(['sort' => SORT_ASC, 'id' => SORT_ASC])
                            ->all();
                        $child2 = [];
                        if($listRubricChild2){
                            foreach ($listRubricChild2 as $itemChild2){
                                $child2[] =[
                                    'title' => $itemChild2->title,
                                    'id' => $itemChild2->id
                                ];
                            }
                        }
                        $child[] =[
                            'title' => $itemChild->title,
                            'id' => $itemChild->id,
                            'child' => $child2
                        ];
                    }
                }

                $rybric[] =[
                    'title' => $item->title,
                    'id' => $item->id,
                    'child' => $child
                ];
            }
        }else{
            $listRubric = Rubric::find()
                ->andWhere(['parent_id' => Rubric::TRAIDER_RUBRIC_ID])
                ->orderBy(['sort' => SORT_ASC, 'id' => SORT_ASC])
                ->all();
            if($listRubric){
                foreach ($listRubric as $item){
                    $rybric[] =[
                        'title' => $item->title,
                        'id' => $item->id,
                    ];
                }
            }
        }
        
        return  $rybric;
    }

    public function getListType()
    {
        if ($this->listType) {
            setcookie("listType", $this->listType);
            $_COOKIE['listType'] = $this->listType;
        } elseif (isset($_COOKIE['listType'])) {
            $this->listType = $_COOKIE['listType'];
        }  else {
            $this->listType = 'list';
        }

        return $this->listType;
    }
}