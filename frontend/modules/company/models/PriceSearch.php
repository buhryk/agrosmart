<?php

namespace frontend\modules\company\models;

use backend\modules\price2\models\Price;
use backend\modules\price2\models\PriceItem;
use backend\modules\setting\models\Setting;
use common\components\LinguaStemRu;
use common\helpers\MyHelpers;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cabinet\models\Sale;
use backend\modules\rubric\models\Rubric;
/**
 * SaleSearch represents the model behind the search form about `frontend\modules\cabinet\models\Sale`.
 */
class PriceSearch extends Sale
{
    public $from_date;
    public $to_date;
    public $place;


    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            //  [['volume', 'price'], 'number'],
            [['address', 'from_date', 'to_date'], 'safe'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $company_id, $type = NULL, $currency, $trader = false)
    {
        $this->load($params);

        if(empty($this->to_date) && empty($this->from_date)){
            $quareLastSale = Sale::find()
                ->andWhere(['company_id' => $company_id])
                ->andWhere(['type' => $type])
                ->andWhere(['<=','date', date('y-m-d')])
                ->andWhere(['currency_id' => $currency])
                ->orderBy(['date' => SORT_DESC]);
            if($trader){
                $quareLastSale->innerJoin(Rubric::tableName(), 'rubric_id = '.Rubric::tableName().'.id')
                    ->andWhere(['parent_id' => Rubric::TRAIDER_RUBRIC_ID]);
            }

            $lastSale = $quareLastSale->one();

            if($lastSale){
                $this->to_date = $lastSale->date;
                $this->from_date = $lastSale->date;
            }
        }

        $query = self::find()
            ->select('sale.*,  CONCAT(sale.`city_id`, sale.`rubric_id`, sale.`address`, sale.`description_address`) as place')
            ->innerJoin('rubric_model_lang', 'rubric_model_lang.rubric_id = sale.rubric_id')
            ->andWhere(['type' => $type])
            ->andWhere(['company_id' => $company_id])
            ->groupBy('place')
            ->indexBy('place')
            ->orderBy(['title'=> SORT_ASC]);

        if ($trader) {
            $query->innerJoin(Rubric::tableName(), 'sale.rubric_id = '.Rubric::tableName().'.id')
                ->andWhere(['parent_id' => Rubric::TRAIDER_RUBRIC_ID]);
        }

        if($currency){
            $query->andWhere(['currency_id' => $currency]);
        }

        if($this->to_date){
            $to_date = $this->to_date > date('Y-m-d') ? date('Y-m-d') : $this->to_date;
            $query->andWhere('date = :to_date', [':to_date' => $to_date]);
        }else{
            $query->andWhere('date = :to_date', [':to_date' => date('Y-m-d')]);
        }

        return $query->all();
    }

    public function searchFrom($company_id, $type = NULL, $currency = NULL)
    {
        $query = self::find()
            ->select('sale.*,  CONCAT(sale.`city_id`, sale.`rubric_id`, sale.`address`) as place')
            ->andWhere(['type' => $type])
            ->andWhere(['company_id' => $company_id])
            ->groupBy('place')
            ->indexBy('place');

        if($currency){
            $query->andWhere(['currency_id' => $currency]);
        }

        if($this->from_date){
            $query->andWhere('date = :from_date', [':from_date' =>$this->from_date]);
        }else{
            $query->andWhere('date = :from_date', [':from_date' => $this->from_date ]);
        }

        return $query->all();
    }

    public static function getSearch($type, $q, $prof, $company)
    {
        if($prof){
            $query = 'SELECT * FROM price WHERE company_id =' . $company . ' AND `type`=' . $type . ' ORDER BY id DESC';
        }else{

            $setting = Setting::find()->where(['alias' => 'company-list'])->one();
            $company = MyHelpers::multiexplode($setting->value);

            $stemmer = new LinguaStemRu();

            $q = mb_strtolower($q);
            $arr_words = explode(' ', $q);

            $where1 = '';
            $not_where1 = '';
            $where2 = '';
            $not_where2 = '';
            $where3 = '';
            $not_where3 = '';
            $where4 = '';
            $not_where4 = '';
            $where5 = '';
            $not_where5 = ' AND ( ';
            $where6 = ' AND ( ';
            $not_where6 = ' AND ( ';

            $i = 0;
            foreach ($arr_words as $item){
                $i++;

                if($item != 'для'){
                    $word = ( mb_strlen($stemmer->stem_word($item), 'UTF-8' ) > 2 ) ? $stemmer->stem_word( $item ) : $item;

                    $where1 = " AND lower(price_item.name) LIKE '" . $q . "' ";
                    $not_where1 = " AND lower(price_item.name) not LIKE '" . $q . "' ";

                    $where2 = " AND lower(price_item.name) LIKE '" . $q . "%' ";
                    $not_where2 = " AND lower(price_item.name) not LIKE '" . $q . "%' ";

                    $where3 = " AND lower(price_item.name) LIKE '%" . $q . "' ";
                    $not_where3 = " AND lower(price_item.name) not LIKE '%" . $q . "' ";

                    $where4 = " AND lower(price_item.name) LIKE '%" . $q . "%' ";
                    $not_where4 = " AND lower(price_item.name) not LIKE '%" . $q . "%' ";

                    $or = (  count($arr_words)  == $i ) ? ' ) ' : ' or ';

                    if(mb_strlen($word, 'UTF-8' ) < 3){
                        $where5 .= " ";
                        $where6 .= " ";
                    }else{
                        $where5 .= " AND lower(price_item.name) LIKE '%" . $word . "%' ";

                        if($i == 1){
                            $where6 .= "  lower(price_item.name) LIKE '" . $item . "_%' " . $or;
                        }else{
                            $where6 .= "  lower(price_item.name) LIKE '% " . $item . "_%' " . $or;
                        }
                    }

                    $not_where5 .= " lower(price_item.name) not LIKE '%" . $word . "%' " . $or;

                    $not_where6 .= "  lower(price_item.name) not LIKE '% " . $item . " %' " . $or;
                }

            }
//            echo count($arr_words);
//            exit;

            $select = '
              SELECT
              price_item.name AS p_i_name, 
              price.name AS p_name, 
              price_item.id AS p_i_id, 
              price.id AS p_id, 
              price_item.price,
              price.company_id,
              price.updated_at AS p_updated_at, 
            ';

            $from_and_join = '
              FROM price_item 
              LEFT JOIN price ON price.`id` = price_item.`price_id`
              LEFT JOIN consumer ON consumer.`id` = price.`author_id`
            ';

            $common = "
               price.status = " . Price::STATUS_ACTIVE . "
              AND price.company_id NOT IN ('".implode("','", $company)."')
              AND consumer.status = 10
              AND price.type = " . $type . "
            ";

            $query =
              $select . '
                1 AS weight 
              ' . $from_and_join . '
              WHERE  
              ' . $common . '
              ' . $where1 . '
              
                           UNION
                           
               ' .$select . '
              2 AS weight 
              ' . $from_and_join . '
              WHERE  
             
              ' . $common . '
              ' . $not_where1 . '             
              ' . $where2 . '  
              
                           UNION
                           
                ' .$select . '
              3 AS weight 
              ' . $from_and_join . '
              WHERE  
             
              ' . $common . '
              ' . $not_where1 . ' 
              ' . $not_where2 . '
              ' . $where3 . '  
              
                           UNION
                           
               ' .$select . '
              4 AS weight 
              ' . $from_and_join . '
              WHERE  
             
              ' . $common . '
              ' . $not_where1 . ' 
              ' . $not_where2 . '
              ' . $not_where3 . '
              ' . $where4 . '  
              
                           UNION
               ' .$select . '
              5 AS weight 
              ' . $from_and_join . '
              WHERE  
             
              ' . $common . '
              ' . $not_where1 . '
              ' . $not_where2 . '
              ' . $not_where3 . '
              ' . $not_where4 . '
              ' . $where5 . '   
              
                           UNION
               ' .$select . '
              6 AS weight 
              ' . $from_and_join . '
              WHERE  
             
              ' . $common . '
              ' . $not_where1 . '
              ' . $not_where2 . '
              ' . $not_where3 . '
              ' . $not_where4 . '
              ' . $not_where5 . '
              ' . $where6 . '  
            
                  
              ORDER BY weight ASC, p_updated_at DESC
            ';

        }

        return $query;
    }

    public function countSearch($type, $q, $prof, $company)
    {
         return count( Yii::$app->db->createCommand( self::getSearch($type, $q, $prof, $company) )->queryAll());
    }

}
