<?php

namespace frontend\modules\company\models;

use backend\modules\commondata\models\City;
use backend\modules\commondata\models\Region;
use frontend\modules\cabinet\models\Subsidiary;
use Yii;
use yii\base\Model;
use frontend\models\Company;
use frontend\models\CompanyRubric;
use yii\helpers\ArrayHelper;
use common\helpers\GoogleMapsApiHelper;
/**
 * CompanyMapSearch represents the model behind the search form about `frontend\models\Company`.
 */
class CompanyMapSearch extends Company
{
    public $rubrics;
    public $sort;
    public $radius;
    public $point_location;

    public function rules()
    {
        return [
            [['id', 'type', 'radius'], 'integer'],
            [['rubrics', 'sort', 'point_location'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Company::find();
        $this->load($params);

        $query->innerJoin(Subsidiary::tableName(), Subsidiary::tableName().'.company_id='.Company::tableName().'.id');

        $query->andFilterWhere([
            Company::tableName().'.id' => $this->id,
            Company::tableName().'.type' => $this->type,
        ]);

        if ($this->rubrics) {
            $companiesIds = CompanyRubric::find()
                ->where(['in', CompanyRubric::tableName().'.rubric_id', is_array($this->rubrics) ? $this->rubrics : array($this->rubrics)])
                ->groupBy(CompanyRubric::tableName().'.company_id')
                ->all();

            $query->andFilterWhere(['in', Company::tableName().'.id', $companiesIds ?
                ArrayHelper::getColumn($companiesIds, 'company_id') : [0]]);
        }

        $query->andFilterWhere([Company::tableName().'.status' => Company::STATUS_ACTIVE_YES]);

        $query->groupBy('company.id');

        $query->orderBy('last_activity_date DESC');

        return $query;
    }

    public static function getSortingProperties()
    {
        return [
            'rating-desc' => Yii::t('common', 'COMPANY_SORT_RATING_DESC'),
            'rating-asc' => Yii::t('common', 'COMPANY_SORT_RATING_ASC'),
        ];
    }

    public function getCoordinat()
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();
        return json_encode($googleMapsApiHelper->getLocationByAddress($this->point_location));
    }
}