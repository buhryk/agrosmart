<?php

namespace frontend\modules\company\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\cabinet\models\Sale;

/**
 * SaleSearch represents the model behind the search form about `frontend\modules\cabinet\models\Sale`.
 */
class TraderPriceSearch extends Sale
{
    public $from_date;
    public $to_date;
    public $place;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            //  [['volume', 'price'], 'number'],
            [['address', 'from_date', 'to_date'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $company_id, $type = NULL, $currency)
    {
        $this->load($params);
        
        $querySale = Sale::find()
            ->distinct('company_id')
            ->andWhere(['between', 'date', date('Y-m-d', time()-(60*60*24*30)), date('Y-m-d', time()+(60*60*24*30))])
            ->andWhere(['type' => $type]);
        
        if(empty($this->to_date) && empty($this->from_date)){
            $lastSale = Sale::find()
                ->where(['company_id' => $company_id])
                ->andWhere(['type' => $type])
                ->andWhere(['<=','date', date('y-m-d')])
                ->andWhere(['currency_id' => $currency])
                ->orderBy(['date' => SORT_DESC])
                ->one();

            if($lastSale){
                $this->to_date = $lastSale->date;
                $this->from_date = $lastSale->date;
            }
        }

        $query = self::find()
            ->select('sale.*,  CONCAT(sale.`city_id`, sale.`rubric_id`, sale.`address`) as place')
            ->andWhere(['type' => $type])
            ->andWhere(['company_id' => $company_id])
            ->groupBy('place')
            ->indexBy('place')
            ->orderBy(['date'=> SORT_DESC]);

        if($currency){
            $query->andWhere(['currency_id' => $currency]);
        }
        if($this->to_date){
            $to_date = $this->to_date > date('Y-m-d') ? date('Y-m-d') : $this->to_date;
            $query->andWhere('date = :to_date', [':to_date' => $to_date]);
        }else{
            $query->andWhere('date = :to_date', [':to_date' => date('Y-m-d')]);
        }

        return $query->all();
    }
    public function searchFrom($company_id, $type = NULL, $currency = NULL)
    {
        $query = self::find()
            ->select('sale.*,  CONCAT(sale.`city_id`, sale.`rubric_id`, sale.`address`) as place')
            ->andWhere(['type' => $type])
            ->andWhere(['company_id' => $company_id])
            ->groupBy('place')
            ->indexBy('place')
            ->orderBy(['date'=> SORT_DESC]);

        if($currency){
            $query->andWhere(['currency_id' => $currency]);
        }

        if($this->from_date){
            $query->andWhere('date = :from_date', [':from_date' =>$this->from_date]);
        }else{
            $query->andWhere('date = :from_date', [':from_date' => $this->from_date ]);
        }

        return $query->all();
    }

}
