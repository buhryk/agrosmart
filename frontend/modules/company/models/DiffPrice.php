<?php

namespace frontend\modules\company\models;

use Yii;

class DiffPrice
{
    public function overcome($current_list, $previus_list = NULL, $convertePice = false){
        $array = [];
        foreach ($current_list as $key => $value){
            $price = $value->price;

            $array[] = [
                'rubric' => $value->rubric->title,
                'volume' => $value->volume,
                'location' => $value->location,
                'description_address' => $value->description_address,
                'price' => $price,
                'author_id' => $value->user_updated ? $value->user_updated : $value->user_created, 
                'measurement' => $value->measurement->short_title,
                'oblast' => $value->oblast->title,
                'date' => $value->date,
                'diff' => isset($previus_list[$key]) ? $this->diff($value->price,  $previus_list[$key]->price) : NULL,
            ];
        }
        return $array;
    }



    public function diff($current_price, $previous_price){
        return $current_price - $previous_price;
    }
}