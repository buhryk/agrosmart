<?php
use frontend\assets\CompanyMapAsset;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

CompanyMapAsset::register($this);

$pageH1 = Yii::t('common', 'Companies on map');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['map'])]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Companies'), 'url' => ['companies']];
$this->params['breadcrumbs'][] = $pageH1;

$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$electedCompaniesIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedCompanies, 'id') : [];
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<?php if (Yii::$app->user->isGuest) { ?>
    <a href="<?= Url::to(['/site/signup-company']); ?>" class="btn-green btn-center mtb25">
        <?= Yii::t('common', 'Add your company'); ?>
    </a>
<?php } elseif (!Yii::$app->user->identity->company) { ?>
    <a href="<?= Url::to(['register']); ?>" class="btn-green btn-center mtb25">
        <?= Yii::t('common', 'Add your company'); ?>
    </a>
<?php } else { ?>
    <p>&nbsp;</p>
<?php } ?>

<?php echo $this->render('_map_search', ['model' => $searchModel, 'dataProvider' => $dataProvider, 'count'=> $count]); ?>

<section class="container rating-company pb0">
    <div class="col-sm-12" id="subsidiaries-map" style="height: 600px; width: 100%;"></div>
    <?php
        $radius = $searchModel->radius ? $searchModel->radius : 0;
    ?>
    <?php $this->registerJs("var locations = ".$locations.";  
     var radius = $radius;
     var point_location = $searchModel->coordinat;",
        \yii\web\View::POS_HEAD,
        'my-button-handler'
    ); ?>
    <div class="row">
        <div class="col-sm-9">
            <div class="row view-product">
                <?php foreach ($models as $model) { ?>
                    <?= $this->render('_company', ['model' => $model, 'electedCompaniesIds' => $electedCompaniesIds]); ?>
                <?php } ?>
            </div>
            <div style="text-align: center">
                <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages, 'registerLinkTags' => true]); ?>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">

        </div>
    </div>
</section>