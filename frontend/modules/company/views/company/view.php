<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;
use frontend\assets\CompanyPageAsset;
use kartik\rating\StarRating;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use frontend\components\tool\Image;

CompanyPageAsset::register($this);

$pageH1 =  Yii::t('common', 'Company').' : '.HtmlPurifier::process($model->name);
$this->title = $pageH1.' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => '/company/'.$model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Companies'), 'url' => ['companies']];
$this->params['breadcrumbs'][] = $model->name;

$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$electedCompaniesIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedCompanies, 'id') : [];
$electedAdvertisementsIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedAdvertisements, 'id') : [];
$electedUsersIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedUsers, 'id') : [];
$savedAddresses = $currentUser ? ArrayHelper::getColumn($currentUser->savedAddresses, 'address') : [];
$messageAddToElected = Yii::t('common', 'Add to elected');
$messageRemoveFromElected = Yii::t('common', 'Remove from elected');
$priceWidget = \frontend\modules\company\widgets\PriceWidget::widget(['company_id' => $model->id]);
$companyUser = ArrayHelper::getColumn($model->companyUsers, 'user_id');

?>

<div class="bread">
    <h1> <?=$pageH1 ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container one-company-page">
    <div class="row">
        <div class="col-sm-9 top-marclear">
            <div class="company-cart clear">
                <div class="col-sm-5">
                    <?= $model->logotype ?
                        Html::img(Image::resize('/'.$model->logotype, 274, 185),  ['alt' => $model->name]) :
                        Html::img('/images/user_274x185.jpg', ['alt' => $model->name]);
                    ?>
                </div>
                <div class="col-sm-7">
                    <span class="fs19 fw600 tu"><?=Html::encode($model->name); ?></span>
                    <p><?= HtmlPurifier::process($model->getShortDescription()); ?></p>
                    <?php if ($rubrics) { ?>
                        <p>
                            <span class="fw600"><?= Yii::t('common', 'Branches'); ?>:</span>
                            <?= implode(', ', ArrayHelper::getColumn($rubrics, 'title')); ?>
                        </p>
                    <?php } ?>
                    <?php echo StarRating::widget([
                        'name' => 'rating_company_'.$model->primaryKey,
                        'value' => $model->ratio,
                        'pluginOptions' => [
                            'step' => 0.1,
                            'readonly' => true,
                            'showClear' => false
                        ],
                        'options' => [
                            'data-company_id' => $model->primaryKey,
                            'data-url' => \yii\helpers\Url::to(['/company/ratio/create'])
                        ],
                        'pluginEvents' => [
                            'rating.change' => "function() {
                                $(this).rating('refresh', {disabled: true});

                                $.ajax({
                                    method: 'post',
                                    url: $(this).data('url'),
                                    dataType: 'json',
                                    data: {
                                        company_id: $(this).data('company_id'),
                                        mark: $(this).val()
                                    },
                                    success: function(data) {
                                        if (data.status == 'success') {

                                        }
                                    }
                                });
                            }",
                        ]
                    ]); ?>
                    <?php  if (!Yii::$app->user->isGuest && !in_array($currentUser->id, $companyUser) && isset($model->admin->user_id)) {

                        echo Html::a(Yii::t('common', 'Write'),
                            ['/cabinet/dialog/create-message-modal', 'receiver' => $model->admin->user_id],
                            ['class' => 'btn-green modalButton']);
                    } ?>
                </div>
                <div class="col-sm-12">
                    <?php if ($advertisements) { ?>
                        <a href="#product-block" class="prod-link"><?= Yii::t('advertisement', 'Products and services'); ?></a>
                    <?php } ?>
<!--                    --><?php //if ($prices) { ?>
                        <a href="#price-block" class="prod-link"><?= Yii::t('common', 'Prices'); ?></a>
<!--                    --><?php //} ?>
                    <?php if ($priceWidget) { ?>
                    <a href="#price-block" class="prod-link"><?= Yii::t('common', 'Prices'); ?></a>
                    <?php } ?>
                    <a href="#feedback-block" class="prod-link"><?= Yii::t('common', 'Feedbacks'); ?></a>
                    <?php if ($subsidiary) { ?>
                        <a href="#contact-block" class="prod-link"><?= Yii::t('common', 'Branches and contacts'); ?></a>
                    <?php } ?>
                    <?php if ($model->full_description) { ?>
                        <a href="#company-block" class="prod-link"><?= Yii::t('common', 'About company'); ?></a>
                    <?php } ?>
                </div>
            </div>
            <?php if ($advertisements) { ?>
                <div class="another-product companies-products">
                    <div class="company-infoblock pt50 pb25" id="product-block">
                        <span class="fs19 fw600 ttu db mb10"><?= Yii::t('advertisement', 'Products and services'); ?></span>
                    </div>
                    <?php foreach ($advertisements as $advertisement) { ?>
                        <?= $this->render('@frontend/modules/product/views/product/_advertisement', [
                            'model' => $advertisement,
                            'company' => $model,
                            'electedCompaniesIds' => $electedCompaniesIds,
                            'electedAdvertisementsIds' => $electedAdvertisementsIds,
                            'electedUsersIds' => $electedUsersIds,
                            'messageAddToElected' => $messageAddToElected,
                            'messageRemoveFromElected' => $messageRemoveFromElected,
                            'savedAddresses' => $savedAddresses
                        ]); ?>
                    <?php } ?>

                    <div style="text-align: center">
                        <a href="<?= \yii\helpers\Url::to(['products', 'id' => $model->primaryKey]); ?>" class="btn-green">
                            <?= Yii::t('common', 'Go to all company advertisements'); ?>
                        </a>
                    </div>
                </div>
            <?php } ?>

            <div class="company-infoblock pt50 pb25" id="price-block">
                <span class="fs19 fw600 ttu db mb10"><?= Yii::t('common', 'Prices'); ?></span>
            </div>

            <?= $this->render('@frontend/modules/company/views/company/price', [
                'prices' => $prices,
                'type' => $type,
            ]); ?>


            <div class="col-sm-12">
                <?= $priceWidget ?>
                <?php if ($subsidiary) { ?>
                    <?php $addresses = []; ?>
                    <div class="col-sm-12 pt50 pb50 another-product" id="contact-block">
                        <span class="fs19 fw600 ttu din"><?= Yii::t('common', 'Branches and contacts'); ?></span>
                        <div class="over-sc">
                            <table class="branches-table col-xs-12 mt20 text-center">
                                <tbody>
                                <tr class="grey-row">
                                    <th class="text-center"><?= Yii::t('cabinet', 'CITY'); ?></th>
                                    <th class="text-center"><?= Yii::t('cabinet', 'TYPE'); ?></th>
                                    <th class="text-center"><?= Yii::t('cabinet', 'ADDRESS'); ?></th>
                                    <th class="text-center"><?= Yii::t('cabinet', 'PHONE'); ?></th>
                                    <th class="text-center">Email</th>
                                </tr>
                                <?php foreach ($subsidiary as $one) { ?>
                                    <?php $addresses[] = [
                                        'detailAddress' => $one->detailAddress,
                                        'simpleAddress' => $one->city->title . ', ' . $one->address
                                    ]; ?>
                                    <tr class="white-row">
                                        <td><?= $one->city->title; ?></td>
                                        <td><?= $one->getType(); ?></td>
                                        <td><?= $one->address; ?></td>
                                        <td><?= $one->phone; ?></td>
                                        <td><?= $one->email; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <div id="subsidiaries-map" style="width: 100%; height: 415px; margin-bottom: 10px; margin-top: 25px;"></div>

                        <?php $locations = "[";

                        foreach ($subsidiary as $item) {
                            if($item->coordinates && $item->coordinates !='null') {
                                $locations .= "{detailAddress: '".$item->location."',
                              coordinates:".$item->coordinates."},";
                            }
                        }
                        $locations = trim($locations, ',') . "]";

                        $this->registerJs("var locations = ".$locations.";",
                            \yii\web\View::POS_HEAD,
                            'my-button-handler'
                        ); ?>
                    </div>
                <?php } ?>
                <div class="col-sm-12 pt50" id="feedback-block">
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="fs19 fw600 ttu din" style="display: block"><?= Yii::t('common', 'Feedbacks'); ?></span>
                            <span class="all-rating"><?= Yii::t('common', 'Common rating'); ?>:</span>
                            <?php echo StarRating::widget([
                                'name' => 'rating_company_'.$model->primaryKey,
                                'value' => $model->ratio,
                                'pluginOptions' => [
                                    'step' => 0.1,
                                    'readonly' => true,
                                    'disabled' => true,
                                    'showClear' => false,
                                    'showCaption' => false
                                ],
                            ]); ?>
                        </div>
                        <div class="col-sm-6">
                            <?php if ($model->canCreateFeedback()) { ?>
                                <?php echo Html::a(Yii::t('common', 'Leave feedback'), ['feedback/create', 'id' => $model->primaryKey], [
                                    'class' => 'btn-green pull-right modalButton'
                                ]) ?>
                            <?php } ?>
                        </div>
                    </div>
                    <?php $feedbacks = $model->getFeedbacks(3); ?>
                    <?php if ($feedbacks) { ?>
                        <?php $feedbacksStatistic = $model->feedbackStatistic; ?>
                        <p class="feed">
                            <?php foreach ($feedbacksStatistic as $one) { ?>
                                <?php if ($one['countPoints']) { ?>
                                    <span><?= $one['title']; ?>: <span class="percent"><?= $one['percentage']; ?>%</span></span>
                                <?php } ?>
                            <?php } ?>
                        </p>

                        <?php foreach ($feedbacks as $feedback) { ?>
                            <?= $this->render('_feedback', [
                                'model' => $feedback
                            ]); ?>
                        <?php } ?>

                        <div class="col-sm-12 text-center mt25 mb25">
                            <a href="<?= \yii\helpers\Url::to(['feedbacks', 'id' => $model->primaryKey]); ?>" class="btn-green">
                                <?= Yii::t('common', 'More feedbacks'); ?>
                            </a>
                        </div>
                    <?php } else { ?>
                        <h4 style="margin-bottom: 20px;">
                            <?= Yii::t('common', 'As long as there is no user reviews'); ?>
                        </h4>
                    <?php } ?>
                </div>
                <?php if ($model->full_description) { ?>
                    <div class="col-sm-12 pt50" id="company-block">
                        <span class="fs19 fw600 ttu din"><?= Yii::t('common', 'About company'); ?></span>
                        <div class="bgg" style="padding: 15px; margin-bottom: 10px;">
                            <?=HtmlPurifier::process($model->getFullDescription()); ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>

    </div>
</div>
