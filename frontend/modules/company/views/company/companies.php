<?php
use frontend\assets\CompanyRatingAsset;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$electedCompaniesIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedCompanies, 'id') : [];


CompanyRatingAsset::register($this);

$pageH1 = Yii::t('common', 'Companies');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['companies'])]);
$this->params['breadcrumbs'][] = $pageH1;
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<?php if (Yii::$app->user->isGuest) { ?>
    <a href="<?= Url::to(['/site/signup-company']); ?>" class="btn-green btn-center mtb25">
        <?= Yii::t('common', 'Add your company'); ?>
    </a>
<?php } elseif (!Yii::$app->user->identity->company) { ?>
    <a href="<?= Url::to(['create']); ?>" class="btn-green btn-center mtb25">
        <?= Yii::t('common', 'Add your company'); ?>
    </a>
<?php } else { ?>
    <p>&nbsp;</p>
<?php } ?>

<?php echo $this->render('_search', ['model' => $searchModel, 'dataProvider' => $dataProvider]); ?>

<section class="container rating-company pb0">
    <div class="row">
        <div class="col-sm-9 ">
            <div class="row view-product">
                <?php echo \yii\widgets\ListView::widget([
                    'dataProvider' => $searchModel->topCompany,
                    'itemView' => '_company',
                    'showOnEmpty' => true,
                    'viewParams' => [
                        'electedCompaniesIds' => $electedCompaniesIds,
                    ]
                ]); ?>
                <?php echo \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_company',
                    'pager' => [
                        'registerLinkTags' => true,
                    ],
                    'viewParams' => [
                        'electedCompaniesIds' => $electedCompaniesIds,
                    ]
                ]); ?>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</section>