<?php
use yii\widgets\Breadcrumbs;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\ArrayHelper;
use kartik\rating\StarRating;
use yii\helpers\Html;


$pageH1 = Yii::t('common', 'Feedbacks').' : '.Html::encode($model->name);
$this->title = $pageH1.' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => '/company/company/feedbacks/'.$model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Companies'), 'url' => ['companies']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $pageH1;

$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$electedCompaniesIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedCompanies, 'id') : [];
$electedAdvertisementsIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedAdvertisements, 'id') : [];
$electedUsersIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedUsers, 'id') : [];
$messageAddToElected = Yii::t('common', 'Add to elected');
$messageRemoveFromElected = Yii::t('common', 'Remove from elected');
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container one-company-page">
    <div class="row">
        <div class="col-sm-9 top-marclear">
            <div class="company-cart clear">
                <div class="col-sm-5">
                    <?= $model->logotype ? Yii::$app->thumbnail->img($model->logotype, ['thumbnail' => ['width' => 274, 'height' => 185]], ['alt' => $model->name]) :
                        Html::img('/images/user_274x185.jpg', ['alt' => $model->name]);
                    ?>
                </div>
                <div class="col-sm-7">
                    <span class="fs19 fw600 tu"><?= $this->title; ?></span>
                    <p><?= \yii\helpers\HtmlPurifier::process($model->getShortDescription()) ?></p>
                    <?php if ($rubrics) { ?>
                        <p>
                            <span class="fw600"><?= Yii::t('common', 'Branches'); ?>:</span>
                            <?= implode(', ', ArrayHelper::getColumn($rubrics, 'title')); ?>
                        </p>
                    <?php } ?>
                    <?php echo StarRating::widget([
                        'name' => 'rating_company_'.$model->primaryKey,
                        'value' => $model->ratio,
                        'pluginOptions' => [
                            'step' => 0.1,
                            'readonly' => true,
                            'showClear' => false
                        ],
                        'options' => [
                            'data-company_id' => $model->primaryKey,
                            'data-url' => \yii\helpers\Url::to(['/company/ratio/create'])
                        ],
                        'pluginEvents' => [
                            'rating.change' => "function() { 
                                $(this).rating('refresh', {disabled: true});
                                
                                $.ajax({
                                    method: 'post',
                                    url: $(this).data('url'),
                                    dataType: 'json',
                                    data: {
                                        company_id: $(this).data('company_id'),
                                        mark: $(this).val()
                                    },
                                    success: function(data) {
                                        if (data.status == 'success') {
                                            
                                        }
                                    }
                                });
                            }",
                        ]
                    ]); ?>
                </div>
            </div>

        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>

        <div class="col-sm-12 pt50" id="feedback-block">
            <div class="row">
                <div class="col-sm-6">
                    <span class="fs19 fw600 ttu din" style="display: block"><?= Yii::t('common', 'Feedbacks'); ?></span>
                    <span class="all-rating"><?= Yii::t('common', 'Common rating'); ?>:</span>
                    <div style="display: inline-block">
                        <?php echo StarRating::widget([
                            'name' => 'rating_company_'.$model->primaryKey,
                            'value' => $model->ratio,
                            'pluginOptions' => [
                                'step' => 0.1,
                                'readonly' => !$canCreateMark,
                                'showClear' => false
                            ],
                            'options' => [
                                'data-company_id' => $model->primaryKey,
                                'data-url' => \yii\helpers\Url::to(['/company/ratio/create'])
                            ],
                            'pluginEvents' => [
                                'rating.change' => "function() { 
                                $(this).rating('refresh', {disabled: true});
                                
                                $.ajax({
                                    method: 'post',
                                    url: $(this).data('url'),
                                    dataType: 'json',
                                    data: {
                                        company_id: $(this).data('company_id'),
                                        mark: $(this).val()
                                    },
                                    success: function(data) {
                                        if (data.status == 'success') {
                                            
                                        }
                                    }
                                });
                            }",
                            ]
                        ]); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?php if ($model->canCreateFeedback()) { ?>
                        <?php echo Html::a(Yii::t('common', 'Leave feedback'), ['feedback/create', 'id' => $model->primaryKey], [
                            'class' => 'btn-green pull-right modalButton'
                        ]) ?>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-12" style="margin-top: 20px; margin-bottom: 30px;">
                    <?php echo \yii\widgets\ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => '_feedback',
                        'viewParams' => [
                            'electedCompaniesIds' => $electedCompaniesIds,
                            'messageAddToElected' => $messageAddToElected,
                            'messageRemoveFromElected' => $messageRemoveFromElected,
                            'electedAdvertisementsIds' => $electedAdvertisementsIds,
                            'electedUsersIds' => $electedUsersIds
                        ]
                    ]); ?>
                </div>
            </div>
        </div>

        <?php if ($model->full_description) { ?>
            <div class="col-sm-12 pt50" id="company-block">
                <span class="fs19 fw600 ttu din"><?= Yii::t('common', 'About company'); ?></span>
                <div class="bgg" style="padding: 15px; margin-bottom: 10px;">
                    <?= $model->full_description; ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>