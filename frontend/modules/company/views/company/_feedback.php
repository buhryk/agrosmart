<?php
use yii\helpers\Url;
use frontend\modules\company\models\Feedback;
use yii\helpers\Html;


$user = $model->user;
?>

<div class="block-feedback">
    <div class="row">
        <div class="col-sm-3">
            <img src="/images/feedback-user.jpg" alt="">
            <span class="date"><?= date('d.m.Y', $model->created_at); ?></span>
        </div>
        <div class="col-sm-9">
            <span class="fs19 fw600 ttu din"><?= Html::encode($user->fullName); ?></span>
            <div class="pull-right feed-icon">
                <?php if ($model->price_relevance) { ?>
                    <span class="feed-price <?= $model->price_relevance == Feedback::ANSWER_POSITIVE ? 'plus' : 'minus'; ?>">Цена: </span>
                <?php } ?>
                <?php if ($model->description_relevance) { ?>
                    <span class="feed-price <?= $model->description_relevance == Feedback::ANSWER_POSITIVE ? 'plus' : 'minus'; ?>">Описание: </span>
                <?php } ?>
                <?php if ($model->conditions_relevance) { ?>
                    <span class="feed-price <?= $model->conditions_relevance == Feedback::ANSWER_POSITIVE ? 'plus' : 'minus'; ?>">Условия сделки: </span>
                <?php } ?>
            </div>
            <?php $company = $user->company; ?>
            <?php if ($company) { ?>
                <a href="<?= Url::to(['view', 'id' => $company->primaryKey]); ?>" class="prod-link db">
                    <?=Html::encode($company->name); ?>
                </a>
            <?php } ?>
            <p class="mt25">
                <?=Html::encode( $model->text); ?>
            </p>
        </div>
    </div>
</div>