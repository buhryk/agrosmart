<?php
use backend\modules\price2\models\Price;
use frontend\assets\PriceAsset;
use frontend\modules\company\models\PriceSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

//PriceAsset::register($this);

$model_price = new PriceSearch();
$id = Yii::$app->request->get('id');
$sale_count = $model_price->countSearch(Price::TYPE_SALE, null, true, $id);
$purchase_count = $model_price->countSearch(Price::TYPE_PURCHASE, null, true, $id);
$current_count = ($type == Price::TYPE_SALE) ? $sale_count : $purchase_count;
?>

<?php Pjax::begin(['timeout' => 3000]) ?>

    <ul class="nav b-tab__nav b-prise-profile__tab-nav ap-choose sale-ul-tab">
        <li class="ap-choose__item <?= ($type == Price::TYPE_SALE) ? 'active' : '' ?>"
            style="<?= (!$sale_count) ? 'pointer-events: none; cursor: default;  color: #999"' : ''?>">
            <a href="<?= Url::toRoute(['view', 'id' => $id, 'type' => Price::TYPE_SALE]) ?>">
                <?= Yii::t('price', 'sale') ?> <span>(<?= $sale_count ?>)</span>
            </a>
        </li>
        <li class="ap-choose__item <?= ($type == Price::TYPE_PURCHASE) ? 'active' : '' ?>"
            style="<?= (!$purchase_count) ? 'pointer-events: none; cursor: default;  color: #999"' : ''?>">
            <a href="<?= Url::toRoute(['view', 'id' => $id, 'type' => Price::TYPE_PURCHASE]) ?>">
                <?= Yii::t('price', 'purchase') ?> <span>(<?= $purchase_count ?>)</span>
            </a>
        </li>
    </ul>
<?php if ($prices) { ?>
    <div class="b-table">
        <div class="b-table__list">
            <?php foreach ($prices as $price): ?>
                <div class="b-panel">
                    <div class="b-panel__info">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="b-panel__title">
                                    <a href="<?= Url::toRoute(['/company/price/view', 'id' => $price['id']])?>">
                                        <b><?= Html::encode($price['name']) ?></b>
                                    </a>
                                </div>
                                <div class="b-panel__date"><span><?= date('d.m.y', $price['updated_at']) ?></span></div>
                                <div class="b-panel__text b-panel__text_max">
                                    <p><?= Html::encode( $price['description']) ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php } ?>
<?php if($current_count >= 5):?>
    <div class="b-prise-profile__btn-wrap text-center">
        <a href="<?= Url::toRoute(['/company/price/all', 'id' => $id, 'type' => $type]) ?>"
           class="b-btn b-prise-profile__btn btn-green" data-pjax="0">
        <?= Yii::t('price',
                'VIEW ALL PRICES') ?>
        </a>
    </div>
<?php endif; ?>

<?php Pjax::end() ?>
