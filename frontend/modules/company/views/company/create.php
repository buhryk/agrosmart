<?php
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\models\Company;
use frontend\models\CompanyUser;

$this->title = Yii::t('common','Добавление компании');
?>

<div class="bread">
    <h1><?=Yii::t('common', 'Добавление компании') ?></h1>
    <div class="bread-bg">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/"><?=Yii::t('common', 'Главная') ?></a></li>
                <li class="active"><?=Yii::t('common', 'Добавление компании') ?></li>
            </ul>
        </div>
    </div>
</div>


<section class="container rating-company pb0">
    <div class="row">
        <div class="col-sm-9 ">
            <div class="row ">
                <?php $form = ActiveForm::begin([
                    'id' => 'form-signup',
                    'options' => [
                        'class' => 'registration-form clearfix',
                        'autocomplete' => 'off'
                    ]
                ]); ?>
                <?php echo $form->field($model, 'name')->textInput([
                    'placeholder' => 'Название компании',
                    'class' => 'registration__input placeholder'
                ])->label(false); ?>

                <?php echo $form->field($model, 'type')->dropDownList(Company::getTypesList(), [
                    'prompt' => Yii::t('cabinet', 'Choose company type')
                ])->label(false) ?>
                
                <div class="form-group">
                    <?= Html::submitButton(\Yii::t('users', 'ENROLL'), ['class' => 'btn-green btn-center mtb50']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</section>