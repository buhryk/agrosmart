<?php
use backend\modules\price2\models\Price;
use frontend\modules\company\models\PriceSearch;
use yii\helpers\Url;
use kartik\rating\StarRating;
use frontend\models\Company;
use yii\helpers\Html;
use frontend\modules\elected\models\Elected;
use frontend\components\tool\Image;

$top = $model->allotted ? 'top' : NULL;

$messageAddToElected = isset($messageAddToElected) ? $messageAddToElected : Yii::t('common', 'Add to elected');
$messageRemoveFromElected = isset($messageRemoveFromElected) ? $messageRemoveFromElected : Yii::t('common', 'Remove from elected');

?>

<div class="sales-product clear one-company-block <?=$top ?>">
    <div class="col-sm-4">
        <a href="#">
            <figure class="effect-chico">
                <?php if(isset($allCompanies)) : ?>
                    <span class="rating-number">
                        <?php foreach ($allCompanies as $key => $one) {?>
                            <?php if ($one->id == $model->primaryKey) {
                                echo $key + 1;
                                break;
                            } ?>
                        <?php } ?>
                    </span>
                <?php endif; ?>
                <?= $model->logotype ?
                    Html::img(Image::resize('/'.$model->logotype, 274, 185),  ['alt' => $model->name]) :
                    Html::img('/images/user_274x185.jpg', ['alt' => $model->name]);
                ?>
            </figure>
        </a>
    </div>
    <div class="col-sm-5">
        <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"><?=$model->getName(); ?></a>
        <div class="location contact-info">
            <?php $contactInfo = $model->contactInfo; ?>
            <?php foreach ($contactInfo as $info) { ?>
                <div><?= $info; ?></div>
            <?php } ?>
        </div>
        <?php if (!Yii::$app->user->isGuest) { ?>
            <a class="green-link change-elected <?= in_array($model->primaryKey, $electedCompaniesIds) ?
                'remove-from-elected' : 'add-to-elected'; ?>"
               data-type="<?= Elected::ALIAS_COMPANY; ?>"
               data-record_id="<?= $model->primaryKey; ?>"
               data-message-add="<?= $messageAddToElected; ?>"
               data-message-remove="<?= $messageRemoveFromElected; ?>"
               style="cursor: pointer"
            >
                <?= in_array($model->primaryKey, $electedCompaniesIds) ? $messageRemoveFromElected : $messageAddToElected; ?>
            </a>
        <?php } ?>
        <p class="comp-status">
            <?php if ($model->is_confirmed): ?>
                <?= Yii::t('common', 'Status'); ?>:&nbsp;<?= $model->isConfirmedDetail; ?>
            <?php endif; ?>
        </p>

    </div>
    <div class="col-sm-3">
        <i><?= Yii::t('common', 'Company was on the portal'); ?>:<br><?= date('d.m.Y', $model->last_activity_date); ?></i>
        <?php
        $advertisementsCount = $model->advertisementsCount;
        $saleZakupkaCount = $model->saleZakupkaCount;
        $saleProdCount = $model->saleProdCount;
        $price = new PriceSearch();
        $sale_count = $price->countSearch(Price::TYPE_SALE, '', true, $model->id);
        $purchase_count = $price->countSearch(Price::TYPE_PURCHASE, '', true, $model->id);
        ?>

        <?php if ($advertisementsCount) { ?>
            <a href="<?= Url::to(['products', 'id' => $model->primaryKey]); ?>" class="link-underline">
                <?= Yii::t('advertisement', 'Products and services'); ?>&nbsp;(<?= $advertisementsCount; ?>)
            </a>
        <?php } ?>

<!--        --><?php //if ($saleZakupkaCount) { ?>
<!--            <a href="--><?//= Url::to(['/company/price/zakupka-info', 'company_id' => $model->id]); ?><!--" class="link-underline">-->
<!--                --><?//= Yii::t('common', 'PURCHASE PRICE'); ?><!--&nbsp;(--><?//= $saleZakupkaCount; ?><!--)-->
<!--            </a>-->
<!--        --><?php //} ?>

        <?php if ($saleProdCount) { ?>
            <a href="<?= Url::to(['/company/price/prodazha-info', 'company_id' => $model->id]); ?>" class="link-underline">
                <?= Yii::t('common', 'Sales prices'); ?>&nbsp;(<?= $saleProdCount; ?>)
            </a>
        <?php } ?>

        <?php if ($sale_count) { ?>
            <a href="<?= Url::to(['/company/price/all', 'id' => $model->id, 'type' => Price::TYPE_SALE]); ?>" class="link-underline">
                <?= Yii::t('price', 'Prices sale'); ?>&nbsp;(<?= $sale_count; ?>)
            </a>
        <?php } ?>

        <?php if ($purchase_count) { ?>
            <a href="<?= Url::to(['/company/price/all', 'id' => $model->id, 'type' => Price::TYPE_PURCHASE]); ?>" class="link-underline">
                <?= Yii::t('price', 'Prices purchase'); ?>&nbsp;(<?= $purchase_count; ?>)
            </a>
        <?php } ?>

        <?php echo StarRating::widget([
            'name' => 'rating_company_'.$model->primaryKey,
            'value' => $model->ratio,
            'pluginOptions' => [
                'step' => 0.1,
                'readonly' => true,
                'disabled' => true,
                'showClear' => false,
                'showCaption' => false
            ],
        ]); ?>

    </div>
    <div class="clearfix"></div>
</div>
