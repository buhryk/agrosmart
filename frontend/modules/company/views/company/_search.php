<?php

use yii\widgets\ActiveForm;
use frontend\models\Company;
use yii\helpers\Url;
use frontend\modules\company\models\CompanySearch;
use kartik\select2\Select2;
use backend\modules\rubric\models\Rubric;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$classname = \yii\helpers\StringHelper::basename(get_class($model));
$action = Yii::$app->controller->action->id;

/* @var $this yii\web\View */
/* @var $model frontend\modules\company\models\CompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-search">
    <?php $form = ActiveForm::begin([
        'method' => 'get',
        'action' => [$action]
    ]); ?>

    <div class="container-fluid rating-company bg-g">
        <div class="container">
            <div class="row filter-accordion">
                <?php $mainRubrics = Rubric::find()->where(['parent_id' => null])->orderBy(['sort' => SORT_ASC, 'id' => SORT_ASC])->all(); ?>
                <?php foreach ($mainRubrics as $key => $mainRubric) { ?>
                    <?php $subrubrics = $mainRubric->subrubrics; ?>
                    <?php if (!$subrubrics) continue; ?>
                    <?php if (($key % 4) == 0 || $key == 0) { ?>
                        <div class="row">
                    <?php } ?>
                    <?php $subrubricsIds = ArrayHelper::getColumn($subrubrics, 'id'); ?>
                    <div class="col-xs-6 col-md-3">
                        <div class="panel-group" id="accordion-main-rubric-<?= $key; ?>">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <p class="bold fs16 black"
                                       data-toggle="collapse"
                                       data-parent="#accordion-main-rubric-<?= $key; ?>"
                                       href="#main-rubric-<?= $key; ?>"
                                    >
                                        <?= $mainRubric->title; ?><i class="fa fa-angle-down panel__icon" aria-hidden="true"></i>
                                    </p>
                                </div>
                                <?php $classOpen = ($model->rubrics && array_uintersect($subrubricsIds, $model->rubrics, 'strcasecmp')) ? 'in' : ''; ?>
                                <div id="main-rubric-<?= $key; ?>" class="panel-collapse collapse <?= $classOpen; ?>">
                                    <ul class="panel-collapse__inner">
                                        <?php foreach ($subrubrics as $keySubrubric => $subrubric) { ?>
                                            <?php $checked = ($model->rubrics && in_array($subrubric->primaryKey, $model->rubrics)) ? 'checked="checked"' : ''; ?>
                                            <input type="checkbox"
                                                   id="rubric-<?= $subrubric->primaryKey; ?>"
                                                   name="<?= $classname . '[rubrics][]'; ?>"
                                                   value="<?= $subrubric->primaryKey; ?>"
                                                   <?= $checked; ?>
                                            >
                                            <label for="rubric-<?= $subrubric->primaryKey; ?>"><?= $subrubric->title; ?></label>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (($key % 4) == 3 || ($key == (count($mainRubrics)-1) && $keySubrubric == (count($subrubrics)-1))) { ?>
                        </div>
                    <?php } ?>
                <?php } ?>

                <div class="col-sm-8 col-sm-offset-2">
                    <div class="row">
                        <div class="col-sm-7">
                            <?php echo $form->field($model, 'id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Company::find()->all(), 'id', 'name'),
                                'options' => ['placeholder' => Yii::t('users', 'COMPANY_NAME')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false); ?>
                            <?php echo $form->field($model, 'type')->widget(Select2::classname(), [
                                'data' => Company::getTypesList(),
                                'options' => ['prompt' => Yii::t('common', 'Choose type')],
                                'pluginOptions' => ['allowClear' => true]
                            ])->label(false); ?>
                        </div>
                        <div class="col-sm-5">
                            <?php if ($action !== 'map') { ?>
                                <a href="<?= Url::to(['map']); ?>" class="btn-green company-in-map">
                                    <?= Yii::t('common', 'companies on map'); ?>
                                </a>
                            <?php } ?>
                            <?php if ($action == 'rating') { ?>
                            <div style="padding-top: <?= $action == 'rating' ? '15' : '57'; ?>px;">
                                <?php echo $form->field($model, 'sort')->widget(Select2::classname(), [
                                    'data' => CompanySearch::getSortingProperties(),
                                    'pluginOptions' => ['allowClear' => true]
                                ])->label(false); ?>
                            </div>
                            <?php } else { ?>
                                <a href="<?= Url::to(['rating']); ?>" class="btn-green company-in-map" style="margin-top: <?= $action == 'companies' ? '15px' : '0'; ?>">
                                    <?= Yii::t('common', 'Rating companies'); ?>
                                </a>
                            <?php } ?>

                        </div>
                        <div class="col-sm-7">
                            <?= Html::submitButton(Yii::t('common', 'Lets search'), ['class' => 'btn-green btn-right']) ?>
                        </div>
                        <div class="col-sm-5">
                            <a href="<?= Url::to([$action]); ?>" class="btn-green btn-left btn-reset">
                                <?= Yii::t('common', 'Reset'); ?>
                            </a>
                        </div>
                    </div>
                    <span class="filter-result text-center">
                        <?= $dataProvider->getTotalCount(); ?>
                        <span><?= Yii::t('common', 'searched companies'); ?></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>