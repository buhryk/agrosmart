<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use backend\modules\commondata\models\Currency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use backend\modules\commondata\models\Region;
use frontend\modules\product\models\Advertisement;
use frontend\modules\product\models\AdvertisementSearch;
use backend\modules\commondata\models\City;
use yii\helpers\HtmlPurifier;

$getCityUrl = \yii\helpers\Url::to(['/city-list/index']);
$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$electedCompaniesIds = $currentUser ? \yii\helpers\ArrayHelper::getColumn($currentUser->electedCompanies, 'id') : [];
$electedAdvertisementsIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedAdvertisements, 'id') : [];
$electedUsersIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedUsers, 'id') : [];
$savedAddresses = $currentUser ? ArrayHelper::getColumn($currentUser->savedAddresses, 'address') : [];
$messageAddToElected = Yii::t('common', 'Add to elected');
$messageRemoveFromElected = Yii::t('common', 'Remove from elected');


$pageH1 =   Yii::t('advertisement', 'Products and services').' : '.HtmlPurifier::process($company->name);
$this->title = $pageH1.' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Companies'), 'url' => ['companies']];
$this->params['breadcrumbs'][] = ['label' => $company->name, 'url' => ['view', 'id' => $company->primaryKey]];
$this->params['breadcrumbs'][] = $pageH1;
$this->registerLinkTag(['rel' => 'canonical', 'href' => '/company/company/products/'.$company->id]);

?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="text-center" style="padding: 15px 0">
    <a href="<?= Url::to(['/product/product/create']); ?>" class="btn-green big">
        <?= Yii::t('advertisement', 'Publish product/service'); ?>
    </a>
</div>

<div class="category-filter">
    <div class="container">
        <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => ['products', 'id' => $company->primaryKey],
            'options' => [
                'class' => 'row',
                'id' => 'advertisements-form'
            ]
        ]); ?>

        <div class="col-sm-6">
            <?php $city = $searchModel->city_id ? City::find()
                ->where([City::tableName().'.id' => $searchModel->city_id])
                ->joinWith('region')
                ->one() : null ?>

            <?= $form->field($searchModel, 'city_id')->widget(Select2::className(), [
                'initValueText' => $city ? $city->detailInfo : '', // set the initial display text
                'options' => [
                    'placeholder' => Yii::t('advertisement', 'Enter city name')
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    'ajax' => [
                        'url' => $getCityUrl,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ])->label(false); ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->field($searchModel, 'region_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Region::find()->joinWith('lang')->all(), 'id', 'title'),
                'options' => [
                    'placeholder' => Yii::t('common', 'Choose region')
                ],
                'pluginOptions' => ['allowClear' => true]
            ])->label(false); ?>
        </div>
        <div class="big-filter text-center">
            <button type="button" class="" data-toggle="collapse" href="#form-up">
                <span class="filter-pro flaticon-down-arrow">
                    <?= Yii::t('advertisement', 'Advanced filter'); ?>
                </span>
            </button>
        </div>
        <div id="form-up" class="collapse in">
            <div class="one-row">
                <div class="col-sm-6 filter-price">
                    <label for=""><?= Yii::t('advertisement', 'Price'); ?>:</label>
                    <div class="row">
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'price_from')->textInput([
                                'placeholder' => Yii::t('advertisement', 'From')
                            ])->label(false); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'price_to')->textInput([
                                'placeholder' => Yii::t('advertisement', 'To')
                            ])->label(false); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'currency_id')
                                ->dropDownList(ArrayHelper::map(Currency::find()->all(), 'id', 'code'))
                                ->label(false); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 filter-price">
                    <label for=""><?= Yii::t('advertisement', 'Rubric'); ?>:</label>
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($searchModel, 'rubric_id')
                                ->dropDownList(ArrayHelper::map($company->advertisementsRubrics, 'id', 'title'), ['prompt' => '...'])
                                ->label(false);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="one-row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'type')->dropDownList(Advertisement::getTypes(),['prompt' => '...']); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'from_who')->dropDownList([
                                Advertisement::FROM_COMPANY => Yii::t('advertisement', 'Companies advertisements'),
                                Advertisement::FROM_PRIVATE_PERSON => Yii::t('advertisement', 'Private persons advertisements'),
                            ], ['prompt' => '...']); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'sort')->dropDownList(AdvertisementSearch::getSortingProperties()); ?>
                        </div>
                    </div>
                </div>
                <?php if ($choosenRubric) { ?>
                    <?php $measurementData = $choosenRubric->getMeasurementsDataForDropDownLists(); ?>
                    <?php if ($measurementData) { ?>
                        <div class="col-sm-6">
                            <label for=""><?= Yii::t('advertisement', 'Volume'); ?>:</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($searchModel, 'volume_from')->textInput([
                                        'placeholder' => Yii::t('advertisement', 'From')
                                    ])->label(false); ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($searchModel, 'volume_to')->textInput([
                                        'placeholder' => Yii::t('advertisement', 'To')
                                    ])->label(false); ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($searchModel, 'measurement_id')->dropDownList($measurementData)->label(false); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
            <p>&nbsp;</p>
            <div class="one-row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12" style="text-align: right">
                            <?php echo Html::submitButton(Yii::t('advertisement', 'Search'), ['class' => 'btn-green']) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12 reset-filter">
                            <a href="<?= Url::to(['products', 'id' => $company->primaryKey]); ?>" class="btn-green">
                                <?= Yii::t('advertisement', 'Reset filter'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="one-row" style="line-height: 44px;">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12 total-count"><?= $dataProvider->getTotalCount(); ?></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12" style="font-size: 19px;">
                            <?= Yii::t('advertisement', 'count of products and services'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="bg-grey">
    <div class="container all-products-container">
        <div class="row">
            <div class="col-sm-9 top-marclear">
                <?php echo \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '@frontend/modules/product/views/product/_advertisement',
                    'viewParams' => [
                        'electedCompaniesIds' => $electedCompaniesIds,
                        'electedAdvertisementsIds' => $electedAdvertisementsIds,
                        'electedUsersIds' => $electedUsersIds,
                        'savedAddresses' => $savedAddresses,
                        'messageAddToElected' => $messageAddToElected,
                        'messageRemoveFromElected' => $messageRemoveFromElected,
                        'company' => $company
                    ]
                ]); ?>
            </div>
            <div class="col-sm-3 sidebar-banner">
                <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
            </div>
        </div>
    </div>
</div>
