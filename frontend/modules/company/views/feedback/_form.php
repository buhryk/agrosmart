<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use frontend\modules\company\models\Feedback;
use kartik\widgets\StarRating;

$classname = \yii\helpers\StringHelper::basename(get_class($model));
?>

<div class="modal-body">
    <div class="subsidiary-form">
        <div id="company-feedback-errors" style="color: #a94442!important;"></div>
        <?php $form = ActiveForm::begin([
            'id'=>'company-feedback-form',
            'enableAjaxValidation' => false,
            'options' => [
                'onsubmit' => 'return false',
                'data-url' => \yii\helpers\Url::to(['feedback/create', 'id' => $company->primaryKey])
            ]
        ]); ?>

        <div style="margin-bottom: 15px;">
            <div class="form-modal-title"><?= Yii::t('common', 'You are offered a deal at the same price, which was listed in the ad / page on the company?'); ?></div>
            <label for="authorized">
                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[price_relevance]'; ?>"
                       value="<?= Feedback::ANSWER_POSITIVE; ?>" data-role="none" checked="checked">
                <?= Yii::t('common', 'Yes'); ?>
            </label>
            <label for="authorized">
                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[price_relevance]'; ?>"
                       value="<?= Feedback::ANSWER_NEGATIVE; ?>" data-role="none" checked="checked">
                <?= Yii::t('common', 'No'); ?>
            </label>
            <label for="authorized">
                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[price_relevance]'; ?>"
                       value="<?= Feedback::ANSWER_EMPTY; ?>" data-role="none" checked="checked">
                <?= Yii::t('common', 'I do not remember'); ?>
            </label>
        </div>
        <div style="margin-bottom: 15px;">
            <div><?= Yii::t('common', 'Product / service matched the description?'); ?></div>
            <label for="authorized">
                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[description_relevance]'; ?>"
                       value="<?= Feedback::ANSWER_POSITIVE; ?>" data-role="none" checked="checked">
                <?= Yii::t('common', 'Yes'); ?>
            </label>
            <label for="authorized">
                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[description_relevance]'; ?>"
                       value="<?= Feedback::ANSWER_NEGATIVE; ?>" data-role="none" checked="checked">
                <?= Yii::t('common', 'No'); ?>
            </label>
            <label for="authorized">
                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[description_relevance]'; ?>"
                       value="<?= Feedback::ANSWER_EMPTY; ?>" data-role="none" checked="checked">
                <?= Yii::t('common', 'I do not remember'); ?>
            </label>
        </div>
        <div style="margin-bottom: 25px;">
            <div><?= Yii::t('common', 'Terms of the transaction (delivery) are met in full?'); ?></div>
            <label for="authorized">
                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[conditions_relevance]'; ?>"
                       value="<?= Feedback::ANSWER_POSITIVE; ?>" data-role="none" checked="checked">
                <?= Yii::t('common', 'Yes'); ?>
            </label>
            <label for="authorized">
                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[conditions_relevance]'; ?>"
                       value="<?= Feedback::ANSWER_NEGATIVE; ?>" data-role="none" checked="checked">
                <?= Yii::t('common', 'No'); ?>
            </label>
            <label for="authorized">
                <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[conditions_relevance]'; ?>"
                       value="<?= Feedback::ANSWER_EMPTY; ?>" data-role="none" checked="checked">
                <?= Yii::t('common', 'I do not remember'); ?>
            </label>
        </div>

        <?= $form->field($model, 'text')->textarea(['style' => 'resize: none;', 'rows' => 4]) ?>
        
        <?= $form->field($model, 'rating')->widget(StarRating::classname(), [
            'pluginOptions' => ['step' => 0.1]
        ]);
        ?>
        <?= $form->field($company, 'id')->hiddenInput()->label(false); ?>

        <div class="form-group">
            <?= Html::button(Yii::t('common', 'Send'), [
                'class' => 'btn-green',
                'id' => 'send-user-feedback',
                'type' => 'button',
                'onclick' => 'return false;',
                'pluginOptions' => [
                    'step' => 0.1,
                    'readonly' => true,
                    'disabled' => true,
                    'showClear' => false,
                    'showCaption' => false
                ],
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>