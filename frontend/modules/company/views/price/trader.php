<?php
/**
 * Created by PhpStorm.
 * User: Borys
 * Date: 03.02.2017
 * Time: 13:34
 */
use frontend\assets\CompanyPriceAsset;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use frontend\assets\CompanyRatingAsset;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use frontend\modules\cabinet\models\Sale;

CompanyPriceAsset::register($this);
CompanyRatingAsset::register($this);
$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$savedAddresses = $currentUser ? ArrayHelper::getColumn($currentUser->savedAddresses, 'address') : [];

$this->title = Yii::t('common', 'PRICE_TRADER');

$pageH1 = Yii::$app->controller->action->id == 'trader-prodazha' ?
    Yii::t('common/title', 'Цены трейдеров : продажа') :
    Yii::t('common/title', 'Цены трейдеров : закупка') ;
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->params['breadcrumbs'][] = $pageH1;
$type = $searchModel->type;
?>
<?php Pjax::begin() ?>
<div class="bread">
    <h1><?=$pageH1 ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-3 sidebar-grey" id="sidebar">
            <div id="blocker"></div>
            <div>
                <a href="<?=Url::to('/company/price/zakupka') ?>" style=" width: 100%" class="btn-green pull-right mtb25t btn-min">
                    <?=Yii::t('common', 'PRICE_FOR_PRODUCT_FOR_AGRICULTURE') ?>
                </a>
                <div class="op-button">
                    <button type="button" class="sr-btn">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img class="sr_close" src="/images/important-close.png" alt="important-close">
                </div>

                <div class="sbbg">
                    <?php $action = Yii::$app->controller->action->id;  ?>
                    <div class="sell-block <?=$action == 'trader-zakupka' ? 'active' : '' ?>">
                        <a href="<?=Url::to(['trader-zakupka']) ?>" class="like-tabs"><?=Yii::t('common', 'ZAKUPKA') ?></a>
                    </div>
                    <div class="sell-block <?=$action == 'trader-prodazha' ? 'active' : '' ?>">
                        <a href="<?=Url::to(['trader-prodazha']) ?>" class="like-tabs"><?=Yii::t('common', 'PRODAZHA') ?></a>
                    </div>
                    <?=$this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-9 logistic-calc" data-content="page_content" >
            <div class="selected-price-block">
                <?=\frontend\modules\company\widgets\SelectedPriceWidget::widget(['type' => $action == 'trader-zakupka' ? Sale::TYPE_ZAKUPKA : Sale::TYPE_PRODAZHA]) ?>
            </div>
            <div class="row">
                <div class="col-md-12 " style="">
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <a href="<?= Url::to(['/site/signup-company']); ?>" class="btn-green pull-right  btn-min mtb25">
                            <?= Yii::t('common', 'Add your company'); ?>
                        </a>
                    <?php } elseif (!Yii::$app->user->identity->company) { ?>
                        <a href="<?= Url::to(['/company/company/create']); ?>" class="btn-green pull-right  btn-min mtb25">
                            <?= Yii::t('common', 'Add your company'); ?>
                        </a>
                    <?php } else { ?>
                        <a href="<?= Url::to(['/cabinet/sale/zakupka']); ?>" style="margin-right: -15px; margin-bottom: 15px;" class="btn-green pull-right mtb25t btn-min">
                            <?= Yii::t('common', 'Добавить цены вашей компании'); ?>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xs-12 important-info">
                <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                <div class="important-info-content">
                    <p class="important-info__title">
<?=Yii::t('common', 'Цены на этой странице собраны из предложений купли/продажи 
                        всех пользователей портала. Цены обновляются ежедневно в живом режиме.') ?>

                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 icon-block-price">
                    <span data-value="block" class="glyphicon glyphicon-th <?=$searchModel->getListType() == 'block' ? 'active' : '';  ?>"></span>
                    <span data-value="list" class="glyphicon glyphicon-th-list <?=$searchModel->getListType() == 'list' ? 'active' : '';  ?>"></span>
                </div>
                <?php echo \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => $searchModel->listType == 'block' ? '_view-block' : '_view',
                    'itemOptions' => [
                        'tag' => false,
                    ],
                    'pager' => [
                        'registerLinkTags' => true,
                    ],
                    'viewParams' => [
                        'savedAddresses' => $savedAddresses,
                        'type' => $type
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs("
        function setEqualHeight(columns) {
            var tallestcolumn = 0;
            columns.each(
                function() {
                    currentHeight = $(this).height();
                    if (currentHeight > tallestcolumn) {
                        tallestcolumn = currentHeight;
                    }
                }
            );
            columns.height(tallestcolumn);
        }

        setEqualHeight($(\".item-block-price\"));
    ",
    \yii\web\View::POS_LOAD,
    'my-button-handler'
); ?>

<?php Pjax::end() ?>

