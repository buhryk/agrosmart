<?php
/**
 * Created by PhpStorm.
 * User: Borys
 * Date: 03.02.2017
 * Time: 13:34
 */
use frontend\assets\CompanyPriceAsset;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use frontend\assets\CompanyRatingAsset;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

CompanyPriceAsset::register($this);
CompanyRatingAsset::register($this);
$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$savedAddresses = $currentUser ? ArrayHelper::getColumn($currentUser->savedAddresses, 'address') : [];

$pageH1 = Yii::$app->controller->action->id == 'prodazha' ?
    Yii::t('common/title', 'Цены на с\х продукцию : продажа') :
    Yii::t('common/title', 'Цены на с\х продукцию : закупка') ;
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);

$this->params['breadcrumbs'][] = $pageH1;
$type = $searchModel->type;
?>

<?php Pjax::begin() ?>
<div class="bread">
    <h1><?=$pageH1?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-3 sidebar-grey" id="sidebar">
            <div id="blocker"></div>
            <div>
                <a href="<?=Url::to('/company/price/trader-zakupka') ?>" style=" width: 100%" class="btn-green pull-right mtb25t btn-min">
                   <?=Yii::t('common', 'PRICE_TRADER') ?>
                </a>
                <div class="op-button">
                    <button type="button" class="sr-btn">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img class="sr_close" src="/images/important-close.png" alt="important-close">
                </div>
                <div class="sbbg">
                    <?php $action = Yii::$app->controller->action->id;    ?>
                    <div class="sell-block <?=$action == 'zakupka' ? 'active' : '' ?>">
                        <a href="<?=Url::to(['zakupka']) ?>" class="like-tabs"><?=Yii::t('common', 'ZAKUPKA') ?></a>
                    </div>
                    <div class="sell-block <?=$action == 'prodazha' ? 'active' : '' ?>">
                        <a href="<?=Url::to(['prodazha']) ?>" class="like-tabs"><?=Yii::t('common', 'PRODAZHA') ?></a>
                    </div>
                    <?=$this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-9 logistic-calc" data-content="page_content">
            <div class="row">
                <div class="col-md-12 " style="">
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <a href="<?= Url::to(['/site/signup-company']); ?>" class="btn-green  pull-right  btn-min mtb25">
                            <?= Yii::t('common', 'Add your company'); ?>
                        </a>
                    <?php } elseif (!Yii::$app->user->identity->company) { ?>
                        <a href="<?= Url::to(['/company/company/create']); ?>" class="btn-green pull-right  btn-min mtb25">
                            <?= Yii::t('common', 'Add your company'); ?>
                        </a>
                    <?php } else { ?>

                    <?php } ?>
                </div>
            </div>

            <div class="col-xs-12 important-info">
                <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                <div class="important-info-content">
                    <p class="important-info__title"><?=Yii::t('common', 'Цены на этой странице собраны из предложений купли/продажи всех пользователей портала. Цены обновляются ежедневно в живом режиме.') ?></p>
                </div>
            </div>



            <div class="row">
                <?php echo \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_view',
                    'pager' => [
                        'registerLinkTags' => true,
                    ],
                    'viewParams' => [
                        'savedAddresses' => $savedAddresses,
                        'type' => $type
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php Pjax::end() ?>