
<?php
use yii\helpers\Url;
use kartik\rating\StarRating;
use frontend\modules\cabinet\models\Sale;
use yii\helpers\Html;

$listSale = Sale::getCompanySaleList($model->id, $type);
$action = Yii::$app->controller->action->id;

?>

<div class="col-xs-12 price-cart">
    <div class="sales-product clear">
        <div class="col-sm-2">
            <a href="<?=Url::to(['company/view', 'id' => $model->id]) ?>" data-pjax="0">
                <figure class="effect-chico">
                    <?php $logoPath = $model->logotype ? '/'.$model->logotype : Yii::getAlias('@web').'/images/user_274x185.jpg'; ?>
                    <img src="<?=\frontend\components\tool\Image::resize($logoPath ,115 , 77) ?>">
                </figure>
            </a>
        </div>
        <div class="col-sm-6">
            <a href="<?=Url::to(['company/view', 'id' => $model->id]) ?>" data-pjax="0">
                <?= Html::encode($model->name); ?>
            </a>
            <div class="location">
                <?php $contactInfo = $model->contactInfo; ?>
                <?php foreach ($contactInfo as $info) { ?>
                    <div><?= $info; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-sm-4">
            <i><?= Yii::t('common', 'Company was on the portal'); ?>:&nbsp;<?= date('d.m.Y', $model->last_activity_date); ?></i>
            <?php
                $advertisementsCount = $model->advertisementsCount;
                $saleZakupkaCount = $model->saleZakupkaCount;
                $saleProdCount = $model->saleProdCount;
            ?>
            <?php if ($advertisementsCount) { ?>
                <a href="<?= Url::to(['/company/company/products', 'id' => $model->id]); ?>" class="link-underline" data-pjax="0">
                    <?= Yii::t('advertisement', 'Products and services'); ?>&nbsp;(<?= $advertisementsCount; ?>)
                </a>
            <?php } ?>

            <?php if ($saleZakupkaCount) { ?>
                <a href="<?= Url::to(['/company/price/zakupka-info', 'company_id' => $model->id]); ?>" class="link-underline" data-pjax="0">
                    <?= Yii::t('common', 'PURCHASE PRICE'); ?>&nbsp;(<?= $saleZakupkaCount; ?>)
                </a>
            <?php } ?>

            <?php if ($saleProdCount) { ?>
                <a href="<?= Url::to(['/company/price/prodazha-info', 'company_id' => $model->id]); ?>" class="link-underline" data-pjax="0">
                    <?= Yii::t('common', 'Sales prices'); ?>&nbsp;(<?= $saleProdCount; ?>)
                </a>
            <?php } ?>
            <div id="stars-gold" data-rating="3">
                <?php echo StarRating::widget([
                    'name' => 'rating_company_'.$model->primaryKey,
                    'value' => $model->ratio,
                    'pluginOptions' => [
                        'step' => 0.1,
                        'readonly' => true,
                        'disabled' => true,
                        'showClear' => false,
                        'showCaption' => false
                    ],
                ]); ?>
           </div>
        </div>
    </div>
    <div class="table-container">
        <?php if (count($listSale)) : ?>
        <table class="branches-table little col-xs-12 text-center">
            <tbody>
            <tr class="grey-row">
                <th><?=Yii::t('common', 'Products') ?></th>
                <th><?=Yii::t('common', 'Price') ?></th>
                <th><?=Yii::t('cabinet', 'VOLUME') ?> </th>
                <th><?=Yii::t('common', 'Address') ?></th>
                <th></th>
            </tr>

            <?php if(is_array($listSale)): ?>
                <?php foreach ($listSale as $item): ?>
                    <tr class="white-row">
                        <td width="30%"><?=$item->rubric->title ?> </td>
                        <td><?=ceil($item->price) ?> </td>
                        <td width="130px"><?=ceil($item->generalVolume) ?> <?=$item->measurementTitle ?></td>
                        <td> <?=$item->location ?> </td>

                        <?php if (!Yii::$app->user->isGuest && !in_array($item->location , $savedAddresses)) { ?>
                            <td>
                                <a class="green-link add-address-to-saved" data-address="<?= $item->location ; ?>" style="cursor:pointer;">
                                    <?= Yii::t('common', 'Remember address'); ?>
                                </a>
                            </td>
                        <?php } ?>

                    </tr>
                <?php endforeach; ?>
            <?php endif;  ?>
            </tbody>
        </table>
        <?php endif; ?>
        <div class="">
            <a href="<?=Url::to([$action.'-info', 'company_id' => $model->id]) ?>" class="btn-green btn-min" data-pjax="0">
                <?=Yii::t('common', 'See all') ?>
            </a>
        </div>
    </div>
</div>