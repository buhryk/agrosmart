<?php
use yii\bootstrap\ActiveForm;
use backend\modules\rubric\models\Rubric;
use backend\modules\commondata\models\Region;
use frontend\modules\company\models\CompanyPriceSearch;

$classname = \yii\helpers\StringHelper::basename(get_class($model));
?>
<?php $form = ActiveForm::begin([
    'action' => [Yii::$app->controller->action->id],
    'method' => 'get',
    'id' => 'price-search',
    'options'=>[
        'data-pjax'=>'#x1g'
    ],
]); ?>

    <div class="clearfix"></div>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <h2 class="sidebar-grey__title"><?=Yii::t('common', 'Продукция') ?></h2>
        <div class="panel panel-default">
            <?php $listRubric = $model->getRubricList(); ?>

            <?php if(empty($model->trader)): ?>

                <?php foreach ($listRubric as $item): ?>
                    <div class="panel-heading" role="tab" id="headingOne-<?=$item['id'] ?>">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne-<?=$item['id'] ?>" aria-expanded="true" aria-controls="collapseOne-<?=$item['id'] ?>">
                                <?=$item['title'] ?>
                            </a>
                        </h4>
                    </div>

                    <?php
                    $checked = '';
                    $in = '';
                    $str = '';
                    if(isset($item['child'])):
                        foreach ($item['child'] as $child) {

                            if(!empty($child['child'])){
                                $input = '';
                                foreach ($child['child'] as $child2) {
                                    if (is_array($model->rubrics) && in_array($child2['id'], $model->rubrics)) {
                                        $checked = 'checked';
                                        $in = ' in';
                                    } else {
                                        $checked = '';
                                    }
                                    $input .= '<input type="checkbox"   name="' . $classname . '[rubrics][]"  id="test-id-'.$child2['id'] .'" value="' . $child2['id'] . '" ' . $checked . '>';
                                    $input .= '<label for="test-id-'.$child2['id'].'">' . $child2['title'] . '</label>';
                                }

                                $str.='
                                    <div class="accordion-group sub-accordion">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseInnerOne-'.$child['id'].'">
                                                '.$child['title'].'
                                            </a>
                                        </div>
                                        <div id="collapseInnerOne-'.$child['id'].'" class="accordion-body collapse  '.$in.'">
                                            <div class="accordion-inner">
                                                   '.$input.'
                                            </div>
                                        </div>
                                    </div>
                                ';
                            }

                            if(empty($child['child'])){
                                if (is_array($model->rubrics) && in_array($child['id'], $model->rubrics)) {
                                    $checked = 'checked';
                                    $in = 'in';
                                } else {
                                    $checked = '';
                                }
                                $str .= '<input type="checkbox"   name="' . $classname . '[rubrics][]"  id="test-id-'.$child['id'] .'" value="' . $child['id'] . '" ' . $checked . '>';
                                $str .= '<label for="test-id-'.$child['id'].'">' . $child['title'] . '</label>';

                            }
                        }
                        ?>
                        <div id="collapseOne-<?=$item['id'] ?>"
                             class="panel-collapse collapse <?=$in ?>"
                             role="tabpanel"
                             aria-labelledby="headingOne-<?=$item['id'] ?>">
                            <div class="panel-body">
                                <?=$str ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?=$form->field($model, 'listType')->hiddenInput()->label(false) ?>
                <?php foreach ($listRubric as $item):  ?>
                    <div class="sidebar-light-grey-input  panel-heading">
                        <input type="checkbox" id="category-id-<?=$item['id'] ?>"
                               name="<?=$classname ?>[rubrics][]" value="<?=$item['id'] ?>"
                            <?=is_array($model->rubrics) && in_array($item['id'], $model->rubrics) ? 'checked' : '' ?>>
                        <label for="category-id-<?=$item['id'] ?>"><?=$item['title'] ?></label>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <h2 class="sidebar-grey__title reg"><?=Yii::t('common', 'Regions') ?></h2>
    <div class="sidebar-grey-region">
        <?php $region = Region::find()->all() ?>
        <input type="hidden" name="<?=$classname ?>[oblast]">
        <?php foreach ($region as $item): ?>
            <div class="sidebar-grey-input col-sm-12">
                <input type="checkbox" id="oblast-id-<?=$item->id ?>"
                       name="<?=$classname ?>[oblast][]" value="<?=$item->id ?>"
                    <?=is_array($model->oblast) && in_array($item->id, $model->oblast) ? 'checked' : '' ?>>
                <label for="oblast-id-<?=$item->id ?>"><?=$item->title ?></label>
            </div>
        <?php endforeach; ?>
    </div>
<?php ActiveForm::end(); ?>