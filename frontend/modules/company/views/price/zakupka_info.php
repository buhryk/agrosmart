<?php
/**
 * Created by PhpStorm.
 * User: Borys
 * Date: 03.02.2017
 * Time: 13:34
 */
use frontend\assets\CompanyPriceAsset;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use frontend\assets\CompanyRatingAsset;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use frontend\modules\cabinet\models\Sale;
use yii\helpers\Html;

CompanyPriceAsset::register($this);
CompanyRatingAsset::register($this);
$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$savedAddresses = $currentUser ? ArrayHelper::getColumn($currentUser->savedAddresses, 'address') : [];

$company_id = Yii::$app->request->get('company_id');

$pageH1 = Yii::t('common', 'PURCHASE PRICE').' : '.$company->name;
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['/company/price/zakupka-info', 'company_id' => $company->id])]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'PRICE_FOR_PRODUCT_FOR_AGRICULTURE'), 'url' => ['/company/price/zakupka']];
$this->params['breadcrumbs'][] = $pageH1;



?>
<? Pjax::begin() ?>
<div class="bread">
    <h1><?=Html::encode($pageH1) ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">

        <div>
            <ul class="nav nav-tabs ap-choose sale-ul-tab">
                <li class="ap-choose__item <?=$currency == Sale::TABLE_UAH ? 'active' : '' ?>">
                    <a  href="<?=Url::to(['zakupka-info','company_id'=> $company->id]) ?>">
                        <?=Yii::t('cabinet', 'UAH_TABLE') ?>
                    </a>
                </li>
                <li class="ap-choose__item <?=$currency == Sale::TABLE_CURRENCY ? 'active' : '' ?>">
                    <a  href="<?=Url::to(['zakupka-info', 'company_id'=> $company->id, 'currency' => Sale::TABLE_CURRENCY]) ?>">
                        <?=Yii::t('cabinet', 'USD_TABLE') ?>
                    </a>
                </li>
            </ul>
            <?=$this->render('common_info', [
                'saleItems' => $saleItems,
                'model'=>$model,
                'company' => $company,
                'currency' => $currency,
                'type' => Sale::TYPE_ZAKUPKA
            ]) ?>
        </div>

    </div>
</div>

<? Pjax::end() ?>