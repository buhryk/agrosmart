<?php
use backend\modules\price2\models\Price;
use frontend\assets\PriceAsset;
use frontend\models\Company;
use frontend\modules\company\models\PriceSearch;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

PriceAsset::register($this);

$model_price = new PriceSearch();
$id = Yii::$app->request->get('id');
$sale_count = $model_price->countSearch(Price::TYPE_SALE, '', true, $id);
$purchase_count = $model_price->countSearch(Price::TYPE_PURCHASE, '', true, $id);
$current_count = ($type == Price::TYPE_SALE) ? $sale_count : $purchase_count;

$this->title = Yii::t('price', 'Prices').': ' . $price->company->name;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('price', 'Company'),
    'url' => '/company'
];
$this->params['breadcrumbs'][] = [
    'label' => $price->company->name,
    'url' => Url::to(['/company/company/view', 'id' => $price->company->id])
];
$this->params['breadcrumbs'][] = Yii::t('price', 'Prices');

?>


<div class="bread">
    <h1><?= $this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<?php Pjax::begin(['timeout' => 3000]) ?>
<div class="container">
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-sm-9">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav b-tab__nav b-prise-profile__tab-nav ap-choose sale-ul-tab">
                            <li class="ap-choose__item <?= ($type == Price::TYPE_SALE) ? 'active' : '' ?>"
                                style="<?= (!$sale_count) ? 'pointer-events: none; cursor: default;  color: #999"' : ''?>">
                                <a href="<?= Url::toRoute(['all', 'id' => $id, 'type' => Price::TYPE_SALE]) ?>">
                                    <?= Yii::t('price', 'sale') ?> <span>(<?= $sale_count ?>)</span>
                                </a>
                            </li>
                            <li class="ap-choose__item <?= ($type == Price::TYPE_PURCHASE) ? 'active' : '' ?>"
                                style="<?= (!$purchase_count) ? 'pointer-events: none; cursor: default;  color: #999"' : ''?>">
                                <a href="<?= Url::toRoute(['all', 'id' => $id, 'type' => Price::TYPE_PURCHASE]) ?>">
                                    <?= Yii::t('price', 'purchase') ?> <span>(<?= $purchase_count ?>)</span>
                                </a>
                            </li>
                        </ul>
                        <div class="b-table">
                            <div class="b-table__list">
                                <?php foreach ($prices as $price): ?>
                                    <div class="b-panel">
                                        <div class="b-panel__info">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-8">
                                                    <div class="b-panel__title">
                                                        <a href="<?= Url::toRoute([
                                                            '/company/price/view',
                                                            'id' => $price['id']
                                                        ]) ?>">
                                                            <b><?= Html::encode($price['name']) ?></b>
                                                        </a>
                                                    </div>
                                                    <div class="b-panel__date">
                                                                            <span class="b-panel__date-line"><?= date('d.m.y',
                                                                                    $price['updated_at']) ?></span>
                                                        <span><?= ($price['type'] == 0) ?
                                                                mb_convert_case( Yii::t('price', 'sale'), MB_CASE_TITLE, "UTF-8" ) :
                                                                mb_convert_case( Yii::t('price', 'purchase'), MB_CASE_TITLE, "UTF-8" ) ?></span>
                                                    </div>
                                                    <div class="b-panel__text">
                                                        <p><?= StringHelper::truncate(Html::encode($price['description']),
                                                                180) ?></p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4">
                                                    <div class="b-panel__cell-right">
                                                        <?php
                                                            $company = Company::find()->where(['id' => $price['company_id']])->one();
                                                        ?>
                                                        <a href="<?= Url::toRoute([
                                                            '/company/company/view',
                                                            'id' => $company->id
                                                        ]) ?>" data-pjax="0">
                                                            <?= Html::encode($company->name) ?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="b-pagination">
                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pagination,
                        'maxButtonCount' => 6,
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?= \backend\modules\advertising\widgets\AdvertisingWidget::widget([
                'key' => 'content',
                'typePosition' => 'apeak'
            ]) ?>
        </div>
    </div>
</div>

<?php Pjax::end() ?>
