<?php
use common\components\LinguaStemRu;
use frontend\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;

foreach ($prices as $item): ?>
        <div class="b-panel">
            <div class="b-panel__info">
                <div class="row b-panel__flex">
                    <div class="col-xs-8 col-sm-8">
                        <div class="b-panel__title">
                            <a href="<?= Url::toRoute([
                                '/company/price/view',
                                'id' => $item['p_id'],
                                'item' => $item['p_i_id']
                            ]) ?>">
                                <?php
                                $stemmer = new LinguaStemRu();
                                ?>
                                <?= \yii\helpers\HtmlPurifier::process( $stemmer->allotment($q, $item['p_i_name']) ) ?>
                            </a>
                        </div>
                        <div class="b-panel__date"><span>
                                <?= date('d.m.y',
                                    $item['p_updated_at']) ?>
                            </span></div>
                        <div class="b-panel__text">
                            <p>
                                <a href="<?= Url::toRoute([
                                    '/company/price/view',
                                    'id' => $item['p_id'],
                                ]) ?>">
                                    <?= Html::encode($item['p_name']) ?>
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-1 b-panel__flex-center">
                                                <span>
                                                    <a href="<?= Url::toRoute([
                                                        '/company/price/view',
                                                        'id' => $item['p_id'],
                                                        'item' => $item['p_i_id']
                                                    ]) ?>">
                                                        <?= \common\helpers\MyHelpers::typePrice($item['price'])  ?>
                                                    </a>
                                                </span>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="b-panel__cell-right">
                            <?php
                                $company = Company::find()->where(['id' => $item['company_id']])->one();
                            ?>
                            <a href="<?= Url::toRoute([
                                '/company/company/view',
                                'id' => $company->id
                            ]) ?>" data-pjax="0" >
                                <?= Html::encode($company->name) ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php endforeach; ?>