<?php
use yii\helpers\Url;
use kartik\widgets\StarRating;
use yii\helpers\Html;

$action = Yii::$app->controller->action->id;
?>
<div class="col-xs-6 col-md-3 item-block-price">
    <a href="<?=Url::to([$action.'-info', 'company_id' => $model->id]) ?>">
        <figure class="effect-chico">
            <?php $logoPath = $model->logotype ? '/'.$model->logotype : Yii::getAlias('@web').'/images/user_274x185.jpg'; ?>
            <img src="<?=\frontend\components\tool\Image::resize($logoPath ,274 , 185) ?>">
        </figure>
        <div id="stars-gold" data-rating="3">
            <?php echo StarRating::widget([
                'name' => 'rating_company_'.$model->primaryKey,
                'value' => $model->ratio,
                'pluginOptions' => [
                    'step' => 0.1,
                    'readonly' => true,
                    'disabled' => true,
                    'showClear' => false,
                    'showCaption' => false
                ],
            ]); ?>
        </div>
        <h3 class="title-item-block"><?=\yii\helpers\Html::encode($model->name); ?> </h3>
    </a>
 </div>
