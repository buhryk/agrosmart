<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

\frontend\assets\PriceAsset::register($this);

$type = ($price->type == 0) ? Yii::t('price', 'sale') : Yii::t('price', 'purchase');

$this->title = Html::encode( $price->name );
$this->params['breadcrumbs'][] = [
        'label' => Yii::t('price', 'Company'),
        'url' => '/company'
    ];
$this->params['breadcrumbs'][] = [
    'label' => $price->company->name,
    'url' => Url::to(['/company/company/view', 'id' => $price->company->id])
];
$this->params['breadcrumbs'][] = [
        'label' => Yii::t('price', 'Prices'),
       'url' => Url::to(['/company/price/all', 'id' => $price->company->id]).'#product-block'
    ];
$this->params['breadcrumbs'][] = Html::encode( $price->name );
?>

<div class="bread">
    <h1><?= Yii::t('price', 'Price') . ' '?><?= Yii::t('price', $type)?> : <?= $price->company->name ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">

        <!--price-list-->
        <div class="b-prise-result col-md-8 col-md-push-2 clearfix">
            <div class="b-prise-result__info">
                <div class="b-prise-result__title"><?= Html::encode( $price->name )?></div>
                <div class="b-prise-result__date"><span class="b-prise-result__date-line">
                        <?= date('d.m.y', $price->updated_at) ?>
                    </span><span><?= mb_convert_case($type, MB_CASE_TITLE, "UTF-8") ?></span></div>
                <div class="view-price">
                    <div class="b-prise-result__text">
                        <p>
                            <a href="<?= Url::toRoute(['/company/company/view', 'id' => $price->company->id]) ?>">
                               <b><?= $price->company->name ?></b>
                            </a>
                        </p>
                    </div>
                    <div class="b-prise-result__text">
                        <p class="price-description"><?= Html::encode( $price->description ) ?></p>
                    </div>
                </div>
            </div>
            <div class="b-table-container">
                <div class="b-table-wrap clearfix">
                    <table class="b-table text-center">
                        <tbody>
                        <tr class="white-row">
                            <th><?= Yii::t('price','denomination')?></th>
                            <th><?= Yii::t('price','cost')?></th>
                            <?php
                                $name1 = ($price->priceColumns->name1  ) ? $price->priceColumns->name1 : false;
                                $name2 = ($price->priceColumns->name2 ) ? $price->priceColumns->name2 : false;
                                $name3 = ($price->priceColumns->name3 ) ? $price->priceColumns->name3 : false;

                                echo ($name1) ?  '<th>' . Html::encode( $name1 ) . '</th>' : '';
                                echo ($name2) ?  '<th>' . Html::encode( $name2 ). '</th>' : '';
                                echo ($name3) ?  '<th>' . Html::encode( $name3 ) . '</th>' : '';
                            ?>
                        </tr>
                        <?php foreach ($price_item as $item):?>
                        
                            <tr class="<?= ($item->id != Yii::$app->request->get('item')) ? 'white-row' : 'grey-row'?>">
                                <td><?= $item->name ?></td>
                                <td><?= \common\helpers\MyHelpers::typePrice($item->price)  ?></td>
                                <?php
                                    echo ($name1) ?  '<td>' . Html::encode( $item->column1 ) . '</td>' : '';
                                    echo ($name2) ?  '<td>' . Html::encode( $item->column2 ). '</td>' : '';
                                    echo ($name3) ?  '<td>' . Html::encode( $item->column3 ) . '</td>' : '';
                                ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-8 col-sm-push-2 text-center">
                <a href="<?= Url::toRoute(['/company/company/view', 'id' => $price->company->id]) ?>" class="btn-green b-prise-result_btn"><?= Yii::t('price','Go to company profile')?></a>
            </div>
        </div>
        <!--end price-list--->


    </div>
</div>