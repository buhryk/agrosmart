<?php
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Html;
use frontend\modules\cabinet\models\Sale;

$subsidiary = $company->subsidiaries;
$action = Yii::$app->controller->action->id;


?>
<div class="branches sale-block-content">
     <div class="row">
          <div class="show-prices">
                        <?php $form = ActiveForm::begin([
                            'method'=>'get',
                            'action'=>[$action, 'company_id' => $company->id, 'currency' => $currency],
                            'options' => [
                                'data-pjax' => true,
                                'id' => 'data-rang-sale'
                            ]]); ?>
              <div class="row">
                  <div class="col-md-4">
                        <span><?=Yii::t('cabinet', 'Анализ цен') ?> : </span>
                        <div id="sale-date-range">
                            <?= DatePicker::widget([
                                'model' => $model,
                                'attribute' => 'from_date',
                                'attribute2' => 'to_date',

                                'value' => $model->from_date ? date('Y-m-d') : $model->from_date,
                                'type' => DatePicker::TYPE_RANGE,
                                'options' => [],
                                'separator' => '<i class="glyphicon glyphicon-calendar"></i>',
                                'value2' => $model->to_date ? date('Y-m-d') : $model->to_date,
                                'pluginOptions' => [
                                    'minDate'=> date('Y-m-d'),
                                    'autoclose'=>true,
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true,
                                    "onSelectDate" => "alert('tesssst')",
                                ],
                                'pluginEvents' => [
                                    "changeDate" => "function(){ $('form#data-rang-sale').submit();}",
                                ],
                            ]); ?>
                        </div>
                  </div>
                   <div class="col-md-4">
                       <p>
                           <br>

                           <?=$type == Sale::TYPE_ZAKUPKA
                               ? Yii::t('price', 'Таблица закупок за')
                               : Yii::t('price', 'Таблица продаж за') ?>
                           : <?=$model->to_date ?   $model->to_date : date('Y-m-d') ?>
                       </p>
                   </div>
              </div>
                        <?php ActiveForm::end() ?>
                    </div>
          <div id="price-page">
                        <table class="branches-table purchase-table col-xs-12">
                            <tbody>
                            <tr class="grey-row">
                                <th><?=Yii::t('common', 'Products') ?></th>
                                <th><?=Yii::t('cabinet', 'VOLUME') ?> </th>
                                <th><?=Yii::t('common', 'Price') ?></th>
                                <th><?=Yii::t('common', 'It is the product') ?><img class="table-info-button" src="/images/info.png" alt="info"></th>
                                <th><?= Yii::t('cabinet', 'Описание месторасположения') ?></th>

                            </tr>
                            <?php foreach ($saleItems as $key => $item):  ?>
                                <tr class="white-row">
                                    <td><?=$item['rubric'] ?> </td>
                                    <td><?=$item['volume'] ?> <span class="pull-right"><?=$item['measurement'] ?></span> </td>
                                    <td><?=$item['price'] ?>
                                        <?php if($item['diff'] > 0): ?>
                                            <span class='new-price-top p-green'>+ <?=$item['diff']?></span>
                                        <?php elseif ($item['diff'] < 0): ?>
                                            <span class='new-price-top p-red'><?=$item['diff']?></span>
                                        <?php endif; ?>
                                    </td>
                                    <td><?=$item['location'] ?> </td>
                                    <td><?=$item['description_address'] ?> </td>
                                    <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->id != $item['author_id']) {?>
                                    <td>
                                           <?php echo Html::a('<i class="glyphicon glyphicon-envelope"></i>',
                                                ['/cabinet/dialog/create-message-modal', 'receiver' => $item['author_id']],
                                                ['class' => 'modalButton']
                                            ,['encode' => false,]);
                                         ?>
                                    </td>
                                    <?php } ?>

                                </tr>

                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
     </div>
    <div class="row">
        
    </div>
</div>
<?php if ($subsidiary) { ?>
    <?php $addresses = []; ?>
    <div class="col-sm-12 pt50 pb50 another-product" id="contact-block">
        <span class="fs19 fw600 ttu din"><?= Yii::t('common', 'Branches and contacts'); ?></span>
        <table class="branches-table col-xs-12 mt20 text-center">
            <tbody>
            <tr class="grey-row">
                <th class="text-center"><?= Yii::t('cabinet', 'CITY'); ?></th>
                <th class="text-center"><?= Yii::t('cabinet', 'TYPE'); ?></th>
                <th class="text-center"><?= Yii::t('cabinet', 'ADDRESS'); ?></th>
                <th class="text-center"><?= Yii::t('cabinet', 'PHONE'); ?></th>
                <th class="text-center">Email</th>
            </tr>
            <?php foreach ($subsidiary as $one) { ?>
                <?php $addresses[] = [
                    'detailAddress' => $one->detailAddress,
                    'simpleAddress' => $one->city->title . ', ' . $one->address
                ]; ?>
                <tr class="white-row">
                    <td><?= $one->city->title; ?></td>
                    <td><?= $one->getType(); ?></td>
                    <td><?= $one->address; ?></td>
                    <td><?= $one->phone; ?></td>
                    <td><?= $one->email; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="clearfix"></div>
        <div id="subsidiaries-map" style="width: 100%;  margin-bottom: 10px; margin-top: 25px;"></div>

        <?php $locations = "[";
        foreach ($addresses as $location) {
            $locations .= "{detailAddress: '".$location['detailAddress']."', simpleAddress: '".$location['simpleAddress']."'},";
        }
        $locations = trim($locations, ',') . "]";

        $this->registerJs("var locations = ".$locations.";",
            \yii\web\View::POS_HEAD,
            'my-button-handler'
        ); ?>
    </div>
<?php } ?>