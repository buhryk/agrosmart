<?php
use frontend\assets\PriceAsset;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;
use common\components\LinguaStemRu;

PriceAsset::register($this);

$this->title = Yii::t('price', 'Search');
$this->params['breadcrumbs'][] = Yii::t('price', 'Prices');
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="bread">
    <h1><?= $this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9">
            <div class="block-input-search b-search__search-field">
                <form action="<?= \yii\helpers\Url::toRoute(['search-rezult']) ?>">
                    <input type="text"
                           id="price-search-field"
                           class="form-control"
                           name="q"
                           placeholder='<?= Yii::t('price', 'Enter text to search for') ?>'/>
                    <button class="send-off"><i class="glyphicon glyphicon-search"></i></button>
                </form>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="b-table">
                        <div class="b-table__list">
                            <?php foreach ($prices as $price): ?>
                                <div class="b-panel">
                                    <div class="b-panel__info">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-8">
                                                <div class="b-panel__title">
                                                    <a href="<?= Url::toRoute([
                                                        '/company/price/view',
                                                        'id' => $price->id
                                                    ]) ?>">
                                                        <b><?= Html::encode($price->name) ?></b>
                                                    </a>
                                                </div>
                                                <div class="b-panel__date">
                                                    <span class="b-panel__date-line"><?= date('d.m.y',
                                                            $price->updated_at) ?></span>
                                                    <span><?= ($price->type == 0) ?
                                                            mb_convert_case( Yii::t('price', 'sale'), MB_CASE_TITLE, "UTF-8" ) :
                                                            mb_convert_case( Yii::t('price', 'purchase'), MB_CASE_TITLE, "UTF-8" ) ?></span>
                                                </div>
                                                <div class="b-panel__text">
                                                    <p><?= StringHelper::truncate(Html::encode($price->description),
                                                            180) ?></p>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-4">
                                                <div class="b-panel__cell-right">
                                                    <a href="<?= Url::toRoute([
                                                        '/company/company/view',
                                                        'id' => $price->company->id
                                                    ]) ?>">
                                                        <?= Html::encode($price->company->name) ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="b-pagination">
                            <?php
                            echo LinkPager::widget([
                                'pagination' => $pagination,
                                'maxButtonCount' => 6,
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?= \backend\modules\advertising\widgets\AdvertisingWidget::widget([
                'key' => 'content',
                'typePosition' => 'apeak'
            ]) ?>
        </div>
    </div>
</div>
