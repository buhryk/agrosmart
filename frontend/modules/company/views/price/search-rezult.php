<?php
use backend\modules\price2\models\Price;
use common\components\LinguaStemRu;
use frontend\assets\PriceAsset;
use frontend\modules\company\models\PriceSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

PriceAsset::register($this);

$model = new PriceSearch();

$sale_count = $model->countSearch(Price::TYPE_SALE, $q, false, null);
$purchase_count = $model->countSearch(Price::TYPE_PURCHASE, $q, false, null);

$this->title = Yii::t('price', 'Search'). ' " '.$q.' "';
$this->params['breadcrumbs'][] = ['label' => Yii::t('price', 'Prices'), 'url' => Url::toRoute('search-index')];

if ($q) {
    $this->params['breadcrumbs'][] = $q;
}

$this->params['breadcrumbs'][] = ($type == 0) ? Yii::t('price', 'sale') : Yii::t('price', 'purchase');
?>

<?php Pjax::begin(['timeout' => 3000]) ?>
    <div class="bread">
        <h1><?= $this->title ?></h1>
        <div class="bread-bg">
            <div class="container">
                <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <!--price-search-->
            <div class="col-sm-12">
                <div class="block-input-search b-search__search-field">
                    <form>
                        <input type="text"
                               id="price-search-field"
                               class="form-control"
                               name="q"
                               value="<?= $q ?>"
                               placeholder='<?= Yii::t('price', 'Enter text to search for') ?>'/>
                        <button class="send-off"><i class="glyphicon glyphicon-search"></i></button>
                    </form>
                </div>
                <!--price-->
                <div class="row">
                    <div class="col-md-9" id="top-search-price">
                        <ul class="nav b-tab__nav ap-choose sale-ul-tab">
                            <li class="ap-choose__item <?= ($type == Price::TYPE_SALE) ? 'active' : '' ?>"
                                style="<?= (!$sale_count) ? 'pointer-events: none; cursor: default;  color: #999"' : ''?>"
                            >
                                <a href="<?= Url::toRoute(['search-rezult', 'type' => Price::TYPE_SALE, 'q' => $q]) ?>">
                                    <?= Yii::t('price', 'sale') ?> <span>(<?= $sale_count ?>)</span>
                                </a>
                            </li>
                            <li class="ap-choose__item <?= ($type == Price::TYPE_PURCHASE) ? 'active' : '' ?>"
                                style="<?= (!$purchase_count) ? 'pointer-events: none; cursor: default;  color: #999"' : ''?>" >
                                <a href="<?= Url::toRoute([
                                    'search-rezult',
                                    'type' => Price::TYPE_PURCHASE,
                                    'q' => $q
                                ]) ?>">
                                    <?= Yii::t('price', 'purchase') ?> <span>(<?= $purchase_count ?>)</span>
                                </a>
                            </li>
                        </ul>
                        <div class="b-table">
                            <div class="b-table__list">

                                <?= $this->render('search_item', ['prices' => $prices,  'q' => $q]) ?>

                            </div>
                            <div class="b-pagination">
                                <?php
                                    echo LinkPager::widget([
                                        'pagination' => $pagination,
                                        'maxButtonCount' => 6,
                                    ]);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 sidebar-banner">
                        <?= \backend\modules\advertising\widgets\AdvertisingWidget::widget([
                            'key' => 'content',
                            'typePosition' => 'apeak'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$script = <<< JS
    $('html, body').animate({scrollTop: $('#top-search-price').offset().top  }, 99 );
JS;
$this->registerJs($script);
Pjax::end()
?>