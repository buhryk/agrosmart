<?php

namespace frontend\modules\ordertransport\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use frontend\modules\ordertransport\models\Order;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class OrderController extends Controller
{
    public function actionCreate()
    {
        $model = new Order();

        if (Yii::$app->user->isGuest) {
            return $this->renderAjax('modal');
        } else {
            if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                $model->user_id = Yii::$app->user->identity->id;

                $validate = $model->validate();
                $errors = $model->getErrors();

                $response = [
                    'status' => true,
                    'validation' => $validate,
                ];

                if (!$validate) {
                    $response['errors'] = $errors;
                } else {
                    $model->save();
                    Yii::$app->session->setFlash('success', Yii::t('instruments', 'Your order have successfully applied'));
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                return $response;
            } else {
                return $this->renderAjax('create', [
                    'model' => $model
                ]);
            }
        }
    }
}