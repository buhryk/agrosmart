<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\modules\ordertransport\models\Order as Model;
?>

<div class="modal-body">
    <div class="ordertransport-form">
        <div id="ordertransport-errors" style="color: #a94442;"></div>
        <?php $form = ActiveForm::begin([
            'id' => 'form-ordertransport',
            'enableAjaxValidation' => false,
            'options' => [
                'onsubmit' => 'return false',
                'data-url' => Url::to(['create'])
            ]
        ]); ?>

        <?= $form->field($model, 'from_location')->textInput(['id' => 'from_location_ordertransport']); ?>
        <?= $form->field($model, 'to_location')->textInput(['id' => 'to_location_ordertransport']); ?>

        <script type="application/javascript">
            new google.maps.places.Autocomplete(document.getElementById('from_location_ordertransport'));
            new google.maps.places.Autocomplete(document.getElementById('to_location_ordertransport'));
        </script>

        <?= $form->field($model, 'transport_type')->dropDownList(Model::getTransportTypesList(), ['prompt' => Yii::t('instruments', 'Tonnage and volume')]); ?>
        <?= $form->field($model, 'message')->textarea(['rows' => 6, 'style' => 'resize: none;']); ?>

        <div class="form-group">
            <?php echo Html::button(Yii::t('common', 'Send'), [
                'class' => 'btn btn-primary',
                'id' => 'save-ordertransport',
                'type' => 'button',
                'onclick' => 'return false;'
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>