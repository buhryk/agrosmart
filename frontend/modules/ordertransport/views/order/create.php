<?php
use yii\helpers\Html;

$this->title = Yii::t('instruments', 'Route and transport tonnage');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="modal-dialog modal-ms">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title custom-font" style="display: inline-block;"><?= Html::encode($this->title) ?></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="/images/close-modal.png" alt="close-modal">
            </button>
        </div>
        <?= $this->render('_form', [
            'model' => $model
        ]) ?>
    </div>
</div>