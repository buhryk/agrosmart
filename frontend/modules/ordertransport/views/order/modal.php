<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('instruments', 'Route and transport tonnage');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="modal-dialog modal-ms">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title custom-font" style="display: inline-block;"></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="/images/close-modal.png" alt="close-modal">
            </button>
        </div>
        <div class="modal-body" style="padding-bottom: 50px;">
            <h3 style="text-align: center">
                <?= Yii::t('common', 'Данный функционал доступен только для авторизированных пользователей'); ?>
            </h3>
            <div style="text-transform: uppercase; text-align: center">
                <div style="margin-top: 40px;">
                    <a href="<?= Url::to(['/site/signup']); ?>" class="btn-green" style="display: inline-block; width: 250px;">
                        <?= Yii::t('cabinet', 'REGISTRATION'); ?>
                    </a>
                </div>
                <div style="margin-top: 20px;">
                    <a href="<?= Url::to(['/site/login']); ?>" class="btn-green" style="display: inline-block; background: #404f47; width: 250px;">
                        <?= Yii::t('cabinet', 'LOGIN'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>