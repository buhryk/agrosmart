<?php

namespace frontend\modules\ordertransport\models;

use common\helpers\GoogleMapsApiHelper;
use frontend\models\User;
use Yii;

/**
 * This is the model class for table "ordertransport".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $from_location
 * @property string $to_location
 * @property integer $transport_type
 * @property string $message
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 */
class Order extends \yii\db\ActiveRecord
{
    const TRANSPORT_TYPE_UP_TO_1_5_TONS = 1;
    const TRANSPORT_TYPE_UP_TO_5_TONS = 2;
    const TRANSPORT_TYPE_UP_TO_10_TONS = 3;
    const TRANSPORT_TYPE_UP_TO_21_TONS = 4;

    const STATUS_NEW = 0;
    const STATUS_VIEWED = 1;
    const STATUS_PROCESSED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ordertransport';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'from_location', 'to_location', 'transport_type'], 'required'],
            [['user_id', 'transport_type', 'created_at', 'updated_at', 'status'], 'integer'],
            ['transport_type', 'in', 'range' => [
                self::TRANSPORT_TYPE_UP_TO_1_5_TONS,
                self::TRANSPORT_TYPE_UP_TO_5_TONS,
                self::TRANSPORT_TYPE_UP_TO_10_TONS,
                self::TRANSPORT_TYPE_UP_TO_21_TONS
            ]],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_VIEWED, self::STATUS_PROCESSED]],
            [['message'], 'string'],
            [['from_location', 'to_location', 'message'], 'string', 'max' => 255],
            ['message', 'string', 'max' => 2000],
            [['from_location', 'to_location'], 'validateLocation'],
            ['status', 'default', 'value' => self::STATUS_NEW]
        ];
    }

    public function validateLocation($attribute, $params)
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();

        if ($this->$attribute) {
            $coordinates = $googleMapsApiHelper->getLocationByAddress($this->$attribute);
            if (!$coordinates) {
                $this->addError($attribute, Yii::t('instruments', 'Wrong location', ['field' => $this->getAttributeLabel($attribute)]));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'from_location' => Yii::t('instruments', 'Отправление из'),
            'to_location' => Yii::t('instruments', 'Прибытие в'),
            'transport_type' => Yii::t('instruments', 'Tonnage and volume'),
            'message' => Yii::t('common', 'Message'),
            'created_at' => Yii::t('common', 'Date created at'),
            'updated_at' => Yii::t('common', 'Date last updated at'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    public static function getTransportTypesList()
    {
        return [
            self::TRANSPORT_TYPE_UP_TO_1_5_TONS => Yii::t('instruments', 'TRANSPORT_TYPE_UP_TO_1_5_TONS'),
            self::TRANSPORT_TYPE_UP_TO_5_TONS => Yii::t('instruments', 'TRANSPORT_TYPE_UP_TO_5_TONS'),
            self::TRANSPORT_TYPE_UP_TO_10_TONS => Yii::t('instruments', 'TRANSPORT_TYPE_UP_TO_10_TONS'),
            self::TRANSPORT_TYPE_UP_TO_21_TONS => Yii::t('instruments', 'TRANSPORT_TYPE_UP_TO_21_TONS')
        ];
    }

    public function getDetailTransportType()
    {
        foreach (self::getTransportTypesList() as $key => $title) {
            if ($this->transport_type == $key) {
                return $title;
            }
        }

        return 'Undefined';
    }

    public static function getAllStatuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'COMPLAINT_STATUS_NEW'),
            self::STATUS_VIEWED => Yii::t('common', 'COMPLAINT_STATUS_VIEWED'),
            self::STATUS_PROCESSED => Yii::t('common', 'COMPLAINT_STATUS_PROCESSED')
        ];
    }

    public function getDetailStatus()
    {
        foreach (self::getAllStatuses() as $key => $title) {
            if ($this->status == $key) {
                return $title;
            }
        }

        return 'Undefined';
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}