<?php

namespace frontend\modules\saveddata\controllers;

use frontend\modules\saveddata\models\SavedAddress;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

class SavedAddressController extends Controller
{
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            throw new AccessDeniedException('Access denied');
        }

        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax) {
            $request = Yii::$app->request->post();

            if (empty($request['address'])) {
                throw new BadRequestHttpException('Not all parameters provided');
            }

            $address = $request['address'];
            $userId = Yii::$app->user->identity->id;

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($this->isAddressIsSavedForUser($userId, $address)) {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'message' => Yii::t('common', 'Address is already saved')
                ];
            }

            if ($this->createSavedAddress($userId, $address)) {
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Address added to saved')
                ];
            } else {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'message' => Yii::t('common', 'Something went wrong. Please, try again after reloading page')
                ];
            }
        }
    }

    public function actionDelete()
    {
        if (Yii::$app->user->isGuest) {
            throw new AccessDeniedException('Access denied');
        }

        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax) {
            $request = Yii::$app->request->post();

            if (empty($request['address'])) {
                throw new BadRequestHttpException('Not all parameters provided');
            }

            $address = $request['address'];
            $userId = Yii::$app->user->identity->id;

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($this->isAddressIsSavedForUser($userId, $address)) {
                SavedAddress::deleteAll([
                    'user_id' => $userId,
                    'address' => $address
                ]);

                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Record was removed from elected')
                ];
            } else {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'message' => Yii::t('common', 'Something went wrong. Please, try again after reloading page')
                ];
            }
        }
    }

    private function isAddressIsSavedForUser($userId, $address)
    {
        $record = SavedAddress::findOne([
            'user_id' => $userId,
            'address' => $address
        ]);

        return $record === null ? false : true;
    }

    private function createSavedAddress($userId, $address)
    {
        $model = new SavedAddress();
        $model->user_id = $userId;
        $model->address = $address;

        return $model->save() ? true : false;
    }
}