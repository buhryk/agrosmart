<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 28.02.2017
 * Time: 17:11
 */
use frontend\modules\cabinet\models\Sale;

?>

<?php

?>

<div class="container">
    <div class="row">
        <h1><?=Yii::t('common', 'Обновление цен для')?> <?= $company->name?></h1>
            <?php if ($data): ?>
                <div class="col-md-8">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td style="white-space: pre-wrap;">
                                    <strong>
                                        <?=Yii::t('common', 'Компания') ?>
                                    </strong>
                                </td>
                                <td style="white-space: pre-wrap;">
                                    <strong>
                                        <?=Yii::t('common', 'Адрес') ?>
                                    </strong>
                                </td>
                                <td>
                                    <strong>
                                        <?=Yii::t('common', 'Культура') ?>
                                    </strong>
                                </td>
                                <td>
                                    <strong>
                                        <?=Yii::t('common', 'Тип') ?>
                                    </strong>
                                </td>
                                <td>
                                    <strong>
                                        <?=Yii::t('common', 'Цена') ?>
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                        <?php foreach ($data as $item) :
                            $type = $item->type == Sale::TYPE_PRODAZHA ? 'prodazha' : 'zakupka';
                            $confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['/company/price/'.$type.'-info', 'company_id' => $item->company_id ]);
                        ?>
                        <tr>
                            <td style="white-space: pre-wrap;"><?= $item->company->name ?></td>
                            <td style="white-space: pre-wrap;"><?= $item->location ?></td>
                            <td><?= $item->rubric->title ?></td>
                            <td><?= \frontend\modules\cabinet\models\Sale::OffersTypes()[$item->type] ?></td>
                            <td><?= $item->price ?></td>
                            <td><a href="<?=$confirmLink ?>"> <?=Yii::t('common', 'Просмотреть') ?> </a> </td>
                        </tr>
                        <? endforeach;?>
                    </table>
                </div>
            <?php else: ?>
                <strong>
                    <?=Yii::t('common', 'На данный момент предложений не найдено. Попробуйте позже обновить запрос.') ?>
                </strong>
            <?php endif; ?>
    </div>
</div>