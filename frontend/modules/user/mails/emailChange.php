<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user frontend\models\User */
//$user->email_confirm_token
$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/email-change-confirm', 'token' => $user->email_confirm_token]);


?>

<?= Yii::t('users','USER EMAIL TEMPLATE',['username'=>Html::encode($user->username),'new_email'=>Html::encode($user->new_email),'link' => $confirmLink])?>

