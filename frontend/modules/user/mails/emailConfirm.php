<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user frontend\models\User */
//$user->email_confirm_token
$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/email-confirm', 'token' => $user->email_confirm_token]);
?>

    Здравствуйте, <?= Html::encode($user->username) ?>!

    Для подтверждения адреса пройдите по ссылке:
    <a href="<?= $confirmLink ?>"> <?= $confirmLink ?> </a>
