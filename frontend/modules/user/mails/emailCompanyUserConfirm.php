<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user frontend\models\User */
//$user->email_confirm_token
$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/company-user-finish-auth', 'token' => $user->email_confirm_token]);
?>

Здравствуйте, <?= Html::encode($user->username) ?>!

Для подтверждения адреса пройдите по ссылке и закончите регистрацию:
<a href="<?= $confirmLink ?>"> <?= $confirmLink ?> </a>
