<?php

namespace frontend\modules\user\controllers;

use backend\modules\event\models\Event;
use backend\modules\news\models\NewsCategory;
use backend\modules\news\models\NewsUserAccessibilityType;
use backend\modules\rubric\models\Rubric;
use frontend\models\User;
use frontend\modules\news\models\CategoryNewsSearch;
use frontend\modules\user\models\UserAdvertisementSearch;
use Yii;
use backend\modules\news\models\News;
use frontend\modules\news\models\NewsSearch;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * NewsController implements the CRUD actions for News model.
 */
class UserController extends Controller
{
    public function actionView($id)
    {
        $model = User::find()
            ->where(['id' => $id, 'status' => User::STATUS_ACTIVE])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('view', [
            'model' => $model,
            'advertisements' => $model->getAdvertisements(3)
        ]);
    }

    public function actionProducts($id)
    {
        $model = User::find()
            ->where(['id' => $id, 'status' => User::STATUS_ACTIVE])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $searchModel = new UserAdvertisementSearch(['user' => $model]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $choosenRubric = $searchModel->rubric_id ?
            Rubric::find()->where(['id' => $searchModel->rubric_id, 'active' => Rubric::ACTIVE_YES])->one() : null;

        return $this->render('products', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'choosenRubric' => $choosenRubric
        ]);
    }


}