<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\ArrayHelper;
use frontend\modules\complaint\models\Complaint;
use yii\helpers\Html;

$pageH1 = $model->fullName;
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => '/user/user/view/'.$model->id]);

$this->params['breadcrumbs'][] = $pageH1;

$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$electedCompaniesIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedCompanies, 'id') : [];
$electedUsersIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedUsers, 'id') : [];
$electedAdvertisementsIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedAdvertisements, 'id') : [];
$savedAddresses = $currentUser ? ArrayHelper::getColumn($currentUser->savedAddresses, 'address') : [];
$messageAddToElected = Yii::t('common', 'Add to elected');
$messageRemoveFromElected = Yii::t('common', 'Remove from elected');
$company = $model->company;
?>
<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9 top-marclear">
            <div class="personal-info row">
                <div class="col-sm-5">
                    <?php echo Yii::$app->thumbnail->img('/images/feedback-user.jpg',
                        ['thumbnail' => ['width' => 345, 'height' => 259]],
                        ['class' => 'personal-info__photo']
                    ); ?>
                </div>
                <div class="col-sm-7">
                    <p class="personal-info__name"><?= $pageH1; ?></p>
                    <p class="personal-info__contact">Телефон: <span><?= $model->phone; ?></span></p>
                    <p class="personal-info__contact">Email: <span><?= $model->email; ?></span></p>
                    <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->id != $model->id) {
                        echo Html::a(Yii::t('common', 'Write'),
                            ['/cabinet/dialog/create-message-modal', 'receiver' => $model->id],
                            ['class' => 'btn-green modalButton']);
                    } ?>

                    <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->id != $model->id) { ?>
                        <?php $checkComplaintExist = Complaint::find()->where([
                            'from_user_id' => Yii::$app->user->identity->id,
                            'table_name' => Complaint::ALIAS_USER,
                            'record_id' => $model->id,
                            'status' => Complaint::STATUS_NEW
                        ])->one(); ?>
                        <?php if (!$checkComplaintExist) { ?>
                            <div>
                                <?= Html::a(Yii::t('common', 'Complaint'),
                                    ['/complaint/complaint/create',
                                        'type' => Complaint::ALIAS_USER,
                                        'record_id' => $model->id,
                                    ],
                                    ['class' => 'personal-info__complain complain-button mt45 modalButton']);
                                ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <?php if ($advertisements) { ?>
                <div class="another-product companies-products" style="padding-bottom: 30px;">
                    <div class="company-infoblock pt50 pb25" id="product-block">
                        <span class="fs19 fw600 ttu db mb10"><?= Yii::t('advertisement', 'Products and services'); ?></span>
                    </div>
                    <?php foreach ($advertisements as $advertisement) { ?>
                        <?= $this->render('@frontend/modules/product/views/product/_advertisement', [
                            'model' => $advertisement,
                            'company' => $company,
                            'electedCompaniesIds' => $electedCompaniesIds,
                            'electedUsersIds' => $electedUsersIds,
                            'messageAddToElected' => $messageAddToElected,
                            'messageRemoveFromElected' => $messageRemoveFromElected,
                            'savedAddresses' => $savedAddresses,
                            'electedAdvertisementsIds' => $electedAdvertisementsIds
                        ]); ?>
                    <?php } ?>

                    <div style="text-align: center">
                        <a href="<?= \yii\helpers\Url::to(['products', 'id' => $model->primaryKey]); ?>" class="btn-green">
                            <?= Yii::t('users', 'All user products and services'); ?>
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>

</div>