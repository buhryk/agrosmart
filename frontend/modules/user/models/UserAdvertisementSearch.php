<?php

namespace frontend\modules\user\models;

use backend\modules\commondata\models\City;
use backend\modules\commondata\models\Currency;
use yii\data\ActiveDataProvider;
use frontend\modules\product\models\Advertisement;
use frontend\modules\product\models\AdvertisementSearch;

class UserAdvertisementSearch extends AdvertisementSearch
{

    public $user;

    public function __construct(array $config)
    {
        $this->user = isset($config['user']) ? $config['user'] : null;
        parent::__construct($config);
    }

    public function behaviors()
    {
        return [];
    }

    public function search($params)
    {
        $query = Advertisement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->select([Advertisement::tableName().'.*']);

        $query->joinWith('city')
            ->joinWith('currency')
            ->joinWith('author');

        $query->andFilterWhere([
            'rubric_id' => $this->rubric_id,
            'city_id' => $this->city_id,
            'author_id' => $this->user->id,
            Advertisement::tableName().'.type' => $this->type,
        ]);

        $query->andWhere('company_id is null');

        if (($this->price_from || $this->price_to) && $this->currency_id ) {
            $this->addFilterByPrice($query, $this->price_from, $this->price_to, $this->currency_id);
        }

        if (($this->volume_from || $this->volume_to) && $this->measurement_id ) {
            $this->addFilterByVolume($query, $this->volume_from, $this->volume_to, $this->measurement_id);
        }

        $query->andFilterWhere([Advertisement::tableName().'.status' => Advertisement::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()]);

        if ($this->region_id) {
            $query->andFilterWhere([City::tableName().'.oblast_id' => $this->region_id]);
        }

        if ($this->sort) {
            if ($this->sort == self::SORT_CREATED_AT_DESC) {
                $query->orderBy(['created_at' => SORT_DESC]);
            } elseif ($this->sort == self::SORT_CREATED_AT_ASC) {
                $query->orderBy(['created_at' => SORT_ASC]);
            } elseif (in_array($this->sort, [self::SORT_PRICE_DESC, self::SORT_PRICE_ASC])) {
                $query->addSelect(['(price/'.Currency::tableName().'.weight) AS universalPrice']);
                if ($this->sort == self::SORT_PRICE_DESC) {
                    $query->orderBy('universalPrice DESC');
                } else {
                    $query->orderBy('universalPrice ASC');
                }
            }
        }

        return $dataProvider;
    }
}