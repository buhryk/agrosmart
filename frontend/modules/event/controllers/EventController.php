<?php

namespace frontend\modules\event\controllers;

use backend\modules\commondata\models\Country;
use backend\modules\event\models\EventLang;
use backend\modules\seo\models\Seo;
use Yii;
use backend\modules\event\models\Event;
use frontend\modules\event\models\EventSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class EventController extends Controller
{
    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug)
    {
        $model = $this->findModelByAlias($slug);
        $location = $this->buildMarker($model);
        $subjectEvents = Event::find()
            ->joinWith('lang')
            ->where(['active' => Event::ACTIVE_YES, 'subject_id' => $model->subject_id])
            ->andWhere(['<>', Event::tableName().'.id', $model->primaryKey])
            ->orderBy('start_date DESC')
            ->groupBy(EventLang::tableName().'.event_id')
            ->limit(5)
            ->all();

        $seo = ($seoModel = $model->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);

        return $this->render('view', [
            'model' => $model,
            'locations' => $location,
            'subjectEvents' => $subjectEvents
        ]);
    }

    public function actionCalendar()
    {
        $events = Event::find()
            ->where(['active' => Event::ACTIVE_YES])
            ->joinWith('lang')
            ->all();

        $data = [
            'header' => [
                'left' => 'prev,next today',
				'center' => 'title',
				'right' => 'month,basicWeek,basicDay'
            ],
            'locale' => Yii::$app->language == 'uk-UK' ? 'uk' : 'ru',
            'defaultDate' => date('Y-m-d'),
            'events' => []
        ];

        foreach ($events as $event) {
            $oneEventData = [
                'id' => $event->primaryKey,
                'title' => $event->title,
                'url' => Url::to(['/event/event/view', 'slug' => $event->alias]),
                'start' => $event->start_date
            ];

            if ($event->end_date) {
                $oneEventData['end'] = date('Y-m-d', strtotime($event->end_date . ' +1 day'));
            }

            $data['events'][] = $oneEventData;
        }

        return $this->render('calendar', [
            'data' => json_encode($data)
        ]);
    }

    public function actionData()
    {
        $models = Event::find()
            ->andWhere(['country_id' => 2])
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $data = [];

        foreach ($models as $model) {
            $data[] = [
                'title' => $model->title,
                'description' => $model->short_description,
                'text' => $model->text,
                'type' => $model->type->title,
                'start_date' => $model->start_date,
                'end_date' => $model->end_date,
                'address' => $model->address,
                'url' => $model->alias
            ];
        }

        return json_encode($data);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $alias
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelByAlias($alias)
    {
        $model = Event::find()
            ->where(['alias' => $alias, 'active' => Event::ACTIVE_YES])
            ->joinWith('lang')
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    private function buildMarker(Event $event)
    {
        if ($event->address_confirmed != Event::ADDRESS_CONFIRMED_YES || !$event->address) {
            return '';
        }

        $locations = "[";
        $coordinates = $this->getLocationByAddress($event->address);
        $content = "<div class=\"marker\">";
        $content .= "<div class=\"marker-title\">$event->address</div>";
        $content .= "<div>".$event->title."</div>";
        $content .= '</div>';
        $locations .= "['".$content."',".$coordinates['lat'].",".$coordinates['lng']."]]";

        return $locations;
    }

    private function getLocationByAddress($address)
    {
        $cityclean = str_replace (" ", "+", $address);
        $details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $cityclean . "&sensor=false";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geoloc = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if ($geoloc['status'] == 'OK' && isset($geoloc['results'][0]['geometry']['location'])) {
            return $geoloc['results'][0]['geometry']['location'];
        }

        return null;
    }
}