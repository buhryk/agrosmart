<?php

namespace frontend\modules\event\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\event\models\Event;

/**
 * EventSearch represents the model behind the search form about `backend\modules\event\models\Event`.
 */
class EventSearch extends Event
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'type_id', 'country_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find()->joinWith('lang');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'subject_id' => $this->subject_id,
            'type_id' => $this->type_id,
            'country_id' => $this->country_id,
            'active' => Event::ACTIVE_YES,
        ]);

        $query->orderBy('start_date DESC');

        return $dataProvider;
    }
}