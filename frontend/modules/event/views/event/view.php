<?php
use yii\widgets\Breadcrumbs;
use frontend\assets\EventPageAsset;
use yii\helpers\Html;
use frontend\components\tool\Image;
use yii\helpers\Url;

EventPageAsset::register($this);

if (!$this->title) {
    $this->title = $model->title. ' | '.Yii::$app->params['titlePrefix'];
    $this->registerMetaTag(['name'=>'description', 'content'=> $model->title.' | '.Yii::$app->params['descriptionPrefix']]);
}

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['view', 'slug' => $model->alias])]);
?>

<div class="bread">
    <h1><?= $model->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container event">
    <div class="row">
        <div class="col-sm-9 logistic-calc" data-content="page_content">
            <div class="event-photo">
                <?php $mainImage = $model->image; ?>
                <?php if ($mainImage) { ?>
                    <?php echo Yii::$app->thumbnail->img($mainImage->path, [ 'thumbnail' =>  ['width' => 328, 'height' => 246] ]) ;
//                    Html::img(Image::resize($mainImage->path, 328, 246),
//                        ['alt' => $mainImage->alt ? $mainImage->alt : $model->title]
//                    );
                    ?>
                <?php } else { ?>
                    <?php echo Yii::$app->thumbnail->placeholder(['width' => 328, 'height' => 246, 'text' => '328x246'],
                        ['alt' => $model->title]
                    ); ?>
                <?php } ?>
            </div>
            <div class="event-info">
                <p class="event-info__title"><?= Yii::t('event', 'Time spending'); ?></p>
                <p class="event-info__main-info">
                    <?= $model->formattedDateRange; ?> (<?= $model->formattedDayRange; ?>)
                </p>
                <?php
                $startDate = new DateTime($model->start_date);
                $endDate = new DateTime(date('Y-m-d', time()));
                $diff = $endDate->diff($startDate)->format("%a");
                ?>

                <?php if ($diff && $endDate->format('Y-m-d') < $startDate->format('Y-m-d') ) { ?>
                    <p class="event-info__title"><?= Yii::t('event', 'Days before event'); ?>:
                        <span style="font-size: 19px;color: #000; text-transform: uppercase; font-weight: normal">
                            <?= $diff; ?>
                        </span>
                    </p>
                <?php } ?>
                <div class="event-info-about"><?= $model->text; ?></div>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</div>

<?php if ($locations) { ?>
    <div class="container-fluid grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="event-map">
                        <h3 class="event-map__title"><?= Yii::t('event', 'Event on the map'); ?></h3>
                    </div>
                    <div id="events_map" style="height: 400px; width: 100%"></div>
                    <?php $this->registerJs("var locations = ".$locations.";",
                        \yii\web\View::POS_HEAD,
                        'my-button-handler'
                    ); ?>
                </div>
                <div class="col-sm-3 sidebar-banner">
                    <?= \frontend\modules\news\widgets\LastNewsWidget::widget([]); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($subjectEvents) { ?>
    <div class="container another-events">
        <h3 class="another-events__title"><?= Yii::t('event', 'Thematic events'); ?></h3>
        <?php foreach ($subjectEvents as $one) { ?>
            <div class="another-events-item">
                <a href="<?= \yii\helpers\Url::to(['view', 'slug' => $one->alias]); ?>">
                    <p class="another-events-item__date"><?= date('d.m.Y', strtotime($one->start_date)); ?></p>
                    <?php $mainImage = $one->image; ?>
                    <?php if ($mainImage) { ?>
                        <?php echo Yii::$app->thumbnail->img($mainImage->path,
                            ['thumbnail' => ['width' => 140, 'height' => 85]],
                            ['class' => 'another-events-item__photo', 'alt' => $mainImage->alt ? $mainImage->alt : $one->title]
                        ); ?>
                    <?php } else { ?>
                        <?php echo Yii::$app->thumbnail->placeholder(['width' => 140, 'height' => 85, 'text' => '145x174'],
                            ['class' => 'another-events-item__photo', ['alt' => $one->title]]
                        ); ?>
                    <?php } ?>
                    <p class="another-events-item__name"><?= $one->title; ?></p>
                </a>
            </div>
        <?php } ?>
    </div>
<?php } ?>