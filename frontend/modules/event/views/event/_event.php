<?php
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\components\tool\Image;
?>

<div class="row one-event-block">
    <a href="<?= Url::to(['view', 'slug' => $model->alias]); ?>" class="logistic-calc-item clearfix">
        <div class="col-sm-3 col-xs-12">
            <?php $mainImage = $model->image; ?>
            <?php if ($mainImage) { ?>
                <?php
                echo Yii::$app->thumbnail->img($mainImage->path, [ 'thumbnail' =>  ['width' => 255, 'height' => 161] ]);
//                echo Html::img(Image::resize('/'.$mainImage->path, 255, 161),
//                    ['alt' => $mainImage->alt ? $mainImage->alt : $model->title]
//                );
                ?>
            <?php } else { ?>
                <?php echo Yii::$app->thumbnail->placeholder(['width' => 255, 'height' => 161, 'text' => '195x151'],
                    ['alt' => $model->title]
                ); ?>
            <?php } ?>
        </div>
        <div class="col-sm-3 col-xs-12">
            <p class="logistic-calc-item__text ttl"><?= $model->title; ?></p>
        </div>
        <div class="col-sm-3 col-xs-6">
            <p class="logistic-calc-item__text"><?= $model->formattedDateRange; ?></p>
        </div>
        <div class="col-sm-3 col-xs-6">
            <p class="logistic-calc-item__text"><?= $model->address; ?></p>
        </div>
    </a>
</div>