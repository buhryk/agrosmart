<?php
use frontend\assets\EventsCalendarAsset;
use yii\widgets\Breadcrumbs;
EventsCalendarAsset::register($this);

$pageH1 = Yii::t('event', 'Events calendar');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);


$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $pageH1;
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9 mt45">
            <div id="calendar"></div>
            <?php $this->registerJs("var events = ".$data.";",
                \yii\web\View::POS_HEAD,
                'my-button-handler'
            ); ?>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
            <?= \frontend\modules\news\widgets\LastNewsWidget::widget(['options' => ['count' => 3]]); ?>
        </div>
    </div>
</div>