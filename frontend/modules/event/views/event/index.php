<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\event\models\EventType;
use backend\modules\event\models\EventSubject;
use backend\modules\commondata\models\Country;
use backend\modules\event\models\Event;

$pageH1 = Yii::t('common', 'Events');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['index'])]);
$this->params['breadcrumbs'][] = $pageH1;
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9 logistic-calc events events-container">
            <div class="row p0 mb20">
                <div class="col-xs-12 col-md-8">
                    <div class="event-form">
                        <?php $form = ActiveForm::begin([
                            'method' => 'GET',
                            'action' => ['index'],
                            'options' => ['id' => 'events-filter-form']
                        ]); ?>
                        <div style="display: inline-block">
                            <?= $form->field($searchModel, 'subject_id')
                                ->dropDownList(ArrayHelper::map(EventSubject::find()->joinWith('lang')->all(), 'id', 'title'), [
                                    'prompt' => 'Тематика',
                                    'class' => 'one-event-dropdown-filter form-control'
                                ])->label(false)
                            ?>
                        </div>
                        <div style="display: inline-block">
                            <?= $form->field($searchModel, 'type_id')
                                ->dropDownList(ArrayHelper::map(EventType::find()->joinWith('lang')->all(), 'id', 'title'), [
                                    'prompt' => 'Тип',
                                    'class' => 'one-event-dropdown-filter form-control'
                                ])->label(false)
                            ?>
                        </div>
                        <div style="display: inline-block">
                            <?= $form->field($searchModel, 'country_id')
                                ->dropDownList(ArrayHelper::map(
                                    Country::find()
                                        ->where(['in', Country::tableName().'.id', ArrayHelper::getColumn(Event::find()->select('country_id')->distinct()->all(), 'country_id')])
                                        ->joinWith('lang')
                                        ->all(),
                                    'id', 'title'),
                                    ['prompt' => Yii::t('common', 'Страна'), 'class' => 'one-event-dropdown-filter form-control'])
                                ->label(false)
                            ?>
                        </div>

                        <?php $this->registerJs("
                            $('.one-event-dropdown-filter').on('change', function(){
                                $('form#events-filter-form').submit();
                            });
                        ",
                            \yii\web\View::POS_READY,
                            'my-button-handler'
                        ); ?>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <a href="<?= Url::to(['calendar']); ?>" class="fr">
                        <?= Yii::t('event', 'Events calendar'); ?> <img class="ml10" src="/images/calendar.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-sm-12">
                <?php echo \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_event',
                    'pager' => [
                        'registerLinkTags' => true,
                    ],
                    'itemOptions' => ['class' => 'row']
                ]); ?>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
            <?= \frontend\modules\news\widgets\LastNewsWidget::widget([]); ?>
        </div>
    </div>
</div>