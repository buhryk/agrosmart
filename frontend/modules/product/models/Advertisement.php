<?php

namespace frontend\modules\product\models;

use backend\modules\commondata\models\City;
use common\helpers\SitemapHelper;
use frontend\modules\cabinet\models\PullTop;
use frontend\modules\cabinet\models\Subsidiary;
use frontend\modules\cabinet\models\Top;
use Yii;
use frontend\models\User;
use frontend\models\Company;
use backend\modules\rubric\models\Rubric;
use backend\modules\commondata\models\Currency;
use backend\modules\rubric\models\RubricMeasurement;
use backend\modules\transliterator\services\TransliteratorService;
use backend\modules\tariff\models\Tariff;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use common\helpers\ReplaceLinkHelper;

class Advertisement extends \yii\db\ActiveRecord
{
    const TYPE_SALE = 1;
    const TYPE_PURCHASE = 2;
    const TYPE_SERVICE = 3;
    const STATUS_ACTIVE_YES = 1;
    const STATUS_ACTIVE_NO = 0;
    const STATUS_DEACTIVATED_BY_ADMIN = 2;
    const FROM_PRIVATE_PERSON = 1;
    const FROM_COMPANY = 2;
    const ADMIN_STATUS_NEW = 0;
    const ADMIN_STATUS_VIEWED = 1;

    public static function tableName()
    {
        return 'advertisement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'rubric_id', 'author_id', 'currency_id', 'phones', 'city_id'], 'required'],
            [['text', 'email'], 'string'],
            [['rubric_id', 'measurement_id', 'from_user_id', 'from_subsidiary_id', 'author_id', 'company_id', 'created_at', 'updated_at',
                'status', 'active_to', 'type', 'currency_id', 'views_count', 'city_id', 'admin_status'], 'integer'],
            [['price', 'volume'], 'number', 'min' => '0'],
            [['title', 'alias', 'phones', 'address', 'email'], 'string', 'max' => 255],
            ['alias', 'unique'],
            ['email', 'email'],
            ['phones', 'validatePhones'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE_YES],
            ['views_count', 'default', 'value' => 0],
            ['admin_status', 'in', 'range' => [self::ADMIN_STATUS_NEW, self::ADMIN_STATUS_VIEWED]],
            ['admin_status', 'default', 'value' => self::ADMIN_STATUS_NEW]
        ];
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'common\behaviors\Slug',
                'in_attribute' => 'title',
                'out_attribute' => 'alias',
                'translit' => true
            ],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('common', 'Title'),
            'alias' => 'Alias',
            'text' => 'Text',
            'rubric_id' => 'Rubric ID',
            'price' => 'Price',
            'volume' => 'Volume',
            'measurement_id' => 'Measurement ID',
            'from_user_id' => 'From user ID',
            'from_subsidiary_id' => 'From subsidiary id',
            'author_id' => 'Author ID',
            'company_id' => 'Company ID',
            'phones' => 'Phones',
            'created_at' => Yii::t('common', 'Date created at'),
            'updated_at' => 'Updated At',
            'status' => Yii::t('common', 'Status'),
            'active_to' => 'Active To',
            'type' => Yii::t('common', 'Type'),
            'city_id' => 'City',
            'address' => 'Address',
            'email' => 'Email',
            'admin_status' => 'Статус админа'
        ];
    }

    public function getTitle()
    {
        return Html::encode($this->title);
    }

    public function getText()
    {
        return ReplaceLinkHelper::replace(HtmlPurifier::process( $this->text ));
    }

    public function validatePhones($attribute, $params)
    {
        if ($this->phones) {

            $phones = explode(',', $this->phones);
            foreach ($phones as $key => $phone) {
                $phones[$key] = trim($phone);
            }

            foreach ($phones as $key => $phone) {
                if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/", $phone)) {
                    $this->addError('phones', Yii::t('advertisement', 'Phone numbers must be written in the format', ['format' => '0XX-XXX-XX-XX']));
                }
            }
        }
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id'])->joinWith('lang');
    }

    public function getLocation()
    {
        $city = $this->city;
        $area = (($area = $city->area) !== null)  ? $area : null;
        $region = $city->region;
        $address = $this->address ? ', '.$this->address : '';

        return $city->title . ', ' . ($area ? ($area->title . ', ') : '') . $region->title . $address;
    }

    public function getRubric()
    {
        return $this->hasOne(Rubric::className(), ['id' => 'rubric_id']);
    }

    public function getMeasurement()
    {
        return $this->hasOne(RubricMeasurement::className(), ['id' => 'measurement_id'])
            ->joinWith('category')
            ->joinWith('lang');
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            if (!$this->email && $this->from_user_id) {
                $user = User::findOne($this->from_user_id);
                if ($user) {
                    $this->email = $user->email;
                }
            } elseif (!$this->email && $this->from_subsidiary_id) {
                $subsidiary = Subsidiary::findOne($this->from_subsidiary_id);
                if ($subsidiary) {
                    $this->email = $subsidiary->email;
                }
            } else {
                $user = User::findOne($this->author_id);
                if ($user) {
                    $this->email = $user->email;
                }
            }

            $this->created_at = time();
            $this->updated_at = $this->created_at;
        } else {
            $this->updated_at = time();
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function getImages()
    {
        return $this->hasMany(AdvertisementImage::className(), ['advertisement_id' => 'id'])
            ->orderBy('is_main DESC')
            ->addOrderBy('sort DESC');
    }

    public function saveImages($images = [])
    {
        if ($this->isNewRecord) {
            return false;
        }

        foreach ($images as $image) {
            $model = new AdvertisementImage();
            $model->advertisement_id = $this->primaryKey;
            $model->is_main = $image['is_main'] ? AdvertisementImage::IS_MAIN_YES : AdvertisementImage::IS_MAIN_NO;
            $model->path = $image['path'];
            $model->save();
        }
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getDetailPrice()
    {
        if ($this->price && $this->currency_id && ($currency = $this->currency) !== null) {
            return $this->price . ' ' . $currency->code;
        }

        return Yii::t('advertisement', 'contract price');
    }

    public function getDetailVolume()
    {
        $detailVolume = '';
        if ($this->volume && $this->measurement_id && ($measurement = $this->measurement) !== null) {
            $detailVolume = $this->volume . ' ' . $measurement->short_title;
        }

        return $detailVolume;
    }

    public function getOtherUserAdvertisements($count = false)
    {
        return $this->hasMany(self::className(), ['author_id' => 'author_id'])
            ->where(['<>', self::tableName().'.id', $this->primaryKey])
            ->limit($count)
            ->joinWith('author')
            ->all();
    }

    public function getOtherUserActiveAdvertisements($count = false)
    {
        return $this->hasMany(self::className(), ['author_id' => 'author_id'])
            ->where(['<>', self::tableName().'.id', $this->primaryKey])
            ->andWhere(['status' => self::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
            ->limit($count)
            ->orderBy('created_at DESC')
            ->all();
    }

    public function getOtherCompanyAdvertisements($count = false)
    {
        return $this->hasMany(self::className(), ['company_id' => 'company_id'])
            ->where(['<>', self::tableName().'.id', $this->primaryKey])
            ->limit($count)
            ->joinWith('company')
            ->all();
    }

    public function getOtherCompanyActiveAdvertisements($count = false)
    {
        return $this->hasMany(self::className(), ['company_id' => 'company_id'])
            ->where(['<>', self::tableName().'.id', $this->primaryKey])
            ->andWhere(['status' => self::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
            ->limit($count)
            ->orderBy('created_at DESC')
            ->all();
    }

    public function getSimilarAdvertisements($count = false)
    {
        return $this->hasMany(self::className(), ['rubric_id' => 'rubric_id'])
            ->where(['<>', self::tableName().'.id', $this->primaryKey])
            ->limit($count)
            ->joinWith('author')
            ->all();
    }

    public function getSimilarActiveAdvertisements($count = false)
    {
        return $this->hasMany(self::className(), ['rubric_id' => 'rubric_id'])
            ->where(['<>', self::tableName().'.id', $this->primaryKey])
            ->andWhere([self::tableName().'.status' => self::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
            ->limit($count)
            ->joinWith('author')
            ->all();
    }

    public static function getAllStatuses()
    {
        return [
            self::STATUS_ACTIVE_NO => Yii::t('advertisement', 'STATUS_ACTIVE_NO'),
            self::STATUS_ACTIVE_YES => Yii::t('advertisement', 'STATUS_ACTIVE_YES'),
            self::STATUS_DEACTIVATED_BY_ADMIN => Yii::t('advertisement', 'STATUS_DEACTIVATED_BY_ADMIN')
        ];
    }

    public function getDetailStatus()
    {
        foreach (self::getAllStatuses() as $key => $title) {
            if ($this->status == $key) {
                return $title;
            }
        }

        return 'Undefined';
    }

    public static function getTypes()
    {
        return [
            self::TYPE_SALE => Yii::t('advertisement', 'TYPE_SALE'),
            self::TYPE_PURCHASE => Yii::t('advertisement', 'TYPE_PURCHASE'),
            self::TYPE_SERVICE => Yii::t('advertisement', 'TYPE_SERVICE'),
        ];
    }

    public function getDetailType()
    {
        foreach (self::getTypes() as $key => $title) {
            if ($this->type == $key) {
                return $title;
            }
        }

        return 'Undefined';
    }

    public function getAllotted()
    {
        return $this->hasOne(Top::className(), ['model_id' => 'id'])
            ->andWhere(['model_name' => Tariff::MODEL_PRODUCT])
            ->andWhere(['in','type' , [Tariff::TYPE_ALLOTTED, Tariff::TYPE_COMPLEX]])
            ->andWhere(['<=', 'start', time()])
            ->andWhere(['>=', 'finish', time()]);
    }

    public static function getAllAdminStatuses()
    {
        return [
            self::ADMIN_STATUS_NEW => 'Новое',
            self::ADMIN_STATUS_VIEWED => 'Просмотренное'
        ];
    }

    public function getAdminStatusDetail()
    {
        return isset(self::getAllAdminStatuses()[$this->admin_status]) ? self::getAllAdminStatuses()[$this->admin_status] : 'Undefined';
    }
}