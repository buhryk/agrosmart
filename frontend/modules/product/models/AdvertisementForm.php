<?php

namespace frontend\modules\product\models;

use backend\modules\rubric\models\Rubric;
use frontend\models\Company;
use frontend\models\User;
use yii\behaviors\SluggableBehavior;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class AdvertisementForm extends Model
{
    public $rubric_id;
    public $type;
    public $title;
    public $text;
    public $active_to;
    public $without_active_to;
    public $currency;
    public $price;
    public $contract_price;
    public $volume;
    public $measurement_id;
    public $is_agree;
    public $alias;
    public $author_id;
    public $from_user_id;
    public $from_subsidiary_id;
    public $company_id;
    public $status;
    public $phones;
    public $city_id;
    public $address;
    public $image1, $image2, $image3, $image4, $image5, $image6;
    public $publish_from;

    const PUBLISH_FROM_OTHER_USER = 'other';
    const PUBLISH_FROM_SUBSIDIARY = 'subsidiary';
    const PUBLISH_FROM_AUTHOR = 'author';

    public function rules()
    {
        return [
            [['title', 'city_id'], 'required'],
            ['title', 'trim'],
            ['phones', 'required', 'message' => Yii::t('advertisement', 'You need to enter at least one phone number')],
            ['rubric_id', 'required', 'message' => Yii::t('advertisement', 'You need to choose rubric')],
            [['text'], 'string'],
            [['rubric_id', 'measurement_id', 'author_id', 'from_user_id', 'from_subsidiary_id', 'company_id', 'status',
                'active_to', 'type', 'currency', 'city_id'], 'integer'],
            [['without_active_to', 'contract_price', 'is_agree'], 'integer', 'max' => 1],
            [['price', 'volume'], 'number', 'min' => '0'],
            ['title', 'string', 'max' => 70],
            ['publish_from', 'string'],
            ['publish_from', 'in', 'range' => [self::PUBLISH_FROM_OTHER_USER, self::PUBLISH_FROM_SUBSIDIARY, self::PUBLISH_FROM_AUTHOR]],
            [['alias', 'phones', 'address'], 'string', 'max' => 255],
            ['active_to', 'validateActiveTo'],
            ['volume', 'validateVolume'],
            ['rubric_id', 'exist', 'skipOnError' => true, 'targetClass' => Rubric::className(),
                'targetAttribute' => ['rubric_id' => 'id'], 'message' => Yii::t('advertisement', 'Rubric not found')],
            [['is_agree', 'contract_price', 'price', 'without_active_to'], 'default', 'value' => 0],
            ['status', 'default', 'value' => 1],
            ['is_agree', 'validateIsAgree'],
            ['price', 'validatePriceWithContractPrice'],
            ['from_user_id', 'validateFromUserId'],
            ['from_subsidiary_id', 'validateFromSubsidiaryId'],
            ['phones', 'validatePhones'],
            [['image1','image2','image3','image4','image5','image6'],
                'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024*1024*2]
        ];
    }

    public static function getImagesPropertyNames()
    {
        return ['image1','image2','image3','image4','image5','image6'];
    }

    public function validateActiveTo($attribute, $params)
    {
        if ($this->active_to && $this->without_active_to) {
            $this->addError('active_to', Yii::t('advertisement', 'You must choose active to time or until'));
        }
    }

    public function validatePhones($attribute, $params)
    {
        if ($this->phones) {

            $phones = explode(',', $this->phones);
            foreach ($phones as $key => $phone) {
                $phones[$key] = trim($phone);
            }

            foreach ($phones as $key => $phone) {
                if(!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/", $phone)) {
                    $this->addError('phones', Yii::t('advertisement', 'Phone numbers must be written in the format', ['format' => '0XX-XXX-XX-XX']));
                }
            }
        }
    }

    public function validateVolume($attribute, $params)
    {
        if ($this->volume && $this->rubric_id && !$this->measurement_id) {
            $this->addError('measurement_id', Yii::t('advertisement', 'You must choose measurement'));
        }
    }

    public function validateIsAgree($attribute, $params)
    {
        if (!$this->is_agree) {
            $this->addError('is_agree', Yii::t('advertisement', 'If you want to publish advertisement you must agree with sites rules'));
        }
    }

    public function validatePriceWithContractPrice($attribute, $params)
    {
        if (($this->price == '0' && !$this->contract_price) || ($this->price && $this->contract_price == 1)) {
            $this->addError('price', Yii::t('advertisement', 'You must set price or set price as contract'));
        }
    }

    public function validateFromUserId()
    {
        if ($this->from_user_id && !Yii::$app->user->isGuest) {

            $company = Yii::$app->user->identity->company;
            $companyUsers = $company ? $company->companyUsers : [];

            if (!in_array($this->from_user_id, ArrayHelper::getColumn($companyUsers, 'user_id'))) {
                $this->addError('author_id', Yii::t('advertisement', 'Wrong from_user_id'));
            }
        }
    }

    public function validateFromSubsidiaryId()
    {
        if ($this->from_subsidiary_id && !Yii::$app->user->isGuest) {

            $company = Yii::$app->user->identity->company;
            $subsidiaries = $company ? $company->subsidiaries : [];

            if (!in_array($this->from_subsidiary_id, ArrayHelper::getColumn($subsidiaries, 'id'))) {
                $this->addError('author_id', Yii::t('advertisement', 'Wrong from_subsidiary_id'));
            }
        }
    }

    public function setImages($images)
    {
        $imagePropertiesNames = $this::getImagesPropertyNames();
        $images = $this->rearrange($images);

        foreach ($images as $key => $image) {
            if ($image['name'] && $image['type'] && $image['tmp_name'] && $image['size']
                && in_array($key, $imagePropertiesNames)) {

                $this->$key = UploadedFile::getInstance($this, $key);
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('advertisement', 'Title'),
            'alias' => 'Alias',
            'text' => Yii::t('advertisement', 'Advertisement text'),
            'rubric_id' => Yii::t('advertisement', 'Rubric'),
            'price' => Yii::t('common',  'Price'),
            'volume' => Yii::t('common',  'Volume'),
            'measurement_id' => 'Measurement ID',
            'author_id' => 'Author ID',
            'company_id' => 'Company ID',
            'status' => 'Status',
            'active_to' => Yii::t('common', 'Active To'),
            'type' => 'Type',
            'city_id' => Yii::t('common', 'City'),
            'address' => 'Address',
            'phones' => 'Phones',
            'from_user_id' => 'From user ID',
            'from_subsidiary_id' => 'From subsidiary ID'
        ];
    }

    public function beforeValidate()
    {
        if ($this->active_to) {
            $this->active_to = \DateTime::createFromFormat('d/m/Y',$this->active_to)->format('U');
        }
        if ($this->publish_from == self::PUBLISH_FROM_OTHER_USER && !$this->from_user_id) {
            $this->addError('author_id', Yii::t('advertisement', 'Wrong from_user_id'));
        }
        if ($this->publish_from == self::PUBLISH_FROM_SUBSIDIARY && !$this->from_subsidiary_id) {
            $this->addError('author_id', Yii::t('advertisement', 'Wrong from_subsidiary_id'));
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public function afterValidate()
    {
        if ($this->active_to) {
            $this->active_to = date('d/m/Y', $this->active_to);
        }

        parent::afterValidate(); // TODO: Change the autogenerated stub
    }

    public function createAdvertisement()
    {
        $model = new Advertisement();
        $model->author_id = $this->author_id;
        $model->company_id = $this->company_id;
        $model->rubric_id = $this->rubric_id;
        $model->title = $this->title;
        $model->text = $this->text;
        $model->type = $this->type;
        $model->currency_id = $this->currency;
        $model->volume = $this->volume;
        $model->measurement_id = $this->measurement_id;
        $model->price = $this->price ? $this->price : null;
        $model->active_to = ($this->active_to && !$this->without_active_to) ?
            \DateTime::createFromFormat('d/m/Y',$this->active_to)->format('U') : null;
        $model->status = $this->status;
        $model->phones = $this->phones;
        $model->address = $this->address;
        $model->city_id = $this->city_id;
        if ($this->publish_from == self::PUBLISH_FROM_OTHER_USER) {
            $model->from_user_id = $this->from_user_id;
        }
        if ($this->publish_from == self::PUBLISH_FROM_SUBSIDIARY) {
            $model->from_subsidiary_id = $this->from_subsidiary_id;
        }

        $isSave = $model->save() ? true : false;

        if ($isSave) {
            $this->saveImages($model);

            return $model;
        }

        return null;
    }

    public function saveImages(Advertisement $advertisement)
    {
        $images = [];
        $dir = Yii::getAlias('@web') . 'uploads/products/';
        $imagePropertiesNames = $this::getImagesPropertyNames();

        foreach ($imagePropertiesNames as $key) {
            if ($this->$key && !empty($this->$key->name)) {
                $filename = $dir.time().rand(0,1000).'.'.$this->$key->extension;
                $path = $this->$key->saveAs($filename);

                if (!empty($this->$key) && $path) {
                    $images[] = [
                        'is_main' => empty($images) ? true : false,
                        'path' => $filename
                    ];
                }
            }
        }

        if ($images) {
            $advertisement->saveImages($images);
        }
    }

    function rearrange( $arr ){
        $new = [];

        foreach( $arr as $key => $all ){
            foreach( $all as $i => $val ){
                $new[$i][$key] = $val;
            }
        }

        return $new;
    }
}
