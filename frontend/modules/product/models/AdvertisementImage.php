<?php

namespace frontend\modules\product\models;

use Yii;

/**
 * This is the model class for table "advertisement_image".
 *
 * @property integer $id
 * @property string $advertisement_id
 * @property string $path
 * @property integer $is_main
 * @property integer $sort
 */
class AdvertisementImage extends \yii\db\ActiveRecord
{
    const IS_MAIN_YES = 1;
    const IS_MAIN_NO = 0;

    public static function tableName()
    {
        return 'advertisement_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['advertisement_id', 'path'], 'required'],
            [['is_main', 'sort', 'advertisement_id'], 'integer'],
            [['path'], 'string', 'max' => 255],
            ['is_main', 'default', 'value' => self::IS_MAIN_NO],
            ['sort', 'default', 'value' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'advertisement_id' => 'Advertisement ID',
            'path' => 'Path',
            'is_main' => 'Is Main',
            'sort' => 'Sort',
        ];
    }

    public function getAdvertisement()
    {
        return $this->hasOne(Advertisement::className(), ['id' => 'advertisement_id']);
    }
}