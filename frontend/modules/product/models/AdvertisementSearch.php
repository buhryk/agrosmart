<?php

namespace frontend\modules\product\models;

use backend\modules\advertising\models\Advertising;
use backend\modules\commondata\models\City;
use backend\modules\commondata\models\CityLang;
use backend\modules\commondata\models\Currency;
use backend\modules\commondata\models\RegionLang;
use backend\modules\rubric\models\Rubric;
use backend\modules\rubric\models\RubricLang;
use backend\modules\rubric\models\RubricMeasurement;
use backend\modules\tariff\models\Tariff;
use frontend\models\Company;
use frontend\models\User;
use frontend\modules\cabinet\models\Top;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * AdvertisementSearch represents the model behind the search form about `frontend\modules\product\models\Advertisement`.
 */
class AdvertisementSearch extends Advertisement
{
    public $sort;
    public $price_from;
    public $price_to;
    public $volume_from;
    public $volume_to;
    public $from_who;
    public $region_id;
    public $topProducts;
    public $search;
    public $searchType;

    const SORT_CREATED_AT_DESC = 1;
    const SORT_CREATED_AT_ASC = 2;
    const SORT_PRICE_DESC = 3;
    const SORT_PRICE_ASC = 4;
    const DEFAULT_SORT = self::SORT_CREATED_AT_DESC;

    public function rules()
    {
        return [
            [['id', 'rubric_id', 'measurement_id', 'created_at', 'updated_at',
                'status', 'active_to', 'type', 'currency_id', 'region_id', 'sort', 'city_id', 'searchType'], 'integer'],
            [['title', 'alias', 'text', 'phone', 'from_who'], 'safe'],
            [['price_from', 'price_to', 'volume_from', 'volume_to'], 'number'],
            ['search', 'string']
        ];
    }

    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $attributeLabels['sort'] = Yii::t('advertisement', 'Sort by');
        $attributeLabels['from_who'] = Yii::t('advertisement', 'From who');
        $attributeLabels['type'] = Yii::t('advertisement', 'Advertisement type');

        return $attributeLabels;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Advertisement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->select([Advertisement::tableName().'.*']);

        $query
            ->joinWith('company')
            ->joinWith('currency')
            ->joinWith('author');

        if ($this->search && empty($this->searchType)) {
            $this->search = trim($this->search);
            $cityModel = CityLang::find()->andWhere(['title' => $this->search])->one();

            if ($cityModel) {
                $this->city_id = $cityModel->city_id;
            }

            $oblastModel = RegionLang::find()->andWhere(['like','title' , $this->search])->one();

            if ($oblastModel) {
                $this->region_id = $oblastModel->oblast_id;
            }

            $query->orFilterWhere(['like', Advertisement::tableName().'.title' , $this->search])
                ->orFilterWhere( ['like', Advertisement::tableName().'.text' , $this->search])
                ->orFilterWhere(['like', Advertisement::tableName().'.address' , $this->search])
                ->orFilterWhere( ['in', 'rubric_id', $this->getRubrictByTitleIds($this->search)]);

        } else {
            $this->search = trim($this->search);
            $query->orFilterWhere(['like', Advertisement::tableName().'.title' , $this->search]);
        }

        $query->andFilterWhere([
            Advertisement::tableName().'.city_id' => $this->city_id,
            Advertisement::tableName().'.type' => $this->type,
            User::tableName().'.status' => User::STATUS_ACTIVE,
        ]);

        $query->andFilterWhere(['or',
            ['and', ['IS NOT', 'company_id', (new Expression('Null'))], [Company::tableName().'.status' => Company::STATUS_ACTIVE_YES]],
            ['IS', 'company_id', (new Expression('Null'))]
        ]);

        if ($this->rubric_id) {
            $query->andFilterWhere(['in', 'rubric_id', $this->getRubricsSubricsIds($this->rubric_id)]);
        }

        $query->andWhere([Advertisement::tableName().'.status' => Advertisement::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()]);

        $newQuery = clone $query;
        $this->selectTopProduct($newQuery);

        if (($this->price_from || $this->price_to) && $this->currency_id ) {
            $this->addFilterByPrice($query, $this->price_from, $this->price_to, $this->currency_id);
        }

        if (($this->volume_from || $this->volume_to) && $this->measurement_id ) {
            $this->addFilterByVolume($query, $this->volume_from, $this->volume_to, $this->measurement_id);
        }

        if ($this->region_id) {
            $query->joinWith('city');
            $query->andFilterWhere([City::tableName().'.oblast_id' => $this->region_id]);
        }

        if ($this->from_who) {
            if ($this->from_who == self::FROM_COMPANY) {
                $query->andFilterWhere(['IS NOT', 'company_id', (new Expression('Null'))]);
            } elseif ($this->from_who == self::FROM_PRIVATE_PERSON) {
                $query->andFilterWhere(['IS', 'company_id', (new Expression('Null'))]);
            }
        }

        if ($this->sort) {
            if ($this->sort == self::SORT_CREATED_AT_DESC) {
                $query->orderBy(['created_at' => SORT_DESC]);
            } elseif ($this->sort == self::SORT_CREATED_AT_ASC) {
                $query->orderBy(['created_at' => SORT_ASC]);
            } elseif (in_array($this->sort, [self::SORT_PRICE_DESC, self::SORT_PRICE_ASC])) {
                $query->addSelect(['(price/'.Currency::tableName().'.weight) AS universalPrice']);
                if ($this->sort == self::SORT_PRICE_DESC) {
                    $query->orderBy('universalPrice DESC');
                } else {
                    $query->orderBy('universalPrice ASC');
                }
            }
        } else {
            $query->orderBy(['created_at' => SORT_DESC]);
        }


        return $dataProvider;
    }

    public function selectTopProduct($query)
    {
        $query->select(Advertisement::tableName().'.*, round(1, 500) as `random`');
        $query->innerJoin(Top::tableName(), Advertisement::tableName().'.`id` = `model_id`')
           ->andWhere(['model_name' => Tariff::MODEL_PRODUCT])
           ->andWhere(['in', Top::tableName().'.type' , [Tariff::TYPE_TOP, Tariff::TYPE_COMPLEX]])
           ->andWhere(['<=', 'start', time()])
           ->andWhere(['>=', 'finish', time()])
           //->andWhere(['advertisement.status' => Advertisement::STATUS_ACTIVE_YES])
           ->orderBy('random');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => 3,
            'pagination' => [
                'pageSize' =>3,
            ]
        ]);

        $this->topProducts = $dataProvider;
    }

    protected function addFilterByPrice($query, $price_from, $price_to, $currency_id)
    {
        $currentCurrency = Currency::findOne($currency_id);
        $currencies = Currency::find()->all();
        if (!$currentCurrency) {
            return $query;
        }

        $conditions = [];
        $conditions[0] = 'or';

        if ($price_from && $price_to) {
            foreach ($currencies as $currency) {
                if ($currency->primaryKey == $currency_id) {
                    $conditions[] = ['and',
                        ['>=', 'price', $price_from],
                        ['<=', 'price', $price_to],
                        ['=', 'currency_id', $currency_id]];
                } else {
                    $conditions[] = ['and',
                        ['>=', 'price', ($price_from * $currency->weight) / $currentCurrency->weight],
                        ['<=', 'price', ($price_to * $currency->weight) / $currentCurrency->weight],
                        ['=', 'currency_id', $currency->primaryKey]];
                }
            }
        } elseif ($price_from && !$price_to) {
            foreach ($currencies as $currency) {
                if ($currency->primaryKey == $currency_id) {
                    $conditions[] = ['and',
                        ['>=', 'price', $price_from],
                        ['=', 'currency_id', $currency_id]];
                } else {
                    $conditions[] = ['and',
                        ['>=', 'price', ($price_from * $currency->weight) / $currentCurrency->weight],
                        ['=', 'currency_id', $currency->primaryKey]];
                }
            }
        } elseif ($price_to && !$price_from) {
            foreach ($currencies as $currency) {
                if ($currency->primaryKey == $currency_id) {
                    $conditions[] = ['and',
                        ['>=', 'price', 0],
                        ['<=', 'price', $price_to],
                        ['=', 'currency_id', $currency_id]];
                } else {
                    $conditions[] = ['and',
                        ['>=', 'price', 0],
                        ['<=', 'price', ($price_to * $currency->weight) / $currentCurrency->weight],
                        ['=', 'currency_id', $currency->primaryKey]];
                }
            }
        }

        $query->andFilterWhere($conditions);

        return $query;
    }

    protected function addFilterByVolume($query, $volume_from, $volume_to, $measurement_id)
    {
        $currentMeasurement = RubricMeasurement::findOne($measurement_id);
        if (!$currentMeasurement) {
            return $query;
        }
        $measurementCategory = $currentMeasurement->category;
        if (!$measurementCategory) {
            return $query;
        }

        $measurements = $measurementCategory->measurements;

        $conditions = [];
        $conditions[0] = 'or';

        if ($volume_from && $volume_to) {
            foreach ($measurements as $measurement) {
                if ($measurement->primaryKey == $measurement_id) {
                    $conditions[] = ['and',
                        ['>=', 'volume', $volume_from],
                        ['<=', 'volume', $volume_to],
                        ['=', 'measurement_id', $measurement_id]];
                } else {
                    $conditions[] = ['and',
                        ['>=', 'volume', ($volume_from * $currentMeasurement->weight) / $measurement->weight],
                        ['<=', 'volume', ($volume_to * $currentMeasurement->weight) / $measurement->weight],
                        ['=', 'measurement_id', $measurement->primaryKey]];
                }
            }
        } elseif ($volume_from && !$volume_to) {
            foreach ($measurements as $measurement) {
                if ($measurement->primaryKey == $measurement_id) {
                    $conditions[] = ['and',
                        ['>=', 'volume', $volume_from],
                        ['=', 'measurement_id', $measurement_id]];
                } else {
                    $conditions[] = ['and',
                        ['>=', 'volume', ($volume_from * $currentMeasurement->weight) / $measurement->weight],
                        ['=', 'measurement_id', $measurement->primaryKey]];
                }
            }
        } elseif ($volume_to && !$volume_from) {
            foreach ($measurements as $measurement) {
                if ($measurement->primaryKey == $measurement_id) {
                    $conditions[] = ['and',
                        ['>=', 'volume', 0],
                        ['<=', 'volume', $volume_to],
                        ['=', 'measurement_id', $measurement_id]];
                } else {
                    $conditions[] = ['and',
                        ['>=', 'volume', 0],
                        ['<=', 'volume', ($volume_to * $currentMeasurement->weight) / $measurement->weight],
                        ['=', 'measurement_id', $measurement->primaryKey]];
                }
            }
        }

        $query->andFilterWhere($conditions);

        return $query;
    }

    public static function getSortingProperties()
    {
        return [
            self::SORT_CREATED_AT_DESC => Yii::t('advertisement', 'SORT_CREATED_AT_DESC'),
            self::SORT_CREATED_AT_ASC => Yii::t('advertisement', 'SORT_CREATED_AT_ASC'),
            self::SORT_PRICE_DESC => Yii::t('advertisement', 'SORT_PRICE_DESC'),
            self::SORT_PRICE_ASC => Yii::t('advertisement', 'SORT_PRICE_ASC'),
        ];
    }

    private function getRubrictByTitleIds($title)
    {
        $ids = [];
        $rubricModels = RubricLang::find()->andWhere(['like', 'title', $title])->asArray()->all();
        foreach ($rubricModels as $item) {
            $ids = array_merge($ids, $this->getRubricsSubricsIds($item['rubric_id']));
        }

        return $ids;
    }

    public function getRubricsSubricsIds($rubricId)
    {
        $ids = [$rubricId];

        $rubric = Rubric::findOne($rubricId);
        $subrubrics = $rubric->subrubrics;
        foreach ($subrubrics as $subrubric) {
            $ids[] = $subrubric->primaryKey;
            $ids = array_merge($ids, $this->getSubrubricsIds($subrubric));
        }

        return $ids;
    }

    public function getSubrubricsIds(Rubric $rubric)
    {
        $ids = [];
        $subrubrics = $rubric->subrubrics;

        foreach ($subrubrics as $subrubric) {
            $ids[] = $subrubric->primaryKey;
            $ids = array_merge($ids, $this->getSubrubricsIds($subrubric));
        }

        return $ids;
    }
}