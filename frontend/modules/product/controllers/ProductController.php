<?php

namespace frontend\modules\product\controllers;

use backend\modules\rubric\models\Rubric;
use frontend\models\Company;
use frontend\models\CompanyUser;
use frontend\models\SignupForm;
use frontend\models\SignupUserForm;
use frontend\models\User;
use frontend\modules\product\models\AdvertisementForm;
use frontend\modules\cabinet\models\Subsidiary;
use Yii;
use frontend\modules\product\models\Advertisement;
use frontend\modules\product\models\AdvertisementSearch;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\GoneHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ProductController extends Controller
{

    public function actionIndex()
    {
        $searchModel = new AdvertisementSearch();
        $params = Yii::$app->request->queryParams;

        $choosenRubric = Yii::$app->request->get('rubric') ? Rubric::find()->where([
            'alias' => Yii::$app->request->get('rubric'),
            'active' => Rubric::ACTIVE_YES
        ])->one() : null;

        if ($choosenRubric) {
            $params['AdvertisementSearch']['rubric_id'] = $choosenRubric->primaryKey;
        }

        if ($choosenRubric) {
            $searchModel->rubric_id = $choosenRubric->primaryKey;
        }

        $dataProvider = $searchModel->search($params);

        if ($searchModel->rubric_id && !$choosenRubric) {
            $choosenRubric = Rubric::findOne($searchModel->rubric_id);
        }

        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'viewedAdvertisements' => $this->getViewedAdvertisements(0, 3),
            'choosenRubric' => $choosenRubric
        ];

        return $this->render('index', $data);
    }

    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->render('select_type');
        }

        if (Yii::$app->user->identity->companyUser) {
            return $this->redirect('create-as-company');
        }

        return $this->redirect('create-as-private-person');
    }

    public function actionCreateAsPrivatePerson()
    {
        $params = [];

        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->companyUser) {
                return $this->redirect(['create-as-company']);
            }
            $model = new AdvertisementForm();
            $user = Yii::$app->user->identity;
            $model->author_id = $user->getId();
            if ($user->can_add_advertisements == User::CAN_ADD_ADVERTISEMENTS_NO) {
                throw new NotAcceptableHttpException('Access denied');
            }
        } else {
            $model = new AdvertisementForm();
            $user = new SignupUserForm();
            $params['user'] = $user;
        }

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $request = Yii::$app->request->post();
            $model->load($request);

            if (!Yii::$app->user->isGuest) {
                $validate = $model->validate();
                $errors = $model->getErrors();
            } else {
                $user->load($request);
                $user->admit = 1;

                if ($user->email && !$user->username) {
                    $user->username = $user->email;
                }
                $user->password = Yii::$app->security->generatePasswordHash(Yii::$app->security->generateRandomString(16));

                $validate = Model::validateMultiple([$model, $user]);
                $errors = array_merge($model->getErrors(), $user->getErrors());
            }

            if (isset($_FILES[StringHelper::basename(get_class($model))])) {
                $images = $_FILES[StringHelper::basename(get_class($model))];
                $model->setImages($images);
            }

            if ($validate && !$errors && Yii::$app->user->isGuest) {
                $user = $user->signup();
                $user->status = User::STATUS_NOT_CONFIRMED;
                $user->generatePasswordResetToken();
                $user->save();
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            $response = [
                'status' => true,
                'validation' => $validate,
                'errors' => $errors
            ];

            if ($validate && !$errors && $user) {
                $model->author_id = $user->primaryKey;
                $advertisement = $model->createAdvertisement();

                if ($advertisement) {
                    if (Yii::$app->user->isGuest){
                        $response['success_url'] = Url::to(['success', 'id' => $advertisement->primaryKey]);
                    } else {
                        $response['success_url'] = Url::to(['/cabinet/pull-top/index',
                            'model_id' => $advertisement->primaryKey,
                            'model_name' => 'product',
                            'type' => 'new'
                        ]);
                    }
                }
            }

            return $response;
        }

        $params['model'] = $model;

        return $this->render('create', $params);
    }

    public function actionCreateAsCompany()
    {
        $company = !Yii::$app->user->isGuest ? Yii::$app->user->identity->company : null;
        if (Yii::$app->user->isGuest) {
            return $this->render('create_as_company_default');
        } elseif (!Yii::$app->user->isGuest && !$company) {
            return $this->redirect(['create-as-private-person']);
        }

        $model = new AdvertisementForm();
        $user = Yii::$app->user->identity;
        $otherCompanyUsers = $this->getOtherCompanyUsers($company);
        $subsidiaries = $this->getSubsidiaries($company);

        if ($user->can_add_advertisements == User::CAN_ADD_ADVERTISEMENTS_NO) {
            throw new NotAcceptableHttpException('Access denied');
        }

        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $request = Yii::$app->request->post();
            $model->load($request);
            $model->author_id = $user->id;
            $model->company_id = $company->primaryKey;

            $validate = $model->validate();
            $errors = $model->getErrors();

            if (isset($_FILES[StringHelper::basename(get_class($model))])) {
                $images = $_FILES[StringHelper::basename(get_class($model))];
                $model->setImages($images);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            $response = [
                'status' => true,
                'validation' => $validate,
                'errors' => $errors
            ];

            if ($validate && !$errors && $user) {
                $advertisement = $model->createAdvertisement();
                if ($advertisement) {
                    $response['success_url'] = Url::to(['/cabinet/pull-top/index',
                        'model_id' => $advertisement->primaryKey,
                        'model_name' => 'product',
                        'type' => 'new'
                    ]);
                }
            }

            return $response;
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'otherCompanyUsers' => $otherCompanyUsers,
            'subsidiaries' => $subsidiaries
        ]);
    }

    private function getOtherCompanyUsers(Company $company)
    {
        $usersData = [];

        $otherCompanyUsers = CompanyUser::find()->where(['company_id' => $company->primaryKey])
            ->andWhere(['<>', 'user_id', Yii::$app->user->identity->id])
            ->joinWith('user')
            ->all();

        foreach ($otherCompanyUsers as $oneUser) {
            $usersData[] = [
                'id' => $oneUser->user_id,
                'fullname' => $oneUser->user->fullname
            ];
        }

        return $usersData;
    }

    public function getSubsidiaries(Company $company)
    {
        $subsidiariesData = [];

        $subsidiaries = Subsidiary::find()->where(['company_id' => $company->primaryKey])
            ->joinWith('city')
            ->all();

        foreach ($subsidiaries as $subsidiary) {
            $subsidiariesData[] = [
                'id' => $subsidiary->primaryKey,
                'info' => $subsidiary->getType() . ' - ' . $subsidiary->detailAddress
            ];
        }

        return $subsidiariesData;
    }

    protected function findModel($id)
    {
        if (($model = Advertisement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetRubricSubrubrics()
    {
        if (Yii::$app->request->isAjax) {
            $request = Yii::$app->request->post();

            if (empty($request['rubric_id'])) {
                throw new BadRequestHttpException('Not all parameters provided.');
            }

            $rubricId = (int)$request['rubric_id'];

            $rubric = Rubric::find()
                ->where(['id' => $rubricId])
                ->one();

            if (!$rubric) {
                throw new NotFoundHttpException('Rubric not found.');
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'status' => true,
                'subrubrics' => ArrayHelper::map($rubric->subrubrics, 'id', 'title', 'sort')
            ];
        }
    }

    public function actionGetRubricMeasurements()
    {
        if (Yii::$app->request->isAjax) {
            $request = Yii::$app->request->post();

            if (empty($request['rubric_id'])) {
                throw new BadRequestHttpException('Not all parameters provided.');
            }

            $rubricId = (int)$request['rubric_id'];

            $rubric = Rubric::find()
                ->where(['id' => $rubricId])
                ->one();

            if (!$rubric) {
                throw new NotFoundHttpException('Rubric not found.');
            }

            $measurementsCategories = $rubric->measurementCategories;
            $result = [];

            foreach ($measurementsCategories as $category) {
                $result[] = [
                    'title' => $category->title,
                    'measurements' => ArrayHelper::map($category->measurements, 'id', 'title')
                ];
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'status' => true,
                'measurements' => $result
            ];
        }
    }

    public function actionSuccess($id)
    {
        return $this->render('success', [
            'model' => $this->findModel($id)
        ]);
    }

    public function getCompanyUsersForDropDownList(Company $company)
    {
        $array = [];

        foreach ($company->companyUsers as $companyUser) {
            $fullname = '';
            $user = $companyUser->user;
            if ($user->name) {
                $fullname .= $user->name;
            }
            if ($user->surname) {
                $fullname .= ($fullname ? ' ' : '') . $user->surname;
            }
            if ($user->email) {
                if (!$fullname) {
                    $fullname = Yii::t('advertisement', 'Without name');
                }
                $fullname .= ' (' . $user->email . ')';
            }

            $array[] = ['id' => $user->id, 'fullname' => $fullname];
        }

        return $array;
    }

    public function actionView($slug)
    {
        $model = Advertisement::find()
            ->where([Advertisement::tableName().'.alias' => $slug, 'status' => Advertisement::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
            ->joinWith('rubric')
            ->one();

        if (!$model) {
            if (Advertisement::find()->where(['alias' => $slug])->exists()) {
                throw new GoneHttpException('The requested page deleted');
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

        }

        $viewedAdvertisements = $this->getViewedAdvertisements($model->primaryKey, 4);
        $this->saveAdvertisementView($model->primaryKey);

        $model->views_count += 1;
        $model->update(false);

        return $this->render('view', [
            'model' => $model,
            'viewedAdvertisements' => $viewedAdvertisements
        ]);
    }

    private function saveAdvertisementView($id)
    {
        $viewedAdvertisementsIds = Yii::$app->session->get('viewed_advertisements');

        if (!$viewedAdvertisementsIds) {
            Yii::$app->session->set('viewed_advertisements', json_encode([$id]));
        } else {
            $viewedAdvertisementsIds = json_decode($viewedAdvertisementsIds);

            foreach ($viewedAdvertisementsIds as $key => $viewedAdvertisementId) {
                if ($viewedAdvertisementId == $id) {
                    array_splice($viewedAdvertisementsIds, $key);
                    $viewedAdvertisementsIds[] = $id;
                    Yii::$app->session->set('viewed_advertisements', json_encode($viewedAdvertisementsIds));
                    return true;
                }
            }

            $viewedAdvertisementsIds[] = $id;
            Yii::$app->session->set('viewed_advertisements', json_encode($viewedAdvertisementsIds));
        }
    }

    private function getViewedAdvertisements($for = 0, $count = false)
    {
        $viewedAdvertisementsIds = Yii::$app->session->get('viewed_advertisements');

        $result = [];

        if (!$viewedAdvertisementsIds) {
            return $result;
        }

        $viewedAdvertisementsIds = json_decode($viewedAdvertisementsIds);

        $ids = [];
        for ($i = count($viewedAdvertisementsIds) - 1; $i >= 0; $i--) {
            $ids[] = $viewedAdvertisementsIds[$i];
            if (count($ids) >= $count) {
                break;
            }
        }

        $advertisements =  Advertisement::find()
            ->where([Advertisement::tableName().'.status' => Advertisement::STATUS_ACTIVE_YES])
            ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
            ->andWhere(['in', Advertisement::tableName().'.id', $ids]);

        if ($for) {
            $advertisements = $advertisements->andWhere(['<>', Advertisement::tableName().'.id', $for]) ;
        }

        $advertisements = $advertisements
            ->joinWith('author')
            ->limit($count)
            ->all();

        foreach ($ids as $id) {
            foreach ($advertisements as $advertisement) {
                if ($id == $advertisement->primaryKey) {
                    $result[] = $advertisement;
                    break;
                }
            }
        }

        return $result;
    }
}
