<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use backend\modules\commondata\models\Currency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use backend\modules\commondata\models\Region;
use frontend\modules\product\models\Advertisement;
use frontend\modules\product\models\AdvertisementSearch;
use backend\modules\commondata\models\City;

$getCityUrl = \yii\helpers\Url::to(['/city-list/index']);
$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$electedAdvertisementsIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedAdvertisements, 'id') : [];
$electedCompaniesIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedCompanies, 'id') : [];
$electedUsersIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedUsers, 'id') : [];
$savedAddresses = $currentUser ? ArrayHelper::getColumn($currentUser->savedAddresses, 'address') : [];
$messageAddToElected = Yii::t('common', 'Add to elected');
$messageRemoveFromElected = Yii::t('common', 'Remove from elected');

$pageH1 = $choosenRubric ? Yii::t('advertisement', 'Products and services').' : '.$choosenRubric->title : Yii::t('advertisement', 'Products and services');

if (isset($choosenRubric->seo)){
    $this->title = $choosenRubric->seo->title;
    $this->registerMetaTag(['name'=>'description', 'content'=> $choosenRubric->seo->description]);
    $this->registerMetaTag(['name'=>'keywords', 'content'=> $choosenRubric->seo->keywords]);
    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['index', 'rubric' =>$choosenRubric->alias])]);
} else {
    $this->title = $pageH1 . ' | '. 'agro-smart.com.ua';
    $this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['/product/product/index'])]);
}

$this->params['breadcrumbs'][] = Yii::t('advertisement', 'Products and services');

?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<div class="container" id="common-search">
  <div class="row">
      <div class="block-input-search">
         <?php $form = ActiveForm::begin(['method' => 'get',  'action' => ['index']])?>
              <?=Html::activeInput('text', $searchModel,
                  'search',
                  ['class' => 'form-control', 'placeholder' => Yii::t('common', 'Я ищу...')]) ?>
              <button class="send-off"><i class="glyphicon glyphicon-search"></i></button>
              <input type="checkbox"
                     id="advertisementsearch-searchtype"
                     name="AdvertisementSearch[searchType]"
                     value="1"
              <?=$searchModel->searchType ? 'checked' : '' ?>>
              <label for="advertisementsearch-searchtype">
                  <?=Yii::t('advertisement', 'Искать только в заголовках') ?>
              </label>
          <?php ActiveForm::end() ?>
      </div>
  </div>
</div>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options' => [
        'class' => 'row',
        'id' => 'advertisements-form'
    ]
]); ?>
<div class="text-center" style="margin-top: 20px;">
    <a href="<?= Url::to(['create']); ?>" class="btn-green big">
        <?= Yii::t('advertisement', 'Publish product/service'); ?>
    </a>
</div>

<?= $this->render('_rubric_filter', [
    'choosenRubric' => $choosenRubric
]); ?>

<div class="category-filter">
    <div class="container">


        <div class="col-sm-6">
            <?php $city = $searchModel->city_id ? City::find()
                ->where([City::tableName().'.id' => $searchModel->city_id])
                ->joinWith('region')
                ->one() : null ?>

            <?= $form->field($searchModel, 'city_id')->widget(Select2::className(), [
                'initValueText' => $city ? $city->detailInfo : '', // set the initial display text
                'options' => [
                    'placeholder' => Yii::t('advertisement', 'Enter city name')
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    'ajax' => [
                        'url' => $getCityUrl,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ])->label(false); ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->field($searchModel, 'region_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Region::find()->joinWith('lang')->all(), 'id', 'title'),
                'options' => [
                    'placeholder' => Yii::t('common', 'Choose region')
                ],
                'pluginOptions' => ['allowClear' => true]
            ])->label(false); ?>
        </div>
        <div class="big-filter text-center">
            <button type="button" class="collapsed" data-toggle="collapse" href="#form-up">
                <span class="filter-pro flaticon-down-arrow">
                    <?= Yii::t('advertisement', 'Advanced filter'); ?>
                </span>
            </button>
        </div>
        <div id="form-up" class="collapse in">
            <div class="col-sm-12" style="height: 0;">
                <?= $form->field($searchModel, 'rubric_id')->hiddenInput()->label(false); ?>
            </div>
            <div class="one-row">
                <div class="col-sm-6 filter-price">
                    <label for=""><?= Yii::t('advertisement', 'Price'); ?>:</label>
                    <div class="row">
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'price_from')->textInput([
                                'placeholder' => Yii::t('advertisement', 'From')
                            ])->label(false); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'price_to')->textInput([
                                'placeholder' => Yii::t('advertisement', 'To')
                            ])->label(false); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'currency_id')
                                ->dropDownList(ArrayHelper::map(Currency::find()->all(), 'id', 'code'))
                                ->label(false); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?php if ($choosenRubric) { ?>
                        <label for=""><?= Yii::t('advertisement', 'Volume'); ?>:</label>
                        <div class="row">
                            <div class="col-sm-4">
                                <?= $form->field($searchModel, 'volume_from')->textInput([
                                    'placeholder' => Yii::t('advertisement', 'From')
                                ])->label(false); ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($searchModel, 'volume_to')->textInput([
                                    'placeholder' => Yii::t('advertisement', 'To')
                                ])->label(false); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $measurementData = $choosenRubric->getMeasurementsDataForDropDownLists(); ?>
                                <?php if ($measurementData) {
                                    echo $form->field($searchModel, 'measurement_id')
                                        ->dropDownList($measurementData)
                                        ->label(false);
                                } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="one-row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'type')->dropDownList(Advertisement::getTypes(),['prompt' => '...']); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'from_who')->dropDownList([
                                Advertisement::FROM_COMPANY => Yii::t('advertisement', 'Companies advertisements'),
                                Advertisement::FROM_PRIVATE_PERSON => Yii::t('advertisement', 'Private persons advertisements'),
                            ], ['prompt' => '...']); ?>
                        </div>
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'sort')->dropDownList(AdvertisementSearch::getSortingProperties()); ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <p>&nbsp;</p>
            <div class="one-row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12 mtac" style="text-align: right">
                            <?php echo Html::submitButton(Yii::t('advertisement', 'Search'), ['class' => 'btn-green']) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12 reset-filter  mtac">
                            <a href="<?= Url::to(['index']); ?>" class="btn-green">
                                <?= Yii::t('advertisement', 'Reset filter'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="one-row" style="line-height: 44px;">
                <div class="col-sm-6 col-xs-6">
                    <div class="row">
                        <div class="col-sm-12 total-count"><?= $dataProvider->getTotalCount(); ?></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12" style="font-size: 19px;">
                            <?= Yii::t('advertisement', 'count of products and services'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<?php ActiveForm::end(); ?>
<div class="bg-grey">
    <div class="container all-products-container">
        <div class="row">
            <div class="col-sm-9 top-marclear">
                <?php echo \yii\widgets\ListView::widget([
                    'dataProvider' => $searchModel->topProducts,
                    'itemView' => '_advertisement',
                    'showOnEmpty' => true,
                    'layout' => "{items}",
                    'viewParams' => [
                        'electedCompaniesIds' => $electedCompaniesIds,
                        'electedAdvertisementsIds' => $electedAdvertisementsIds,
                        'electedUsersIds' => $electedUsersIds,
                        'messageAddToElected' => $messageAddToElected,
                        'messageRemoveFromElected' => $messageRemoveFromElected,
                        'savedAddresses' => $savedAddresses
                    ]
                ]); ?>


                <?php echo \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_advertisement',
                    'pager' => [
                        'registerLinkTags' => true,
                    ],

                    'viewParams' => [
                        'electedCompaniesIds' => $electedCompaniesIds,
                        'electedAdvertisementsIds' => $electedAdvertisementsIds,
                        'electedUsersIds' => $electedUsersIds,
                        'messageAddToElected' => $messageAddToElected,
                        'messageRemoveFromElected' => $messageRemoveFromElected,
                        'savedAddresses' => $savedAddresses
                    ]
                ]); ?>
            </div>
            <div class="col-sm-3 sidebar-banner" style="margin-top: 66px;">
               <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
            </div>
        </div>
    </div>
</div>

<?php if ($viewedAdvertisements) { ?>
    <div class="container view-product">
        <div class="title-black text-center">
            <?= Yii::t('advertisement', 'Viewed advertisements'); ?>
        </div>
        <div class="row">
            <?php foreach ($viewedAdvertisements as $model) { ?>
                <?= $this->render('_viwed_advertisement', ['model' => $model]); ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>
