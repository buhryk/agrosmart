<?php
use yii\helpers\Url;
use common\helpers\DateHelper;
use kartik\rating\StarRating;
use frontend\modules\elected\models\Elected;
use frontend\models\Company;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\components\tool\Image;

$images = $model->images;
$company = isset($company) ? $company : $model->company;
$location = $model->location;
$user = isset($user) ? $user : $model->author;
$top = $model->allotted ? 'top' : NULL;
$currentUser = isset($currentUser) ? $currentUser : (!Yii::$app->user->isGuest ? Yii::$app->user->identity : null);
$messageAddToElected = isset($messageAddToElected) ? $messageAddToElected : Yii::t('common', 'Add to elected');
$messageRemoveFromElected = isset($messageRemoveFromElected) ? $messageRemoveFromElected : Yii::t('common', 'Remove from elected');
$electedAdvertisementsIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedAdvertisements, 'id') : [];
?>

<div class="sales-product col-xs-12 col-sm-12 clear product-item-mob <?=$top ?>">
    <div class="col-sm-4 col-xs-12 sales-product-image product-item-mob" style="height: 310px;">
        <a href="<?= Url::to(['/product/product/view', 'slug' => $model->alias]); ?>">
            <figure class="effect-chico">
                <?= isset($images[0]) ? Yii::$app->thumbnail->img($images[0]->path,
                    ['thumbnail' => ['width' => 204, 'height' => 204]],
                    ['alt' => $model->getTitle()]) : \yii\helpers\Html::img('/images/prod.jpg', ['alt' => $model->getTitle()]);
                ?>
            </figure>
        </a>
    </div>
    <div class="col-sm-5 p0">
        <span class="product-type">
           <?= ucfirst($model->detailType); ?>
        </span>
        <a href="<?= Url::to(['/product/product/view', 'slug' => $model->alias]); ?>" class="prod-title">
            <?=$model->getTitle(); ?>
        </a>
        <div class="price"> <small class="title"><?=Yii::t('advertisement', 'Цена') ?>  :</small> <?= $model->detailPrice; ?></div>
        <?php if ($model->volume): ?>
            <p class="weight"><small class="title"><?=Yii::t('advertisement', 'Обьем') ?> :</small>  <?= $model->volume ? $model->detailVolume : ''; ?></p>
        <?php endif; ?>
        <p class="prod-date">
            <?= date('d', $model->created_at).' '.DateHelper::getRussianMonth(date('F', $model->created_at)).', '.date('Y', $model->created_at); ?>
        </p>
        <div class="location">
            <p title="<?= $location; ?>"><?= $location; ?></p>
            <?php if (!Yii::$app->user->isGuest && !in_array($location, $savedAddresses)) { ?>
                <a class="green-link add-address-to-saved" data-address="<?= $location; ?>" style="cursor:pointer;">
                    <?= Yii::t('common', 'Remember address'); ?>
                </a>
            <?php } ?>
        </div>
        <p class="prod-phones"><?= $model->phones; ?></p>
        <div class="prod-text" >
            <?=$model->getText(); ?>
        </div>

        <?php if (!Yii::$app->user->isGuest) { ?>
            <a class="green-link change-elected <?= in_array($model->primaryKey, $electedAdvertisementsIds) ?
                'remove-from-elected' : 'add-to-elected'; ?>"
               data-type="<?= Elected::ALIAS_ADVERTISEMENT; ?>"
               data-record_id="<?= $model->primaryKey; ?>"
               data-message-add="<?= $messageAddToElected; ?>"
               data-message-remove="<?= $messageRemoveFromElected; ?>"
               style="cursor: pointer"
            >
                <?= in_array($model->primaryKey, $electedAdvertisementsIds) ? $messageRemoveFromElected : $messageAddToElected; ?>
            </a>
        <?php } ?>
    </div>
    <div class="col-sm-3 company" style="text-align: center">
        <?php if ($company) { ?>
            <a href="<?= Url::to(['/company/company/view', 'id' => $company->primaryKey]); ?>" class="prod-owner">
                <figure>
                    <?php $companyLogo = $company->logotype ? ('/'.$company->logotype) : '/images/user.jpg'; ?>
                    <?=  Html::img(Image::resize($companyLogo, 98, 98));
                    ?>
                </figure>
                <div class="name-company">
                    <?= Yii::t('cabinet', 'COMPANY'); ?>:
                    <br><?=Html::encode($company->name); ?>
                </div>
            </a>
            <?= StarRating::widget([
                'name' => 'rating_company_'.$model->primaryKey,
                'value' => $company->ratio,
                'pluginOptions' => ['step' => 0.1, 'readonly' => true, 'showClear' => false, 'showCaption' => false]
            ]); ?>
            <?php if (!Yii::$app->user->isGuest) { ?>
                <a class="green-link change-elected <?= in_array($model->primaryKey, $electedCompaniesIds) ?
                    'remove-from-elected' : 'add-to-elected'; ?>"
                   data-type="<?= Elected::ALIAS_COMPANY; ?>"
                   data-record_id="<?= $company->primaryKey; ?>"
                   data-message-add="<?= $messageAddToElected; ?>"
                   data-message-remove="<?= $messageRemoveFromElected; ?>"
                   style="cursor: pointer"
                >
                    <?= in_array($company->primaryKey, $electedCompaniesIds) ? $messageRemoveFromElected : $messageAddToElected; ?>
                </a>
            <?php } ?>

            <div style="text-align: left">
            <?php if ($company->is_confirmed): ?>
                <?= Yii::t('common', 'Status'); ?>:<br>
                <span style="font-size: 13px;"><?= $company->isConfirmedDetail; ?></span>
            <?php endif ?>
            </div>
        <?php } else { ?>
            <a href="<?= Url::to(['/user/user/view', 'id' => $user->primaryKey]); ?>" class="prod-owner">
                <figure>
                    <img src="/images/user.jpg" alt="">
                </figure>

                <div class="name-company"><?= $user->fullName; ?></div>
                <?php if (!Yii::$app->user->isGuest) { ?>
                    <a class="green-link change-elected <?= in_array($model->primaryKey, $electedUsersIds) ?
                        'remove-from-elected' : 'add-to-elected'; ?>"
                       data-type="<?= Elected::ALIAS_USER; ?>"
                       data-record_id="<?= $user->primaryKey; ?>"
                       data-message-add="<?= $messageAddToElected; ?>"
                       data-message-remove="<?= $messageRemoveFromElected; ?>"
                       style="cursor: pointer"
                       >

                        <?= in_array($user->id, $electedUsersIds) ? $messageRemoveFromElected : $messageAddToElected; ?>
                    </a>
                <?php } ?>
            </a>
        <?php } ?>
    </div>
</div>