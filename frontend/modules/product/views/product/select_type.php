<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = Yii::t('advertisement', 'Add product/service');
$this->params['breadcrumbs'][] = ['label' => Yii::t('advertisement', 'Products and services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<h2 class="ap-prod-title"><?= Yii::t('advertisement', 'Choose advertisement type'); ?></h2>
<ul class="ap-choose">
    <li class="ap-choose__item">
        <a href="<?= Url::to(['/site/signup-company']); ?>"><?= Yii::t('advertisement', 'company'); ?></a>
    </li>
    <li class="ap-choose__item">
        <a href="<?= Url::to(['/site/signup']); ?>"><?= Yii::t('advertisement', 'private person'); ?></a>
    </li>
</ul>
<div class="ap-wrapper">
    <div class="ap-container">
        <div class="ap-info-text">
            <p class="ap-info-text__item"><?= Yii::t('advertisement', 'Чтобы публиковать объявления, нужно выбрать от кого!'); ?></p>
            <p class="ap-info-text__item"><?= Yii::t('advertisement', 'Вы сможете управлять вашими объявлениями, публиковать цены,'); ?></p>
            <p class="ap-info-text__item"><?= Yii::t('advertisement', 'а также использовать все инструменты ЛК'); ?></p>
        </div>
    </div>
</div>