<?php
use backend\modules\rubric\models\Rubric;
use yii\helpers\Url;

$parents = [];
if ($choosenRubric) {
    $parent = $choosenRubric->activeParent;
    while ($parent !== null) {
        $parents[] = $parent;
        $parent = $parent->activeParent;
    }
}
?>

<?php if (!$choosenRubric) { ?>
    <?php $rubrics = Rubric::find()
        ->where(['parent_id' => null, 'active' => Rubric::ACTIVE_YES])
        ->joinWith('lang')
        ->orderBy(['sort' => SORT_ASC, 'id' => SORT_ASC])
        ->all(); ?>

    <?php if ($rubrics) { ?>
        <div class="icon-category" style="margin-top: 20px;">
            <div class="container">
                <div class="row">
                    <?php foreach ($rubrics as $rubric) { ?>
                        <div class="item col-sm-2 col-xs-6">
                            <a href="<?= Url::to(['index', 'rubric' => $rubric->alias]); ?>">
                                <?php $mainImage = $rubric->icon;
                                if ($mainImage) {
                                    echo Yii::$app->thumbnail->img($mainImage, ['thumbnail' => ['width' => 90, 'height' => 90]]);
                                } else {
                                    echo Yii::$app->thumbnail->placeholder(['width' => 90, 'height' => 90, 'text' => '90x90']);
                                } ?>
                                <p><?= $rubric->title; ?></p>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } else { ?>
    <?php $subrubrics = $choosenRubric->activeSubrubrics; ?>

    <div class="icon-category text-sub" style="margin-top: 20px;">
        <div class="container">
            <ul class="breadcrumb row">
                <li><a href="<?= Url::to(['index']); ?>"><?= Yii::t('advertisement', 'All rubrics'); ?></a></li>
                <?php if ($parents) { ?>
                    <?php for ($i = (count($parents)-1); $i >= 0; $i--) { ?>
                        <li>
                            <a href="<?= Url::to(['index', 'rubric' => $parents[$i]->alias]); ?>">
                                <?= $parents[$i]->title; ?>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
                <li><?= $choosenRubric->title; ?></li>
            </ul>
            <?php if ($subrubrics) { ?>
                <ul class="row">
                    <?php foreach ($subrubrics as $subrubric) { ?>
                        <li class="col-sm-2">
                            <a href="<?= Url::to(['index', 'rubric' => $subrubric->alias]); ?>">
                                <p><?= $subrubric->title; ?></p>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
    </div>
<?php } ?>