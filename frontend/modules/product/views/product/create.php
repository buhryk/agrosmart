<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\rubric\models\Rubric;
use frontend\assets\NewAdvertisementAsset;
use backend\modules\commondata\models\Currency;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\helpers\Html;
use frontend\modules\product\models\Advertisement;
use frontend\modules\product\models\AdvertisementForm;
use backend\modules\commondata\models\Region;
use vova07\imperavi\Widget;

NewAdvertisementAsset::register($this);


$pageH1 = Yii::t('advertisement', 'Add product/service');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('advertisement', 'Products and services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $pageH1;
$action = Yii::$app->controller->action->id;
$getCityUrl = \yii\helpers\Url::to(['/city-list/index']);
$classname = \yii\helpers\StringHelper::basename(get_class($model));
$currentUser = Yii::$app->user->identity;
$oblastList = Region::find()->all();
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<?php if (Yii::$app->user->isGuest) { ?>
    <ul class="ap-choose">
        <li class="ap-choose__item <?= $action == 'create-as-private-person' ? 'active' : ''; ?>">
            <a href="<?= Url::to(['create-as-company']); ?>"><?= Yii::t('advertisement', 'company'); ?></a>
        </li>
        <li class="ap-choose__item <?= $action == 'create-as-company' ? 'active' : ''; ?>">
            <a href="<?= Url::to(['create-as-private-person']); ?>"><?= Yii::t('advertisement', 'private person'); ?></a>
        </li>
    </ul>
<?php } ?>

<div class="ap-wrapper register-user">
    <div class="ap-container">

        <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'ap-company-form clearfix',
                'onsubmit' => 'return false',
                'id' => 'advertisement-form',
                'data-url' => isset($company) ? Url::to(['create-as-company']) : Url::to(['create-as-private-person']),
                'enctype' => 'multipart/form-data'
            ]
        ]); ?>

        <h2 class="ap-prod-title"><?= Yii::t('advertisement', 'Add advertisement'); ?></h2>

        <?php if (isset($user) && $user) { ?>
            <div class="row">
                <div class="col-sm-6 ap-form-contacts">
                    <p class="ap-form-contacts__title"><?= Yii::t('advertisement', 'Contact details'); ?>*:</p>
                    <?php echo Html::activeTextInput($user, 'name', [
                        'placeholder' => Yii::t('advertisement', 'Name'),
                        'class' => 'form-control ap-form-contacts__input'
                    ]); ?>
                    <div class="help-block form-error-block custom-error-block" id="error-name"></div>

                    <?php echo Html::activeTextInput($user, 'surname', [
                        'placeholder' => Yii::t('advertisement', 'Surname'),
                        'class' => 'form-control ap-form-contacts__input'
                    ]); ?>
                    <div class="help-block form-error-block custom-error-block" id="error-surname"></div>

                    <?php echo Html::activeTextInput($user, 'email', [
                        'placeholder' => 'Email',
                        'class' => 'form-control ap-form-contacts__input',
                        'type' => 'email'
                    ]); ?>
                    <div class="help-block form-error-block custom-error-block" id="error-email"></div>

                    <?php echo Html::activeTextInput($user, 'phone', [
                        'placeholder' => Yii::t('advertisement', 'Phone'),
                        'class' => 'form-control ap-form-contacts__input'
                    ]); ?>
                    <div class="help-block form-error-block custom-error-block" id="error-phone"></div>

                    <?= $form->field($user, 'oblast_id')->widget(Select2::className(), [
                        'data' => ArrayHelper::map($oblastList, 'id', 'title'),
                        'options' => ['placeholder' => Yii::t('users', 'Выберите область')],

                    ])->label(false); ?>
                    <div class="help-block form-error-block custom-error-block" id="error-oblast_id"></div>
                </div>
            </div>
        <?php } ?>

        <?php if (isset($company) && $company) { ?>
            <div class="row">
                <div class="col-sm-6 ap-form-contacts">
                    <p class="ap-form-contacts__title"><?= Yii::t('advertisement', 'Contact details'); ?>*:</p>
                    <div class="help-block form-error-block custom-error-block" id="error-author_id"></div>
                    <?php $currentUser = Yii::$app->user->identity; ?>
                    <?php echo Html::activeTextInput($model, 'author_id', [
                        'type' => 'hidden',
                        'id' => 'author_id',
                        'value' => $currentUser->id
                    ]); ?>
                    <label for="authorized">
                        <input type="radio" id="authorized" class="select-author" name="<?= $classname . '[publish_from]'; ?>"
                               value="<?= AdvertisementForm::PUBLISH_FROM_AUTHOR; ?>" data-role="none" checked="checked">
                        <?php $username = '';
                        if ($currentUser->name) {
                            $username .= $currentUser->name;
                        }
                        if ($currentUser->surname) {
                            $username .= ($username ? ' ' : '') . $currentUser->surname;
                        } ?>
                        <?= Yii::t('advertisement', 'advertisement author'); ?>&nbsp;(<?= $username; ?>)
                    </label>
                    <div class="clearfix"></div>

                    <?php if ($otherCompanyUsers) { ?>
                        <label for="another">
                            <input type="radio" id="another" class="select-author" name="<?= $classname . '[publish_from]'; ?>"
                                   value="<?= AdvertisementForm::PUBLISH_FROM_OTHER_USER; ?>">
                            <?= Yii::t('advertisement', 'other user'); ?>
                        </label>
                        <?php echo Html::dropDownList($classname.'[from_user_id]', null, ArrayHelper::map($otherCompanyUsers, 'id', 'fullname'), [
                            'class' => 'ap-select-heading slct-w50-ib hidden',
                            'id' => 'company-users-dropdown',
                            'data-role' => 'none',
                            'prompt' => '...'
                        ]); ?>
                        <div class="clearfix"></div>
                    <?php } ?>

                    <?php if ($subsidiaries) { ?>
                        <label for="subsidiary">
                            <input type="radio" id="subsidiary" class="select-author" name="<?= $classname . '[publish_from]'; ?>"
                                   value="<?= AdvertisementForm::PUBLISH_FROM_SUBSIDIARY; ?>">
                            <?= Yii::t('advertisement', 'subsidiary'); ?>
                        </label>
                        <?php echo Html::dropDownList($classname.'[from_subsidiary_id]', null, ArrayHelper::map($subsidiaries, 'id', 'info'), [
                            'class' => 'ap-select-heading slct-w50-ib hidden',
                            'id' => 'company-subsidiaries-dropdown',
                            'data-role' => 'none',
                            'prompt' => '...'
                        ]); ?>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

        <div class="row">
            <div class="col-sm-6 ap-company-heading">
            <p class="ap-company-heading__title"><?= Yii::t('advertisement', 'Phones'); ?>*:</p>
                <div class="help-block form-error-block custom-error-block" id="error-phones"></div>
                <?php echo Html::activeTextInput($model, 'phones', [
                    'placeholder' => Yii::t('advertisement', 'Phone numbers separated by commas'),
                    'style' => 'width: 100%;',
                    'class' => 'form-control',
                    'label' => false,
                    'value' => $currentUser ? $currentUser->phone : ''
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 ap-company-heading" id="rubrics-container">
                <p class="ap-company-heading__title"><?= Yii::t('advertisement', 'Choose rubric'); ?>*:</p>
                <div class="help-block form-error-block custom-error-block" id="error-rubric_id"></div>
                <?php echo Html::activeTextInput($model, 'rubric_id', [
                    'type' => 'hidden',
                    'id' => 'rubric_id',
                    'data-url-get-rubric-measurements' => Url::to(['get-rubric-measurements']),
                    'data-url' => Url::to(['get-rubric-subrubrics'])
                ]); ?>
                <select class="ap-select-heading select-rubric-dropdownlist" id="heading" data-level="1">
                    <option>...</option>
                    <?php $rubrics = Rubric::find()->where(['parent_id' => null])->joinWith('lang')->all(); ?>
                    <?php foreach ($rubrics as $rubric) { ?>
                        <option value="<?= $rubric->primaryKey; ?>"><?= $rubric->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 ap-company-heading">
                <div class="ap-prod-type col-sm-12">
                    <p class="ap-prod-type__title"><?= Yii::t('advertisement','Type of product/service'); ?>:</p>
                    <?= $form->field($model, 'type')->dropDownList(Advertisement::getTypes(), [
                        'class' => 'ap-select-heading slct-w50-ib'
                    ])->label(false); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 ap-form-rubric">
                <p class="ap-form-rubric__title"><?= Yii::t('advertisement','Title'); ?>*:</p>
                <?php echo Html::activeTextInput($model, 'title', [
                    'class' => 'ap-form-rubric__input',
                    'label' => false
                ]); ?>
                <div class="help-block form-error-block custom-error-block" id="error-title"></div>

                <span class="ap-form-rubric__left-chars">
                    <?= Yii::t('advertisement', 'Left'); ?>&nbsp;
                    <span style="count-left-signs-title">70</span>&nbsp;
                    <?= Yii::t('advertisement', 'signs'); ?>
                </span>
            </div>
            <div class="col-sm-6 ap-form-date pd30">
                <span class="ap-form-date__title"><?= Yii::t('advertisement', 'Active to'); ?>:</span>
                <div style="clear: both"></div>
                <?php echo DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'active_to',
                    'options' => ['placeholder' => '...', 'style' => 'width: 130px;', 'disabled'=> 'disabled'],
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'format' => 'dd/mm/yyyy',
                        'todayHighlight' => true
                    ]
                ]); ?>

                <input type="checkbox" id="until" name="<?= $classname . '[without_active_to]'; ?>" value="1" checked="checked">
                <label for="until" id="style-for-until-checkbox"><?= Yii::t('advertisement', 'until date to'); ?></label>
                <div class="help-block form-error-block custom-error-block" id="error-active-to"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 ap-form-text">
                <p class="ap-form-text__title"><?= Yii::t('advertisement', 'Advertisement text'); ?>:</p>

                <?= $form->field($model, 'text')->widget(Widget::className(), [
                    'settings' => [
                        'language' => \Yii::$app->language,
                        'minHeight' => 300,
                        'plugins' => [
                            'fullscreen',
                        ]
                    ]
                ])->label(false) ?>

                <span class="ap-form-text__left-chars">
                        <?= Yii::t('advertisement', 'Left'); ?>&nbsp;
                        <span class="count-left-signs-text">2500</span>&nbsp;
                    <?= Yii::t('advertisement', 'signs'); ?>
                    </span>
            </div>
        </div>
        <div class="row ap-form-photos">
            <p class="ap-form-photos__title"><?= Yii::t('advertisement', 'Photos'); ?>:</p>
            <div class="help-block form-error-block custom-error-block" id="error-image1"></div>
            <div class="help-block form-error-block custom-error-block" id="error-image2"></div>
            <div class="help-block form-error-block custom-error-block" id="error-image3"></div>
            <div class="help-block form-error-block custom-error-block" id="error-image4"></div>
            <div class="help-block form-error-block custom-error-block" id="error-image5"></div>
            <div class="help-block form-error-block custom-error-block" id="error-image6"></div>
            <div class="col-sm-4 ap-form-photo">
                <img class="square-add-button" src="<?= Yii::getAlias('@web')?>/images/square-add-button.png"
                     alt="square-add-button">
                <input type="file" name="<?= $classname . '[image1]'; ?>" id="image1">
                <img class="ap-form-photo__close" src="<?= Yii::getAlias('@web')?>/images/important-close.png"
                     alt="close">
                <label class="image1-label" for="image1"></label>
                <div class="item item1"></div>
            </div>
            <div class="col-sm-4 ap-form-photo mb-hidden">
                <img class="square-add-button" src="<?= Yii::getAlias('@web')?>/images/square-add-button.png"
                     alt="square-add-button">
                <input type="file" name="<?= $classname . '[image2]'; ?>" id="image2">
                <img class="ap-form-photo__close" src="<?= Yii::getAlias('@web')?>/images/important-close.png"
                     alt="close">
                <label class="image2-label" for="image2"></label>
                <div class="item item2"></div>
            </div>
            <div class="col-sm-4 ap-form-photo mb-hidden">
                <img class="square-add-button" src="<?= Yii::getAlias('@web')?>/images/square-add-button.png"
                     alt="square-add-button">
                <input type="file" name="<?= $classname . '[image3]'; ?>" id="image3">
                <img class="ap-form-photo__close" src="<?= Yii::getAlias('@web')?>/images/important-close.png"
                     alt="close">
                <label class="image3-label" for="image3"></label>
                <div class="item item3"></div>
            </div>
            <div class="col-sm-4 ap-form-photo mb-hidden">
                <img class="square-add-button" src="<?= Yii::getAlias('@web')?>/images/square-add-button.png"
                     alt="square-add-button">
                <input type="file" name="<?= $classname . '[image4]'; ?>" id="image4">
                <img class="ap-form-photo__close" src="<?= Yii::getAlias('@web')?>/images/important-close.png"
                     alt="close">
                <label class="image4-label" for="image4"></label>
                <div class="item item4"></div>
            </div>
            <div class="col-sm-4 ap-form-photo mb-hidden">
                <img class="square-add-button" src="<?= Yii::getAlias('@web')?>/images/square-add-button.png"
                     alt="square-add-button">
                <input type="file" name="<?= $classname . '[image5]'; ?>" id="image5">
                <img class="ap-form-photo__close" src="<?= Yii::getAlias('@web')?>/images/important-close.png"
                     alt="close">
                <label class="image5-label" for="image5"></label>
                <div class="item item5"></div>
            </div>
            <div class="col-sm-4 ap-form-photo mb-hidden">
                <img class="square-add-button" src="<?= Yii::getAlias('@web')?>/images/square-add-button.png"
                     alt="square-add-button">
                <input type="file" name="<?= $classname . '[image6]'; ?>" id="image6">
                <img class="ap-form-photo__close" src="<?= Yii::getAlias('@web')?>/images/important-close.png"
                     alt="close">
                <label class="image6-label" for="image6"></label>
                <div class="item item6"></div>
            </div>
        </div>
        <div class="row ap-form-location">
            <p class="ap-form-location__title"><?= Yii::t('advertisement', 'Geo position'); ?>*:</p>
            <div>
                <div class="help-block form-error-block custom-error-block" id="error-city_id"></div>
                <div class="help-block form-error-block custom-error-block" id="error-address"></div>
                <div class="clearfix"></div>
            </div>
            <div class="one-address-container">
                <div class="col-sm-6 region">
                    <?= $form->field($model, 'city_id')->widget(Select2::className(), [
                        'options' => [
                            'placeholder' => Yii::t('advertisement', 'Enter city name')
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 2,
                            'ajax' => [
                                'url' => $getCityUrl,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ])->label(false); ?>
                </div>
                <div class="col-sm-6 region city">
                    <?php echo Html::activeTextInput($model, 'address', [
                        'placeholder' => Yii::t('advertisement', 'Enter address'),
                        'class' => 'input-new-advertisement-address',
                    ]) ?>
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
        <div class="row ap-form-price">
            <p class="ap-form-price__title"><?= Yii::t('advertisement', 'Price'); ?>*:</p>
            <div class="help-block form-error-block custom-error-block" id="error-price"></div>
            <div class="col-sm-3 price">
                <?php echo Html::activeTextInput($model, 'price', [
                    'type' => 'number',
                    'placeholder' => Yii::t('advertisement', 'Input price'),
                    'style' => 'width: 100%;',
                    'class' => 'form-control',
                    'label' => false,
                    'disabled' =>'disabled'
                ]) ?>
            </div>
            <div class="col-sm-3">
                <?php echo $form->field($model, 'currency')->label(false)->dropDownList(ArrayHelper::map(
                    Currency::find()->where(['status' => Currency::ACTIVE_YES])->all(), 'id', 'code'))
                ?>
            </div>
            <div class="col-sm-6" style="padding-top: 10px;">
                <input type="checkbox" id="price" name="<?= $classname . '[contract_price]'; ?>" value="1" checked="checked">
                <label for="price"><?= Yii::t('advertisement', 'contract price'); ?></label>
            </div>
        </div>
        <div class="row ap-form-volume">
            <p class="ap-form-volume__title"><?= Yii::t('advertisement', 'Volume'); ?>:</p>
            <div class="col-sm-3 volume">
                <?= $form->field($model, 'volume')->label(false)->textInput([
                    'type' => 'number',
                    'placeholder' => Yii::t('advertisement', 'The number without commas and dots'),
                    'style' => 'width: 100%;'
                ]); ?>
            </div>
            <div class="col-sm-3">
                <?php $measurements = \backend\modules\rubric\models\RubricMeasurementCategory::find()
                    ->joinWith('lang')
                    ->all();
                $data = [];
                foreach ($measurements as $measurement) {
                    $data[$measurement->title] = ArrayHelper::map($measurement->measurements, 'id', 'short_title');
                } ?>

                <?= $form->field($model, 'measurement_id')->dropDownList([],['id' => 'choose-measurement'])->label(false); ?>

                <div class="help-block form-error-block custom-error-block" id="error-measurement-id"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 ap-form-agreement">
                <p class="ap-form-agreement__title">
                    <?= Yii::t('advertisement', 'Confirm your consent to the processing of personal data'); ?>*:
                </p>
                <input type="checkbox" id="agree" name="<?= $classname . '[is_agree]'; ?>" value="1">

                <label for="agree">
                    <?= Yii::t('advertisement', 'I agree to the service terms of use, as well as the transmission and processing of my personal data'); ?>
                </label>
                <div class="help-block form-error-block custom-error-block" id="error-is_agree"></div>
            </div>
        </div>
        <div class="row">
            <div class="ap-form-submit">
                <!--<button class="ap-form-submit__preview"><?/*= Yii::t('advertisement', 'preview'); */?></button>-->
                <button class="ap-form-submit__publish" id="submit-advertisement-form"><?= Yii::t('advertisement', 'publish'); ?></button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?=$this->registerJs("$('#signupuserform-phone, #registercompanyform-phones').mask('099-999-99-99');", \yii\web\View::POS_READY)
?>