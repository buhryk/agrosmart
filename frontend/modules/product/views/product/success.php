<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = Yii::t('advertisement', 'Add product/service');
$this->params['breadcrumbs'][] = ['label' => Yii::t('advertisement', 'Products and services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bread">
    <h1><?= $this->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="ap-success-container">
    <p class="ap-success-container__message"><?= Yii::t('advertisement', 'Your message was accepted'); ?></p>
    <div class="ap-success-info">
        <p class="ap-success-info__title"><?= Yii::t('advertisement', 'After moderation it will be at link'); ?>&nbsp;-</p>
        <?php $href = Url::to(['view', 'slug' => $model->alias]); ?>
        <?php $host = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']; ?>
        <a class="ap-success-info__link" href="<?= $href; ?>"><?= $host . $href; ?></a>
        <p class="ap-success-info__message">Если ваше обьявление не нарушает <a href="<?=Url::to(['/page/pravila-ispolzovaniya']) ?>">Правила использования портала и Правила размещения обьявлений</a>, через 10 минут мы опубликуем его в общей ленте.</p>
    </div>
</div>