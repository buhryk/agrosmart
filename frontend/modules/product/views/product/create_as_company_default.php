<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$pageH1 = Yii::t('advertisement', 'Add product/service');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);


$this->params['breadcrumbs'][] = ['label' => Yii::t('advertisement', 'Products and services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $pageH1;
?>
<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<h2 class="ap-prod-title"><?= Yii::t('advertisement', 'Choose advertisement type'); ?></h2>
<ul class="ap-choose">
    <li class="ap-choose__item">
        <a href="<?= Url::to(['create-as-company']); ?>"><?= Yii::t('advertisement', 'company'); ?></a>
    </li>
    <li class="ap-choose__item">
        <a href="<?= Url::to(['create-as-private-person']); ?>"><?= Yii::t('advertisement', 'private person'); ?></a>
    </li>
</ul>
<div class="ap-wrapper">
    <div class="ap-container">
        <div class="ap-info-text">
            <p class="ap-info-text__item"><?= Yii::t('advertisement', 'Чтобы публиковать объявления, нужно создать профиль компании!'); ?></p>
            <p class="ap-info-text__item"><?= Yii::t('advertisement', 'Вы сможете управлять вашими объявлениями, публиковать цены,'); ?></p>
            <p class="ap-info-text__item"><?= Yii::t('advertisement', 'а также использовать все инструменты ЛК'); ?></p>
        </div>
        <a href="<?php echo Url::to(['/site/signup']); ?>" class="btn-green create-company-link">
            <?php echo Yii::t('advertisement', 'create company profile'); ?>
        </a>
    </div>
</div>