<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use frontend\modules\elected\models\Elected;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\rating\StarRating;
use frontend\models\Company;
use frontend\modules\complaint\models\Complaint;
use frontend\components\tool\Image;

$rubric = $model->rubric;
$images = $model->images;
$user = $model->author;
$currentUser = !Yii::$app->user->isGuest ? Yii::$app->user->identity : null;
$company = $model->company;

$this->title = $model->getTitle().' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $model->getTitle().' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['view', 'slug' =>$model->alias])]);
$parentsRubrics = [];

$parentRubric = $rubric ? $rubric->parent : null;
while ($parentRubric !== null) {
    array_unshift($parentsRubrics, ['label' => $parentRubric->title, 'url' => ['index', 'rubric' => $parentRubric->alias]]);
    $parentRubric = $parentRubric->parent;
}

$this->params['breadcrumbs'][] = ['label' => Yii::t('advertisement', 'Products and services'), 'url' => ['index']];
foreach ($parentsRubrics as $parentRubric) {
    $this->params['breadcrumbs'][] = $parentRubric;
}
if ($rubric) {
    $this->params['breadcrumbs'][] = ['label' => $rubric->title, 'url' => ['index', 'rubric' => $rubric->alias]];
}
$this->params['breadcrumbs'][] = $model->getTitle();
$electedAdvertisementsIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedAdvertisements, 'id') : [];
$electedCompaniesIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedCompanies, 'id') : [];
$electedUsersIds = $currentUser ? ArrayHelper::getColumn($currentUser->electedUsers, 'id') : [];
$savedAddresses = $currentUser ? ArrayHelper::getColumn($currentUser->savedAddresses, 'address') : [];
$messageAddToElected = Yii::t('common', 'Add to elected');
$messageRemoveFromElected = Yii::t('common', 'Remove from elected');
?>

<div class="bread">
    <h1><?= $model->getTitle(); ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="bg-product">
    <div class="container one-product-container">
        <div class="row">
            <div class="col-sm-9 top-marclear">
                <div class="row product-sale">
                    <div class="col-sm-5">
                        <?= isset($images[0]) ? Yii::$app->thumbnail->img($images[0]->path, ['thumbnail' => ['width' => 345, 'height' => 250]], ['alt' => $model->title]) :
                            Html::img('/images/product_345x250.jpg', ['alt' => $model->title]);
                        ?>
                    </div>
                    <div class="col-sm-7">
                        <span class="product-id">ID: <?= $model->primaryKey; ?></span>
                        <div class="product-add-info pull-right text-right">
                            <span class="product-add-date text-right"><?= date('d.m.Y',  $model->created_at); ?></span>|<span class="product-add-time"><?= Yii::t('advertisement', 'Added at', ['time' => date('H:i', $model->created_at)]); ?></span>
                            <br />
                            <span class="product-actual-date">
                                <?php if ($model->active_to) { ?>
                                    <?= Yii::t('advertisement', 'Advertisement active to', [
                                        'date' => date('d.m.Y', $model->active_to)
                                    ]); ?>
                                <?php } else { ?>
                                    <?= Yii::t('advertisement', 'until date to'); ?>
                                <?php } ?>
                            </span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-ib">
                                <p class="product-price">
                                    <span>  <small><?=Yii::t('advertisement', 'Цена') ?></small> :  <?= $model->detailPrice; ?>
                                </p>
                            </div>
                            <div class="col-sm-6 col-ib text-right">
                                <?php if ($model->volume) { ?>
                                    <span class="product-weight">
                                        <span>  <?=Yii::t('advertisement', 'Обьем') ?>: </span>	&nbsp;	&nbsp;  <?= $model->detailVolume; ?>
                                    </span>
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6">
                                <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->id != $model->author_id) {
                                    echo Html::a(Yii::t('common', 'Write'),
                                        ['/cabinet/dialog/create-message-modal', 'receiver' => $model->author_id],
                                        ['class' => 'btn-green modalButton']);
                                } ?>
                            </div>
                            <div class="col-sm-6">
                                <!--<button type="submit" class="btn-green">позвонить</button>-->
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6 prod-link1">
                                <?php if (!Yii::$app->user->isGuest) { ?>
                                    <a class="prod-link change-elected <?= in_array($model->primaryKey, $electedAdvertisementsIds) ?
                                        'remove-from-elected' : 'add-to-elected'; ?>"
                                       data-type="<?= Elected::ALIAS_ADVERTISEMENT; ?>"
                                       data-record_id="<?= $model->primaryKey; ?>"
                                       data-message-add="<?= $messageAddToElected; ?>"
                                       data-message-remove="<?= $messageRemoveFromElected; ?>"
                                    >
                                        <?= in_array($model->primaryKey, $electedAdvertisementsIds) ? $messageRemoveFromElected : $messageAddToElected; ?>
                                    </a>
                                <?php } ?>
                            </div>
                            <div class="col-sm-6 prod-link3">
                                <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->id != $model->author_id) { ?>
                                    <?php $checkComplaintExist = Complaint::find()->where([
                                        'from_user_id' => Yii::$app->user->identity->id,
                                        'record_id' => $model->primaryKey,
                                        'table_name' => Complaint::ALIAS_ADVERTISEMENT,
                                        'status' => Complaint::STATUS_NEW
                                    ])->one(); ?>
                                    <?php if (!$checkComplaintExist) {
                                        echo Html::a(Yii::t('common', 'Complaint'),
                                            ['/complaint/complaint/create',
                                                'type' => Complaint::ALIAS_ADVERTISEMENT,
                                                'record_id' => $model->id,
                                            ],
                                            ['class' => 'prod-link modalButton']);
                                    } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <?php if ($user) { ?>
                            <a href="<?= Url::to(['/user/user/view', 'id' => $user->id]); ?>"
                               style="display: block;font-size: 19px;font-weight: 700; color: black; text-decoration: none;"
                            >
                                <?= $user->fullname; ?>
                            </a>
                            <table class="table advertasing-contacts-table">
                                <tbody>
                                <tr>
                                    <td><?= Yii::t('advertisement', 'Phones'); ?>:</td>
                                    <td><?= $model->phones; ?></td>
                                </tr>
                                <tr>
                                    <td><?= Yii::t('advertisement', 'Тип'); ?>:</td>
                                    <td><?= $model->detailType; ?></td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td><?= $model->email; ?></td>
                                </tr>
                                <?php $location = $model->location; ?>
                                <?php if ($location) { ?>
                                    <td><?= Yii::t('advertisement', 'Address'); ?>:</td>
                                    <td>
                                        <div><?= $location; ?></div>
                                        <?php if (!Yii::$app->user->isGuest && !in_array($location, $savedAddresses)) { ?>
                                            <a class="standart-link add-address-to-saved" data-address="<?= $location; ?>">
                                                <?= Yii::t('common', 'Remember address'); ?>
                                            </a>
                                        <?php } ?>
                                    </td>
                                <?php } ?>
                                </tbody>
                            </table>
                        <?php } ?>
                    </div>
                    <div class="col-sm-12">
                        <p><?=$model->getText(); ?></p>
                    </div>
                    <?php if ($company) { ?>
                        <div class="col-sm-2 norm-line">
                            <?php $companyLogo = $company->logotype ? ('/'.$company->logotype) : '/images/user.jpg'; ?>
                            <?=  Html::img(Image::resize($companyLogo, 120, 81));
                            ?>
                        </div>
                        <div class="col-sm-10 norm-line" >
                            <div><?php echo \yii\helpers\HtmlPurifier::process($company->getShortDescription()); ?></div>
                            <?php echo StarRating::widget([
                                'name' => 'rating_company_'.$model->primaryKey,
                                'value' => $company->ratio,
                                'pluginOptions' => [
                                    'step' => 0.1,
                                    'readonly' => true,
                                    'showClear' => false,
                                    'showCaption' => false,
                                ]
                            ]); ?>

                            <?php if ($company->is_confirmed): ?>
                                <span class="rdl-line">|</span>
                                <span><?= Yii::t('common', 'Status'); ?>:&nbsp;<span class="red-al"><?= $company->isConfirmedDetail; ?></span>
                            <?php endif; ?>
                            </span>
                            <span class="rdl-line">|</span>
                            <a href="<?= Url::to(['/company/company/view', 'id' => $company->primaryKey]); ?>" class="link-g">
                                <?= Yii::t('common', 'DETAIL'); ?>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="col-sm-12 norm-line">
                        <?php if (count($images) > 1) { ?>
                            <div class="owl-product">
                                <?php for ($i = 1; $i < count($images); $i++) { ?>
                                    <div class="item">
                                        <?= Yii::$app->thumbnail->img($images[$i]->path, [
                                            'thumbnail' => ['width' => 345, 'height' => 250]
                                        ]); ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <!--
                        <p class="text-center"><?= Yii::t('advertisement', 'views'); ?>: <span><?= $model->views_count; ?></span></p>
                        -->
                    </div>
                </div>

                <?php if ($company) { ?>
                    <?php if (!empty($otherCompanyAdvertisements = $model->getOtherCompanyActiveAdvertisements(3))) { ?>
                        <div class="row view-product another-product">
                            <div class="title-black text-center">
                                <?= Yii::t('advertisement', 'Other company advertisements'); ?>
                            </div>
                            <?php foreach ($otherCompanyAdvertisements as $one) { ?>
                                <?= $this->render('_advertisement', [
                                    'model' => $one,
                                    'user' => $user,
                                    'company' => $company,
                                    'electedCompaniesIds' => $electedCompaniesIds,
                                    'electedUsersIds' => $electedUsersIds,
                                    'messageAddToElected' => $messageAddToElected,
                                    'messageRemoveFromElected' => $messageRemoveFromElected,
                                    'savedAddresses' => $savedAddresses
                                ]); ?>
                            <?php } ?>
                            <div class="text-center">
                                <a href="<?= Url::to(['/company/company/products', 'id' => $company->primaryKey]); ?>" class="btn-green load-more">
                                    <?= Yii::t('common', 'View all'); ?>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <?php if (!empty($otherUserAdvertisements = $model->getOtherUserActiveAdvertisements(3))) { ?>
                        <div class="row view-product another-product">
                            <div class="title-black text-center">
                                <?= Yii::t('advertisement', 'Other author advertisements'); ?>
                            </div>
                            <?php foreach ($otherUserAdvertisements as $one) { ?>
                                <?= $this->render('_advertisement', [
                                    'model' => $one,
                                    'user' => $user,
                                    'company' => $company ? $company : null,
                                    'electedCompaniesIds' => $electedCompaniesIds,
                                    'electedUsersIds' => $electedUsersIds,
                                    'messageAddToElected' => $messageAddToElected,
                                    'messageRemoveFromElected' => $messageRemoveFromElected,
                                    'savedAddresses' => $savedAddresses
                                ]); ?>
                            <?php } ?>
                            <div class="text-center">
                                <a href="<?= Url::to(['/user/user/products', 'id' => $user->primaryKey]); ?>" class="btn-green load-more">
                                    <?= Yii::t('common', 'View all'); ?>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>

                <?php if (!empty($similarAdvertisements = $model->getSimilarActiveAdvertisements(2))) { ?>
                    <div class="row view-product similar-products">
                        <div class="title-black text-center">
                            <?= Yii::t('advertisement', 'Similar advertisements'); ?>
                        </div>
                        <?php foreach ($similarAdvertisements as $one) { ?>
                            <?= $this->render('_advertisement', [
                                'model' => $one,
                                //'user' => $one->author,
                                'electedCompaniesIds' => $electedCompaniesIds,
                                'electedUsersIds' => $electedUsersIds,
                                'messageAddToElected' => $messageAddToElected,
                                'messageRemoveFromElected' => $messageRemoveFromElected,
                                'savedAddresses' => $savedAddresses
                            ]); ?>
                        <?php } ?>
                        <div class="text-center">
                            <a href="<?= Url::to(['index', 'rubric' => $rubric ? $rubric->alias : '']); ?>" class="btn-green load-more">
                                <?= Yii::t('common', 'View all'); ?>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="col-sm-3 sidebar-banner">
                <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
            </div>

            <div class="col-sm-12">
                <?php if ($viewedAdvertisements) { ?>
                    <div class="row view-product look-product">
                        <div class="title-black text-center"><?= Yii::t('advertisement', 'Viewed advertisements'); ?></div>
                        <div class="row">
                            <?php foreach ($viewedAdvertisements as $one) { ?>
                                <?= $this->render('_viwed_advertisement', [
                                    'model' => $one
                                ]); ?>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>
    </div>
</div>