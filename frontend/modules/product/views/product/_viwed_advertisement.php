<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\StringHelper;

$images = $model->images;
$location = $model->location;
?>

<div class="col-sm-3">
    <div class="sales-product sales-product-watched clear">
        <div class="">
            <a href="<?= Url::to(['view', 'slug' => $model->alias]); ?>">
                <figure class="effect-chico">
                    <?= isset($images[0]) ? Yii::$app->thumbnail->img($images[0]->path, ['thumbnail' => ['width' => 268, 'height' => 189]], ['alt' => $model->getTitle()]) :
                        Html::img('/images/product_268x189.jpg', ['alt' => $model->title]);
                    ?>
                </figure>
            </a>
        </div>
        <div class="">
            <a href="<?= Url::to(['view', 'slug' => $model->alias]); ?>" style="height: 60px; overflow: hidden; text-align: center;">
                <?=StringHelper::truncate($model->getTitle(),  50); ?>

            </a>
            <p class="price"><?= $model->detailPrice; ?></p>
        </div>
    </div>
</div>