<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\modules\complaint\models\Complaint as Model;

$classname = \yii\helpers\StringHelper::basename(get_class($model));
?>

<div class="modal-body">
    <div class="subsidiary-form">
        <div id="advertisement-complaint-errors" style="color: red;"></div>
        <?php $form = ActiveForm::begin([
            'id' => 'form-advertisement-complaint',
            'enableAjaxValidation' => false,
            'options' => [
                'onsubmit' => 'return false',
                'data-url' => Url::to(['create'])
            ]
        ]); ?>

        <?= $form->field($model, 'table_name')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'record_id')->hiddenInput()->label(false); ?>

        <?php foreach (Model::getAllTypes() as $value => $title) { ?>
            <div class="advertisement-complaint-one-checkbox">
                <input type="checkbox" id="until-<?= $value; ?>" name="<?= $classname . '[types][]'; ?>" value="<?= $value; ?>">
                <label for="until-<?= $value; ?>" id="style-for-until-checkbox"><?= $title; ?></label>
            </div>
        <?php } ?>

        <?= $form->field($model, 'text', ['options' => ['style' => 'margin: 25px 0 20px 0;']])
            ->textarea(['style' => 'resize: none;'])
            ->label(Yii::t('common', 'Comment'));
        ?>

        <div class="form-group">
            <?php echo Html::button(Yii::t('users', 'SEND'), [
                'class' => 'btn btn-primary',
                'id' => 'save-complaint',
                'type' => 'button',
                'onclick' => 'return false;'
            ]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>