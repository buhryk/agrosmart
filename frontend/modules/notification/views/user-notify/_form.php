<?php
/**
 * Created by PhpStorm.
 * User: PHP-dveloper
 * Date: 02.03.2017
 * Time: 15:25
 */
use \kartik\select2\Select2;
use \yii\web\JsExpression;

$url = \yii\helpers\Url::to(['/city-list/index']);

?>
    <?=

    $form->field($model, 'city_id')->widget(Select2::className(), [
        'initValueText' => !empty($model->city) ? $model->city->title : '', // set the initial display text
        'options' => ['placeholder' => 'Ведите email пользователя'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new \yii\web\JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],

    ]);

?>
