<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use backend\modules\rubric\models\Rubric;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
use frontend\modules\notification\models\Notification;

$this->title = \Yii::t('cabinet', 'Notification page');
$this->params['breadcrumbs'][] = ['label' => Yii::t('cabinet', 'PRIVATE_CABINET'), 'url' => ['/cabinet/user/profile']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
    "$('#choose-logo').on('click', function() { $('#choose-logo-file').click() });",
    \yii\web\View::POS_READY,
    'my-button-handler'
);
?>

<div class="bread">
    <h1><?= $this->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-sm-3" id="sidebar">
            <div class="panel-group green-sidebar cabinet-sidebar" id="accordion">
                <?php echo \frontend\modules\cabinet\widgets\CabinetMenu::widget()?>
            </div>
        </div>
        <div class="notification-default-index">
            <div class="row">
                <div class="col-md-8">
                    <div class="margin-bottom-10">
                        <div class="important-info ">
                            <img class="important-info__i" src="/images/important-i.png" alt="important-i">
                            <img class="important-info__close" src="/images/important-close.png" alt="important-close">
                            <img class="important-info__hide" src="/images/important-hide.png" alt="important-hide">
                            <div class="important-info-content">
                                <p class="important-info__title">
                                    <?=Yii::t('informing', 'My Account - Notifications') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <table class="table">
                        <thead>
                        <td><?=Yii::t('common', 'Сообщение') ?></td>
                        <td><?=Yii::t('common', 'Дата сообщения') ?></td>
                        <td><?=Yii::t('common', 'Действия') ?></td>
                        </thead>
                        <?php foreach ($models as $model) : ?>
                            <tr class="<?=$model->status == Notification::STATUS_NEW ? 'new' : '' ?>">
                                <td>
                                    <?= $model->message ?>
                                </td>
                                <td>
                                    <?= date('Y-m-d H:i',$model->created_at) ?>
                                </td>
                                <td>
                                    <?php $icon = $model->status == Notification::STATUS_VIEW ? 'glyphicon-eye-close' : 'glyphicon-eye-open';
                                    $title = $model->status == Notification::STATUS_VIEW ? Yii::t('common', 'Отметить как непрочитанное') : Yii::t('common', 'Отметить как прочитанное');
                                        echo \yii\bootstrap\Html::a('<span class="glyphicon '.$icon.'"></span>',
                                            ['view','id' => $model->id],
                                            ['title' => $title]);
                                     ?>
                                    <?php
                                        echo \yii\bootstrap\Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                            ['delete','id' => $model->id],
                                            ['title' => Yii::t('common', 'Удалить')]
                                        );
                                     ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </table>
                    <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages, 'maxButtonCount' => 10]); ?>
                </div>
            </div>
        </div>
    </div>
</div>



