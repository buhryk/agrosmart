<?php

namespace frontend\modules\notification\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property integer $status
 * @property string $message
 * @property string $created_at
 */
class Notification extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 0;
    const STATUS_VIEW = 1;
    const STATUS_CANNOTT_DISABLE =2;

    const TYPE_ADMIN_MASSAGE = 0;
    const TYPE_USER_MESSAGE = 1;
    const TYPE_COMPANY_MESSAGE  = 2;

    public static function getAllTypes(){

        return [

            self::TYPE_USER_MESSAGE => Yii::t('cabinet','User message'),
            self::TYPE_COMPANY_MESSAGE => Yii::t('cabinet','Company massages')

        ];

    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'message'], 'required'],
            [['target_id', 'type', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['created_at'], 'default' , 'value' => function(){
                return time();
            }],
            [['status'],'default' , 'value' => self::STATUS_NEW],
            [['message'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'status' => 'Status',
            'message' => 'Message',
            'created_at' => 'Created At',
        ];
    }

    public static function findAllUserNotification()
    {

        return Notification::find()
            ->where(['type' => Notification::TYPE_USER_MESSAGE, 'target_id'=> \Yii::$app->user->identity->getId()])
            ->orWhere(['type' => Notification::TYPE_COMPANY_MESSAGE, 'target_id' => \Yii::$app->user->identity->getId()])
            ->orWhere(['type' => Notification::TYPE_ADMIN_MASSAGE])

            ->orderBy(['status'=>SORT_ASC, 'created_at'=>SORT_DESC, ])
            ->all();


    }

    public static function findNewUserNotification()
    {
        return Notification::find()
            ->where(['type' => Notification::TYPE_USER_MESSAGE,'target_id'=> \Yii::$app->user->identity->getId(),'status'=>Notification::STATUS_NEW])
            ->orWhere(['type' => Notification::TYPE_COMPANY_MESSAGE,'status' => Notification::STATUS_NEW,'target_id' => \Yii::$app->user->identity->getId()])
            ->orWhere(['type' => Notification::TYPE_ADMIN_MASSAGE])
            ->orderBy(['type'=>SORT_DESC])
            ->all();
    }

}
