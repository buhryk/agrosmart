<?php

namespace frontend\modules\notification\controllers;

use frontend\modules\notification\models\Notification;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `notification` module
 */
class UserNotifyController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $query = Notification::find()
            ->where(['type' => Notification::TYPE_USER_MESSAGE, 'target_id'=> \Yii::$app->user->identity->getId()])
            ->orWhere(['type' => Notification::TYPE_COMPANY_MESSAGE, 'target_id' => \Yii::$app->user->identity->getId()])
            ->orWhere(['type' => Notification::TYPE_ADMIN_MASSAGE])

            ->orderBy(['status'=>SORT_ASC, 'created_at'=>SORT_DESC, ]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index',['models' => $models, 'pages' => $pages]);
    }

    public function actionView($id)
    {

        $model = $this->findModel($id);
        if ($model->status == Notification::STATUS_VIEW) {
            $model->status = Notification::STATUS_NEW;
        } elseif ($model->status == Notification::STATUS_NEW) {
            $model->status = Notification::STATUS_VIEW;
        }
        $model->save();
        \Yii::$app->session->setFlash('info', \Yii::t('common', 'Статус изменен'));

        return $this->redirect('index');
    }

    public function actionDelete($id){

        $this->findModel($id)->delete();

        $this->redirect('index');
    }

    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
