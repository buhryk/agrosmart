<?php
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
?>

<div class="news-search">
    <?php $form = ActiveForm::begin([
        'action' => ['search'],
        'options' => ['id' => 'search-news-form'],
        'method' => 'get',
    ]); ?>

    <div>
        <?php echo Html::activeTextInput($model, 'search', [
            'class' => 'search-input news-search',
            'placeholder' => Yii::t('news', 'search all news'),
            'id' => 'news-search-string'
        ]); ?>
        <button type="submit">
            <?= Html::img('/images/search.png'); ?>
        </button>
    </div>

    <?php ActiveForm::end(); ?>
    <?php $this->registerJs("
            $('#search-news-form').on('submit', function(){
                var searchStringInput = $('#news-search-string'); 
                if (!searchStringInput.val()) {
                    return false;
                }
            });
        ",
        \yii\web\View::POS_READY,
        'my-button-handler'
    ); ?>
</div>