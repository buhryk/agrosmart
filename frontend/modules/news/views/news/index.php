<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\helpers\StringHelper;

$pageH1 = Yii::t('news', 'News');
$this->title = $pageH1 .' | '.Yii::$app->params['titlePrefix'];
$this->registerMetaTag(['name'=>'description', 'content'=> $pageH1.' | '.Yii::$app->params['descriptionPrefix']]);
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['index'])]);
$this->params['breadcrumbs'][] = $pageH1;
?>

<div class="bread">
    <div class="container">
        <h1><?= $pageH1; ?></h1>
    </div>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <?= $this->render('_search_form', ['model' => $searchModel]); ?>
</div>
<div class="container-fluid mt40 half-color-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 news-container" data-content="page_content">
                <?php foreach ($lastNews as $news) { ?>
                    <div class="news-item clearfix">
                        <div class="col-xs-12 news-item-header">
                            <a href="<?= Url::to(['view', 'slug' => $news->alias]); ?>" style="text-decoration: none">
                                <h3 class="news-item__title"><?= $news->title; ?></h3>
                            </a>
                            <i class="news-item__date"><?= date('d.m.Y', $news->published_at); ?></i>
                        </div>
                        <div class="col-sm-6">
                            <?php $mainImage = $news->image; ?>
                            <?php if ($mainImage) { ?>
                                <?php echo Yii::$app->thumbnail->img($mainImage->path,
                                    ['thumbnail' => ['width' => 262, 'height' => 262]],
                                    ['alt' => $mainImage->alt ? $mainImage->alt : $news->title]
                                ); ?>
                            <?php } else { ?>
                                <?php echo Yii::$app->thumbnail->placeholder(
                                    ['width' => 262, 'height' => 262, 'text' => '262x262'],
                                    ['alt' => $news->title]
                                ); ?>
                            <?php } ?>
                        </div>
                        <div class="col-sm-6">
                            <p><?= StringHelper::truncate($news->short_description, 220); ?></p>
                            <a href="<?= Url::to(['view', 'slug' => $news->alias]); ?>" class="btn-green">
                                <?= Yii::t('common', 'further') . '...'; ?>
                            </a>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-xs-12 col-md-7">
                    <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages, 'maxButtonCount' => 3, 'registerLinkTags' => true,]); ?>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="hot-news">
                    <?php foreach ($categories as $category) { ?>
                        <div class="hot-news-item">
                            <a class="hot-news-item__title" href="<?= Url::to(['category', 'slug' => $category->alias]); ?>">
                                <?= $category->title; ?>
                            </a>
                            <!--<p class="hot-news-item__subtitle">Вы понимаете, где выгоднее продать/купить</p>-->
                            <p class="hot-news-item__text" style="margin-top: 10px;">
                                <?= $category->short_description; ?>
                            </p>
                        </div>
                    <?php } ?>
                </div>
                <div class="calendar">
                    <?php foreach ($lastEvents as $event) { ?>
                        <div class="calendar-item">
                            <h5 class="calendar-item__title" style="font-size: 22px;">
                                <?= $event->formattedDateRange; ?>
                            </h5>
                            <?php $mainImage = $event->image; ?>
                            <?php if ($mainImage) { ?>
                                <?php echo Yii::$app->thumbnail->img($mainImage->path,
                                    ['thumbnail' => ['width' => 270, 'height' => 100]],
                                    ['class' => 'calendar-item__img', 'alt' => $mainImage->alt ? $mainImage->alt : $event->title]
                                ); ?>
                            <?php } else { ?>
                                <?php echo Yii::$app->thumbnail->placeholder(
                                    ['width' => 270, 'height' => 100, 'text' => '270x100'],
                                    ['class' => 'calendar-item__img', 'alt' => $event->title]
                                ); ?>
                            <?php } ?>
                            <a href="<?= Url::to(['/event/event/view', 'slug' => $event->alias]); ?>" class="calendar-item__link">
                                <?= $event->title; ?>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-3 sidebar-banner m0">
                <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
            </div>
        </div>
    </div>
</div>