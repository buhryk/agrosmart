<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\helpers\Html;

if (!$this->title) {
    $this->title = $model->title. ' | '.Yii::$app->params['titlePrefix'];
    $this->registerMetaTag(['name'=>'description', 'content'=> $model->title.' | '.Yii::$app->params['descriptionPrefix']]);
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('news', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['view', 'slug' => $model->alias])]);
?>

<div class="bread">
    <h1><?= $model->title ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9 mt45">
            <div class="full-news-item clearfix">
                <div class="short-info">
                    <div>
                        <i class="short-info__date"><?= date('d.m.Y', $model->published_at); ?></i>
                        <i class="short-info__time"><?= date('H:i', $model->published_at); ?></i>
                        <img class="latest-news-item__watched-img" src="/images/eye.png" alt="eye">
                        <span class="latest-news-item__watched-count"><?= $model->views_count; ?></span>
                        <div class="short-info-socials">
                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                            <script src="//yastatic.net/share2/share.js"></script>
                            <div class="ya-share2" data-services="facebook,vkontakte,gplus,twitter"></div>
                        </div>
                    </div>
                    <div class="col-sm-12 p0 short-info-image">
                        <?php $mainImage = $model->image; ?>
                        <?php if ($mainImage) { ?>
                            <?php echo Yii::$app->thumbnail->img($mainImage->path,
                                ['thumbnail' => ['width' => 328, 'height' => 246]],
                                ['alt' => $mainImage->alt ? $mainImage->alt : $this->title]
                            ); ?>
                        <?php } else { ?>
                            <?php echo Yii::$app->thumbnail->placeholder(
                                ['width' => 328, 'height' => 246, 'text' => '328x246'],
                                ['alt' => $this->title]
                            ); ?>
                        <?php } ?>
                    </div>
                    <?php if ($mainImage && $mainImage->title) { ?>
                        <p class="short-info__desc"><?= $mainImage->title; ?></p>
                    <?php } ?>
                </div>
                <div class="full-text">
                    <h3 class="full-text__title"><?= $model->title; ?></h3>
                    <div class="full-text-content">
                        <?= $model->content; ?>
                    </div>
                </div>
                <?php echo \frontend\widgets\CommentWidget::widget([
                    'model' => $model,
                    'dataProviderConfig' => [
                        'pagination' => [
                            'pageSize' => 10
                        ]
                    ],
                    'maxLevel' => 6,
                ]); ?>
            </div>

            <?php if ($anotherNews) { ?>
                <div class="another-news">
                    <h3 class="another-news__title"><?= Yii::t('news', 'Related news'); ?></h3>

                    <?php foreach ($anotherNews as $key => $one) { ?>
                        <?php if (($key % 3) == 0 || $key == 0) { ?>
                            <div class="row">
                        <?php } ?>
                        <div class="col-sm-4 another-news-item">
                            <?php $mainImage = $one->image; ?>
                            <?php if ($mainImage) { ?>
                                <?php echo Yii::$app->thumbnail->img($mainImage->path,
                                    ['thumbnail' => ['width' => 270, 'height' => 203]],
                                    ['class' => 'another-news-item__photo', 'alt' => $mainImage->alt ? $mainImage->alt : $one->title]
                                ); ?>
                            <?php } else { ?>
                                <?php echo Yii::$app->thumbnail->placeholder(
                                    ['width' => 270, 'height' => 203, 'text' => '270x203'],
                                    ['class' => 'another-news-item__photo', 'alt' => $one->title]
                                ); ?>
                            <?php } ?>
                            <a href="<?= Url::to(['view', 'slug' => $one->alias]); ?>" style="text-decoration: none">
                                <h5 class="another-news-item__title"><?= $one->title; ?></h5>
                            </a>
                            <div class="another-news-item__text">
                                <?php $shortText = \yii\helpers\StringHelper::truncate($one->short_description, 110); ?>
                                <?= $shortText . (strlen($shortText) < 255 ? '' : '...'); ?>
                            </div>
                        </div>
                        <?php if (($key % 3) == 2 || ($key == (count($anotherNews) - 1))) { ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
            <?php if ($latestNews) { ?>
                <?= \frontend\modules\news\widgets\LastNewsWidget::widget(['models' => $latestNews]); ?>
            <?php } ?>
        </div>
    </div>
</div>