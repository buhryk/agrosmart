<?php
use yii\helpers\Url;
?>

<div class="category-item clearfix">
    <div class="col-sm-3 p0">
        <?php $mainImage = $model->image; ?>
        <?php if ($mainImage) { ?>
            <?php echo Yii::$app->thumbnail->img($mainImage->path,
                ['thumbnail' => ['width' => 218, 'height' => 163]],
                ['class' => 'category-item__photo', 'alt' => $mainImage->alt ? $mainImage->alt : $model->title]
            ); ?>
        <?php } else { ?>
            <?php echo Yii::$app->thumbnail->placeholder(['width' => 218, 'height' => 163, 'text' => '218x163'],
                ['class' => 'category-item__photo', 'alt' => $model->title]
            ); ?>
        <?php } ?>
    </div>
    <div class="col-sm-9">
        <h3 class="category-item__title category-one-news-title">
            <a href="<?= Url::to(['view', 'slug' => $model->alias]); ?>">
                <?= $model->title; ?>
            </a>
        </h3>
        <p class="category-item__date"><?= date('d.m.Y', $model->published_at); ?></p>
        <p class="category-item__text">
            <?php $shortText = substr($model->short_description, 0, 570); ?>
            <?= $shortText . (strlen($shortText) < 570 ? '' : '...'); ?>
        </p>
    </div>
</div>