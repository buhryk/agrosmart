<?php
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use frontend\modules\news\models\CategoryNewsSearch;
use yii\helpers\Url;

$pageH1 = $model->title;

$this->params['breadcrumbs'][] = ['label' => Yii::t('news', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $pageH1;
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['category', 'slug' => $model->alias])]);
?>

<div class="bread">
    <h1><?= $pageH1; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9 mt45 category-news-container">
            <?php foreach ($news as $one) { ?>
                <?= $this->render('_one_category_news', ['model' => $one]); ?>
            <?php } ?>

            <div class="row message-pagination" style="margin-bottom: 40px;">
                <div class="col-xs-12 col-md-4">
                    <?php $form = ActiveForm::begin([
                        'method' => 'get',
                        'action' => ['category', 'slug' => $model->alias],
                        'options' => ['id' => 'per-page-category-news-page']
                    ]); ?>
                    <span class="message-pagination__text"><?= Yii::t('common', 'Show per page'); ?>:</span>
                    <?php echo Html::activeDropDownList($searchModel,
                        'page_size',
                        CategoryNewsSearch::getPerPagePropertiesList(), [
                            'class' => 'message-pagination__select all-select',
                            'id' => 'per-page-category-news'
                    ]); ?>
                    <?php $this->registerJs("
                        $('#per-page-category-news').on('change', function(){
                            $('form#per-page-category-news-page').submit();
                        });
                    ",
                        \yii\web\View::POS_READY,
                        'my-button-handler'
                    ); ?>
                    <?php ActiveForm::end(); ?>
                </div>

                <div class="col-xs-12 col-md-7">
                    <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages, 'maxButtonCount' => 6, 'registerLinkTags' => true]); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>

            <?= \frontend\modules\news\widgets\LastNewsWidget::widget(['options' => ['count' => 3]]); ?>
        </div>
    </div>
</div>