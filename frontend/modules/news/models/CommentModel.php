<?php

namespace frontend\modules\news\models;

use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\news\models\News;
use yii2mod\comments\models\CommentModel as Model;



class CommentModel extends Model
{
    public function getAuthorName()
    {
        if ($this->author->hasMethod('getUsername')) {
            return $this->author->getUsername();
        }
        return $this->author->fullName;
    }
}