<?php

namespace frontend\modules\news\models;

use backend\modules\news\models\News;

class CategoryNewsSearch extends News
{
    const DEFAULT_PAGE_SIZE = 10;
    const PAGE_SIZE_10_PER_PAGE = 10;
    const PAGE_SIZE_25_PER_PAGE = 25;
    const PAGE_SIZE_50_PER_PAGE = 50;

    public $page_size;

    public function rules()
    {
        return [
            [['page_size'], 'integer'],
            ['page_size', 'default', 'value' => self::DEFAULT_PAGE_SIZE]
        ];
    }

    public function __construct(array $config)
    {
        $this->page_size = self::DEFAULT_PAGE_SIZE;
        parent::__construct($config);
    }

    public static function getPerPagePropertiesList()
    {
        return [
            self::PAGE_SIZE_10_PER_PAGE => self::PAGE_SIZE_10_PER_PAGE,
            self::PAGE_SIZE_25_PER_PAGE => self::PAGE_SIZE_25_PER_PAGE,
            self::PAGE_SIZE_50_PER_PAGE => self::PAGE_SIZE_50_PER_PAGE
        ];
    }
}