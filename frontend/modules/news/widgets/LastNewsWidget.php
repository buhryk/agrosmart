<?php

namespace frontend\modules\news\widgets;

use backend\modules\news\models\NewsLang;
use backend\modules\news\models\NewsUserAccessibility;
use yii\bootstrap\Widget;
use backend\modules\news\models\News;

class LastNewsWidget extends Widget
{
    public $models = [];
    public $options;
    const DEFAULT_COUNT_LAST_NEWS = 3;

    public function init()
    {
        if (!$this->models) {
            $this->models = News::find()
                ->where(['active' => News::ACTIVE_YES])
                ->innerJoin(NewsUserAccessibility::tableName(),NewsUserAccessibility::tableName().'.news_id='.News::tableName().'.id')
                ->andWhere(['in', NewsUserAccessibility::tableName().'.type_id', News::getVisabilityTypesIdsForCurrentUser()])
                ->andWhere(['<=', 'published_at', time()])
                ->orderBy('published_at DESC')
                ->limit(isset($this->options['count']) ? $this->options['count'] : self::DEFAULT_COUNT_LAST_NEWS)
                ->all();
        }
    }

    public function run()
    {
        return $this->render('latest_news', ['models' => $this->models]);
    }
}
