<?php
use yii\helpers\Url;
use common\helpers\DateHelper;
use yii\helpers\Html;
?>

<div class="latest-news">
    <h3 class="latest-news__title"><?= Yii::t('news', 'Latest news'); ?></h3>
    <?php foreach ($models as $one) { ?>
        <div class="latest-news-item clearfix">
            <div class="col-xs-12">
                <span class="latest-news-item__date"><?= date('d.m.Y', $one->published_at); ?></span>
                <span class="latest-news-item__day"><?= DateHelper::getDay(date('D', $one->published_at)); ?></span>
                <span class="latest-news-item__time"><?= date('H:i', $one->published_at); ?></span>
            </div>
            <div class="col-xs-12">
                <a href="<?= Url::to(['/news/news/view', 'slug' => $one->alias]); ?>">
                    <p class="latest-news-item__text"><?=$one->title; ?></p>
                </a>
            </div>
            <div class="col-xs-12">
                <img class="latest-news-item__watched-img" src="/images/eye.png" alt="eye">
                <span class="latest-news-item__watched-count"><?= $one->views_count; ?></span>
            </div>
        </div>
    <?php } ?>
</div>