<?php

namespace frontend\modules\news\controllers;

use backend\modules\event\models\Event;
use backend\modules\event\models\EventLang;
use backend\modules\news\models\NewsCategory;
use backend\modules\news\models\NewsLang;
use backend\modules\news\models\NewsUserAccessibilityType;
use backend\modules\seo\models\Seo;
use frontend\modules\news\models\CategoryNewsSearch;
use Yii;
use backend\modules\news\models\News;
use frontend\modules\news\models\NewsSearch;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $query = News::find()
            ->where(['active' => News::ACTIVE_YES, 'is_top' => News::IS_TOP_YES])
            ->joinWith('lang')
            ->joinWith('userAccessibilities')
            ->andWhere(['in', 'type_id', News::getVisabilityTypesIdsForCurrentUser()])
            ->andWhere(['<=', 'published_at', time()])
            ->orderBy('published_at DESC');
        $categories = NewsCategory::find()
            ->where(['active' => NewsCategory::ACTIVE_YES, 'is_interesting' => NewsCategory::IS_INTERESTING_YES])
            ->joinWith('lang')
            ->all();
        $lastEvents = Event::find()
            ->joinWith('lang')
            ->where(['active' => Event::ACTIVE_YES])
            ->orderBy('created_at DESC')
            ->groupBy(EventLang::tableName().'.event_id')
            ->limit(2)
            ->all();

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $lastNews = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'lastNews' => $lastNews,
            'pages' => $pages,
            'categories' => $categories,
            'lastEvents' => $lastEvents
        ]);
    }

    public function actionSearch()
    {
        $paginationSearchModel = new CategoryNewsSearch([]);
        $paginationSearchModel->load(Yii::$app->request->queryParams);

        $newsSearchModel = new NewsSearch();
        $newsSearchModel->load(Yii::$app->request->queryParams);
        if (!$newsSearchModel->search) {
            return $this->redirect(['index']);
        }
        $query = News::find()
            ->where(['active' => News::ACTIVE_YES])
            ->joinWith('lang')
            ->joinWith('userAccessibilities')
            ->andWhere(['in', 'type_id', News::getVisabilityTypesIdsForCurrentUser()])
            ->andWhere(['or',
                ['like', 'title', '%'.trim($newsSearchModel->search).'%', false],
                ['like', 'short_description', '%'.trim($newsSearchModel->search).'%', false]
            ])
            ->andWhere(['<=', 'published_at', time()])
            ->orderBy(['published_at' => SORT_DESC]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => $paginationSearchModel->page_size
        ]);

        $news = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('search', [
            'news' => $news,
            'pages' => $pages,
            'paginationSearchModel' => $paginationSearchModel,
            'newsSearchModel' => $newsSearchModel
        ]);
    }

    /**
     * Displays a single News model.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($slug)
    {
        $model = News::find()
            ->where(['active' => News::ACTIVE_YES, 'alias' => $slug])
            ->joinWith('lang')
            ->joinWith('userAccessibilities')
            ->andWhere(['in', 'type_id', News::getVisabilityTypesIdsForCurrentUser()])
            ->andWhere(['<=', 'published_at', time()])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $seo = ($seoModel = $model->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);

        $model->views_count += 1;
        $model->update(false);

        $anotherNews = News::find()
            ->where(['active' => News::ACTIVE_YES, 'category_id' => $model->category_id])
            ->andWhere(['<>', News::tableName().'.id', $model->primaryKey])
            ->joinWith('lang')
            ->joinWith('userAccessibilities')
            ->andWhere(['in', 'type_id', News::getVisabilityTypesIdsForCurrentUser()])
            ->andWhere(['<=', 'published_at', time()])
            ->groupBy(NewsLang::tableName().'.news_id')
            ->limit(6)
            ->orderBy('published_at DESC')
            ->all();

        $latestNews = News::find()
            ->where(['active' => News::ACTIVE_YES])
            ->andWhere(['<>', News::tableName().'.id', $model->primaryKey])
            ->joinWith('lang')
            ->joinWith('userAccessibilities')
            ->andWhere(['in', 'type_id', News::getVisabilityTypesIdsForCurrentUser()])
            ->andWhere(['<=', 'published_at', time()])
            ->groupBy(NewsLang::tableName().'.news_id')
            ->limit(3)
            ->orderBy('published_at DESC')
            ->all();

        return $this->render('view', [
            'model' => $model,
            'anotherNews' => $anotherNews,
            'latestNews' => $latestNews
        ]);
    }

    public function actionCategory($slug)
    {
        $model = NewsCategory::find()
            ->where(['active' => NewsCategory::ACTIVE_YES, 'alias' => $slug])
            ->joinWith('lang')
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $seo = ($seoModel = $model->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);

        $searchModel = new CategoryNewsSearch([]);
        $searchModel->load(Yii::$app->request->queryParams);

        $query = News::find()
            ->where(['category_id' => $model->primaryKey, 'active' => News::ACTIVE_YES])
            ->joinWith('lang')
            ->joinWith('userAccessibilities')
            ->andWhere(['in', 'type_id', News::getVisabilityTypesIdsForCurrentUser()])
            ->andWhere(['<=', 'published_at', time()])
            ->orderBy(['published_at'=>SORT_DESC]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => $searchModel->page_size
        ]);

        $news = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('category', [
            'model' => $model,
            'searchModel' => $searchModel,
            'news' => $news,
            'pages' => $pages
        ]);
    }
}