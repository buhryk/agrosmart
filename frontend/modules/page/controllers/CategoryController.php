<?php

namespace frontend\modules\page\controllers;

use backend\modules\page\models\Category;
use backend\modules\page\models\Page;
use backend\modules\seo\models\Seo;
use frontend\modules\instruments\models\InstrumentGet;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * Displays a single Category model.
     * @param integer $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($slug)
    {
        if($slug =='dstu') {
            $instrumentGet = new InstrumentGet(7);
            $instrument = $instrumentGet->searchOne(\Yii::$app->request->get('company_type'));

            if ($instrument['status'] == InstrumentGet::STATUS_NO_ACCCES) {
                return $this->redirect(['/instruments/instrument/no-access', 'id' => $instrumentGet->instrument_id ]);
            }
        }

        $model = Category::find()
            ->where(['alias' => $slug, 'active' => Category::ACTIVE_YES])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $pagesQuery = Page::find()
            ->where(['active' => Page::ACTIVE_YES, 'category_id' => $model->primaryKey])
            ->joinWith('lang');

        $countQuery = clone $pagesQuery;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $models = $pagesQuery->offset($pages->offset)->limit($pages->limit)->all();

        $seo = ($seoModel = $model->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);

        return $this->render('view', [
            'pages' => $pages,
            'models' => $models,
            'category' => $model
        ]);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}