<?php

namespace frontend\modules\page\controllers;

use backend\modules\news\models\News;
use backend\modules\news\models\NewsUserAccessibility;
use backend\modules\page\models\Page;
use backend\modules\seo\models\Seo;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\page\models\Category as PageCategory;
use frontend\modules\instruments\models\InstrumentGet;
/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{

    public function actionView($slug)
    {
        $model = Page::find()
            ->where([Page::tableName().'.alias' => $slug, Page::tableName().'.active' => Page::ACTIVE_YES])
            ->leftJoin(PageCategory::tableName(), PageCategory::tableName().'.id='.Page::tableName().'.category_id')
            ->andWhere(['and',
                ['or',
                    [Page::tableName().'.category_id' =>  null],
                    [PageCategory::tableName().'.active' => PageCategory::ACTIVE_YES]
                ]])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $seo = ($seoModel = $model->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);

        $view = 'view';
        $data['model'] = $model;

        $categoryPage = $model->category;
        if ($categoryPage && $categoryPage->alias == PageCategory::ALIAS_CATEGORY_MAINROLES) {
            $view = 'view_mainroles';
            $data['modelTop'] = Page::find()
                ->where([Page::tableName().'.alias' => $model->alias.'-top'])
                ->joinWith('lang')
                ->one();
            $data['modelBottom'] = Page::find()
                ->where([Page::tableName().'.alias' => $model->alias.'-bottom'])
                ->joinWith('lang')
                ->one();
            $data['news'] = News::find()
                ->where(['active' => News::ACTIVE_YES])
                ->joinWith('lang')
                ->joinWith('userAccessibilities')
                ->andWhere(['in', 'type_id', News::getVisabilityTypesIdsForCurrentUser()])
                ->andWhere(['<=', 'published_at', time()])
                ->groupBy(NewsUserAccessibility::tableName().'.news_id')
                ->orderBy('published_at DESC')
                ->limit(2)
                ->all();
        }

        return $this->render($view, $data);
    }
    
    
}