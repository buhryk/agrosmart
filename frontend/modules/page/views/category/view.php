<?php
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;


if (!$this->title) {
    $this->title = $model->title. ' | '.Yii::$app->params['titlePrefix'];
    $this->registerMetaTag(['name'=>'description', 'content'=> $model->title.' | '.Yii::$app->params['descriptionPrefix']]);
}
$this->params['breadcrumbs'][] = $category->title;
?>

<div class="bread">
    <h1><?= $category->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dstu">
    <div class="row">
        <div class="col-sm-9 mt45">
            <?php foreach ($models as $model) { ?>
                <div class="item">
                    <p class="item__title"><?= $model->title; ?></p>
                    <a href="<?= Url::to(['page/view', 'slug' => $model->alias]); ?>" class="item__link">
                        <?= $model->short_description; ?>
                    </a>
                </div>
            <?php } ?>
            <div style="text-align: center">
                <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pages]); ?>
            </div>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</div>