<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

if (!$this->title) {
    $this->title = $model->title. ' | '.Yii::$app->params['titlePrefix'];
    $this->registerMetaTag(['name'=>'description', 'content'=> $model->title.' | '.Yii::$app->params['descriptionPrefix']]);
}
$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['view','slug' => $model->alias])]);
$this->params['breadcrumbs'][] = $model->title;
?>

<div class="bread">
    <h1><?= $model->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>

<?php if (isset($modelTop) && $modelTop) echo $modelTop->text; ?>

<?php if ($news) { ?>
    <div class="news-list">
        <div class="container">
            <div class="row">
                <?php foreach ($news as $one) { ?>
                    <div class="col-sm-6">
                        <div class="item-news">
                            <h6><?= $one->title; ?></h6>
                            <div class="date"><?= date('d.m.Y', $one->published_at); ?></div>
                            <div class="media">
                                <a class="pull-left" href="<?= Url::to(['/news/news/view', 'slug' => $one->alias]); ?>">
                                    <?php $mainImage = $one->image; ?>
                                    <?php if ($mainImage) { ?>
                                        <?php echo Yii::$app->thumbnail->img($mainImage->path,
                                            ['thumbnail' => ['width' => 237, 'height' => 237]],
                                            ['class' => 'media-object', 'alt' => $mainImage->alt ? $mainImage->alt : $one->title]
                                        ); ?>
                                    <?php } else { ?>
                                        <?php echo Yii::$app->thumbnail->placeholder(['width' => 237, 'height' => 237, 'text' => '237x237'],
                                            ['class' => 'media-object', 'alt' => $one->title]
                                        ); ?>
                                    <?php } ?>
                                </a>
                                <div class="media-body">
                                    <?php $shortText = substr($one->short_description, 0, 400); ?>
                                    <p><?= $shortText . (strlen($shortText) < 400 ? '' : '...'); ?></p>
                                    <a href="<?= Url::to(['/news/news/view', 'slug' => $one->alias]); ?>" class="btn-green">
                                        <?= Yii::t('common', 'further'); ?> ...
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="text-center">
                <a href="<?= Url::to(['/news/news/index']); ?>" class="btn-green">
                    <?= Yii::t('news', 'All news'); ?>
                </a>
            </div>
        </div>
    </div>
    <?php $this->registerJs("
        function setEqualHeight(columns) {
            var tallestcolumn = 0;
            columns.each(
                function() {
                    currentHeight = $(this).height();
                    if (currentHeight > tallestcolumn) {
                        tallestcolumn = currentHeight;
                    }
                }
            );
            columns.height(tallestcolumn);
        }

        setEqualHeight($(\".item-news h6\"));
    ",
        \yii\web\View::POS_READY,
        'my-button-handler'
    ); ?>
<?php } ?>

<?=\frontend\modules\instruments\widgets\InstrumentWidget::widget(['model_id' => $model->id]) ?>
<div class="bg-grey home-category">
    <?= \frontend\widgets\MainServicesWidget::widget([]); ?>
</div>
<?php if (isset($modelBottom) && $modelBottom) echo $modelBottom->text; ?>
