<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

if (!$this->title) {
    $this->title = $model->title. ' | '.Yii::$app->params['titlePrefix'];
    $this->registerMetaTag(['name'=>'description', 'content'=> $model->title.' | '.Yii::$app->params['descriptionPrefix']]);

}

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::to(['view','slug' => $model->alias])]);

$category = $model->category;
if ($category && $category->alias == 'dstu') {
    $this->params['breadcrumbs'][] = ['label' => $category->title, 'url' => ['category/view', 'slug' => $category->alias]];
}
$this->params['breadcrumbs'][] = $model->title;
?>

<div class="bread">
    <h1><?= $model->title; ?></h1>
    <div class="bread-bg">
        <div class="container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
        </div>
    </div>
</div>
<div class="container dstu">
    <div class="row">
        <div class="col-sm-9 mt45">
            <?= $model->text; ?>
        </div>
        <div class="col-sm-3 sidebar-banner">
            <?=\backend\modules\advertising\widgets\AdvertisingWidget::widget(['key' => 'content', 'typePosition' => 'apeak']) ?>
        </div>
    </div>
</div>