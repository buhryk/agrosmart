<?php

namespace frontend\modules\elected\controllers;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;
use frontend\modules\elected\models\Elected;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * ElectedController implements the CRUD actions for Elected model.
 */
class ElectedController extends Controller
{
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            throw new AccessDeniedException('Access denied');
        }

        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax) {
            $request = Yii::$app->request->post();

            if (empty($request['type']) || empty($request['record_id'])) {
                throw new BadRequestHttpException('Not all parameters provided');
            }

            $type = $request['type'];
            $className = Elected::getClassNameByAlias($type);
            $recordId = (int)$request['record_id'];
            $userId = Yii::$app->user->identity->id;

            if (!$className) {
                throw new BadRequestHttpException('Wrong type');
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($this->isRecordIsElectedForUser($userId, $className, $recordId)) {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'message' => Yii::t('common', 'Record is already elected')
                ];
            }

            if ($this->createElected($userId, $className::tableName(), $recordId)) {
                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Record was added to elected')
                ];
            } else {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'message' => Yii::t('common', 'Something went wrong. Please, try again after reloading page')
                ];
            }
        }
    }

    public function actionDelete()
    {
        if (Yii::$app->user->isGuest) {
            throw new AccessDeniedException('Access denied');
        }

        if (Yii::$app->request->method == 'POST' && Yii::$app->request->isAjax) {
            $request = Yii::$app->request->post();

            if (empty($request['type']) || empty($request['record_id'])) {
                throw new BadRequestHttpException('Not all parameters provided');
            }

            $type = $request['type'];
            $className = Elected::getClassNameByAlias($type);
            $recordId = (int)$request['record_id'];
            $userId = Yii::$app->user->identity->id;

            if (!$className) {
                throw new BadRequestHttpException('Wrong type');
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($this->isRecordIsElectedForUser($userId, $className, $recordId)) {
                Elected::deleteAll([
                    'user_id' => $userId,
                    'table_name' => $className::tableName(),
                    'record_id' => $recordId
                ]);

                return [
                    'status' => 'success',
                    'message' => Yii::t('common', 'Record was removed from elected')
                ];
            } else {
                return [
                    'status' => 'error',
                    'title' => Yii::t('common', 'Error'),
                    'message' => Yii::t('common', 'Something went wrong. Please, try again after reloading page')
                ];
            }
        }
    }

    private function isRecordIsElectedForUser($userId, $className, $recordId)
    {
        $record = Elected::findOne([
            'record_id' => $recordId,
            'table_name' => $className::tableName(),
            'user_id' => $userId
        ]);

        return $record === null ? false : true;
    }

    private function createElected($userId, $tableName, $recordId)
    {
        $model = new Elected();
        $model->user_id = $userId;
        $model->table_name = $tableName;
        $model->record_id = $recordId;

        return $model->save() ? true : false;
    }
}