<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 19.03.2018
 * Time: 13:16
 */

namespace common\components;


class LinguaStemRu
{
    var $VERSION = "0.02";
    var $Stem_Caching = 0;
    var $Stem_Cache = array();
    var $VOWEL = '/аеиоуыэюя/';
    
    var $PERFECTIVEGROUND = '/((ив|ивши|ена|ившись|ыв|ывши|ывшись)|((?<=[ая])(в|вши|вшись)))$/';
    var $REFLEXIVE = '/(с[яьи])$/';
    var $ADJECTIVE = '/(ее|ие|ые|ое|ими|ыми|ей|ий|ій|ый|ой|ем|им|ым|ом|его|ого|ему|ому|их|ых|ую|юю|ая|яя|ою|ею|ій|ий|а|е|ова|ове|ів|є|їй|єє|еє|я|ім|ем|им|ім|их|іх|ою|йми|іми|у|ю|ого|ому|ої)$/';
    var $PARTICIPLE = '/((ивш|ій|ывш|ующ|ого|ому|им|ім|а|ій|у|ою|ій|і|их|йми|их)|((?<=[ая])(ем|нн|вш|ющ|щ)))$/';
    var $VERB = '/((ыла|ена|ейте|уйте|ите|ыли|ей|уй|ыл|им|ым|ен|ыло|ено|ят|ует|уют|ит|ыт|ены|ить|ыть|ишь|ую|ю)|((?<=[ая])(ла|на|ете|йте|ли|й|л|ем|н|ло|но|ет|ют|ны|ть|ешь|нно|ся|ив|ать|ять|у|ав|али|учи|ячи|вши|ши|е|ме|ати|яти|є)))$/';
    var $NOUN = '/(а|ев|ов|ие|ье|е|иями|ями|ами|еи|ии|и|ией|еня|ей|ой|ий|й|иям|ям|ием|ем|ам|ом|о|у|ах|иях|ях|ы|ь|ию|ью|ю|ия|ья|я|і|ові|ї|ею|єю|ою|є|еві|ем|єм|ів|їв|ю)$/';
    var $RVRE = '/^(.*?[аеиоуыэюяіїє])(.*)$/';
    var $DERIVATIONAL = '/[^аеиоуыэюяіїє][аеиоуыэюяіїє]+[^аеиоуыэюяіїє]+[аеиоуыэюяіїє].*(?<=о)сть?$/';

    function s(&$s, $re, $to)
    {
        $orig = $s;
        $s = preg_replace($re, $to, $s);
        return $orig !== $s;
    }

    function m($s, $re)
    {
        return preg_match($re, $s);
    }

    function stem_word($word)
    {
        $word = mb_strtolower($word);
//        $word = strtr($word, 'ё', 'е');
        $word = str_ireplace('', '', $word);
        # Check against cache of stemmed words
        if ($this->Stem_Caching && isset($this->Stem_Cache[$word])) {
        return $this->Stem_Cache[$word];
    }
        $stem = $word;
        do {
            if (!preg_match($this->RVRE, $word, $p)) break;
            $start = $p[1];
            $RV = $p[2];
            if (!$RV) break;

            # Step 1
            if (!$this->s($RV, $this->PERFECTIVEGROUND, '')) {
                $this->s($RV, $this->REFLEXIVE, '');

                if ($this->s($RV, $this->ADJECTIVE, '')) {
                    $this->s($RV, $this->PARTICIPLE, '');
                } else {
                    if (!$this->s($RV, $this->VERB, ''))
                        $this->s($RV, $this->NOUN, '');
                }
            }

            # Step 2
            $this->s($RV, '/и$/', '');

            # Step 3
            if ($this->m($RV, $this->DERIVATIONAL))
                $this->s($RV, '/ость?$/', '');

            # Step 4
            if (!$this->s($RV, '/ь$/', '')) {
                $this->s($RV, '/ейше?/', '');
                $this->s($RV, '/нн$/', 'н');
            }

            $stem = $start.$RV;
        } while(false);
        if ($this->Stem_Caching) $this->Stem_Cache[$word] = $stem;
        return $stem;
    }

    function stem_caching($parm_ref)
    {
        $caching_level = @$parm_ref['-level'];
        if ($caching_level) {
            if (!$this->m($caching_level, '/^[012]$/')) {
                die(__CLASS__ . "::stem_caching() - Legal values are '0','1' or '2'. '$caching_level' is not a legal value");
            }
            $this->Stem_Caching = $caching_level;
        }
        return $this->Stem_Caching;
    }

    function clear_stem_cache()
    {
        $this->Stem_Cache = array();
    }

    function allotment($q, $name)
    {
        $words_search1 = explode(' ', $q);
        $words_name1 = explode(' ', $name);
        $words_search = [];
        $words_name = [];

        foreach ($words_search1 as $k => $words){
            $words_search[$k+1] = $words;
        }
        foreach ($words_name1 as $k => $words){
            $words_name[$k+1] = $words;
        }

        foreach ($words_search as $word){

                $word_preg = (mb_strlen($word, 'UTF-8' ) > 2 ) ? $this->stem_word($word) : $word ;

                if ( array_search($this->stem_word($word), $words_name) ){
                    $name = preg_replace('/('. $word_preg .')/iu', '<b>$1</b>', $name);
                }else{
                    $name = preg_replace('/('. $word_preg  .'[^\s]*+)/iu', '<b>$1</b>', $name);
                }
        }

        return $name;
    }
}