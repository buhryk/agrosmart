<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 01.02.2018
 * Time: 10:20
 */

namespace common\components;
use backend\modules\price2\models\Price;
use backend\modules\price2\models\PriceColumn;
use backend\modules\price2\models\PriceItem;
use PHPExcel;
use Yii;
use yii\db\Exception;

use yii\base\Component;

class ExelPrice extends Component
{
    static $arr = [];

    public function error($err = false)
    {
        array_push(self::$arr, $err);
    }

    public function import($inputFile, $type, $priceModel)
    {
        try{
            $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExel = $objReader->load($inputFile);
        }catch (Exception $e){
            die('Error');
        }
        $sheet = $objPHPExel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColmn = $sheet->getHighestColumn();

        if ($priceModel) {
            $price = $priceModel;
        } else {
            $price = new Price();
        }


        $b1 = $sheet->getCell('B1')->getValue();
        $b2 = $sheet->getCell('B2')->getValue();

        if (!$b1 || $b1 == 'заполнить'){
             $this->error(Yii::t('price','Not filled price name').' - B:1');
        }
        $price->name = strval($sheet->getCell('B1')->getValue());



//        if(!$b2 || $b2 == 'заполнить'){
//            $this->error(Yii::t('price','Not filled price description').' - B:2');
//        }

        $description =  strval($sheet->getCell('B2')->getValue());

        $price->description = $description == 'заполнить' ? '' : $description;

        $company = Yii::$app->user->identity->company;
        $price->company_id = $company ? $company->primaryKey : 0;
        $price->type = $type;

        $save_price = false;
        if(!self::$arr){
            $save_price = $price->save();
        }

        $price_last = $price;

        $column = new PriceColumn();
        $column->price_id = $price_last->id;

        $default_column = 'заполнить, если нужно';

        $column->name1 = ($sheet->getCell('C3')->getValue() != $default_column) ? (string)$sheet->getCell('C3')->getValue() : null;
        $column->name2 = ($column->name1 && $sheet->getCell('D3')->getValue() != $default_column) ? (string)$sheet->getCell('D3')->getValue() : null;
        $column->name3 = ($column->name2 && $sheet->getCell('E3')->getValue()  != $default_column) ? (string)$sheet->getCell('E3')->getValue() : null;

        if(!self::$arr){
            $column->save();
        }

        if ($priceModel && $save_price ) {
            PriceItem::deleteAll(['price_id' => $price->id]);
        }

        if(!$sheet->getCell('A4')->getValue() && !$sheet->getCell('B4')->getValue()){
            $this->error(Yii::t('price','No files found in the file'));
        }else{
            $c3 = 0; $d3 = 0; $e3 = 0;
            for( $row = 1; $row <= $highestRow; $row++){
                $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColmn.$row,null,true,false);

                if($row == 1){
                    continue;
                }

                $item = new PriceItem();

                if ($row > 3){
                    $a = $sheet->getCell('A'.$row)->getValue();
                    $b = $sheet->getCell('B'.$row)->getValue();
                    $c = $sheet->getCell('C'.$row)->getValue();
                    $d = $sheet->getCell('D'.$row)->getValue();
                    $e = $sheet->getCell('E'.$row)->getValue();
                    str_replace(",", ".", $b);


                    if(!$a && $b){
                        $this->error(Yii::t('price','Not filled price or name list').' - A:'.$row);
                        continue;
                    }
                    if($a && !$b){
                        $this->error(Yii::t('price','Not filled price or name list').' - B:'.$row);
                        continue;
                    }
                    if($a && $b && $c && $c3 == 0 && !$sheet->getCell('C3')->getValue()){
                        $c3++;
                        $this->error(Yii::t('price','Not filled column 1').' - C:3');
                        continue;
                    }
                    if($a && $b && $d && $d3 == 0 && !$sheet->getCell('D3')->getValue()){
                        $d3++;
                        $this->error(Yii::t('price','Not filled column 2').' - D:3');
                        continue;
                    }
                    if($a && $b && $e && $e3 == 0 && !$sheet->getCell('E3')->getValue()){
                        $e3++;
                        $this->error(Yii::t('price','Not filled column 3').' - E:3');
                        continue;
                    }

                    $item->price_id = $price_last->id;
                    $item->name = $rowData[0][0];
                    $item->price = $rowData[0][1];

                    $item->column1 = ($rowData[0][2] && $column->name1) ? $rowData[0][2] : null;
                    $item->column2 = ($rowData[0][3] && $column->name2) ? $rowData[0][3] : null;
                    $item->column3 = ($rowData[0][4] && $column->name3) ? $rowData[0][4] : null;

                    if(!self::$arr){
                        $item->save();
                    }
                }
            }
        }
        if($save_price && self::$arr){
            $price->deleteAll(['id' => $price_last->id]);
        }

        return $price;
    }

    public function export($id)
    {
        $price = Price::findOne($id);
        $price_item = PriceItem::find()->where(['price_id' => $id])->all();
        $price_column = PriceColumn::find()->where(['price_id' => $id])->one();

        $objPHPExcel = new PHPExcel;

        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');

        $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");


        $objSheet = $objPHPExcel->getActiveSheet();

        $objSheet->setTitle(Yii::t('price','price-list'));

        $objSheet->getColumnDimension('A')->setWidth(30);
        $objSheet->getColumnDimension('B')->setWidth(20);
        $objSheet->getColumnDimension('C')->setWidth(25);
        $objSheet->getColumnDimension('D')->setWidth(25);
        $objSheet->getColumnDimension('E')->setWidth(25);

        $objSheet->getStyle('A1:A2')->getFont()->setBold(true);
        $objSheet->getStyle('B1:B2')->getFont()->setItalic(true);

        $objSheet->getStyle('B1:B3')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $objSheet->getStyle('A1:A2')
        ->getFill()
        ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('cccccc');

        $objSheet->getRowDimension('3')->setRowHeight(15);

        $objSheet->getStyle('A3:E3')
        ->applyFromArray(
        array('fill'    => array(
            'type'      => \PHPExcel_Style_Fill::FILL_SOLID,
            'color'     => array('rgb' => '92d050')
        ),
            'borders' => array(
                'bottom'    => array('style' => \PHPExcel_Style_Border::BORDER_THIN)
            )

        ));

        $objSheet->getCell('A1')->setValue(Yii::t('price','Price list name'));
        $objSheet->getCell('B1')->setValue($price->name);
        $objSheet->getCell('A2')->setValue(Yii::t('price','Price list description'));
        $objSheet->getCell('B2')->setValue($price->description);

        $objSheet->getStyle('A3')->getFont()->setBold(true);
        $objSheet->getStyle('B3')->getFont()->setBold(true);
        $objSheet->getCell('A3')->setValue(Yii::t('price','denomination'));
        $objSheet->getCell('B3')->setValue(Yii::t('price','cost'));
        $objSheet->getCell('C3')->setValue($price_column->name1);
        $objSheet->getCell('D3')->setValue($price_column->name2);
        $objSheet->getCell('E3')->setValue($price_column->name3);

        $i = 4;
        foreach ($price_item as $item){
            $objSheet->getCell('A'.$i)->setValue($item->name);
            $objSheet->getCell('B'.$i)->setValue($item->price);
            $objSheet->getCell('C'.$i)->setValue($item->column1);
            $objSheet->getCell('D'.$i)->setValue($item->column2);
            $objSheet->getCell('E'.$i)->setValue($item->column3);
            $i++;
        }

        $this->downloadFile($objPHPExcel);
    }

    protected function downloadFile($objPHPExcel)
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="price.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}