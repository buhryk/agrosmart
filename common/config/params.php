<?php
return [
    'adminEmail' => 'notification@agro-smart.com.ua',
    'supportEmail' => 'notification@agro-smart.com.ua',
    'user.passwordResetTokenExpire' => 3600,
    'frontendUrl' => 'https://agro-smart.com.ua'
];
