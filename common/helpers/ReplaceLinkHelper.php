<?php
namespace common\helpers;

class ReplaceLinkHelper
{
    static function replace($string)
    {
        $string = str_replace('<a', '<span' ,$string);
        return str_replace('/a>', '/span>' ,$string);
    }
}