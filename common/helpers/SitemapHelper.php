<?php
namespace common\helpers;

use backend\modules\event\models\Event;
use backend\modules\event\models\EventLang;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsCategory;
use backend\modules\news\models\NewsCategoryLang;
use backend\modules\news\models\NewsLang;
use backend\modules\page\models\Category;
use backend\modules\page\models\CategoryLang;
use backend\modules\page\models\Page;
use backend\modules\page\models\PageLang;
use backend\modules\rubric\models\Rubric;
use backend\modules\rubric\models\RubricLang;
use frontend\models\Company;
use frontend\models\User;
use frontend\modules\product\models\Advertisement;
use Yii;

class SitemapHelper {
    public $frontendUrl;
    protected $langs = ['', 'ua'];
    private $rubrics;
    private $products;
    private $categoryPages;
    private $pages;
    private $users;
    private $companies;
    private $categoryNews;
    private $news;
    private $events;

    const DEFAULT_PARAMS = ['priority' => '0.8', 'changefreq' => 'weekly'];

    public function __construct($counter)
    {
        $this->frontendUrl = isset(Yii::$app->params['frontendUrl']) ? Yii::$app->params['frontendUrl'] : 'https://agro-smart.com.ua';


        if($counter == 1){
            $this->rubrics = Rubric::find()
                ->select('*')
                ->addSelect('COUNT('.RubricLang::tableName().'.rubric_id) AS count_translates')
                ->leftJoin(RubricLang::tableName(), RubricLang::tableName().'.rubric_id = '.Rubric::tableName().'.id')
                ->groupBy([RubricLang::tableName().'.rubric_id'])
                ->andWhere(['active' => Rubric::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
        }
        else if($counter == 2){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand(" SELECT alias FROM `advertisement` WHERE (`status`=1) AND (active_to >= ". time() ." OR active_to is null) ");
            $this->products = $command->queryAll();

            /*$this->products = Advertisement::find()->where(['status' => Advertisement::STATUS_ACTIVE_YES])
                ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
                ->all();*/
        }
        else if($counter == 3){
            $this->categoryPages = Category::find()
                ->select('*')
                ->addSelect('COUNT('.CategoryLang::tableName().'.category_id) AS count_translates')
                ->leftJoin(CategoryLang::tableName(), CategoryLang::tableName().'.category_id = '.Category::tableName().'.id')
                ->groupBy([CategoryLang::tableName().'.category_id'])
                ->andWhere(['active' => Category::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
        }
        else if($counter == 4){
            $this->pages = Page::find()
                ->select('*')
                ->addSelect('COUNT('.PageLang::tableName().'.page_id) AS count_translates')
                ->leftJoin(PageLang::tableName(), PageLang::tableName().'.page_id = '.Page::tableName().'.id')
                ->groupBy([PageLang::tableName().'.page_id'])
                ->andWhere(['active' => Page::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
        }
        else if($counter == 5){
            $this->users = User::find()->where(['status' => User::STATUS_ACTIVE])->all();
        }
        else if($counter == 6){
            $this->companies = Company::find()->all();
        }
        else if($counter == 7){
            $this->categoryNews = NewsCategory::find()
                ->select('*')
                ->addSelect('COUNT('.NewsCategoryLang::tableName().'.category_id) AS count_translates')
                ->leftJoin(NewsCategoryLang::tableName(), NewsCategoryLang::tableName().'.category_id = '.NewsCategory::tableName().'.id')
                ->groupBy([NewsCategoryLang::tableName().'.category_id'])
                ->andWhere(['active' => NewsCategory::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
        }
        else if($counter == 8){
            $this->news = News::find()
                ->select('*')
                ->addSelect('COUNT('.NewsLang::tableName().'.news_id) AS count_translates')
                ->leftJoin(NewsLang::tableName(), NewsLang::tableName().'.news_id = '.News::tableName().'.id')
                ->groupBy([NewsLang::tableName().'.news_id'])
                ->joinWith('userAccessibilities')
                ->andWhere(['active' => News::ACTIVE_YES, 'type_id' => 9])
                ->andWhere(['<=', 'published_at', time()])
                ->having("count_translates = 2")
                ->all();
        }
        else if($counter == 9){
            $this->events = Event::find()
                ->select('*')
                ->addSelect('COUNT('.EventLang::tableName().'.event_id) AS count_translates')
                ->leftJoin(EventLang::tableName(), EventLang::tableName().'.event_id = '.Event::tableName().'.id')
                ->groupBy([EventLang::tableName().'.event_id'])
                ->andWhere(['active' => Event::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
        }

    }

    public function createSitemap($counter)
    {
        //echo $counter; //exit;
        if($counter == 1){
            $dom = new \DOMDocument("1.0", "utf-8");
            $urlset = $dom->createElement('urlset');
            $urlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');
            $urlset->setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
            $urlset->setAttribute('xsi:schemaLocation','http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');

            $urlset = self::generateRubrics($this->rubrics, $dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }
        else if($counter == 2){
            $dom = new \DOMDocument;
            $dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            $urlset = $dom->getElementsByTagName('urlset')->item(0);

            $urlset = self::generateProducts($this->products, $dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }
        else if($counter == 3){
            $dom = new \DOMDocument;
            $dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            $urlset = $dom->getElementsByTagName('urlset')->item(0);

            $urlset = self::generatePageCategories($this->categoryPages, $dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }
        else if($counter == 4){
            $dom = new \DOMDocument;
            $dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            $urlset = $dom->getElementsByTagName('urlset')->item(0);

            $urlset = self::generatePages($this->pages, $dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }
        else if($counter == 5){
            $dom = new \DOMDocument;
            $dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            $urlset = $dom->getElementsByTagName('urlset')->item(0);

            $urlset = self::generateUsers($this->users, $dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }
        else if($counter == 6){
            $dom = new \DOMDocument;
            $dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            $urlset = $dom->getElementsByTagName('urlset')->item(0);

            $urlset = self::generateCompanies($this->companies, $dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }
        else if($counter == 7){
            $dom = new \DOMDocument;
            $dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            $urlset = $dom->getElementsByTagName('urlset')->item(0);

            $urlset = self::generateNewsCategories($this->categoryNews, $dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }
        else if($counter == 8){
            $dom = new \DOMDocument;
            $dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            $urlset = $dom->getElementsByTagName('urlset')->item(0);

            $urlset = self::generateNews($this->news, $dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }
        else if($counter == 9){
            $dom = new \DOMDocument;
            $dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            $urlset = $dom->getElementsByTagName('urlset')->item(0);

            $urlset = self::generateEvents($this->events, $dom, $urlset);
            $urlset = self::generateOther($dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }
        /*

            $urlset = self::generateRubrics($this->rubrics, $dom, $urlset);
            $urlset = self::generateProducts($this->products, $dom, $urlset);
            $urlset = self::generatePageCategories($this->categoryPages, $dom, $urlset);
            $urlset = self::generatePages($this->pages, $dom, $urlset);
            $urlset = self::generateUsers($this->users, $dom, $urlset);
            $urlset = self::generateCompanies($this->companies, $dom, $urlset);
            $urlset = self::generateNewsCategories($this->categoryNews, $dom, $urlset);
            $urlset = self::generateNews($this->news, $dom, $urlset);
            $urlset = self::generateEvents($this->events, $dom, $urlset);
            $urlset = self::generateOther($dom, $urlset);

        $dom->appendChild($urlset);
        $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');*/
    }

    private function generateRubrics($rubrics, $dom, $urlset)
    {
        foreach ($rubrics as $rubric) {
            foreach ($this->langs as $lang) {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'product/rubric/' . $rubric->alias);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.8']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generateProducts($products, $dom, $urlset)
    {
        foreach ($products as $product) {
            $url = $dom->createElement('url');
            $lock = $dom->createElement('loc', $this->frontendUrl . '/product/' . $product['alias']);
            $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
            $urlset->appendChild($url);
        }

        return $urlset;
    }

    private function generatePageCategories($categories, $dom, $urlset) {
        foreach ($categories as $category) {
            foreach ($this->langs as $lang) {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'page/category/' . $category->alias);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.8']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generatePages($pages, $dom, $urlset) {
        foreach ($pages as $page) {
            foreach ($this->langs as $lang) {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'page/' . $page->alias);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generateUsers($users, $dom, $urlset)
    {
        foreach ($users as $user) {
            $url = $dom->createElement('url');
            $lock = $dom->createElement('loc', $this->frontendUrl . '/user/user/view/' . $user->id);
            $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
            $urlset->appendChild($url);

            $url = $dom->createElement('url');
            $lock = $dom->createElement('loc', $this->frontendUrl . '/user/user/products/' . $user->id);
            $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
            $urlset->appendChild($url);
        }

        return $urlset;
    }

    private function generateCompanies($companies, $dom, $urlset)
    {
        foreach ($companies as $company) {
            $url = $dom->createElement('url');
            $lock = $dom->createElement('loc', $this->frontendUrl . '/company/' . $company->id);
            $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
            $urlset->appendChild($url);

            $url = $dom->createElement('url');
            $lock = $dom->createElement('loc', $this->frontendUrl . '/company/company/products/' . $company->id);
            $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
            $urlset->appendChild($url);

            $url = $dom->createElement('url');
            $lock = $dom->createElement('loc', $this->frontendUrl . '/company/price/zakupka/' . $company->id);
            $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
            $urlset->appendChild($url);

            $url = $dom->createElement('url');
            $lock = $dom->createElement('loc', $this->frontendUrl . '/company/company/feedbacks/' . $company->id);
            $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
            $urlset->appendChild($url);
        }

        return $urlset;
    }

    private function generateNewsCategories($categories, $dom, $urlset) {
        foreach ($categories as $category) {
            foreach ($this->langs as $lang) {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'news/category/' . $category->alias);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.8']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generateNews($news, $dom, $urlset) {
        foreach ($news as $one) {
            foreach ($this->langs as $lang) {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'news/' . $one->alias);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generateEvents($events, $dom, $urlset) {
        foreach ($events as $event) {
            foreach ($this->langs as $lang) {
                if(strripos($event->alias, '&') === false){
                    $url = $dom->createElement('url');
                    $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'event/' . $event->alias);
                    $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
                    $urlset->appendChild($url);
                }
            }
        }

        return $urlset;
    }

    private function generateOther($dom, $urlset)
    {
        $other = [
            ['url' => 'instruments/analytics', 'multilang' => true],
            ['url' => '/instruments/fuel/prices', 'multilang' => false],
            ['url' => 'instruments/analytics-price', 'multilang' => true],
            ['url' => 'instruments', 'multilang' => true],
            ['url' => 'instruments/map-price-company', 'multilang' => true],
            ['url' => 'instruments/elevators/map', 'multilang' => true],
            ['url' => '/instruments/logistics-calculators', 'multilang' => false],
            ['url' => '/instruments/logistics-calculators/avto', 'multilang' => false],

            ['url' => 'company/price/zakupka', 'multilang' => true],
            ['url' => 'company/price/prodazha', 'multilang' => true],
            ['url' => 'instruments/quality-calculation', 'multilang' => true],
            ['url' => '/instruments/quality-calculation/pshenitsa', 'multilang' => false],
        ];

        foreach ($other as $item) {
            if ($item['multilang']) {
                foreach ($this->langs as $lang) {
                    $url = $dom->createElement('url');
                    $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . $item['url']);
                    $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
                    $urlset->appendChild($url);
                }
            } else {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . $item['url']);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generateUrlChildrenNodes($url, $lock, $dom, $params = self::DEFAULT_PARAMS)
    {
        $url->appendChild($lock);

        $changefreqValue = (isset($params['changefreq']) && $params['changefreq']) ?
            $params['changefreq'] : self::DEFAULT_PARAMS['changefreq'];
        $changefreq = $dom->createElement('changefreq', $changefreqValue);
        $url->appendChild($changefreq);

        $priorityValue = (isset($params['priority']) && $params['priority']) ?
            $params['priority'] : self::DEFAULT_PARAMS['priority'];
        $priority = $dom->createElement('priority', $priorityValue);
        $url->appendChild($priority);

        return $url;
    }

    public function url($url)
    {
        return Yii::$app->urlManager->createAbsoluteUrl($url);
    }
}