<?php
namespace common\helpers;

class GoogleMapsApiHelper {

    public $lang;
    public $baseUrl = 'https://maps.googleapis.com/maps/api/geocode/json?';
    public function __construct()
    {
        $this->lang = \Yii::$app->language == 'ru-RU' ? 'ru' : 'uk';
    }

    public function getDistance($location1, $location2)
    {
        $coordinates1 = $location1['lat'].','.$location1['lng'];
        $coordinates2 = $location2['lat'].','.$location2['lng'];

        print_r($coordinates2);
        $urlx = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$coordinates1&destinations=$coordinates2&sensor=false&language=".$this->lang;
        $ch = curl_init($urlx);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = json_decode(curl_exec($ch));
        curl_close($ch);

        if (isset($data->rows[0]->elements[0]->distance->text)) {
            return $data->rows[0]->elements[0]->distance->text;
        }

        return null;
    }

    public function getLocationByAddress($address)
    {

        $url = $this->baseUrl.http_build_query([
            'address' => $address,
                'sensor' => 'false',
                'key' => 'AIzaSyCv9uSoiamEokiAV6F_U9Ahb4rUj2-hMKQ'
            ]);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER      => 1,
            CURLOPT_URL                 => $url,
            CURLOPT_SSL_VERIFYPEER      => false,
            CURLOPT_FAILONERROR         => true,
        ));
        $geoloc = json_decode(curl_exec($curl), true);

        curl_close($curl);


        if ($geoloc['status'] == 'OK' && isset($geoloc['results'][0]['geometry']['location'])) {
            return $geoloc['results'][0]['geometry']['location'];
        }

        return null;
    }
}