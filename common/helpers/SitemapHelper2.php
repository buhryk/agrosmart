<?php
namespace common\helpers;

use backend\modules\event\models\Event;
use backend\modules\event\models\EventLang;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsCategory;
use backend\modules\news\models\NewsCategoryLang;
use backend\modules\news\models\NewsLang;
use backend\modules\page\models\Category;
use backend\modules\page\models\CategoryLang;
use backend\modules\page\models\Page;
use backend\modules\page\models\PageLang;
use backend\modules\rubric\models\Rubric;
use backend\modules\rubric\models\RubricLang;
use frontend\models\Company;
use frontend\models\User;
use frontend\modules\product\models\Advertisement;
use Yii;

class SitemapHelper2 {
   
   public $frontendUrl;
    protected $langs = ['', 'ua'];
    private $rubrics;
    private $products;
    private $categoryPages;
    private $pages;
    private $users;
    private $companies;
    private $categoryNews;
    private $news;
    private $events;
    private $countRows;
    private $defaultCountRow = 30000;
    private $newAllCount = 30000;
    private $defaultCountRowVal = 10000;

    private $currentFile = 1;
    private $domObject;
    private $thisUrlset;
    private $ajaxCounter;

    const DEFAULT_PARAMS = ['priority' => '0.8', 'changefreq' => 'weekly'];

    public function __construct($counter)
    {
        $this->frontendUrl = isset(Yii::$app->params['frontendUrl']) ? Yii::$app->params['frontendUrl'] : 'https://agro-smart.com.ua';

        $this->domObject = new \DOMDocument("1.0", "utf-8");
        $this->ajaxCounter = $counter;

        if($counter == 1){
            $this->rubrics = Rubric::find()
                ->select('*')
                ->addSelect('COUNT('.RubricLang::tableName().'.rubric_id) AS count_translates')
                ->leftJoin(RubricLang::tableName(), RubricLang::tableName().'.rubric_id = '.Rubric::tableName().'.id')
                ->groupBy([RubricLang::tableName().'.rubric_id'])
                ->andWhere(['active' => Rubric::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand(" SELECT alias FROM `advertisement` WHERE (`status`=1) AND (active_to >= ". time() ." OR active_to is null) ");
            $this->products = $command->queryAll();
        }
        else if($counter == 2){
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand(" SELECT alias FROM `advertisement` WHERE (`status`=1) AND (active_to >= ". time() ." OR active_to is null) ");
            $this->products = $command->queryAll();

            /*$this->products = Advertisement::find()->where(['status' => Advertisement::STATUS_ACTIVE_YES])
                ->andWhere('active_to >= :active_to OR active_to is null', [':active_to' => time()])
                ->all();*/
        }
        else if($counter == 3){
            $this->categoryPages = Category::find()
                ->select('*')
                ->addSelect('COUNT('.CategoryLang::tableName().'.category_id) AS count_translates')
                ->leftJoin(CategoryLang::tableName(), CategoryLang::tableName().'.category_id = '.Category::tableName().'.id')
                ->groupBy([CategoryLang::tableName().'.category_id'])
                ->andWhere(['active' => Category::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
        }
        else if($counter == 4){
            $this->pages = Page::find()
                ->select('*')
                ->addSelect('COUNT('.PageLang::tableName().'.page_id) AS count_translates')
                ->leftJoin(PageLang::tableName(), PageLang::tableName().'.page_id = '.Page::tableName().'.id')
                ->groupBy([PageLang::tableName().'.page_id'])
                ->andWhere(['active' => Page::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
        }
        else if($counter == 5){
            $this->users = User::find()->where(['status' => User::STATUS_ACTIVE])->all();
        }
        else if($counter == 6){
            $this->companies = Company::find()->all();
        }
        else if($counter == 7){
            $this->categoryNews = NewsCategory::find()
                ->select('*')
                ->addSelect('COUNT('.NewsCategoryLang::tableName().'.category_id) AS count_translates')
                ->leftJoin(NewsCategoryLang::tableName(), NewsCategoryLang::tableName().'.category_id = '.NewsCategory::tableName().'.id')
                ->groupBy([NewsCategoryLang::tableName().'.category_id'])
                ->andWhere(['active' => NewsCategory::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
        }
        else if($counter == 8){
            $this->news = News::find()
                ->select('*')
                ->addSelect('COUNT('.NewsLang::tableName().'.news_id) AS count_translates')
                ->leftJoin(NewsLang::tableName(), NewsLang::tableName().'.news_id = '.News::tableName().'.id')
                ->groupBy([NewsLang::tableName().'.news_id'])
                ->joinWith('userAccessibilities')
                ->andWhere(['active' => News::ACTIVE_YES, 'type_id' => 9])
                ->andWhere(['<=', 'published_at', time()])
                ->having("count_translates = 2")
                ->all();
        }
        else if($counter == 9){
            $this->events = Event::find()
                ->select('*')
                ->addSelect('COUNT('.EventLang::tableName().'.event_id) AS count_translates')
                ->leftJoin(EventLang::tableName(), EventLang::tableName().'.event_id = '.Event::tableName().'.id')
                ->groupBy([EventLang::tableName().'.event_id'])
                ->andWhere(['active' => Event::ACTIVE_YES])
                ->having("count_translates = 2")
                ->all();
        }

    }

    private function setCountRows($count){
        $this->countRows = $count;
    }
    private function setCurrentFile($count){
        $this->currentFile = $count;
    }
    private function createFile(){
        //$this->domObject = new \DOMDocument("1.0", "utf-8");
        //$this->thisUrlset = '';
        $this->thisUrlset = $this->domObject->createElement('urlset');
        $this->thisUrlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');
        $this->thisUrlset->setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        $this->thisUrlset->setAttribute('xsi:schemaLocation','http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');

        //$urlset = self::generateRubrics($this->rubrics, $this->domObject, $urlset);



    }
    private function saveThisXml($urlset){

        $this->domObject->appendChild($urlset);
        $this->domObject->save(Yii::getAlias('@frontend') .'/web/sitemap'.$this->currentFile.'.xml');
        $this->thisUrlset->nodeValue = '';
        if($this->countRows >= $this->newAllCount){
            $this->currentFile++;
            $this->countRows = 0;
            //echo $this->countRows.' --- ';
        }
        //echo $this->countRows.' - ';
        $this->getFileCount();
    }
    private function getThisCountRow(){
         return $this->domObject->getElementsByTagName('url')->length;
    }

    private function getFileCount(){

        if($this->ajaxCounter == 1) { //FIRST ITER
            //$this->domObject = new \DOMDocument("1.0", "utf-8");
            $this->thisUrlset = $this->domObject->createElement('urlset');
            $this->thisUrlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');
            $this->thisUrlset->setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
            $this->thisUrlset->setAttribute('xsi:schemaLocation','http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
            $this->ajaxCounter++;
            $this->countRows = 1;
            if (file_exists(Yii::getAlias('@frontend') . '/web/sitemap2.xml')) unlink(Yii::getAlias('@frontend') . '/web/sitemap2.xml');

            return;
        }

        if(isset($this->currentFile)){
            try {
                $this->domObject->load(Yii::getAlias('@frontend') . '/web/sitemap' . $this->currentFile . '.xml');
                $this->thisUrlset = $this->domObject->getElementsByTagName('urlset')->item(0);
                $this->countRows = $this->domObject->getElementsByTagName('url')->length;

                    if ($this->defaultCountRow <= $this->countRows) {

                        $this->countRows = 0;
                        //$this->createFile();
                    } else {
                        $this->thisUrlset = $this->domObject->getElementsByTagName('urlset')->item(0);
                    }
            }
            catch (\Exception $ex){
                if($this->currentFile == 1) {
                    //$this->createFile();
                }
                /*if($this->currentFile == 1){
                    $dom->load(Yii::getAlias('@frontend') . '/web/sitemap' . $this->currentFile-- . '.xml');
                    $countAllRow = $dom->getElementsByTagName('url')->length;
                    if(isset($countAllRow)){
                        if($countAllRow >= $this->defaultCountRow){
                            $this->setCountRows($countAllRow);
                        }
                    }
                }*/
            }

        }
        else{
            $this->domObject->load(Yii::getAlias('@frontend') .'/web/sitemap'.$this->currentFile.'.xml');
            $this->countRows = $this->domObject->getElementsByTagName('url')->length;
            if(isset($this->countRows)){
                if($this->countRows >= $this->defaultCountRow){
                    //echo $this->countRows.' - ';
                    //$this->currentFile++;
                    //$this->setCountRows($this->countRows);
                    //$this->getFileCount();
                }
            }
        }

        //$dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
    }

    public function createSitemap($counter)
    {
        $this->getFileCount();
        if($counter == 1) {
            //self::generateProducts($this->products, $this->domObject, $this->thisUrlset);
            self::generateRubrics($this->rubrics, $this->domObject, $this->thisUrlset);
            echo $counter;
        }
        else if($counter == 2) {
            self::generateProducts($this->products, $this->domObject, $this->thisUrlset);
            echo $counter;
        }

        else if($counter == 3){
            self::generatePageCategories($this->categoryPages, $this->domObject, $this->thisUrlset);
            echo $counter;
        }
        else if($counter == 4){
            self::generatePages($this->pages, $this->domObject, $this->thisUrlset);
            echo $counter;
        }
        else if($counter == 5){
            self::generateUsers($this->users, $this->domObject, $this->thisUrlset);
            echo $counter;
        }
        else if($counter == 6){
            self::generateCompanies($this->companies, $this->domObject, $this->thisUrlset);
            echo $counter;
        }
        else if($counter == 7){
            self::generateNewsCategories($this->categoryNews, $this->domObject, $this->thisUrlset);
            echo $counter;
        }
        else if($counter == 8){
            self::generateNews($this->news, $this->domObject, $this->thisUrlset);
            echo $counter;
        }
        else if($counter == 9){
            self::generateEvents($this->events, $this->domObject, $this->thisUrlset);
            //self::generateOther($this->domObject, $this->thisUrlset);
            echo $counter;
        }
        //echo $this->countRows;
        exit;
        //echo $counter; //exit;
        /*if($counter == 1){
            $dom = new \DOMDocument("1.0", "utf-8");
            $dom->load(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            $countAllRow = $dom->getElementsByTagName('url')->length;
            if(isset($countAllRow)){
                $this->setCountRows($countAllRow);
            }
            echo $this->countRows;

            exit;
            $urlset = $dom->createElement('urlset');
            $urlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');
            $urlset->setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
            $urlset->setAttribute('xsi:schemaLocation','http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');

            $urlset = self::generateRubrics($this->rubrics, $dom, $urlset);

            $dom->appendChild($urlset);
            $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
            echo $counter;
        }*/

    }

    private function generateRubrics($rubrics, $dom, $urlset)
    {
        $lastIter = count($rubrics) *2;
        $lastIterCount = 1;

        $iter = 0;

        foreach ($rubrics as $rubric) {
            foreach ($this->langs as $lang) {
                if($this->defaultCountRowVal > $iter+1){
                    $this->countRows++;
                    $url = $this->domObject->createElement('url');
                    $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'product/rubric/' . $rubric->alias);
                    $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.8']);
                    $this->thisUrlset->appendChild($url);
                    $iter++;
                    $lastIterCount++;

                    if($lastIterCount == $lastIter){
                        $this->saveThisXml($this->thisUrlset);
                        $this->thisUrlset->nodeValue = '';
                        //var_dump($urlset);
                        //echo $this->countRows;
                        return ;
                    }
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
                else{
                    $lastIterCount++;
                    $iter = 0;

                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }

                }
            }
        }
        //echo $this->countRows; exit;
        return $urlset;
    }

    private function generateProducts($products, $dom, $urlset)
    {
        $lastIter = count($products);
        $lastIterCount = 1;

        $iter = 0;
        foreach ($products as $product) {
            if($this->defaultCountRowVal > $iter+1) {
                $this->countRows++;
                $url = $this->domObject->createElement('url');
                $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/product/' . $product['alias']);
                $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                $this->thisUrlset->appendChild($url);
                $iter++;
                $lastIterCount++;

                if($lastIterCount == $lastIter){
                    $this->saveThisXml($this->thisUrlset);

                    $this->thisUrlset->nodeValue = '';

                    return ;
                }
                if($this->countRows <= $this->defaultCountRow){

                }
                else{
                    $this->saveThisXml($this->thisUrlset);
                }
            }
            else{
                $lastIterCount++;
                $iter = 0;
                if($this->countRows <= $this->defaultCountRow){

                }
                else{
                    $this->saveThisXml($this->thisUrlset);
                }
            }
        }

        return $urlset;
    }

    private function generatePageCategories($categories, $dom, $urlset)
    {
        $lastIter = count($categories)*2;
        $lastIterCount = 1;

        $iter = 0;
        foreach ($categories as $category) {
            foreach ($this->langs as $lang) {
                if($this->defaultCountRowVal >= $iter+1) {
                    $this->countRows++;
                    $url = $this->domObject->createElement('url');
                    $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'page/category/' . $category->alias);
                    $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.8']);
                    $this->thisUrlset->appendChild($url);

                    $iter++;
                    $lastIterCount++;

                    if($lastIterCount == $lastIter){
                        $this->saveThisXml($this->thisUrlset);

                        $this->thisUrlset->nodeValue = '';

                        return ;
                    }
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
                else{
                    $lastIterCount++;
                    $iter = 0;
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
            }
        }

        return $urlset;
    }

    private function generatePages($pages, $dom, $urlset)
    {
        $lastIter = count($pages)*2;
        $lastIterCount = 1;

        $iter = 0;
        foreach ($pages as $page) {
            foreach ($this->langs as $lang) {
                if($this->defaultCountRowVal >= $iter+1) {
                    $this->countRows++;
                    $url = $this->domObject->createElement('url');
                    $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'page/' . $page->alias);
                    $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                    $this->thisUrlset->appendChild($url);

                    $iter++;
                    $lastIterCount++;

                    if($lastIterCount == $lastIter){
                        $this->saveThisXml($this->thisUrlset);

                        $this->thisUrlset->nodeValue = '';

                        return ;
                    }
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
                else{
                    $lastIterCount++;
                    $iter = 0;
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
            }
        }

        return $urlset;
    }

    private function generateUsers($users, $dom, $urlset)
    {
        $lastIter = count($users);
        $lastIterCount = 1;

        $iter = 0;
        foreach ($users as $user) {
            if($this->defaultCountRowVal >= $iter+1) {
                $this->countRows++;
                $url = $this->domObject->createElement('url');
                $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/user/user/view/' . $user->id);
                $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                $this->thisUrlset->appendChild($url);

                $url = $this->domObject->createElement('url');
                $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/user/user/products/' . $user->id);
                $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                $this->thisUrlset->appendChild($url);

                $iter++;
                $lastIterCount++;

                if($lastIterCount == $lastIter){
                    $this->saveThisXml($this->thisUrlset);

                    $this->thisUrlset->nodeValue = '';

                    return ;
                }
                if($this->countRows <= $this->defaultCountRow){

                }
                else{
                    $this->saveThisXml($this->thisUrlset);
                }
            }
            else{
                $lastIterCount++;
                $iter = 0;
                if($this->countRows <= $this->defaultCountRow){

                }
                else{
                    $this->saveThisXml($this->thisUrlset);
                }
            }
        }

        return $urlset;
    }

    private function generateCompanies($companies, $dom, $urlset)
    {
        $lastIter = count($companies);
        $lastIterCount = 1;

        $iter = 0;
        foreach ($companies as $company) {
            if($this->defaultCountRowVal >= $iter+1) {
                $this->countRows++;
                $url = $this->domObject->createElement('url');
                $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/company/' . $company->id);
                $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                $this->thisUrlset->appendChild($url);

                $url = $this->domObject->createElement('url');
                $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/company/company/products/' . $company->id);
                $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                $this->thisUrlset->appendChild($url);

                $url = $this->domObject->createElement('url');
                $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/company/price/zakupka/' . $company->id);
                $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                $this->thisUrlset->appendChild($url);

                $url = $this->domObject->createElement('url');
                $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/company/company/feedbacks/' . $company->id);
                $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                $this->thisUrlset->appendChild($url);

                $iter++;
                $lastIterCount++;

                if($lastIterCount == $lastIter){
                    $this->saveThisXml($this->thisUrlset);

                    $this->thisUrlset->nodeValue = '';

                    return ;
                }
                if($this->countRows <= $this->defaultCountRow){

                }
                else{
                    $this->saveThisXml($this->thisUrlset);
                }
            }
            else{
                $lastIterCount++;
                $iter = 0;
                if($this->countRows <= $this->defaultCountRow){

                }
                else{
                    $this->saveThisXml($this->thisUrlset);
                }
            }
        }

        return $urlset;
    }

    private function generateNewsCategories($categories, $dom, $urlset) {
        $lastIter = count($categories)*2;
        $lastIterCount = 1;

        $iter = 0;
        foreach ($categories as $category) {
            foreach ($this->langs as $lang) {
                if($this->defaultCountRowVal >= $iter+1) {
                    $this->countRows++;
                    $url = $this->domObject->createElement('url');
                    $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'news/category/' . $category->alias);
                    $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.8']);
                    $this->thisUrlset->appendChild($url);

                    $iter++;
                    $lastIterCount++;

                    if($lastIterCount == $lastIter){
                        $this->saveThisXml($this->thisUrlset);

                        $this->thisUrlset->nodeValue = '';

                        return ;
                    }
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
                else{
                    $lastIterCount++;
                    $iter = 0;
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
            }
        }

        return $urlset;
    }

    private function generateNews($news, $dom, $urlset)
    {
        $lastIter = count($news)*2;
        $lastIterCount = 1;

        $iter = 0;
        foreach ($news as $one) {
            foreach ($this->langs as $lang) {
                if($this->defaultCountRowVal >= $iter+1) {
                    $this->countRows++;
                    $url = $this->domObject->createElement('url');
                    $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'news/' . $one->alias);
                    $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                    $this->thisUrlset->appendChild($url);

                    $iter++;
                    $lastIterCount++;

                    if($lastIterCount == $lastIter){
                        $this->saveThisXml($this->thisUrlset);

                        $this->thisUrlset->nodeValue = '';

                        return ;
                    }
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
                else{
                    $lastIterCount++;
                    $iter = 0;
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
            }
        }

        return $urlset;
    }

    private function generateEvents($events, $dom, $urlset) {
        $lastIter = count($events)*2;
        $lastIterCount = 1;

        $iter = 0;
        foreach ($events as $event) {
            foreach ($this->langs as $lang) {
                if($this->defaultCountRowVal >= $iter+1) {
                    $this->countRows++;
                    if (strripos($event->alias, '&') === false) {
                        $url = $this->domObject->createElement('url');
                        $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'event/' . $event->alias);
                        $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                        $this->thisUrlset->appendChild($url);

                        $iter++;
                        $lastIterCount++;

                        if($lastIterCount == $lastIter){
                            $this->saveThisXml($this->thisUrlset);

                            $this->thisUrlset->nodeValue = '';

                            return ;
                        }
                        if($this->countRows <= $this->defaultCountRow){

                        }
                        else{
                            $this->saveThisXml($this->thisUrlset);
                        }
                    }
                }
                else{
                    $lastIterCount++;
                    $iter = 0;
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
            }
        }

        return $urlset;
    }

    private function generateOther($dom, $urlset)
    {
        $other = [
            ['url' => 'instruments/analytics', 'multilang' => true],
            ['url' => '/instruments/fuel/prices', 'multilang' => false],
            ['url' => 'instruments/analytics-price', 'multilang' => true],
            ['url' => 'instruments', 'multilang' => true],
            ['url' => 'instruments/map-price-company', 'multilang' => true],
            ['url' => 'instruments/elevators/map', 'multilang' => true],
            ['url' => '/instruments/logistics-calculators', 'multilang' => false],
            ['url' => '/instruments/logistics-calculators/avto', 'multilang' => false],

            ['url' => 'company/price/zakupka', 'multilang' => true],
            ['url' => 'company/price/prodazha', 'multilang' => true],
            ['url' => 'instruments/quality-calculation', 'multilang' => true],
            ['url' => '/instruments/quality-calculation/pshenitsa', 'multilang' => false],
        ];

        $lastIter = count($other);
        $lastIterCount = 1;

        $iter = 0;
        foreach ($other as $item) {
            if($this->defaultCountRowVal >= $iter+1) {
                $this->countRows++;
                if ($item['multilang']) {
                    foreach ($this->langs as $lang) {
                        $url = $this->domObject->createElement('url');
                        $lock = $this->domObject->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . $item['url']);
                        $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                        $this->thisUrlset->appendChild($url);
                        $iter++;
                        $lastIterCount++;

                        if($lastIterCount == $lastIter){
                            $this->saveThisXml($this->thisUrlset);

                            $this->thisUrlset->nodeValue = '';

                            return ;
                        }
                        if($this->countRows <= $this->defaultCountRow){

                        }
                        else{
                            $this->saveThisXml($this->thisUrlset);
                        }
                    }
                } else {
                    $url = $this->domObject->createElement('url');
                    $lock = $this->domObject->createElement('loc', $this->frontendUrl . $item['url']);
                    $url = self::generateUrlChildrenNodes($url, $lock, $this->domObject, ['priority' => '0.5']);
                    $this->thisUrlset->appendChild($url);
                    $iter++;
                    $lastIterCount++;

                    if($lastIterCount == $lastIter){
                        $this->saveThisXml($this->thisUrlset);

                        $this->thisUrlset->nodeValue = '';

                        return ;
                    }
                    if($this->countRows <= $this->defaultCountRow){

                    }
                    else{
                        $this->saveThisXml($this->thisUrlset);
                    }
                }
            }
            else{
                $lastIterCount++;
                $iter = 0;
                if($this->countRows <= $this->defaultCountRow){

                }
                else{
                    $this->saveThisXml($this->thisUrlset);
                }
            }
        }

        return $urlset;
    }

    private function generateUrlChildrenNodes($url, $lock, $dom, $params = self::DEFAULT_PARAMS)
    {
        $url->appendChild($lock);

        $changefreqValue = (isset($params['changefreq']) && $params['changefreq']) ?
            $params['changefreq'] : self::DEFAULT_PARAMS['changefreq'];
        $changefreq = $dom->createElement('changefreq', $changefreqValue);
        $url->appendChild($changefreq);

        $priorityValue = (isset($params['priority']) && $params['priority']) ?
            $params['priority'] : self::DEFAULT_PARAMS['priority'];
        $priority = $dom->createElement('priority', $priorityValue);
        $url->appendChild($priority);

        return $url;
    }

    public function url($url)
    {
        return Yii::$app->urlManager->createAbsoluteUrl($url);
    }
}