<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.03.2018
 * Time: 13:17
 */

namespace common\helpers;


class MyHelpers
{
    public  static function typePrice($price)
    {
        $explodeParam = explode('.', $price);
        if(is_numeric($price)) {
            if (isset($explodeParam[1])) {
                return $price;
            } else {
                return $price . '.00';
            }
        }
        else
        {
            return $price;
        }
    }

    public  static function multiexplode($string)
    {
        $delimiters = [' ', '.', ',', '/', '|', ':', ';', '. ', ', ', '/ ', '| ', ': ', '; '];
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }
}