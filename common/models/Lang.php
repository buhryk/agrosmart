<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lang".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property integer $default
 * @property integer $date_update
 * @property integer $date_created
 * @property string $local

 */
class Lang extends \yii\db\ActiveRecord
{
    const ACTIVE_YES = 1;
    const ACTIVE_NOT = 0;

    static $current = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['default', 'date_update', 'date_created'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['url'], 'string', 'max' => 45],
            [['local'], 'string', 'max' => 245],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'default' => 'Default',
            'date_update' => 'Date Update',
            'date_created' => 'Date Created',
            'local' => 'Local',
        ];
    }


    static function getCurrent()
    {
        if( self::$current === null ){
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

    /*
     * set base path
     */
    static function setCurrent($url = null)
    {
        $language = self::getLangByUrl($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->local;
        if(self::$current->url == 'ru') {

        } else {
            Yii::$app->urlManager->baseUrl = '/'.self::$current->url;
        }

    }

//Получения объекта языка по умолчанию
    static function getDefaultLang()
    {
        return Lang::find()->where('`default` = :default', [':default' => 1])->one();
    }

//Получения объекта языка по буквенному идентификатору
    static function getLangByUrl($url = null)
    {
        if ($url === null) {
            return null;
        } else {
            $language = Lang::find()->where('url = :url', [':url' => $url])->one();
            if ( $language === null ) {
                return null;
            }else{
                return $language;
            }
        }
    }

}
