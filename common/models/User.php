<?php
namespace common\models;

use backend\modules\accesscontrol\models\Role;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const SCENARIO_CREATE = 'create';
    const SCENARIO_CHANGE_PASSWORD = 'change_password';

    public $password;
    public $confirm_password;

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['email', 'role_id'], 'required'],
            [['password', 'confirm_password'], 'required', 'on' => [
                self::SCENARIO_CREATE,
                self::SCENARIO_CHANGE_PASSWORD
            ]],
            ['email', 'email'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['phone', 'string', 'max' => 20],
            [['name', 'surname'], 'string'],
            ['username', 'required'],
            ['role_id', 'integer'],
            [['email', 'username', 'phone'], 'unique']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('users', 'ID'),
            'username' => 'Login',
            'name' => Yii::t('users', 'NAME'),
            'surname' => Yii::t('users', 'SURNAME'),
            'phone' => Yii::t('users', 'PHONE'),
            'role_id' => 'Role ID',
            'email' => 'Email'
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /*
     * @mfAVhp dropdown-bunch for views/rbac
     */
    /*public static function getUserName($user_id) {

        $models = static::find()
            ->where(['id' => $user_id])
            ->one();

        return $models['username'];
    }*/

    public function getUsername()
    {
        return $this->username;
    }

    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    public function beforeSave($insert)
    {
        if (!$this->auth_key) {
            $this->generateAuthKey();
        }

        if (!$this->password_hash || $this->scenario == self::SCENARIO_CHANGE_PASSWORD) {
            $this->setPassword($this->password);
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public static function getAllStatuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('users', 'USER_STATUS_ACTIVE'),
            self::STATUS_DELETED => Yii::t('users', 'USER_STATUS_DELETED')
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getAllStatuses()[$this->status]) ? self::getAllStatuses()[$this->status] : 'Undefined';
    }
}