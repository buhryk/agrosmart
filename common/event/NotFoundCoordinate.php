<?php
namespace common\event;

class NotFoundCoordinate {
    public $subsidiary;
    public $email = 'contacts@agro-smart.com.ua';

    public function __construct($subsidiary)
    {
        $this->subsidiary = $subsidiary;
    }

    public function send()
    {
        \Yii::$app->mailer->compose( ['html' => 'notFoundCoordinate'], ['subsidiary' => $this->subsidiary])
            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Не удалось определить координаты филиала ' . \Yii::$app->name)
            ->send();
    }
}