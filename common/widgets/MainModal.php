<?php

namespace common\widgets;

use yii\bootstrap\Widget;
use yii\bootstrap\Modal;
use yii\web\View;

/**
 * Модальное окно
 * @package backend\widgets
 * @author Tymkovskyi Aleksandr <tymkovskyi.aleksandr@gmail.com>
 */
class MainModal extends Widget
{
    /**
     * @var string идентификатор
     */
    public $id = 'kartik-modal';

    /**
     * Запуск виджета
     */
    public function run()
    {
        ?>
        <div class="modal  fade" id="<?=$this->id ?>" role="dialog"  aria-hidden="true" >
            <div class="modal-dialog" role="document">

            </div>
        </div>
    <?php


        $this->view->registerJs("
           $(document).on('ready pjax:success', function() {
                $('.modalButton').click(function(e) {
                    e.preventDefault();
                    $('#{$this->id}').modal().load($(this).attr('href'));   
                });
            });

        ", View::POS_READY);
    }
}
