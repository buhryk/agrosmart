<?php
namespace common\widgets;

use Yii;
use yii\base\View;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class Alert extends \yii\bootstrap\Widget
{

    public function run()
    {
        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();
        $js ='';

        foreach ($flashes as $type => $data) {
                $meesage = addslashes($data);
                $js.= "$.sticky('$meesage' , { stickyClass: '$type' }); ";
                $session->removeFlash($type);
        }

        $this->view->registerJs($js);
    }
}
