<?php
namespace common\widgets;
use common\models\Lang;

class WLangFrontend extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        return $this->render('wlang_frontend', [
            'current' => Lang::getCurrent(),
            'langs' => Lang::find()->all(),
            'default' => Lang::getDefaultLang()
        ]);
    }
}