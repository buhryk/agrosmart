<?php
use yii\helpers\Html;
?>

<ul class="lang-widget" id="langs">
    <?php foreach ($langs as $lang): ?>
        <li class="item-lang <?= $lang['url'] == $current['url'] ? 'active' : ''; ?>">
            <?php $language = $lang->url=='ru'?'':'/'.$lang->url; ?>
            <?= Html::a($lang->name, $language . Yii::$app->getRequest()->getLangUrl(), ['class']) ?>
        </li>
    <?php endforeach; ?>
</ul>


