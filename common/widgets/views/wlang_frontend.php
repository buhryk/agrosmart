<?php
use yii\helpers\Html;
?>

<p class="languages__select"><?= $current['name']; ?></p>
<ul class="navigation__languages">
    <?php foreach ($langs as $lang) { ?>
        <?php if ($lang['url'] == $current['url']) continue; ?>
        <li>
            <?php $language = $lang->url=='ru'?'':'/'.$lang->url; ?>
            <?= Html::a($lang->name, $language . Yii::$app->getRequest()->getLangUrl()) ?>
        </li>
    <?php } ?>
</ul>


