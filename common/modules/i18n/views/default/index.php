<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\modules\i18n\models\SourceMessage;
use common\modules\i18n\models\Message;

$this->title = \Yii::t('i18n', 'i18n');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="row">

    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Yii::t('i18n', 'SOURCE')?><br/>
                <?= Html::a(Yii::t('i18n', 'ADD'), ['/i18n/default/create-source'], ['class' => 'btn btn-success btn-sm']) ?>
            </div>
            <div class="panel-body">
            <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' =>$source_message_data_provider,
                    'filterModel' => $source_message_search_model,
                    'columns' => [
                        'id',
                        [
                            'attribute' => 'category',
                            'filter' => Html::activeDropDownList($source_message_search_model, 'category', array(null => \Yii::t('i18n', 'ALL')) + SourceMessage::getCategoryDropdown(), ['class' => 'form-control', 'multiple' => false]),
                        ],
                        'message',
                        [
                            'attribute' => 'created_at',
                            'format' => ['datetime'],
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Yii::t('i18n', 'TRANSLATIONS')?><br/>
                <?= Html::a(Yii::t('i18n', 'ADD'), ['/i18n/default/create-translation'], ['class' => 'btn btn-success btn-sm']) ?>
            </div>
            <div class="panel-body">
            <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $message_data_provider,
                    'filterModel' => $message_search_model,
                    'columns' => [
                        'id',
                        [
                            'attribute' => 'language',
                            'filter' => Html::activeDropDownList($message_search_model, 'language', array(null => \Yii::t('i18n', 'ALL')) + Message::getLanguageDropdown(), ['class' => 'form-control', 'multiple' => false]),
                        ],
                        'translation',
                        [
                            'attribute' => 'updated_at',
                            'format' => ['datetime'],
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}',
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
            </div>
        </div>
    </div>

</div>
