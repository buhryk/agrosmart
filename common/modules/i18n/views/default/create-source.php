<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\i18n\models\SourceMessage */

$this->title = Yii::t('i18n', 'ADD_SOURCE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('i18n', 'i18n'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-source', [
        'model' => $model,
    ]) ?>

</div>
