<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\i18n\models\Message */

$this->title = Yii::t('i18n', 'CREATE_TRANSLATION');
$this->params['breadcrumbs'][] = ['label' => Yii::t('i18n', 'i18n'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-translation', [
        'model' => $model,
    ]) ?>

</div>
