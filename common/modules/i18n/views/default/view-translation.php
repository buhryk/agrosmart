<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\i18n\models\Message */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('i18n', 'i18n'), 'url' => ['index']];
$this->params['breadcrumbs'][] = \Yii::t('i18n', 'TRANSLATIONS');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('i18n', 'UPDATE'), ['update', 'id' => $model->id, 'language' => $model->language], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'language',
            'translation:ntext',
        ],
    ]) ?>

</div>
