<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\i18n\models\SourceMessage;

/* @var $this yii\web\View */
/* @var $model common\modules\i18n\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-message-form">

    <div class="btn-group">
        <?= \Yii::t('i18n', 'CREATED') ?>
        <span class="caret dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>
        <div class="dropdown-menu">
            <?php foreach (SourceMessage::getCategoryDropdown() as $item) {
                        echo $item . '<br/>';
                    } ?>
        </div>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true, 'placeholder' => \Yii::t('i18n', 'SELECT_FROM_ABOVE_OR_CREATE_NEW')]) ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('i18n', 'ADD') : Yii::t('i18n', 'UPDATE'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
