<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\i18n\models\Message */

$this->title = Yii::t('i18n', 'UPDATE_TRANSLATION') . ' ' . $model->id . ' : ' . $model->language;
$this->params['breadcrumbs'][] = ['label' => Yii::t('i18n', 'i18n'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id, 'language' => $model->language]];
$this->params['breadcrumbs'][] = Yii::t('i18n', 'UPDATE');
?>
<div class="message-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-translation', [
        'model' => $model,
    ]) ?>

</div>
