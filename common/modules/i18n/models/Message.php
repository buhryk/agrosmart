<?php

namespace common\modules\i18n\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * @property SourceMessage $id0
 */
class Message extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language', 'translation'], 'required'],
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['language', 'translation'], 'string'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => SourceMessage::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('i18n', 'SOURCE_ID'),
            'language' => Yii::t('i18n', 'LANGUAGE'),
            'translation' => Yii::t('i18n', 'TRANSLATIONS'),
            'updated_at' => Yii::t('i18n', 'UPDATED_AT'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
    }
    /*
      * @inheritdoc
      * @return iteration of language for dropdown
      */
    public static function getLanguageDropdown() {

        $query = ArrayHelper::map(static::find()->select(['language', 'language'])->all(), 'language', 'language');

        return $query;
    }
}
