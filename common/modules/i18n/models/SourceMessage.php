<?php

namespace common\modules\i18n\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "source_message".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'source_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
            [['message', 'category'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('i18n', 'ID'),
            'category' => Yii::t('i18n', 'CATEGORY'),
            'message' => Yii::t('i18n', 'MESSAGE'),
            'created_at' => Yii::t('i18n', 'CREATED_AT'),
        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['id' => 'id']);
    }
    /*
      * @inheritdoc
      * @return iteration of category for dropdown
      */
    public static function getCategoryDropdown() {

        $query = ArrayHelper::map(static::find()->select(['category', 'category'])->all(), 'category', 'category');

        return $query;
    }
}
