<?php

namespace common\modules\i18n;

use Yii;
/**
 * i18n module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * replace main layout
     */
    public $layout = 'i18n';
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\i18n\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        self::registerTranslations();
    }

    public static function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations['i18n']) && !isset(Yii::$app->i18n->translations['i18n/*'])) {
            Yii::$app->i18n->translations['i18n'] = [
                // DB was created
                //'class' => 'yii\i18n\DbMessageSource',
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/i18n/messages',
                'sourceLanguage' => 'en-US',
                'forceTranslation' => true,
                'fileMap' => [
                    'i18n' => 'i18n.php',
                ],
                'on missingTranslation' => ['common\modules\i18n\TranslationEventHandler', 'handleMissingTranslation'],
            ];
        }
    }
}
