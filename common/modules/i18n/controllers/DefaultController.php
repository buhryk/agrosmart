<?php

namespace common\modules\i18n\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\modules\i18n\models\Message;
use common\modules\i18n\models\SourceMessage;
use common\modules\i18n\models\MessageSearch;
use common\modules\i18n\models\SourceMessageSearch;

/**
 * Default controller for the `i18n` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Renders the index view for the module
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        $message_search_model = new MessageSearch();
        $message_data_provider = $message_search_model->search(Yii::$app->request->queryParams);

        $source_message_search_model = new SourceMessageSearch();
        $source_message_data_provider = $source_message_search_model->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'message_search_model' => $message_search_model,
            'message_data_provider' => $message_data_provider,
            'source_message_search_model' => $source_message_search_model,
            'source_message_data_provider' => $source_message_data_provider,
        ]);
    }
    /**
     * Creates a new SourceMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateSource()
    {
        $model = new SourceMessage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', Yii::t('i18n', 'NEW_SOURCE_WAS_SUCCESSFULLY_ADDED'));
            return $this->render('view-source', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-source', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Creates a new Message model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateTranslation()
    {
        $model = new Message();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', Yii::t('i18n', 'NEW_TRANSLATION_WAS_SUCCESSFULLY_ADDED'));
            return $this->render('view-translation', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-translation', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Displays a single Message model.
     * @param integer $id
     * @param string $language
     * @return mixed
     */
    public function actionView($id, $language)
    {
        return $this->render('view-translation', [
            'model' => $this->findTranslationModel($id, $language),
        ]);
    }
    /**
     * Updates an existing Message model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $language
     * @return mixed
     */
    public function actionUpdate($id, $language)
    {
        $model = $this->findTranslationModel($id, $language);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', Yii::t('i18n', 'TRANSLATION_WAS_SUCCESSFULLY_UPDATED'));
            return $this->render('view-translation', [
                'model' => $model,
            ]);
        } else {

            return $this->render('update-translation', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Deletes an existing SourceMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findSourceModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the SourceMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findSourceModel($id)
    {
        if (($model = SourceMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Finds the Message model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $language
     * @return Message the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTranslationModel($id, $language)
    {
        if (($model = Message::findOne(['id' => $id, 'language' => $language])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /*
     * i18n EN/RU/UK engine for layouts dropdown switch
     * @return reload for current page
     */
    public function actionEn() {
            Yii::$app->session->set('LANG', 'en');
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionRu() {
            Yii::$app->session->set('LANG', '');
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionUk() {
            Yii::$app->session->set('LANG', 'uk');
        return $this->redirect(Yii::$app->request->referrer);
    }
}
