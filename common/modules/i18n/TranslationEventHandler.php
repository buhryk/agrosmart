<?php
/*
 * TranslationEventHandler.php
 *
 * Copyright 2016 mfAVhp <mfavhp@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
namespace common\modules\i18n;

use yii\i18n\MissingTranslationEvent;
use yii\helpers\Html;

class TranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        echo Html::a(\strtoupper($event->language) . ": {$event->category}<br/>{$event->message}", ['/i18n/default/index'], ['class' => 'btn btn-danger btn-xs']);
        $event->translatedMessage = "MISSING: {$event->category}.{$event->message}" . ' for language ' . \strtoupper($event->language);
    }
}
