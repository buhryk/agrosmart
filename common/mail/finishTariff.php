<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$link = Yii::$app->urlManager->createAbsoluteUrl(['/cabinet/tariff/view', 'id' => $data->package_id]);
?>
<div class="password-reset">
    <?=Yii::t('sendEmail', '<p>Здравствуйте, ваш тарифный план заканчивается через 3 дня.
     С уважением администрация сайта.</p>
     <a href="{url}"> Подробнее </a>', ['url' =>$link]) ?>

</div>
