<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$link = '//admin.agro-smart.com.ua/consumer/subsidiary/update?id='.$subsidiary->id;
?>
<div class="password-reset">
    <p>
        Не удалось определить координаты для компании : <?=$subsidiary->company->name ?><br>
        Для филиала c адресом : <?=$subsidiary->location ?>
    </p>
    <a href="<?=$link ?>">Исправить: <?=$link ?> </a>

</div>
