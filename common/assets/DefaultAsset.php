<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Legacy application asset bundle.
 */
class DefaultAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/default';

    public $css = [
    ];
    public $js = [
    ];
    public $img = [
    ];
    public $font = [
    ];
    public $vendor = [
    ];

    public function init()
    {
        parent::init();

        $dirs = ['css', 'js', 'img', 'font', 'vendor'];

        $assets = [];
        /*
         * @return array
         * fill current bundle
         */
        $scandir = function($root) use (&$assets, &$scandir) {

            $files = scandir($this->sourcePath . DIRECTORY_SEPARATOR . $root);

            $files = preg_grep("/(^\.|LICENSE|\.(.htaccess|md|json|txt)$)/", $files, PREG_GREP_INVERT);

            foreach ($files as $file) {

                if (!is_dir($this->sourcePath . DIRECTORY_SEPARATOR . $root . DIRECTORY_SEPARATOR . $file)) {

                    $assets[] = $root . DIRECTORY_SEPARATOR . $file;

                } else {

                    $scandir($root . DIRECTORY_SEPARATOR . $file);
                }
            }
            return $assets;
        };

        foreach ($dirs as $dir) {

            $this->{$dir} = array_merge($this->{$dir}, $scandir($dir));

            $assets = [];
        }
    }
}
