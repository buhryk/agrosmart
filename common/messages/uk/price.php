<?php
$connection = Yii::$app->db;
$connection->createCommand()->delete('source_message', 'category = "price"')->execute();

return [

    'cancel' =>' Відмінити',
    'my prices' =>'мої прайси',
    'updated_at' =>'Дата оновлення',
    'view' =>'Продивитись',
    'Edit' =>'Редагувати',
    'Update date' =>'Оновити дату',
    'Reload again' =>'Завантажити заново',
    'Download' =>'Завантажити',
    'Delete' =>'Видалити',
    'Price ' =>'Прайс',
    'denomination' => 'Найменування',
    'Go to company profile' => 'Перейти на профіль компанії',
    'cost' => 'Цена',
    'sale' => 'продажа',
    'purchase' => 'закупка',
    'Download more pricelists' => 'Завантажити більше прайсів',
    'PRICE PURCHASE' => 'Прайс закупки',
    'PRICE SALE' => 'Прайс продажи',
    'Search' => 'Пошук',
    'Not filled price name' => 'Не заповнена назва прайса, комірка',
    'Not filled price description' => 'Не заповнене описання прайса, комірка',
    'fill in if necessary' => 'заповнити, якщо треба',
    'No files found in the file' => 'У файлі не знайдено товарів',
    'Incomplete price denomination' => 'Не заповнене найменування прайса, комірка',
    'Not filled price list' => 'Не заповнена ціна прайса, комірка',
    'In the cell' => 'В комірці',
    'must be a numeric value' => 'повинно бути числове значення',
    'price-list' => 'прайс-лист',
    'Price list name' => 'Назва прайс-листа',
    'Price list description' => 'Опис прайс-листа',
    'Personal Cabinet' => 'Особистий кабінет',
    'Company' => 'компанії',
    'information window price' => 'В даному розділі ви можете завантажити ваші прайс-листи. Вони відображуються в розділі <a
                                href="#">Ціни
                            на с/х продукцію </a> та використовуються нами також для <a href="#">Аналітики цін </a>',
    'add-price list' => 'добавити прайс-листи',
    'Warning' => 'Помилка',
    'check the file and upload again' => 'Перевірте файл та <a href="#">завантажте його заново</a>',
    'Thank you! Your price has been successfully downloaded and published on the website' => '<p class="b-thanks__title">Дякуемо! Ваш прайс успішно завантажений і опублікований на сайті.</p>
                                    <p class="b-thanks__info">Ви можете подивитися його перейшовши по <a href="#" target="_blank">посиланню</a></p>',
    'upload price list' => 'Завантажити прайс-лист',
    'upload file' => ' <div class="b-title">Завантаження файла</div>
                                <p>Необхідно виконати наступні дії</p>',
    'download exel' => '<li>Заполните шаблон своими товарами
                                    <div class="b-list-num__text">
                                        Заполните шаблон своими данными (см. пример) так, чтобы в ячейке "A" было наименование товара, а
                                        в ячейке "B" - цена. Также можно заполнить ячейки "C,D,E", но необходимо будет указать наименование
                                        этих колонок в первой строке.
                                    </div>
                                </li>',
    'download template exel' => 'Завантажте Excel-шаблон',
    'download template' => 'скачать шаблон',
    'Upload your file' => 'Завантажте ваш файл',

    'selected file' => 'Виберіть файл',
    'No file selected' => 'Файл не вибран',
    'The file size must not exceed' => 'заповнений .xls файл, розмір файлу не повинен перевищувати 10 Mb',
    'Required field' => '<li>* - Поле обов\'язкове для заповнення</li>
                                        <li><a href="/ua/page/pravila-ispolzovaniya">Правила розміщення інформації</a></li>',
    'Enter text to search for' => 'Введіть текст для пошуку, наприклад: "насіння"',
    'Prices' => 'Прайси',
    'Reloading "{name}"' => 'Завантаження заново "{name}"',
];