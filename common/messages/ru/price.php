<?php

return [

    'cancel' =>' Отменить',
    'my prices' =>'мои прайсы',
    'updated_at' =>'Дата обновления',
    'view' =>'Посмотреть',
    'Edit' =>'Редактировать',
    'Update date' =>'Обновить дату',
    'Reload again' =>'Загрузить заново',
    'Download' =>'Скачать',
    'Delete' =>'Удалить',
    'Price' =>'Прайс',
    'Prices' =>'Прайсы',
    'VIEW ALL PRICES' => 'Смотреть все прайсы',
    'denomination' => 'Наименование',
    'Go to company profile' => 'Перейти на профиль компании',
    'cost' => 'Цена',
    'sale' => 'Продажа',
    'purchase' => 'Закупка',
    'Download more pricelists' => 'Загрузить больше прайсов',
    'PRICE PURCHASE' => 'Прайс закупки',
    'PRICE SALE' => 'Прайс продажи',
    'select a file' => 'Выберите файл для загрузки.',
    'The wrong format was selected' => 'Не правильный формат файла. Разрешена загрузка файлов только со следующими расширениями: xls, xlsx.',
    'Prices sale' => 'Прайсы продажи',
    'Prices purchase' => 'Прайсы закупки',
    'Search' => 'Поиск',
    'Not filled price name' => 'Не заполненое название прайса, ячейка',
    'Not filled price description' => 'Не заполненое описание прайса, ячейка',
    'No files found in the file' => 'В файле не найдено товаров',
    'Incomplete price denomination' => 'Не заполненое наименование прайса, ячейка',
    'Not filled price list' => 'Не заполненая цена прайса, ячейка',
    'Not filled price or name list' => 'Обязательные колонки "Наименование" и "Цена" должны быть заполнены. Ячейка',
    'In the cell' => 'В ячейке',
    'must be a numeric value' => 'должно быть числовое значение',
    'price-list' => 'прайс-лист',
    'This section is under construction' => 'Данный раздел находится в разработке',
    'Price list name' => 'Название прайс-листа',
    'Price list description' => 'Описание прайс-листа',
    'Personal Cabinet' => 'Личный кабинет',
    'Company' => 'Компании',
    'information window price' => 'В данном разделе вы можете загрузить ваши прайс-листы. Они показываются в разделе <a
                                href="#">Цены
                            на с/х продукцию </a> и используются нами также для <a href="#">Аналитики цен </a>',
    'add-price list' => 'добавить прайс-листы',
    'Warning' => 'Ошибка',
    'check the file and upload again' => 'Проверьте файл и <a href="#">загрузите его повторно</a>',
    'Thank you! Your price has been successfully downloaded and published on the website' => '<p class="b-thanks__title">Спасибо! Ваш прайс успешно загружен и опубликован на сайте.</p>',
    'You can see it by going to' => 'Вы можете посмотреть его перейдя по',
    'link' => 'ссылке',
    'My price' => 'Мои цены',
    'selection by price' => 'Подбор по цене',
    'publish price-list on site' => 'Опубликуйте прайс-лист на сайте:',
    'upload price list' => 'Загрузить прайс-лист',
    'published price list' => 'Опубликовать прайс-лист',
    'upload file' => ' <div class="b-title">Загрузка файла</div>
                                <p>Необходимо выполнить следующие действия</p>',
    'download exel' => '<li>Заполните шаблон своими товарами
                                    <div class="b-list-num__text">
                                        Заполните шаблон своими данными (см. пример) так, чтобы в ячейке "A" было наименование товара, а
                                        в ячейке "B" - цена. Также можно заполнить ячейки "C,D,E", но необходимо будет указать наименование
                                        этих колонок в первой строке.
                                    </div>
                                </li>',
    'download template exel' => 'Скачайте Excel-шаблон',
    'download template' => 'скачать шаблон',
    'Upload your file' => 'Загрузите ваш файл',

    'selected file' => 'Выберите файл',
    'No file selected' => 'Файл не выбран',
    'The file size must not exceed' => 'заполненный .xls файл, размер файла не должен превышать 10 Mb',
    'Required field' => '<li>* - Поле обязательно для заполнения</li>
                                        <li><a href="/page/pravila-ispolzovaniya">Правила размещения информации</a></li>',
    'Enter text to search for' => 'Введите текст для поиска, например: "семена"',
    'Reloading "{name}"' => 'Загрузка заново "{name}"',
];