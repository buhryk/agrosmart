<?php

use yii\db\Migration;

class m170110_081111_update_tables_structure extends Migration
{
    public function up()
    {
        $this->alterColumn('advertisement_image', 'advertisement_id', $this->integer()->notNull());
    }

    public function down()
    {
        echo "m170110_081111_update_tables_structure cannot be reverted.\n";

        return false;
    }
}
