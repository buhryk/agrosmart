<?php

use yii\db\Migration;

class m130532_201442_company_user_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%company_user}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'position' => "ENUM('select...', 'director', 'content-manager', 'sale-manager', 'broker', 'promoter') NOT NULL DEFAULT 'select...'",
        ], $tableOptions);
        $this->addForeignKey('user_fkcu', '{{%company_user}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('company_fkcu', '{{%company_user}}', 'company_id', '{{%company}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m130532_201442_company_user_init cannot be reverted.\n";
        $this->dropForeignKey('company_fkcu', '{{%company_user}}');
        $this->dropForeignKey('user_fkcu', '{{%company_user}}');
        $this->dropTable('{{%company_user}}');
    }
}
