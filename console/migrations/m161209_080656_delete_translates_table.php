<?php

use yii\db\Migration;

class m161209_080656_delete_translates_table extends Migration
{
    public function up()
    {
        $this->dropTable('translates');
    }

    public function down()
    {
        $this->createTable('translates', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->unique()->notNull(),
            'ru' => $this->text()->notNull(),
            'ua' => $this->text()->notNull(),
            'en' => $this->text()->notNull()
        ]);
    }
}
