<?php

use yii\db\Migration;

class m161216_104856_create_elevators_tables extends Migration
{
    public function up()
    {
        $this->createTable('elevator_model', [
            'id' => $this->primaryKey(),
            'address' => $this->string(255)->notNull(),
            'coordinates' => $this->string(255)->notNull(),
            'region_id' => $this->string(255)->notNull(),
            'property_type' => $this->integer()->notNull(),
            'accessibility' => $this->integer()->notNull(),
            'volume' => $this->integer()->notNull(),
            'loading' => $this->integer()->notNull(),
            'unloading' => $this->integer()->notNull(),
            'active' => $this->smallInteger(1)->defaultValue(1)->notNull(),
        ]);

        $this->createTable('elevator_model_lang', [
            'id' => $this->primaryKey(),
            'elevator_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(255)->null(),
        ]);

        $this->createTable('elevator_phone', [
            'id' => $this->primaryKey(),
            'elevator_id' => $this->integer()->notNull(),
            'phone' => $this->string(55)->notNull()
        ]);

        $this->createTable('elevator_region', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->unique()->notNull(),
        ]);

        $this->createTable('elevator_region_lang', [
            'id' => $this->primaryKey(),
            'region_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(255)->null(),
        ]);
    }

    public function down()
    {
        $this->dropTable('elevator_model');
        $this->dropTable('elevator_model_lang');
        $this->dropTable('elevator_phone');
        $this->dropTable('elevator_region');
        $this->dropTable('elevator_region_lang');
    }
}
