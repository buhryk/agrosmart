<?php

use yii\db\Migration;

class m161212_132844_change_field extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `news_tag` CHANGE `alias` `alias` VARCHAR( 55 ) NOT NULL ;');
    }

    public function down()
    {
        $this->execute('ALTER TABLE  `news_tag` CHANGE  `alias`  `alias` INT( 11 ) NOT NULL ;');
    }
}
