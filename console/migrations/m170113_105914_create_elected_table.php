<?php

use yii\db\Migration;

/**
 * Handles the creation of table `elected`.
 */
class m170113_105914_create_elected_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('elected', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'table_name' => $this->string(55)->notNull(),
            'record_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('elected');
    }
}
