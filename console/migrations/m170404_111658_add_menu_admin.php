<?php

use yii\db\Migration;

class m170404_111658_add_menu_admin extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%admin_menu}}', ['title', 'description', 'icon', 'path'], [
            ["Выбранные цены", "Выбранные цены","glyphicon glyphicon glyphicon-usd", '/price/selected-price'],
        ]);
    }

    public function down()
    {
        echo "m170404_111658_add_menu_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
