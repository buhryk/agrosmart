<?php

use yii\db\Migration;

class m170120_143825_change_user_and_company_tables_structure extends Migration
{
    public function up()
    {
        $this->addColumn('company', 'is_confirmed', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('consumer', 'can_add_advertisements', $this->integer()->notNull()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn('company', 'is_confirmed');
        $this->dropColumn('consumer', 'can_add_advertisements');
    }
}
