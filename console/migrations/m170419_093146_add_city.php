<?php

use yii\db\Migration;

class m170419_093146_add_city extends Migration
{
    public function up()
    {
        $this->execute("
        INSERT INTO `city` (`id`, `oblast_id`,  `type`) VALUES
        (27969, 20, 1);
        
        INSERT INTO `city_lang` (`id`, `city_id`, `lang`, `title`) VALUES
        (55931, 27969, 'ru-RU', 'Запорожье'),
        (55932, 27969, 'uk-UK', 'Запоріжжя');
        ");

    }

    public function down()
    {
        echo "m170419_093146_add_city cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
