<?php

use yii\db\Migration;

/**
 * Handles the creation of table `images`.
 */
class m161214_155547_create_images_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('images', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(55)->notNull(),
            'record_id' => $this->integer()->notNull(),
            'path' => $this->string(255)->notNull(),
            'active' => $this->smallInteger(1)->defaultValue(1)->notNull(),
            'sort' => $this->integer()->defaultValue(1)->notNull()
        ]);

        $this->createTable('images_lang', [
            'id' => $this->primaryKey(),
            'image_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(255)->null(),
            'alt' => $this->string(255)->null()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('images');
        $this->dropTable('images_lang');
    }
}
