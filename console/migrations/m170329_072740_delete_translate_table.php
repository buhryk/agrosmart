<?php

use yii\db\Migration;

class m170329_072740_delete_translate_table extends Migration
{
    public function up()
    {
        $this->dropTable('message');
        $this->dropTable('source_message');
    }

    public function down()
    {

    }


}
