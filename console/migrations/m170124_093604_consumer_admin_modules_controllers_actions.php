<?php

use yii\db\Migration;

class m170124_093604_consumer_admin_modules_controllers_actions extends Migration
{
    public function up()
    {
        $this->dropTable('enterpriser');
        $this->createTable('company_mark', [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'mark' => $this->float()->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
        ]);
    }

    public function down()
    {
        $this->dropTable('company_mark');
    }
}
