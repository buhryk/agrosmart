<?php

use yii\db\Migration;

class m170104_113719_create_currencies extends Migration
{
    public function up()
    {
        $this->createTable('currency', [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->unique()->notNull(),
            'code' => $this->string(5)->unique()->notNull(),
            'sign' => $this->string(5)->notNull(),
            'weight' => $this->float()->notNull()->defaultValue(1),
            'status' => $this->integer()->notNull()->defaultValue(1),
            'is_main' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->batchInsert('{{%currency}}', ['id', 'title', 'code', 'sign', 'weight', 'status', 'is_main'], [
            ["1", "Украинская гривна", "UAH", "грн.", "27", "1", "0"],
            ["2", "Доллар США", "USD", "$", "1", "1", "1"],
            ["3", "Российский рубль", "RUB", "р.", "66", "1", "0"],
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'parent_id', 'icon'], [
            ["25","/commondata/currency/index","Список валют","Список валют","16",""]
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["23", "8", "currency", "Валюты", "Управление валютами"],
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["111","23","index","Просмотр списка","Просмотр списка валют"],
            ["112","23","create","Создать","Добавление валюты"],
            ["113","23","view","Просмотреть","Просмотр валюты"],
            ["114","23","update","Редактировать","Редактирование валют"],
            ["115","23","delete","Удалить","Удаление валюты"],
        ]);

        $this->addColumn('advertisement', 'currency_id', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropTable('currency');
        $this->dropColumn('advertisement', 'currency_id');
    }
}
