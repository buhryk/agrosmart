<?php

use yii\db\Migration;

class m170407_154910_add_admin_menu extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%admin_menu}}', ['title', 'description', 'icon', 'path', 'parent_id'], [
            ["Пакеты список", "Пакеты ","glyphicon glyphicon-folder-open", '/package/package/index', '48'],
            ["Пользователи", "Пользователи ","", '/package/package-user/index', '48'],
        ]);
    }

    public function down()
    {
        echo "m170407_154910_add_admin_menu cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
