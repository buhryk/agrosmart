<?php

use yii\db\Migration;

class m170306_075259_add_colum_advertiswing extends Migration
{
    public function up()
    {
        $this->addColumn('advertising', 'link', $this->string(255));
    }

    public function down()
    {
        echo "m170306_075259_add_colum_advertiswing cannot be reverted.\n";

        return false;
    }

}
