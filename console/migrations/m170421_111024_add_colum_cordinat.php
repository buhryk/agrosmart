<?php

use yii\db\Migration;

class m170421_111024_add_colum_cordinat extends Migration
{
    public function up()
    {
        $this->addColumn('sale', 'coordinates', $this->string());
    }

    public function down()
    {
        echo "m170421_111024_add_colum_cordinat cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
