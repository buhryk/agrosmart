<?php

use yii\db\Migration;

/**
 * Class m180209_102137_replace_foreignkey_price_table
 */
class m180209_102137_replace_foreignkey_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-price-author_id', 'price');
        $this->addForeignKey(
            'fk-price-author_id2',
            'price',
            'author_id',
            'consumer',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180209_102137_replace_foreignkey_price_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180209_102137_replace_foreignkey_price_table cannot be reverted.\n";

        return false;
    }
    */
}
