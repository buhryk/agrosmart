<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dialog`.
 */
class m170214_085615_create_dialog_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('dialog', [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->notNull(),
            'to_user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createTable('dialog_detail', [
            'id' => $this->primaryKey(),
            'dialog_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(1),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->createTable('dialog_message', [
            'id' => $this->primaryKey(),
            'dialog_id' => $this->integer()->notNull(),
            'from_user_id' => $this->integer()->notNull(),
            'to_user_id' => $this->integer()->notNull(),
            'text' => $this->text()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('dialog');
        $this->dropTable('dialog_detail');
        $this->dropTable('dialog_message');
    }
}
