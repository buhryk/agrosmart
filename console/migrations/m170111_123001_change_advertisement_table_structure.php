<?php

use yii\db\Migration;

class m170111_123001_change_advertisement_table_structure extends Migration
{
    public function up()
    {
        $this->addColumn('advertisement', 'views_count', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('advertisement', 'views_count');
    }
}
