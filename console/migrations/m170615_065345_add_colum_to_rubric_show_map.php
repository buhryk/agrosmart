<?php

use yii\db\Migration;

class m170615_065345_add_colum_to_rubric_show_map extends Migration
{
    public function up()
    {
        $this->addColumn('rubric_model', 'show_map', $this->integer()->defaultValue(1));
    }

    public function down()
    {
        echo "m170615_065345_add_colum_to_rubric_show_map cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
