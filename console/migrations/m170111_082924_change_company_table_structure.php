<?php

use yii\db\Migration;

class m170111_082924_change_company_table_structure extends Migration
{
    public function up()
    {
        $this->dropColumn('company', 'type');
        $this->addColumn('company', 'type', $this->integer()->notNull()->defaultValue(1));
    }

    public function down()
    {
        echo "m170111_082924_change_company_table_structure cannot be reverted.\n";

        return false;
    }
}
