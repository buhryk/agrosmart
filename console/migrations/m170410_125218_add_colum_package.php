<?php

use yii\db\Migration;

class m170410_125218_add_colum_package extends Migration
{
    public function up()
    {
        $this->addColumn('package', 'is_page', $this->smallInteger()->defaultValue(1));
    }
}
