<?php

use yii\db\Migration;

/**
 * Handles the creation of table `complaint`.
 */
class m170116_120536_create_complaint_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('complaint', [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->notNull(),
            'table_name' => $this->string()->notNull(),
            'record_id' => $this->integer()->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->createTable('advertisement_complaint', [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->notNull(),
            'advertisement_id' => $this->integer()->notNull(),
            'types' => $this->string(128)->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->integer()->notNull()->defaultValue(0)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('complaint');
        $this->dropTable('advertisement_complaint');
    }
}
