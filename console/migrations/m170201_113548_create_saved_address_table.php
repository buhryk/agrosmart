<?php

use yii\db\Migration;

/**
 * Handles the creation of table `saved_address`.
 */
class m170201_113548_create_saved_address_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('saved_address', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'address' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('saved_address');
    }
}
