<?php

use yii\db\Migration;

class m130529_201442_user_adjust_column extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'name', $this->string()->notNull());
        $this->addColumn('user', 'surname', $this->string()->notNull());
        $this->addColumn('user', 'phone', $this->string(20)->notNull());
    }
}
