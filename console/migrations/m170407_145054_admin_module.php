<?php

use yii\db\Migration;

class m170407_145054_admin_module extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%module}}', ['id', 'alias', 'controller_namespace', 'title', 'description'], [
            ["21","translate","backend\\modules\\tariff\\controllers","Тарифы","Модуль тарифы для поднятия в топ"],
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["41","21","default","Перевод","Контроллер для тарифов для поднятия в топ"],
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["183","41","index","Просмотр списка","Просмотр списка тарифов"],
            ["184","41","create","Создание","Создание тарифов"],
            ["185","41","view","Просмотр","Просмотр тарифов"],
            ["186","41","update","Редактирование","Редактирование  тарифов"],
            ["187","41","delete","Удаление","Удаление тарифов"]
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["42","8","default","Пользовательские роли", "Пользовательские роли"],
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["188","42","index","Просмотр списка","Просмотр списка тарифов"],
            ["189","42","create","Создание","Создание тарифов"],
            ["190","42","view","Просмотр","Просмотр тарифов"],
            ["191","42","update","Редактирование","Редактирование  тарифов"],
            ["192","42","delete","Удаление","Удаление тарифов"]
        ]);

    }

    public function down()
    {
        echo "m170407_145054_admin_module cannot be reverted.\n";

        return false;
    }
}
