<?php

use yii\db\Migration;

class m161205_131454_measurement extends Migration
{
    public function up()
    {
        $this->createTable('rubric_measurement', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'weight' => $this->integer()->defaultValue(100)->notNull(),
        ]);

        $this->createTable('rubric_measurement_lang', [
            'id' => $this->primaryKey(),
            'measurement_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'short_title' => $this->string(128)->notNull(),
            'title' => $this->string(128)->notNull()
        ]);

        $this->createTable('rubric_measurement_category', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->notNull(),
        ]);

        $this->createTable('rubric_measurement_category_lang', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull()
        ]);

        $this->createTable('rubric_measurement_categories_rubrics', [
            'category_id' => $this->integer()->notNull(),
            'rubric_id' => $this->integer()->notNull(),
        ]);

    }

    public function down()
    {
        $this->dropTable('rubric_measurement');
        $this->dropTable('rubric_measurement_lang');
        $this->dropTable('rubric_measurement_category');
        $this->dropTable('rubric_measurement_category_lang');
        $this->dropTable('rubric_measurement_categories_rubrics');
    }
}
