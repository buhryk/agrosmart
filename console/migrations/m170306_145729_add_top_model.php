<?php

use yii\db\Migration;

class m170306_145729_add_top_model extends Migration
{
    public function up()
    {
        $this->createTable('top', [
            'id' => $this->primaryKey(),
            'model_name' => $this->string()->notNull(),
            'start' => $this->integer()->notNull(),
            'finish' => $this->integer()->notNull(),
            'refill_id' => $this->integer(),
            'type' => $this->string()->notNull(),
        ]);

        $this->createTable('refill', [
            'id' => $this->primaryKey(),
            'created_at' => $this->string()->notNull(),
            'type' => $this->integer()->notNull(),
            'num' => $this->string(40)->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('top');
        $this->dropTable('refill');
    }


}
