<?php

use yii\db\Migration;

class m170131_133858_change_tables_structure extends Migration
{
    public function up()
    {
        $this->createTable('{{%enterpriser}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
        ]);

        $this->addColumn('advertisement', 'email', $this->string(128)->notNull());
        $this->dropColumn('company', 'brief');
        $this->dropColumn('company', 'about');
        $this->dropColumn('company', 'title');
        $this->dropColumn('company', 'region_id');
        $this->addColumn('company', 'short_description', $this->text()->null());
        $this->addColumn('company', 'full_description', $this->text()->null());
        $this->dropColumn('advertisement', 'user_id');
        $this->addColumn('advertisement', 'from_user_id', $this->integer()->null());
        $this->addColumn('advertisement', 'from_subsidiary_id', $this->integer()->null());
    }

    public function down()
    {
        $this->dropTable('{{%enterpriser}}');
        $this->dropColumn('advertisement', 'email');
        $this->dropColumn('company', 'short_description');
        $this->dropColumn('company', 'full_description');
        $this->dropColumn('advertisement', 'from_user_id');
        $this->dropColumn('advertisement', 'from_subsidiary_id');
    }
}
