<?php

use yii\db\Migration;

class m170130_080114_sale_update extends Migration
{
    public function up()
    {
        $this->dropColumn('sale', 'location');

        $this->addColumn('sale', 'address', $this->string(300)->notNull());
        $this->addColumn('sale', 'measurement_id', $this->integer()->notNull());
        $this->addColumn('sale', 'currency_id', $this->integer()->notNull());
        $this->addColumn('sale', 'user_created', $this->integer()->notNull());
        $this->addColumn('sale', 'user_updated', $this->integer()->notNull());
        $this->addColumn('sale', 'city_id', $this->integer()->notNull());
        $this->addColumn('sale', 'oblast_id', $this->integer()->notNull());
        $this->addColumn('sale', 'date', $this->date()->notNull());
    }

    public function down()
    {
        echo "m170130_080114_sale_update cannot be reverted.\n";

        return false;
    }
}
