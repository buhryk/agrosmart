<?php

use yii\db\Migration;

class m161215_101620_change_some_tables_structure extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE  `event_model` DROP  `image` ;');
        $this->execute('ALTER TABLE  `news_model` DROP  `image` ;');
        $this->execute('ALTER TABLE  `news_category` DROP  `image` ;');

        $this->createTable('seo_lang', [
            'id' => $this->primaryKey(),
            'seo_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'meta_title' => $this->string(255)->null(),
            'meta_keywords' => $this->text()->null(),
            'meta_description' => $this->text()->null(),
        ]);

        $this->execute('ALTER TABLE  `seo` DROP  `meta_title` ;');
        $this->execute('ALTER TABLE  `seo` DROP  `meta_keywords` ;');
        $this->execute('ALTER TABLE  `seo` DROP  `meta_description` ;');

        $this->execute('ALTER TABLE  `images` ADD  `is_main` SMALLINT( 6 ) NOT NULL DEFAULT  \'0\';');
    }

    public function down()
    {
        $this->execute('ALTER TABLE  `event_model` ADD  `image` VARCHAR( 255 ) NULL DEFAULT NULL ;');
        $this->execute('ALTER TABLE  `news_model` ADD  `image` VARCHAR( 255 ) NULL DEFAULT NULL ;');
        $this->execute('ALTER TABLE  `news_category` ADD  `image` VARCHAR( 255 ) NULL DEFAULT NULL ;');

        $this->dropTable('seo_lang');

        $this->execute('ALTER TABLE  `seo` ADD  `meta_title` VARCHAR( 255 ) NULL DEFAULT NULL ;');
        $this->execute('ALTER TABLE  `seo` ADD  `meta_keywords` TEXT NULL DEFAULT NULL ;');
        $this->execute('ALTER TABLE  `seo` ADD  `meta_description` TEXT NULL DEFAULT NULL ;');

        $this->execute('ALTER TABLE  `images` DROP  `is_main` ;');
    }
}
