<?php

use yii\db\Migration;

/**
 * Class m180321_093021_add_settins_company
 */
class m180321_093021_add_settins_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
       INSERT INTO setting (alias, `value`, description)
        VALUES ('company-list', '7', 'Список id компаний для которых нужно показать раздел Мои цены');
    ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180321_093021_add_settins_company cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_093021_add_settins_company cannot be reverted.\n";

        return false;
    }
    */
}
