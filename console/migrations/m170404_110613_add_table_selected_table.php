<?php

use yii\db\Migration;

class m170404_110613_add_table_selected_table extends Migration
{
    public function up()
    {
        $this->createTable('selected_price',[
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'rubric_id' => $this->integer()->notNull(),
            'position' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ] );
    }

    public function down()
    {
       $this->dropTable('selected_price');
    }



}
