<?php

use yii\db\Migration;

class m170307_102304_add_table_tariff extends Migration
{
    public function up()
    {
        $this->createTable('tariff', [
            'id' =>$this->primaryKey(),
            'price' => $this->decimal(11.2)->notNull(),
            'count_deys' => $this->integer()->notNull(),
            'type' => $this->smallInteger(2),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'position' => $this->integer()->defaultValue(1),
            'model_name' => $this->string()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('tariff');
    }


}
