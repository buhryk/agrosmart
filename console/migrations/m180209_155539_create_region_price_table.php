<?php

use yii\db\Migration;

/**
 * Handles the creation of table `region_price`.
 */
class m180209_155539_create_region_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->dropTable('price_oblast');

        $this->createTable('price_oblast', [
            'price_id' => $this->integer()->notNull(),
            'oblast_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-price-price_oblast_p',
            'price_oblast',
            'price_id',
            'price',
            'id',
            'CASCADE'
        );

       $this->addPrimaryKey('pk-price-price_oblast-key', 'price_oblast', ['price_id', 'oblast_id']);


        $this->addForeignKey(
            'fk-price-price_oblast_o',
            'price_oblast',
            'oblast_id',
            'oblast',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('price_oblast');
    }
}
