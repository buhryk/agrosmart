<?php

use yii\db\Migration;

class m161221_132525_add_user_role_field extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'role_id', $this->smallInteger()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('user', 'role_id');
    }
}
