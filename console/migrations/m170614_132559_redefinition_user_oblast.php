<?php

use yii\db\Migration;

class m170614_132559_redefinition_user_oblast extends Migration
{
    public function up()
    {
        $models = \frontend\models\User::find()->andWhere(['IS NOT', 'oblast_id', NULL])->all();
        foreach ($models as $item) {
            $model = new \frontend\models\ConsumerOblast();
            $model->user_id = $item->id;
            $model->oblast_id = $item->oblast_id;
            $model->save();
        }
    }

    public function down()
    {
        echo "m170614_132559_redefinition_user_oblast cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
