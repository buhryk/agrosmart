<?php

use yii\db\Migration;

class m170412_072005_acounting_create extends Migration
{
    public function up()
    {
        $this->addColumn('accounting','natural_count' , $this->integer()->defaultValue(NULL));
    }

    public function down()
    {
        echo "m170412_072005_acounting_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
