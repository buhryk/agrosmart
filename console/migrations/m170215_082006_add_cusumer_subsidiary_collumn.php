<?php

use yii\db\Migration;

class m170215_082006_add_cusumer_subsidiary_collumn extends Migration
{
    public function up()
    {
        $this->addColumn('company_user', 'subsidiary_id', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('company_user', 'subsidiary_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
