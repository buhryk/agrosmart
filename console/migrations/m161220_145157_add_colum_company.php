<?php

use yii\db\Migration;

class m161220_145157_add_colum_company extends Migration
{
    public function up()
    {
        $this->addColumn('company', 'rubric_id', $this->integer()->defaultValue(null));
        $this->addColumn('company', 'region_id', $this->integer()->defaultValue(null));
    }


}
