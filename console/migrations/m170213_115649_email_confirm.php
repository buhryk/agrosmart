<?php

use yii\db\Migration;

class m170213_115649_email_confirm extends Migration
{
    public function up()
    {
        $this->addColumn('consumer', 'email_confirm_token', $this->string());

    }

    public function down()
    {
        $this->dropColumn('consumer', 'email_confirm_token');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
