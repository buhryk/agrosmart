<?php

use yii\db\Migration;

class m170109_141134_add_table_sale extends Migration
{
    public function up()
    {
        $this->createTable('{{%sale}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'rubric_id' => $this->integer()->notNull(),
            'volume' => $this->decimal(10,3)->notNull()->defaultValue(0),
            'price' => $this->decimal(10,2)->notNull()->defaultValue(0),
            'location' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'type' => $this->smallInteger()->notNull(),
        ]);
    }

    public function down()
    {

    }


}
