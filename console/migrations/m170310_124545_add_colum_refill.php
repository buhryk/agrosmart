<?php

use yii\db\Migration;

class m170310_124545_add_colum_refill extends Migration
{
    public function up()
    {
        $this->addColumn('refill' ,'order_id' , $this->string()->defaultValue(NULL));
    }

    public function down()
    {
        echo "m170310_124545_add_colum_refill cannot be reverted.\n";

        return false;
    }

}
