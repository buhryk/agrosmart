<?php

use yii\db\Migration;

class m170103_092246_advertisement extends Migration
{
    public function up()
    {
        $this->createTable('advertisement', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'alias' => $this->string(255)->unique()->notNull(),
            'text' => $this->text()->null()->defaultValue(null),
            'rubric_id' => $this->integer()->notNull(),
            'price' => $this->float()->null()->defaultValue(null),
            'volume' => $this->float()->null()->defaultValue(null),
            'measurement_id' => $this->integer()->null()->defaultValue(null),
            'user_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->null()->defaultValue(null),
            'phone' => $this->string(255)->null()->defaultValue(null),
            'created_at' => $this->integer()->defaultValue(0),
            'updated_at' => $this->integer()->defaultValue(0),
            'status' => $this->integer()->defaultValue(0),
            'active_to' => $this->integer()->null()->defaultValue(null),
            'type' => $this->integer()->defaultValue(1)
        ]);

        $this->createTable('advertisement_location', [
            'id' => $this->primaryKey(),
            'advertisement_id' => $this->integer()->notNull(),
            'region_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'address' => $this->string(255)->notNull()
        ]);

        $this->createTable('advertisement_image', [
            'id' => $this->primaryKey(),
            'advertisement_id' => $this->string(55)->notNull(),
            'path' => $this->string(255)->notNull(),
            'is_main' => $this->integer()->notNull()->defaultValue(0),
            'sort' => $this->integer()->notNull()->defaultValue(1)
        ]);
    }

    public function down()
    {
        $this->dropTable('advertisement');
        $this->dropTable('advertisement_location');
        $this->dropTable('advertisement_image');
    }
}
