<?php

use yii\db\Migration;

/**
 * Handles the creation of table `city`.
 */
class m170102_212018_create_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        //$this->dropTable('city');
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'oblast_id' => $this->integer()->notNull()
        ]);

        $this->createTable('city_lang', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('city');
        $this->dropTable('city_lang');
    }
}
