<?php

use yii\db\Migration;

class m161229_094702_add_countries_update_regions extends Migration
{
    public function up()
    {
        $this->truncateTable('event_country');
        $this->batchInsert('{{%event_country}}', ['id', 'alias'], [
            ["7", "belarus"],["6", "rossiya"],["2", "ukraina"]
        ]);

        $this->truncateTable('event_country_lang');
        $this->batchInsert('{{%event_country_lang}}', ['id', 'country_id', 'lang', 'title'], [
            ["3", "2", "ru-RU", "Украина"],
            ["4", "2", "uk-UK", "Україна"],
            ["5", "6", "ru-RU", "Россия"],
            ["6", "6", "uk-UK", "Росія"],
            ["7", "7", "ru-RU", "Беларусь"],
            ["8", "7", "uk-UK", "Білорусь"],
        ]);

        $this->execute('UPDATE `oblast` SET `country_id`=2 WHERE 1');
    }

    public function down()
    {

    }
}
