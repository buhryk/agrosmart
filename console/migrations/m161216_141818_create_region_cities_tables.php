<?php

use yii\db\Migration;

class m161216_141818_create_region_cities_tables extends Migration
{
    public function up()
    {
        $this->execute('DELETE FROM  `oblast` WHERE 1');
        $this->dropColumn('oblast', 'name');
        $this->addColumn('oblast', 'alias', $this->string(55)->unique()->null());

        $this->createTable('oblast_lang', [
            'id' => $this->primaryKey(),
            'oblast_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(55)->notNull(),
        ]);
    }

    public function down()
    {
        $this->addColumn('oblast', 'name', $this->string(55)->unique()->notNull());
        $this->dropColumn('oblast', 'alias');
        $this->dropTable('oblast_lang');
    }
}
