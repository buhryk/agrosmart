<?php

use yii\db\Migration;

class m170405_080521_add_colum_instrument extends Migration
{
    public function up()
    {
        $this->addColumn('instrument' ,'image_cabinet' , $this->string()->defaultValue(''));
    }

    public function down()
    {
        echo "m170405_080521_add_colum_instrument cannot be reverted.\n";

        return false;
    }

}
