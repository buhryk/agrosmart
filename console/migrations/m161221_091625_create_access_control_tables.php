<?php

use yii\db\Migration;

class m161221_091625_create_access_control_tables extends Migration
{
    public function up()
    {
        $this->createTable('role', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(55)->unique()->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull()
        ]);

        $this->createTable('module', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->notNull(),
            'controller_namespace' => $this->string(255)->unique()->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull(),
        ]);

        $this->createTable('module_controller', [
            'id' => $this->primaryKey(),
            'module_id' => $this->integer()->notNull(),
            'alias' => $this->string(128)->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull(),
        ]);

        $this->createTable('module_controller_action', [
            'id' => $this->primaryKey(),
            'controller_id' => $this->integer()->notNull(),
            'alias' => $this->string(128)->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull()
        ]);

        $this->createTable('role_action_access', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer()->notNull(),
            'role_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('admin_menu', [
            'id' => $this->primaryKey(),
            'path' => $this->string(128)->unique()->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull(),
        ]);

        $this->createTable('admin_menu_role_access', [
            'id' => $this->primaryKey(),
            'menu_id' => $this->integer()->notNull(),
            'role_id'=> $this->integer()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('role');
        $this->dropTable('module');
        $this->dropTable('module_controller');
        $this->dropTable('module_controller_action');
        $this->dropTable('role_action_access');
        $this->dropTable('admin_menu');
        $this->dropTable('admin_menu_role_access');
    }

}
