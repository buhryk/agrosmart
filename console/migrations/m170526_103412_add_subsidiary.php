<?php

use yii\db\Migration;
use common\helpers\GoogleMapsApiHelper;

class m170526_103412_add_subsidiary extends Migration
{
    public function up()
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();
        $models = \frontend\modules\cabinet\models\Subsidiary::find()->all();

        foreach ($models as $model) {
            if ($model->coordinates == 'null' or  !$model->coordinates) {
                $coordinates = $googleMapsApiHelper->getLocationByAddress($model->location);
                if (!$coordinates) {
                    $city = $model->city;
                    $region = $city->region;
                    $address = $city->title . ', ' . $region->title;
                    $coordinates = $googleMapsApiHelper->getLocationByAddress($address);
                    if ($coordinates) {
                        $coordinates = $googleMapsApiHelper->getLocationByAddress($city->title);
                    }
                }

                $model->coordinates = json_encode($coordinates);
                $model->save();

            }
        }
        return true;

    }

    public function down()
    {
        echo "m170526_103412_add_subsidiary cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
