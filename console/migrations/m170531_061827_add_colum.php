<?php

use yii\db\Migration;

class m170531_061827_add_colum extends Migration
{
    public function up()
    {
        $this->addColumn('sale', 'description_address', $this->string()->defaultValue(NULL));
    }

    public function down()
    {
        echo "m170531_061827_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
