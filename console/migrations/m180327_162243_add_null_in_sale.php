<?php

use yii\db\Migration;

/**
 * Class m180327_162243_add_null_in_sale
 */
class m180327_162243_add_null_in_sale extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('sale', 'user_updated', $this->integer()->null());
        $this->alterColumn('sale', 'user_created', $this->integer()->null());
        $this->alterColumn('sale', 'created_at', $this->integer()->null());
        $this->alterColumn('sale', 'updated_at', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180327_162243_add_null_in_sale cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180327_162243_add_null_in_sale cannot be reverted.\n";

        return false;
    }
    */
}
