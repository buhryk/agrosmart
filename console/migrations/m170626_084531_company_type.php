<?php

use yii\db\Migration;

class m170626_084531_company_type extends Migration
{
    public function up()
    {
        Yii::$app->db->createCommand()->update('company', ['type' => 4], ['type' => 5])->execute();
    }

    public function down()
    {
        echo "m170626_084531_company_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
