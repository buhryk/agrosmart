<?php

use yii\db\Migration;

class m170103_144211_create_table_area extends Migration
{
    public function up()
    {
        $this->createTable('area', [
            'id' => $this->primaryKey(),
            'oblast_id' => $this->integer()->notNull()
        ]);

        $this->createTable('area_lang', [
            'id' => $this->primaryKey(),
            'area_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
        ]);

        $this->addColumn('city', 'area_id', $this->integer()->notNull());
        $this->addColumn('city', 'type', $this->integer(1)->notNull());
    }

    public function down()
    {
        $this->dropTable('{{%area}}');
        $this->dropTable('{{%area_lang}}');
    }

}
