<?php

use yii\db\Migration;

class m170328_143225_add_user extends Migration
{
    public function up()
    {
        $this->addColumn('contact', 'user_id', $this->integer()->defaultValue(NULL));
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
