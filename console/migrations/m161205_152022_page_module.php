<?php

use yii\db\Migration;

class m161205_152022_page_module extends Migration
{
    public function up()
    {
        $this->createTable('page_category', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->defaultValue(null)->null(),
            'alias' => $this->string(128)->unique()->notNull(),
            'active' => $this->integer()->defaultValue(1)->notNull()
        ]);

        $this->createTable('page_category_lang', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
        ]);

        $this->createTable('page_model', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->null(),
            'alias' => $this->string(128)->unique()->notNull(),
            'active' => $this->integer()->defaultValue(1)->notNull()
        ]);

        $this->createTable('page_model_lang', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'short_description' => $this->text()->null(),
            'text' => $this->text()->notNull()
        ]);

        $this->createTable('seo', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(128)->notNull(),
            'record_id' => $this->integer()->notNull(),
            'meta_title' => $this->string(255)->null(),
            'meta_keywords' => $this->text()->null(),
            'meta_description' => $this->text()->null()
        ]);
    }

    public function down()
    {
        $this->dropTable('page_category');
        $this->dropTable('page_category_lang');
        $this->dropTable('page_model');
        $this->dropTable('page_model_lang');
        $this->dropTable('seo');
    }
}
