<?php

use yii\db\Migration;

class m170211_112623_update_complaint_table_structure extends Migration
{
    public function up()
    {
        $this->addColumn('complaint', 'types', $this->string(128)->notNull());
        $this->dropTable('advertisement_complaint');
    }

    public function down()
    {
        $this->dropColumn('complaint', 'types');
    }
}
