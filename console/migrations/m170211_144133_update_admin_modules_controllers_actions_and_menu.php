<?php

use yii\db\Migration;

class m170211_144133_update_admin_modules_controllers_actions_and_menu extends Migration
{
    public function up()
    {
        $this->execute('UPDATE `module_controller` SET `alias` = \'complaint\', `title` = \'Жалобы\', `description` = \'Контроллер для управления жалобами\' WHERE `module_controller`.`id` = 25;');
        $this->execute('DELETE FROM `admin_menu` WHERE `admin_menu`.`id` = 22;');
        $this->execute('DELETE FROM `admin_menu` WHERE `admin_menu`.`id` = 23;');
        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'parent_id', 'icon'], [
            ["32","/complaint/complaint/index","Жалобы","Модуль жалоб","","glyphicon glyphicon-headphones"],
        ]);
    }

    public function down()
    {
        echo "m170211_144133_update_admin_modules_controllers_actions_and_menu cannot be reverted.\n";

        return false;
    }
}
