<?php

use yii\db\Migration;

class m170524_065527_add_colum_subsidiary extends Migration
{
    public function up()
    {
        $this->addColumn('subsidiary', 'coordinates', $this->string()->defaultValue(NULL));
    }

    public function down()
    {
        echo "m170524_065527_add_colum_subsidiary cannot be reverted.\n";

        return false;
    }

}
