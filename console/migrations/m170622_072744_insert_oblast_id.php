<?php

use yii\db\Migration;

class m170622_072744_insert_oblast_id extends Migration
{
    public function up()
    {
        $models = \frontend\models\ConsumerOblast::find()->all();

        foreach ($models as $item) {
            $model = \frontend\models\User::findOne($item->user_id);
            $model->oblast_id = $item->oblast_id;
            $model->save();
        }
    }

    public function down()
    {
        echo "m170622_072744_insert_oblast_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
