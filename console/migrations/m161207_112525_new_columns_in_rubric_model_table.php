<?php

use yii\db\Migration;

class m161207_112525_new_columns_in_rubric_model_table extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `rubric_model` ADD `alias` VARCHAR( 128 ) NULL DEFAULT NULL ;');
        $this->execute('ALTER TABLE `rubric_model` ADD `sort` INT NULL DEFAULT \'100\';');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `rubric_model` DROP `alias` ;');
        $this->execute('ALTER TABLE `rubric_model` DROP `sort` ;');
    }
}
