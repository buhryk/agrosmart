<?php

use yii\db\Migration;
use common\helpers\GoogleMapsApiHelper;

class m170524_103319_subsidiary_geocoding extends Migration
{
    public function up()
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();
        $models = \frontend\modules\cabinet\models\Subsidiary::find()->all();

        foreach ($models as $model) {
            $cordinate = json_encode($googleMapsApiHelper->getLocationByAddress($model->location));
            if($cordinate) {
                $model->coordinates = json_encode($cordinate);
                $model->save();
            }

        }
        return true;

    }

    public function down()
    {
        echo "m170524_103319_subsidiary_geocoding cannot be reverted.\n";

        return false;
    }

}
