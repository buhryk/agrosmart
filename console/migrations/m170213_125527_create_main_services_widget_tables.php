<?php

use yii\db\Migration;

class m170213_125527_create_main_services_widget_tables extends Migration
{
    public function up()
    {
        $this->createTable('main_services_widget', [
            'id' => $this->primaryKey(),
            'sort' => $this->integer()->notNull()->defaultValue(1),
            'active' => $this->integer()->notNull()->defaultValue(1),
            'type' => $this->integer()->notNull()->defaultValue(1)
        ]);
        $this->createTable('main_services_widget_lang', [
            'id' => $this->primaryKey(),
            'record_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'url' => $this->string(128)->notNull(),
            'image' => $this->string(255)->null()
        ]);


        $this->batchInsert('{{%module}}', ['id', 'alias', 'controller_namespace', 'title', 'description'], [
            ["14","mainservices","backend\\modules\\mainservices\\controllers","Главный виджет сервисов","Главный виджет сервисов"]
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["29","14","block","Блок","Контроллер для создания, просмотра, редактирования и удаления блоков в главном виджете сервисов"]
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["137","29","index","Просмотр списка","Просмотр списка блоков"],
            ["138","29","view","Просмотр","Просмотр блока"],
            ["139","29","create","Создание","Создание блока"],
            ["140","29","update","Редактирование","Редактирование блока"],
            ["141","29","delete","Удаление","Удаление блока"]
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'parent_id', 'icon'], [
            ["34","/mainservices/block/index","Виджет сервисов","Модуль настройки виджета сервисов","","glyphicon glyphicon-th-large"]
        ]);
    }

    public function down()
    {
        $this->dropTable('main_services_widget');
        $this->dropTable('main_services_widget_lang');
    }
}
