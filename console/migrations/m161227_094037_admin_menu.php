<?php

use yii\db\Migration;

class m161227_094037_admin_menu extends Migration
{
    public function up()
    {
        $this->addColumn('admin_menu', 'parent_id', $this->integer()->null()->defaultValue(null));
        $this->addColumn('admin_menu', 'icon', $this->string(55)->null()->defaultValue(null));
        $this->dropColumn('admin_menu', 'path');
        $this->addColumn('admin_menu', 'path', $this->string(128)->null()->defaultValue(null));

        $this->execute('UPDATE `module` SET `controller_namespace`="backend\\modules\\page\\controllers" WHERE `id`=3');

        $this->truncateTable('admin_menu');
        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'parent_id', 'icon'], [
            ["1","","Рубрикатор","Рубрикатор","","glyphicon glyphicon-th-list"],
            ["2","/rubric/rubric/index","Список рубрик","Страница списка рубрик","1",""],
            ["3","/rubric/measurement-category/index","Единицы измерений","Страница списка единиц измерений","1",""],
            ["4","","Страницы","Модуль страниц","","glyphicon glyphicon-duplicate"],
            ["5","/page/page/index","Список страниц","Список страниц","4",""],
            ["6","/page/category/index","Список категорий","Список категорий страниц","4",""],
            ["7","","Новости","Модуль новостей","","glyphicon glyphicon-file"],
            ["8","/news/news/index","Список новостей","Список новостей","7",""],
            ["9","/news/category/index","Список категорий","Список категорий новостей","7",""],
            ["10","/news/tag/index","Список тегов","Список тегов новостей","7",""],
            ["11","","События","Модуль событий","","glyphicon glyphicon-time"],
            ["12","/event/event/index","Список событий","Список событий","11",""],
            ["13","/event/subject/index","Тематики","Список тематик событий","11",""],
            ["14","/event/type/index","Типы","Список типов событий","11",""],
            ["15","/elevator/elevator/index","Элеваторы","Модуль элеваторов","","glyphicon glyphicon-flag"],
            ["16","","Общие данные","Модуль общих данных","","glyphicon glyphicon-option-horizontal"],
            ["17","/commondata/region/index","Список регионов","Список регионов","16",""],
            ["18","/commondata/country/index","Список стран","Список стран","16",""],
            ["19","","Настройка доступов","Модуль настройки доступов","","glyphicon glyphicon-eye-open"],
            ["20","/accesscontrol/role/index","Список ролей","Список ролей пользователей админ-панели","19",""],
            ["21","/accesscontrol/module/index","Список модулей","Список модулей приложения","19",""],
            ["24","/adminmenu/menu/index","Меню админ панели","Список пунктов меню админ панели","19",""]
        ]);
    }

    public function down()
    {
        $this->dropColumn('admin_menu', 'parent_id');
        $this->dropColumn('admin_menu', 'icon');
        $this->alterColumn('admin_menu', 'path', $this->string(128)->unique()->notNull());
        $this->addColumn('admin_menu', 'path', $this->string(128)->unique()->notNull());
    }
}
