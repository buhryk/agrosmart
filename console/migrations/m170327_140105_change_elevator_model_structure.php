<?php

use yii\db\Migration;

class m170327_140105_change_elevator_model_structure extends Migration
{
    public function up()
    {
        $this->alterColumn('elevator_model', 'property_type', $this->integer()->null()->defaultValue(null));
        $this->alterColumn('elevator_model', 'volume', $this->integer()->null()->defaultValue(null));
        $this->alterColumn('elevator_model', 'loading', $this->integer()->null()->defaultValue(null));
        $this->alterColumn('elevator_model', 'unloading', $this->integer()->null()->defaultValue(null));
        $this->alterColumn('elevator_model', 'laboratory', $this->integer()->null()->defaultValue(null));
    }

    public function down()
    {
        echo "m170327_140105_change_elevator_model_structure cannot be reverted.\n";

        return false;
    }
}
