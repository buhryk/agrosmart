<?php

use yii\db\Migration;

class m130531_201442_company_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'type' => "ENUM('select...', 'trader', 'farmer', 'logician', 'distributer', 'dealer', 'processor') NOT NULL DEFAULT 'select...'",
        ], $tableOptions);
    }

    public function down()
    {
        echo "m130531_201442_company_init cannot be reverted.\n";
        $this->dropTable('{{%company}}');
    }
}
