<?php

use yii\db\Migration;

class m170307_101109_add_colum_top extends Migration
{
    public function up()
    {
        $this->addColumn('top', 'model_id', $this->integer()->notNull());
    }

    public function down()
    {
        echo "m170307_101109_add_colum_top cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
