<?php

use yii\db\Migration;

class m170118_141947_update_advertisement_table extends Migration
{
    public function up()
    {
        $this->dropTable('advertisement_location');
        $this->dropColumn('advertisement', 'phone');
        $this->addColumn('advertisement', 'city_id', $this->integer()->notNull());
        $this->addColumn('advertisement', 'address', $this->string(255)->notNull());
        $this->addColumn('advertisement', 'phones', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropTable('advertisement_phone');
        $this->dropColumn('advertisement', 'city_id');
        $this->dropColumn('advertisement', 'address');
        $this->dropColumn('advertisement', 'phones');
    }
}
