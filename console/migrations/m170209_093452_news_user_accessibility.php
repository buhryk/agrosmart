<?php

use yii\db\Migration;

class m170209_093452_news_user_accessibility extends Migration
{
    public function up()
    {
        $this->createTable('news_user_accessibility', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull()
        ]);

        $this->createTable('news_user_accessibility_type', [
            'id' => $this->primaryKey(),
            'description' => $this->string(55)->notNull(),
        ]);

        $this->batchInsert('{{%news_user_accessibility_type}}', ['id', 'description'], [
            ["1","Трейдерам"],
            ["2","Фермерам"],
            ["3","Логистам"],
            ["4","Дистрибьюторам"],
            ["5","Дилерам"],
            ["6","Переработчикам"],
            ["7","Незарегистрированным пользователям"],
            ["8","Зарегистрированным пользователям без компании"],
            ["9","Всем пользователям"]
        ]);
    }

    public function down()
    {
        $this->dropTable('news_user_accessibility');
        $this->dropTable('news_user_accessibility_type');
    }
}
