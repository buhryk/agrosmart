<?php

use yii\db\Migration;

class m161220_084101_add_elevator_new_column extends Migration
{
    public function up()
    {
        $this->addColumn('elevator_model', 'address_confirmed', $this->integer()->defaultValue(0)->notNull());
    }

    public function down()
    {
        $this->dropColumn('elevator_model', 'address_confirmed');
    }
}
