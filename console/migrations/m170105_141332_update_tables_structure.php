<?php

use yii\db\Migration;

class m170105_141332_update_tables_structure extends Migration
{
    public function up()
    {
        $this->dropColumn('advertisement_location', 'region_id');
    }

    public function down()
    {
        $this->addColumn('advertisement_location', 'region_id', $this->integer()->notNull());
    }
}
