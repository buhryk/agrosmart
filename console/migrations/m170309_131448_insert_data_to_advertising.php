<?php

use yii\db\Migration;

class m170309_131448_insert_data_to_advertising extends Migration
{
    public function up()
    {
        $this->execute("
               INSERT INTO `advertising_group` (`key`, `name`) VALUES
                ('header', 'Верхний блок'),
                ('content', 'Правый блок'),
                ('footer', 'Нижний блок');
        ");
    }

    public function down()
    {
        echo "m170309_131448_insert_data_to_advertising cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
