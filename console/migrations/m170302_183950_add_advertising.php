<?php

use yii\db\Migration;

class m170302_183950_add_advertising extends Migration
{
    public function up()
    {
        $this->addColumn('advertising_position', 'width', $this->integer()->notNull());
        $this->addColumn('advertising_position', 'height', $this->integer()->notNull());
    }

    public function down()
    {
        echo "m170302_183950_add_advertising cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
