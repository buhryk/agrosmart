<?php

use yii\db\Migration;

class m170327_114130_add_culum extends Migration
{
    public function up()
    {
        $this->addColumn('advertising_group', 'active' , $this->integer()->defaultValue(1));
    }

    public function down()
    {
        echo "m170327_114130_add_culum cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
