<?php

use yii\db\Migration;

class m161219_081549_added_regions_values extends Migration
{
    public function up()
    {
       // $this->dropForeignKey('oblast_fker', 'elevator_requisite');
        $this->truncateTable('oblast_lang');
        $this->truncateTable('oblast');
      // $this->addForeignKey('oblast_fker', '{{%elevator_requisite}}', 'oblast_id', '{{%oblast}}', 'id', 'CASCADE', 'CASCADE');

        $this->batchInsert('{{%oblast}}', ['id', 'alias'], [
            ["25","ar-krym"],["1","cherkasskaya-obl"],["2","chernigovskaya-obl"],["3","chernovitskaya-obl"],
            ["4","dnepropetrovskaya-obl"],["5","donetskaya-obl"],["6","ivano-frankovskaya-obl"],["7","kharkovskaya-obl"],
            ["8","khersonkaya-obl"],["9","khmelnitskaya-obl"],["10","kievskaya-obl"],["23","kirovogradskaya-obl"],
            ["11","luganskaya-obl"],["20","lutskaya-obl"],["12","lvovskaya-obl"],["13","nikolaevskaya-obl"],
            ["14","odesskaya-obl"],["15","poltavskaya-obl"],["16","rovenskaya-obl"],["17","sumskaya-obl"],
            ["18","ternopolskaya-obl"],["19","vinnitskaya-obl"],["22","volynskaya-obl"],["24","zakarpatskaya-obl"],
            ["21","zhitomirskaya-obl"]
        ]);

        $this->batchInsert('{{%oblast_lang}}', ['id', 'oblast_id', 'lang', 'title'], [
            ["1","1","ru-RU","Черкасская обл"],["2","1","uk-UK","Черкаська обл"],["3","2","ru-RU","Черниговская обл"],
            ["4","2","uk-UK","Чернігівська обл"],["5","3","uk-UK","Чернівецька обл"],["6","3","ru-RU","Черновицкая обл"],
            ["7","4","uk-UK","Дніпропетровська обл"],["8","4","ru-RU","Днепропетровская обл"],["9","5","ru-RU","Донецкая обл"],
            ["10","5","uk-UK","Донецька обл"],["11","6","uk-UK","Івано-Франківська обл"],["12","6","ru-RU","Ивано-Франковская обл"],
            ["13","7","uk-UK","Харківська обл"],["14","7","ru-RU","Харьковская обл"],["15","8","uk-UK","Херсонська обл"],
            ["16","8","ru-RU","Херсонкая обл"],["17","9","uk-UK","Хмельницька обл"],["18","9","ru-RU","Хмельницкая обл"],
            ["19","10","ru-RU","Киевская обл"],["20","10","uk-UK","Київська обл"],["21","11","uk-UK","Луганська обл"],
            ["22","11","ru-RU","Луганская обл"],["23","12","uk-UK","Львівська обл"],["24","12","ru-RU","Львовская обл"],
            ["25","13","ru-RU","Николаевская обл"],["26","13","uk-UK","Миколаївська обл"],["27","14","ru-RU","Одесская обл"],
            ["28","14","uk-UK","Одеська обл"],["29","15","ru-RU","Полтавская обл"],["30","15","uk-UK","Полтавська обл"],
            ["31","16","ru-RU","Ровенская обл"],["32","16","uk-UK","Рівненьска обл"],["33","17","uk-UK","Сумська обл"],
            ["34","17","ru-RU","Сумская обл"],["35","18","uk-UK","Тернопільська обл"],["36","18","ru-RU","Тернопольская обл"],
            ["37","19","uk-UK","Вінницька обл"],["38","19","ru-RU","Винницкая обл"],["39","20","uk-UK","Луцька обл"],
            ["40","20","ru-RU","Луцкая обл"],["41","21","uk-UK","Житомирська обл"],["42","21","ru-RU","Житомирская обл"],
            ["43","22","ru-RU","Волынская обл"],["44","22","uk-UK","Волинська обл"],["45","23","uk-UK","Кіровоградська обл"],
            ["46","23","ru-RU","Кировоградская обл"], ["47","24","uk-UK","Закарпатська обл"],["48","24","ru-RU","Закарпатская обл"],
            ["49","25","ru-RU","АР Крым"],["50","25","uk-UK","АР Крим"]
        ]);
    }

    public function down()
    {
        echo "m161219_081549_added_regions_values cannot be reverted.\n";

        return false;
    }
}
