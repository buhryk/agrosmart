<?php

use yii\db\Migration;

class m170227_154028_dispatch_prices extends Migration
{
    public function up()
    {
        $this->createTable('prices_dispath', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'rubric_id' => $this->integer()->notNull(),
            'filter' => $this->integer()->notNull(),
            'value' => $this->integer()->notNull()->defaultValue(0),
            'type' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('prices_dispath');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
