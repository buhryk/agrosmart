<?php

use yii\db\Migration;

class m170404_070832_add_colum_instrument extends Migration
{
    public function up()
    {
        $this->addColumn('instrument', 'parent_id', $this->integer()->defaultValue(NULL)) ;
    }

    public function down()
    {
        echo "m170404_070832_add_colum_instrument cannot be reverted.\n";

        return false;
    }



}
