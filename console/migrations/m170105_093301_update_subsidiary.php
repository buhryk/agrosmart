<?php

use yii\db\Migration;

class m170105_093301_update_subsidiary extends Migration
{
    public function up()
    {
        $this->dropColumn('subsidiary', 'company_user_id');
        $this->addColumn('subsidiary', 'company_id', $this->integer()->notNull());
    }

    public function down()
    {
      
    }

}
