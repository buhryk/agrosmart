<?php

use yii\db\Migration;

class m170301_140356_add_advertising extends Migration
{
    public function up()
    {
        $this->createTable('advertising_group', [
            'key' => $this->string(45)->unique(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createTable('advertising_position', [
            'id' => $this->primaryKey(),
            'group_key' => $this->string()->notNull(),
            'position' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'price' => $this->decimal(11,2)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createTable('advertising_model_position', [
            'model_id' => $this->integer()->notNull(),
            'position_id' => $this->integer()->notNull()
        ]);

        $this->createTable('advertising', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'start' => $this->date()->notNull(),
            'finish' => $this->date()->notNull(),
            'image' => $this->string('355')->notNull(),
            'name' => $this->string('255')->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('advertising_group');
        $this->dropTable('advertising_position');
        $this->dropTable('advertising_model_position');
        $this->dropTable('advertising');
    }
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }

}
