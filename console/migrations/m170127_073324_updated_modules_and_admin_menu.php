<?php

use yii\db\Migration;

class m170127_073324_updated_modules_and_admin_menu extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%module}}', ['id', 'alias', 'controller_namespace', 'title', 'description'], [
            ["13","consumer","backend\\modules\\consumer\\controllers","Пользователи портала","Модуль пользователей портала (пользователи и компании)"]
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["26","13","user","Пользователи","Контроллер для управления пользователями портала"],
            ["27","13","company","Компании","Контроллер для управления компаниями"]
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["123","26","index","Просмотр списка","Просмотр списка пользователей"],
            ["124","26","view","Просмотр","Просмотр информации о пользователе"],
            ["125","26","change-status","Изменение статуса","Изменение статуса пользователя"],
            ["126","26","change-can-add-advertisements","Изменение возможности создания объявлений","Изменение возможности создания объявлений"],
            ["127","27","index","Просмотр списка","Просмотр списка компаний"],
            ["128","27","view","Просмотр","Просмотр информации о компании"],
            ["129","27","change-is-confirmed","Изменение статуса подтвержденности компании","Изменение статуса подтвержденности компании"],
            ["130","27","change-status","Изменение статуса","Изменение статуса компании"]
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'parent_id', 'icon'], [
            ["29","","Пользователи портала","Пользователи приложения: люди и компании","","glyphicon glyphicon-user"],
            ["30","/consumer/user/index","Пользователи","Пользователи приложения","29","glyphicon glyphicon-headphones"],
            ["31","/consumer/company/index","Компании","Зарегистрированные компании","29",""]
        ]);
    }

    public function down()
    {
        echo "m170127_073324_updated_modules_and_admin_menu cannot be reverted.\n";

        return false;
    }
}
