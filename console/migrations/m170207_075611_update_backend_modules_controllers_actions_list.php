<?php

use yii\db\Migration;

class m170207_075611_update_backend_modules_controllers_actions_list extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["28","7","contact","Контакты","Контроллер для управления контактами элеваторов"]
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["131","28","index","Просмотр списка","Просмотр списка контактов элеваторов"],
            ["132","28","create","Создание","Создание контакта элеватора"],
            ["133","28","view","Просмотр","Просмотр контакта элеватора"],
            ["134","28","update","Редактирование","Редактирование контакта элеватора"],
            ["135","28","delete","Удаление","Удаление контакта элеватора"]
        ]);

        $this->addColumn('elevator_model', 'additional_info', $this->text()->null()->defaultValue(null));
        $this->addColumn('event_model', 'created_at', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('elevator_model', 'additional_info');
        $this->dropColumn('event_model', 'created_at');
    }
}
