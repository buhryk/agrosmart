<?php

use yii\db\Migration;

class m170404_080514_add_instrument extends Migration
{
    public function up()
    {
   
        $this->execute("
            INSERT INTO `instrument` (`id`, `created_at`, `updated_at`, `position`, `status`, `image`, `type`, `url`, `parent_id`) VALUES
            (12, 1491290871, 1491290871, 1, 1, '/uploads/instrument/log-calc-car.png', 1, '/instruments/logistics-calculators/avto', 5),
            (13, 1491291175, 1491292980, 1, 1, '/uploads/instrument/zerno-img.png', 1, '/instruments/quality-calculation/pshenitsa', 9),
            (14, 1491292547, 1491292918, 14, 1, '/uploads/instrument/company-prices-map.png', 1, '/company/company/map', NULL);


        INSERT INTO `instrument_lang` (`id`, `instrument_id`, `lang`, `title`, `description`, `text`) VALUES
            (21, 12, 'ru-RU', 'Логистический калькулятор авто', '<p class=\"logistic-calc-item__text-title\">Считаете затраты на автоперевозку\r\n</p><p class=\"logistic-calc-item__text\">Калькулятор дает вам общую стоимость на перевозку зерновых автотранспортом\r\n</p>', ''),
            (22, 13, 'ru-RU', 'Расчет качества пшеницы', '\r\n	<p class=\"logistic-calc-item__text-title\">Калькулятор определяет сорт культуры по ДСТУ\r\n	</p>\r\n	<p class=\"logistic-calc-item__text\">Вам необходимо ввести 4 главных показателя, нажать на \"Рассчитать\", и вы получаете точный сорт\r\n	</p>\r\n', ''),
            (23, 14, 'ru-RU', 'Карту компаний', '', ''),
            (24, 14, 'uk-UK', 'Логістичний калькулятор авто', '', ''),
            (25, 13, 'uk-UK', 'Розрахунок якості пшениці', '', '');
        ");



    }

    public function down()
    {
        echo "m170404_080514_add_instrument cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
