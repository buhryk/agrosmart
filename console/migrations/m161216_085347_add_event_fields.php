<?php

use yii\db\Migration;

class m161216_085347_add_event_fields extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `event_model` ADD `address` VARCHAR( 255 ) NULL DEFAULT NULL ;');
        $this->execute('ALTER TABLE `event_model` ADD `address_confirmed` TINYINT NOT NULL DEFAULT \'0\';');
    }

    public function down()
    {
        $this->execute('ALTER TABLE  `event_model` DROP  `address` ;');
        $this->execute('ALTER TABLE  `event_model` DROP  `address_confirmed` ;');
    }
}
