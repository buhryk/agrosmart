<?php

use yii\db\Migration;

class m170419_131857_add_colum_consumer extends Migration
{
    public function up()
    {
        $this->addColumn('consumer','is_new', $this->smallInteger()->defaultValue(1));
    }

    public function down()
    {
        echo "m170419_131857_add_colum_consumer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
