<?php

use yii\db\Migration;

class m161229_084723_update_region_table extends Migration
{
    public function up()
    {
        $this->addColumn('oblast', 'country_id', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('oblast', 'country_id');
    }
}
