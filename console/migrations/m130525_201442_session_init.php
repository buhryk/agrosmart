<?php

use yii\db\Migration;

class m130525_201442_session_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%session}}', [
            'id'=> $this->char(40)->notNull(),
            'expire'=> $this->integer(),
            'data'=> 'LONGBLOB',
        ], $tableOptions);
        $this->addPrimaryKey('pk_id', '{{%session}}', 'id');
    }

    public function down()
    {
        echo "m130525_201442_session_init cannot be reverted.\n";
        $this->dropTable('{{%session}}');

        return false;
    }
}
