<?php

use yii\db\Migration;

class m170704_073226_add_index_area extends Migration
{
    public function up()
    {
        $this->createIndex('idx-lang-area_l', 'area_lang', 'lang');
        $this->createIndex('idx-area_id-area_l', 'area_lang', 'area_id');
    }

    public function down()
    {
        echo "m170704_073226_add_index_area cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
