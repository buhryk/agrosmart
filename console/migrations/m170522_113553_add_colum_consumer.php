<?php

use yii\db\Migration;

class m170522_113553_add_colum_consumer extends Migration
{
    public function up()
    {
        $this->addColumn('consumer', 'oblast_id' , $this->integer()->defaultValue(NULL));
    }

    public function down()
    {
        echo "m170522_113553_add_colum_consumer cannot be reverted.\n";

        return false;
    }

}
