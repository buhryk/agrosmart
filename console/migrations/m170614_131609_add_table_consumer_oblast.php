<?php

use yii\db\Migration;

class m170614_131609_add_table_consumer_oblast extends Migration
{
    public function up()
    {
        $this->createTable('consumer_oblast', [
            'user_id' => $this->integer()->unique(),
            'oblast_id' => $this->integer(),
        ]);

    }

    public function down()
    {
        echo "m170614_131609_add_table_consumer_oblast cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
