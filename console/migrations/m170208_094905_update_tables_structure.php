<?php

use yii\db\Migration;

class m170208_094905_update_tables_structure extends Migration
{
    public function up()
    {
        $this->addColumn('news_model', 'is_top', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('news_category', 'is_interesting', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('news_model', 'is_top');
        $this->dropColumn('news_category', 'is_interesting');
    }
}
