<?php

use backend\modules\adminmenu\AdminMenu;
use yii\db\Migration;

/**
 * Class m180323_093158_add_in_admin_nenu
 */
class m180323_093158_add_in_admin_nenu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
       INSERT INTO admin_menu (title, description, parent_id, icon, path)
        VALUES ('Прайсы', 'Список прайсов', NULL, 'glyphicon glyphicon-usd', '/price2/price' );
    ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180323_093158_add_in_admin_nenu cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180323_093158_add_in_admin_nenu cannot be reverted.\n";

        return false;
    }
    */
}
