<?php

use yii\db\Migration;

class m170104_090116_create_superadmin_default_user extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%user}}', ['id', 'username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'status', 'created_at', 'updated_at', 'name', 'surname', 'phone', 'role_id'], [
            ["1", "admin", "sUv5pXw5OlFGqapA9eknY85umPtyAvph", '$2y$13$Uvw0yCEssV7f1oXh65Supe71pf5WM.aCj/5RW6Uhxg.ChGFxY2R8G', null, "admin@asd.asd", "10", "1483520400", "1483520400", "admin", "admin", "0996179939", "1"]
        ]);
    }

    public function down()
    {
        echo "m170104_090116_create_superadmin_default_user cannot be reverted.\n";

        return false;
    }
}
