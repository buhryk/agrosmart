<?php

use yii\db\Migration;

class m170329_082709_add extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%admin_menu}}', ['title', 'description', 'icon', 'path'], [
            ["Контакты", "Контакты","glyphicon glyphicon-phone-alt", '/contact/contact/index'],
            ["Переводы", "Переводы", "glyphicon glyphicon-transfer", "/translate/default/index"]
        ]);
    }

    public function down()
    {
        echo "m170329_082709_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
