<?php

use yii\db\Migration;

class m170211_141536_update_company_at extends Migration
{
    public function up()
    {
        $this->alterColumn('company','created_at','integer NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m170211_141536_update_company_at cannot be reverted.\n";
        $this->alterColumn('company','created_at','integer DEFAULT NULL');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
