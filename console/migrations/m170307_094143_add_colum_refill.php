<?php

use yii\db\Migration;

class m170307_094143_add_colum_refill extends Migration
{
    public function up()
    {
        $this->addColumn('refill', 'sum', $this->decimal(11.2));
    }

    public function down()
    {
        echo "m170307_094143_add_colum_refill cannot be reverted.\n";

        return false;
    }

}
