<?php

use yii\db\Migration;

/**
 * Handles the creation of table `country`.
 */
class m170209_151913_create_country_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->notNull()
        ]);

        $this->createTable('country_lang', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)
        ]);

        $this->batchInsert('{{%country}}', ['id', 'alias'], [
            ["1","rossiya"],
            ["2","ukraina"],
            ["3","belarus"],
        ]);

        $this->batchInsert('{{%country_lang}}', ['id', 'country_id', 'lang', 'title'], [
            ["1","1", "ru-RU", "Россия"],
            ["2","1", "uk-UK", "Росія"],
            ["3","2", "ru-RU", "Украина"],
            ["4","2", "uk-UK", "Україна"],
            ["5","3", "ru-RU", "Беларусь"],
            ["6","3", "uk-UK", "Білорусь"]
        ]);

        $this->dropTable('event_country');
        $this->dropTable('event_country_lang');
        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["136","2", "delete-accessibility-property", "Удаление доступа на просмотр новостей пользователями портала", "Удаление доступа на просмотр новостей пользователями портала"]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('country');
        $this->dropTable('country_lang');
    }
}
