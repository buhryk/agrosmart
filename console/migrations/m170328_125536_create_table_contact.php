<?php

use yii\db\Migration;

class m170328_125536_create_table_contact extends Migration
{
    public function up()
    {
        $this->createTable('contact', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'name' => $this->string(255)->notNull(),
            'surname' => $this->string(255)->defaultValue(NULL),
            'phone' => $this->string(25)->defaultValue(NULL),
            'email' => $this->string(25)->defaultValue(NULL),
            'message' => $this->text(),
            'status' => $this->integer()->defaultValue(1),

        ]);
    }

    public function down()
    {
        $this->dropTable('contact');
    }
    
}
