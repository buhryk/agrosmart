<?php

use yii\db\Migration;

class m170407_064040_add_site_role extends Migration
{
    public function up()
    {

  
        $this->execute("

INSERT INTO `site_role` (`id`, `name`, `authorized`, `type`, `page_id`) VALUES
(1, 'Трейдерам', 1, 1, 4),
(2, 'Фермерам', 1, 1, 2),
(3, 'Логистам', 1, 1, 34),
(4, 'Дистрибьюторам', 1, 1, 35),
(5, 'Дилерам', 1, 1, 36),
(6, 'Переработчикам', 1, 1, 37),
(7, 'Незарегистрированным пользователям', 2, 0, NULL),
(8, 'Зарегистрированным пользователям без компании', 1, 2, NULL),
(9, 'Всем пользователям', 0, 0, NULL),
(10, 'Незарегистрированным трейдерам', 2, 1, 4),
(11, 'Незарегистрированным фермерам', 2, 1, 2),
(12, 'Незарегистрированным логистам', 2, 1, 34),
(13, 'Незарегистрированным дистрибьюторам', 2, 1, 35),
(14, 'Незарегистрированным дилерам', 2, 1, 36),
(15, 'Незарегистрированным переработчикам', 2, 1, 37);


        ");

    }

    public function down()
    {
        echo "m170407_064040_add_site_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
