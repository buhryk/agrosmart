<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

use yii\db\Migration;

/**
 * Initializes i18n messages tables.
 *
 *
 *
 * @author Dmitry Naumenko <d.naumenko.a@gmail.com>
 * @since 2.0.7
 */
class m130526_201442_i18n_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%source_message}}', [
            'id' => $this->primaryKey(),
            'category' => $this->string()->defaultValue('i18n'),
            'message' => $this->text(),
            'entity_id' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->createIndex('idx_source_message_category', '{{%source_message}}', 'category');

        $this->createTable('{{%message}}', [
            'id' => $this->integer()->notNull(),
            'language' => "ENUM('en', 'ru', 'uk') NOT NULL",
            'translation' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_message_source_message', '{{%message}}', 'id', '{{%source_message}}', 'id', 'CASCADE', 'RESTRICT');
        $this->createIndex('idx_message_language', '{{%message}}', 'language');
        $this->createIndex('idx_source_message_entity_id', '{{%source_message}}', 'entity_id');

        $this->insert('{{%source_message}}', ['message' => 'TRANSLATION_IS_ABSENT']);
        $this->batchInsert('{{%message}}', ['id', 'language', 'translation'], [
            [1, 'ru', 'Перевод отсутствует'],
            [1, 'uk', 'Переклад вiдсутнiй'],
            [1, 'en', 'Translation is absent']
        ]);
    }

    public function down()
    {
        echo "m130526_201442_i18n_init cannot be reverted.\n";
        $this->dropForeignKey('fk_message_source_message', '{{%message}}');
        $this->dropTable('{{%message}}');
        $this->dropTable('{{%source_message}}');
    }
}
