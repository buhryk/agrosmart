<?php

use yii\db\Migration;

class m170526_120910_subsidiary_delete extends Migration
{
    public function up()
    {
        $subsidiaris = \frontend\modules\cabinet\models\Subsidiary::find()->all();

        foreach ($subsidiaris as $subsidiary) {
            $company = \frontend\models\Company::findOne($subsidiary->company_id);
            if (empty($company)) {
                echo "\n subsidiary_id=".$subsidiary->id." res".$subsidiary->delete();
            }
        }
    }

    public function down()
    {
        echo "m170526_120910_subsidiary_delete cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
