<?php

use yii\db\Migration;

class m170704_072905_index_table_city extends Migration
{
    public function up()
    {
        $this->createIndex('idx-lang-city_l', 'city_lang', 'lang');
        $this->createIndex('idx-city_id-city_l', 'city_lang', 'city_id');
    }

    public function down()
    {
        echo "m170704_072905_index_table_city cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
