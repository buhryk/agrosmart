<?php

use yii\db\Migration;

class m170327_083900_add_insturemnti_main_pages extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%page_category}}', ['id', 'parent_id', 'alias', 'active'], [
            ["5","","instrumenty-glavnaya-stranitsa","0"]
        ]);

        $this->batchInsert('{{%page_category_lang}}', ['id', 'category_id', 'lang', 'title'], [
            ["8","5","ru-RU","Инструменты главная страница"],
            ["9","5","uk-UK","Інструменти головна сторінка"]
        ]);

        $this->batchInsert('{{%page_model}}', ['id', 'category_id', 'alias', 'active'], [
            ["25","5","karta-tsen-predpriyatiy-glavnaya","0"],
            ["26","5","karta-elevatorov-glavnaya","0"],
            ["27","5","logisticheskiy-kalkulyator-glavnaya","0"],
            ["28","5","tseny-na-s-kh-produktsiyu-glavnaya","0"]
        ]);

        $this->batchInsert('{{%page_model_lang}}', ['id', 'page_id', 'lang', 'title', 'short_description', 'text'], [
            ['48', '25', 'ru-RU', 'Карта цен предприятий', 'Карта цен предприятий', '<div class="item">
            <a href="/ru/instruments/map-price-company/index">
                <img src="/images/icon1.png">
                <h4>Карта цен предприятий</h4>
                Вы понимаете, где выгоднее продать/купить
                <p>На карте выводятся цены на все с/х продукты по каждой области в соотношении мин-макс</p>
            </a>
        </div>'],
            ['49', '25', 'uk-UK', 'Карта цін підприємств', 'Карта цін підприємств', '<div class="item">
            <a href="/ua/instruments/map-price-company/index">
                <img src="/images/icon1.png">
                <h4>Карта цін підприємств</h4>
                <span>Ви розумієте, де вигідно продати/купити</span>
                <p>На карті виводяться ціни на всі с/г продукти по кожній області в співвідношенні мін-макс</p>
            </a>
        </div>'],
            ['50', '26', 'ru-RU', 'Карта элеваторов', 'Карта элеваторов', '<div class="item">
            <a href="/ru/instruments/elevators/map">
                <img src="/images/icon2.png">
                <h4>Карта элеваторов</h4>
                Сразу понятно, куда везти свой продукт
                <p>Собраны все элеваторы страны. На карте считается расстояние от вас до выбранного элеватора</p>
            </a>
        </div>'],
            ['51', '26', 'uk-UK', 'Карта елеваторів', 'Карта елеваторів', '<div class="item">
            <a href="/ua/instruments/elevators/map">
                <img src="/images/icon2.png">
                <h4>Карта елеваторів</h4>
                <span>Зразу зрозуміло, куда везти свій продукт</span>
                <p>Зібрані всі елеватори країни. На карті розраховується відстань від вас до обраного елеватора</p>
            </a>
        </div>'],
            ['52', '27', 'ru-RU', 'Логистический калькулятор', 'Логистический калькулятор', '<div class="item">
            <a href="/ru/instruments/logistics-calculators/index">
                <img src="/images/icon3.png">
                <h4>Логистический калькулятор</h4>
                Считаете затраты на авто перевозку
                <p>Калькулятор дает вам общую стоимость на перевозку зерновых автотранспортом</p>
            </a>
        </div>'],
            ['53', '27', 'uk-UK', 'Логістичний калькулятор', 'Логістичний калькулятор', '<div class="item">
            <a href="/ua/instruments/logistics-calculators/index">
                <img src="/images/icon3.png">
                <h4>Логістичний калькулятор</h4>
                <span>Рахуєте затрати на авто перевозку</span>
                <p>Калькулятор дає вам загальну вартість на перевозку зернових автотранспортом</p>
            </a>
        </div>'],
            ['54', '28', 'ru-RU', 'Цены на с/х продукцию', 'Цены на с/х продукцию', '<div class="item">
            <a href="/ru/company/price/zakupka">
                <img src="/images/icon4.png">
                <h4>Цены на с/х продукцию</h4>
                Каталог агро-предприятий и их цен на с/х продукцию
                <p>Каталог всех цен позволяет понять мин. и макс цену</p>
            </a>
        </div>'],
            ['55', '28', 'uk-UK', 'Ціни на с/г продукцію', 'Ціни на с/г продукцію', '<div class="item">
            <a href="/ua/company/price/zakupka">
                <img src="/images/icon4.png">
                <h4>Ціни на с/г продукцію</h4>
                <span>Каталог агро-підприємств та їх ціни на с/г продукцію</span>
                <p>Каталог всіх цін дозволяє зрозуміти мін. та макс. ціну</p>
            </a>
        </div>'],
        ]);
    }

    public function down()
    {
        echo "m170327_083900_add_insturemnti_main_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
