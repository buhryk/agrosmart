<?php

use yii\db\Migration;

class m161216_114528_edit_elevator_tables extends Migration
{
    public function up()
    {
        $this->alterColumn('elevator_model', 'region_id', $this->integer()->notNull());
        $this->createTable('elevators_accessibilities', [
            'id' => $this->primaryKey(),
            'elevator_id' => $this->integer()->notNull(),
            'accessibility_id' => $this->integer()->notNull(),
        ]);
        $this->dropColumn('elevator_model', 'accessibility');
        $this->addColumn('elevator_model', 'alias', $this->string(128)->unique()->notNull());
    }

    public function down()
    {
        $this->alterColumn('elevator_model', 'region_id', $this->string(255)->notNull());
        $this->dropTable('elevators_accessibilities');
        $this->addColumn('elevator_model', 'accessibility', $this->integer()->notNull());
        $this->dropColumn('elevator_model', 'alias');
    }
}
