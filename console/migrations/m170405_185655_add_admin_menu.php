<?php

use yii\db\Migration;

class m170405_185655_add_admin_menu extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%admin_menu}}', ['title', 'description', 'icon', 'path'], [
            ["Пакеты", "Пакеты ","glyphicon glyphicon-folder-open", '/package/package/index'],
        ]);
    }

    public function down()
    {
        echo "m170405_185655_add_admin_menu cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
