<?php

use yii\db\Migration;

class m170123_114511_backend_consumer_modules_etc extends Migration
{
    public function up()
    {
        $this->alterColumn('company', 'logotype', $this->string(255)->null()->defaultValue(null));
        $this->alterColumn('company', 'ratio', $this->float()->notNull()->defaultValue(0));
        $this->alterColumn('company', 'title', $this->string(255)->null()->defaultValue(null));
    }

    public function down()
    {
        echo "m170123_114511_backend_consumer_modules_etc cannot be reverted.\n";

        return false;
    }
}
