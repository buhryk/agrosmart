<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rubric`.
 */
class m161202_101254_create_rubric_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('rubric_model', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->null()->defaultValue(null),
            'active' => $this->smallInteger()->defaultValue(1)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('rubric');
    }
}
