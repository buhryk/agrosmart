<?php

use yii\db\Migration;

class m170207_124542_add_colum_sale extends Migration
{
    public function up()
    {
        $this->addColumn('sale', 'is_list', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        echo "m170207_124542_add_colum_sale cannot be reverted.\n";

        return false;
    }
    
}
