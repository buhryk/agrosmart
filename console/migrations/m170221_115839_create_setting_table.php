<?php

use yii\db\Migration;

/**
 * Handles the creation of table `setting`.
 */
class m170221_115839_create_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('setting', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->unique()->notNull(),
            'value' => $this->text()->notNull(),
            'description' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->batchInsert('{{%module}}', ['id', 'alias', 'controller_namespace', 'title', 'description'], [
            ["15","setting","backend\\modules\\setting\\controllers","Настройки","Модуль настроек сайта"]
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["30","15","setting","Настройки","Контроллер для управления настройками сайта"]
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["142","30","index","Просмотр списка","Просмотр списка настроек"],
            ["143","30","create","Создание","Создание настройки"],
            ["144","30","view","Просмотр","Просмотр настройки"],
            ["145","30","update","Редактирование","Редактирование настройки"],
            ["146","30","delete","Удаление","Удаление настройки"]
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'parent_id', 'icon'], [
            ["35","/setting/setting/index","Список настроек","Список настроек сайта","","glyphicon glyphicon-cog"]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('setting');
    }
}
