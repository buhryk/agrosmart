<?php

use yii\db\Migration;

class m170404_125015_add_colum_selected_price extends Migration
{
    public function up()
    {
        $this->addColumn('selected_price', 'type', $this->integer()->defaultValue(1));
    }

    public function down()
    {
        echo "m170404_125015_add_colum_selected_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
