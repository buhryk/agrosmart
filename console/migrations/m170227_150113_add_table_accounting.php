<?php

use yii\db\Migration;

class m170227_150113_add_table_accounting extends Migration
{
    public function up()
    {
        $this->createTable('accounting', [
            'id' => $this->primaryKey(),
            'responsible' => $this->string(128),
            'product' => $this->string(128)->notNull(),
            'buyers' => $this->string(128),
            'basis_delivery' => $this->string(128),
            'seller' => $this->string(128)->notNull(),
            'basis_purchase' => $this->string(128),
            'elevator' => $this->string(128),
            'logist' => $this->string(128),
            'numbercars_per_day' => $this->string(128),
            'fpproval_date_line' => $this->string(128),
            'currency' => $this->string(128),
            'count_day' => $this->string(128),

            'count' => $this->decimal(11,2),
            'price' => $this->decimal(11,2),
            'exw_count' => $this->decimal(11,2),
            'exw_price' => $this->decimal(11,2),
            'meneger_count' => $this->decimal(11,2),
            'meneger_price' => $this->decimal(11,2),
            'traveling_expenses_count' => $this->decimal(11,2),
            'traveling_expenses_price' => $this->decimal(11,2),
            'counterparty_count' => $this->decimal(11,2),
            'counterparty_price' => $this->decimal(11,2),
            'broker_count' => $this->decimal(11,2),
            'broker_price' => $this->decimal(11,2),
            'elevator_days' => $this->decimal(11,2),
            'elevator_price' => $this->decimal(11,2),
            'elevator_count' => $this->decimal(11,2),
            'elevator_service_count' => $this->decimal(11,2),
            'elevator_service_price' => $this->decimal(11,2),
            'elevator_shipment_days' => $this->decimal(11,2),
            'elevator_shipment_price' => $this->decimal(11,2),
            'elevator_census_days' => $this->decimal(11,2),
            'elevator_census_price' => $this->decimal(11,2),
            'logistics_costs_f1_count' => $this->decimal(11,2),
            'logistics_costs_f1_price' => $this->integer(),
            'logistics_costs_f2_count' => $this->integer(),
            'logistics_costs_f2_price' => $this->integer(),
            'logistics_costs_count' => $this->decimal(11,2),
            'logistics_costs_price' => $this->decimal(11,2),
            'natural_decline' => $this->decimal(11,2),
            'tax_burden_percent' => $this->decimal(11,2),
            'tax_burden_count' => $this->decimal(11,2),
            'upgrade_count' => $this->decimal(11,2),
            'upgrade_price' => $this->decimal(11,2),
            'cost_money_percent' => $this->decimal(11,2),
            'cost_money_count' => $this->decimal(11,2)
        ]);
    }

    public function down()
    {
        echo "m170227_150113_add_table_accounting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
