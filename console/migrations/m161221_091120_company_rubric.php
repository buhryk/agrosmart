<?php

use yii\db\Migration;

class m161221_091120_company_rubric extends Migration
{
    public function up()
    {
        $this->createTable('company_rubric', [
            'company_id' => $this->integer()->notNull(),
            'rubric_id' => $this->integer()->notNull(),
        ]);
        $this->dropColumn('company', 'rubric_id');
    }

    public function down()
    {
        $this->dropTable('company_rubric');
    }

}
