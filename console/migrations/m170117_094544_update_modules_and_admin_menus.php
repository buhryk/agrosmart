<?php

use yii\db\Migration;

class m170117_094544_update_modules_and_admin_menus extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%module}}', ['id', 'alias', 'controller_namespace', 'title', 'description'], [
            ["11","advertisement","backend\\modules\\advertisement\\controllers","Объявления","Модуль объявлений"],
            ["12","complaint","backend\\modules\\complaint\\controllers","Жалобы","Модуль жалоб на пользователей/компании и объявления"]
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["24","11","advertisement","Объявление","Контроллер для отображения и управления объявлениями"],
            ["25","12","advertisement-complaint","Жалобы на объявления","Контроллер для управления жалобами на объявления"]
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["116","24","index","Просмотр списка","Просмотр списка объявлений"],
            ["117","24","view","Просмотр","Просмотр объявления"],
            ["118","24","change-status","Изменение статуса","Изменение статуса объявления"],
            ["119","24","delete","Удалить","Удаление объявления"],
            ["120","25","index","Просмотр списка","Просмотр списка жалоб на объявления"],
            ["121","25","view","Просмотрет жалобы","Просмотр конкретной жалобы"],
            ["122","25","change-status","Изменение статуса","Изменение статуса жалобы"]
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'parent_id', 'icon'], [
            ["26","/advertisement/advertisement/index","Объявления","Модуль объявлений","","glyphicon glyphicon-shopping-cart"],
            ["27","","Жалобы","Модуль жалоб","","glyphicon glyphicon-headphones"],
            ["28","/complaint/advertisement-complaint/index","Жалобы на объявления","Жалобы на объявления","27",""]
        ]);
    }

    public function down()
    {
        echo "m170117_094544_update_modules_and_admin_menus cannot be reverted.\n";

        return false;
    }
}
