<?php

use yii\db\Migration;

class m170503_103223_event_addres_multy_leng extends Migration
{
    public function up()
    {
        $this->addColumn('event_model_lang', 'address', $this->string()->defaultValue(NULL));
        $this->dropColumn('event_model', 'address');
    }

    public function down()
    {
        echo "m170503_103223_event_addres_multy_leng cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
