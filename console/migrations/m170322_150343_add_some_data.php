<?php

use yii\db\Migration;

class m170322_150343_add_some_data extends Migration
{
    public function up()
    {
        $this->dropColumn('elevator_model', 'additional_info');
        $this->addColumn('elevator_model_lang', 'additional_info', $this->text()->null()->defaultValue(null));
        $this->dropColumn('elevator_model', 'address');
        $this->addColumn('elevator_model_lang', 'address', $this->string(255)->notNull());

        $this->dropColumn('elevator_contact', 'fio');
        $this->createTable('elevator_contact_lang', [
            'id' => $this->primaryKey(),
            'lang' => $this->string(5)->notNull(),
            'contact_id' => $this->integer()->notNull(),
            'fio' => $this->string(255)->notNull()
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'title', 'description', 'parent_id', 'icon', 'path'], [
            ["42","Дополнительно","Дополнительно","","glyphicon glyphicon-th",""],
            ["43","Сгенерировать sitemap","URL для генерации (обновления) sitemap","42","","/site/sitemap"]
        ]);
    }

    public function down()
    {
        $this->dropTable('elevator_contact_lang');
    }
}
