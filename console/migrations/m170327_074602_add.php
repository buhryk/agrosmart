<?php

use yii\db\Migration;

class m170327_074602_add extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%page_category}}', ['id', 'parent_id', 'alias', 'active'], [
            ["4","","instrumenty","0"]
        ]);

        $this->batchInsert('{{%page_category_lang}}', ['id', 'category_id', 'lang', 'title'], [
            ["6","4","ru-RU","Инструменты"],
            ["7","4","uk-UK","Інструменти"]
        ]);

        $this->batchInsert('{{%page_model}}', ['id', 'category_id', 'alias', 'active'], [
            ["18","4","karta-tsen-predpriyatiy","0"],
            ["19","4","karta-elevatorov","0"],
            ["20","4","logisticheskiy-kalkulyator","0"],
            ["21","4","tseny-na-s-kh-produktsiyu","0"],
            ["22","4","dstp-gost","0"],
            ["23","4","instrumenty-inkoterms","0"],
            ["24","4","raschet-kachestva-zernovykh","0"],
        ]);

        $this->batchInsert('{{%page_model_lang}}', ['id', 'page_id', 'lang', 'title', 'short_description', 'text'], [
            ['34', '18', 'ru-RU', 'Карта цен предприятий', 'Карта цен предприятий', '<div class="row">
	<a href="/ru/instruments/map-price-company/index" class="logistic-calc-item clearfix">
	<div class="col-sm-2 col-xs-4">
		<img src="/images/company-prices-map.png" alt="map">
	</div>
	<div class="col-sm-4 col-xs-8">
		<p class="logistic-calc-item__title">Карта цен предприятий
		</p>
	</div>
	<div class="col-sm-6 col-xs-12">
		<p class="logistic-calc-item__text-title">Вы понимаете, где выгоднее продать/купить
		</p>
		<p class="logistic-calc-item__text">На карте выводятся цены на все с/х продукты по каждой области в соотношении мин-макс
		</p>
	</div>
	</a>
</div>'],
            ['35', '18', 'uk-UK', 'Карта цін підприємств', 'Карта цін підприємств', '<div class="row">
                <a href="/ua/instruments/map-price-company/index" class="logistic-calc-item clearfix">
                    <div class="col-sm-2 col-xs-4">
                        <img src="/images/company-prices-map.png" alt="map">
                    </div>
                    <div class="col-sm-4 col-xs-8">
                        <p class="logistic-calc-item__title">Карта цін підприємств</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p class="logistic-calc-item__text-title">Ви розумієте, де вигідно продати/купити</p>
                        <p class="logistic-calc-item__text">На карті виводяться ціни на всі с/г продукти по кожній області в співвідношенні мін-макс</p>
                    </div>
                </a>
            </div>'],
            ['36', '19', 'ru-RU', 'Карта элеваторов', 'Карта элеваторов', '<div class="row">
                <a href="/ru/instruments/elevators/map" class="logistic-calc-item clearfix">
                    <div class="col-sm-2 col-xs-4">
                        <img src="/images/elevators-map.png" alt="el-map">
                    </div>
                    <div class="col-sm-4 col-xs-8">
                        <p class="logistic-calc-item__title">Карта элеваторов</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p class="logistic-calc-item__text-title">
                            Сразу понятно, куда везти свой продукт                        </p>
                        <p class="logistic-calc-item__text">
                            Собраны все элеваторы страны. На карте считается расстояние от вас до выбранного элеватора                        </p>
                    </div>
                </a>
            </div>'],
            ['37', '19', 'uk-UK', 'Карта елеваторів', 'Карта елеваторів', '<div class="row">
                <a href="/ua/instruments/elevators/map" class="logistic-calc-item clearfix">
                    <div class="col-sm-2 col-xs-4">
                        <img src="/images/elevators-map.png" alt="el-map">
                    </div>
                    <div class="col-sm-4 col-xs-8">
                        <p class="logistic-calc-item__title">Карта елеваторів</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p class="logistic-calc-item__text-title">
                            Зразу зрозуміло, куда везти свій продукт                        </p>
                        <p class="logistic-calc-item__text">
                            Зібрані всі елеватори країни. На карті розраховується відстань від вас до обраного елеватора                        </p>
                    </div>
                </a>
            </div>'],
            ['38', '20', 'ru-RU', 'Логистический калькулятор', 'Логистический калькулятор', '<div class="row">
                <a href="/ru/instruments/logistics-calculators/index" class="logistic-calc-item clearfix">
                    <div class="col-sm-2 col-xs-4">
                        <img src="/images/logistic-calculator.png" alt="map">
                    </div>
                    <div class="col-sm-4 col-xs-8">
                        <p class="logistic-calc-item__title">Логистический калькулятор</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p class="logistic-calc-item__text-title">Считаете затраты на авто перевозку</p>
                        <p class="logistic-calc-item__text">Калькулятор дает вам общую стоимость на перевозку зерновых автотранспортом</p>
                    </div>
                </a>
            </div>'],
            ['39', '20', 'uk-UK', 'Логістичний калькулятор', 'Логістичний калькулятор', '<div class="row">
                <a href="/ua/instruments/logistics-calculators/index" class="logistic-calc-item clearfix">
                    <div class="col-sm-2 col-xs-4">
                        <img src="/images/logistic-calculator.png" alt="map">
                    </div>
                    <div class="col-sm-4 col-xs-8">
                        <p class="logistic-calc-item__title">Логістичний калькулятор</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p class="logistic-calc-item__text-title">Рахуєте затрати на авто перевозку</p>
                        <p class="logistic-calc-item__text">Калькулятор дає вам загальну вартість на перевозку зернових автотранспортом</p>
                    </div>
                </a>
            </div>'],
            ['40', '21', 'ru-RU', 'Цены на с/х продукцию', 'Цены на с/х продукцию', '<div class="row">
                <a href="/ru/company/price/zakupka" class="logistic-calc-item clearfix">
                    <div class="col-sm-2 col-xs-4">
                        <img src="/images/sh-prices.png" alt="prices">
                    </div>
                    <div class="col-sm-4 col-xs-8">
                        <p class="logistic-calc-item__title">Цены на с/х продукцию</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p class="logistic-calc-item__text-title">Каталог агро-предприятий и их цен на с/х продукцию</p>
                        <p class="logistic-calc-item__text">Каталог всех цен позволяет понять мин. и макс цену</p>
                    </div>
                </a>
            </div>'],
            ['41', '21', 'uk-UK', 'Ціни на с/х продукцію', 'Ціни на с/х продукцію', '<div class="row">
                <a href="/ua/company/price/zakupka" class="logistic-calc-item clearfix">
                    <div class="col-sm-2 col-xs-4">
                        <img src="/images/sh-prices.png" alt="prices">
                    </div>
                    <div class="col-sm-4 col-xs-8">
                        <p class="logistic-calc-item__title">Ціни на с/г продукцію</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p class="logistic-calc-item__text-title">Каталог агро-підприємств та їх ціни на с/г продукцію</p>
                        <p class="logistic-calc-item__text">Каталог всіх цін дозволяє зрозуміти мін. та макс. ціну</p>
                    </div>
                </a>
            </div>'],
            ['42', '22', 'ru-RU', 'ДСТУ (ГОСТ)', 'ДСТУ (ГОСТ)', '<div class="row">
                    <a href="/ru/page/category/dstu" class="logistic-calc-item clearfix">
                        <div class="col-sm-2 col-xs-4">
                            <img src="/images/tstu-gost.png" alt="gost">
                        </div>
                        <div class="col-sm-4 col-xs-8">
                            <p class="logistic-calc-item__title">ДСТУ (ГОСТ)</p>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <p class="logistic-calc-item__text-title">
                                Справочник ДСТУ на основную с/х продукцию                            </p>
                            <p class="logistic-calc-item__text">
                                Удобный помощник при проведении сделок и определении качества покупаемой продукции                            </p>
                        </div>
                    </a>
                </div>'],
            ['43', '22', 'uk-UK', 'ДСТУ (ГОСТ)', 'ДСТУ (ГОСТ)', '<div class="row">
                    <a href="/ua/page/category/dstu" class="logistic-calc-item clearfix">
                        <div class="col-sm-2 col-xs-4">
                            <img src="/images/tstu-gost.png" alt="gost">
                        </div>
                        <div class="col-sm-4 col-xs-8">
                            <p class="logistic-calc-item__title">ДСТУ (ГОСТ)</p>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <p class="logistic-calc-item__text-title">
                                Довідник ДСТУ на основну с/г продукцію                            </p>
                            <p class="logistic-calc-item__text">
                                Зручний помічник при проведенні операцій та визначенні якості продукції, що купується                            </p>
                        </div>
                    </a>
                </div>'],
            ['44', '23', 'ru-RU', 'Инкотермс', 'Инкотермс', '<div class="row">
                    <a href="/ru/page/inkoterms" class="logistic-calc-item clearfix">
                        <div class="col-sm-2 col-xs-4">
                            <img src="/images/incoterms.png" alt="incoterms">
                        </div>
                        <div class="col-sm-4 col-xs-8">
                            <p class="logistic-calc-item__title">Инкотермс</p>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <p class="logistic-calc-item__text-title">
                                Понятный справочник с актуальными терминами                            </p>
                            <p class="logistic-calc-item__text">
                                Поддерживаем актуальность справочника                            </p>
                        </div>
                    </a>
                </div>'],
            ['45', '23', 'uk-UK', 'Інкотермс', 'Інкотермс', '<div class="row">
                    <a href="/ua/page/inkoterms" class="logistic-calc-item clearfix">
                        <div class="col-sm-2 col-xs-4">
                            <img src="/images/incoterms.png" alt="incoterms">
                        </div>
                        <div class="col-sm-4 col-xs-8">
                            <p class="logistic-calc-item__title">Інкотермс</p>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <p class="logistic-calc-item__text-title">
                                Зрозумілий довідник з актуальними термінами                            </p>
                            <p class="logistic-calc-item__text">
                                Підтримуємо актуальність довідника                            </p>
                        </div>
                    </a>
                </div>'],
            ['46', '24', 'ru-RU', 'Расчет качества зерновых', 'Расчет качества зерновых', '<div class="row">
                <a href="/ru/instruments/quality-calculation/index" class="logistic-calc-item clearfix">
                    <div class="col-sm-2 col-xs-4">
                        <img src="/images/incoterms.png" alt="incoterms">
                    </div>
                    <div class="col-sm-4 col-xs-8">
                        <p class="logistic-calc-item__title">Расчет качества зерновых</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p class="logistic-calc-item__text-title">
                            Понятный справочник с актуальными терминами                        </p>
                        <p class="logistic-calc-item__text">
                            Поддерживаем актуальность справочника                        </p>
                    </div>
                </a>
            </div>'],
            ['47', '24', 'uk-UK', 'Розрахунок якості зернових', 'Розрахунок якості зернових', '<div class="row">
                <a href="/ua/instruments/quality-calculation/index" class="logistic-calc-item clearfix">
                    <div class="col-sm-2 col-xs-4">
                        <img src="/images/incoterms.png" alt="incoterms">
                    </div>
                    <div class="col-sm-4 col-xs-8">
                        <p class="logistic-calc-item__title">Розрахунок якості зернових</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p class="logistic-calc-item__text-title">
                            Зрозумілий довідник з актуальними термінами                        </p>
                        <p class="logistic-calc-item__text">
                            Підтримуємо актуальність довідника                        </p>
                    </div>
                </a>
            </div>'],
        ]);
    }

    public function down()
    {
        echo "m170327_074602_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
