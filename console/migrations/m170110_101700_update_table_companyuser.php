<?php

use yii\db\Migration;

class m170110_101700_update_table_companyuser extends Migration
{
    public function up()
    {
        $this->dropColumn('company_user', 'position');
        $this->addColumn('company_user', 'role_id', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        echo "m170110_101700_update_table_companyuser cannot be reverted.\n";

        return false;
    }
}
