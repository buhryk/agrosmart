<?php

use yii\db\Migration;

class m170403_080548_add_instruments_module extends Migration
{
    public function up()
    {
        $this->createTable('instrument', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'position' => $this->integer(),
            'status' => $this->integer()->defaultValue(1),
            'type' => $this->smallInteger()->defaultValue(1),
            'image' => $this->string(250)->notNull(),
            'url' => $this->string(250)->notNull(),
        ]);

        $this->createTable('instrument_lang', [
            'id' => $this->primaryKey(),
            'instrument_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text(),
            'text' => $this->text()
        ]);

        $this->createTable('instrument_accessibility_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createTable('instrument_accessibility', [
            'instrument_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('instrument_visibility', [
            'instrument_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
        ]);

    }

    public function down()
    {
        $this->dropTable('instrument');
        $this->dropTable('instrument_lang');
        $this->dropTable('instrument_accessibility_type');
        $this->dropTable('instrument_accessibility');
        $this->dropTable('instrument_visibility');
    }
    
}
