<?php

use yii\db\Migration;

class m161209_101050_create_news_tables extends Migration
{
    public function up()
    {
        $this->createTable('news_category', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->defaultValue(null)->null(),
            'alias' => $this->string(128)->unique()->notNull(),
            'active' => $this->smallInteger()->defaultValue(1),
            'image' => $this->string(255)->null()->defaultValue(null),
            'sort' => $this->integer()->null()->defaultValue(100)
        ]);

        $this->createTable('news_category_lang', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'short_description' => $this->text()->null(),
        ]);

        $this->createTable('news_model', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'alias' => $this->string(128)->unique()->notNull(),
            'active' => $this->smallInteger()->defaultValue(0),
            'image' => $this->string(255)->null()->defaultValue(null),
            'views_count' => $this->integer()->notNull()->defaultValue(0),
            'published_at' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createTable('news_model_lang', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'short_description' => $this->text()->notNull(),
            'content' => $this->text()->notNull(),
        ]);

        $this->createTable('news_tag', [
            'id' => $this->primaryKey(),
            'alias' => $this->integer()->notNull(),
        ]);

        $this->createTable('news_tag_lang', [
            'id' => $this->primaryKey(),
            'tag_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
        ]);

        $this->createTable('news_news_tags', [
            'category_id' => $this->integer()->notNull(),
            'rubric_id' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('news_category');
        $this->dropTable('news_category_lang');
        $this->dropTable('news_model');
        $this->dropTable('news_model_lang');
        $this->dropTable('news_tag');
        $this->dropTable('news_tag_lang');
        $this->dropTable('news_news_tags');
    }
}
