<?php

use yii\db\Migration;

class m170522_134505_add_table_consumer_responsible extends Migration
{
    public function up()
    {
        $this->createTable('consumer_responsible', [
            'id' => $this->primaryKey(),
            'consumer_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->integer()->defaultValue(1)
         ]);
    }

    public function down()
    {
        echo "m170522_134505_add_table_consumer_responsible cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
