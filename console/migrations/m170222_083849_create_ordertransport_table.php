<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ordertransport`.
 */
class m170222_083849_create_ordertransport_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ordertransport', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'from_location' => $this->string(255)->notNull(),
            'to_location' => $this->string(255)->notNull(),
            'transport_type' => $this->integer()->notNull(),
            'message' => $this->text()->null(),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->batchInsert('{{%module}}', ['id', 'alias', 'controller_namespace', 'title', 'description'], [
            ["16","ordertransport","backend\\modules\\ordertransport\\controllers","Заказы транспорта","Модуль заказов транспорта"]
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["31","16","order","Заказы транспорта","Контроллер для управления заказами транспорта"]
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["147","31","index","Просмотр списка","Просмотр списка заказов"],
            ["148","31","view","Просмотр","Просмотр заказа"],
            ["149","31","change-status","Изменение статуса","Изменение статуса заказа"],
            ["150","31","update","Редактирование","Удаление заказа"]
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'parent_id', 'icon'], [
            ["36","/ordertransport/order/index","Заказы транспорта","Модуль заказов транспорта","","glyphicon glyphicon-phone-alt"]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('ordertransport');
    }
}
