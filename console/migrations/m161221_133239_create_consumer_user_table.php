<?php

use yii\db\Migration;

/**
 * Handles the creation of table `consumer_user`.
 */
class m161221_133239_create_consumer_user_table extends Migration
{
    public function up()
    {
        $tableOptions = null;

        $this->dropForeignKey('company_fkcu', '{{%company_user}}');
        $this->dropForeignKey('user_fkcu', '{{%company_user}}');
        $this->dropForeignKey('user_fke', '{{%enterpriser}}');

        $this->createTable('{{%consumer}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'name'=>$this->string(100)->notNull(),
            'surname'=>$this->string(100)->notNull(),
            'phone'=>$this->string(20)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m161221_133239_create_consumer_user_table cannot be reverted.\n";
        $this->dropTable('{{%consumer}}');
    }
}
