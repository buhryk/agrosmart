<?php

use yii\db\Migration;

class m170421_082533_create_table extends Migration
{
    public function up()
    {
        $this->addColumn('accounting', 'user_id', $this->integer()->notNull());
    }

    public function down()
    {
        echo "m170421_082533_create_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
