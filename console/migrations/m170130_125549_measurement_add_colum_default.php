<?php

use yii\db\Migration;

class m170130_125549_measurement_add_colum_default extends Migration
{
    public function up()
    {
        $this->addColumn('rubric_measurement', 'default', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        echo "m170130_125549_measurement_add_colum_default cannot be reverted.\n";

        return false;
    }


    
}
