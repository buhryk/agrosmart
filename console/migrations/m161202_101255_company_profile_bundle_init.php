<?php

use yii\db\Migration;

class m161202_101255_company_profile_bundle_init extends Migration
{
    public function up()
    {
        $this->createTable('{{%oblast}}', [
            'id' => $this->primaryKey(),
            'name' => "ENUM('select...', 'CherkasyO', 'ChernihivO', 'ChernivtsiO', 'DnipropetrovskO', 'DonetskO', 'Ivano-FrankivskO', 'KharkivO', 'KhersonO', 'KhmelnytskyiO', 'KievO', 'KropyvnytskyiO', 'LuhanskO', 'LvivO', 'MykolaivO', 'OdessaO', 'PoltavaO', 'RivneO', 'SumyO', 'TernopilO', 'VinnytsiaO', 'LutskO', 'UzhhorodO', 'ZaporizhiaO', 'ZhytomyrO') NOT NULL DEFAULT 'select...'",
        ]);
        $this->createTable('{{%subsidiary}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'company_user_id' => $this->integer()->notNull(),
            'type' => "ENUM('select...', 'main-office', 'sale-point', 'dealer-office') NOT NULL DEFAULT 'select...'",
            'address' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        /*$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%oblast}}', [
            'id' => $this->primaryKey(),
            'name' => "ENUM('select...', 'CherkasyO', 'ChernihivO', 'ChernivtsiO', 'DnipropetrovskO', 'DonetskO', 'Ivano-FrankivskO', 'KharkivO', 'KhersonO', 'KhmelnytskyiO', 'KievO', 'KropyvnytskyiO', 'LuhanskO', 'LvivO', 'MykolaivO', 'OdessaO', 'PoltavaO', 'RivneO', 'SumyO', 'TernopilO', 'VinnytsiaO', 'LutskO', 'UzhhorodO', 'ZaporizhiaO', 'ZhytomyrO') NOT NULL DEFAULT 'select...'",
        ], $tableOptions);
        $this->batchInsert('{{%oblast}}', ['name'], [
            ['CherkasyO'],
            ['ChernihivO'],
            ['ChernivtsiO'],
            ['DnipropetrovskO'],
            ['DonetskO'],
            ['Ivano-FrankivskO'],
            ['KharkivO'],
            ['KhersonO'],
            ['KhmelnytskyiO'],
            ['KievO'],
            ['KropyvnytskyiO'],
            ['LuhanskO'],
            ['LvivO'],
            ['MykolaivO'],
            ['OdessaO'],
            ['PoltavaO'],
            ['RivneO'],
            ['SumyO'],
            ['TernopilO'],
            ['VinnytsiaO'],
            ['LutskO'],
            ['UzhhorodO'],
            ['ZaporizhiaO'],
            ['ZhytomyrO']
        ]);



        $this->createTable('{{%subsidiary}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'company_user_id' => $this->integer()->notNull(),
            'type' => "ENUM('select...', 'main-office', 'sale-point', 'dealer-office') NOT NULL DEFAULT 'select...'",
            'address' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createTable('{{%elevator_requisite}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'company_user_id' => $this->integer()->notNull(),
            'oblast_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'type' => "ENUM('select...', 'state', 'SFGCU', 'SACU', 'SAC', 'other') NOT NULL DEFAULT 'select...'",
            'address' => $this->string()->notNull(),
            'volume' => $this->decimal(10,3)->notNull()->defaultValue(0),
            'daily_acceptance' => $this->decimal(10,3)->notNull()->defaultValue(0),
            'daily_dispatch' => $this->decimal(10,3)->notNull()->defaultValue(0),
            'autoway' => $this->boolean()->defaultValue(false),
            'waterway' => $this->boolean()->defaultValue(false),
            'railway' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createTable('{{%purchase}}', [
            'id' => $this->primaryKey(),
            'company_user_id' => $this->integer()->notNull(),
            'rubric_model_id' => $this->integer()->notNull(),
            'volume' => $this->decimal(10,3)->notNull()->defaultValue(0),
            'price' => $this->decimal(10,2)->notNull()->defaultValue(0),
            'location' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createTable('{{%sale}}', [
            'id' => $this->primaryKey(),
            'company_user_id' => $this->integer()->notNull(),
            'rubric_model_id' => $this->integer()->notNull(),
            'volume' => $this->decimal(10,3)->notNull()->defaultValue(0),
            'price' => $this->decimal(10,2)->notNull()->defaultValue(0),
            'location' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);*/
    }

    public function down()
    {
        /*
        $this->dropTable('{{%subsidiary}}');
        $this->dropTable('{{%elevator_requisite}}');

        $this->dropTable('{{%oblast}}');
        $this->dropTable('{{%purchase}}');
        $this->dropTable('{{%sale}}');*/
    }
}
