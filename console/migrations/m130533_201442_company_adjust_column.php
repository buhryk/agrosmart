<?php

use yii\db\Migration;

class m130533_201442_company_adjust_column extends Migration
{
    public function up()
    {
        $this->addColumn('company', 'logotype', $this->string()->notNull()->defaultValue('default_logo.jpg'));
        $this->addColumn('company', 'brief', $this->text());
        $this->addColumn('company', 'about', $this->text());
        $this->addColumn('company', 'title', $this->string()->notNull()->defaultValue(\Yii::t('dict', 'TITLE')));
    }
}
