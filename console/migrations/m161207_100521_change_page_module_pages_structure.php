<?php

use yii\db\Migration;

class m161207_100521_change_page_module_pages_structure extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE `page_category` CHANGE `alias` `alias` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;');
        $this->execute('ALTER TABLE `page_model` CHANGE `alias` `alias` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;');
    }

    public function down()
    {
        $this->execute('ALTER TABLE `page_category` CHANGE `alias` `alias` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;');
        $this->execute('ALTER TABLE `page_model` CHANGE `alias` `alias` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;');
    }
}
