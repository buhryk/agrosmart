<?php

use yii\db\Migration;

class m170407_135416_admin_module extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%module}}', ['id', 'alias', 'controller_namespace', 'title', 'description'], [
            ["17","translate","backend\\modules\\translate\\controllers","Переводы","Модуль перевода итерфейса"],
            ["18","package","backend\\modules\\package\\controllers","Пакеты","Модуль пакетов пользователей портала"],
            ["19","instrument","backend\\modules\\instrument\\controllers","Инструменты","Модуль инструментов с возможностью настраивать к ним доступ"],
            ["20","advertising","backend\\modules\\advertising\\controllers","Реклама","Модуль рекламных блоков"],
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["34","17","default","Перевод","Контроллер для перевода итерфейса"],
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["35","18","package","Пакеты","Контроллер для пакетов пользователей портала"],
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["36","19","instrument","Инструменты","Контроллер для роботы с инструментов"],
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["37","20","advertising","Баннера", "Контроллер для работы с баннерами"],
            ["38","20","advertising-group", "Рекламные секции","Контроллер для работы секциями "],
            ["39","20","advertising-position", "Позиции рекламных блоков","Позиции рекламных блоков"],
            ["40","20","default","Список баннеров", "Список баннеров"],
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["157","34","index","Просмотр списка","Просмотр списка переводов"],
            ["158","34","update","Перевод","Перевод итерфейса"],
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
           // ["159","35","index","Просмотр списка","Просмотр списка пакетов"],
            ["160","35","create","Создание","Создание пакетов"],
            ["161","35","view","Просмотр","Просмотр пакета"],
            ["162","35","update","Редактирование","Редактирование контакта пакетов"],
            ["163","35","delete","Удаление","Удаление  пакета"]
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["164","36","index","Просмотр списка","Просмотр списка инструментов"],
            ["166","36","create","Создание","Создание инструментов"],
            ["167","36","view","Просмотр","Просмотр инструментов"],
            ["168","36","update","Редактирование","Редактирование  инструмента"],
            ["169","36","delete","Удаление","Удаление инструмента"]
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["170","37","index","Просмотр списка","Просмотр списка баннеров"],
            ["171","37","create","Создание","Создание баннера"],
            ["172","37","update","Редактирование","Редактирование  баннера"],
            ["173","37","delete","Удаление","Удаление баннера"],

            ["174","38","index","Просмотр списка","Просмотр списка рекламные секций"],
            ["175","38","create","Создание","Создание рекламные секции"],
            ["176","38","update","Редактирование","Редактирование рекламные секции"],
            ["177","38","delete","Удаление","Удаление рекламные секции"],

            ["178","39","index","Просмотр списка","Просмотр списка позиции рекламных блоков"],
            ["179","39","create","Создание","Создание рекламные позиции рекламных блоков"],
            ["180","39","update","Редактирование","Редактирование позиции рекламных блоков"],
            ["181","39","delete","Удаление","Удаление позиции рекламных блоков"],

            ["182","40","index","Просмотр списка","Просмотр списка рекламных блоков "],

        ]);

    }

    public function down()
    {
        echo "m170407_135416_admin_module cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
