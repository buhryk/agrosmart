<?php

use yii\db\Migration;

class m170407_130509_add_colum extends Migration
{
    public function up()
    {
        $this->addColumn('package','old_price' , $this->decimal()->defaultValue(NULL));
    }

    public function down()
    {
        echo "m170407_130509_add_colum cannot be reverted.\n";

        return false;
    }


}
