<?php

use yii\db\Migration;

class m170405_142105_add_packed extends Migration
{
    public function up()
    {
        $this->createTable('site_role', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'authorized' => $this->smallInteger()->defaultValue('0'),
            'type' => $this->smallInteger()->defaultValue('0'),
            'page_id' => $this->integer(),
        ]);

        $this->createTable('package', [
            'id' => $this->primaryKey(),
            'site_role_id' =>  $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'price' => $this->decimal(),
            'count_user' => $this->integer(),
            'interim' => $this->integer(),
            'position' => $this->integer(),
        ]);

        $this->createTable('package_lang', [
            'id' => $this->primaryKey(),
            'package_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'text' => $this->text(),
        ]);

        $this->createTable('package_instrument', [
            'package_id' => $this->integer()->notNull(),
            'instrument_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('package_user', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'package_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'model_id' => $this->integer()->notNull(),
            'start' => $this->integer()->notNull(),
            'finish' => $this->integer()->notNull(),
            'refill_id' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {

    }
}
