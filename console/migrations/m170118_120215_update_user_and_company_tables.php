<?php

use yii\db\Migration;

class m170118_120215_update_user_and_company_tables extends Migration
{
    public function up()
    {
        $this->createTable('company_feedback', [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'price_relevance' => $this->integer()->null()->defaultValue(null),
            'description_relevance' => $this->integer()->null()->defaultValue(null),
            'conditions_relevance' => $this->integer()->null()->defaultValue(null),
            'text' => $this->text()->null()->defaultValue(null),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->addColumn('company', 'status', $this->integer()->notNull()->defaultValue(1));
        $this->addColumn('company', 'ratio', $this->integer()->null()->defaultValue(null));
        $this->addColumn('company', 'last_activity_date', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('consumer', 'last_activity_date', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('rubric_model', 'icon', $this->string(255)->null()->defaultValue(null));
        $this->addColumn('rubric_model', 'can_add_prices', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('company', 'status');
        $this->dropColumn('company', 'ratio');
        $this->dropColumn('company', 'last_activity_date');
        $this->dropColumn('consumer', 'last_activity_date');
        $this->dropTable('company_feedback');
        $this->dropColumn('rubric_model', 'icon');
        $this->dropColumn('rubric_model', 'can_add_prices');
    }
}
