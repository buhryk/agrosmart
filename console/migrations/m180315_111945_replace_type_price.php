<?php

use yii\db\Migration;

/**
 * Class m180315_111945_replace_type_price
 */
class m180315_111945_replace_type_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        ALTER TABLE price_item
        MODIFY price   varchar(100)     not null;
    ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180315_111945_replace_type_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180315_111945_replace_type_price cannot be reverted.\n";

        return false;
    }
    */
}
