<?php

use yii\db\Migration;

class m161213_150546_event_module extends Migration
{
    public function up()
    {
        $this->createTable('event_subject', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->unique()->notNull(),
        ]);

        $this->createTable('event_subject_lang', [
            'id' => $this->primaryKey(),
            'subject_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
        ]);

        $this->createTable('event_type', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->unique()->notNull(),
        ]);

        $this->createTable('event_type_lang', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
        ]);

        $this->createTable('event_country', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->unique()->notNull(),
        ]);

        $this->createTable('event_country_lang', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
        ]);

        $this->createTable('event_model', [
            'id' => $this->primaryKey(),
            'subject_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'alias' => $this->string(128)->unique()->notNull(),
            'image' => $this->string(255)->null()->defaultValue(null),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->null(),
            'active' => $this->smallInteger()->defaultValue(0)
        ]);

        $this->createTable('event_model_lang', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'short_description' => $this->text()->notNull(),
            'text' => $this->text()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('event_subject');
        $this->dropTable('event_subject_lang');
        $this->dropTable('event_type');
        $this->dropTable('event_type_lang');
        $this->dropTable('event_country');
        $this->dropTable('event_country_lang');
        $this->dropTable('event_model');
        $this->dropTable('event_model_lang');
    }
}
