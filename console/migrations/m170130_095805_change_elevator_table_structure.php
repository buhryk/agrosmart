<?php

use yii\db\Migration;

class m170130_095805_change_elevator_table_structure extends Migration
{
    public function up()
    {
        $this->dropTable('elevator_region');
        $this->dropTable('elevator_region_lang');
        $this->dropTable('elevator_phone');
        $this->dropColumn('elevator_model', 'region_id');
        $this->addColumn('elevator_model', 'city_id', $this->integer()->notNull());
        $this->addColumn('elevator_model', 'phones', $this->string(255)->null());
    }

    public function down()
    {
        echo "m170130_095805_change_elevator_table_structure cannot be reverted.\n";

        return false;
    }
}
