<?php

use yii\db\Migration;

/**
 * Handles the creation of table `elevator_contact`.
 */
class m170206_131006_create_elevator_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('elevator_contact', [
            'id' => $this->primaryKey(),
            'elevator_id' => $this->integer()->notNull(),
            'fio' => $this->string(255)->notNull(),
            'phones' => $this->string(255)->notNull(),
            'email' => $this->string(128)->null()->defaultValue(null)
        ]);

        $this->addColumn('elevator_model', 'email', $this->string(128)->null()->defaultValue(null));
        $this->addColumn('elevator_model', 'site', $this->string(128)->null()->defaultValue(null));
        $this->addColumn('elevator_model', 'laboratory', $this->integer()->null());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('elevator_contact');
        $this->dropColumn('elevator_model', 'email');
        $this->dropColumn('elevator_model', 'site');
        $this->dropColumn('elevator_model', 'laboratory');
    }
}
