<?php

use yii\db\Migration;

class m170211_120439_update_company_at extends Migration
{
    public function up()
    {
        $this->addColumn('company', 'created_at', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('company', 'created_at');
        echo "m170211_120439_update_company_at cannot be reverted.\n";

        return false;
    }

}
