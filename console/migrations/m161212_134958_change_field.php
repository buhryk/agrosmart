<?php

use yii\db\Migration;

class m161212_134958_change_field extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE  `news_news_tags` CHANGE  `category_id`  `news_id` INT( 11 ) NOT NULL,
          CHANGE  `rubric_id`  `tag_id` INT( 11 ) NOT NULL ;');
    }

    public function down()
    {
        $this->execute('ALTER TABLE  `news_news_tags` CHANGE  `news_id`  `category_id` INT( 11 ) NOT NULL,
          CHANGE  `tag_id`  `rubric_id` INT( 11 ) NOT NULL ;');
    }
}
