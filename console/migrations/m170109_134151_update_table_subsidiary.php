<?php

use yii\db\Migration;

class m170109_134151_update_table_subsidiary extends Migration
{
    public function up()
    {
        $this->dropColumn('subsidiary', 'type');
        $this->addColumn('subsidiary', 'type', $this->smallInteger()->notNull());

    }

    public function down()
    {

    }

}
