<?php

use yii\db\Migration;

class m170404_063146_addMenu extends Migration
{
    public function up()
    {

        $this->batchInsert('{{%admin_menu}}', ['title', 'description', 'icon', 'path'], [
            ["Инструменты", "Инструменты","glyphicon glyphicon glyphicon-wrench", '/instrument/instrument'],
        ]);

        $this->execute("
INSERT INTO `instrument` (`id`, `created_at`, `updated_at`, `position`, `status`, `image`, `type`, `url`) VALUES
(3, 1491218464, 1491238845, 3, 1, '/uploads/instrument/company-prices-map.png', 1, '/instruments/map-price-company/index'),
(4, 1491218593, 1491236950, 4, 1, '/uploads/instrument/elevators-map.png', 1, '/instruments/elevators/map'),
(5, 1491218711, 1491240068, 5, 1, '/uploads/instrument/logistic-calculator.png', 1, '/instruments/logistics-calculators/index'),
(6, 1491218788, 1491240363, 6, 1, '/uploads/instrument/sh-prices.png', 1, '/company/price/zakupka'),
(7, 1491218864, 1491240651, 7, 1, '/uploads/instrument/tstu-gost.png', 1, '/page/category/dstu'),
(8, 1491218952, 1491240726, 8, 1, '/uploads/instrument/incoterms.png', 1, '/page/inkoterms'),
(9, 1491219089, 1491240830, 9, 1, '/uploads/instrument/incoterms.png', 1, '/instruments/quality-calculation/index'),
(10, 1491221600, 1491241125, 10, 1, '/uploads/instrument/analiycs1.png', 2, '/instruments/fuel/prices'),
(11, 1491221652, 1491241495, 11, 1, '/uploads/instrument/analiycs2.png', 2, '/instruments/analytics-price/index');



INSERT INTO `instrument_accessibility` (`instrument_id`, `type_id`) VALUES
(4, 4),
(3, 5),
(5, 9),
(6, 9),
(7, 9),
(8, 5),
(9, 9),
(10, 9),
(11, 11);






INSERT INTO `instrument_lang` (`id`, `instrument_id`, `lang`, `title`, `description`, `text`) VALUES
(3, 3, 'ru-RU', 'Карта цен предприятий', '<p class=\"logistic-calc-item__text-title\">Вы понимаете, где выгоднее продать/купить\r\n</p><p class=\"logistic-calc-item__text\">На карте выводятся цены на все с/х продукты по каждой области в соотношении мин-макс\r\n</p>', ''),
(4, 4, 'ru-RU', 'Карта элеваторов', '<p class=\"logistic-calc-item__text-title\">\r\n		Сразу понятно, куда везти свой продукт\r\n</p><p class=\"logistic-calc-item__text\">\r\n		Собраны все элеваторы страны. На карте считается расстояние от вас до выбранного элеватора\r\n</p>', ''),
(5, 5, 'ru-RU', 'Логистический калькулятор', '<p class=\"logistic-calc-item__text-title\">Считаете затраты на авто перевозку</p><p class=\"logistic-calc-item__text\">Калькулятор дает вам общую стоимость на перевозку зерновых автотранспортом</p>', ''),
(6, 6, 'ru-RU', 'Цены на с/х продукцию', '<p class=\"logistic-calc-item__text-title\">Каталог агро-предприятий и их цен на с/х продукцию\r\n</p><p class=\"logistic-calc-item__text\">Каталог всех цен позволяет понять мин. и макс цену\r\n</p>', ''),
(7, 7, 'ru-RU', 'ДСТУ (ГОСТ)', '<p class=\"logistic-calc-item__text-title\">\r\n		Справочник ДСТУ на основную с/х продукцию\r\n	</p><p class=\"logistic-calc-item__text\">\r\n		Удобный помощник при проведении сделок и определении качества покупаемой продукции\r\n	</p>', ''),
(8, 8, 'ru-RU', 'Инкотермс', '<p class=\"logistic-calc-item__text-title\">\r\n		Понятный справочник с актуальными терминами\r\n	</p><p class=\"logistic-calc-item__text\">\r\n		Поддерживаем актуальность справочника\r\n	</p>', ''),
(9, 9, 'ru-RU', 'Расчет качества зерновых', '<p class=\"logistic-calc-item__text-title\">\r\n                            Понятный справочник с актуальными терминами                        </p><p class=\"logistic-calc-item__text\">\r\n                            Поддерживаем актуальность справочника                        </p>', ''),
(10, 10, 'ru-RU', 'Цены на топливо', '', ''),
(11, 11, 'ru-RU', 'Цены трейдеров', '', ''),
(12, 3, 'uk-UK', 'Карта цін підприємств', '<p class=\"logistic-calc-item__text-title\">Ви розумієте, де вигідно продати/купити</p><p class=\"logistic-calc-item__text\">На карті виводяться ціни на всі с/г продукти по кожній області в співвідношенні мін-макс</p>', ''),
(13, 4, 'uk-UK', 'Карта елеваторів', '<p class=\"logistic-calc-item__text-title\">\r\n                            Зразу зрозуміло, куда везти свій продукт                        </p><p class=\"logistic-calc-item__text\">\r\n                            Зібрані всі елеватори країни. На карті розраховується відстань від вас до обраного елеватора                        </p>', ''),
(14, 5, 'uk-UK', 'Логістичний калькулятор', '\r\n                        <p class=\"logistic-calc-item__text-title\">Рахуєте затрати на авто перевозку</p>\r\n                        <p class=\"logistic-calc-item__text\">Калькулятор дає вам загальну вартість на перевозку зернових автотранспортом</p>\r\n             ', ''),
(15, 6, 'uk-UK', 'Ціни на с/г продукцію', '<p class=\"logistic-calc-item__text-title\">Каталог агро-підприємств та їх ціни на с/г продукцію</p><p class=\"logistic-calc-item__text\">Каталог всіх цін дозволяє зрозуміти мін. та макс. ціну</p>', ''),
(16, 7, 'uk-UK', 'ДСТУ (ГОСТ)', '\r\n                            <p class=\"logistic-calc-item__text-title\">\r\n                                Довідник ДСТУ на основну с/г продукцію                            </p>\r\n                            <p class=\"logistic-calc-item__text\">\r\n                                Зручний помічник при проведенні операцій та визначенні якості продукції, що купується                            </p>\r\n                       ', ''),
(17, 8, 'uk-UK', 'Інкотермс', '<p class=\"logistic-calc-item__text-title\">\r\n                                Зрозумілий довідник з актуальними термінами                            </p><p class=\"logistic-calc-item__text\">\r\n                                Підтримуємо актуальність довідника                            </p>', ''),
(18, 9, 'uk-UK', 'Розрахунок якості зернових', '\r\n                        <p class=\"logistic-calc-item__text-title\">\r\n                            Зрозумілий довідник з актуальними термінами                        </p>\r\n                        <p class=\"logistic-calc-item__text\">\r\n                            Підтримуємо актуальність довідника                        </p>\r\n               ', ''),
(19, 10, 'uk-UK', 'Ціни на паливо', '', ''),
(20, 11, 'uk-UK', 'Ціни трейдерів', '', '');



INSERT INTO `instrument_visibility` (`instrument_id`, `type_id`) VALUES
(4, 5),
(4, 9),
(3, 9),
(5, 9),
(6, 9),
(7, 9),
(8, 9),
(9, 9),
(10, 9),
(11, 9);

        ");

    }

    public function down()
    {
        echo "m170404_063146_addMenu cannot be reverted.\n";

        return false;
    }


}
