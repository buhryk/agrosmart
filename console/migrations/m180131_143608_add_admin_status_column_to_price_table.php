<?php

use yii\db\Migration;

/**
 * Handles adding admin_status to table `price`.
 */
class m180131_143608_add_admin_status_column_to_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('price', 'admin_status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('price', 'admin_status');
    }
}
