<?php

use yii\db\Migration;

class m170214_072445_add_column_to_consumer_table extends Migration
{
    public function up()
    {
        $this->addColumn('consumer', 'new_email', $this->string(128)->null());
    }

    public function down()
    {
        $this->dropColumn('consumer', 'new_email');
    }
}
