<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rubric_lang`.
 */
class m161202_120901_create_rubric_lang_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('rubric_model_lang', [
            'id' => $this->primaryKey(),
            'rubric_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('rubric_lang');
    }
}
