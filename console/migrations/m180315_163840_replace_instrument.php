<?php

use yii\db\Migration;

/**
 * Class m180315_163840_replace_instrument
 */
class m180315_163840_replace_instrument extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        UPDATE instrument SET url='/company/price/search-index' WHERE id=6
    ");
        $this->execute("
        UPDATE  instrument_lang 
        SET title='Прайсы', 
        description='<p class=\"logistic-calc-item__text-title\">Прайс-листы компаний АПК Украины.
</p><p class=\"logistic-calc-item__text\">Выбирай агро продукцию или услуги по лучшим ценам.
</p>' WHERE instrument_id=6
    ");
    }

    /**
     * {@inheritdoc}
     */

    public function safeDown()
    {
        echo "m180315_163840_replace_instrument cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180315_163840_replace_instrument cannot be reverted.\n";

        return false;
    }
    */
}
