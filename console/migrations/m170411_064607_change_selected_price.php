<?php

use yii\db\Migration;

class m170411_064607_change_selected_price extends Migration
{
    public function up()
    {
        $this->dropColumn('selected_price', 'company_id');
        
        $this->createTable('selected_price_company', [
            'company_id' => $this->integer()->notNull(),
            'selected_price_id' => $this->integer()->notNull(),
        ]);

        $this->addColumn('selected_price', 'oblast_id', $this->integer()->notNull());
    }

    public function down()
    {
        echo "m170411_064607_change_selected_price cannot be reverted.\n";

        return false;
    }

}
