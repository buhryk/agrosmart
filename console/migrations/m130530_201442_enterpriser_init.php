<?php

use yii\db\Migration;

class m130530_201442_enterpriser_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%enterpriser}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('user_fke', '{{%enterpriser}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m130530_201442_enterpriser_init cannot be reverted.\n";
        $this->dropForeignKey('user_fke', '{{%enterpriser}}');
        $this->dropTable('{{%enterpriser}}');
    }
}
