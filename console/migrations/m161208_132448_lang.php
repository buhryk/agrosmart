<?php

use yii\db\Migration;

class m161208_132448_lang extends Migration
{
    public function up()
    {
        $this->createTable('lang', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'url' => $this->string(10)->notNull(),
            'default'=>$this->integer(2)->defaultValue(0),
            'date_create' => $this->integer(),
            'date_update' => $this->integer(),
            'local'=>$this->string('20')->notNull(),
            'active'=>$this->integer(2)->defaultValue(1)
        ]);

        $this->execute("INSERT INTO `lang` (`id`, `name`, `url`, `default`, `date_update`, `date_create`, `local`, `active`) VALUES
        (1, 'RU', 'ru', 1, 1460404786, 1460404786, 'ru-RU', 1),
        (2, 'Ua', 'ua', 0, 1460404786, 1460404786, 'uk-UK', 1);");
    }

    public function down()
    {
        $this->dropTable('lang');
    }
}
