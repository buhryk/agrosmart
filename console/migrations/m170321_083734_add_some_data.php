<?php

use yii\db\Migration;

class m170321_083734_add_some_data extends Migration
{
    public function up()
    {
        $this->addColumn('news_model', 'show_on_main_page', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('advertisement', 'admin_status', $this->integer()->notNull()->defaultValue(0));
        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["32","2","comment","Комментарии","Комментарии новостей"]
        ]);
        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["151","26","update","Редактирование","Редактирование пользователя"],
            ["152","27","update","Редактирование","Редактирование компании"],
            ["153","27","image-upload","Загрузка изображений","Загрузка изображений"],
            ["154","32","index","Просмотр списка","Просмотр списка комментариев"],
            ["155","32","view","Просмотр","Просмотр комментария"],
            ["156","32","update","Редактирование","Редактирование комментария"],
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'title', 'description', 'parent_id', 'icon', 'path'], [
            ["41","Комментарии","Комментарии новостей","7","","/news/comment/index"]
        ]);
    }

    public function down()
    {
        $this->dropColumn('news_model', 'show_on_main_page');
        $this->dropColumn('advertisement', 'admin_status');
    }
}
