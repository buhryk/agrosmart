<?php

use yii\db\Migration;

/**
 * Handles the creation of table `price`.
 */
class m180131_085758_create_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('price', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer(),
            'company_id' => $this->integer(),
            'udater_id' => $this->integer(),
            'name' => $this->string('255')->notNull(),
            'description' => $this->string('500'),
            'type' => $this->smallInteger()->defaultValue(1),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable('price_item', [
            'id' => $this->primaryKey(),
            'price_id' => $this->integer(),
            'name' => $this->string('255')->notNull(),
            'price' => $this->float()->notNull(),
            'column1' => $this->string('255')->null(),
            'column2' => $this->string('255')->null(),
            'column3' => $this->string('255')->null(),
        ]);

        $this->createTable('price_column',[
            'id' => $this->primaryKey(),
            'price_id' => $this->integer(),
            'name1' => $this->string('255')->null(),
            'name2' => $this->string('255')->null(),
            'name3' => $this->string('255')->null(),
        ]);

        $this->createIndex(
            'idx-price-author_id',
            'price',
            'author_id'
        );

        $this->addForeignKey(
            'fk-price-author_id',
            'price',
            'author_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-price-company_id',
            'price',
            'company_id'
        );

        $this->addForeignKey(
            'fk-price-company_id',
            'price',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-price-price_id',
            'price_item',
            'price_id'
        );

        $this->addForeignKey(
            'fk-price-price_id',
            'price_item',
            'price_id',
            'price',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-price_column-price_id',
            'price_column',
            'price_id'
        );

        $this->addForeignKey(
            'fk-price_column-price_id',
            'price_column',
            'price_id',
            'price',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('price');
    }
}
