<?php

use yii\db\Migration;

class m170405_101248_update_colum_elevator_contact extends Migration
{
    public function up()
    {
        $this->alterColumn('elevator_contact', 'phones', $this->string(250)->defaultValue(''));
        $this->alterColumn('elevator_contact_lang', 'fio', $this->string(250)->defaultValue(''));
    }
    
}
