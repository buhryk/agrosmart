<?php

use yii\db\Migration;

/**
 * Handles the creation of table `translates`.
 */
class m161206_151548_create_translates_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('translates', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->unique()->notNull(),
            'ru' => $this->text()->notNull(),
            'ua' => $this->text()->notNull(),
            'en' => $this->text()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('translates');
    }
}
