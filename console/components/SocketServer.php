<?php
namespace console\components;

use frontend\models\Test;
use function GuzzleHttp\Promise\promise_for;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use yii\base\Model;
use yii\helpers\Json;

class SocketServer implements MessageComponentInterface
{
    protected $clients;
    protected $users = [];


    public function __construct()
    {
        $this->clients = new \SplObjectStorage; // Для хранения технической информации об присоединившихся клиентах используется технология SplObjectStorage, встроенная в PHP
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        echo "New connection!= ({$conn->resourceId})\n";
    }


    /**
     * on message
     * @param \Ratchet\ConnectionInterface $from
     * @param type $msg
     */
    public function onMessage(\Ratchet\ConnectionInterface $from, $msg)
    {
        $numRecv = count($this->clients) - 1;

        $data = Json::decode($msg);
        if (is_array($data)) {
            echo "is array \n";
        }

        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        if (!is_array($data)) {
            return;
        }

        if (array_key_exists('u', $data) && array_key_exists('dest', $data) && $data['dest'] == 'registeruser') {
            $this->users[$data['u']] = $from;
        }

        if (array_key_exists('to', $data)) {
            $id = $data['to'];
            if (array_key_exists($id, $this->users) && $connection = $this->users[$id]) {
                $connection->send($this->normalizeMessage($data, $connection, $from));
            }
        }

        echo 'message='.$from->resourceId."\n";//id, присвоенное подключившемуся клиенту
    }

    /**
     * normalize message
     * @param mixed $message
     * @param \Ratchet\ConnectionInterface $connection
     * @return type
     */
    public function normalizeMessage($data, $connection, $sender = null)
    {
        $message = [
            'receiver' => $connection->resourceId,
            'sender' => ($sender ? $sender->resourceId : $connection->resourceId),
        ];
        if (!is_array($data)) {
            $message['data'] = $data;
        } else {
            $message = array_merge($message, $data);
        }
        return Json::encode($message);
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}